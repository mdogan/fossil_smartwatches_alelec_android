package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.pr4;
import com.fossil.blesdk.obfuscated.sr4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.model.ua.UAActivityTimeSeries;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.underamour.UASharePref;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hr3 {
    @DexIgnore
    public Retrofit a;
    @DexIgnore
    public dr3 b;
    @DexIgnore
    public ir3 c; // = new ir3("https://www.mapmyfitness.com/v7.1/");
    @DexIgnore
    public fr3 d;
    @DexIgnore
    public gr3 e;
    @DexIgnore
    public er3 f;
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public String h; // = "";

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void onSuccess();
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0017, code lost:
        if (r2 != null) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0023, code lost:
        if (r0 != null) goto L_0x0027;
     */
    @DexIgnore
    public final void a() {
        String str;
        String str2;
        Access a2 = new SoLibraryLoader().a((Context) PortfolioApp.W.c());
        if (a2 != null) {
            str = a2.getB();
        }
        str = "";
        this.g = str;
        if (a2 != null) {
            str2 = a2.getC();
        }
        str2 = "";
        this.h = str2;
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.a(HttpLoggingInterceptor.Level.NONE);
        OkHttpClient.b bVar = new OkHttpClient.b();
        bVar.a((Interceptor) httpLoggingInterceptor);
        OkHttpClient a3 = bVar.a();
        Retrofit.b bVar2 = new Retrofit.b();
        bVar2.a("https://api.ua.com/v7.1/");
        bVar2.a((pr4.a) ns4.a());
        bVar2.a((sr4.a) GsonConverterFactory.a());
        bVar2.a(a3);
        this.a = bVar2.a();
        Retrofit retrofit3 = this.a;
        if (retrofit3 != null) {
            this.b = (dr3) retrofit3.a(dr3.class);
            dr3 dr3 = this.b;
            if (dr3 != null) {
                String str3 = this.g;
                if (str3 != null) {
                    String str4 = this.h;
                    if (str4 != null) {
                        this.d = new fr3(dr3, str3, str4);
                        dr3 dr32 = this.b;
                        if (dr32 != null) {
                            fr3 fr3 = this.d;
                            if (fr3 != null) {
                                this.e = new gr3(dr32, fr3);
                                dr3 dr33 = this.b;
                                if (dr33 != null) {
                                    fr3 fr32 = this.d;
                                    if (fr32 != null) {
                                        String str5 = this.g;
                                        if (str5 != null) {
                                            this.f = new er3(dr33, fr32, str5);
                                        } else {
                                            wd4.a();
                                            throw null;
                                        }
                                    } else {
                                        wd4.a();
                                        throw null;
                                    }
                                } else {
                                    wd4.a();
                                    throw null;
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final Object b(String str, kc4<? super cb4> kc4) {
        if (this.e == null) {
            a();
        }
        gr3 gr3 = this.e;
        if (gr3 != null) {
            String str2 = this.g;
            if (str2 == null) {
                str2 = "";
            }
            gr3.a(str2);
            gr3.b(str);
            gr3.b();
        }
        return cb4.a;
    }

    @DexIgnore
    public final boolean b() {
        String a2 = UASharePref.c.a().a();
        return !(a2 == null || a2.length() == 0);
    }

    @DexIgnore
    public final Object a(String str, kc4<? super String> kc4) {
        if (TextUtils.isEmpty(this.g)) {
            a();
        }
        ir3 ir3 = this.c;
        String str2 = this.g;
        if (str2 == null) {
            str2 = "";
        }
        String url = ir3.a(str2, str).toString();
        wd4.a((Object) url, "uaUrlBuilder.buildOAuth2\u2026, redirectUrl).toString()");
        return url;
    }

    @DexIgnore
    public final Object a(String str, b bVar, kc4<? super cb4> kc4) {
        if (this.d == null) {
            a();
        }
        fr3 fr3 = this.d;
        if (fr3 != null) {
            fr3.a(str, bVar);
        }
        return cb4.a;
    }

    @DexIgnore
    public final Object a(UAActivityTimeSeries uAActivityTimeSeries, d dVar, kc4<? super cb4> kc4) {
        if (this.f == null) {
            a();
        }
        er3 er3 = this.f;
        if (er3 != null) {
            er3.a(uAActivityTimeSeries, dVar);
        }
        return cb4.a;
    }

    @DexIgnore
    public final Object a(c cVar, kc4<? super cb4> kc4) {
        if (this.d == null) {
            a();
        }
        fr3 fr3 = this.d;
        if (fr3 != null) {
            fr3.a(cVar);
        }
        return cb4.a;
    }
}
