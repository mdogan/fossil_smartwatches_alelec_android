package com.fossil.blesdk.obfuscated;

import android.os.Process;
import java.util.concurrent.BlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xh1 extends Thread {
    @DexIgnore
    public /* final */ Object e; // = new Object();
    @DexIgnore
    public /* final */ BlockingQueue<wh1<?>> f;
    @DexIgnore
    public /* final */ /* synthetic */ uh1 g;

    @DexIgnore
    public xh1(uh1 uh1, String str, BlockingQueue<wh1<?>> blockingQueue) {
        this.g = uh1;
        ck0.a(str);
        ck0.a(blockingQueue);
        this.f = blockingQueue;
        setName(str);
    }

    @DexIgnore
    public final void a() {
        synchronized (this.e) {
            this.e.notifyAll();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0065, code lost:
        r1 = r6.g.i;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x006b, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r6.g.j.release();
        r6.g.i.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0084, code lost:
        if (r6 != r6.g.c) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0086, code lost:
        com.fossil.blesdk.obfuscated.xh1 unused = r6.g.c = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0092, code lost:
        if (r6 != r6.g.d) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0094, code lost:
        com.fossil.blesdk.obfuscated.xh1 unused = r6.g.d = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x009a, code lost:
        r6.g.d().s().a("Current scheduler thread is neither worker nor network");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00a9, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00aa, code lost:
        return;
     */
    @DexIgnore
    public final void run() {
        boolean z = false;
        while (!z) {
            try {
                this.g.j.acquire();
                z = true;
            } catch (InterruptedException e2) {
                a(e2);
            }
        }
        try {
            int threadPriority = Process.getThreadPriority(Process.myTid());
            while (true) {
                wh1 wh1 = (wh1) this.f.poll();
                if (wh1 != null) {
                    Process.setThreadPriority(wh1.f ? threadPriority : 10);
                    wh1.run();
                } else {
                    synchronized (this.e) {
                        if (this.f.peek() == null && !this.g.k) {
                            try {
                                this.e.wait(30000);
                            } catch (InterruptedException e3) {
                                a(e3);
                            }
                        }
                    }
                    synchronized (this.g.i) {
                        if (this.f.peek() == null) {
                        }
                    }
                }
            }
        } catch (Throwable th) {
            synchronized (this.g.i) {
                this.g.j.release();
                this.g.i.notifyAll();
                if (this == this.g.c) {
                    xh1 unused = this.g.c = null;
                } else if (this == this.g.d) {
                    xh1 unused2 = this.g.d = null;
                } else {
                    this.g.d().s().a("Current scheduler thread is neither worker nor network");
                }
                throw th;
            }
        }
    }

    @DexIgnore
    public final void a(InterruptedException interruptedException) {
        this.g.d().v().a(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
    }
}
