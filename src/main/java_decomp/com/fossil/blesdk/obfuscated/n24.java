package com.fossil.blesdk.obfuscated;

import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class n24 extends TimerTask {
    @DexIgnore
    public /* final */ /* synthetic */ m24 e;

    @DexIgnore
    public n24(m24 m24) {
        this.e = m24;
    }

    @DexIgnore
    public void run() {
        if (i04.q()) {
            f24.b().e("TimerTask run");
        }
        k04.e(this.e.b);
        cancel();
        this.e.a();
    }
}
