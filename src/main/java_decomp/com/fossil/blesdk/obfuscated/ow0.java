package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ow0 extends nw0<FieldDescriptorType, Object> {
    @DexIgnore
    public ow0(int i) {
        super(i, (ow0) null);
    }

    @DexIgnore
    public final void g() {
        if (!a()) {
            for (int i = 0; i < b(); i++) {
                Map.Entry a = a(i);
                if (((ou0) a.getKey()).c()) {
                    a.setValue(Collections.unmodifiableList((List) a.getValue()));
                }
            }
            for (Map.Entry entry : c()) {
                if (((ou0) entry.getKey()).c()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.g();
    }
}
