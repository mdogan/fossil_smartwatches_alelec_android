package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r13 implements MembersInjector<DianaCustomizeEditActivity> {
    @DexIgnore
    public static void a(DianaCustomizeEditActivity dianaCustomizeEditActivity, DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
        dianaCustomizeEditActivity.B = dianaCustomizeEditPresenter;
    }

    @DexIgnore
    public static void a(DianaCustomizeEditActivity dianaCustomizeEditActivity, k42 k42) {
        dianaCustomizeEditActivity.C = k42;
    }
}
