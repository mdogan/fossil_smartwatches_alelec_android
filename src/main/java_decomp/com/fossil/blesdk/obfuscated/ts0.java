package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ts0 extends Handler {
    @DexIgnore
    public ts0(Looper looper) {
        super(looper);
    }

    @DexIgnore
    public final void dispatchMessage(Message message) {
        super.dispatchMessage(message);
    }

    @DexIgnore
    public ts0(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
