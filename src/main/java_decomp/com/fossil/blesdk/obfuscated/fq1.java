package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fq1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<fq1> CREATOR; // = new gq1();
    @DexIgnore
    public int e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ String j;
    @DexIgnore
    public /* final */ String k;
    @DexIgnore
    public /* final */ byte l;
    @DexIgnore
    public /* final */ byte m;
    @DexIgnore
    public /* final */ byte n;
    @DexIgnore
    public /* final */ byte o;
    @DexIgnore
    public /* final */ String p;

    @DexIgnore
    public fq1(int i2, String str, String str2, String str3, String str4, String str5, String str6, byte b, byte b2, byte b3, byte b4, String str7) {
        this.e = i2;
        this.f = str;
        this.g = str2;
        this.h = str3;
        this.i = str4;
        this.j = str5;
        this.k = str6;
        this.l = b;
        this.m = b2;
        this.n = b3;
        this.o = b4;
        this.p = str7;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && fq1.class == obj.getClass()) {
            fq1 fq1 = (fq1) obj;
            if (this.e != fq1.e || this.l != fq1.l || this.m != fq1.m || this.n != fq1.n || this.o != fq1.o || !this.f.equals(fq1.f)) {
                return false;
            }
            String str = this.g;
            if (str == null ? fq1.g != null : !str.equals(fq1.g)) {
                return false;
            }
            if (!this.h.equals(fq1.h) || !this.i.equals(fq1.i) || !this.j.equals(fq1.j)) {
                return false;
            }
            String str2 = this.k;
            if (str2 == null ? fq1.k != null : !str2.equals(fq1.k)) {
                return false;
            }
            String str3 = this.p;
            String str4 = fq1.p;
            if (str3 != null) {
                return str3.equals(str4);
            }
            if (str4 == null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (((this.e + 31) * 31) + this.f.hashCode()) * 31;
        String str = this.g;
        int i2 = 0;
        int hashCode2 = (((((((hashCode + (str != null ? str.hashCode() : 0)) * 31) + this.h.hashCode()) * 31) + this.i.hashCode()) * 31) + this.j.hashCode()) * 31;
        String str2 = this.k;
        int hashCode3 = (((((((((hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31) + this.l) * 31) + this.m) * 31) + this.n) * 31) + this.o) * 31;
        String str3 = this.p;
        if (str3 != null) {
            i2 = str3.hashCode();
        }
        return hashCode3 + i2;
    }

    @DexIgnore
    public final String toString() {
        int i2 = this.e;
        String str = this.f;
        String str2 = this.g;
        String str3 = this.h;
        String str4 = this.i;
        String str5 = this.j;
        String str6 = this.k;
        byte b = this.l;
        byte b2 = this.m;
        byte b3 = this.n;
        byte b4 = this.o;
        String str7 = this.p;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 211 + String.valueOf(str2).length() + String.valueOf(str3).length() + String.valueOf(str4).length() + String.valueOf(str5).length() + String.valueOf(str6).length() + String.valueOf(str7).length());
        sb.append("AncsNotificationParcelable{, id=");
        sb.append(i2);
        sb.append(", appId='");
        sb.append(str);
        sb.append('\'');
        sb.append(", dateTime='");
        sb.append(str2);
        sb.append('\'');
        sb.append(", notificationText='");
        sb.append(str3);
        sb.append('\'');
        sb.append(", title='");
        sb.append(str4);
        sb.append('\'');
        sb.append(", subtitle='");
        sb.append(str5);
        sb.append('\'');
        sb.append(", displayName='");
        sb.append(str6);
        sb.append('\'');
        sb.append(", eventId=");
        sb.append(b);
        sb.append(", eventFlags=");
        sb.append(b2);
        sb.append(", categoryId=");
        sb.append(b3);
        sb.append(", categoryCount=");
        sb.append(b4);
        sb.append(", packageName='");
        sb.append(str7);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 2, this.e);
        lk0.a(parcel, 3, this.f, false);
        lk0.a(parcel, 4, this.g, false);
        lk0.a(parcel, 5, this.h, false);
        lk0.a(parcel, 6, this.i, false);
        lk0.a(parcel, 7, this.j, false);
        String str = this.k;
        if (str == null) {
            str = this.f;
        }
        lk0.a(parcel, 8, str, false);
        lk0.a(parcel, 9, this.l);
        lk0.a(parcel, 10, this.m);
        lk0.a(parcel, 11, this.n);
        lk0.a(parcel, 12, this.o);
        lk0.a(parcel, 13, this.p, false);
        lk0.a(parcel, a);
    }
}
