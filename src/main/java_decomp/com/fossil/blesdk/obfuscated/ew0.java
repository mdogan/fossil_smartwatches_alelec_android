package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ew0 extends ge0<Object> implements pd0 {
    @DexIgnore
    public ew0(Context context) {
        super(context, nd0.o, null, (ef0) new se0());
    }

    @DexIgnore
    public static pd0 a(Context context) {
        return new ew0(context);
    }

    @DexIgnore
    public final ie0<Status> a(sd0 sd0) {
        jy0 jy0 = new jy0(sd0, a());
        a(jy0);
        return jy0;
    }
}
