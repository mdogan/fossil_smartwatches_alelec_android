package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.helper.AnalyticsHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wl2 extends vl2 {
    @DexIgnore
    public String i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wl2(AnalyticsHelper analyticsHelper, String str, String str2) {
        super(analyticsHelper, str, str2);
        wd4.b(analyticsHelper, "analyticsHelper");
        wd4.b(str, "traceName");
        tp4.a(wd4.a((Object) str, (Object) "view_appearance"), "traceName should be view_appearance", new Object[0]);
    }

    @DexIgnore
    public final boolean a(wl2 wl2) {
        if (wl2 == null) {
            return false;
        }
        return wd4.a((Object) wl2.i, (Object) this.i);
    }

    @DexIgnore
    public final wl2 b(String str) {
        wd4.b(str, "viewName");
        this.i = str;
        a("view_name", str);
        return this;
    }

    @DexIgnore
    public final String e() {
        return this.i;
    }

    @DexIgnore
    public String toString() {
        return "View name: " + this.i + ", running: " + b();
    }
}
