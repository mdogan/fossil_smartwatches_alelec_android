package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class o14 implements q24 {
    @DexIgnore
    public /* final */ /* synthetic */ List a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ h14 c;

    @DexIgnore
    public o14(h14 h14, List list, boolean z) {
        this.c = h14;
        this.a = list;
        this.b = z;
    }

    @DexIgnore
    public void a() {
        k04.c();
        this.c.a((List<r14>) this.a, this.b, true);
    }

    @DexIgnore
    public void b() {
        k04.d();
        this.c.a((List<r14>) this.a, 1, this.b, true);
    }
}
