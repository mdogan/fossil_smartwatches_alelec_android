package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class p14 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ int e;
    @DexIgnore
    public /* final */ /* synthetic */ h14 f;

    @DexIgnore
    public p14(h14 h14, int i) {
        this.f = h14;
        this.e = i;
    }

    @DexIgnore
    public void run() {
        this.f.b(this.e, true);
        this.f.b(this.e, false);
    }
}
