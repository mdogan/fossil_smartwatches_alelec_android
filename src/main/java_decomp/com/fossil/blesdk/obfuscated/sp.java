package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.CallbackException;
import com.bumptech.glide.load.engine.DecodeJob;
import com.bumptech.glide.load.engine.GlideException;
import com.fossil.blesdk.obfuscated.wp;
import com.fossil.blesdk.obfuscated.ww;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sp<R> implements DecodeJob.b<R>, ww.f {
    @DexIgnore
    public static /* final */ c C; // = new c();
    @DexIgnore
    public DecodeJob<R> A;
    @DexIgnore
    public volatile boolean B;
    @DexIgnore
    public /* final */ e e;
    @DexIgnore
    public /* final */ yw f;
    @DexIgnore
    public /* final */ wp.a g;
    @DexIgnore
    public /* final */ h8<sp<?>> h;
    @DexIgnore
    public /* final */ c i;
    @DexIgnore
    public /* final */ tp j;
    @DexIgnore
    public /* final */ er k;
    @DexIgnore
    public /* final */ er l;
    @DexIgnore
    public /* final */ er m;
    @DexIgnore
    public /* final */ er n;
    @DexIgnore
    public /* final */ AtomicInteger o;
    @DexIgnore
    public ko p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public bq<?> u;
    @DexIgnore
    public DataSource v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public GlideException x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public wp<?> z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ tv e;

        @DexIgnore
        public a(tv tvVar) {
            this.e = tvVar;
        }

        @DexIgnore
        public void run() {
            synchronized (this.e.a()) {
                synchronized (sp.this) {
                    if (sp.this.e.a(this.e)) {
                        sp.this.a(this.e);
                    }
                    sp.this.b();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ tv e;

        @DexIgnore
        public b(tv tvVar) {
            this.e = tvVar;
        }

        @DexIgnore
        public void run() {
            synchronized (this.e.a()) {
                synchronized (sp.this) {
                    if (sp.this.e.a(this.e)) {
                        sp.this.z.d();
                        sp.this.b(this.e);
                        sp.this.c(this.e);
                    }
                    sp.this.b();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public <R> wp<R> a(bq<R> bqVar, boolean z, ko koVar, wp.a aVar) {
            return new wp(bqVar, z, true, koVar, aVar);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ tv a;
        @DexIgnore
        public /* final */ Executor b;

        @DexIgnore
        public d(tv tvVar, Executor executor) {
            this.a = tvVar;
            this.b = executor;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof d) {
                return this.a.equals(((d) obj).a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Iterable<d> {
        @DexIgnore
        public /* final */ List<d> e;

        @DexIgnore
        public e() {
            this(new ArrayList(2));
        }

        @DexIgnore
        public static d c(tv tvVar) {
            return new d(tvVar, pw.a());
        }

        @DexIgnore
        public void a(tv tvVar, Executor executor) {
            this.e.add(new d(tvVar, executor));
        }

        @DexIgnore
        public void b(tv tvVar) {
            this.e.remove(c(tvVar));
        }

        @DexIgnore
        public void clear() {
            this.e.clear();
        }

        @DexIgnore
        public boolean isEmpty() {
            return this.e.isEmpty();
        }

        @DexIgnore
        public Iterator<d> iterator() {
            return this.e.iterator();
        }

        @DexIgnore
        public int size() {
            return this.e.size();
        }

        @DexIgnore
        public e(List<d> list) {
            this.e = list;
        }

        @DexIgnore
        public boolean a(tv tvVar) {
            return this.e.contains(c(tvVar));
        }

        @DexIgnore
        public e a() {
            return new e(new ArrayList(this.e));
        }
    }

    @DexIgnore
    public sp(er erVar, er erVar2, er erVar3, er erVar4, tp tpVar, wp.a aVar, h8<sp<?>> h8Var) {
        this(erVar, erVar2, erVar3, erVar4, tpVar, aVar, h8Var, C);
    }

    @DexIgnore
    public synchronized sp<R> a(ko koVar, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.p = koVar;
        this.q = z2;
        this.r = z3;
        this.s = z4;
        this.t = z5;
        return this;
    }

    @DexIgnore
    public synchronized void b(DecodeJob<R> decodeJob) {
        this.A = decodeJob;
        (decodeJob.u() ? this.k : c()).execute(decodeJob);
    }

    @DexIgnore
    public synchronized void c(tv tvVar) {
        boolean z2;
        this.f.a();
        this.e.b(tvVar);
        if (this.e.isEmpty()) {
            a();
            if (!this.w) {
                if (!this.y) {
                    z2 = false;
                    if (z2 && this.o.get() == 0) {
                        h();
                    }
                }
            }
            z2 = true;
            h();
        }
    }

    @DexIgnore
    public final boolean d() {
        return this.y || this.w || this.B;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        r4.j.a(r4, r1, (com.fossil.blesdk.obfuscated.wp<?>) null);
        r0 = r2.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003d, code lost:
        if (r0.hasNext() == false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003f, code lost:
        r1 = r0.next();
        r1.b.execute(new com.fossil.blesdk.obfuscated.sp.a(r4, r1.a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0052, code lost:
        b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0055, code lost:
        return;
     */
    @DexIgnore
    public void e() {
        synchronized (this) {
            this.f.a();
            if (this.B) {
                h();
            } else if (this.e.isEmpty()) {
                throw new IllegalStateException("Received an exception without any callbacks to notify");
            } else if (!this.y) {
                this.y = true;
                ko koVar = this.p;
                e a2 = this.e.a();
                a(a2.size() + 1);
            } else {
                throw new IllegalStateException("Already failed once");
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0046, code lost:
        r5.j.a(r5, r0, r2);
        r0 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
        if (r0.hasNext() == false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0055, code lost:
        r1 = r0.next();
        r1.b.execute(new com.fossil.blesdk.obfuscated.sp.b(r5, r1.a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0068, code lost:
        b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006b, code lost:
        return;
     */
    @DexIgnore
    public void f() {
        synchronized (this) {
            this.f.a();
            if (this.B) {
                this.u.a();
                h();
            } else if (this.e.isEmpty()) {
                throw new IllegalStateException("Received a resource without any callbacks to notify");
            } else if (!this.w) {
                this.z = this.i.a(this.u, this.q, this.p, this.g);
                this.w = true;
                e a2 = this.e.a();
                a(a2.size() + 1);
                ko koVar = this.p;
                wp<?> wpVar = this.z;
            } else {
                throw new IllegalStateException("Already have resource");
            }
        }
    }

    @DexIgnore
    public boolean g() {
        return this.t;
    }

    @DexIgnore
    public final synchronized void h() {
        if (this.p != null) {
            this.e.clear();
            this.p = null;
            this.z = null;
            this.u = null;
            this.y = false;
            this.B = false;
            this.w = false;
            this.A.a(false);
            this.A = null;
            this.x = null;
            this.v = null;
            this.h.a(this);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    public yw i() {
        return this.f;
    }

    @DexIgnore
    public sp(er erVar, er erVar2, er erVar3, er erVar4, tp tpVar, wp.a aVar, h8<sp<?>> h8Var, c cVar) {
        this.e = new e();
        this.f = yw.b();
        this.o = new AtomicInteger();
        this.k = erVar;
        this.l = erVar2;
        this.m = erVar3;
        this.n = erVar4;
        this.j = tpVar;
        this.g = aVar;
        this.h = h8Var;
        this.i = cVar;
    }

    @DexIgnore
    public void b(tv tvVar) {
        try {
            tvVar.a(this.z, this.v);
        } catch (Throwable th) {
            throw new CallbackException(th);
        }
    }

    @DexIgnore
    public synchronized void a(tv tvVar, Executor executor) {
        this.f.a();
        this.e.a(tvVar, executor);
        boolean z2 = true;
        if (this.w) {
            a(1);
            executor.execute(new b(tvVar));
        } else if (this.y) {
            a(1);
            executor.execute(new a(tvVar));
        } else {
            if (this.B) {
                z2 = false;
            }
            uw.a(z2, "Cannot add callbacks to a cancelled EngineJob");
        }
    }

    @DexIgnore
    public void b() {
        wp<?> wpVar;
        synchronized (this) {
            this.f.a();
            uw.a(d(), "Not yet complete!");
            int decrementAndGet = this.o.decrementAndGet();
            uw.a(decrementAndGet >= 0, "Can't decrement below 0");
            if (decrementAndGet == 0) {
                wpVar = this.z;
                h();
            } else {
                wpVar = null;
            }
        }
        if (wpVar != null) {
            wpVar.g();
        }
    }

    @DexIgnore
    public final er c() {
        if (this.r) {
            return this.m;
        }
        return this.s ? this.n : this.l;
    }

    @DexIgnore
    public void a(tv tvVar) {
        try {
            tvVar.a(this.x);
        } catch (Throwable th) {
            throw new CallbackException(th);
        }
    }

    @DexIgnore
    public void a() {
        if (!d()) {
            this.B = true;
            this.A.k();
            this.j.a(this, this.p);
        }
    }

    @DexIgnore
    public synchronized void a(int i2) {
        uw.a(d(), "Not yet complete!");
        if (this.o.getAndAdd(i2) == 0 && this.z != null) {
            this.z.d();
        }
    }

    @DexIgnore
    public void a(bq<R> bqVar, DataSource dataSource) {
        synchronized (this) {
            this.u = bqVar;
            this.v = dataSource;
        }
        f();
    }

    @DexIgnore
    public void a(GlideException glideException) {
        synchronized (this) {
            this.x = glideException;
        }
        e();
    }

    @DexIgnore
    public void a(DecodeJob<?> decodeJob) {
        c().execute(decodeJob);
    }
}
