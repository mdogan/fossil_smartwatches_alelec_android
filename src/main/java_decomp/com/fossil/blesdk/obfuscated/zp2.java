package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zp2 implements MembersInjector<WatchAppCommuteTimeManager> {
    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, PortfolioApp portfolioApp) {
        watchAppCommuteTimeManager.a = portfolioApp;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, ApiServiceV2 apiServiceV2) {
        watchAppCommuteTimeManager.b = apiServiceV2;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, LocationSource locationSource) {
        watchAppCommuteTimeManager.c = locationSource;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, UserRepository userRepository) {
        watchAppCommuteTimeManager.d = userRepository;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, fn2 fn2) {
        watchAppCommuteTimeManager.e = fn2;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, sc scVar) {
        watchAppCommuteTimeManager.f = scVar;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, mr2 mr2) {
        watchAppCommuteTimeManager.g = mr2;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, DianaPresetRepository dianaPresetRepository) {
        watchAppCommuteTimeManager.h = dianaPresetRepository;
    }
}
