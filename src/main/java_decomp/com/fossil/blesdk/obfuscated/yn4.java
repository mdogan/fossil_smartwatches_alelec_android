package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.km4;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.internal.http2.ErrorCode;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yn4 implements ln4 {
    @DexIgnore
    public static /* final */ List<String> f; // = vm4.a((T[]) new String[]{"connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade", ":method", ":path", ":scheme", ":authority"});
    @DexIgnore
    public static /* final */ List<String> g; // = vm4.a((T[]) new String[]{"connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade"});
    @DexIgnore
    public /* final */ Interceptor.Chain a;
    @DexIgnore
    public /* final */ in4 b;
    @DexIgnore
    public /* final */ zn4 c;
    @DexIgnore
    public bo4 d;
    @DexIgnore
    public /* final */ Protocol e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ap4 {
        @DexIgnore
        public boolean f; // = false;
        @DexIgnore
        public long g; // = 0;

        @DexIgnore
        public a(kp4 kp4) {
            super(kp4);
        }

        @DexIgnore
        public final void a(IOException iOException) {
            if (!this.f) {
                this.f = true;
                yn4 yn4 = yn4.this;
                yn4.b.a(false, yn4, this.g, iOException);
            }
        }

        @DexIgnore
        public long b(vo4 vo4, long j) throws IOException {
            try {
                long b = c().b(vo4, j);
                if (b > 0) {
                    this.g += b;
                }
                return b;
            } catch (IOException e) {
                a(e);
                throw e;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            super.close();
            a((IOException) null);
        }
    }

    @DexIgnore
    public yn4(OkHttpClient okHttpClient, Interceptor.Chain chain, in4 in4, zn4 zn4) {
        Protocol protocol;
        this.a = chain;
        this.b = in4;
        this.c = zn4;
        if (okHttpClient.A().contains(Protocol.H2_PRIOR_KNOWLEDGE)) {
            protocol = Protocol.H2_PRIOR_KNOWLEDGE;
        } else {
            protocol = Protocol.HTTP_2;
        }
        this.e = protocol;
    }

    @DexIgnore
    public jp4 a(pm4 pm4, long j) {
        return this.d.d();
    }

    @DexIgnore
    public void b() throws IOException {
        this.c.flush();
    }

    @DexIgnore
    public void cancel() {
        bo4 bo4 = this.d;
        if (bo4 != null) {
            bo4.c(ErrorCode.CANCEL);
        }
    }

    @DexIgnore
    public static List<vn4> b(pm4 pm4) {
        km4 c2 = pm4.c();
        ArrayList arrayList = new ArrayList(c2.b() + 4);
        arrayList.add(new vn4(vn4.f, pm4.e()));
        arrayList.add(new vn4(vn4.g, rn4.a(pm4.g())));
        String a2 = pm4.a("Host");
        if (a2 != null) {
            arrayList.add(new vn4(vn4.i, a2));
        }
        arrayList.add(new vn4(vn4.h, pm4.g().n()));
        int b2 = c2.b();
        for (int i = 0; i < b2; i++) {
            ByteString encodeUtf8 = ByteString.encodeUtf8(c2.a(i).toLowerCase(Locale.US));
            if (!f.contains(encodeUtf8.utf8())) {
                arrayList.add(new vn4(encodeUtf8, c2.b(i)));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public void a(pm4 pm4) throws IOException {
        if (this.d == null) {
            this.d = this.c.a(b(pm4), pm4.a() != null);
            this.d.h().a((long) this.a.a(), TimeUnit.MILLISECONDS);
            this.d.l().a((long) this.a.b(), TimeUnit.MILLISECONDS);
        }
    }

    @DexIgnore
    public void a() throws IOException {
        this.d.d().close();
    }

    @DexIgnore
    public Response.a a(boolean z) throws IOException {
        Response.a a2 = a(this.d.j(), this.e);
        if (!z || tm4.a.a(a2) != 100) {
            return a2;
        }
        return null;
    }

    @DexIgnore
    public static Response.a a(km4 km4, Protocol protocol) throws IOException {
        km4.a aVar = new km4.a();
        int b2 = km4.b();
        tn4 tn4 = null;
        for (int i = 0; i < b2; i++) {
            String a2 = km4.a(i);
            String b3 = km4.b(i);
            if (a2.equals(":status")) {
                tn4 = tn4.a("HTTP/1.1 " + b3);
            } else if (!g.contains(a2)) {
                tm4.a.a(aVar, a2, b3);
            }
        }
        if (tn4 != null) {
            Response.a aVar2 = new Response.a();
            aVar2.a(protocol);
            aVar2.a(tn4.b);
            aVar2.a(tn4.c);
            aVar2.a(aVar.a());
            return aVar2;
        }
        throw new ProtocolException("Expected ':status' header not present");
    }

    @DexIgnore
    public qm4 a(Response response) throws IOException {
        in4 in4 = this.b;
        in4.f.e(in4.e);
        return new qn4(response.e(GraphRequest.CONTENT_TYPE_HEADER), nn4.a(response), ep4.a((kp4) new a(this.d.e())));
    }
}
