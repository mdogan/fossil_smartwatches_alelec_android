package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.vt2;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n03 extends as2 implements m03, xs3.g {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((rd4) null);
    @DexIgnore
    public l03 j;
    @DexIgnore
    public ur3<de2> k;
    @DexIgnore
    public vt2 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return n03.n;
        }

        @DexIgnore
        public final n03 b() {
            return new n03();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements vt2.b {
        @DexIgnore
        public /* final */ /* synthetic */ n03 a;

        @DexIgnore
        public b(n03 n03) {
            this.a = n03;
        }

        @DexIgnore
        public void a(ContactWrapper contactWrapper) {
            wd4.b(contactWrapper, "contactWrapper");
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(childFragmentManager, contactWrapper, contactWrapper.getCurrentHandGroup(), n03.b(this.a).h());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ n03 e;

        @DexIgnore
        public c(n03 n03) {
            this.e = n03;
        }

        @DexIgnore
        public final void onClick(View view) {
            n03.b(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ n03 e;
        @DexIgnore
        public /* final */ /* synthetic */ de2 f;

        @DexIgnore
        public d(n03 n03, de2 de2) {
            this.e = n03;
            this.f = de2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.f.s;
            wd4.a((Object) imageView, "binding.ivClear");
            imageView.setVisibility(i3 > 0 ? 0 : 4);
            vt2 a = this.e.l;
            if (a != null) {
                a.a(String.valueOf(charSequence));
            }
            AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = this.f.t;
            wd4.a((Object) alphabetFastScrollRecyclerView, "binding.rvContacts");
            RecyclerView.m layoutManager = alphabetFastScrollRecyclerView.getLayoutManager();
            if (layoutManager != null) {
                ((LinearLayoutManager) layoutManager).f(0, 0);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ de2 e;

        @DexIgnore
        public e(de2 de2) {
            this.e = de2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.q.setText("");
        }
    }

    /*
    static {
        String simpleName = n03.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationHybridContac\u2026nt::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ l03 b(n03 n03) {
        l03 l03 = n03.j;
        if (l03 != null) {
            return l03;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void H() {
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        l03 l03 = this.j;
        if (l03 != null) {
            l03.i();
            return true;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Typeface typeface;
        wd4.b(layoutInflater, "inflater");
        de2 de2 = (de2) ra.a(layoutInflater, R.layout.fragment_notification_hybrid_contact, viewGroup, false, O0());
        de2.r.setOnClickListener(new c(this));
        de2.q.addTextChangedListener(new d(this, de2));
        de2.s.setOnClickListener(new e(de2));
        de2.t.setIndexBarVisibility(true);
        de2.t.setIndexBarHighLateTextVisibility(true);
        de2.t.setIndexbarHighLateTextColor((int) R.color.greyishBrown);
        de2.t.setIndexBarTransparentValue(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        de2.t.setIndexTextSize(9);
        de2.t.setIndexBarTextColor((int) R.color.black);
        if (Build.VERSION.SDK_INT >= 26) {
            typeface = getResources().getFont(R.font.font_bold);
            wd4.a((Object) typeface, "resources.getFont(R.font.font_bold)");
        } else {
            typeface = Typeface.DEFAULT_BOLD;
            wd4.a((Object) typeface, "Typeface.DEFAULT_BOLD");
        }
        de2.t.setTypeface(typeface);
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = de2.t;
        alphabetFastScrollRecyclerView.setLayoutManager(new LinearLayoutManager(alphabetFastScrollRecyclerView.getContext()));
        alphabetFastScrollRecyclerView.setAdapter(this.l);
        this.k = new ur3<>(this, de2);
        wd4.a((Object) de2, "binding");
        return de2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        l03 l03 = this.j;
        if (l03 != null) {
            l03.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        l03 l03 = this.j;
        if (l03 != null) {
            l03.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void x0() {
        vt2 vt2 = this.l;
        if (vt2 != null) {
            vt2.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public void a(l03 l03) {
        wd4.b(l03, "presenter");
        this.j = l03;
    }

    @DexIgnore
    public void a(List<ContactWrapper> list, FilterQueryProvider filterQueryProvider, int i) {
        wd4.b(list, "listContactWrapper");
        wd4.b(filterQueryProvider, "filterQueryProvider");
        vt2 vt2 = new vt2((Cursor) null, list, i);
        vt2.a((vt2.b) new b(this));
        this.l = vt2;
        vt2 vt22 = this.l;
        if (vt22 != null) {
            vt22.a(filterQueryProvider);
            ur3<de2> ur3 = this.k;
            if (ur3 != null) {
                de2 a2 = ur3.a();
                if (a2 != null) {
                    AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = a2.t;
                    if (alphabetFastScrollRecyclerView != null) {
                        alphabetFastScrollRecyclerView.setAdapter(this.l);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void a(ArrayList<ContactWrapper> arrayList) {
        wd4.b(arrayList, "contactWrappersSelected");
        Intent intent = new Intent();
        intent.putExtra("CONTACT_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void a(Cursor cursor) {
        vt2 vt2 = this.l;
        if (vt2 != null) {
            vt2.c(cursor);
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        if (str.hashCode() != 1018078562 || !str.equals("CONFIRM_REASSIGN_CONTACT")) {
            return;
        }
        if (i != R.id.tv_ok) {
            vt2 vt2 = this.l;
            if (vt2 != null) {
                vt2.notifyDataSetChanged();
            } else {
                wd4.a();
                throw null;
            }
        } else {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null) {
                ContactWrapper contactWrapper = (ContactWrapper) extras.getSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER");
                if (contactWrapper != null) {
                    l03 l03 = this.j;
                    if (l03 != null) {
                        l03.a(contactWrapper);
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        }
    }
}
