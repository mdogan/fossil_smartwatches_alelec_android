package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pv3 extends zo4 {
    @DexIgnore
    public boolean f;

    @DexIgnore
    public pv3(jp4 jp4) {
        super(jp4);
    }

    @DexIgnore
    public void a(vo4 vo4, long j) throws IOException {
        if (this.f) {
            vo4.skip(j);
            return;
        }
        try {
            super.a(vo4, j);
        } catch (IOException e) {
            this.f = true;
            a(e);
        }
    }

    @DexIgnore
    public void a(IOException iOException) {
        throw null;
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.f) {
            try {
                super.close();
            } catch (IOException e) {
                this.f = true;
                a(e);
            }
        }
    }

    @DexIgnore
    public void flush() throws IOException {
        if (!this.f) {
            try {
                super.flush();
            } catch (IOException e) {
                this.f = true;
                a(e);
            }
        }
    }
}
