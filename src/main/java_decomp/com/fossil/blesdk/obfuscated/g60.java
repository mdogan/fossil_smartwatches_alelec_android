package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.file.FileHandle;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class g60 extends Phase {
    @DexIgnore
    public /* final */ short A;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z; // = k90.a(super.n(), ob4.a((T[]) new ResourceType[]{ResourceType.FILE_CONFIG, ResourceType.TRANSFER_DATA}));

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g60(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, short s, String str) {
        super(peripheral, aVar, phaseId, str);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(phaseId, "id");
        wd4.b(str, "phaseUuid");
        this.A = s;
    }

    @DexIgnore
    public final short A() {
        return this.A;
    }

    @DexIgnore
    public boolean a(Phase phase) {
        wd4.b(phase, "otherPhase");
        return !(phase instanceof g60) || ((g60) phase).A != this.A;
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(xa0.a(super.u(), JSONKey.FILE_HANDLE, o90.a(this.A)), JSONKey.FILE_HANDLE_DESCRIPTION, FileHandle.Companion.a(this.A));
    }

    @DexIgnore
    public boolean a(Request request) {
        RequestId f = request != null ? request.f() : null;
        return f != null && f60.a[f.ordinal()] == 1;
    }
}
