package com.fossil.blesdk.obfuscated;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yp {
    @DexIgnore
    public /* final */ Map<ko, sp<?>> a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<ko, sp<?>> b; // = new HashMap();

    @DexIgnore
    public sp<?> a(ko koVar, boolean z) {
        return a(z).get(koVar);
    }

    @DexIgnore
    public void b(ko koVar, sp<?> spVar) {
        Map<ko, sp<?>> a2 = a(spVar.g());
        if (spVar.equals(a2.get(koVar))) {
            a2.remove(koVar);
        }
    }

    @DexIgnore
    public void a(ko koVar, sp<?> spVar) {
        a(spVar.g()).put(koVar, spVar);
    }

    @DexIgnore
    public final Map<ko, sp<?>> a(boolean z) {
        return z ? this.b : this.a;
    }
}
