package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class b24 {
    @DexIgnore
    public static /* final */ /* synthetic */ boolean a; // = (!b24.class.desiredAssertionStatus());

    @DexIgnore
    public static byte[] a(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }

    @DexIgnore
    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        d24 d24 = new d24(i3, new byte[((i2 * 3) / 4)]);
        if (d24.a(bArr, i, i2, true)) {
            int i4 = d24.b;
            byte[] bArr2 = d24.a;
            if (i4 == bArr2.length) {
                return bArr2;
            }
            byte[] bArr3 = new byte[i4];
            System.arraycopy(bArr2, 0, bArr3, 0, i4);
            return bArr3;
        }
        throw new IllegalArgumentException("bad base-64");
    }

    @DexIgnore
    public static byte[] b(byte[] bArr, int i) {
        return b(bArr, 0, bArr.length, i);
    }

    @DexIgnore
    public static byte[] b(byte[] bArr, int i, int i2, int i3) {
        e24 e24 = new e24(i3, (byte[]) null);
        int i4 = (i2 / 3) * 4;
        int i5 = 2;
        if (!e24.f) {
            int i6 = i2 % 3;
            if (i6 != 0) {
                if (i6 == 1) {
                    i4 += 2;
                } else if (i6 == 2) {
                    i4 += 3;
                }
            }
        } else if (i2 % 3 > 0) {
            i4 += 4;
        }
        if (e24.g && i2 > 0) {
            int i7 = ((i2 - 1) / 57) + 1;
            if (!e24.h) {
                i5 = 1;
            }
            i4 += i7 * i5;
        }
        e24.a = new byte[i4];
        e24.a(bArr, i, i2, true);
        if (a || e24.b == i4) {
            return e24.a;
        }
        throw new AssertionError();
    }
}
