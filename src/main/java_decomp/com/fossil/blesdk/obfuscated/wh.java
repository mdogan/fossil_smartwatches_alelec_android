package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wh {
    @DexIgnore
    public static vh a(ViewGroup viewGroup) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new uh(viewGroup);
        }
        return th.a(viewGroup);
    }

    @DexIgnore
    public static void a(ViewGroup viewGroup, boolean z) {
        if (Build.VERSION.SDK_INT >= 18) {
            yh.a(viewGroup, z);
        } else {
            xh.a(viewGroup, z);
        }
    }
}
