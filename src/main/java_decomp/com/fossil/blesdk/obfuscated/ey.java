package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.os.Build;
import com.crashlytics.android.answers.SessionEvent;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ey implements p64<SessionEvent> {
    @DexIgnore
    /* renamed from: b */
    public byte[] a(SessionEvent sessionEvent) throws IOException {
        return a(sessionEvent).toString().getBytes("UTF-8");
    }

    @DexIgnore
    @TargetApi(9)
    public JSONObject a(SessionEvent sessionEvent) throws IOException {
        try {
            JSONObject jSONObject = new JSONObject();
            dy dyVar = sessionEvent.a;
            jSONObject.put("appBundleId", dyVar.a);
            jSONObject.put("executionId", dyVar.b);
            jSONObject.put("installationId", dyVar.c);
            jSONObject.put("limitAdTrackingEnabled", dyVar.d);
            jSONObject.put("betaDeviceToken", dyVar.e);
            jSONObject.put("buildId", dyVar.f);
            jSONObject.put("osVersion", dyVar.g);
            jSONObject.put("deviceModel", dyVar.h);
            jSONObject.put("appVersionCode", dyVar.i);
            jSONObject.put("appVersionName", dyVar.j);
            jSONObject.put("timestamp", sessionEvent.b);
            jSONObject.put("type", sessionEvent.c.toString());
            if (sessionEvent.d != null) {
                jSONObject.put("details", new JSONObject(sessionEvent.d));
            }
            jSONObject.put("customType", sessionEvent.e);
            if (sessionEvent.f != null) {
                jSONObject.put("customAttributes", new JSONObject(sessionEvent.f));
            }
            jSONObject.put("predefinedType", sessionEvent.g);
            if (sessionEvent.h != null) {
                jSONObject.put("predefinedAttributes", new JSONObject(sessionEvent.h));
            }
            return jSONObject;
        } catch (JSONException e) {
            if (Build.VERSION.SDK_INT >= 9) {
                throw new IOException(e.getMessage(), e);
            }
            throw new IOException(e.getMessage());
        }
    }
}
