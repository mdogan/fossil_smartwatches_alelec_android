package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class c21 extends k01 implements b21 {
    @DexIgnore
    public c21() {
        super("com.google.android.gms.fitness.internal.service.IFitnessSensorService");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            a((x11) z01.a(parcel, x11.CREATOR), q01.a(parcel.readStrongBinder()));
        } else if (i == 2) {
            a((lq0) z01.a(parcel, lq0.CREATOR), i11.a(parcel.readStrongBinder()));
        } else if (i != 3) {
            return false;
        } else {
            a((z11) z01.a(parcel, z11.CREATOR), i11.a(parcel.readStrongBinder()));
        }
        return true;
    }
}
