package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r42 implements Factory<ApiServiceV2> {
    @DexIgnore
    public /* final */ o42 a;
    @DexIgnore
    public /* final */ Provider<yo2> b;
    @DexIgnore
    public /* final */ Provider<cp2> c;

    @DexIgnore
    public r42(o42 o42, Provider<yo2> provider, Provider<cp2> provider2) {
        this.a = o42;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static r42 a(o42 o42, Provider<yo2> provider, Provider<cp2> provider2) {
        return new r42(o42, provider, provider2);
    }

    @DexIgnore
    public static ApiServiceV2 b(o42 o42, Provider<yo2> provider, Provider<cp2> provider2) {
        return a(o42, provider.get(), provider2.get());
    }

    @DexIgnore
    public static ApiServiceV2 a(o42 o42, yo2 yo2, cp2 cp2) {
        ApiServiceV2 a2 = o42.a(yo2, cp2);
        o44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public ApiServiceV2 get() {
        return b(this.a, this.b, this.c);
    }
}
