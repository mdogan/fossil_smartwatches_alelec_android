package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dg3;
import com.fossil.blesdk.obfuscated.eg3;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface xf3 extends w52<wf3> {
    @DexIgnore
    void W();

    @DexIgnore
    void a(MFSleepDay mFSleepDay);

    @DexIgnore
    void a(Date date, boolean z, boolean z2, boolean z3);

    @DexIgnore
    void c(ArrayList<dg3.a> arrayList);

    @DexIgnore
    void p(List<eg3.b> list);
}
