package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.legacy.threedotzero.ActivePreset;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* compiled from: lambda */
public final /* synthetic */ class g82 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon19 e;
    @DexIgnore
    private /* final */ /* synthetic */ ActivePreset f;

    @DexIgnore
    public /* synthetic */ g82(PresetRepository.Anon19 anon19, ActivePreset activePreset) {
        this.e = anon19;
        this.f = activePreset;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f);
    }
}
