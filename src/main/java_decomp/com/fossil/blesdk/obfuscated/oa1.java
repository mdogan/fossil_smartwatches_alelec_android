package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface oa1<T> {
    @DexIgnore
    int a(T t);

    @DexIgnore
    T a();

    @DexIgnore
    void a(T t, na1 na1, j81 j81) throws IOException;

    @DexIgnore
    void a(T t, tb1 tb1) throws IOException;

    @DexIgnore
    boolean a(T t, T t2);

    @DexIgnore
    int b(T t);

    @DexIgnore
    void b(T t, T t2);

    @DexIgnore
    boolean c(T t);

    @DexIgnore
    void d(T t);
}
