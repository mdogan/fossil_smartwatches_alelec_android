package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j61 extends wb1<j61> {
    @DexIgnore
    public static volatile j61[] i;
    @DexIgnore
    public Long c; // = null;
    @DexIgnore
    public String d; // = null;
    @DexIgnore
    public String e; // = null;
    @DexIgnore
    public Long f; // = null;
    @DexIgnore
    public Float g; // = null;
    @DexIgnore
    public Double h; // = null;

    @DexIgnore
    public j61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static j61[] e() {
        if (i == null) {
            synchronized (ac1.b) {
                if (i == null) {
                    i = new j61[0];
                }
            }
        }
        return i;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        Long l = this.c;
        if (l != null) {
            vb1.b(1, l.longValue());
        }
        String str = this.d;
        if (str != null) {
            vb1.a(2, str);
        }
        String str2 = this.e;
        if (str2 != null) {
            vb1.a(3, str2);
        }
        Long l2 = this.f;
        if (l2 != null) {
            vb1.b(4, l2.longValue());
        }
        Float f2 = this.g;
        if (f2 != null) {
            vb1.a(5, f2.floatValue());
        }
        Double d2 = this.h;
        if (d2 != null) {
            vb1.a(6, d2.doubleValue());
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof j61)) {
            return false;
        }
        j61 j61 = (j61) obj;
        Long l = this.c;
        if (l == null) {
            if (j61.c != null) {
                return false;
            }
        } else if (!l.equals(j61.c)) {
            return false;
        }
        String str = this.d;
        if (str == null) {
            if (j61.d != null) {
                return false;
            }
        } else if (!str.equals(j61.d)) {
            return false;
        }
        String str2 = this.e;
        if (str2 == null) {
            if (j61.e != null) {
                return false;
            }
        } else if (!str2.equals(j61.e)) {
            return false;
        }
        Long l2 = this.f;
        if (l2 == null) {
            if (j61.f != null) {
                return false;
            }
        } else if (!l2.equals(j61.f)) {
            return false;
        }
        Float f2 = this.g;
        if (f2 == null) {
            if (j61.g != null) {
                return false;
            }
        } else if (!f2.equals(j61.g)) {
            return false;
        }
        Double d2 = this.h;
        if (d2 == null) {
            if (j61.h != null) {
                return false;
            }
        } else if (!d2.equals(j61.h)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(j61.b);
        }
        yb1 yb12 = j61.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (j61.class.getName().hashCode() + 527) * 31;
        Long l = this.c;
        int i2 = 0;
        int hashCode2 = (hashCode + (l == null ? 0 : l.hashCode())) * 31;
        String str = this.d;
        int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.e;
        int hashCode4 = (hashCode3 + (str2 == null ? 0 : str2.hashCode())) * 31;
        Long l2 = this.f;
        int hashCode5 = (hashCode4 + (l2 == null ? 0 : l2.hashCode())) * 31;
        Float f2 = this.g;
        int hashCode6 = (hashCode5 + (f2 == null ? 0 : f2.hashCode())) * 31;
        Double d2 = this.h;
        int hashCode7 = (hashCode6 + (d2 == null ? 0 : d2.hashCode())) * 31;
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            i2 = this.b.hashCode();
        }
        return hashCode7 + i2;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Long l = this.c;
        if (l != null) {
            a += vb1.c(1, l.longValue());
        }
        String str = this.d;
        if (str != null) {
            a += vb1.b(2, str);
        }
        String str2 = this.e;
        if (str2 != null) {
            a += vb1.b(3, str2);
        }
        Long l2 = this.f;
        if (l2 != null) {
            a += vb1.c(4, l2.longValue());
        }
        Float f2 = this.g;
        if (f2 != null) {
            f2.floatValue();
            a += vb1.c(5) + 4;
        }
        Double d2 = this.h;
        if (d2 == null) {
            return a;
        }
        d2.doubleValue();
        return a + vb1.c(6) + 8;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Long.valueOf(ub1.f());
            } else if (c2 == 18) {
                this.d = ub1.b();
            } else if (c2 == 26) {
                this.e = ub1.b();
            } else if (c2 == 32) {
                this.f = Long.valueOf(ub1.f());
            } else if (c2 == 45) {
                this.g = Float.valueOf(Float.intBitsToFloat(ub1.g()));
            } else if (c2 == 49) {
                this.h = Double.valueOf(Double.longBitsToDouble(ub1.h()));
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
