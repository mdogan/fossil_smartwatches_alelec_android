package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParameters;
import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParametersSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b50 {
    @DexIgnore
    public static /* final */ b50 a; // = new b50();

    /*
    static {
        new ConnectionParametersSet(24, 40, 0, 600);
        new ConnectionParametersSet(9, 12, 0, 72);
        new ConnectionParametersSet(80, 108, 4, 600);
    }
    */

    @DexIgnore
    public final boolean a(ConnectionParameters connectionParameters, ConnectionParametersSet connectionParametersSet) {
        wd4.b(connectionParameters, "connectionParameters");
        wd4.b(connectionParametersSet, "connectionParametersSet");
        int minimumInterval$blesdk_productionRelease = connectionParametersSet.getMinimumInterval$blesdk_productionRelease();
        int maximumInterval$blesdk_productionRelease = connectionParametersSet.getMaximumInterval$blesdk_productionRelease();
        int interval = connectionParameters.getInterval();
        return minimumInterval$blesdk_productionRelease <= interval && maximumInterval$blesdk_productionRelease >= interval && connectionParameters.getLatency() == connectionParametersSet.getLatency$blesdk_productionRelease() && connectionParameters.getTimeout() >= connectionParametersSet.getTimeout$blesdk_productionRelease();
    }
}
