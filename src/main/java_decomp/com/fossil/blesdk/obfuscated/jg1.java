package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jg1 implements Parcelable.Creator<ig1> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        fg1 fg1 = null;
        String str2 = null;
        long j = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 2) {
                str = SafeParcelReader.f(parcel, a);
            } else if (a2 == 3) {
                fg1 = SafeParcelReader.a(parcel, a, fg1.CREATOR);
            } else if (a2 == 4) {
                str2 = SafeParcelReader.f(parcel, a);
            } else if (a2 != 5) {
                SafeParcelReader.v(parcel, a);
            } else {
                j = SafeParcelReader.s(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new ig1(str, fg1, str2, j);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ig1[i];
    }
}
