package com.fossil.blesdk.obfuscated;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ic4 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        public /* final */ /* synthetic */ jd4[] e;

        @DexIgnore
        public a(jd4[] jd4Arr) {
            this.e = jd4Arr;
        }

        @DexIgnore
        public final int compare(T t, T t2) {
            return ic4.b(t, t2, this.e);
        }
    }

    @DexIgnore
    public static final <T> int b(T t, T t2, jd4<? super T, ? extends Comparable<?>>[] jd4Arr) {
        for (jd4<? super T, ? extends Comparable<?>> jd4 : jd4Arr) {
            int a2 = a((Comparable) jd4.invoke(t), (Comparable) jd4.invoke(t2));
            if (a2 != 0) {
                return a2;
            }
        }
        return 0;
    }

    @DexIgnore
    public static final <T extends Comparable<?>> int a(T t, T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return -1;
        }
        if (t2 == null) {
            return 1;
        }
        return t.compareTo(t2);
    }

    @DexIgnore
    public static final <T> Comparator<T> a(jd4<? super T, ? extends Comparable<?>>... jd4Arr) {
        wd4.b(jd4Arr, "selectors");
        if (jd4Arr.length > 0) {
            return new a(jd4Arr);
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
}
