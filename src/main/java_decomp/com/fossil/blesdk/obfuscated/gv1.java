package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class gv1 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends hv1 {
        @DexIgnore
        public /* final */ Charset a;

        @DexIgnore
        public a(Charset charset) {
            tt1.a(charset);
            this.a = charset;
        }

        @DexIgnore
        public Reader a() throws IOException {
            return new InputStreamReader(gv1.this.a(), this.a);
        }

        @DexIgnore
        public String toString() {
            return gv1.this.toString() + ".asCharSource(" + this.a + ")";
        }
    }

    @DexIgnore
    public hv1 a(Charset charset) {
        return new a(charset);
    }

    @DexIgnore
    public abstract InputStream a() throws IOException;
}
