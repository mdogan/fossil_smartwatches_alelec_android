package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.google.firebase.FirebaseApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class fw1 implements lw1 {
    @DexIgnore
    public static /* final */ lw1 a; // = new fw1();

    @DexIgnore
    public final Object a(kw1 kw1) {
        return ew1.a((FirebaseApp) kw1.a(FirebaseApp.class), (Context) kw1.a(Context.class), (cx1) kw1.a(cx1.class));
    }
}
