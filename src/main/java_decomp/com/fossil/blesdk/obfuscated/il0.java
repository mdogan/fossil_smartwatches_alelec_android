package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.bk0;
import com.fossil.blesdk.obfuscated.ie0;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class il0 implements ie0.a {
    @DexIgnore
    public /* final */ /* synthetic */ ie0 a;
    @DexIgnore
    public /* final */ /* synthetic */ yn1 b;
    @DexIgnore
    public /* final */ /* synthetic */ bk0.a c;
    @DexIgnore
    public /* final */ /* synthetic */ bk0.b d;

    @DexIgnore
    public il0(ie0 ie0, yn1 yn1, bk0.a aVar, bk0.b bVar) {
        this.a = ie0;
        this.b = yn1;
        this.c = aVar;
        this.d = bVar;
    }

    @DexIgnore
    public final void a(Status status) {
        if (status.L()) {
            this.b.a(this.c.a(this.a.a(0, TimeUnit.MILLISECONDS)));
            return;
        }
        this.b.a((Exception) this.d.a(status));
    }
}
