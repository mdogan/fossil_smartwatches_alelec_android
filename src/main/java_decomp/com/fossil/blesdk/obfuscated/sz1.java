package com.fossil.blesdk.obfuscated;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.LongSerializationPolicy;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Excluder;
import com.google.gson.internal.bind.TreeTypeAdapter;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sz1 {
    @DexIgnore
    public Excluder a; // = Excluder.k;
    @DexIgnore
    public LongSerializationPolicy b; // = LongSerializationPolicy.DEFAULT;
    @DexIgnore
    public rz1 c; // = FieldNamingPolicy.IDENTITY;
    @DexIgnore
    public /* final */ Map<Type, tz1<?>> d; // = new HashMap();
    @DexIgnore
    public /* final */ List<d02> e; // = new ArrayList();
    @DexIgnore
    public /* final */ List<d02> f; // = new ArrayList();
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public String h;
    @DexIgnore
    public int i; // = 2;
    @DexIgnore
    public int j; // = 2;
    @DexIgnore
    public boolean k; // = false;
    @DexIgnore
    public boolean l; // = false;
    @DexIgnore
    public boolean m; // = true;
    @DexIgnore
    public boolean n; // = false;
    @DexIgnore
    public boolean o; // = false;
    @DexIgnore
    public boolean p; // = false;

    @DexIgnore
    public sz1 a(int... iArr) {
        this.a = this.a.a(iArr);
        return this;
    }

    @DexIgnore
    public sz1 b(pz1 pz1) {
        this.a = this.a.a(pz1, true, false);
        return this;
    }

    @DexIgnore
    public sz1 a(FieldNamingPolicy fieldNamingPolicy) {
        this.c = fieldNamingPolicy;
        return this;
    }

    @DexIgnore
    public sz1 a(pz1 pz1) {
        this.a = this.a.a(pz1, false, true);
        return this;
    }

    @DexIgnore
    public sz1 a(Type type, Object obj) {
        boolean z = obj instanceof c02;
        j02.a(z || (obj instanceof wz1) || (obj instanceof tz1) || (obj instanceof TypeAdapter));
        if (obj instanceof tz1) {
            this.d.put(type, (tz1) obj);
        }
        if (z || (obj instanceof wz1)) {
            this.e.add(TreeTypeAdapter.a(TypeToken.get(type), obj));
        }
        if (obj instanceof TypeAdapter) {
            this.e.add(TypeAdapters.a(TypeToken.get(type), (TypeAdapter) obj));
        }
        return this;
    }

    @DexIgnore
    public Gson a() {
        ArrayList arrayList = r1;
        ArrayList arrayList2 = new ArrayList(this.e.size() + this.f.size() + 3);
        arrayList2.addAll(this.e);
        Collections.reverse(arrayList2);
        ArrayList arrayList3 = new ArrayList(this.f);
        Collections.reverse(arrayList3);
        arrayList2.addAll(arrayList3);
        a(this.h, this.i, this.j, arrayList2);
        return new Gson(this.a, this.c, this.d, this.g, this.k, this.o, this.m, this.n, this.p, this.l, this.b, this.h, this.i, this.j, this.e, this.f, arrayList);
    }

    @DexIgnore
    public final void a(String str, int i2, int i3, List<d02> list) {
        oz1 oz1;
        oz1 oz12;
        oz1 oz13;
        if (str != null && !"".equals(str.trim())) {
            oz1 oz14 = new oz1(Date.class, str);
            oz1 = new oz1(Timestamp.class, str);
            oz13 = new oz1(java.sql.Date.class, str);
            oz12 = oz14;
        } else if (i2 != 2 && i3 != 2) {
            oz12 = new oz1(Date.class, i2, i3);
            oz1 oz15 = new oz1(Timestamp.class, i2, i3);
            oz1 oz16 = new oz1(java.sql.Date.class, i2, i3);
            oz1 = oz15;
            oz13 = oz16;
        } else {
            return;
        }
        list.add(TypeAdapters.a(Date.class, oz12));
        list.add(TypeAdapters.a(Timestamp.class, oz1));
        list.add(TypeAdapters.a(java.sql.Date.class, oz13));
    }
}
