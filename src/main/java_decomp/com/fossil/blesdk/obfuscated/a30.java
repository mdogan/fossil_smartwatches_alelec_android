package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.watchapp.WatchAppId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class a30 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[WatchAppId.values().length];

    /*
    static {
        a[WatchAppId.DIAGNOSTICS.ordinal()] = 1;
        a[WatchAppId.WELLNESS.ordinal()] = 2;
        a[WatchAppId.WORKOUT.ordinal()] = 3;
        a[WatchAppId.MUSIC.ordinal()] = 4;
        a[WatchAppId.NOTIFICATIONS_PANEL.ordinal()] = 5;
        a[WatchAppId.EMPTY.ordinal()] = 6;
        a[WatchAppId.STOP_WATCH.ordinal()] = 7;
        a[WatchAppId.ASSISTANT.ordinal()] = 8;
        a[WatchAppId.TIMER.ordinal()] = 9;
        a[WatchAppId.WEATHER.ordinal()] = 10;
        a[WatchAppId.COMMUTE.ordinal()] = 11;
    }
    */
}
