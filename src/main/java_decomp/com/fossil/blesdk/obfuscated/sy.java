package com.fossil.blesdk.obfuscated;

import com.crashlytics.android.core.Report;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sy implements dz {
    @DexIgnore
    public /* final */ fz a;
    @DexIgnore
    public /* final */ pz b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[Report.Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /*
        static {
            a[Report.Type.JAVA.ordinal()] = 1;
            a[Report.Type.NATIVE.ordinal()] = 2;
        }
        */
    }

    @DexIgnore
    public sy(fz fzVar, pz pzVar) {
        this.a = fzVar;
        this.b = pzVar;
    }

    @DexIgnore
    public boolean a(cz czVar) {
        int i = a.a[czVar.b.getType().ordinal()];
        if (i == 1) {
            this.a.a(czVar);
            return true;
        } else if (i != 2) {
            return false;
        } else {
            this.b.a(czVar);
            return true;
        }
    }
}
