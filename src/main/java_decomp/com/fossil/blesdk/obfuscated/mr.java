package com.fossil.blesdk.obfuscated;

import android.net.Uri;
import android.text.TextUtils;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mr implements ko {
    @DexIgnore
    public /* final */ nr b;
    @DexIgnore
    public /* final */ URL c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public URL f;
    @DexIgnore
    public volatile byte[] g;
    @DexIgnore
    public int h;

    @DexIgnore
    public mr(URL url) {
        this(url, nr.a);
    }

    @DexIgnore
    public String a() {
        String str = this.d;
        if (str != null) {
            return str;
        }
        URL url = this.c;
        uw.a(url);
        return url.toString();
    }

    @DexIgnore
    public final byte[] b() {
        if (this.g == null) {
            this.g = a().getBytes(ko.a);
        }
        return this.g;
    }

    @DexIgnore
    public Map<String, String> c() {
        return this.b.a();
    }

    @DexIgnore
    public final String d() {
        if (TextUtils.isEmpty(this.e)) {
            String str = this.d;
            if (TextUtils.isEmpty(str)) {
                URL url = this.c;
                uw.a(url);
                str = url.toString();
            }
            this.e = Uri.encode(str, "@#&=*+-_.,:!?()/~'%;$");
        }
        return this.e;
    }

    @DexIgnore
    public final URL e() throws MalformedURLException {
        if (this.f == null) {
            this.f = new URL(d());
        }
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof mr)) {
            return false;
        }
        mr mrVar = (mr) obj;
        if (!a().equals(mrVar.a()) || !this.b.equals(mrVar.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public URL f() throws MalformedURLException {
        return e();
    }

    @DexIgnore
    public int hashCode() {
        if (this.h == 0) {
            this.h = a().hashCode();
            this.h = (this.h * 31) + this.b.hashCode();
        }
        return this.h;
    }

    @DexIgnore
    public String toString() {
        return a();
    }

    @DexIgnore
    public mr(String str) {
        this(str, nr.a);
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        messageDigest.update(b());
    }

    @DexIgnore
    public mr(URL url, nr nrVar) {
        uw.a(url);
        this.c = url;
        this.d = null;
        uw.a(nrVar);
        this.b = nrVar;
    }

    @DexIgnore
    public mr(String str, nr nrVar) {
        this.c = null;
        uw.a(str);
        this.d = str;
        uw.a(nrVar);
        this.b = nrVar;
    }
}
