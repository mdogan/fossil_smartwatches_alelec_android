package com.fossil.blesdk.obfuscated;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface dz0 {
    @DexIgnore
    ExecutorService a(ThreadFactory threadFactory, int i);

    @DexIgnore
    ScheduledExecutorService a(int i, ThreadFactory threadFactory, int i2);
}
