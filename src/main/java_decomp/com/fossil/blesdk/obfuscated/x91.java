package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zztv;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface x91 extends z91 {
    @DexIgnore
    void a(zztv zztv) throws IOException;

    @DexIgnore
    y91 c();

    @DexIgnore
    zzte d();

    @DexIgnore
    int e();

    @DexIgnore
    y91 f();
}
