package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.StreetViewPanoramaOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface df1 extends IInterface {
    @DexIgnore
    me1 a(tn0 tn0, GoogleMapOptions googleMapOptions) throws RemoteException;

    @DexIgnore
    oe1 a(tn0 tn0, StreetViewPanoramaOptions streetViewPanoramaOptions) throws RemoteException;

    @DexIgnore
    void a(tn0 tn0, int i) throws RemoteException;

    @DexIgnore
    le1 c(tn0 tn0) throws RemoteException;

    @DexIgnore
    je1 zze() throws RemoteException;

    @DexIgnore
    e51 zzf() throws RemoteException;
}
