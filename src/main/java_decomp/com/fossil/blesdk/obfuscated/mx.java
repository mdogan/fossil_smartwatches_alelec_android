package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mx {
    @DexIgnore
    public /* final */ ScheduledExecutorService a;
    @DexIgnore
    public /* final */ List<b> b; // = new ArrayList();
    @DexIgnore
    public volatile boolean c; // = true;
    @DexIgnore
    public /* final */ AtomicReference<ScheduledFuture<?>> d; // = new AtomicReference<>();
    @DexIgnore
    public boolean e; // = true;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            mx.this.d.set((Object) null);
            mx.this.a();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public mx(ScheduledExecutorService scheduledExecutorService) {
        this.a = scheduledExecutorService;
    }

    @DexIgnore
    public void b() {
        if (this.c && !this.e) {
            this.e = true;
            try {
                this.d.compareAndSet((Object) null, this.a.schedule(new a(), 5000, TimeUnit.MILLISECONDS));
            } catch (RejectedExecutionException e2) {
                r44.g().b("Answers", "Failed to schedule background detector", e2);
            }
        }
    }

    @DexIgnore
    public void c() {
        this.e = false;
        ScheduledFuture andSet = this.d.getAndSet((Object) null);
        if (andSet != null) {
            andSet.cancel(false);
        }
    }

    @DexIgnore
    public void a(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public final void a() {
        for (b a2 : this.b) {
            a2.a();
        }
    }

    @DexIgnore
    public void a(b bVar) {
        this.b.add(bVar);
    }
}
