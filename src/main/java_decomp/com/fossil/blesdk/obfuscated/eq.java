package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class eq {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper(), new a());

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Handler.Callback {
        @DexIgnore
        public boolean handleMessage(Message message) {
            if (message.what != 1) {
                return false;
            }
            ((bq) message.obj).a();
            return true;
        }
    }

    @DexIgnore
    public synchronized void a(bq<?> bqVar) {
        if (this.a) {
            this.b.obtainMessage(1, bqVar).sendToTarget();
        } else {
            this.a = true;
            bqVar.a();
            this.a = false;
        }
    }
}
