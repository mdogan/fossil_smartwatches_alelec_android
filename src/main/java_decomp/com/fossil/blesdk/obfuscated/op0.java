package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.fitness.data.DataPoint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class op0 implements Parcelable.Creator<DataPoint> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v4, types: [java.lang.Object[]] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        ap0 ap0 = null;
        gp0[] gp0Arr = null;
        ap0 ap02 = null;
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 1:
                    ap0 = SafeParcelReader.a(parcel2, a, ap0.CREATOR);
                    break;
                case 3:
                    j = SafeParcelReader.s(parcel2, a);
                    break;
                case 4:
                    j2 = SafeParcelReader.s(parcel2, a);
                    break;
                case 5:
                    gp0Arr = SafeParcelReader.b(parcel2, a, gp0.CREATOR);
                    break;
                case 6:
                    ap02 = SafeParcelReader.a(parcel2, a, ap0.CREATOR);
                    break;
                case 7:
                    j3 = SafeParcelReader.s(parcel2, a);
                    break;
                case 8:
                    j4 = SafeParcelReader.s(parcel2, a);
                    break;
                default:
                    SafeParcelReader.v(parcel2, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel2, b);
        return new DataPoint(ap0, j, j2, gp0Arr, ap02, j3, j4);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new DataPoint[i];
    }
}
