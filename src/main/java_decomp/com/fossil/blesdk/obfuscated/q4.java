package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.SolverVariable;
import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class q4 {
    @DexIgnore
    public static int p; // = 1000;
    @DexIgnore
    public static r4 q;
    @DexIgnore
    public int a; // = 0;
    @DexIgnore
    public HashMap<String, SolverVariable> b; // = null;
    @DexIgnore
    public a c;
    @DexIgnore
    public int d; // = 32;
    @DexIgnore
    public int e;
    @DexIgnore
    public n4[] f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean[] h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public /* final */ o4 l;
    @DexIgnore
    public SolverVariable[] m;
    @DexIgnore
    public int n;
    @DexIgnore
    public /* final */ a o;

    @DexIgnore
    public interface a {
        @DexIgnore
        SolverVariable a(q4 q4Var, boolean[] zArr);

        @DexIgnore
        void a(SolverVariable solverVariable);

        @DexIgnore
        void a(a aVar);

        @DexIgnore
        void clear();

        @DexIgnore
        SolverVariable getKey();
    }

    @DexIgnore
    public q4() {
        int i2 = this.d;
        this.e = i2;
        this.f = null;
        this.g = false;
        this.h = new boolean[i2];
        this.i = 1;
        this.j = 0;
        this.k = i2;
        this.m = new SolverVariable[p];
        this.n = 0;
        n4[] n4VarArr = new n4[i2];
        this.f = new n4[i2];
        h();
        this.l = new o4();
        this.c = new p4(this.l);
        this.o = new n4(this.l);
    }

    @DexIgnore
    public static r4 j() {
        return q;
    }

    @DexIgnore
    public SolverVariable a(Object obj) {
        SolverVariable solverVariable = null;
        if (obj == null) {
            return null;
        }
        if (this.i + 1 >= this.e) {
            f();
        }
        if (obj instanceof ConstraintAnchor) {
            ConstraintAnchor constraintAnchor = (ConstraintAnchor) obj;
            solverVariable = constraintAnchor.e();
            if (solverVariable == null) {
                constraintAnchor.a(this.l);
                solverVariable = constraintAnchor.e();
            }
            int i2 = solverVariable.b;
            if (i2 == -1 || i2 > this.a || this.l.c[i2] == null) {
                if (solverVariable.b != -1) {
                    solverVariable.a();
                }
                this.a++;
                this.i++;
                int i3 = this.a;
                solverVariable.b = i3;
                solverVariable.g = SolverVariable.Type.UNRESTRICTED;
                this.l.c[i3] = solverVariable;
            }
        }
        return solverVariable;
    }

    @DexIgnore
    public SolverVariable b() {
        r4 r4Var = q;
        if (r4Var != null) {
            r4Var.n++;
        }
        if (this.i + 1 >= this.e) {
            f();
        }
        SolverVariable a2 = a(SolverVariable.Type.SLACK, (String) null);
        this.a++;
        this.i++;
        int i2 = this.a;
        a2.b = i2;
        this.l.c[i2] = a2;
        return a2;
    }

    @DexIgnore
    public n4 c() {
        n4 a2 = this.l.a.a();
        if (a2 == null) {
            a2 = new n4(this.l);
        } else {
            a2.d();
        }
        SolverVariable.b();
        return a2;
    }

    @DexIgnore
    public SolverVariable d() {
        r4 r4Var = q;
        if (r4Var != null) {
            r4Var.m++;
        }
        if (this.i + 1 >= this.e) {
            f();
        }
        SolverVariable a2 = a(SolverVariable.Type.SLACK, (String) null);
        this.a++;
        this.i++;
        int i2 = this.a;
        a2.b = i2;
        this.l.c[i2] = a2;
        return a2;
    }

    @DexIgnore
    public o4 e() {
        return this.l;
    }

    @DexIgnore
    public final void f() {
        this.d *= 2;
        this.f = (n4[]) Arrays.copyOf(this.f, this.d);
        o4 o4Var = this.l;
        o4Var.c = (SolverVariable[]) Arrays.copyOf(o4Var.c, this.d);
        int i2 = this.d;
        this.h = new boolean[i2];
        this.e = i2;
        this.k = i2;
        r4 r4Var = q;
        if (r4Var != null) {
            r4Var.d++;
            r4Var.o = Math.max(r4Var.o, (long) i2);
            r4 r4Var2 = q;
            r4Var2.A = r4Var2.o;
        }
    }

    @DexIgnore
    public void g() throws Exception {
        r4 r4Var = q;
        if (r4Var != null) {
            r4Var.e++;
        }
        if (this.g) {
            r4 r4Var2 = q;
            if (r4Var2 != null) {
                r4Var2.q++;
            }
            boolean z = false;
            int i2 = 0;
            while (true) {
                if (i2 >= this.j) {
                    z = true;
                    break;
                } else if (!this.f[i2].e) {
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                b(this.c);
                return;
            }
            r4 r4Var3 = q;
            if (r4Var3 != null) {
                r4Var3.p++;
            }
            a();
            return;
        }
        b(this.c);
    }

    @DexIgnore
    public final void h() {
        int i2 = 0;
        while (true) {
            n4[] n4VarArr = this.f;
            if (i2 < n4VarArr.length) {
                n4 n4Var = n4VarArr[i2];
                if (n4Var != null) {
                    this.l.a.a(n4Var);
                }
                this.f[i2] = null;
                i2++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public void i() {
        o4 o4Var;
        int i2 = 0;
        while (true) {
            o4Var = this.l;
            SolverVariable[] solverVariableArr = o4Var.c;
            if (i2 >= solverVariableArr.length) {
                break;
            }
            SolverVariable solverVariable = solverVariableArr[i2];
            if (solverVariable != null) {
                solverVariable.a();
            }
            i2++;
        }
        o4Var.b.a(this.m, this.n);
        this.n = 0;
        Arrays.fill(this.l.c, (Object) null);
        HashMap<String, SolverVariable> hashMap = this.b;
        if (hashMap != null) {
            hashMap.clear();
        }
        this.a = 0;
        this.c.clear();
        this.i = 1;
        for (int i3 = 0; i3 < this.j; i3++) {
            this.f[i3].c = false;
        }
        h();
        this.j = 0;
    }

    @DexIgnore
    public final void c(n4 n4Var) {
        n4[] n4VarArr = this.f;
        int i2 = this.j;
        if (n4VarArr[i2] != null) {
            this.l.a.a(n4VarArr[i2]);
        }
        n4[] n4VarArr2 = this.f;
        int i3 = this.j;
        n4VarArr2[i3] = n4Var;
        SolverVariable solverVariable = n4Var.a;
        solverVariable.c = i3;
        this.j = i3 + 1;
        solverVariable.c(n4Var);
    }

    @DexIgnore
    public final void b(n4 n4Var) {
        n4Var.a(this, 0);
    }

    @DexIgnore
    public final void d(n4 n4Var) {
        if (this.j > 0) {
            n4Var.d.a(n4Var, this.f);
            if (n4Var.d.a == 0) {
                n4Var.e = true;
            }
        }
    }

    @DexIgnore
    public int b(Object obj) {
        SolverVariable e2 = ((ConstraintAnchor) obj).e();
        if (e2 != null) {
            return (int) (e2.e + 0.5f);
        }
        return 0;
    }

    @DexIgnore
    public void c(SolverVariable solverVariable, SolverVariable solverVariable2, int i2, int i3) {
        n4 c2 = c();
        SolverVariable d2 = d();
        d2.d = 0;
        c2.b(solverVariable, solverVariable2, d2, i2);
        if (i3 != 6) {
            a(c2, (int) (c2.d.b(d2) * -1.0f), i3);
        }
        a(c2);
    }

    @DexIgnore
    public void b(a aVar) throws Exception {
        r4 r4Var = q;
        if (r4Var != null) {
            r4Var.s++;
            r4Var.t = Math.max(r4Var.t, (long) this.i);
            r4 r4Var2 = q;
            r4Var2.u = Math.max(r4Var2.u, (long) this.j);
        }
        d((n4) aVar);
        a(aVar);
        a(aVar, false);
        a();
    }

    @DexIgnore
    public void a(n4 n4Var, int i2, int i3) {
        n4Var.a(a(i3, (String) null), i2);
    }

    @DexIgnore
    public SolverVariable a(int i2, String str) {
        r4 r4Var = q;
        if (r4Var != null) {
            r4Var.l++;
        }
        if (this.i + 1 >= this.e) {
            f();
        }
        SolverVariable a2 = a(SolverVariable.Type.ERROR, str);
        this.a++;
        this.i++;
        int i3 = this.a;
        a2.b = i3;
        a2.d = i2;
        this.l.c[i3] = a2;
        this.c.a(a2);
        return a2;
    }

    @DexIgnore
    public void b(SolverVariable solverVariable, SolverVariable solverVariable2, int i2, int i3) {
        n4 c2 = c();
        SolverVariable d2 = d();
        d2.d = 0;
        c2.a(solverVariable, solverVariable2, d2, i2);
        if (i3 != 6) {
            a(c2, (int) (c2.d.b(d2) * -1.0f), i3);
        }
        a(c2);
    }

    @DexIgnore
    public final SolverVariable a(SolverVariable.Type type, String str) {
        SolverVariable a2 = this.l.b.a();
        if (a2 == null) {
            a2 = new SolverVariable(type, str);
            a2.a(type, str);
        } else {
            a2.a();
            a2.a(type, str);
        }
        int i2 = this.n;
        int i3 = p;
        if (i2 >= i3) {
            p = i3 * 2;
            this.m = (SolverVariable[]) Arrays.copyOf(this.m, p);
        }
        SolverVariable[] solverVariableArr = this.m;
        int i4 = this.n;
        this.n = i4 + 1;
        solverVariableArr[i4] = a2;
        return a2;
    }

    @DexIgnore
    public void b(SolverVariable solverVariable, SolverVariable solverVariable2, boolean z) {
        n4 c2 = c();
        SolverVariable d2 = d();
        d2.d = 0;
        c2.b(solverVariable, solverVariable2, d2, 0);
        if (z) {
            a(c2, (int) (c2.d.b(d2) * -1.0f), 1);
        }
        a(c2);
    }

    @DexIgnore
    public void a(n4 n4Var) {
        if (n4Var != null) {
            r4 r4Var = q;
            if (r4Var != null) {
                r4Var.f++;
                if (n4Var.e) {
                    r4Var.g++;
                }
            }
            if (this.j + 1 >= this.k || this.i + 1 >= this.e) {
                f();
            }
            boolean z = false;
            if (!n4Var.e) {
                d(n4Var);
                if (!n4Var.c()) {
                    n4Var.a();
                    if (n4Var.a(this)) {
                        SolverVariable b2 = b();
                        n4Var.a = b2;
                        c(n4Var);
                        this.o.a((a) n4Var);
                        a(this.o, true);
                        if (b2.c == -1) {
                            if (n4Var.a == b2) {
                                SolverVariable c2 = n4Var.c(b2);
                                if (c2 != null) {
                                    r4 r4Var2 = q;
                                    if (r4Var2 != null) {
                                        r4Var2.j++;
                                    }
                                    n4Var.d(c2);
                                }
                            }
                            if (!n4Var.e) {
                                n4Var.a.c(n4Var);
                            }
                            this.j--;
                        }
                        z = true;
                    }
                    if (!n4Var.b()) {
                        return;
                    }
                } else {
                    return;
                }
            }
            if (!z) {
                c(n4Var);
            }
        }
    }

    @DexIgnore
    public final int a(a aVar, boolean z) {
        r4 r4Var = q;
        if (r4Var != null) {
            r4Var.h++;
        }
        for (int i2 = 0; i2 < this.i; i2++) {
            this.h[i2] = false;
        }
        boolean z2 = false;
        int i3 = 0;
        while (!z2) {
            r4 r4Var2 = q;
            if (r4Var2 != null) {
                r4Var2.i++;
            }
            i3++;
            if (i3 >= this.i * 2) {
                return i3;
            }
            if (aVar.getKey() != null) {
                this.h[aVar.getKey().b] = true;
            }
            SolverVariable a2 = aVar.a(this, this.h);
            if (a2 != null) {
                boolean[] zArr = this.h;
                int i4 = a2.b;
                if (zArr[i4]) {
                    return i3;
                }
                zArr[i4] = true;
            }
            if (a2 != null) {
                int i5 = -1;
                float f2 = Float.MAX_VALUE;
                for (int i6 = 0; i6 < this.j; i6++) {
                    n4 n4Var = this.f[i6];
                    if (n4Var.a.g != SolverVariable.Type.UNRESTRICTED && !n4Var.e && n4Var.b(a2)) {
                        float b2 = n4Var.d.b(a2);
                        if (b2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            float f3 = (-n4Var.b) / b2;
                            if (f3 < f2) {
                                i5 = i6;
                                f2 = f3;
                            }
                        }
                    }
                }
                if (i5 > -1) {
                    n4 n4Var2 = this.f[i5];
                    n4Var2.a.c = -1;
                    r4 r4Var3 = q;
                    if (r4Var3 != null) {
                        r4Var3.j++;
                    }
                    n4Var2.d(a2);
                    SolverVariable solverVariable = n4Var2.a;
                    solverVariable.c = i5;
                    solverVariable.c(n4Var2);
                }
            }
            z2 = true;
        }
        return i3;
    }

    @DexIgnore
    public final int a(a aVar) throws Exception {
        float f2;
        boolean z;
        int i2 = 0;
        while (true) {
            int i3 = this.j;
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (i2 >= i3) {
                z = false;
                break;
            }
            n4[] n4VarArr = this.f;
            if (n4VarArr[i2].a.g != SolverVariable.Type.UNRESTRICTED && n4VarArr[i2].b < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z = true;
                break;
            }
            i2++;
        }
        if (!z) {
            return 0;
        }
        boolean z2 = false;
        int i4 = 0;
        while (!z2) {
            r4 r4Var = q;
            if (r4Var != null) {
                r4Var.k++;
            }
            i4++;
            int i5 = 0;
            int i6 = -1;
            int i7 = -1;
            float f3 = Float.MAX_VALUE;
            int i8 = 0;
            while (i5 < this.j) {
                n4 n4Var = this.f[i5];
                if (n4Var.a.g != SolverVariable.Type.UNRESTRICTED && !n4Var.e && n4Var.b < f2) {
                    int i9 = 1;
                    while (i9 < this.i) {
                        SolverVariable solverVariable = this.l.c[i9];
                        float b2 = n4Var.d.b(solverVariable);
                        if (b2 > f2) {
                            int i10 = i8;
                            float f4 = f3;
                            int i11 = i7;
                            int i12 = i6;
                            for (int i13 = 0; i13 < 7; i13++) {
                                float f5 = solverVariable.f[i13] / b2;
                                if ((f5 < f4 && i13 == i10) || i13 > i10) {
                                    i11 = i9;
                                    i12 = i5;
                                    f4 = f5;
                                    i10 = i13;
                                }
                            }
                            i6 = i12;
                            i7 = i11;
                            f3 = f4;
                            i8 = i10;
                        }
                        i9++;
                        f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    }
                }
                i5++;
                f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
            if (i6 != -1) {
                n4 n4Var2 = this.f[i6];
                n4Var2.a.c = -1;
                r4 r4Var2 = q;
                if (r4Var2 != null) {
                    r4Var2.j++;
                }
                n4Var2.d(this.l.c[i7]);
                SolverVariable solverVariable2 = n4Var2.a;
                solverVariable2.c = i6;
                solverVariable2.c(n4Var2);
            } else {
                z2 = true;
            }
            if (i4 > this.i / 2) {
                z2 = true;
            }
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        return i4;
    }

    @DexIgnore
    public final void a() {
        for (int i2 = 0; i2 < this.j; i2++) {
            n4 n4Var = this.f[i2];
            n4Var.a.e = n4Var.b;
        }
    }

    @DexIgnore
    public void a(SolverVariable solverVariable, SolverVariable solverVariable2, boolean z) {
        n4 c2 = c();
        SolverVariable d2 = d();
        d2.d = 0;
        c2.a(solverVariable, solverVariable2, d2, 0);
        if (z) {
            a(c2, (int) (c2.d.b(d2) * -1.0f), 1);
        }
        a(c2);
    }

    @DexIgnore
    public void a(SolverVariable solverVariable, SolverVariable solverVariable2, int i2, float f2, SolverVariable solverVariable3, SolverVariable solverVariable4, int i3, int i4) {
        int i5 = i4;
        n4 c2 = c();
        c2.a(solverVariable, solverVariable2, i2, f2, solverVariable3, solverVariable4, i3);
        if (i5 != 6) {
            c2.a(this, i5);
        }
        a(c2);
    }

    @DexIgnore
    public void a(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, SolverVariable solverVariable4, float f2, int i2) {
        n4 c2 = c();
        c2.a(solverVariable, solverVariable2, solverVariable3, solverVariable4, f2);
        if (i2 != 6) {
            c2.a(this, i2);
        }
        a(c2);
    }

    @DexIgnore
    public n4 a(SolverVariable solverVariable, SolverVariable solverVariable2, int i2, int i3) {
        n4 c2 = c();
        c2.a(solverVariable, solverVariable2, i2);
        if (i3 != 6) {
            c2.a(this, i3);
        }
        a(c2);
        return c2;
    }

    @DexIgnore
    public void a(SolverVariable solverVariable, int i2) {
        int i3 = solverVariable.c;
        if (i3 != -1) {
            n4 n4Var = this.f[i3];
            if (n4Var.e) {
                n4Var.b = (float) i2;
            } else if (n4Var.d.a == 0) {
                n4Var.e = true;
                n4Var.b = (float) i2;
            } else {
                n4 c2 = c();
                c2.c(solverVariable, i2);
                a(c2);
            }
        } else {
            n4 c3 = c();
            c3.b(solverVariable, i2);
            a(c3);
        }
    }

    @DexIgnore
    public static n4 a(q4 q4Var, SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, float f2, boolean z) {
        n4 c2 = q4Var.c();
        if (z) {
            q4Var.b(c2);
        }
        c2.a(solverVariable, solverVariable2, solverVariable3, f2);
        return c2;
    }

    @DexIgnore
    public void a(ConstraintWidget constraintWidget, ConstraintWidget constraintWidget2, float f2, int i2) {
        ConstraintWidget constraintWidget3 = constraintWidget;
        ConstraintWidget constraintWidget4 = constraintWidget2;
        SolverVariable a2 = a((Object) constraintWidget3.a(ConstraintAnchor.Type.LEFT));
        SolverVariable a3 = a((Object) constraintWidget3.a(ConstraintAnchor.Type.TOP));
        SolverVariable a4 = a((Object) constraintWidget3.a(ConstraintAnchor.Type.RIGHT));
        SolverVariable a5 = a((Object) constraintWidget3.a(ConstraintAnchor.Type.BOTTOM));
        SolverVariable a6 = a((Object) constraintWidget4.a(ConstraintAnchor.Type.LEFT));
        SolverVariable a7 = a((Object) constraintWidget4.a(ConstraintAnchor.Type.TOP));
        SolverVariable a8 = a((Object) constraintWidget4.a(ConstraintAnchor.Type.RIGHT));
        SolverVariable a9 = a((Object) constraintWidget4.a(ConstraintAnchor.Type.BOTTOM));
        n4 c2 = c();
        double d2 = (double) f2;
        SolverVariable solverVariable = a4;
        double d3 = (double) i2;
        c2.b(a3, a5, a7, a9, (float) (Math.sin(d2) * d3));
        a(c2);
        n4 c3 = c();
        c3.b(a2, solverVariable, a6, a8, (float) (Math.cos(d2) * d3));
        a(c3);
    }
}
