package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sp3 extends bs2 implements mq3, xs3.g {
    @DexIgnore
    public static /* final */ a r; // = new a((rd4) null);
    @DexIgnore
    public lq3 k;
    @DexIgnore
    public ur3<rb2> l;
    @DexIgnore
    public ce1 m;
    @DexIgnore
    public View n;
    @DexIgnore
    public Bitmap o;
    @DexIgnore
    public yn p;
    @DexIgnore
    public HashMap q;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final sp3 a() {
            return new sp3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sp3 e;

        @DexIgnore
        public b(sp3 sp3) {
            this.e = sp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ee1 {
        @DexIgnore
        public /* final */ /* synthetic */ sp3 a;

        @DexIgnore
        public c(sp3 sp3) {
            this.a = sp3;
        }

        @DexIgnore
        public final void a(ce1 ce1) {
            this.a.m = ce1;
            wd4.a((Object) ce1, "googleMap");
            ie1 b = ce1.b();
            wd4.a((Object) b, "googleMap.uiSettings");
            b.a(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ sp3 a;
        @DexIgnore
        public /* final */ /* synthetic */ rb2 b;

        @DexIgnore
        public d(sp3 sp3, rb2 rb2) {
            this.a = sp3;
            this.b = rb2;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            us3.a((View) this.b.x);
            ConstraintLayout constraintLayout = this.b.q;
            wd4.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(z ? 0 : 4);
            sp3.a(this.a).a(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ rb2 a;
        @DexIgnore
        public /* final */ /* synthetic */ sp3 b;

        @DexIgnore
        public e(rb2 rb2, sp3 sp3, WatchSettingViewModel.c cVar) {
            this.a = rb2;
            this.b = sp3;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            wd4.b(str, "serial");
            wd4.b(str2, "filePath");
            sp3.b(this.b).a(str2).a(this.a.w);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ sp3 b;

        @DexIgnore
        public f(View view, String str, sp3 sp3, WatchSettingViewModel.c cVar) {
            this.a = view;
            this.b = sp3;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            wd4.b(str, "serial");
            wd4.b(str2, "filePath");
            if (this.b.isActive()) {
                View findViewById = this.a.findViewById(R.id.image);
                if (findViewById != null) {
                    ((ImageView) findViewById).setImageBitmap(bl2.a(str2));
                    int dimensionPixelSize = PortfolioApp.W.c().getResources().getDimensionPixelSize(R.dimen.dp96);
                    this.a.measure(View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824), View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824));
                    View view = this.a;
                    view.layout(0, 0, view.getMeasuredWidth(), this.a.getMeasuredHeight());
                    this.b.o = bl2.a(this.a);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ lq3 a(sp3 sp3) {
        lq3 lq3 = sp3.k;
        if (lq3 != null) {
            return lq3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ yn b(sp3 sp3) {
        yn ynVar = sp3.p;
        if (ynVar != null) {
            return ynVar;
        }
        wd4.d("mRequestManager");
        throw null;
    }

    @DexIgnore
    public void K0() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showLocationNotFound");
        rb2 T0 = T0();
        if (T0 != null) {
            SwitchCompat switchCompat = T0.x;
            wd4.a((Object) switchCompat, "binding.swLocate");
            switchCompat.setEnabled(false);
            T0.u.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.disabled));
            ConstraintLayout constraintLayout = T0.q;
            wd4.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            FlexibleTextView flexibleTextView = T0.r;
            wd4.a((Object) flexibleTextView, "binding.ftvError");
            flexibleTextView.setVisibility(0);
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final rb2 T0() {
        ur3<rb2> ur3 = this.l;
        if (ur3 != null) {
            return ur3.a();
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b0() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showLocationPermissionError");
        rb2 T0 = T0();
        if (T0 != null) {
            SwitchCompat switchCompat = T0.x;
            wd4.a((Object) switchCompat, "binding.swLocate");
            switchCompat.setChecked(false);
            ConstraintLayout constraintLayout = T0.q;
            wd4.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
        }
    }

    @DexIgnore
    public void k(int i) {
        ur3<rb2> ur3 = this.l;
        if (ur3 != null) {
            rb2 a2 = ur3.a();
            if (a2 != null) {
                float b2 = DeviceHelper.o.b(i);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FindDeviceFragment", "showProximity - rssi: " + i + ", distanceInFt: " + b2);
                if (b2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && b2 <= 5.0f) {
                    FlexibleTextView flexibleTextView = a2.z;
                    wd4.a((Object) flexibleTextView, "binding.tvLocationStatus");
                    flexibleTextView.setText(tm2.a(getContext(), (int) R.string.Profile_MyWatchFindDevice_FindDevice_Label__VeryClose));
                    FlexibleTextView flexibleTextView2 = a2.z;
                    wd4.a((Object) flexibleTextView2, "binding.tvLocationStatus");
                    flexibleTextView2.setEnabled(true);
                } else if (b2 >= 5.0f && b2 <= 10.0f) {
                    FlexibleTextView flexibleTextView3 = a2.z;
                    wd4.a((Object) flexibleTextView3, "binding.tvLocationStatus");
                    flexibleTextView3.setText(tm2.a(getContext(), (int) R.string.Profile_MyWatchFindDevice_FindDevice_Label__Close));
                    FlexibleTextView flexibleTextView4 = a2.z;
                    wd4.a((Object) flexibleTextView4, "binding.tvLocationStatus");
                    flexibleTextView4.setEnabled(true);
                } else if (b2 < 10.0f || b2 > 30.0f) {
                    FlexibleTextView flexibleTextView5 = a2.z;
                    wd4.a((Object) flexibleTextView5, "binding.tvLocationStatus");
                    flexibleTextView5.setText(tm2.a(getContext(), (int) R.string.Profile_MyWatchFindDevice_DeviceisOutofRange_Label__OutOfRange));
                    FlexibleTextView flexibleTextView6 = a2.z;
                    wd4.a((Object) flexibleTextView6, "binding.tvLocationStatus");
                    flexibleTextView6.setEnabled(false);
                } else {
                    FlexibleTextView flexibleTextView7 = a2.z;
                    wd4.a((Object) flexibleTextView7, "binding.tvLocationStatus");
                    flexibleTextView7.setText(tm2.a(getContext(), (int) R.string.Profile_MyWatchFindDevice_FindDevice_Label__Nearby));
                    FlexibleTextView flexibleTextView8 = a2.z;
                    wd4.a((Object) flexibleTextView8, "binding.tvLocationStatus");
                    flexibleTextView8.setEnabled(true);
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        yn a2 = sn.a((Fragment) this);
        wd4.a((Object) a2, "Glide.with(this)");
        this.p = a2;
    }

    @DexIgnore
    @SuppressLint({"InflateParams"})
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        rb2 rb2 = (rb2) ra.a(layoutInflater, R.layout.fragment_find_device, viewGroup, false, O0());
        this.n = layoutInflater.inflate(R.layout.marker, (ViewGroup) null);
        View view = this.n;
        if (view != null) {
            int dimensionPixelSize = PortfolioApp.W.c().getResources().getDimensionPixelSize(R.dimen.dp96);
            view.measure(View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824), View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824));
            view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
            this.o = bl2.a(view);
        }
        rb2.v.setOnClickListener(new b(this));
        Fragment a2 = getChildFragmentManager().a((int) R.id.f_map);
        if (a2 != null) {
            ((SupportMapFragment) a2).a(new c(this));
            rb2.x.setOnCheckedChangeListener(new d(this, rb2));
            this.l = new ur3<>(this, rb2);
            wd4.a((Object) rb2, "binding");
            return rb2.d();
        }
        throw new TypeCastException("null cannot be cast to non-null type com.google.android.gms.maps.SupportMapFragment");
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "onPause");
        super.onPause();
        lq3 lq3 = this.k;
        if (lq3 != null) {
            lq3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "onResume");
        super.onResume();
        lq3 lq3 = this.k;
        if (lq3 != null) {
            lq3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void y(String str) {
        wd4.b(str, "address");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showAddress: address = " + str);
        ur3<rb2> ur3 = this.l;
        if (ur3 != null) {
            rb2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(boolean z, boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showLocationEnable: enable = " + z + ", needWarning = " + z2);
        ur3<rb2> ur3 = this.l;
        if (ur3 != null) {
            rb2 a2 = ur3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.x;
                wd4.a((Object) switchCompat, "binding.swLocate");
                switchCompat.setChecked(z);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(WatchSettingViewModel.c cVar) {
        wd4.b(cVar, "watchSetting");
        if (isActive()) {
            ur3<rb2> ur3 = this.l;
            if (ur3 != null) {
                rb2 a2 = ur3.a();
                if (a2 != null) {
                    String deviceId = cVar.a().getDeviceId();
                    FlexibleTextView flexibleTextView = a2.y;
                    wd4.a((Object) flexibleTextView, "it.tvDeviceName");
                    flexibleTextView.setText(cVar.b());
                    a2.y.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                    FlexibleTextView flexibleTextView2 = a2.r;
                    wd4.a((Object) flexibleTextView2, "it.ftvError");
                    be4 be4 = be4.a;
                    String a3 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchFindDevice_NoConnectedLocationInfo_Text__YourWatchNameHasntBeenConnected);
                    wd4.a((Object) a3, "LanguageHelper.getString\u2026chNameHasntBeenConnected)");
                    Object[] objArr = {cVar.b()};
                    String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    wd4.a((Object) format, "java.lang.String.format(format, *args)");
                    flexibleTextView2.setText(format);
                    boolean d2 = cVar.d();
                    if (d2) {
                        if (cVar.e()) {
                            a2.y.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.success));
                            FlexibleTextView flexibleTextView3 = a2.y;
                            wd4.a((Object) flexibleTextView3, "it.tvDeviceName");
                            flexibleTextView3.setAlpha(1.0f);
                            a2.y.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, k6.c(PortfolioApp.W.c(), R.drawable.ic_device_status), (Drawable) null);
                        } else {
                            a2.y.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.primaryText));
                            FlexibleTextView flexibleTextView4 = a2.y;
                            wd4.a((Object) flexibleTextView4, "it.tvDeviceName");
                            flexibleTextView4.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView5 = a2.z;
                            wd4.a((Object) flexibleTextView5, "it.tvLocationStatus");
                            flexibleTextView5.setText(tm2.a(getContext(), (int) R.string.Profile_MyWatchFindDevice_DeviceisOutofRange_Label__OutOfRange));
                            FlexibleTextView flexibleTextView6 = a2.z;
                            wd4.a((Object) flexibleTextView6, "it.tvLocationStatus");
                            flexibleTextView6.setEnabled(false);
                        }
                    } else if (!d2) {
                        a2.y.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.primaryText));
                        FlexibleTextView flexibleTextView7 = a2.y;
                        wd4.a((Object) flexibleTextView7, "it.tvDeviceName");
                        flexibleTextView7.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView8 = a2.z;
                        wd4.a((Object) flexibleTextView8, "it.tvLocationStatus");
                        flexibleTextView8.setText(tm2.a(getContext(), (int) R.string.Profile_MyWatchFindDevice_DeviceisOutofRange_Label__OutOfRange));
                        FlexibleTextView flexibleTextView9 = a2.z;
                        wd4.a((Object) flexibleTextView9, "it.tvLocationStatus");
                        flexibleTextView9.setEnabled(false);
                    }
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(deviceId).setSerialPrefix(DeviceHelper.o.b(deviceId)).setType(Constants.DeviceType.TYPE_LARGE);
                    ImageView imageView = a2.w;
                    wd4.a((Object) imageView, "it.ivDevice");
                    type.setPlaceHolder(imageView, DeviceHelper.o.b(deviceId, DeviceHelper.ImageStyle.SMALL)).setImageCallback(new e(a2, this, cVar)).download();
                    View view = this.n;
                    if (view != null) {
                        new CloudImageHelper().with().setSerialNumber(deviceId).setSerialPrefix(DeviceHelper.o.b(deviceId)).setType(Constants.DeviceType.TYPE_LARGE).setImageCallback(new f(view, deviceId, this, cVar)).download();
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(lq3 lq3) {
        wd4.b(lq3, "presenter");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "setPresenter: presenter = " + lq3);
        this.k = lq3;
    }

    @DexIgnore
    public void a(Double d2, Double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showLocation: latitude = " + d2 + ", longitude = " + d3);
        rb2 T0 = T0();
        if (T0 != null) {
            ConstraintLayout constraintLayout = T0.q;
            wd4.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(0);
            FlexibleTextView flexibleTextView = T0.r;
            wd4.a((Object) flexibleTextView, "binding.ftvError");
            flexibleTextView.setVisibility(8);
            if (d2 != null && d3 != null) {
                LatLng latLng = new LatLng(d2.doubleValue(), d3.doubleValue());
                ce1 ce1 = this.m;
                if (ce1 != null) {
                    ce1.b(be1.a(latLng, 13.0f));
                    ce1.a(be1.a(18.0f));
                    ce1.a();
                    Bitmap bitmap = this.o;
                    if (bitmap != null) {
                        lf1 lf1 = new lf1();
                        lf1.e(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchFindDevice_DeviceisOutofRange_Label__LastDetectedLocation));
                        lf1.a(jf1.a(bitmap));
                        lf1.a(latLng);
                        ce1.a(lf1);
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a(long j) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showTime: time = " + j);
        ur3<rb2> ur3 = this.l;
        if (ur3 != null) {
            rb2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(DateUtils.getRelativeTimeSpanString(j));
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(int i, String str) {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showErrorDialog");
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(i, str, childFragmentManager);
        }
        rb2 T0 = T0();
        if (T0 != null) {
            SwitchCompat switchCompat = T0.x;
            wd4.a((Object) switchCompat, "binding.swLocate");
            switchCompat.setChecked(false);
            ConstraintLayout constraintLayout = T0.q;
            wd4.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
        }
    }
}
