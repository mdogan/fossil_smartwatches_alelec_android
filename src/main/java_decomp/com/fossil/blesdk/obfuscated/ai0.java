package com.fossil.blesdk.obfuscated;

import android.util.Log;
import android.util.SparseArray;
import com.fossil.blesdk.obfuscated.he0;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ai0 extends ci0 {
    @DexIgnore
    public /* final */ SparseArray<a> j; // = new SparseArray<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements he0.c {
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ he0 f;
        @DexIgnore
        public /* final */ he0.c g;

        @DexIgnore
        public a(int i, he0 he0, he0.c cVar) {
            this.e = i;
            this.f = he0;
            this.g = cVar;
            he0.a((he0.c) this);
        }

        @DexIgnore
        public final void a(vd0 vd0) {
            String valueOf = String.valueOf(vd0);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("beginFailureResolution for ");
            sb.append(valueOf);
            Log.d("AutoManageHelper", sb.toString());
            ai0.this.b(vd0, this.e);
        }
    }

    @DexIgnore
    public ai0(ze0 ze0) {
        super(ze0);
        this.e.a("AutoManageHelper", (LifecycleCallback) this);
    }

    @DexIgnore
    public static ai0 b(ye0 ye0) {
        ze0 a2 = LifecycleCallback.a(ye0);
        ai0 ai0 = (ai0) a2.a("AutoManageHelper", ai0.class);
        if (ai0 != null) {
            return ai0;
        }
        return new ai0(a2);
    }

    @DexIgnore
    public final void a(int i, he0 he0, he0.c cVar) {
        ck0.a(he0, (Object) "GoogleApiClient instance cannot be null");
        boolean z = this.j.indexOfKey(i) < 0;
        StringBuilder sb = new StringBuilder(54);
        sb.append("Already managing a GoogleApiClient with id ");
        sb.append(i);
        ck0.b(z, sb.toString());
        di0 di0 = this.g.get();
        boolean z2 = this.f;
        String valueOf = String.valueOf(di0);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 49);
        sb2.append("starting AutoManage for client ");
        sb2.append(i);
        sb2.append(" ");
        sb2.append(z2);
        sb2.append(" ");
        sb2.append(valueOf);
        Log.d("AutoManageHelper", sb2.toString());
        this.j.put(i, new a(i, he0, cVar));
        if (this.f && di0 == null) {
            String valueOf2 = String.valueOf(he0);
            StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 11);
            sb3.append("connecting ");
            sb3.append(valueOf2);
            Log.d("AutoManageHelper", sb3.toString());
            he0.c();
        }
    }

    @DexIgnore
    public void d() {
        super.d();
        boolean z = this.f;
        String valueOf = String.valueOf(this.j);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14);
        sb.append("onStart ");
        sb.append(z);
        sb.append(" ");
        sb.append(valueOf);
        Log.d("AutoManageHelper", sb.toString());
        if (this.g.get() == null) {
            for (int i = 0; i < this.j.size(); i++) {
                a b = b(i);
                if (b != null) {
                    b.f.c();
                }
            }
        }
    }

    @DexIgnore
    public void e() {
        super.e();
        for (int i = 0; i < this.j.size(); i++) {
            a b = b(i);
            if (b != null) {
                b.f.d();
            }
        }
    }

    @DexIgnore
    public final void f() {
        for (int i = 0; i < this.j.size(); i++) {
            a b = b(i);
            if (b != null) {
                b.f.c();
            }
        }
    }

    @DexIgnore
    public final a b(int i) {
        if (this.j.size() <= i) {
            return null;
        }
        SparseArray<a> sparseArray = this.j;
        return sparseArray.get(sparseArray.keyAt(i));
    }

    @DexIgnore
    public final void a(int i) {
        a aVar = this.j.get(i);
        this.j.remove(i);
        if (aVar != null) {
            aVar.f.b((he0.c) aVar);
            aVar.f.d();
        }
    }

    @DexIgnore
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        for (int i = 0; i < this.j.size(); i++) {
            a b = b(i);
            if (b != null) {
                printWriter.append(str).append("GoogleApiClient #").print(b.e);
                printWriter.println(":");
                b.f.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
            }
        }
    }

    @DexIgnore
    public final void a(vd0 vd0, int i) {
        Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        a aVar = this.j.get(i);
        if (aVar != null) {
            a(i);
            he0.c cVar = aVar.g;
            if (cVar != null) {
                cVar.a(vd0);
            }
        }
    }
}
