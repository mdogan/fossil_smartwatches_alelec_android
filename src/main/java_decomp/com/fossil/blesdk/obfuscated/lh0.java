package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface lh0 {
    @DexIgnore
    void a(uj0 uj0, Set<Scope> set);

    @DexIgnore
    void b(vd0 vd0);
}
