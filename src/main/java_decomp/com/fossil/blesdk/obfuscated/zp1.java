package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zp1 implements Parcelable.Creator<yp1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        int i = 0;
        byte[] bArr = null;
        String str2 = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 2) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 == 3) {
                str = SafeParcelReader.f(parcel, a);
            } else if (a2 == 4) {
                bArr = SafeParcelReader.b(parcel, a);
            } else if (a2 != 5) {
                SafeParcelReader.v(parcel, a);
            } else {
                str2 = SafeParcelReader.f(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new yp1(i, str, bArr, str2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new yp1[i];
    }
}
