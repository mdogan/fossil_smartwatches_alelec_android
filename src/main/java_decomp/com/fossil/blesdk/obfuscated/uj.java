package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build;
import androidx.work.ExistingWorkPolicy;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.utils.ForceStopRunnable;
import com.fossil.blesdk.obfuscated.ej;
import com.fossil.blesdk.obfuscated.yi;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uj extends kj {
    @DexIgnore
    public static uj j;
    @DexIgnore
    public static uj k;
    @DexIgnore
    public static /* final */ Object l; // = new Object();
    @DexIgnore
    public Context a;
    @DexIgnore
    public yi b;
    @DexIgnore
    public WorkDatabase c;
    @DexIgnore
    public am d;
    @DexIgnore
    public List<qj> e;
    @DexIgnore
    public pj f;
    @DexIgnore
    public tl g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public BroadcastReceiver.PendingResult i;

    @DexIgnore
    public uj(Context context, yi yiVar, am amVar) {
        this(context, yiVar, amVar, context.getResources().getBoolean(ij.workmanager_test_configuration));
    }

    @DexIgnore
    @Deprecated
    public static uj a() {
        synchronized (l) {
            if (j != null) {
                uj ujVar = j;
                return ujVar;
            }
            uj ujVar2 = k;
            return ujVar2;
        }
    }

    @DexIgnore
    public Context b() {
        return this.a;
    }

    @DexIgnore
    public yi c() {
        return this.b;
    }

    @DexIgnore
    public tl d() {
        return this.g;
    }

    @DexIgnore
    public pj e() {
        return this.f;
    }

    @DexIgnore
    public List<qj> f() {
        return this.e;
    }

    @DexIgnore
    public WorkDatabase g() {
        return this.c;
    }

    @DexIgnore
    public am h() {
        return this.d;
    }

    @DexIgnore
    public void i() {
        synchronized (l) {
            this.h = true;
            if (this.i != null) {
                this.i.finish();
                this.i = null;
            }
        }
    }

    @DexIgnore
    public void j() {
        if (Build.VERSION.SDK_INT >= 23) {
            fk.a(b());
        }
        g().d().d();
        rj.a(c(), g(), f());
    }

    @DexIgnore
    public void b(String str) {
        this.d.a(new wl(this, str));
    }

    @DexIgnore
    public uj(Context context, yi yiVar, am amVar, boolean z) {
        this(context, yiVar, amVar, WorkDatabase.a(context.getApplicationContext(), amVar.b(), z));
    }

    @DexIgnore
    public static uj a(Context context) {
        uj a2;
        synchronized (l) {
            a2 = a();
            if (a2 == null) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext instanceof yi.b) {
                    a(applicationContext, ((yi.b) applicationContext).a());
                    a2 = a(applicationContext);
                } else {
                    throw new IllegalStateException("WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider.");
                }
            }
        }
        return a2;
    }

    @DexIgnore
    public uj(Context context, yi yiVar, am amVar, WorkDatabase workDatabase) {
        Context applicationContext = context.getApplicationContext();
        ej.a((ej) new ej.a(yiVar.g()));
        Context context2 = context;
        yi yiVar2 = yiVar;
        am amVar2 = amVar;
        WorkDatabase workDatabase2 = workDatabase;
        List<qj> a2 = a(applicationContext, amVar);
        a(context2, yiVar2, amVar2, workDatabase2, a2, new pj(context2, yiVar2, amVar2, workDatabase2, a2));
    }

    @DexIgnore
    public static void a(Context context, yi yiVar) {
        synchronized (l) {
            if (j != null) {
                if (k != null) {
                    throw new IllegalStateException("WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class levelJavadoc for more information.");
                }
            }
            if (j == null) {
                Context applicationContext = context.getApplicationContext();
                if (k == null) {
                    k = new uj(applicationContext, yiVar, new bm(yiVar.h()));
                }
                j = k;
            }
        }
    }

    @DexIgnore
    public gj a(String str, ExistingWorkPolicy existingWorkPolicy, List<fj> list) {
        return new sj(this, str, existingWorkPolicy, list).a();
    }

    @DexIgnore
    public void a(String str) {
        a(str, (WorkerParameters.a) null);
    }

    @DexIgnore
    public void a(String str, WorkerParameters.a aVar) {
        this.d.a(new vl(this, str, aVar));
    }

    @DexIgnore
    public void a(BroadcastReceiver.PendingResult pendingResult) {
        synchronized (l) {
            this.i = pendingResult;
            if (this.h) {
                this.i.finish();
                this.i = null;
            }
        }
    }

    @DexIgnore
    public final void a(Context context, yi yiVar, am amVar, WorkDatabase workDatabase, List<qj> list, pj pjVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        this.b = yiVar;
        this.d = amVar;
        this.c = workDatabase;
        this.e = list;
        this.f = pjVar;
        this.g = new tl(this.a);
        this.h = false;
        this.d.a(new ForceStopRunnable(applicationContext, this));
    }

    @DexIgnore
    public List<qj> a(Context context, am amVar) {
        return Arrays.asList(new qj[]{rj.a(context, this), new wj(context, amVar, this)});
    }
}
