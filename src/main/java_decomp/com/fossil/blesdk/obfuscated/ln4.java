package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ln4 {
    @DexIgnore
    jp4 a(pm4 pm4, long j);

    @DexIgnore
    qm4 a(Response response) throws IOException;

    @DexIgnore
    Response.a a(boolean z) throws IOException;

    @DexIgnore
    void a() throws IOException;

    @DexIgnore
    void a(pm4 pm4) throws IOException;

    @DexIgnore
    void b() throws IOException;

    @DexIgnore
    void cancel();
}
