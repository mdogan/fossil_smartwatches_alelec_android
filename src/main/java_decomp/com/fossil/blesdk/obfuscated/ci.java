package com.fossil.blesdk.obfuscated;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Build;
import android.util.Log;
import android.util.Property;
import android.view.View;
import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ci {
    @DexIgnore
    public static /* final */ gi a;
    @DexIgnore
    public static Field b;
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public static /* final */ Property<View, Float> d; // = new a(Float.class, "translationAlpha");
    @DexIgnore
    public static /* final */ Property<View, Rect> e; // = new b(Rect.class, "clipBounds");

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Property<View, Float> {
        @DexIgnore
        public a(Class cls, String str) {
            super(cls, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Float get(View view) {
            return Float.valueOf(ci.c(view));
        }

        @DexIgnore
        /* renamed from: a */
        public void set(View view, Float f) {
            ci.a(view, f.floatValue());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends Property<View, Rect> {
        @DexIgnore
        public b(Class cls, String str) {
            super(cls, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Rect get(View view) {
            return g9.e(view);
        }

        @DexIgnore
        /* renamed from: a */
        public void set(View view, Rect rect) {
            g9.a(view, rect);
        }
    }

    /*
    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 22) {
            a = new fi();
        } else if (i >= 21) {
            a = new ei();
        } else if (i >= 19) {
            a = new di();
        } else {
            a = new gi();
        }
    }
    */

    @DexIgnore
    public static void a(View view, float f) {
        a.a(view, f);
    }

    @DexIgnore
    public static bi b(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new ai(view);
        }
        return zh.c(view);
    }

    @DexIgnore
    public static float c(View view) {
        return a.b(view);
    }

    @DexIgnore
    public static ki d(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new ji(view);
        }
        return new ii(view.getWindowToken());
    }

    @DexIgnore
    public static void e(View view) {
        a.c(view);
    }

    @DexIgnore
    public static void a(View view) {
        a.a(view);
    }

    @DexIgnore
    public static void c(View view, Matrix matrix) {
        a.c(view, matrix);
    }

    @DexIgnore
    public static void a(View view, int i) {
        a();
        Field field = b;
        if (field != null) {
            try {
                b.setInt(view, i | (field.getInt(view) & -13));
            } catch (IllegalAccessException unused) {
            }
        }
    }

    @DexIgnore
    public static void b(View view, Matrix matrix) {
        a.b(view, matrix);
    }

    @DexIgnore
    public static void a(View view, Matrix matrix) {
        a.a(view, matrix);
    }

    @DexIgnore
    public static void a(View view, int i, int i2, int i3, int i4) {
        a.a(view, i, i2, i3, i4);
    }

    @DexIgnore
    public static void a() {
        if (!c) {
            try {
                b = View.class.getDeclaredField("mViewFlags");
                b.setAccessible(true);
            } catch (NoSuchFieldException unused) {
                Log.i("ViewUtils", "fetchViewFlagsField: ");
            }
            c = true;
        }
    }
}
