package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.jw1;
import com.google.firebase.components.MissingDependencyException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sw1 extends ow1 {
    @DexIgnore
    public /* final */ List<jw1<?>> a;
    @DexIgnore
    public /* final */ Map<Class<?>, ww1<?>> b; // = new HashMap();
    @DexIgnore
    public /* final */ uw1 c;

    @DexIgnore
    public sw1(Executor executor, Iterable<mw1> iterable, jw1<?>... jw1Arr) {
        this.c = new uw1(executor);
        ArrayList arrayList = new ArrayList();
        arrayList.add(jw1.a(this.c, uw1.class, cx1.class, bx1.class));
        for (mw1 components : iterable) {
            arrayList.addAll(components.getComponents());
        }
        Collections.addAll(arrayList, jw1Arr);
        this.a = Collections.unmodifiableList(jw1.a.a((List<jw1<?>>) arrayList));
        for (jw1<?> a2 : this.a) {
            a(a2);
        }
        a();
    }

    @DexIgnore
    public final void a(boolean z) {
        for (jw1 next : this.a) {
            if (next.e() || (next.f() && z)) {
                a((Class) next.a().iterator().next());
            }
        }
        this.c.a();
    }

    @DexIgnore
    public final <T> fz1<T> b(Class<T> cls) {
        ck0.a(cls, (Object) "Null interface requested.");
        return this.b.get(cls);
    }

    @DexIgnore
    public final <T> void a(jw1<T> jw1) {
        ww1 ww1 = new ww1(jw1.c(), new yw1(jw1, this));
        for (Class<? super T> put : jw1.a()) {
            this.b.put(put, ww1);
        }
    }

    @DexIgnore
    public final void a() {
        for (jw1 next : this.a) {
            Iterator<nw1> it = next.b().iterator();
            while (true) {
                if (it.hasNext()) {
                    nw1 next2 = it.next();
                    if (next2.b() && !this.b.containsKey(next2.a())) {
                        throw new MissingDependencyException(String.format("Unsatisfied dependency for component %s: %s", new Object[]{next, next2.a()}));
                    }
                }
            }
        }
    }
}
