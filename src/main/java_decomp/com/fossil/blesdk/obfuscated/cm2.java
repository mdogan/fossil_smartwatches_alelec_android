package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cm2 {
    @DexIgnore
    public im2 a; // = null;

    @DexIgnore
    public cm2(im2 im2) {
        this.a = im2;
    }

    @DexIgnore
    public bm2 a() {
        return this.a.a(137);
    }
}
