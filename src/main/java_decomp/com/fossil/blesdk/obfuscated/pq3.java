package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pq3 implements Factory<FindDevicePresenter> {
    @DexIgnore
    public static FindDevicePresenter a(sc scVar, DeviceRepository deviceRepository, fn2 fn2, mq3 mq3, fr2 fr2, gr2 gr2, GetAddress getAddress, hr2 hr2, PortfolioApp portfolioApp) {
        return new FindDevicePresenter(scVar, deviceRepository, fn2, mq3, fr2, gr2, getAddress, hr2, portfolioApp);
    }
}
