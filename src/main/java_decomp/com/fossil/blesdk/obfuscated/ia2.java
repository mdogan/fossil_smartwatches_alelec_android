package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ia2 extends ha2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public long z;

    /*
    static {
        B.put(R.id.ib_back, 1);
        B.put(R.id.tv_right, 2);
        B.put(R.id.fet_address_name, 3);
        B.put(R.id.v_underline, 4);
        B.put(R.id.autocomplete_places, 5);
        B.put(R.id.close_iv, 6);
        B.put(R.id.line, 7);
        B.put(R.id.ftv_address_error, 8);
        B.put(R.id.bottom_line, 9);
        B.put(R.id.ll_avoid_tolls_container, 10);
        B.put(R.id.sc_avoid_tolls, 11);
    }
    */

    @DexIgnore
    public ia2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 12, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ia2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[5], objArr[9], objArr[6], objArr[3], objArr[8], objArr[1], objArr[7], objArr[10], objArr[11], objArr[2], objArr[4]);
        this.z = -1;
        this.y = objArr[0];
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
