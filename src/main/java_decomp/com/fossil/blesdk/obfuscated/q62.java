package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q62 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public ArrayList<h13> a;
    @DexIgnore
    public d b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public View e;
        @DexIgnore
        public TextView f;
        @DexIgnore
        public /* final */ /* synthetic */ q62 g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(q62 q62, View view) {
            super(view);
            wd4.b(view, "view");
            this.g = q62;
            View findViewById = view.findViewById(R.id.btn_add);
            wd4.a((Object) findViewById, "view.findViewById(R.id.btn_add)");
            this.e = findViewById;
            View findViewById2 = view.findViewById(R.id.tv_create_new_preset);
            wd4.a((Object) findViewById2, "view.findViewById(R.id.tv_create_new_preset)");
            this.f = (TextView) findViewById2;
            this.e.setOnClickListener(this);
        }

        @DexIgnore
        public final void a(boolean z) {
            if (z) {
                this.e.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.bg_button_grey));
                this.e.setClickable(false);
                this.f.setText(PortfolioApp.W.c().getString(R.string.Customization_MaximumPresets_Actions_Text__YouveSavedMaximumNumberOfPresets));
                return;
            }
            this.e.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.bg_button_black));
            this.e.setClickable(true);
            this.f.setText(PortfolioApp.W.c().getString(R.string.Customization_CreateNew_PresetSwipedCreate_Text__CreateANewPreset));
        }

        @DexIgnore
        public void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if (view != null && view.getId() == R.id.btn_add) {
                this.g.b.z0();
            }
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(h13 h13, List<? extends g8<View, String>> list, List<? extends g8<CustomizeWidget, String>> list2, String str, int i);

        @DexIgnore
        void b(boolean z, String str, String str2, String str3);

        @DexIgnore
        void c(String str, String str2);

        @DexIgnore
        void y0();

        @DexIgnore
        void z0();
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public q62(ArrayList<h13> arrayList, d dVar) {
        wd4.b(arrayList, "mData");
        wd4.b(dVar, "mListener");
        this.a = arrayList;
        this.b = dVar;
        setHasStableIds(true);
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) this.a.get(i).hashCode();
    }

    @DexIgnore
    public int getItemViewType(int i) {
        if (i == this.a.size() - 1) {
            return 0;
        }
        return i == 0 ? 2 : 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        wd4.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeAdapter", "onBindViewHolder pos=" + i);
        boolean z = true;
        if (viewHolder instanceof b) {
            b bVar = (b) viewHolder;
            h13 h13 = this.a.get(i);
            wd4.a((Object) h13, "mData[position]");
            h13 h132 = h13;
            if (i != 0) {
                z = false;
            }
            bVar.a(h132, z);
        } else if (viewHolder instanceof c) {
            c cVar = (c) viewHolder;
            if (this.a.size() <= 10) {
                z = false;
            }
            cVar.a(z);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        if (i == 1 || i == 2) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_hybrid_customize_preset_detail, viewGroup, false);
            wd4.a((Object) inflate, "LayoutInflater.from(pare\u2026et_detail, parent, false)");
            return new b(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_customize_preset_add, viewGroup, false);
        wd4.a((Object) inflate2, "LayoutInflater.from(pare\u2026reset_add, parent, false)");
        return new c(this, inflate2);
    }

    @DexIgnore
    public final void a(ArrayList<h13> arrayList) {
        wd4.b(arrayList, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeAdapter", "setData - data=" + arrayList);
        this.a.clear();
        this.a.addAll(arrayList);
        this.a.add(new h13("", "", new ArrayList(), false));
        notifyDataSetChanged();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public TextView e;
        @DexIgnore
        public CustomizeWidget f;
        @DexIgnore
        public CustomizeWidget g;
        @DexIgnore
        public CustomizeWidget h;
        @DexIgnore
        public TextView i;
        @DexIgnore
        public View j;
        @DexIgnore
        public TextView k;
        @DexIgnore
        public ImageView l;
        @DexIgnore
        public h13 m;
        @DexIgnore
        public View n;
        @DexIgnore
        public View o;
        @DexIgnore
        public View p;
        @DexIgnore
        public View q;
        @DexIgnore
        public /* final */ /* synthetic */ q62 r;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(q62 q62, View view) {
            super(view);
            wd4.b(view, "view");
            this.r = q62;
            View findViewById = view.findViewById(R.id.tv_preset_name);
            wd4.a((Object) findViewById, "view.findViewById(R.id.tv_preset_name)");
            this.e = (TextView) findViewById;
            this.e.setOnClickListener(this);
            View findViewById2 = view.findViewById(R.id.wa_top);
            wd4.a((Object) findViewById2, "view.findViewById(R.id.wa_top)");
            this.f = (CustomizeWidget) findViewById2;
            View findViewById3 = view.findViewById(R.id.wa_middle);
            wd4.a((Object) findViewById3, "view.findViewById(R.id.wa_middle)");
            this.g = (CustomizeWidget) findViewById3;
            View findViewById4 = view.findViewById(R.id.wa_bottom);
            wd4.a((Object) findViewById4, "view.findViewById(R.id.wa_bottom)");
            this.h = (CustomizeWidget) findViewById4;
            View findViewById5 = view.findViewById(R.id.iv_watch_theme_background);
            wd4.a((Object) findViewById5, "view.findViewById(R.id.iv_watch_theme_background)");
            this.l = (ImageView) findViewById5;
            View findViewById6 = view.findViewById(R.id.v_underline);
            wd4.a((Object) findViewById6, "view.findViewById(R.id.v_underline)");
            this.q = findViewById6;
            View findViewById7 = view.findViewById(R.id.line_bottom);
            wd4.a((Object) findViewById7, "view.findViewById(R.id.line_bottom)");
            this.p = findViewById7;
            View findViewById8 = view.findViewById(R.id.line_center);
            wd4.a((Object) findViewById8, "view.findViewById(R.id.line_center)");
            this.o = findViewById8;
            View findViewById9 = view.findViewById(R.id.line_top);
            wd4.a((Object) findViewById9, "view.findViewById(R.id.line_top)");
            this.n = findViewById9;
            View findViewById10 = view.findViewById(R.id.layout_right);
            wd4.a((Object) findViewById10, "view.findViewById(R.id.layout_right)");
            this.j = findViewById10;
            View findViewById11 = view.findViewById(R.id.tv_right);
            wd4.a((Object) findViewById11, "view.findViewById(R.id.tv_right)");
            this.k = (TextView) findViewById11;
            View findViewById12 = view.findViewById(R.id.tv_left);
            wd4.a((Object) findViewById12, "view.findViewById(R.id.tv_left)");
            this.i = (TextView) findViewById12;
            this.j.setOnClickListener(this);
            this.i.setOnClickListener(this);
            this.f.setOnClickListener(this);
            this.g.setOnClickListener(this);
            this.h.setOnClickListener(this);
            if (FossilDeviceSerialPatternUtil.isSamDevice(PortfolioApp.W.c().e())) {
                this.l.setImageResource(R.drawable.hybrid_default_watch_background);
            }
        }

        @DexIgnore
        public final List<g8<View, String>> a() {
            g8[] g8VarArr = new g8[6];
            TextView textView = this.e;
            g8VarArr[0] = new g8(textView, textView.getTransitionName());
            View view = this.q;
            g8VarArr[1] = new g8(view, view.getTransitionName());
            ImageView imageView = this.l;
            if (imageView != null) {
                g8VarArr[2] = new g8(imageView, imageView.getTransitionName());
                View view2 = this.p;
                g8VarArr[3] = new g8(view2, view2.getTransitionName());
                View view3 = this.o;
                g8VarArr[4] = new g8(view3, view3.getTransitionName());
                View view4 = this.n;
                g8VarArr[5] = new g8(view4, view4.getTransitionName());
                return ob4.c(g8VarArr);
            }
            throw new TypeCastException("null cannot be cast to non-null type android.view.View");
        }

        @DexIgnore
        public final List<g8<CustomizeWidget, String>> b() {
            CustomizeWidget customizeWidget = this.f;
            CustomizeWidget customizeWidget2 = this.g;
            CustomizeWidget customizeWidget3 = this.h;
            return ob4.c(new g8(customizeWidget, customizeWidget.getTransitionName()), new g8(customizeWidget2, customizeWidget2.getTransitionName()), new g8(customizeWidget3, customizeWidget3.getTransitionName()));
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0098, code lost:
            if (r0 != null) goto L_0x009c;
         */
        @DexIgnore
        public void onClick(View view) {
            String str;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if ((getAdapterPosition() != -1 || this.m == null) && view != null) {
                switch (view.getId()) {
                    case R.id.layout_right /*2131362478*/:
                        this.r.b.y0();
                        return;
                    case R.id.tv_left /*2131362897*/:
                        if (this.r.a.size() > 2) {
                            Object obj = this.r.a.get(1);
                            wd4.a(obj, "mData[1]");
                            h13 h13 = (h13) obj;
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            StringBuilder sb = new StringBuilder();
                            sb.append("current ");
                            h13 h132 = this.m;
                            sb.append(h132 != null ? h132.c() : null);
                            sb.append(" nextPreset ");
                            sb.append(h13.c());
                            local2.d("CustomizeAdapter", sb.toString());
                            d b = this.r.b;
                            h13 h133 = this.m;
                            if (h133 != null) {
                                boolean d = h133.d();
                                h13 h134 = this.m;
                                if (h134 != null) {
                                    b.b(d, h134.c(), h13.c(), h13.b());
                                    return;
                                } else {
                                    wd4.a();
                                    throw null;
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            return;
                        }
                    case R.id.tv_preset_name /*2131362920*/:
                        d b2 = this.r.b;
                        h13 h135 = this.m;
                        String str2 = "";
                        if (h135 != null) {
                            str = h135.c();
                            break;
                        }
                        str = str2;
                        h13 h136 = this.m;
                        if (h136 != null) {
                            String b3 = h136.b();
                            if (b3 != null) {
                                str2 = b3;
                            }
                        }
                        b2.c(str, str2);
                        return;
                    case R.id.wa_bottom /*2131363029*/:
                        this.r.b.a(this.m, a(), b(), "bottom", getAdapterPosition());
                        return;
                    case R.id.wa_middle /*2131363030*/:
                        this.r.b.a(this.m, a(), b(), "middle", getAdapterPosition());
                        return;
                    case R.id.wa_top /*2131363031*/:
                        this.r.b.a(this.m, a(), b(), ViewHierarchy.DIMENSION_TOP_KEY, getAdapterPosition());
                        return;
                    default:
                        return;
                }
            }
        }

        @DexIgnore
        public final void a(h13 h13, boolean z) {
            wd4.b(h13, "preset");
            if (z) {
                this.j.setSelected(true);
                this.k.setText(PortfolioApp.W.c().getString(R.string.Customization_Delete_PresetDeleted_Label__Applied));
                this.k.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_white_tint, 0, 0, 0);
                this.k.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.white));
                this.j.setClickable(false);
                if (this.r.a.size() == 2) {
                    this.i.setVisibility(4);
                }
            } else {
                this.j.setSelected(false);
                this.k.setText(PortfolioApp.W.c().getString(R.string.Customization_CreateNew_PresetSwiped_CTA__Apply));
                this.k.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                this.k.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.primaryColor));
                this.j.setClickable(true);
                this.i.setVisibility(0);
            }
            this.m = h13;
            this.e.setText(h13.c());
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(h13.a());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "build with microApps=" + arrayList);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                i13 i13 = (i13) it.next();
                String f2 = i13.f();
                if (f2 != null) {
                    int hashCode = f2.hashCode();
                    if (hashCode != -1383228885) {
                        if (hashCode != -1074341483) {
                            if (hashCode == 115029 && f2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                this.f.c(i13.d());
                                this.f.h();
                            }
                        } else if (f2.equals("middle")) {
                            this.g.c(i13.d());
                            this.g.h();
                        }
                    } else if (f2.equals("bottom")) {
                        this.h.c(i13.d());
                        this.h.h();
                    }
                }
            }
        }
    }
}
