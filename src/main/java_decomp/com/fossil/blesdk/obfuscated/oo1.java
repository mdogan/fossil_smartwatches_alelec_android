package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oo1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ xn1 e;
    @DexIgnore
    public /* final */ /* synthetic */ no1 f;

    @DexIgnore
    public oo1(no1 no1, xn1 xn1) {
        this.f = no1;
        this.e = xn1;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.f.b) {
            if (this.f.c != null) {
                this.f.c.onSuccess(this.e.b());
            }
        }
    }
}
