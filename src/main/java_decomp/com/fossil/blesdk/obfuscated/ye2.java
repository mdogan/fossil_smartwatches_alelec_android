package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ye2 extends xe2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public /* final */ RelativeLayout v;
    @DexIgnore
    public long w;

    /*
    static {
        y.put(R.id.cl_toolbar, 1);
        y.put(R.id.aciv_back, 2);
        y.put(R.id.tv_title, 3);
        y.put(R.id.height_container, 4);
        y.put(R.id.ftvHeight, 5);
        y.put(R.id.tl_height_unit, 6);
        y.put(R.id.weight_container, 7);
        y.put(R.id.ftvWeight, 8);
        y.put(R.id.tl_weight_unit, 9);
        y.put(R.id.distance_container, 10);
        y.put(R.id.ftvDistance, 11);
        y.put(R.id.tl_distance_unit, 12);
        y.put(R.id.temperature_container, 13);
        y.put(R.id.ftvTemperature, 14);
        y.put(R.id.tl_temperature_unit, 15);
    }
    */

    @DexIgnore
    public ye2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 16, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    public ye2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[2], objArr[1], objArr[10], objArr[11], objArr[5], objArr[14], objArr[8], objArr[4], objArr[13], objArr[12], objArr[6], objArr[15], objArr[9], objArr[3], objArr[7]);
        this.w = -1;
        this.v = objArr[0];
        this.v.setTag((Object) null);
        a(view);
        f();
    }
}
