package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ah0 extends BroadcastReceiver {
    @DexIgnore
    public Context a;
    @DexIgnore
    public /* final */ bh0 b;

    @DexIgnore
    public ah0(bh0 bh0) {
        this.b = bh0;
    }

    @DexIgnore
    public final void a(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void onReceive(Context context, Intent intent) {
        Uri data = intent.getData();
        if ("com.google.android.gms".equals(data != null ? data.getSchemeSpecificPart() : null)) {
            this.b.a();
            a();
        }
    }

    @DexIgnore
    public final synchronized void a() {
        if (this.a != null) {
            this.a.unregisterReceiver(this);
        }
        this.a = null;
    }
}
