package com.fossil.blesdk.obfuscated;

import com.facebook.appevents.AppEventsConstants;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vr3 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ vr3 b; // = new vr3();

    /*
    static {
        String simpleName = vr3.class.getSimpleName();
        wd4.a((Object) simpleName, "BackendURLUtils::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final String a(int i) {
        return a(PortfolioApp.W.c(), i);
    }

    @DexIgnore
    public final String a(PortfolioApp portfolioApp, int i) {
        wd4.b(portfolioApp, "app");
        int j = portfolioApp.u().j();
        boolean z = false;
        if (j == 0 ? !cg4.b("release", "release", true) : j == 1) {
            z = true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a;
        local.d(str, "getBackendUrl - type=" + i + ", isUsingStagingUrl= " + z);
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 4) {
                        return "";
                    }
                    if (z) {
                        return g62.x.e();
                    }
                    return g62.x.d();
                } else if (z) {
                    return g62.x.p();
                } else {
                    return g62.x.o();
                }
            } else if (z) {
                return g62.x.n();
            } else {
                return g62.x.m();
            }
        } else if (z) {
            return g62.x.j();
        } else {
            return g62.x.g();
        }
    }

    @DexIgnore
    public final String a(PortfolioApp portfolioApp, int i, String str) {
        wd4.b(portfolioApp, "app");
        wd4.b(str, "version");
        int j = portfolioApp.u().j();
        boolean z = false;
        if (j == 0 ? !cg4.b("release", "release", true) : j == 1) {
            z = true;
        }
        if (i != 0) {
            return a(portfolioApp, i);
        }
        if (z) {
            int hashCode = str.hashCode();
            if (hashCode != 49) {
                if (hashCode == 50 && str.equals("2")) {
                    return g62.x.k();
                }
            } else if (str.equals(AppEventsConstants.EVENT_PARAM_VALUE_YES)) {
                return g62.x.j();
            }
            return g62.x.l();
        }
        int hashCode2 = str.hashCode();
        if (hashCode2 != 49) {
            if (hashCode2 == 50 && str.equals("2")) {
                return g62.x.h();
            }
        } else if (str.equals(AppEventsConstants.EVENT_PARAM_VALUE_YES)) {
            return g62.x.g();
        }
        return g62.x.i();
    }
}
