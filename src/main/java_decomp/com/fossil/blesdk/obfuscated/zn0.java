package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.sn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zn0 implements sn0.a {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle a;
    @DexIgnore
    public /* final */ /* synthetic */ sn0 b;

    @DexIgnore
    public zn0(sn0 sn0, Bundle bundle) {
        this.b = sn0;
        this.a = bundle;
    }

    @DexIgnore
    public final void a(un0 un0) {
        this.b.a.b(this.a);
    }

    @DexIgnore
    public final int getState() {
        return 1;
    }
}
