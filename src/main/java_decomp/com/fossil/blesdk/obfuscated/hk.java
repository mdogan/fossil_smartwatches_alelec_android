package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hk {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public hk(boolean z, boolean z2, boolean z3, boolean z4) {
        this.a = z;
        this.b = z2;
        this.c = z3;
        this.d = z4;
    }

    @DexIgnore
    public boolean a() {
        return this.a;
    }

    @DexIgnore
    public boolean b() {
        return this.c;
    }

    @DexIgnore
    public boolean c() {
        return this.d;
    }

    @DexIgnore
    public boolean d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || hk.class != obj.getClass()) {
            return false;
        }
        hk hkVar = (hk) obj;
        if (this.a == hkVar.a && this.b == hkVar.b && this.c == hkVar.c && this.d == hkVar.d) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.a ? 1 : 0;
        if (this.b) {
            i += 16;
        }
        if (this.c) {
            i += 256;
        }
        return this.d ? i + 4096 : i;
    }

    @DexIgnore
    public String toString() {
        return String.format("[ Connected=%b Validated=%b Metered=%b NotRoaming=%b ]", new Object[]{Boolean.valueOf(this.a), Boolean.valueOf(this.b), Boolean.valueOf(this.c), Boolean.valueOf(this.d)});
    }
}
