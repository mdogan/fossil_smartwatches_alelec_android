package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface tv {
    @DexIgnore
    Object a();

    @DexIgnore
    void a(GlideException glideException);

    @DexIgnore
    void a(bq<?> bqVar, DataSource dataSource);
}
