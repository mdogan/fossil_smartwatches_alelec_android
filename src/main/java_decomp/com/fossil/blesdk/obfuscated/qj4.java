package com.fossil.blesdk.obfuscated;

import kotlinx.coroutines.TimeoutKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qj4<U, T extends U> extends gg4<T> implements Runnable, kc4<T>, rc4 {
    @DexIgnore
    public /* final */ long h;
    @DexIgnore
    public /* final */ kc4<U> i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qj4(long j, kc4<? super U> kc4) {
        super(kc4.getContext(), true);
        wd4.b(kc4, "uCont");
        this.h = j;
        this.i = kc4;
    }

    @DexIgnore
    public void a(Object obj, int i2) {
        if (obj instanceof zg4) {
            ij4.a(this.i, ((zg4) obj).a, i2);
        } else {
            ij4.b(this.i, obj, i2);
        }
    }

    @DexIgnore
    public boolean f() {
        return true;
    }

    @DexIgnore
    public String g() {
        return super.g() + "(timeMillis=" + this.h + ')';
    }

    @DexIgnore
    public rc4 getCallerFrame() {
        kc4<U> kc4 = this.i;
        if (!(kc4 instanceof rc4)) {
            kc4 = null;
        }
        return (rc4) kc4;
    }

    @DexIgnore
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public int j() {
        return 2;
    }

    @DexIgnore
    public void run() {
        a((Throwable) TimeoutKt.a(this.h, (ri4) this));
    }
}
