package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jh3 extends eh3 {
    @DexIgnore
    public /* final */ fh3 f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        wd4.a((Object) jh3.class.getSimpleName(), "ReplaceBatteryPresenter::class.java.simpleName");
    }
    */

    @DexIgnore
    public jh3(fh3 fh3) {
        wd4.b(fh3, "mView");
        this.f = fh3;
    }

    @DexIgnore
    public void h() {
        this.f.a(this);
    }
}
