package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.ns2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z43 extends as2 {
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public CommuteTimeWatchAppSettingsViewModel j;
    @DexIgnore
    public k42 k;
    @DexIgnore
    public ns2 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final z43 a(String str) {
            wd4.b(str, MicroAppSetting.SETTING);
            z43 z43 = new z43();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.USER_SETTING, str);
            z43.setArguments(bundle);
            return z43;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ns2.b {
        @DexIgnore
        public /* final */ /* synthetic */ z43 a;

        @DexIgnore
        public b(z43 z43) {
            this.a = z43;
        }

        @DexIgnore
        public void a(AddressWrapper addressWrapper) {
            wd4.b(addressWrapper, "address");
            this.a.a(addressWrapper);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fu3 {
        @DexIgnore
        public /* final */ /* synthetic */ z43 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(int i, int i2, z43 z43) {
            super(i, i2);
            this.f = z43;
        }

        @DexIgnore
        public void b(RecyclerView.ViewHolder viewHolder, int i) {
            wd4.b(viewHolder, "viewHolder");
            int adapterPosition = viewHolder.getAdapterPosition();
            ns2 b = this.f.l;
            z43.c(this.f).b(b != null ? b.a(adapterPosition) : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z43 e;

        @DexIgnore
        public d(z43 z43) {
            this.e = z43;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z43 e;

        @DexIgnore
        public e(z43 z43) {
            this.e = z43;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements dc<CommuteTimeWatchAppSettingsViewModel.b> {
        @DexIgnore
        public /* final */ /* synthetic */ z43 a;

        @DexIgnore
        public f(z43 z43) {
            this.a = z43;
        }

        @DexIgnore
        public final void a(CommuteTimeWatchAppSettingsViewModel.b bVar) {
            List<AddressWrapper> a2 = bVar.a();
            if (a2 != null) {
                this.a.x(a2);
            }
        }
    }

    /*
    static {
        wd4.a((Object) z43.class.getSimpleName(), "CommuteTimeWatchAppSetti\u2026nt::class.java.simpleName");
    }
    */

    @DexIgnore
    public static final /* synthetic */ CommuteTimeWatchAppSettingsViewModel c(z43 z43) {
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = z43.j;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            return commuteTimeWatchAppSettingsViewModel;
        }
        wd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        r(true);
        return true;
    }

    @DexIgnore
    public final void T0() {
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
        if (commuteTimeWatchAppSettingsViewModel == null) {
            wd4.d("mViewModel");
            throw null;
        } else if (commuteTimeWatchAppSettingsViewModel.f()) {
            a(new AddressWrapper());
        } else {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.o(childFragmentManager);
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null && i == 113) {
            AddressWrapper addressWrapper = (AddressWrapper) intent.getParcelableExtra("KEY_SELECTED_ADDRESS");
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
            if (commuteTimeWatchAppSettingsViewModel != null) {
                commuteTimeWatchAppSettingsViewModel.a(addressWrapper);
            } else {
                wd4.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.W.c().g().a(new g53()).a(this);
        k42 k42 = this.k;
        String str = null;
        if (k42 != null) {
            jc a2 = mc.a((Fragment) this, (lc.b) k42).a(CommuteTimeWatchAppSettingsViewModel.class);
            wd4.a((Object) a2, "ViewModelProviders.of(th\u2026ngsViewModel::class.java)");
            this.j = (CommuteTimeWatchAppSettingsViewModel) a2;
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
            if (commuteTimeWatchAppSettingsViewModel != null) {
                Bundle arguments = getArguments();
                if (arguments != null) {
                    str = arguments.getString(Constants.USER_SETTING);
                }
                commuteTimeWatchAppSettingsViewModel.b(str);
                return;
            }
            wd4.d("mViewModel");
            throw null;
        }
        wd4.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        ja2 ja2 = (ja2) ra.a(layoutInflater, R.layout.fragment_commute_time_watch_app_settings, viewGroup, false, O0());
        ja2.r.setOnClickListener(new d(this));
        ja2.q.setOnClickListener(new e(this));
        ns2 ns2 = new ns2();
        ns2.a((ns2.b) new b(this));
        this.l = ns2;
        RecyclerView recyclerView = ja2.s;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.l);
        new pe(new c(0, 4, this)).a(recyclerView);
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            commuteTimeWatchAppSettingsViewModel.e().a(getViewLifecycleOwner(), new f(this));
            new ur3(this, ja2);
            wd4.a((Object) ja2, "binding");
            return ja2.d();
        }
        wd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            commuteTimeWatchAppSettingsViewModel.g();
        } else {
            wd4.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void r(boolean z) {
        if (z) {
            Intent intent = new Intent();
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
            if (commuteTimeWatchAppSettingsViewModel != null) {
                intent.putExtra("COMMUTE_TIME_WATCH_APP_SETTING", commuteTimeWatchAppSettingsViewModel.d());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                wd4.d("mViewModel");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    public final void x(List<AddressWrapper> list) {
        ns2 ns2 = this.l;
        if (ns2 != null) {
            ns2.a((List<AddressWrapper>) wb4.d(list));
        }
    }

    @DexIgnore
    public final void a(AddressWrapper addressWrapper) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("KEY_SELECTED_ADDRESS", addressWrapper);
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
        ArrayList<String> arrayList = null;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            CommuteTimeWatchAppSetting c2 = commuteTimeWatchAppSettingsViewModel.c();
            if (c2 != null) {
                arrayList = c2.getListAddressNameExceptOf(addressWrapper);
            }
            bundle.putStringArrayList("KEY_LIST_ADDRESS", arrayList);
            CommuteTimeSettingsDetailActivity.B.a(this, bundle, 113);
            return;
        }
        wd4.d("mViewModel");
        throw null;
    }
}
