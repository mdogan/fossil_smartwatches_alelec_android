package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class a22 {
    @DexIgnore
    public static /* final */ a22 b; // = new y12((a22) null, 0, 0);
    @DexIgnore
    public /* final */ a22 a;

    @DexIgnore
    public a22(a22 a22) {
        this.a = a22;
    }

    @DexIgnore
    public final a22 a() {
        return this.a;
    }

    @DexIgnore
    public abstract void a(b22 b22, byte[] bArr);

    @DexIgnore
    public final a22 b(int i, int i2) {
        return new v12(this, i, i2);
    }

    @DexIgnore
    public final a22 a(int i, int i2) {
        return new y12(this, i, i2);
    }
}
