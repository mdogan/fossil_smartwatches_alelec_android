package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Handler;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x61 implements u61 {
    @DexIgnore
    public static x61 b;
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public x61(Context context) {
        this.a = context;
        this.a.getContentResolver().registerContentObserver(m61.a, true, new z61(this, (Handler) null));
    }

    @DexIgnore
    public static x61 a(Context context) {
        x61 x61;
        synchronized (x61.class) {
            if (b == null) {
                b = l6.b(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0 ? new x61(context) : new x61();
            }
            x61 = b;
        }
        return x61;
    }

    @DexIgnore
    /* renamed from: b */
    public final String a(String str) {
        if (this.a == null) {
            return null;
        }
        try {
            return (String) v61.a(new y61(this, str));
        } catch (SecurityException e) {
            String valueOf = String.valueOf(str);
            Log.e("GservicesLoader", valueOf.length() != 0 ? "Unable to read GServices for: ".concat(valueOf) : new String("Unable to read GServices for: "), e);
            return null;
        }
    }

    @DexIgnore
    public final /* synthetic */ String c(String str) {
        return m61.a(this.a.getContentResolver(), str, (String) null);
    }

    @DexIgnore
    public x61() {
        this.a = null;
    }
}
