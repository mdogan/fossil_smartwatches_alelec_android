package com.fossil.blesdk.obfuscated;

import com.facebook.internal.Utility;
import com.fossil.blesdk.obfuscated.lm4;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import okhttp3.Protocol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sl4 {
    @DexIgnore
    public /* final */ lm4 a;
    @DexIgnore
    public /* final */ gm4 b;
    @DexIgnore
    public /* final */ SocketFactory c;
    @DexIgnore
    public /* final */ tl4 d;
    @DexIgnore
    public /* final */ List<Protocol> e;
    @DexIgnore
    public /* final */ List<bm4> f;
    @DexIgnore
    public /* final */ ProxySelector g;
    @DexIgnore
    public /* final */ Proxy h;
    @DexIgnore
    public /* final */ SSLSocketFactory i;
    @DexIgnore
    public /* final */ HostnameVerifier j;
    @DexIgnore
    public /* final */ xl4 k;

    @DexIgnore
    public sl4(String str, int i2, gm4 gm4, SocketFactory socketFactory, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier, xl4 xl4, tl4 tl4, Proxy proxy, List<Protocol> list, List<bm4> list2, ProxySelector proxySelector) {
        lm4.a aVar = new lm4.a();
        aVar.f(sSLSocketFactory != null ? Utility.URL_SCHEME : "http");
        aVar.b(str);
        aVar.a(i2);
        this.a = aVar.a();
        if (gm4 != null) {
            this.b = gm4;
            if (socketFactory != null) {
                this.c = socketFactory;
                if (tl4 != null) {
                    this.d = tl4;
                    if (list != null) {
                        this.e = vm4.a(list);
                        if (list2 != null) {
                            this.f = vm4.a(list2);
                            if (proxySelector != null) {
                                this.g = proxySelector;
                                this.h = proxy;
                                this.i = sSLSocketFactory;
                                this.j = hostnameVerifier;
                                this.k = xl4;
                                return;
                            }
                            throw new NullPointerException("proxySelector == null");
                        }
                        throw new NullPointerException("connectionSpecs == null");
                    }
                    throw new NullPointerException("protocols == null");
                }
                throw new NullPointerException("proxyAuthenticator == null");
            }
            throw new NullPointerException("socketFactory == null");
        }
        throw new NullPointerException("dns == null");
    }

    @DexIgnore
    public xl4 a() {
        return this.k;
    }

    @DexIgnore
    public List<bm4> b() {
        return this.f;
    }

    @DexIgnore
    public gm4 c() {
        return this.b;
    }

    @DexIgnore
    public HostnameVerifier d() {
        return this.j;
    }

    @DexIgnore
    public List<Protocol> e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof sl4) {
            sl4 sl4 = (sl4) obj;
            return this.a.equals(sl4.a) && a(sl4);
        }
    }

    @DexIgnore
    public Proxy f() {
        return this.h;
    }

    @DexIgnore
    public tl4 g() {
        return this.d;
    }

    @DexIgnore
    public ProxySelector h() {
        return this.g;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((((((((527 + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode()) * 31) + this.f.hashCode()) * 31) + this.g.hashCode()) * 31;
        Proxy proxy = this.h;
        int i2 = 0;
        int hashCode2 = (hashCode + (proxy != null ? proxy.hashCode() : 0)) * 31;
        SSLSocketFactory sSLSocketFactory = this.i;
        int hashCode3 = (hashCode2 + (sSLSocketFactory != null ? sSLSocketFactory.hashCode() : 0)) * 31;
        HostnameVerifier hostnameVerifier = this.j;
        int hashCode4 = (hashCode3 + (hostnameVerifier != null ? hostnameVerifier.hashCode() : 0)) * 31;
        xl4 xl4 = this.k;
        if (xl4 != null) {
            i2 = xl4.hashCode();
        }
        return hashCode4 + i2;
    }

    @DexIgnore
    public SocketFactory i() {
        return this.c;
    }

    @DexIgnore
    public SSLSocketFactory j() {
        return this.i;
    }

    @DexIgnore
    public lm4 k() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Address{");
        sb.append(this.a.g());
        sb.append(":");
        sb.append(this.a.k());
        if (this.h != null) {
            sb.append(", proxy=");
            sb.append(this.h);
        } else {
            sb.append(", proxySelector=");
            sb.append(this.g);
        }
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public boolean a(sl4 sl4) {
        return this.b.equals(sl4.b) && this.d.equals(sl4.d) && this.e.equals(sl4.e) && this.f.equals(sl4.f) && this.g.equals(sl4.g) && vm4.a((Object) this.h, (Object) sl4.h) && vm4.a((Object) this.i, (Object) sl4.i) && vm4.a((Object) this.j, (Object) sl4.j) && vm4.a((Object) this.k, (Object) sl4.k) && k().k() == sl4.k().k();
    }
}
