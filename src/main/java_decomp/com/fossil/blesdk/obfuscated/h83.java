package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.enums.WorkoutType;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h83 extends as2 implements g83 {
    @DexIgnore
    public ur3<v82> j;
    @DexIgnore
    public f83 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b e; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeDetailActivity.a aVar = ActiveTimeDetailActivity.D;
            Date date = new Date();
            wd4.a((Object) view, "it");
            Context context = view.getContext();
            wd4.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ActiveTimeOverviewDayFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public void b(xr2 xr2, ArrayList<String> arrayList) {
        wd4.b(xr2, "baseModel");
        wd4.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewDayFragment", "showDayDetails - baseModel=" + xr2);
        ur3<v82> ur3 = this.j;
        if (ur3 != null) {
            v82 a2 = ur3.a();
            if (a2 != null) {
                OverviewDayChart overviewDayChart = a2.q;
                if (overviewDayChart != null) {
                    BarChart.c cVar = (BarChart.c) xr2;
                    cVar.b(xr2.a.a(cVar.c()));
                    if (!arrayList.isEmpty()) {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
                    } else {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) ml2.b.a(), false, 2, (Object) null);
                    }
                    overviewDayChart.a(xr2);
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onCreateView");
        v82 v82 = (v82) ra.a(layoutInflater, R.layout.fragment_active_time_overview_day, viewGroup, false, O0());
        v82.r.setOnClickListener(b.e);
        this.j = new ur3<>(this, v82);
        ur3<v82> ur3 = this.j;
        if (ur3 != null) {
            v82 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onResume");
        f83 f83 = this.k;
        if (f83 != null) {
            f83.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onStop");
        f83 f83 = this.k;
        if (f83 != null) {
            f83.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(boolean z, List<WorkoutSession> list) {
        wd4.b(list, "workoutSessions");
        ur3<v82> ur3 = this.j;
        if (ur3 != null) {
            v82 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                LinearLayout linearLayout = a2.u;
                wd4.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.s, list.get(0));
                if (size == 1) {
                    tg2 tg2 = a2.t;
                    if (tg2 != null) {
                        View d = tg2.d();
                        if (d != null) {
                            d.setVisibility(8);
                            return;
                        }
                        return;
                    }
                    return;
                }
                tg2 tg22 = a2.t;
                if (tg22 != null) {
                    View d2 = tg22.d();
                    if (d2 != null) {
                        d2.setVisibility(0);
                    }
                }
                a(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            wd4.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    public void a(f83 f83) {
        wd4.b(f83, "presenter");
        this.k = f83;
    }

    @DexIgnore
    public final void a(tg2 tg2, WorkoutSession workoutSession) {
        String str;
        if (tg2 != null) {
            View d = tg2.d();
            wd4.a((Object) d, "binding.root");
            Context context = d.getContext();
            Pair<Integer, Integer> a2 = WorkoutType.Companion.a(workoutSession.getWorkoutType());
            String a3 = tm2.a(context, a2.getSecond().intValue());
            tg2.t.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = tg2.r;
            wd4.a((Object) flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(a3);
            Triple<Integer, Integer, Integer> c = sk2.c(workoutSession.getDuration());
            wd4.a((Object) c, "DateHelper.getTimeValues(workoutSession.duration)");
            FlexibleTextView flexibleTextView2 = tg2.s;
            wd4.a((Object) flexibleTextView2, "it.ftvWorkoutValue");
            if (wd4.a(c.getFirst().intValue(), 0) > 0) {
                be4 be4 = be4.a;
                String a4 = tm2.a(context, (int) R.string.DashboardDiana_Sleep_DetailPage_Text__NumberHrsNumberMinsNumberSecs);
                wd4.a((Object) a4, "LanguageHelper.getString\u2026rHrsNumberMinsNumberSecs)");
                Object[] objArr = {c.getFirst(), c.getSecond(), c.getThird()};
                str = String.format(a4, Arrays.copyOf(objArr, objArr.length));
                wd4.a((Object) str, "java.lang.String.format(format, *args)");
            } else if (wd4.a(c.getSecond().intValue(), 0) > 0) {
                be4 be42 = be4.a;
                String a5 = tm2.a(context, (int) R.string.DashboardDiana_Sleep_DetailPage_Text__NumberMinsNumberSecs);
                wd4.a((Object) a5, "LanguageHelper.getString\u2026xt__NumberMinsNumberSecs)");
                Object[] objArr2 = {c.getSecond(), c.getThird()};
                str = String.format(a5, Arrays.copyOf(objArr2, objArr2.length));
                wd4.a((Object) str, "java.lang.String.format(format, *args)");
            } else {
                be4 be43 = be4.a;
                String a6 = tm2.a(context, (int) R.string.DashboardDiana_Sleep_DetailPage_Text__NumberSecs);
                wd4.a((Object) a6, "LanguageHelper.getString\u2026ailPage_Text__NumberSecs)");
                Object[] objArr3 = {c.getThird()};
                str = String.format(a6, Arrays.copyOf(objArr3, objArr3.length));
                wd4.a((Object) str, "java.lang.String.format(format, *args)");
            }
            flexibleTextView2.setText(str);
            FlexibleTextView flexibleTextView3 = tg2.q;
            wd4.a((Object) flexibleTextView3, "it.ftvWorkoutTime");
            flexibleTextView3.setText(sk2.a(workoutSession.getStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
        }
    }
}
