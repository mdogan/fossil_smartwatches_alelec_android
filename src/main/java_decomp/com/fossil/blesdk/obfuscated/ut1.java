package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ut1<T> {
    @DexIgnore
    boolean apply(T t);

    @DexIgnore
    boolean equals(Object obj);
}
