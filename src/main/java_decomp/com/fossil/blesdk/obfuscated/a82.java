package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* compiled from: lambda */
public final /* synthetic */ class a82 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon11 e;
    @DexIgnore
    private /* final */ /* synthetic */ String f;
    @DexIgnore
    private /* final */ /* synthetic */ List g;
    @DexIgnore
    private /* final */ /* synthetic */ PresetDataSource.GetRecommendedPresetListCallback h;

    @DexIgnore
    public /* synthetic */ a82(PresetRepository.Anon11 anon11, String str, List list, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
        this.e = anon11;
        this.f = str;
        this.g = list;
        this.h = getRecommendedPresetListCallback;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f, this.g, this.h);
    }
}
