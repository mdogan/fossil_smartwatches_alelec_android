package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.pc1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class od1 extends cf0<g41, rc1> {
    @DexIgnore
    public /* final */ /* synthetic */ j41 d;
    @DexIgnore
    public /* final */ /* synthetic */ af0 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public od1(pc1 pc1, af0 af0, j41 j41, af0 af02) {
        super(af0);
        this.d = j41;
        this.e = af02;
    }

    @DexIgnore
    public final /* synthetic */ void a(ee0.b bVar, yn1 yn1) throws RemoteException {
        ((g41) bVar).a(this.d, (af0<rc1>) this.e, (s31) new pc1.a(yn1));
    }
}
