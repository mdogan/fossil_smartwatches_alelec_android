package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class la1<E> extends n71<E> {
    @DexIgnore
    public static /* final */ la1<Object> g;
    @DexIgnore
    public /* final */ List<E> f;

    /*
    static {
        la1<Object> la1 = new la1<>();
        g = la1;
        la1.B();
    }
    */

    @DexIgnore
    public la1() {
        this(new ArrayList(10));
    }

    @DexIgnore
    public static <E> la1<E> b() {
        return g;
    }

    @DexIgnore
    public final void add(int i, E e) {
        a();
        this.f.add(i, e);
        this.modCount++;
    }

    @DexIgnore
    public final E get(int i) {
        return this.f.get(i);
    }

    @DexIgnore
    public final E remove(int i) {
        a();
        E remove = this.f.remove(i);
        this.modCount++;
        return remove;
    }

    @DexIgnore
    public final E set(int i, E e) {
        a();
        E e2 = this.f.set(i, e);
        this.modCount++;
        return e2;
    }

    @DexIgnore
    public final int size() {
        return this.f.size();
    }

    @DexIgnore
    public la1(List<E> list) {
        this.f = list;
    }

    @DexIgnore
    public final /* synthetic */ a91 b(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.f);
            return new la1(arrayList);
        }
        throw new IllegalArgumentException();
    }
}
