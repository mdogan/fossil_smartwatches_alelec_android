package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.a.f;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class s04 extends p04 {
    @DexIgnore
    public Long m; // = null;
    @DexIgnore
    public String n;
    @DexIgnore
    public String o;

    @DexIgnore
    public s04(Context context, String str, String str2, int i, Long l, l04 l04) {
        super(context, i, l04);
        this.o = str;
        this.n = str2;
        this.m = l;
    }

    @DexIgnore
    public f a() {
        return f.PAGE_VIEW;
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        k24.a(jSONObject, "pi", this.n);
        k24.a(jSONObject, "rf", this.o);
        Long l = this.m;
        if (l == null) {
            return true;
        }
        jSONObject.put("du", l);
        return true;
    }
}
