package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ih4 {
    @DexIgnore
    public static final void a(CoroutineContext coroutineContext, Throwable th) {
        wd4.b(coroutineContext, "context");
        wd4.b(th, "exception");
        try {
            CoroutineExceptionHandler coroutineExceptionHandler = (CoroutineExceptionHandler) coroutineContext.get(CoroutineExceptionHandler.c);
            if (coroutineExceptionHandler != null) {
                coroutineExceptionHandler.handleException(coroutineContext, th);
            } else {
                hh4.a(coroutineContext, th);
            }
        } catch (Throwable th2) {
            hh4.a(coroutineContext, a(th, th2));
        }
    }

    @DexIgnore
    public static final Throwable a(Throwable th, Throwable th2) {
        wd4.b(th, "originalException");
        wd4.b(th2, "thrownException");
        if (th == th2) {
            return th;
        }
        RuntimeException runtimeException = new RuntimeException("Exception while trying to handle coroutine exception", th2);
        ua4.a(runtimeException, th);
        return runtimeException;
    }
}
