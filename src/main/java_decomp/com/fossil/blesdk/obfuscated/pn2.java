package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.helper.AlarmHelper;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pn2 implements MembersInjector<on2> {
    @DexIgnore
    public static void a(on2 on2, fn2 fn2) {
        on2.a = fn2;
    }

    @DexIgnore
    public static void a(on2 on2, jr3 jr3) {
        on2.b = jr3;
    }

    @DexIgnore
    public static void a(on2 on2, AlarmHelper alarmHelper) {
        on2.c = alarmHelper;
    }
}
