package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.vl4;
import java.io.IOException;
import okhttp3.Response;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wr4<T> implements Call<T> {
    @DexIgnore
    public /* final */ bs4 e;
    @DexIgnore
    public /* final */ Object[] f;
    @DexIgnore
    public /* final */ vl4.a g;
    @DexIgnore
    public /* final */ sr4<qm4, T> h;
    @DexIgnore
    public volatile boolean i;
    @DexIgnore
    public vl4 j;
    @DexIgnore
    public Throwable k;
    @DexIgnore
    public boolean l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements wl4 {
        @DexIgnore
        public /* final */ /* synthetic */ qr4 a;

        @DexIgnore
        public a(qr4 qr4) {
            this.a = qr4;
        }

        @DexIgnore
        public final void a(Throwable th) {
            try {
                this.a.onFailure(wr4.this, th);
            } catch (Throwable th2) {
                gs4.a(th2);
                th2.printStackTrace();
            }
        }

        @DexIgnore
        public void onFailure(vl4 vl4, IOException iOException) {
            a(iOException);
        }

        @DexIgnore
        public void onResponse(vl4 vl4, Response response) {
            try {
                try {
                    this.a.onResponse(wr4.this, wr4.this.a(response));
                } catch (Throwable th) {
                    gs4.a(th);
                    th.printStackTrace();
                }
            } catch (Throwable th2) {
                gs4.a(th2);
                a(th2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends qm4 {
        @DexIgnore
        public /* final */ qm4 f;
        @DexIgnore
        public /* final */ xo4 g;
        @DexIgnore
        public IOException h;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends ap4 {
            @DexIgnore
            public a(kp4 kp4) {
                super(kp4);
            }

            @DexIgnore
            public long b(vo4 vo4, long j) throws IOException {
                try {
                    return super.b(vo4, j);
                } catch (IOException e) {
                    b.this.h = e;
                    throw e;
                }
            }
        }

        @DexIgnore
        public b(qm4 qm4) {
            this.f = qm4;
            this.g = ep4.a((kp4) new a(qm4.E()));
        }

        @DexIgnore
        public long C() {
            return this.f.C();
        }

        @DexIgnore
        public mm4 D() {
            return this.f.D();
        }

        @DexIgnore
        public xo4 E() {
            return this.g;
        }

        @DexIgnore
        public void G() throws IOException {
            IOException iOException = this.h;
            if (iOException != null) {
                throw iOException;
            }
        }

        @DexIgnore
        public void close() {
            this.f.close();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qm4 {
        @DexIgnore
        public /* final */ mm4 f;
        @DexIgnore
        public /* final */ long g;

        @DexIgnore
        public c(mm4 mm4, long j) {
            this.f = mm4;
            this.g = j;
        }

        @DexIgnore
        public long C() {
            return this.g;
        }

        @DexIgnore
        public mm4 D() {
            return this.f;
        }

        @DexIgnore
        public xo4 E() {
            throw new IllegalStateException("Cannot read raw response body of a converted body.");
        }
    }

    @DexIgnore
    public wr4(bs4 bs4, Object[] objArr, vl4.a aVar, sr4<qm4, T> sr4) {
        this.e = bs4;
        this.f = objArr;
        this.g = aVar;
        this.h = sr4;
    }

    @DexIgnore
    public void a(qr4<T> qr4) {
        vl4 vl4;
        Throwable th;
        gs4.a(qr4, "callback == null");
        synchronized (this) {
            if (!this.l) {
                this.l = true;
                vl4 = this.j;
                th = this.k;
                if (vl4 == null && th == null) {
                    try {
                        vl4 a2 = a();
                        this.j = a2;
                        vl4 = a2;
                    } catch (Throwable th2) {
                        th = th2;
                        gs4.a(th);
                        this.k = th;
                    }
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (th != null) {
            qr4.onFailure(this, th);
            return;
        }
        if (this.i) {
            vl4.cancel();
        }
        vl4.a(new a(qr4));
    }

    @DexIgnore
    public void cancel() {
        vl4 vl4;
        this.i = true;
        synchronized (this) {
            vl4 = this.j;
        }
        if (vl4 != null) {
            vl4.cancel();
        }
    }

    @DexIgnore
    public synchronized pm4 n() {
        vl4 vl4 = this.j;
        if (vl4 != null) {
            return vl4.n();
        } else if (this.k == null) {
            try {
                vl4 a2 = a();
                this.j = a2;
                return a2.n();
            } catch (RuntimeException e2) {
                e = e2;
                gs4.a(e);
                this.k = e;
                throw e;
            } catch (Error e3) {
                e = e3;
                gs4.a(e);
                this.k = e;
                throw e;
            } catch (IOException e4) {
                this.k = e4;
                throw new RuntimeException("Unable to create request.", e4);
            }
        } else if (this.k instanceof IOException) {
            throw new RuntimeException("Unable to create request.", this.k);
        } else if (this.k instanceof RuntimeException) {
            throw ((RuntimeException) this.k);
        } else {
            throw ((Error) this.k);
        }
    }

    @DexIgnore
    public boolean o() {
        boolean z = true;
        if (this.i) {
            return true;
        }
        synchronized (this) {
            if (this.j == null || !this.j.o()) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    public cs4<T> r() throws IOException {
        vl4 vl4;
        synchronized (this) {
            if (!this.l) {
                this.l = true;
                if (this.k == null) {
                    vl4 = this.j;
                    if (vl4 == null) {
                        try {
                            vl4 = a();
                            this.j = vl4;
                        } catch (IOException | Error | RuntimeException e2) {
                            gs4.a(e2);
                            this.k = e2;
                            throw e2;
                        }
                    }
                } else if (this.k instanceof IOException) {
                    throw ((IOException) this.k);
                } else if (this.k instanceof RuntimeException) {
                    throw ((RuntimeException) this.k);
                } else {
                    throw ((Error) this.k);
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (this.i) {
            vl4.cancel();
        }
        return a(vl4.r());
    }

    @DexIgnore
    public wr4<T> clone() {
        return new wr4<>(this.e, this.f, this.g, this.h);
    }

    @DexIgnore
    public final vl4 a() throws IOException {
        vl4 a2 = this.g.a(this.e.a(this.f));
        if (a2 != null) {
            return a2;
        }
        throw new NullPointerException("Call.Factory returned null.");
    }

    @DexIgnore
    public cs4<T> a(Response response) throws IOException {
        qm4 y = response.y();
        Response.a H = response.H();
        H.a((qm4) new c(y.D(), y.C()));
        Response a2 = H.a();
        int B = a2.B();
        if (B < 200 || B >= 300) {
            try {
                return cs4.a(gs4.a(y), a2);
            } finally {
                y.close();
            }
        } else if (B == 204 || B == 205) {
            y.close();
            return cs4.a(null, a2);
        } else {
            b bVar = new b(y);
            try {
                return cs4.a(this.h.a(bVar), a2);
            } catch (RuntimeException e2) {
                bVar.G();
                throw e2;
            }
        }
    }
}
