package com.fossil.blesdk.obfuscated;

import android.util.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class wx1 implements qn1 {
    @DexIgnore
    public /* final */ vx1 a;
    @DexIgnore
    public /* final */ Pair b;

    @DexIgnore
    public wx1(vx1 vx1, Pair pair) {
        this.a = vx1;
        this.b = pair;
    }

    @DexIgnore
    public final Object then(xn1 xn1) {
        this.a.a(this.b, xn1);
        return xn1;
    }
}
