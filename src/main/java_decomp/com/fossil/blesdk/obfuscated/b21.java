package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface b21 extends IInterface {
    @DexIgnore
    void a(lq0 lq0, h11 h11) throws RemoteException;

    @DexIgnore
    void a(x11 x11, p01 p01) throws RemoteException;

    @DexIgnore
    void a(z11 z11, h11 h11) throws RemoteException;
}
