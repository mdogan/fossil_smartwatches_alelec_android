package com.fossil.blesdk.obfuscated;

import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cj4 extends ck4 implements mi4 {
    @DexIgnore
    public cj4 a() {
        return this;
    }

    @DexIgnore
    public final String a(String str) {
        wd4.b(str, "state");
        StringBuilder sb = new StringBuilder();
        sb.append("List{");
        sb.append(str);
        sb.append("}[");
        Object c = c();
        if (c != null) {
            boolean z = true;
            for (ek4 ek4 = (ek4) c; !wd4.a((Object) ek4, (Object) this); ek4 = ek4.d()) {
                if (ek4 instanceof wi4) {
                    wi4 wi4 = (wi4) ek4;
                    if (z) {
                        z = false;
                    } else {
                        sb.append(", ");
                    }
                    sb.append(wi4);
                }
            }
            sb.append("]");
            String sb2 = sb.toString();
            wd4.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
            return sb2;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public boolean isActive() {
        return true;
    }

    @DexIgnore
    public String toString() {
        return oh4.c() ? a("Active") : super.toString();
    }
}
