package com.fossil.blesdk.obfuscated;

import java.util.concurrent.RejectedExecutionException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.scheduling.CoroutineScheduler;
import kotlinx.coroutines.scheduling.TaskMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class al4 extends ji4 {
    @DexIgnore
    public CoroutineScheduler e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ long h;
    @DexIgnore
    public /* final */ String i;

    @DexIgnore
    public al4(int i2, int i3, long j, String str) {
        wd4.b(str, "schedulerName");
        this.f = i2;
        this.g = i3;
        this.h = j;
        this.i = str;
        this.e = C();
    }

    @DexIgnore
    public final CoroutineScheduler C() {
        return new CoroutineScheduler(this.f, this.g, this.h, this.i);
    }

    @DexIgnore
    public void a(CoroutineContext coroutineContext, Runnable runnable) {
        wd4.b(coroutineContext, "context");
        wd4.b(runnable, "block");
        try {
            CoroutineScheduler.a(this.e, runnable, (gl4) null, false, 6, (Object) null);
        } catch (RejectedExecutionException unused) {
            qh4.k.a(coroutineContext, runnable);
        }
    }

    @DexIgnore
    public void b(CoroutineContext coroutineContext, Runnable runnable) {
        wd4.b(coroutineContext, "context");
        wd4.b(runnable, "block");
        try {
            CoroutineScheduler.a(this.e, runnable, (gl4) null, true, 2, (Object) null);
        } catch (RejectedExecutionException unused) {
            qh4.k.b(coroutineContext, runnable);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ al4(int i2, int i3, String str, int i4, rd4 rd4) {
        this((i4 & 1) != 0 ? il4.c : i2, (i4 & 2) != 0 ? il4.d : i3, (i4 & 4) != 0 ? "DefaultDispatcher" : str);
    }

    @DexIgnore
    public final void a(Runnable runnable, gl4 gl4, boolean z) {
        wd4.b(runnable, "block");
        wd4.b(gl4, "context");
        try {
            this.e.a(runnable, gl4, z);
        } catch (RejectedExecutionException unused) {
            qh4.k.a((Runnable) this.e.a(runnable, gl4));
        }
    }

    @DexIgnore
    public final gh4 b(int i2) {
        if (i2 > 0) {
            return new cl4(this, i2, TaskMode.PROBABLY_BLOCKING);
        }
        throw new IllegalArgumentException(("Expected positive parallelism level, but have " + i2).toString());
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public al4(int i2, int i3, String str) {
        this(i2, i3, il4.e, str);
        wd4.b(str, "schedulerName");
    }
}
