package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.migration.MigrationHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j52 implements Factory<MigrationHelper> {
    @DexIgnore
    public /* final */ o42 a;
    @DexIgnore
    public /* final */ Provider<fn2> b;
    @DexIgnore
    public /* final */ Provider<MigrationManager> c;
    @DexIgnore
    public /* final */ Provider<c62> d;

    @DexIgnore
    public j52(o42 o42, Provider<fn2> provider, Provider<MigrationManager> provider2, Provider<c62> provider3) {
        this.a = o42;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    @DexIgnore
    public static j52 a(o42 o42, Provider<fn2> provider, Provider<MigrationManager> provider2, Provider<c62> provider3) {
        return new j52(o42, provider, provider2, provider3);
    }

    @DexIgnore
    public static MigrationHelper b(o42 o42, Provider<fn2> provider, Provider<MigrationManager> provider2, Provider<c62> provider3) {
        return a(o42, provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public static MigrationHelper a(o42 o42, fn2 fn2, MigrationManager migrationManager, c62 c62) {
        MigrationHelper a2 = o42.a(fn2, migrationManager, c62);
        o44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public MigrationHelper get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
