package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class cn1 extends ms0 implements bn1 {
    @DexIgnore
    public cn1() {
        super("com.google.android.gms.signin.internal.ISignInCallbacks");
    }

    @DexIgnore
    public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 3) {
            a((vd0) ns0.a(parcel, vd0.CREATOR), (ym1) ns0.a(parcel, ym1.CREATOR));
        } else if (i == 4) {
            d((Status) ns0.a(parcel, Status.CREATOR));
        } else if (i == 6) {
            e((Status) ns0.a(parcel, Status.CREATOR));
        } else if (i == 7) {
            a((Status) ns0.a(parcel, Status.CREATOR), (GoogleSignInAccount) ns0.a(parcel, GoogleSignInAccount.CREATOR));
        } else if (i != 8) {
            return false;
        } else {
            a((hn1) ns0.a(parcel, hn1.CREATOR));
        }
        parcel2.writeNoException();
        return true;
    }
}
