package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ca1 extends k61 implements h81 {
    @DexIgnore
    public ca1(IBinder iBinder) {
        super(iBinder, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
    }

    @DexIgnore
    public final Bundle zza(Bundle bundle) throws RemoteException {
        Parcel o = o();
        s61.a(o, (Parcelable) bundle);
        Parcel a = a(1, o);
        Bundle bundle2 = (Bundle) s61.a(a, Bundle.CREATOR);
        a.recycle();
        return bundle2;
    }
}
