package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class uk<T> extends vk<T> {
    @DexIgnore
    public static /* final */ String h; // = ej.a("BrdcstRcvrCnstrntTrckr");
    @DexIgnore
    public /* final */ BroadcastReceiver g; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                uk.this.a(context, intent);
            }
        }
    }

    @DexIgnore
    public uk(Context context, am amVar) {
        super(context, amVar);
    }

    @DexIgnore
    public abstract void a(Context context, Intent intent);

    @DexIgnore
    public void b() {
        ej.a().a(h, String.format("%s: registering receiver", new Object[]{getClass().getSimpleName()}), new Throwable[0]);
        this.b.registerReceiver(this.g, d());
    }

    @DexIgnore
    public void c() {
        ej.a().a(h, String.format("%s: unregistering receiver", new Object[]{getClass().getSimpleName()}), new Throwable[0]);
        this.b.unregisterReceiver(this.g);
    }

    @DexIgnore
    public abstract IntentFilter d();
}
