package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.u81;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o51 extends u81<o51, a> implements z91 {
    @DexIgnore
    public static /* final */ o51 zzauy; // = new o51();
    @DexIgnore
    public static volatile ia1<o51> zznw;
    @DexIgnore
    public String zzauw; // = "";
    @DexIgnore
    public long zzaux;
    @DexIgnore
    public int zznr;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends u81.a<o51, a> implements z91 {
        @DexIgnore
        public a() {
            super(o51.zzauy);
        }

        @DexIgnore
        public /* synthetic */ a(p51 p51) {
            this();
        }
    }

    /*
    static {
        u81.a(o51.class, zzauy);
    }
    */

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (p51.a[i - 1]) {
            case 1:
                return new o51();
            case 2:
                return new a((p51) null);
            case 3:
                return u81.a((x91) zzauy, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\b\u0000\u0002\u0002\u0001", new Object[]{"zznr", "zzauw", "zzaux"});
            case 4:
                return zzauy;
            case 5:
                ia1<o51> ia1 = zznw;
                if (ia1 == null) {
                    synchronized (o51.class) {
                        ia1 = zznw;
                        if (ia1 == null) {
                            ia1 = new u81.b<>(zzauy);
                            zznw = ia1;
                        }
                    }
                }
                return ia1;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
