package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.uj0;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class oj0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<oj0> CREATOR; // = new ql0();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public String h;
    @DexIgnore
    public IBinder i;
    @DexIgnore
    public Scope[] j;
    @DexIgnore
    public Bundle k;
    @DexIgnore
    public Account l;
    @DexIgnore
    public xd0[] m;
    @DexIgnore
    public xd0[] n;
    @DexIgnore
    public boolean o;

    @DexIgnore
    public oj0(int i2) {
        this.e = 4;
        this.g = zd0.a;
        this.f = i2;
        this.o = true;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, this.f);
        lk0.a(parcel, 3, this.g);
        lk0.a(parcel, 4, this.h, false);
        lk0.a(parcel, 5, this.i, false);
        lk0.a(parcel, 6, (T[]) this.j, i2, false);
        lk0.a(parcel, 7, this.k, false);
        lk0.a(parcel, 8, (Parcelable) this.l, i2, false);
        lk0.a(parcel, 10, (T[]) this.m, i2, false);
        lk0.a(parcel, 11, (T[]) this.n, i2, false);
        lk0.a(parcel, 12, this.o);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public oj0(int i2, int i3, int i4, String str, IBinder iBinder, Scope[] scopeArr, Bundle bundle, Account account, xd0[] xd0Arr, xd0[] xd0Arr2, boolean z) {
        this.e = i2;
        this.f = i3;
        this.g = i4;
        if ("com.google.android.gms".equals(str)) {
            this.h = "com.google.android.gms";
        } else {
            this.h = str;
        }
        if (i2 < 2) {
            this.l = iBinder != null ? hj0.a(uj0.a.a(iBinder)) : null;
        } else {
            this.i = iBinder;
            this.l = account;
        }
        this.j = scopeArr;
        this.k = bundle;
        this.m = xd0Arr;
        this.n = xd0Arr2;
        this.o = z;
    }
}
