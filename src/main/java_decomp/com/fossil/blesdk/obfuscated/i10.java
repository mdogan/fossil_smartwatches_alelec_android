package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i10 extends BluetoothCommand {
    @DexIgnore
    public UUID[] k; // = new UUID[0];
    @DexIgnore
    public GattCharacteristic.CharacteristicId[] l; // = new GattCharacteristic.CharacteristicId[0];

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i10(Peripheral.c cVar) {
        super(BluetoothCommandId.DISCOVER_SERVICE, cVar);
        wd4.b(cVar, "bluetoothGattOperationCallbackProvider");
    }

    @DexIgnore
    public void a(Peripheral peripheral) {
        wd4.b(peripheral, "peripheral");
        peripheral.f();
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        return gattOperationResult instanceof x10;
    }

    @DexIgnore
    public void d(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        x10 x10 = (x10) gattOperationResult;
        this.k = x10.c();
        this.l = x10.b();
    }

    @DexIgnore
    public sa0<GattOperationResult> f() {
        return b().f();
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId[] i() {
        return this.l;
    }

    @DexIgnore
    public final UUID[] j() {
        return this.k;
    }
}
