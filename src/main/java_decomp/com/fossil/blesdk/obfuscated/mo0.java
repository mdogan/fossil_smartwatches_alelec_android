package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface mo0 extends IInterface {
    @DexIgnore
    int a(tn0 tn0, String str, boolean z) throws RemoteException;

    @DexIgnore
    tn0 a(tn0 tn0, String str, int i) throws RemoteException;

    @DexIgnore
    int b(tn0 tn0, String str, boolean z) throws RemoteException;

    @DexIgnore
    tn0 b(tn0 tn0, String str, int i) throws RemoteException;

    @DexIgnore
    int n() throws RemoteException;
}
