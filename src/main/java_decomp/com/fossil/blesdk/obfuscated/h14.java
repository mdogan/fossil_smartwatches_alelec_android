package com.fossil.blesdk.obfuscated;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h14 {
    @DexIgnore
    public static u14 l; // = f24.b();
    @DexIgnore
    public static Context m; // = null;
    @DexIgnore
    public static h14 n; // = null;
    @DexIgnore
    public q14 a; // = null;
    @DexIgnore
    public q14 b; // = null;
    @DexIgnore
    public z14 c; // = null;
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public volatile int f; // = 0;
    @DexIgnore
    public v14 g; // = null;
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public ConcurrentHashMap<p04, String> i; // = null;
    @DexIgnore
    public boolean j; // = false;
    @DexIgnore
    public HashMap<String, String> k; // = new HashMap<>();

    @DexIgnore
    public h14(Context context) {
        try {
            this.c = new z14();
            m = context.getApplicationContext();
            this.i = new ConcurrentHashMap<>();
            this.d = f24.u(context);
            this.e = "pri_" + f24.u(context);
            this.a = new q14(m, this.d);
            this.b = new q14(m, this.e);
            a(true);
            a(false);
            d();
            a(m);
            c();
            h();
        } catch (Throwable th) {
            l.a(th);
        }
    }

    @DexIgnore
    public static h14 b(Context context) {
        if (n == null) {
            synchronized (h14.class) {
                if (n == null) {
                    n = new h14(context);
                }
            }
        }
        return n;
    }

    @DexIgnore
    public static h14 i() {
        return n;
    }

    @DexIgnore
    public int a() {
        return this.f;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:?, code lost:
        l.a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0203, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01c4, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        r2 = l;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01ec, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01ed, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01ee, code lost:
        if (r3 != null) goto L_0x01f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01f3, code lost:
        r1.a.getWritableDatabase().endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01fd, code lost:
        r0 = move-exception;
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:71:0x01b7, B:81:0x01d0, B:96:0x01f0] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00b3 A[Catch:{ all -> 0x01cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0120 A[Catch:{ all -> 0x01cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0133 A[Catch:{ all -> 0x01cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x01d7 A[SYNTHETIC, Splitter:B:84:0x01d7] */
    public synchronized v14 a(Context context) {
        Cursor cursor;
        String str;
        String str2;
        boolean z;
        String str3;
        synchronized (this) {
            if (this.g != null) {
                v14 v14 = this.g;
                return v14;
            }
            try {
                this.a.getWritableDatabase().beginTransaction();
                if (i04.q()) {
                    try {
                        l.e("try to load user info from db.");
                    } catch (Throwable th) {
                        th = th;
                        cursor = null;
                    }
                }
                cursor = this.a.getReadableDatabase().query("user", (String[]) null, (String) null, (String[]) null, (String) null, (String) null, (String) null, (String) null);
                try {
                    boolean z2 = true;
                    if (cursor.moveToNext()) {
                        String string = cursor.getString(0);
                        String a2 = k24.a(string);
                        int i2 = cursor.getInt(1);
                        String string2 = cursor.getString(2);
                        long j2 = cursor.getLong(3);
                        long currentTimeMillis = System.currentTimeMillis() / 1000;
                        int i3 = (i2 == 1 || f24.a(j2 * 1000).equals(f24.a(currentTimeMillis * 1000))) ? i2 : 1;
                        if (!string2.equals(f24.q(context))) {
                            i3 |= 2;
                        }
                        String[] split = a2.split(",");
                        if (split == null || split.length <= 0) {
                            a2 = f24.e(context);
                            str2 = a2;
                        } else {
                            str2 = split[0];
                            if (str2 != null) {
                                if (str2.length() < 11) {
                                }
                                z = false;
                                if (split != null || split.length < 2) {
                                    str3 = f24.f(context);
                                    if (str3 != null && str3.length() > 0) {
                                        a2 = str2 + "," + str3;
                                        z = true;
                                    }
                                } else {
                                    str3 = split[1];
                                    a2 = str2 + "," + str3;
                                }
                                this.g = new v14(str2, str3, i3);
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("uid", k24.b(a2));
                                contentValues.put("user_type", Integer.valueOf(i3));
                                contentValues.put("app_ver", f24.q(context));
                                contentValues.put("ts", Long.valueOf(currentTimeMillis));
                                if (z) {
                                    this.a.getWritableDatabase().update("user", contentValues, "uid=?", new String[]{string});
                                }
                                if (i3 != i2) {
                                    this.a.getWritableDatabase().replace("user", (String) null, contentValues);
                                }
                            }
                            String a3 = k24.a(context);
                            if (a3 != null && a3.length() > 10) {
                                str2 = a3;
                            }
                            z = false;
                            if (split != null) {
                            }
                            str3 = f24.f(context);
                            a2 = str2 + "," + str3;
                            z = true;
                            this.g = new v14(str2, str3, i3);
                            ContentValues contentValues2 = new ContentValues();
                            contentValues2.put("uid", k24.b(a2));
                            contentValues2.put("user_type", Integer.valueOf(i3));
                            contentValues2.put("app_ver", f24.q(context));
                            contentValues2.put("ts", Long.valueOf(currentTimeMillis));
                            if (z) {
                            }
                            if (i3 != i2) {
                            }
                        }
                        z = true;
                        if (split != null) {
                        }
                        str3 = f24.f(context);
                        a2 = str2 + "," + str3;
                        z = true;
                        this.g = new v14(str2, str3, i3);
                        ContentValues contentValues22 = new ContentValues();
                        contentValues22.put("uid", k24.b(a2));
                        contentValues22.put("user_type", Integer.valueOf(i3));
                        contentValues22.put("app_ver", f24.q(context));
                        contentValues22.put("ts", Long.valueOf(currentTimeMillis));
                        if (z) {
                        }
                        if (i3 != i2) {
                        }
                    } else {
                        z2 = false;
                    }
                    if (!z2) {
                        String e2 = f24.e(context);
                        String f2 = f24.f(context);
                        if (f2 == null || f2.length() <= 0) {
                            str = e2;
                        } else {
                            str = e2 + "," + f2;
                        }
                        String q = f24.q(context);
                        ContentValues contentValues3 = new ContentValues();
                        contentValues3.put("uid", k24.b(str));
                        contentValues3.put("user_type", 0);
                        contentValues3.put("app_ver", q);
                        contentValues3.put("ts", Long.valueOf(System.currentTimeMillis() / 1000));
                        this.a.getWritableDatabase().insert("user", (String) null, contentValues3);
                        this.g = new v14(e2, f2, 0);
                    }
                    this.a.getWritableDatabase().setTransactionSuccessful();
                    if (cursor != null) {
                        cursor.close();
                    }
                    this.a.getWritableDatabase().endTransaction();
                } catch (Throwable th2) {
                    th = th2;
                    l.a(th);
                    if (cursor != null) {
                        try {
                            cursor.close();
                        } catch (Throwable th3) {
                            th = th3;
                            u14 u14 = l;
                            u14.a(th);
                            v14 v142 = this.g;
                            return v142;
                        }
                    }
                    this.a.getWritableDatabase().endTransaction();
                    v14 v1422 = this.g;
                    return v1422;
                }
            } catch (Throwable th4) {
                th = th4;
                cursor = null;
                l.a(th);
                if (cursor != null) {
                }
                this.a.getWritableDatabase().endTransaction();
                v14 v14222 = this.g;
                return v14222;
            }
            v14 v142222 = this.g;
            return v142222;
        }
    }

    @DexIgnore
    public final String a(List<r14> list) {
        StringBuilder sb = new StringBuilder(list.size() * 3);
        sb.append("event_id in (");
        int size = list.size();
        int i2 = 0;
        for (r14 r14 : list) {
            sb.append(r14.a);
            if (i2 != size - 1) {
                sb.append(",");
            }
            i2++;
        }
        sb.append(")");
        return sb.toString();
    }

    @DexIgnore
    public void a(int i2) {
        this.c.a(new p14(this, i2));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0071, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0073, code lost:
        return;
     */
    @DexIgnore
    public final synchronized void a(int i2, boolean z) {
        try {
            if (this.f > 0 && i2 > 0) {
                if (!k04.a()) {
                    if (i04.q()) {
                        u14 u14 = l;
                        u14.e("Load " + this.f + " unsent events");
                    }
                    ArrayList arrayList = new ArrayList(i2);
                    b(arrayList, i2, z);
                    if (arrayList.size() > 0) {
                        if (i04.q()) {
                            u14 u142 = l;
                            u142.e("Peek " + arrayList.size() + " unsent events.");
                        }
                        a((List<r14>) arrayList, 2, z);
                        r24.b(m).b(arrayList, new o14(this, arrayList, z));
                    }
                }
            }
        } catch (Throwable th) {
            l.a(th);
        }
    }

    @DexIgnore
    public void a(o24 o24) {
        if (o24 != null) {
            this.c.a(new m14(this, o24));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0096 A[SYNTHETIC, Splitter:B:27:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c7  */
    public final void a(p04 p04, q24 q24, boolean z) {
        long j2;
        SQLiteDatabase sQLiteDatabase;
        try {
            sQLiteDatabase = c(z);
            try {
                sQLiteDatabase.beginTransaction();
                if (!z) {
                    if (this.f > i04.j()) {
                        l.h("Too many events stored in db.");
                        this.f -= this.a.getWritableDatabase().delete("events", "event_id in (select event_id from events where timestamp in (select min(timestamp) from events) limit 1)", (String[]) null);
                    }
                }
                ContentValues contentValues = new ContentValues();
                String f2 = p04.f();
                if (i04.q()) {
                    l.e("insert 1 event, content:" + f2);
                }
                contentValues.put("content", k24.b(f2));
                contentValues.put("send_count", "0");
                contentValues.put("status", Integer.toString(1));
                contentValues.put("timestamp", Long.valueOf(p04.b()));
                j2 = sQLiteDatabase.insert("events", (String) null, contentValues);
                sQLiteDatabase.setTransactionSuccessful();
                if (sQLiteDatabase != null) {
                    try {
                        sQLiteDatabase.endTransaction();
                    } catch (Throwable th) {
                        l.a(th);
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                j2 = -1;
                try {
                    l.a(th);
                    if (sQLiteDatabase != null) {
                        sQLiteDatabase.endTransaction();
                    }
                    if (j2 > 0) {
                    }
                } catch (Throwable th3) {
                    l.a(th3);
                }
            }
        } catch (Throwable th4) {
            th = th4;
            sQLiteDatabase = null;
            j2 = -1;
            l.a(th);
            if (sQLiteDatabase != null) {
            }
            if (j2 > 0) {
            }
        }
        if (j2 > 0) {
            this.f++;
            if (i04.q()) {
                l.a((Object) "directStoreEvent insert event to db, event:" + p04.f());
            }
            if (q24 != null) {
                q24.a();
                return;
            }
            return;
        }
        l.d("Failed to store event:" + p04.f());
        return;
        throw th;
    }

    @DexIgnore
    public void a(p04 p04, q24 q24, boolean z, boolean z2) {
        z14 z14 = this.c;
        if (z14 != null) {
            z14.a(new l14(this, p04, q24, z, z2));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a8, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        l.a(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00af, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ca, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00cb, code lost:
        if (r7 != null) goto L_0x00cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        r7.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00d1, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        l.a(r6);
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:27:0x00a3, B:39:0x00b4, B:55:0x00cd] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00bb A[SYNTHETIC, Splitter:B:42:0x00bb] */
    public final synchronized void a(List<r14> list, int i2, boolean z) {
        SQLiteDatabase sQLiteDatabase;
        String str;
        if (list.size() != 0) {
            int b2 = b(z);
            String str2 = null;
            try {
                sQLiteDatabase = c(z);
                if (i2 == 2) {
                    try {
                        str = "update events set status=" + i2 + ", send_count=send_count+1  where " + a(list);
                    } catch (Throwable th) {
                        th = th;
                        l.a(th);
                        if (sQLiteDatabase != null) {
                            try {
                                sQLiteDatabase.endTransaction();
                                return;
                            } catch (Throwable th2) {
                                l.a(th2);
                                return;
                            }
                        }
                        return;
                    }
                } else {
                    str = "update events set status=" + i2 + " where " + a(list);
                    if (this.h % 3 == 0) {
                        str2 = "delete from events where send_count>" + b2;
                    }
                    this.h++;
                }
                if (i04.q()) {
                    l.e("update sql:" + str);
                }
                sQLiteDatabase.beginTransaction();
                sQLiteDatabase.execSQL(str);
                if (str2 != null) {
                    l.e("update for delete sql:" + str2);
                    sQLiteDatabase.execSQL(str2);
                    d();
                }
                sQLiteDatabase.setTransactionSuccessful();
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.endTransaction();
                    return;
                }
            } catch (Throwable th3) {
                th = th3;
                sQLiteDatabase = null;
                l.a(th);
                if (sQLiteDatabase != null) {
                }
                return;
            }
        } else {
            return;
        }
        throw th;
    }

    @DexIgnore
    public void a(List<r14> list, int i2, boolean z, boolean z2) {
        z14 z14 = this.c;
        if (z14 != null) {
            z14.a(new i14(this, list, i2, z, z2));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00ba, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        l.a(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c1, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00de, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00df, code lost:
        if (r8 != null) goto L_0x00e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        r8.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00e5, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        l.a(r8);
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:27:0x00b5, B:39:0x00c8, B:55:0x00e1] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00cf A[SYNTHETIC, Splitter:B:42:0x00cf] */
    public final synchronized void a(List<r14> list, boolean z) {
        SQLiteDatabase sQLiteDatabase;
        Throwable th;
        if (list.size() != 0) {
            if (i04.q()) {
                l.e("Delete " + list.size() + " events, important:" + z);
            }
            StringBuilder sb = new StringBuilder(list.size() * 3);
            sb.append("event_id in (");
            int i2 = 0;
            int size = list.size();
            for (r14 r14 : list) {
                sb.append(r14.a);
                if (i2 != size - 1) {
                    sb.append(",");
                }
                i2++;
            }
            sb.append(")");
            try {
                sQLiteDatabase = c(z);
                try {
                    sQLiteDatabase.beginTransaction();
                    int delete = sQLiteDatabase.delete("events", sb.toString(), (String[]) null);
                    if (i04.q()) {
                        l.e("delete " + size + " event " + sb.toString() + ", success delete:" + delete);
                    }
                    this.f -= delete;
                    sQLiteDatabase.setTransactionSuccessful();
                    d();
                    if (sQLiteDatabase != null) {
                        sQLiteDatabase.endTransaction();
                        return;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    l.a(th);
                    if (sQLiteDatabase != null) {
                        try {
                            sQLiteDatabase.endTransaction();
                            return;
                        } catch (Throwable th3) {
                            l.a(th3);
                            return;
                        }
                    }
                    return;
                }
            } catch (Throwable th4) {
                Throwable th5 = th4;
                sQLiteDatabase = null;
                th = th5;
                l.a(th);
                if (sQLiteDatabase != null) {
                }
                return;
            }
        } else {
            return;
        }
        throw th;
    }

    @DexIgnore
    public void a(List<r14> list, boolean z, boolean z2) {
        z14 z14 = this.c;
        if (z14 != null) {
            z14.a(new j14(this, list, z, z2));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0062 A[SYNTHETIC, Splitter:B:18:0x0062] */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    public final void a(boolean z) {
        SQLiteDatabase sQLiteDatabase;
        try {
            sQLiteDatabase = c(z);
            try {
                sQLiteDatabase.beginTransaction();
                ContentValues contentValues = new ContentValues();
                contentValues.put("status", 1);
                int update = sQLiteDatabase.update("events", contentValues, "status=?", new String[]{Long.toString(2)});
                if (i04.q()) {
                    u14 u14 = l;
                    u14.e("update " + update + " unsent events.");
                }
                sQLiteDatabase.setTransactionSuccessful();
                if (sQLiteDatabase != null) {
                    try {
                        sQLiteDatabase.endTransaction();
                        return;
                    } catch (Throwable th) {
                        l.a(th);
                        return;
                    }
                } else {
                    return;
                }
            } catch (Throwable th2) {
                th = th2;
                try {
                    l.a(th);
                    if (sQLiteDatabase == null) {
                        sQLiteDatabase.endTransaction();
                        return;
                    }
                    return;
                } catch (Throwable th3) {
                    l.a(th3);
                }
            }
        } catch (Throwable th4) {
            th = th4;
            sQLiteDatabase = null;
            l.a(th);
            if (sQLiteDatabase == null) {
            }
        }
        throw th;
    }

    @DexIgnore
    public final int b(boolean z) {
        return !z ? i04.i() : i04.g();
    }

    @DexIgnore
    public void b() {
        if (i04.s()) {
            try {
                this.c.a(new k14(this));
            } catch (Throwable th) {
                l.a(th);
            }
        }
    }

    @DexIgnore
    public final void b(int i2, boolean z) {
        if (i2 == -1) {
            i2 = !z ? e() : f();
        }
        if (i2 > 0) {
            int l2 = i04.l() * 60 * i04.k();
            if (i2 > l2 && l2 > 0) {
                i2 = l2;
            }
            int a2 = i04.a();
            int i3 = i2 / a2;
            int i4 = i2 % a2;
            if (i04.q()) {
                u14 u14 = l;
                u14.e("sentStoreEventsByDb sendNumbers=" + i2 + ",important=" + z + ",maxSendNumPerFor1Period=" + l2 + ",maxCount=" + i3 + ",restNumbers=" + i4);
            }
            for (int i5 = 0; i5 < i3; i5++) {
                a(a2, z);
            }
            if (i4 > 0) {
                a(i4, z);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(6:45|(2:47|48)|49|50|52|53) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:52:0x0102 */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e0 A[SYNTHETIC, Splitter:B:36:0x00e0] */
    public final synchronized void b(o24 o24) {
        Cursor cursor;
        boolean z;
        long j2;
        try {
            String a2 = o24.a();
            String a3 = f24.a(a2);
            ContentValues contentValues = new ContentValues();
            contentValues.put("content", o24.b.toString());
            contentValues.put("md5sum", a3);
            o24.c = a3;
            contentValues.put("version", Integer.valueOf(o24.d));
            cursor = this.a.getReadableDatabase().query("config", (String[]) null, (String) null, (String[]) null, (String) null, (String) null, (String) null);
            while (true) {
                try {
                    if (cursor.moveToNext()) {
                        if (cursor.getInt(0) == o24.a) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        l.a(th);
                        try {
                            return;
                        } catch (Exception unused) {
                            return;
                        }
                    } finally {
                        if (cursor != null) {
                            cursor.close();
                        }
                        this.a.getWritableDatabase().endTransaction();
                    }
                }
            }
            this.a.getWritableDatabase().beginTransaction();
            if (true == z) {
                j2 = (long) this.a.getWritableDatabase().update("config", contentValues, "type=?", new String[]{Integer.toString(o24.a)});
            } else {
                contentValues.put("type", Integer.valueOf(o24.a));
                j2 = this.a.getWritableDatabase().insert("config", (String) null, contentValues);
            }
            if (j2 == -1) {
                l.c("Failed to store cfg:" + a2);
            } else {
                l.a((Object) "Sucessed to store cfg:" + a2);
            }
            this.a.getWritableDatabase().setTransactionSuccessful();
            if (cursor != null) {
                cursor.close();
            }
            try {
                this.a.getWritableDatabase().endTransaction();
                return;
            } catch (Exception unused2) {
                return;
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            l.a(th);
            return;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0089, code lost:
        return;
     */
    @DexIgnore
    public final synchronized void b(p04 p04, q24 q24, boolean z, boolean z2) {
        if (i04.j() > 0) {
            if (i04.J > 0 && !z) {
                if (!z2) {
                    if (i04.J > 0) {
                        if (i04.q()) {
                            u14 u14 = l;
                            u14.e("cacheEventsInMemory.size():" + this.i.size() + ",numEventsCachedInMemory:" + i04.J + ",numStoredEvents:" + this.f);
                            u14 u142 = l;
                            StringBuilder sb = new StringBuilder("cache event:");
                            sb.append(p04.f());
                            u142.e(sb.toString());
                        }
                        this.i.put(p04, "");
                        if (this.i.size() >= i04.J) {
                            g();
                        }
                        if (q24 != null) {
                            if (this.i.size() > 0) {
                                g();
                            }
                            q24.a();
                        }
                    }
                }
            }
            a(p04, q24, z);
        }
    }

    @DexIgnore
    public final void b(List<r14> list, int i2, boolean z) {
        Cursor cursor = null;
        try {
            Cursor query = d(z).query("events", (String[]) null, "status=?", new String[]{Integer.toString(1)}, (String) null, (String) null, (String) null, Integer.toString(i2));
            while (query.moveToNext()) {
                long j2 = query.getLong(0);
                String string = query.getString(1);
                if (!i04.v) {
                    string = k24.a(string);
                }
                String str = string;
                int i3 = query.getInt(2);
                int i4 = query.getInt(3);
                r14 r14 = new r14(j2, str, i3, i4);
                if (i04.q()) {
                    u14 u14 = l;
                    u14.e("peek event, id=" + j2 + ",send_count=" + i4 + ",timestamp=" + query.getLong(4));
                }
                list.add(r14);
            }
            if (query != null) {
                query.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final SQLiteDatabase c(boolean z) {
        return (!z ? this.a : this.b).getWritableDatabase();
    }

    @DexIgnore
    public void c() {
        Cursor cursor = null;
        try {
            Cursor query = this.a.getReadableDatabase().query("config", (String[]) null, (String) null, (String[]) null, (String) null, (String) null, (String) null);
            while (query.moveToNext()) {
                int i2 = query.getInt(0);
                String string = query.getString(1);
                query.getString(2);
                int i3 = query.getInt(3);
                o24 o24 = new o24(i2);
                o24.a = i2;
                o24.b = new JSONObject(string);
                o24.d = i3;
                i04.a(m, o24);
            }
            if (query != null) {
                query.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final SQLiteDatabase d(boolean z) {
        return (!z ? this.a : this.b).getReadableDatabase();
    }

    @DexIgnore
    public final void d() {
        this.f = e() + f();
    }

    @DexIgnore
    public final int e() {
        return (int) DatabaseUtils.queryNumEntries(this.a.getReadableDatabase(), "events");
    }

    @DexIgnore
    public final int f() {
        return (int) DatabaseUtils.queryNumEntries(this.b.getReadableDatabase(), "events");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00cb, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r2 = l;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0120, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0121, code lost:
        if (r3 != null) goto L_0x0123;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        r3.endTransaction();
        d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x012a, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        l.a(r2);
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:26:0x00c4, B:35:0x00d6, B:50:0x0123] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00dd A[SYNTHETIC, Splitter:B:38:0x00dd] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00f1 A[Catch:{ all -> 0x0120, all -> 0x012a, all -> 0x00cb }] */
    public final void g() {
        SQLiteDatabase sQLiteDatabase;
        if (!this.j) {
            synchronized (this.i) {
                if (this.i.size() != 0) {
                    this.j = true;
                    if (i04.q()) {
                        l.e("insert " + this.i.size() + " events ,numEventsCachedInMemory:" + i04.J + ",numStoredEvents:" + this.f);
                    }
                    try {
                        sQLiteDatabase = this.a.getWritableDatabase();
                        try {
                            sQLiteDatabase.beginTransaction();
                            Iterator<Map.Entry<p04, String>> it = this.i.entrySet().iterator();
                            while (it.hasNext()) {
                                p04 p04 = (p04) it.next().getKey();
                                ContentValues contentValues = new ContentValues();
                                String f2 = p04.f();
                                if (i04.q()) {
                                    l.e("insert content:" + f2);
                                }
                                contentValues.put("content", k24.b(f2));
                                contentValues.put("send_count", "0");
                                contentValues.put("status", Integer.toString(1));
                                contentValues.put("timestamp", Long.valueOf(p04.b()));
                                sQLiteDatabase.insert("events", (String) null, contentValues);
                                it.remove();
                            }
                            sQLiteDatabase.setTransactionSuccessful();
                            if (sQLiteDatabase != null) {
                                sQLiteDatabase.endTransaction();
                                d();
                            }
                        } catch (Throwable th) {
                            th = th;
                            l.a(th);
                            if (sQLiteDatabase != null) {
                                try {
                                    sQLiteDatabase.endTransaction();
                                    d();
                                } catch (Throwable th2) {
                                    th = th2;
                                    u14 u14 = l;
                                    u14.a(th);
                                    this.j = false;
                                    if (i04.q()) {
                                    }
                                    return;
                                }
                            }
                            this.j = false;
                            if (i04.q()) {
                            }
                            return;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        sQLiteDatabase = null;
                        l.a(th);
                        if (sQLiteDatabase != null) {
                        }
                        this.j = false;
                        if (i04.q()) {
                        }
                        return;
                    }
                    this.j = false;
                    if (i04.q()) {
                        l.e("after insert, cacheEventsInMemory.size():" + this.i.size() + ",numEventsCachedInMemory:" + i04.J + ",numStoredEvents:" + this.f);
                    }
                } else {
                    return;
                }
            }
        } else {
            return;
        }
        throw th;
    }

    @DexIgnore
    public final void h() {
        Cursor cursor = null;
        try {
            Cursor query = this.a.getReadableDatabase().query("keyvalues", (String[]) null, (String) null, (String[]) null, (String) null, (String) null, (String) null);
            while (query.moveToNext()) {
                this.k.put(query.getString(0), query.getString(1));
            }
            if (query != null) {
                query.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }
}
