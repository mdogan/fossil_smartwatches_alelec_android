package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dy1 implements Runnable {
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ PowerManager.WakeLock f; // = ((PowerManager) a().getSystemService("power")).newWakeLock(1, "fiid-sync");
    @DexIgnore
    public /* final */ FirebaseInstanceId g;
    @DexIgnore
    public /* final */ gy1 h;

    @DexIgnore
    public dy1(FirebaseInstanceId firebaseInstanceId, sx1 sx1, gy1 gy1, long j) {
        this.g = firebaseInstanceId;
        this.h = gy1;
        this.e = j;
        this.f.setReferenceCounted(false);
    }

    @DexIgnore
    public final Context a() {
        return this.g.f().a();
    }

    @DexIgnore
    public final boolean b() {
        cy1 g2 = this.g.g();
        if (!this.g.l() && !this.g.a(g2)) {
            return true;
        }
        try {
            String h2 = this.g.h();
            if (h2 == null) {
                Log.e("FirebaseInstanceId", "Token retrieval failed: null");
                return false;
            }
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "Token successfully retrieved");
            }
            if (g2 == null || (g2 != null && !h2.equals(g2.a))) {
                Context a = a();
                Intent intent = new Intent("com.google.firebase.messaging.NEW_TOKEN");
                intent.putExtra("token", h2);
                ay1.c(a, intent);
                ay1.b(a, new Intent("com.google.firebase.iid.TOKEN_REFRESH"));
            }
            return true;
        } catch (IOException | SecurityException e2) {
            String valueOf = String.valueOf(e2.getMessage());
            Log.e("FirebaseInstanceId", valueOf.length() != 0 ? "Token retrieval failed: ".concat(valueOf) : new String("Token retrieval failed: "));
            return false;
        }
    }

    @DexIgnore
    public final boolean c() {
        ConnectivityManager connectivityManager = (ConnectivityManager) a().getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @DexIgnore
    @SuppressLint({"Wakelock"})
    public final void run() {
        try {
            if (ay1.b().a(a())) {
                this.f.acquire();
            }
            this.g.a(true);
            if (!this.g.j()) {
                this.g.a(false);
            } else if (!ay1.b().b(a()) || c()) {
                if (!b() || !this.h.a(this.g)) {
                    this.g.a(this.e);
                } else {
                    this.g.a(false);
                }
                if (ay1.b().a(a())) {
                    this.f.release();
                }
            } else {
                new ey1(this).a();
                if (ay1.b().a(a())) {
                    this.f.release();
                }
            }
        } finally {
            if (ay1.b().a(a())) {
                this.f.release();
            }
        }
    }
}
