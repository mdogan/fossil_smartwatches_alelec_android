package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mi1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ig1 e;
    @DexIgnore
    public /* final */ /* synthetic */ sl1 f;
    @DexIgnore
    public /* final */ /* synthetic */ ai1 g;

    @DexIgnore
    public mi1(ai1 ai1, ig1 ig1, sl1 sl1) {
        this.g = ai1;
        this.e = ig1;
        this.f = sl1;
    }

    @DexIgnore
    public final void run() {
        ig1 b = this.g.b(this.e, this.f);
        this.g.e.y();
        this.g.e.a(b, this.f);
    }
}
