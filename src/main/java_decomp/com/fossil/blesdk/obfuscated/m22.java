package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m22 implements n22 {
    @DexIgnore
    public int a() {
        return 4;
    }

    @DexIgnore
    public void a(o22 o22) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!o22.i()) {
                break;
            }
            a(o22.c(), sb);
            o22.f++;
            if (sb.length() >= 4) {
                o22.a(a((CharSequence) sb, 0));
                sb.delete(0, 4);
                if (q22.a(o22.d(), o22.f, a()) != a()) {
                    o22.b(0);
                    break;
                }
            }
        }
        sb.append(31);
        a(o22, (CharSequence) sb);
    }

    @DexIgnore
    public static void a(o22 o22, CharSequence charSequence) {
        try {
            int length = charSequence.length();
            if (length != 0) {
                boolean z = true;
                if (length == 1) {
                    o22.l();
                    int a = o22.g().a() - o22.a();
                    if (o22.f() == 0 && a <= 2) {
                        o22.b(0);
                        return;
                    }
                }
                if (length <= 4) {
                    int i = length - 1;
                    String a2 = a(charSequence, 0);
                    if (!(!o22.i()) || i > 2) {
                        z = false;
                    }
                    if (i <= 2) {
                        o22.c(o22.a() + i);
                        if (o22.g().a() - o22.a() >= 3) {
                            o22.c(o22.a() + a2.length());
                            z = false;
                        }
                    }
                    if (z) {
                        o22.k();
                        o22.f -= i;
                    } else {
                        o22.a(a2);
                    }
                    o22.b(0);
                    return;
                }
                throw new IllegalStateException("Count must not exceed 4");
            }
        } finally {
            o22.b(0);
        }
    }

    @DexIgnore
    public static void a(char c, StringBuilder sb) {
        if (c >= ' ' && c <= '?') {
            sb.append(c);
        } else if (c < '@' || c > '^') {
            q22.a(c);
            throw null;
        } else {
            sb.append((char) (c - '@'));
        }
    }

    @DexIgnore
    public static String a(CharSequence charSequence, int i) {
        int length = charSequence.length() - i;
        if (length != 0) {
            char charAt = charSequence.charAt(i);
            char c = 0;
            char charAt2 = length >= 2 ? charSequence.charAt(i + 1) : 0;
            char charAt3 = length >= 3 ? charSequence.charAt(i + 2) : 0;
            if (length >= 4) {
                c = charSequence.charAt(i + 3);
            }
            int i2 = (charAt << 18) + (charAt2 << 12) + (charAt3 << 6) + c;
            char c2 = (char) ((i2 >> 8) & 255);
            char c3 = (char) (i2 & 255);
            StringBuilder sb = new StringBuilder(3);
            sb.append((char) ((i2 >> 16) & 255));
            if (length >= 2) {
                sb.append(c2);
            }
            if (length >= 3) {
                sb.append(c3);
            }
            return sb.toString();
        }
        throw new IllegalStateException("StringBuilder must not be empty");
    }
}
