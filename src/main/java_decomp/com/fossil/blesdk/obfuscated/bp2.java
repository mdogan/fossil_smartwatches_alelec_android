package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.sr4;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.gson.AlarmDeserializer;
import com.portfolio.platform.gson.DianaPresetDeserializer;
import com.portfolio.platform.gson.DianaRecommendPresetDeserializer;
import com.portfolio.platform.gson.HybridPresetDeserializer;
import com.portfolio.platform.gson.HybridRecommendPresetDeserializer;
import com.portfolio.platform.helper.GsonConvertDate;
import com.portfolio.platform.helper.GsonConvertDateTime;
import java.io.File;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.joda.time.DateTime;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bp2 {
    @DexIgnore
    public static /* final */ Retrofit.b a;
    @DexIgnore
    public static Retrofit b;
    @DexIgnore
    public static Cache c;
    @DexIgnore
    public static /* final */ bt3 d;
    @DexIgnore
    public static /* final */ OkHttpClient.b e;
    @DexIgnore
    public static String f;
    @DexIgnore
    public static /* final */ bp2 g; // = new bp2();

    /*
    static {
        Retrofit.b bVar = new Retrofit.b();
        sz1 sz1 = new sz1();
        sz1.b(new kk2());
        sz1.a((pz1) new kk2());
        sz1.a(Date.class, new GsonConvertDate());
        sz1.a(DianaPreset.class, new DianaPresetDeserializer());
        sz1.a(DianaRecommendPreset.class, new DianaRecommendPresetDeserializer());
        sz1.a(HybridPreset.class, new HybridPresetDeserializer());
        sz1.a(HybridRecommendPreset.class, new HybridRecommendPresetDeserializer());
        sz1.a(DateTime.class, new GsonConvertDateTime());
        sz1.a(Alarm.class, new AlarmDeserializer());
        bVar.a((sr4.a) GsonConverterFactory.a(sz1.a()));
        a = bVar;
        bt3 bt3 = new bt3();
        bt3.a(wd4.a((Object) "release", (Object) "release") ? HttpLoggingInterceptor.Level.BASIC : HttpLoggingInterceptor.Level.BODY);
        d = bt3;
        OkHttpClient.b bVar2 = new OkHttpClient.b();
        bVar2.a((Interceptor) d);
        e = bVar2;
    }
    */

    @DexIgnore
    public final void a(String str) {
        wd4.b(str, "baseUrl");
        if (!TextUtils.equals(f, str)) {
            f = str;
            Retrofit.b bVar = a;
            String str2 = f;
            if (str2 != null) {
                bVar.a(str2);
                b = a.a();
                return;
            }
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(tl4 tl4) {
        wd4.b(tl4, "authenticator");
        e.a(tl4);
        a.a(e.a());
        b = a.a();
    }

    @DexIgnore
    public final void a(File file) {
        wd4.b(file, "cacheDir");
        if (c == null) {
            File file2 = new File(file.getAbsolutePath(), "cacheResponse");
            if (!file2.exists()) {
                file2.mkdir();
            }
            c = new Cache(file2, 10485760);
        }
        e.a(c);
    }

    @DexIgnore
    public final void a() {
        Cache cache = c;
        if (cache != null) {
            cache.y();
        }
    }

    @DexIgnore
    public final void a(Interceptor interceptor) {
        e.b(5, TimeUnit.SECONDS);
        e.a(5, TimeUnit.SECONDS);
        e.c(5, TimeUnit.SECONDS);
        e.b().clear();
        e.a((Interceptor) d);
        if (interceptor != null && !e.b().contains(interceptor)) {
            e.a(interceptor);
        }
        a.a(e.a());
        b = a.a();
    }

    @DexIgnore
    public final <S> S a(Class<S> cls) {
        wd4.b(cls, "serviceClass");
        Retrofit retrofit3 = b;
        if (retrofit3 != null) {
            return retrofit3.a(cls);
        }
        wd4.a();
        throw null;
    }
}
