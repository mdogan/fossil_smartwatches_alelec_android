package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mo1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ xn1 e;
    @DexIgnore
    public /* final */ /* synthetic */ lo1 f;

    @DexIgnore
    public mo1(lo1 lo1, xn1 xn1) {
        this.f = lo1;
        this.e = xn1;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.f.b) {
            if (this.f.c != null) {
                this.f.c.onFailure(this.e.a());
            }
        }
    }
}
