package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zw3 extends lv3 {
    @DexIgnore
    public /* final */ dv3 e;
    @DexIgnore
    public /* final */ xo4 f;

    @DexIgnore
    public zw3(dv3 dv3, xo4 xo4) {
        this.e = dv3;
        this.f = xo4;
    }

    @DexIgnore
    public gv3 A() {
        String a = this.e.a(GraphRequest.CONTENT_TYPE_HEADER);
        if (a != null) {
            return gv3.a(a);
        }
        return null;
    }

    @DexIgnore
    public xo4 B() {
        return this.f;
    }

    @DexIgnore
    public long z() {
        return yw3.a(this.e);
    }
}
