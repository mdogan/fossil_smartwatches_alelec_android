package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y24 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ l04 f;
    @DexIgnore
    public /* final */ /* synthetic */ n04 g;

    @DexIgnore
    public y24(Context context, l04 l04, n04 n04) {
        this.e = context;
        this.f = l04;
        this.g = n04;
    }

    @DexIgnore
    public final void run() {
        try {
            m04 m04 = new m04(this.e, k04.a(this.e, false, this.f), this.g.a, this.f);
            m04.g().c = this.g.c;
            new d14(m04).a();
        } catch (Throwable th) {
            k04.m.a(th);
            k04.a(this.e, th);
        }
    }
}
