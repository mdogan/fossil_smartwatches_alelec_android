package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qf4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ ke4 b;

    @DexIgnore
    public qf4(String str, ke4 ke4) {
        wd4.b(str, "value");
        wd4.b(ke4, "range");
        this.a = str;
        this.b = ke4;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof qf4)) {
            return false;
        }
        qf4 qf4 = (qf4) obj;
        return wd4.a((Object) this.a, (Object) qf4.a) && wd4.a((Object) this.b, (Object) qf4.b);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        ke4 ke4 = this.b;
        if (ke4 != null) {
            i = ke4.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return "MatchGroup(value=" + this.a + ", range=" + this.b + ")";
    }
}
