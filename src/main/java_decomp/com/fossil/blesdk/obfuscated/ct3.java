package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ct3 extends ya {
    @DexIgnore
    public static /* final */ a g; // = new a((rd4) null);
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public HashMap f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ct3 a(String str) {
            wd4.b(str, "title");
            ct3 ct3 = new ct3();
            Bundle bundle = new Bundle();
            bundle.putString("DIALOG_TITLE", str);
            ct3.setArguments(bundle);
            return ct3;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProgressDialogFragment", "create with arguments " + getArguments());
        Bundle arguments = getArguments();
        if (arguments != null) {
            String string = arguments.getString("DIALOG_TITLE");
            if (string == null) {
                string = "";
            }
            this.e = string;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_loading_dialog, viewGroup, false);
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(R.id.tv_title);
        if (TextUtils.isEmpty(this.e)) {
            wd4.a((Object) flexibleTextView, "mTvTitle");
            flexibleTextView.setVisibility(8);
        } else {
            wd4.a((Object) flexibleTextView, "mTvTitle");
            flexibleTextView.setVisibility(0);
            flexibleTextView.setText(this.e);
        }
        Dialog dialog = getDialog();
        wd4.a((Object) dialog, "dialog");
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
    }
}
