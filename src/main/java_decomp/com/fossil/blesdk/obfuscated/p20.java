package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.model.file.AssetFile;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p20 extends t20<AssetFile[], byte[]> {
    @DexIgnore
    public static /* final */ l20<AssetFile[]>[] a; // = {new a(), new b()};
    @DexIgnore
    public static /* final */ m20<byte[]>[] b; // = {new c(FileType.ASSET), new d(FileType.ASSET)};
    @DexIgnore
    public static /* final */ p20 c; // = new p20();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends r20<AssetFile[]> {
        @DexIgnore
        public byte[] a(AssetFile[] assetFileArr) {
            wd4.b(assetFileArr, "entries");
            return p20.c.a(assetFileArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends s20<AssetFile[]> {
        @DexIgnore
        public byte[] a(AssetFile[] assetFileArr) {
            wd4.b(assetFileArr, "entries");
            return p20.c.a(assetFileArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends u20<byte[]> {
        @DexIgnore
        public c(FileType fileType) {
            super(fileType);
        }

        @DexIgnore
        public byte[] b(byte[] bArr) {
            wd4.b(bArr, "data");
            return bArr;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends v20<byte[]> {
        @DexIgnore
        public d(FileType fileType) {
            super(fileType);
        }

        @DexIgnore
        public byte[] b(byte[] bArr) {
            wd4.b(bArr, "data");
            return bArr;
        }
    }

    @DexIgnore
    public l20<AssetFile[]>[] a() {
        return a;
    }

    @DexIgnore
    public m20<byte[]>[] b() {
        return b;
    }

    @DexIgnore
    public final byte[] a(AssetFile[] assetFileArr) {
        wd4.b(assetFileArr, "entries");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (AssetFile entryData$blesdk_productionRelease : assetFileArr) {
            byteArrayOutputStream.write(entryData$blesdk_productionRelease.getEntryData$blesdk_productionRelease());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        wd4.a((Object) byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }
}
