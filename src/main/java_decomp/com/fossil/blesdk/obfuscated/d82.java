package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* compiled from: lambda */
public final /* synthetic */ class d82 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon14 e;
    @DexIgnore
    private /* final */ /* synthetic */ RecommendedPreset f;

    @DexIgnore
    public /* synthetic */ d82(PresetRepository.Anon14 anon14, RecommendedPreset recommendedPreset) {
        this.e = anon14;
        this.f = recommendedPreset;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f);
    }
}
