package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bg3 implements Factory<SleepDetailPresenter> {
    @DexIgnore
    public static SleepDetailPresenter a(xf3 xf3, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        return new SleepDetailPresenter(xf3, sleepSummariesRepository, sleepSessionsRepository);
    }
}
