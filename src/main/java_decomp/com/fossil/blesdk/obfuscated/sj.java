package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import androidx.work.ExistingWorkPolicy;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sj extends jj {
    @DexIgnore
    public static /* final */ String j; // = ej.a("WorkContinuationImpl");
    @DexIgnore
    public /* final */ uj a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ExistingWorkPolicy c;
    @DexIgnore
    public /* final */ List<? extends lj> d;
    @DexIgnore
    public /* final */ List<String> e;
    @DexIgnore
    public /* final */ List<String> f;
    @DexIgnore
    public /* final */ List<sj> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public gj i;

    @DexIgnore
    public sj(uj ujVar, String str, ExistingWorkPolicy existingWorkPolicy, List<? extends lj> list) {
        this(ujVar, str, existingWorkPolicy, list, (List<sj>) null);
    }

    @DexIgnore
    public gj a() {
        if (!this.h) {
            ql qlVar = new ql(this);
            this.a.h().a(qlVar);
            this.i = qlVar.b();
        } else {
            ej.a().e(j, String.format("Already enqueued work ids (%s)", new Object[]{TextUtils.join(", ", this.e)}), new Throwable[0]);
        }
        return this.i;
    }

    @DexIgnore
    public ExistingWorkPolicy b() {
        return this.c;
    }

    @DexIgnore
    public List<String> c() {
        return this.e;
    }

    @DexIgnore
    public String d() {
        return this.b;
    }

    @DexIgnore
    public List<sj> e() {
        return this.g;
    }

    @DexIgnore
    public List<? extends lj> f() {
        return this.d;
    }

    @DexIgnore
    public uj g() {
        return this.a;
    }

    @DexIgnore
    public boolean h() {
        return a(this, new HashSet());
    }

    @DexIgnore
    public boolean i() {
        return this.h;
    }

    @DexIgnore
    public void j() {
        this.h = true;
    }

    @DexIgnore
    public sj(uj ujVar, String str, ExistingWorkPolicy existingWorkPolicy, List<? extends lj> list, List<sj> list2) {
        this.a = ujVar;
        this.b = str;
        this.c = existingWorkPolicy;
        this.d = list;
        this.g = list2;
        this.e = new ArrayList(this.d.size());
        this.f = new ArrayList();
        if (list2 != null) {
            for (sj sjVar : list2) {
                this.f.addAll(sjVar.f);
            }
        }
        for (int i2 = 0; i2 < list.size(); i2++) {
            String b2 = ((lj) list.get(i2)).b();
            this.e.add(b2);
            this.f.add(b2);
        }
    }

    @DexIgnore
    public static boolean a(sj sjVar, Set<String> set) {
        set.addAll(sjVar.c());
        Set<String> a2 = a(sjVar);
        for (String contains : set) {
            if (a2.contains(contains)) {
                return true;
            }
        }
        List<sj> e2 = sjVar.e();
        if (e2 != null && !e2.isEmpty()) {
            for (sj a3 : e2) {
                if (a(a3, set)) {
                    return true;
                }
            }
        }
        set.removeAll(sjVar.c());
        return false;
    }

    @DexIgnore
    public static Set<String> a(sj sjVar) {
        HashSet hashSet = new HashSet();
        List<sj> e2 = sjVar.e();
        if (e2 != null && !e2.isEmpty()) {
            for (sj c2 : e2) {
                hashSet.addAll(c2.c());
            }
        }
        return hashSet;
    }
}
