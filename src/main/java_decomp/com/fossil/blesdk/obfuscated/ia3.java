package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ia3 implements Factory<CaloriesOverviewDayPresenter> {
    @DexIgnore
    public static CaloriesOverviewDayPresenter a(ga3 ga3, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, WorkoutSessionRepository workoutSessionRepository) {
        return new CaloriesOverviewDayPresenter(ga3, summariesRepository, activitiesRepository, workoutSessionRepository);
    }
}
