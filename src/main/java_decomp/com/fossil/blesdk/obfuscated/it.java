package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class it implements no<Bitmap, Bitmap> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements bq<Bitmap> {
        @DexIgnore
        public /* final */ Bitmap e;

        @DexIgnore
        public a(Bitmap bitmap) {
            this.e = bitmap;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public int b() {
            return vw.a(this.e);
        }

        @DexIgnore
        public Class<Bitmap> c() {
            return Bitmap.class;
        }

        @DexIgnore
        public Bitmap get() {
            return this.e;
        }
    }

    @DexIgnore
    public boolean a(Bitmap bitmap, mo moVar) {
        return true;
    }

    @DexIgnore
    public bq<Bitmap> a(Bitmap bitmap, int i, int i2, mo moVar) {
        return new a(bitmap);
    }
}
