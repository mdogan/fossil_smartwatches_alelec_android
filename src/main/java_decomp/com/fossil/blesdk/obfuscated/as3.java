package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class as3 implements MembersInjector<yr3> {
    @DexIgnore
    public static void a(yr3 yr3, DeviceRepository deviceRepository) {
        yr3.h = deviceRepository;
    }

    @DexIgnore
    public static void a(yr3 yr3, NotificationSettingsDatabase notificationSettingsDatabase) {
        yr3.i = notificationSettingsDatabase;
    }
}
