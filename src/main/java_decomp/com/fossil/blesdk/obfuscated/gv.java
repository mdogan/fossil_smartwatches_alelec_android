package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gv {
    @DexIgnore
    public /* final */ List<a<?>> a; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public /* final */ Class<T> a;
        @DexIgnore
        public /* final */ io<T> b;

        @DexIgnore
        public a(Class<T> cls, io<T> ioVar) {
            this.a = cls;
            this.b = ioVar;
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    @DexIgnore
    public synchronized <T> io<T> a(Class<T> cls) {
        for (a next : this.a) {
            if (next.a(cls)) {
                return next.b;
            }
        }
        return null;
    }

    @DexIgnore
    public synchronized <T> void a(Class<T> cls, io<T> ioVar) {
        this.a.add(new a(cls, ioVar));
    }
}
