package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ti4 extends xi4 implements yg4 {
    @DexIgnore
    public /* final */ boolean f; // = j();

    @DexIgnore
    public ti4(ri4 ri4) {
        super(true);
        a(ri4);
    }

    @DexIgnore
    public boolean b() {
        return this.f;
    }

    @DexIgnore
    public boolean c() {
        return true;
    }

    @DexIgnore
    public final boolean j() {
        ug4 ug4 = this.parentHandle;
        if (!(ug4 instanceof vg4)) {
            ug4 = null;
        }
        vg4 vg4 = (vg4) ug4;
        if (vg4 != null) {
            xi4 xi4 = (xi4) vg4.h;
            if (xi4 != null) {
                while (!xi4.b()) {
                    ug4 ug42 = xi4.parentHandle;
                    if (!(ug42 instanceof vg4)) {
                        ug42 = null;
                    }
                    vg4 vg42 = (vg4) ug42;
                    if (vg42 != null) {
                        xi4 = (xi4) vg42.h;
                        if (xi4 == null) {
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }
}
