package com.fossil.blesdk.obfuscated;

import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hi1 implements Callable<List<nl1>> {
    @DexIgnore
    public /* final */ /* synthetic */ sl1 e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ ai1 h;

    @DexIgnore
    public hi1(ai1 ai1, sl1 sl1, String str, String str2) {
        this.h = ai1;
        this.e = sl1;
        this.f = str;
        this.g = str2;
    }

    @DexIgnore
    public final /* synthetic */ Object call() throws Exception {
        this.h.e.y();
        return this.h.e.l().a(this.e.e, this.f, this.g);
    }
}
