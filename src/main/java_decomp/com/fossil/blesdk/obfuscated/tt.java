package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.blesdk.obfuscated.Cdo;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tt implements no<ByteBuffer, vt> {
    @DexIgnore
    public static /* final */ a f; // = new a();
    @DexIgnore
    public static /* final */ b g; // = new b();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ List<ImageHeaderParser> b;
    @DexIgnore
    public /* final */ b c;
    @DexIgnore
    public /* final */ a d;
    @DexIgnore
    public /* final */ ut e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public Cdo a(Cdo.a aVar, fo foVar, ByteBuffer byteBuffer, int i) {
            return new ho(aVar, foVar, byteBuffer, i);
        }
    }

    @DexIgnore
    public tt(Context context, List<ImageHeaderParser> list, kq kqVar, hq hqVar) {
        this(context, list, kqVar, hqVar, g, f);
    }

    @DexIgnore
    public tt(Context context, List<ImageHeaderParser> list, kq kqVar, hq hqVar, b bVar, a aVar) {
        this.a = context.getApplicationContext();
        this.b = list;
        this.d = aVar;
        this.e = new ut(kqVar, hqVar);
        this.c = bVar;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Queue<go> a; // = vw.a(0);

        @DexIgnore
        public synchronized go a(ByteBuffer byteBuffer) {
            go poll;
            poll = this.a.poll();
            if (poll == null) {
                poll = new go();
            }
            poll.a(byteBuffer);
            return poll;
        }

        @DexIgnore
        public synchronized void a(go goVar) {
            goVar.a();
            this.a.offer(goVar);
        }
    }

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer, mo moVar) throws IOException {
        return !((Boolean) moVar.a(bu.b)).booleanValue() && jo.a(this.b, byteBuffer) == ImageHeaderParser.ImageType.GIF;
    }

    @DexIgnore
    public xt a(ByteBuffer byteBuffer, int i, int i2, mo moVar) {
        go a2 = this.c.a(byteBuffer);
        try {
            return a(byteBuffer, i, i2, a2, moVar);
        } finally {
            this.c.a(a2);
        }
    }

    @DexIgnore
    public final xt a(ByteBuffer byteBuffer, int i, int i2, go goVar, mo moVar) {
        Bitmap.Config config;
        long a2 = qw.a();
        try {
            fo c2 = goVar.c();
            if (c2.b() > 0) {
                if (c2.c() == 0) {
                    if (moVar.a(bu.a) == DecodeFormat.PREFER_RGB_565) {
                        config = Bitmap.Config.RGB_565;
                    } else {
                        config = Bitmap.Config.ARGB_8888;
                    }
                    Bitmap.Config config2 = config;
                    Cdo a3 = this.d.a(this.e, c2, byteBuffer, a(c2, i, i2));
                    a3.a(config2);
                    a3.b();
                    Bitmap a4 = a3.a();
                    if (a4 == null) {
                        if (Log.isLoggable("BufferGifDecoder", 2)) {
                            Log.v("BufferGifDecoder", "Decoded GIF from stream in " + qw.a(a2));
                        }
                        return null;
                    }
                    xt xtVar = new xt(new vt(this.a, a3, ls.a(), i, i2, a4));
                    if (Log.isLoggable("BufferGifDecoder", 2)) {
                        Log.v("BufferGifDecoder", "Decoded GIF from stream in " + qw.a(a2));
                    }
                    return xtVar;
                }
            }
            return null;
        } finally {
            if (Log.isLoggable("BufferGifDecoder", 2)) {
                Log.v("BufferGifDecoder", "Decoded GIF from stream in " + qw.a(a2));
            }
        }
    }

    @DexIgnore
    public static int a(fo foVar, int i, int i2) {
        int i3;
        int min = Math.min(foVar.a() / i2, foVar.d() / i);
        if (min == 0) {
            i3 = 0;
        } else {
            i3 = Integer.highestOneBit(min);
        }
        int max = Math.max(1, i3);
        if (Log.isLoggable("BufferGifDecoder", 2) && max > 1) {
            Log.v("BufferGifDecoder", "Downsampling GIF, sampleSize: " + max + ", target dimens: [" + i + "x" + i2 + "], actual dimens: [" + foVar.d() + "x" + foVar.a() + "]");
        }
        return max;
    }
}
