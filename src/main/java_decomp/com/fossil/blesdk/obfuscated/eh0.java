package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eh0 {
    @DexIgnore
    public /* final */ jg0 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ge0<?> c;

    @DexIgnore
    public eh0(jg0 jg0, int i, ge0<?> ge0) {
        this.a = jg0;
        this.b = i;
        this.c = ge0;
    }
}
