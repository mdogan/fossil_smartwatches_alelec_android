package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class s44 extends ContextWrapper {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public s44(Context context, String str, String str2) {
        super(context);
        this.b = str;
        this.a = str2;
    }

    @DexIgnore
    public File getCacheDir() {
        return new File(super.getCacheDir(), this.a);
    }

    @DexIgnore
    public File getDatabasePath(String str) {
        File file = new File(super.getDatabasePath(str).getParentFile(), this.a);
        file.mkdirs();
        return new File(file, str);
    }

    @DexIgnore
    @TargetApi(8)
    public File getExternalCacheDir() {
        return new File(super.getExternalCacheDir(), this.a);
    }

    @DexIgnore
    @TargetApi(8)
    public File getExternalFilesDir(String str) {
        return new File(super.getExternalFilesDir(str), this.a);
    }

    @DexIgnore
    public File getFilesDir() {
        return new File(super.getFilesDir(), this.a);
    }

    @DexIgnore
    public SharedPreferences getSharedPreferences(String str, int i) {
        return super.getSharedPreferences(this.b + ":" + str, i);
    }

    @DexIgnore
    public SQLiteDatabase openOrCreateDatabase(String str, int i, SQLiteDatabase.CursorFactory cursorFactory) {
        return SQLiteDatabase.openOrCreateDatabase(getDatabasePath(str), cursorFactory);
    }

    @DexIgnore
    @TargetApi(11)
    public SQLiteDatabase openOrCreateDatabase(String str, int i, SQLiteDatabase.CursorFactory cursorFactory, DatabaseErrorHandler databaseErrorHandler) {
        return SQLiteDatabase.openOrCreateDatabase(getDatabasePath(str).getPath(), cursorFactory, databaseErrorHandler);
    }
}
