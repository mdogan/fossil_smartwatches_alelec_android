package com.fossil.blesdk.obfuscated;

import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.exceptions.OnErrorNotImplementedException;
import io.reactivex.exceptions.UndeliverableException;
import io.reactivex.internal.util.ExceptionHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ta4 {
    @DexIgnore
    public static volatile p94<? super Throwable> a;
    @DexIgnore
    public static volatile q94<? super Runnable, ? extends Runnable> b;
    @DexIgnore
    public static volatile q94<? super v84, ? extends v84> c;
    @DexIgnore
    public static volatile q94<? super z84, ? extends z84> d;
    @DexIgnore
    public static volatile q94<? super w84, ? extends w84> e;
    @DexIgnore
    public static volatile q94<? super d94, ? extends d94> f;
    @DexIgnore
    public static volatile q94<? super s84, ? extends s84> g;
    @DexIgnore
    public static volatile n94<? super z84, ? super b94, ? extends b94> h;

    @DexIgnore
    public static boolean a(Throwable th) {
        if (!(th instanceof OnErrorNotImplementedException) && !(th instanceof MissingBackpressureException) && !(th instanceof IllegalStateException) && !(th instanceof NullPointerException) && !(th instanceof IllegalArgumentException) && !(th instanceof CompositeException)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static void b(Throwable th) {
        p94<? super Throwable> p94 = a;
        if (th == null) {
            th = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        } else if (!a(th)) {
            th = new UndeliverableException(th);
        }
        if (p94 != null) {
            try {
                p94.a(th);
                return;
            } catch (Throwable th2) {
                th2.printStackTrace();
                c(th2);
            }
        }
        th.printStackTrace();
        c(th);
    }

    @DexIgnore
    public static void c(Throwable th) {
        Thread currentThread = Thread.currentThread();
        currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, th);
    }

    @DexIgnore
    public static Runnable a(Runnable runnable) {
        q94<? super Runnable, ? extends Runnable> q94 = b;
        if (q94 == null) {
            return runnable;
        }
        a(q94, runnable);
        return runnable;
    }

    @DexIgnore
    public static <T> b94<? super T> a(z84<T> z84, b94<? super T> b94) {
        n94<? super z84, ? super b94, ? extends b94> n94 = h;
        return n94 != null ? (b94) a(n94, z84, b94) : b94;
    }

    @DexIgnore
    public static <T> w84<T> a(w84<T> w84) {
        q94<? super w84, ? extends w84> q94 = e;
        if (q94 == null) {
            return w84;
        }
        a(q94, w84);
        return w84;
    }

    @DexIgnore
    public static <T> v84<T> a(v84<T> v84) {
        q94<? super v84, ? extends v84> q94 = c;
        if (q94 == null) {
            return v84;
        }
        a(q94, v84);
        return v84;
    }

    @DexIgnore
    public static <T> z84<T> a(z84<T> z84) {
        q94<? super z84, ? extends z84> q94 = d;
        if (q94 == null) {
            return z84;
        }
        a(q94, z84);
        return z84;
    }

    @DexIgnore
    public static <T> d94<T> a(d94<T> d94) {
        q94<? super d94, ? extends d94> q94 = f;
        if (q94 == null) {
            return d94;
        }
        a(q94, d94);
        return d94;
    }

    @DexIgnore
    public static s84 a(s84 s84) {
        q94<? super s84, ? extends s84> q94 = g;
        if (q94 == null) {
            return s84;
        }
        a(q94, s84);
        return s84;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [R, T, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public static <T, R> R a(q94<T, R> q94, T r1) {
        try {
            q94.apply(r1);
            return r1;
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }

    @DexIgnore
    public static <T, U, R> R a(n94<T, U, R> n94, T t, U u) {
        try {
            return n94.a(t, u);
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }
}
