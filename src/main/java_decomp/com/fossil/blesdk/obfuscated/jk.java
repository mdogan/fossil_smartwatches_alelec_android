package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.fossil.blesdk.obfuscated.mk;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jk implements mk.a {
    @DexIgnore
    public static /* final */ String d; // = ej.a("WorkConstraintsTracker");
    @DexIgnore
    public /* final */ ik a;
    @DexIgnore
    public /* final */ mk<?>[] b;
    @DexIgnore
    public /* final */ Object c; // = new Object();

    @DexIgnore
    public jk(Context context, am amVar, ik ikVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = ikVar;
        this.b = new mk[]{new kk(applicationContext, amVar), new lk(applicationContext, amVar), new rk(applicationContext, amVar), new nk(applicationContext, amVar), new qk(applicationContext, amVar), new pk(applicationContext, amVar), new ok(applicationContext, amVar)};
    }

    @DexIgnore
    public void a() {
        synchronized (this.c) {
            for (mk<?> a2 : this.b) {
                a2.a();
            }
        }
    }

    @DexIgnore
    public void b(List<String> list) {
        synchronized (this.c) {
            if (this.a != null) {
                this.a.a(list);
            }
        }
    }

    @DexIgnore
    public void c(List<il> list) {
        synchronized (this.c) {
            for (mk<?> a2 : this.b) {
                a2.a((mk.a) null);
            }
            for (mk<?> a3 : this.b) {
                a3.a(list);
            }
            for (mk<?> a4 : this.b) {
                a4.a((mk.a) this);
            }
        }
    }

    @DexIgnore
    public boolean a(String str) {
        synchronized (this.c) {
            for (mk<?> mkVar : this.b) {
                if (mkVar.a(str)) {
                    ej.a().a(d, String.format("Work %s constrained by %s", new Object[]{str, mkVar.getClass().getSimpleName()}), new Throwable[0]);
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public void a(List<String> list) {
        synchronized (this.c) {
            ArrayList arrayList = new ArrayList();
            for (String next : list) {
                if (a(next)) {
                    ej.a().a(d, String.format("Constraints met for %s", new Object[]{next}), new Throwable[0]);
                    arrayList.add(next);
                }
            }
            if (this.a != null) {
                this.a.b(arrayList);
            }
        }
    }
}
