package com.fossil.blesdk.obfuscated;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class hi extends oh {
    @DexIgnore
    public static /* final */ String[] a; // = {"android:visibilityPropagation:visibility", "android:visibilityPropagation:center"};

    @DexIgnore
    public void a(qh qhVar) {
        View view = qhVar.b;
        Integer num = (Integer) qhVar.a.get("android:visibility:visibility");
        if (num == null) {
            num = Integer.valueOf(view.getVisibility());
        }
        qhVar.a.put("android:visibilityPropagation:visibility", num);
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        iArr[0] = iArr[0] + Math.round(view.getTranslationX());
        iArr[0] = iArr[0] + (view.getWidth() / 2);
        iArr[1] = iArr[1] + Math.round(view.getTranslationY());
        iArr[1] = iArr[1] + (view.getHeight() / 2);
        qhVar.a.put("android:visibilityPropagation:center", iArr);
    }

    @DexIgnore
    public int b(qh qhVar) {
        if (qhVar == null) {
            return 8;
        }
        Integer num = (Integer) qhVar.a.get("android:visibilityPropagation:visibility");
        if (num == null) {
            return 8;
        }
        return num.intValue();
    }

    @DexIgnore
    public int c(qh qhVar) {
        return a(qhVar, 0);
    }

    @DexIgnore
    public int d(qh qhVar) {
        return a(qhVar, 1);
    }

    @DexIgnore
    public String[] a() {
        return a;
    }

    @DexIgnore
    public static int a(qh qhVar, int i) {
        if (qhVar == null) {
            return -1;
        }
        int[] iArr = (int[]) qhVar.a.get("android:visibilityPropagation:center");
        if (iArr == null) {
            return -1;
        }
        return iArr[i];
    }
}
