package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hw2 implements Factory<NotificationAppsPresenter> {
    @DexIgnore
    public static NotificationAppsPresenter a(dw2 dw2, k62 k62, wy2 wy2, qx2 qx2, jw2 jw2, fn2 fn2, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new NotificationAppsPresenter(dw2, k62, wy2, qx2, jw2, fn2, notificationSettingsDatabase);
    }
}
