package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class c10 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[GattOperationResult.GattResult.ResultCode.values().length];

    /*
    static {
        a[GattOperationResult.GattResult.ResultCode.SUCCESS.ordinal()] = 1;
        a[GattOperationResult.GattResult.ResultCode.HID_PROXY_NOT_CONNECTED.ordinal()] = 2;
        a[GattOperationResult.GattResult.ResultCode.HID_FAIL_TO_INVOKE_PRIVATE_METHOD.ordinal()] = 3;
        a[GattOperationResult.GattResult.ResultCode.HID_INPUT_DEVICE_DISABLED.ordinal()] = 4;
        a[GattOperationResult.GattResult.ResultCode.HID_UNKNOWN_ERROR.ordinal()] = 5;
        a[GattOperationResult.GattResult.ResultCode.BLUETOOTH_OFF.ordinal()] = 6;
    }
    */
}
