package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zo0 extends ge0<to0> {
    @DexIgnore
    public static /* final */ yo0 j; // = new t11();

    @DexIgnore
    public zo0(Context context, to0 to0) {
        super(context, i01.G, to0, ge0.a.c);
    }

    @DexIgnore
    public xn1<Void> a(bq0 bq0) {
        return bk0.a(j.a(a(), bq0));
    }
}
