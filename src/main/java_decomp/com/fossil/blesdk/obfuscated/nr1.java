package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nr1 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ float c;

    @DexIgnore
    public nr1(int i, float f, float f2) {
        this.a = i;
        this.b = f;
        this.c = f2;
    }
}
