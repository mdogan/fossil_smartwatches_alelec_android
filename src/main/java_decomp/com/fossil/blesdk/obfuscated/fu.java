package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fu implements hu<Drawable, byte[]> {
    @DexIgnore
    public /* final */ kq a;
    @DexIgnore
    public /* final */ hu<Bitmap, byte[]> b;
    @DexIgnore
    public /* final */ hu<vt, byte[]> c;

    @DexIgnore
    public fu(kq kqVar, hu<Bitmap, byte[]> huVar, hu<vt, byte[]> huVar2) {
        this.a = kqVar;
        this.b = huVar;
        this.c = huVar2;
    }

    @DexIgnore
    public static bq<vt> a(bq<Drawable> bqVar) {
        return bqVar;
    }

    @DexIgnore
    public bq<byte[]> a(bq<Drawable> bqVar, mo moVar) {
        Drawable drawable = bqVar.get();
        if (drawable instanceof BitmapDrawable) {
            return this.b.a(qs.a(((BitmapDrawable) drawable).getBitmap(), this.a), moVar);
        }
        if (!(drawable instanceof vt)) {
            return null;
        }
        hu<vt, byte[]> huVar = this.c;
        a(bqVar);
        return huVar.a(bqVar, moVar);
    }
}
