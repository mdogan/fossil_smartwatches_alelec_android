package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mv2 implements Factory<a63> {
    @DexIgnore
    public static a63 a(hv2 hv2) {
        a63 e = hv2.e();
        o44.a(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }
}
