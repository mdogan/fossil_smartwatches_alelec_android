package com.fossil.blesdk.obfuscated;

import android.os.Build;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yi {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ mj c;
    @DexIgnore
    public /* final */ dj d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public Executor a;
        @DexIgnore
        public mj b;
        @DexIgnore
        public dj c;
        @DexIgnore
        public Executor d;
        @DexIgnore
        public int e; // = 4;
        @DexIgnore
        public int f; // = 0;
        @DexIgnore
        public int g; // = Integer.MAX_VALUE;
        @DexIgnore
        public int h; // = 20;

        @DexIgnore
        public a a(mj mjVar) {
            this.b = mjVar;
            return this;
        }

        @DexIgnore
        public yi a() {
            return new yi(this);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        yi a();
    }

    @DexIgnore
    public yi(a aVar) {
        Executor executor = aVar.a;
        if (executor == null) {
            this.a = a();
        } else {
            this.a = executor;
        }
        Executor executor2 = aVar.d;
        if (executor2 == null) {
            this.b = a();
        } else {
            this.b = executor2;
        }
        mj mjVar = aVar.b;
        if (mjVar == null) {
            this.c = mj.a();
        } else {
            this.c = mjVar;
        }
        dj djVar = aVar.c;
        if (djVar == null) {
            this.d = dj.a();
        } else {
            this.d = djVar;
        }
        this.e = aVar.e;
        this.f = aVar.f;
        this.g = aVar.g;
        this.h = aVar.h;
    }

    @DexIgnore
    public final Executor a() {
        return Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4)));
    }

    @DexIgnore
    public Executor b() {
        return this.a;
    }

    @DexIgnore
    public dj c() {
        return this.d;
    }

    @DexIgnore
    public int d() {
        return this.g;
    }

    @DexIgnore
    public int e() {
        if (Build.VERSION.SDK_INT == 23) {
            return this.h / 2;
        }
        return this.h;
    }

    @DexIgnore
    public int f() {
        return this.f;
    }

    @DexIgnore
    public int g() {
        return this.e;
    }

    @DexIgnore
    public Executor h() {
        return this.b;
    }

    @DexIgnore
    public mj i() {
        return this.c;
    }
}
