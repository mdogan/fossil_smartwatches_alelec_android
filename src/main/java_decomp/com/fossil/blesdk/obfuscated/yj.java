package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import com.fossil.blesdk.obfuscated.bk;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yj implements nj {
    @DexIgnore
    public static /* final */ String h; // = ej.a("CommandHandler");
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ Map<String, nj> f; // = new HashMap();
    @DexIgnore
    public /* final */ Object g; // = new Object();

    @DexIgnore
    public yj(Context context) {
        this.e = context;
    }

    @DexIgnore
    public static Intent a(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_DELAY_MET");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public static Intent b(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_SCHEDULE_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public static Intent c(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_STOP_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public final void d(Intent intent, int i, bk bkVar) {
        ej.a().a(h, String.format("Handling reschedule %s, %s", new Object[]{intent, Integer.valueOf(i)}), new Throwable[0]);
        bkVar.e().j();
    }

    @DexIgnore
    public final void e(Intent intent, int i, bk bkVar) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        ej.a().a(h, String.format("Handling schedule work for %s", new Object[]{string}), new Throwable[0]);
        WorkDatabase g2 = bkVar.e().g();
        g2.beginTransaction();
        try {
            il e2 = g2.d().e(string);
            if (e2 == null) {
                ej a = ej.a();
                String str = h;
                a.e(str, "Skipping scheduling " + string + " because it's no longer in the DB", new Throwable[0]);
            } else if (e2.b.isFinished()) {
                ej a2 = ej.a();
                String str2 = h;
                a2.e(str2, "Skipping scheduling " + string + "because it is finished.", new Throwable[0]);
                g2.endTransaction();
            } else {
                long a3 = e2.a();
                if (!e2.b()) {
                    ej.a().a(h, String.format("Setting up Alarms for %s at %s", new Object[]{string, Long.valueOf(a3)}), new Throwable[0]);
                    xj.a(this.e, bkVar.e(), string, a3);
                } else {
                    ej.a().a(h, String.format("Opportunistically setting an alarm for %s at %s", new Object[]{string, Long.valueOf(a3)}), new Throwable[0]);
                    xj.a(this.e, bkVar.e(), string, a3);
                    bkVar.a((Runnable) new bk.b(bkVar, a(this.e), i));
                }
                g2.setTransactionSuccessful();
                g2.endTransaction();
            }
        } finally {
            g2.endTransaction();
        }
    }

    @DexIgnore
    public final void f(Intent intent, int i, bk bkVar) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        ej.a().a(h, String.format("Handing stopWork work for %s", new Object[]{string}), new Throwable[0]);
        bkVar.e().b(string);
        xj.a(this.e, bkVar.e(), string);
        bkVar.a(string, false);
    }

    @DexIgnore
    public void g(Intent intent, int i, bk bkVar) {
        String action = intent.getAction();
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action)) {
            a(intent, i, bkVar);
        } else if ("ACTION_RESCHEDULE".equals(action)) {
            d(intent, i, bkVar);
        } else if (!a(intent.getExtras(), "KEY_WORKSPEC_ID")) {
            ej.a().b(h, String.format("Invalid request for %s, requires %s.", new Object[]{action, "KEY_WORKSPEC_ID"}), new Throwable[0]);
        } else if ("ACTION_SCHEDULE_WORK".equals(action)) {
            e(intent, i, bkVar);
        } else if ("ACTION_DELAY_MET".equals(action)) {
            b(intent, i, bkVar);
        } else if ("ACTION_STOP_WORK".equals(action)) {
            f(intent, i, bkVar);
        } else if ("ACTION_EXECUTION_COMPLETED".equals(action)) {
            c(intent, i, bkVar);
        } else {
            ej.a().e(h, String.format("Ignoring intent %s", new Object[]{intent}), new Throwable[0]);
        }
    }

    @DexIgnore
    public static Intent a(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_CONSTRAINTS_CHANGED");
        return intent;
    }

    @DexIgnore
    public static Intent b(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_RESCHEDULE");
        return intent;
    }

    @DexIgnore
    public final void c(Intent intent, int i, bk bkVar) {
        Bundle extras = intent.getExtras();
        String string = extras.getString("KEY_WORKSPEC_ID");
        boolean z = extras.getBoolean("KEY_NEEDS_RESCHEDULE");
        ej.a().a(h, String.format("Handling onExecutionCompleted %s, %s", new Object[]{intent, Integer.valueOf(i)}), new Throwable[0]);
        a(string, z);
    }

    @DexIgnore
    public static Intent a(Context context, String str, boolean z) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_EXECUTION_COMPLETED");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        intent.putExtra("KEY_NEEDS_RESCHEDULE", z);
        return intent;
    }

    @DexIgnore
    public final void b(Intent intent, int i, bk bkVar) {
        Bundle extras = intent.getExtras();
        synchronized (this.g) {
            String string = extras.getString("KEY_WORKSPEC_ID");
            ej.a().a(h, String.format("Handing delay met for %s", new Object[]{string}), new Throwable[0]);
            if (!this.f.containsKey(string)) {
                ak akVar = new ak(this.e, i, string, bkVar);
                this.f.put(string, akVar);
                akVar.b();
            } else {
                ej.a().a(h, String.format("WorkSpec %s is already being handled for ACTION_DELAY_MET", new Object[]{string}), new Throwable[0]);
            }
        }
    }

    @DexIgnore
    public void a(String str, boolean z) {
        synchronized (this.g) {
            nj remove = this.f.remove(str);
            if (remove != null) {
                remove.a(str, z);
            }
        }
    }

    @DexIgnore
    public boolean a() {
        boolean z;
        synchronized (this.g) {
            z = !this.f.isEmpty();
        }
        return z;
    }

    @DexIgnore
    public final void a(Intent intent, int i, bk bkVar) {
        ej.a().a(h, String.format("Handling constraints changed %s", new Object[]{intent}), new Throwable[0]);
        new zj(this.e, i, bkVar).a();
    }

    @DexIgnore
    public static boolean a(Bundle bundle, String... strArr) {
        if (bundle == null || bundle.isEmpty()) {
            return false;
        }
        for (String str : strArr) {
            if (bundle.get(str) == null) {
                return false;
            }
        }
        return true;
    }
}
