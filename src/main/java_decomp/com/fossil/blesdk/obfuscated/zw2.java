package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zw2 {
    @DexIgnore
    public /* final */ xw2 a;
    @DexIgnore
    public /* final */ LoaderManager b;

    @DexIgnore
    public zw2(xw2 xw2, LoaderManager loaderManager) {
        wd4.b(xw2, "mView");
        wd4.b(loaderManager, "mLoaderManager");
        this.a = xw2;
        this.b = loaderManager;
    }

    @DexIgnore
    public final LoaderManager a() {
        return this.b;
    }

    @DexIgnore
    public final xw2 b() {
        return this.a;
    }
}
