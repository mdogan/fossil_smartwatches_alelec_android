package com.fossil.blesdk.obfuscated;

import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pq implements hq {
    @DexIgnore
    public /* final */ nq<a, Object> a; // = new nq<>();
    @DexIgnore
    public /* final */ b b; // = new b();
    @DexIgnore
    public /* final */ Map<Class<?>, NavigableMap<Integer, Integer>> c; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, gq<?>> d; // = new HashMap();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public int f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends jq<a> {
        @DexIgnore
        public a a(int i, Class<?> cls) {
            a aVar = (a) b();
            aVar.a(i, cls);
            return aVar;
        }

        @DexIgnore
        public a a() {
            return new a(this);
        }
    }

    @DexIgnore
    public pq(int i) {
        this.e = i;
    }

    @DexIgnore
    public synchronized <T> T a(int i, Class<T> cls) {
        return a(this.b.a(i, cls), cls);
    }

    @DexIgnore
    public synchronized <T> T b(int i, Class<T> cls) {
        a aVar;
        Integer ceilingKey = b((Class<?>) cls).ceilingKey(Integer.valueOf(i));
        if (a(i, ceilingKey)) {
            aVar = this.b.a(ceilingKey.intValue(), cls);
        } else {
            aVar = this.b.a(i, cls);
        }
        return a(aVar, cls);
    }

    @DexIgnore
    public final boolean c(int i) {
        return i <= this.e / 2;
    }

    @DexIgnore
    public synchronized <T> void put(T t) {
        Class<?> cls = t.getClass();
        gq<?> a2 = a(cls);
        int a3 = a2.a(t);
        int b2 = a2.b() * a3;
        if (c(b2)) {
            a a4 = this.b.a(a3, cls);
            this.a.a(a4, t);
            NavigableMap<Integer, Integer> b3 = b(cls);
            Integer num = (Integer) b3.get(Integer.valueOf(a4.b));
            Integer valueOf = Integer.valueOf(a4.b);
            int i = 1;
            if (num != null) {
                i = 1 + num.intValue();
            }
            b3.put(valueOf, Integer.valueOf(i));
            this.f += b2;
            b();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements sq {
        @DexIgnore
        public /* final */ b a;
        @DexIgnore
        public int b;
        @DexIgnore
        public Class<?> c;

        @DexIgnore
        public a(b bVar) {
            this.a = bVar;
        }

        @DexIgnore
        public void a(int i, Class<?> cls) {
            this.b = i;
            this.c = cls;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.b == aVar.b && this.c == aVar.c) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.b * 31;
            Class<?> cls = this.c;
            return i + (cls != null ? cls.hashCode() : 0);
        }

        @DexIgnore
        public String toString() {
            return "Key{size=" + this.b + "array=" + this.c + '}';
        }

        @DexIgnore
        public void a() {
            this.a.a(this);
        }
    }

    @DexIgnore
    public final boolean c() {
        int i = this.f;
        return i == 0 || this.e / i >= 2;
    }

    @DexIgnore
    public final <T> T a(a aVar, Class<T> cls) {
        gq<T> a2 = a(cls);
        T a3 = a(aVar);
        if (a3 != null) {
            this.f -= a2.a(a3) * a2.b();
            c(a2.a(a3), cls);
        }
        if (a3 != null) {
            return a3;
        }
        if (Log.isLoggable(a2.a(), 2)) {
            Log.v(a2.a(), "Allocated " + aVar.b + " bytes");
        }
        return a2.newArray(aVar.b);
    }

    @DexIgnore
    public final void c(int i, Class<?> cls) {
        NavigableMap<Integer, Integer> b2 = b(cls);
        Integer num = (Integer) b2.get(Integer.valueOf(i));
        if (num == null) {
            throw new NullPointerException("Tried to decrement empty size, size: " + i + ", this: " + this);
        } else if (num.intValue() == 1) {
            b2.remove(Integer.valueOf(i));
        } else {
            b2.put(Integer.valueOf(i), Integer.valueOf(num.intValue() - 1));
        }
    }

    @DexIgnore
    public final void b() {
        b(this.e);
    }

    @DexIgnore
    public final void b(int i) {
        while (this.f > i) {
            Object a2 = this.a.a();
            uw.a(a2);
            gq a3 = a(a2);
            this.f -= a3.a(a2) * a3.b();
            c(a3.a(a2), a2.getClass());
            if (Log.isLoggable(a3.a(), 2)) {
                Log.v(a3.a(), "evicted: " + a3.a(a2));
            }
        }
    }

    @DexIgnore
    public final <T> T a(a aVar) {
        return this.a.a(aVar);
    }

    @DexIgnore
    public final boolean a(int i, Integer num) {
        return num != null && (c() || num.intValue() <= i * 8);
    }

    @DexIgnore
    public synchronized void a() {
        b(0);
    }

    @DexIgnore
    public synchronized void a(int i) {
        if (i >= 40) {
            try {
                a();
            } catch (Throwable th) {
                throw th;
            }
        } else if (i >= 20 || i == 15) {
            b(this.e / 2);
        }
    }

    @DexIgnore
    public final NavigableMap<Integer, Integer> b(Class<?> cls) {
        NavigableMap<Integer, Integer> navigableMap = this.c.get(cls);
        if (navigableMap != null) {
            return navigableMap;
        }
        TreeMap treeMap = new TreeMap();
        this.c.put(cls, treeMap);
        return treeMap;
    }

    @DexIgnore
    public final <T> gq<T> a(T t) {
        return a(t.getClass());
    }

    @DexIgnore
    public final <T> gq<T> a(Class<T> cls) {
        gq<T> gqVar = this.d.get(cls);
        if (gqVar == null) {
            if (cls.equals(int[].class)) {
                gqVar = new oq();
            } else if (cls.equals(byte[].class)) {
                gqVar = new mq();
            } else {
                throw new IllegalArgumentException("No array pool found for: " + cls.getSimpleName());
            }
            this.d.put(cls, gqVar);
        }
        return gqVar;
    }
}
