package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzfe$zzb;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r51 implements z81 {
    @DexIgnore
    public static /* final */ z81 a; // = new r51();

    @DexIgnore
    public final boolean zzb(int i) {
        return zzfe$zzb.zzb.zzt(i) != null;
    }
}
