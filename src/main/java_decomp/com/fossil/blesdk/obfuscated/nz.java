package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nz implements d00 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ d00[] b;
    @DexIgnore
    public /* final */ oz c;

    @DexIgnore
    public nz(int i, d00... d00Arr) {
        this.a = i;
        this.b = d00Arr;
        this.c = new oz(i);
    }

    @DexIgnore
    public StackTraceElement[] a(StackTraceElement[] stackTraceElementArr) {
        if (stackTraceElementArr.length <= this.a) {
            return stackTraceElementArr;
        }
        StackTraceElement[] stackTraceElementArr2 = stackTraceElementArr;
        for (d00 d00 : this.b) {
            if (stackTraceElementArr2.length <= this.a) {
                break;
            }
            stackTraceElementArr2 = d00.a(stackTraceElementArr);
        }
        return stackTraceElementArr2.length > this.a ? this.c.a(stackTraceElementArr2) : stackTraceElementArr2;
    }
}
