package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.j62;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u03 extends j62<c, d, b> {
    @DexIgnore
    public static /* final */ String d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements j62.a {
        @DexIgnore
        public b(String str) {
            wd4.b(str, "message");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements j62.b {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements j62.c {
        @DexIgnore
        public /* final */ List<ContactGroup> a;

        @DexIgnore
        public d(List<? extends ContactGroup> list) {
            wd4.b(list, "contactGroups");
            this.a = list;
        }

        @DexIgnore
        public final List<ContactGroup> a() {
            return this.a;
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = u03.class.getSimpleName();
        wd4.a((Object) simpleName, "GetAllHybridContactGroups::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public void a(c cVar) {
        FLogger.INSTANCE.getLocal().d(d, "executeUseCase");
        List<ContactGroup> allContactGroups = en2.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        if (allContactGroups != null) {
            a().onSuccess(new d(allContactGroups));
        } else {
            a().a(new b("Get all contact group failed"));
        }
    }
}
