package com.fossil.blesdk.obfuscated;

import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class az implements c74 {
    @DexIgnore
    public /* final */ sz a;

    @DexIgnore
    public az(sz szVar) {
        this.a = szVar;
    }

    @DexIgnore
    public String a() {
        return this.a.a();
    }

    @DexIgnore
    public InputStream b() {
        return this.a.b();
    }

    @DexIgnore
    public String[] c() {
        return this.a.c();
    }

    @DexIgnore
    public long d() {
        return -1;
    }
}
