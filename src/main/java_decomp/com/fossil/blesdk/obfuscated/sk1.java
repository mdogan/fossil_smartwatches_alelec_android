package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sk1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ el1 e;
    @DexIgnore
    public /* final */ /* synthetic */ Runnable f;

    @DexIgnore
    public sk1(ok1 ok1, el1 el1, Runnable runnable) {
        this.e = el1;
        this.f = runnable;
    }

    @DexIgnore
    public final void run() {
        this.e.y();
        this.e.a(this.f);
        this.e.t();
    }
}
