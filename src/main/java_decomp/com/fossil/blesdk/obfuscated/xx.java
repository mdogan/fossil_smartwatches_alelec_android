package com.fossil.blesdk.obfuscated;

import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xx implements k64 {
    @DexIgnore
    public /* final */ k64 a;
    @DexIgnore
    public /* final */ Random b;
    @DexIgnore
    public /* final */ double c;

    @DexIgnore
    public xx(k64 k64, double d) {
        this(k64, d, new Random());
    }

    @DexIgnore
    public long a(int i) {
        return (long) (a() * ((double) this.a.a(i)));
    }

    @DexIgnore
    public xx(k64 k64, double d, Random random) {
        if (d < 0.0d || d > 1.0d) {
            throw new IllegalArgumentException("jitterPercent must be between 0.0 and 1.0");
        } else if (k64 == null) {
            throw new NullPointerException("backoff must not be null");
        } else if (random != null) {
            this.a = k64;
            this.c = d;
            this.b = random;
        } else {
            throw new NullPointerException("random must not be null");
        }
    }

    @DexIgnore
    public double a() {
        double d = this.c;
        double d2 = 1.0d - d;
        return d2 + (((d + 1.0d) - d2) * this.b.nextDouble());
    }
}
