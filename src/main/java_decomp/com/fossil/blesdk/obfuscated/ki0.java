package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ki0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ji0 e;

    @DexIgnore
    public ki0(ji0 ji0) {
        this.e = ji0;
    }

    @DexIgnore
    public final void run() {
        this.e.q.lock();
        try {
            this.e.i();
        } finally {
            this.e.q.unlock();
        }
    }
}
