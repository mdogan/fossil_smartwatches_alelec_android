package com.fossil.blesdk.obfuscated;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.MalformedJsonException;
import java.io.EOFException;
import java.io.IOException;
import java.io.Writer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q02 {
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        throw new com.google.gson.JsonIOException((java.lang.Throwable) r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0023, code lost:
        throw new com.google.gson.JsonSyntaxException((java.lang.Throwable) r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002a, code lost:
        return com.fossil.blesdk.obfuscated.xz1.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0030, code lost:
        throw new com.google.gson.JsonSyntaxException((java.lang.Throwable) r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000d, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000f, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        throw new com.google.gson.JsonSyntaxException((java.lang.Throwable) r2);
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0016 A[ExcHandler: IOException (r2v5 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x001d A[ExcHandler: MalformedJsonException (r2v4 'e' com.google.gson.stream.MalformedJsonException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x000f A[ExcHandler: NumberFormatException (r2v6 'e' java.lang.NumberFormatException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    public static JsonElement a(JsonReader jsonReader) throws JsonParseException {
        boolean z;
        try {
            jsonReader.Q();
            z = false;
            return TypeAdapters.X.read(jsonReader);
        } catch (EOFException e) {
            e = e;
            z = true;
            if (!z) {
            }
        } catch (MalformedJsonException e2) {
        } catch (IOException e3) {
        } catch (NumberFormatException e4) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Writer {
        @DexIgnore
        public /* final */ Appendable e;
        @DexIgnore
        public /* final */ C0027a f; // = new C0027a();

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.q02$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.q02$a$a  reason: collision with other inner class name */
        public static class C0027a implements CharSequence {
            @DexIgnore
            public char[] e;

            @DexIgnore
            public char charAt(int i) {
                return this.e[i];
            }

            @DexIgnore
            public int length() {
                return this.e.length;
            }

            @DexIgnore
            public CharSequence subSequence(int i, int i2) {
                return new String(this.e, i, i2 - i);
            }
        }

        @DexIgnore
        public a(Appendable appendable) {
            this.e = appendable;
        }

        @DexIgnore
        public void close() {
        }

        @DexIgnore
        public void flush() {
        }

        @DexIgnore
        public void write(char[] cArr, int i, int i2) throws IOException {
            C0027a aVar = this.f;
            aVar.e = cArr;
            this.e.append(aVar, i, i2 + i);
        }

        @DexIgnore
        public void write(int i) throws IOException {
            this.e.append((char) i);
        }
    }

    @DexIgnore
    public static void a(JsonElement jsonElement, JsonWriter jsonWriter) throws IOException {
        TypeAdapters.X.write(jsonWriter, jsonElement);
    }

    @DexIgnore
    public static Writer a(Appendable appendable) {
        return appendable instanceof Writer ? (Writer) appendable : new a(appendable);
    }
}
