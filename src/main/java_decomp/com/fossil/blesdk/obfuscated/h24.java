package com.fossil.blesdk.obfuscated;

import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h24 implements FileFilter {
    @DexIgnore
    public boolean accept(File file) {
        return Pattern.matches("cpu[0-9]", file.getName());
    }
}
