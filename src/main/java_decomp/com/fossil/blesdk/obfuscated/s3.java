package com.fossil.blesdk.obfuscated;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class s3 {
    @DexIgnore
    public /* final */ a a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends o3 {
        @DexIgnore
        public a(s3 s3Var) {
        }
    }

    @DexIgnore
    public s3(a aVar) {
        this.a = aVar;
        new a(this);
    }

    @DexIgnore
    public IBinder a() {
        return this.a.asBinder();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof s3)) {
            return false;
        }
        return ((s3) obj).a().equals(this.a.asBinder());
    }

    @DexIgnore
    public int hashCode() {
        return a().hashCode();
    }
}
