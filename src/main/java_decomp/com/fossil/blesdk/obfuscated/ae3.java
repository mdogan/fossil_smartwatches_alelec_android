package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ae3 implements MembersInjector<ActiveTimeDetailActivity> {
    @DexIgnore
    public static void a(ActiveTimeDetailActivity activeTimeDetailActivity, ActiveTimeDetailPresenter activeTimeDetailPresenter) {
        activeTimeDetailActivity.B = activeTimeDetailPresenter;
    }
}
