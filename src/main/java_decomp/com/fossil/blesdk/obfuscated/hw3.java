package com.fossil.blesdk.obfuscated;

import com.squareup.okhttp.internal.framed.ErrorCode;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface hw3 {
    @DexIgnore
    public static final hw3 a = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements hw3 {
        @DexIgnore
        public void a(cw3 cw3) throws IOException {
            cw3.a(ErrorCode.REFUSED_STREAM);
        }
    }

    @DexIgnore
    void a(cw3 cw3) throws IOException;
}
