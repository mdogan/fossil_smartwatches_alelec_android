package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class pg0 {
    @DexIgnore
    public /* final */ ng0 a;

    @DexIgnore
    public pg0(ng0 ng0) {
        this.a = ng0;
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void a(og0 og0) {
        og0.e.lock();
        try {
            if (og0.o == this.a) {
                a();
                og0.e.unlock();
            }
        } finally {
            og0.e.unlock();
        }
    }
}
