package com.fossil.blesdk.obfuscated;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import androidx.work.WorkInfo;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemjob.SystemJobService;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fk implements qj {
    @DexIgnore
    public static /* final */ String j; // = ej.a("SystemJobScheduler");
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ JobScheduler f;
    @DexIgnore
    public /* final */ uj g;
    @DexIgnore
    public /* final */ rl h;
    @DexIgnore
    public /* final */ ek i;

    @DexIgnore
    public fk(Context context, uj ujVar) {
        this(context, ujVar, (JobScheduler) context.getSystemService("jobscheduler"), new ek(context));
    }

    @DexIgnore
    public static void b(Context context) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        if (jobScheduler != null) {
            List<JobInfo> a = a(context, jobScheduler);
            if (a != null && !a.isEmpty()) {
                for (JobInfo next : a) {
                    if (a(next) == null) {
                        a(jobScheduler, next.getId());
                    }
                }
            }
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public void a(il... ilVarArr) {
        int i2;
        int i3;
        WorkDatabase g2 = this.g.g();
        int length = ilVarArr.length;
        int i4 = 0;
        while (i4 < length) {
            il ilVar = ilVarArr[i4];
            g2.beginTransaction();
            try {
                il e2 = g2.d().e(ilVar.a);
                if (e2 == null) {
                    ej a = ej.a();
                    String str = j;
                    a.e(str, "Skipping scheduling " + ilVar.a + " because it's no longer in the DB", new Throwable[0]);
                    g2.setTransactionSuccessful();
                } else if (e2.b != WorkInfo.State.ENQUEUED) {
                    ej a2 = ej.a();
                    String str2 = j;
                    a2.e(str2, "Skipping scheduling " + ilVar.a + " because it is no longer enqueued", new Throwable[0]);
                    g2.setTransactionSuccessful();
                } else {
                    cl a3 = g2.b().a(ilVar.a);
                    if (a3 != null) {
                        i2 = a3.b;
                    } else {
                        i2 = this.h.a(this.g.c().f(), this.g.c().d());
                    }
                    if (a3 == null) {
                        this.g.g().b().a(new cl(ilVar.a, i2));
                    }
                    a(ilVar, i2);
                    if (Build.VERSION.SDK_INT == 23) {
                        List<Integer> a4 = a(this.e, this.f, ilVar.a);
                        if (a4 != null) {
                            int indexOf = a4.indexOf(Integer.valueOf(i2));
                            if (indexOf >= 0) {
                                a4.remove(indexOf);
                            }
                            if (!a4.isEmpty()) {
                                i3 = a4.get(0).intValue();
                            } else {
                                i3 = this.h.a(this.g.c().f(), this.g.c().d());
                            }
                            a(ilVar, i3);
                        }
                    }
                    g2.setTransactionSuccessful();
                }
                g2.endTransaction();
                i4++;
            } catch (Throwable th) {
                g2.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore
    public fk(Context context, uj ujVar, JobScheduler jobScheduler, ek ekVar) {
        this.e = context;
        this.g = ujVar;
        this.f = jobScheduler;
        this.h = new rl(context);
        this.i = ekVar;
    }

    @DexIgnore
    public void a(il ilVar, int i2) {
        JobInfo a = this.i.a(ilVar, i2);
        ej.a().a(j, String.format("Scheduling work ID %s Job ID %s", new Object[]{ilVar.a, Integer.valueOf(i2)}), new Throwable[0]);
        try {
            this.f.schedule(a);
        } catch (IllegalStateException e2) {
            List<JobInfo> a2 = a(this.e, this.f);
            String format = String.format(Locale.getDefault(), "JobScheduler 100 job limit exceeded.  We count %d WorkManager jobs in JobScheduler; we have %d tracked jobs in our DB; our Configuration limit is %d.", new Object[]{Integer.valueOf(a2 != null ? a2.size() : 0), Integer.valueOf(this.g.g().d().a().size()), Integer.valueOf(this.g.c().e())});
            ej.a().b(j, format, new Throwable[0]);
            throw new IllegalStateException(format, e2);
        } catch (Throwable th) {
            ej.a().b(j, String.format("Unable to schedule %s", new Object[]{ilVar}), th);
        }
    }

    @DexIgnore
    public void a(String str) {
        List<Integer> a = a(this.e, this.f, str);
        if (a != null && !a.isEmpty()) {
            for (Integer intValue : a) {
                a(this.f, intValue.intValue());
            }
            this.g.g().b().b(str);
        }
    }

    @DexIgnore
    public static void a(JobScheduler jobScheduler, int i2) {
        try {
            jobScheduler.cancel(i2);
        } catch (Throwable th) {
            ej.a().b(j, String.format(Locale.getDefault(), "Exception while trying to cancel job (%d)", new Object[]{Integer.valueOf(i2)}), th);
        }
    }

    @DexIgnore
    public static void a(Context context) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        if (jobScheduler != null) {
            List<JobInfo> a = a(context, jobScheduler);
            if (a != null && !a.isEmpty()) {
                for (JobInfo id : a) {
                    a(jobScheduler, id.getId());
                }
            }
        }
    }

    @DexIgnore
    public static List<JobInfo> a(Context context, JobScheduler jobScheduler) {
        List<JobInfo> list;
        try {
            list = jobScheduler.getAllPendingJobs();
        } catch (Throwable th) {
            ej.a().b(j, "getAllPendingJobs() is not reliable on this device.", th);
            list = null;
        }
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        ComponentName componentName = new ComponentName(context, SystemJobService.class);
        for (JobInfo next : list) {
            if (componentName.equals(next.getService())) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static List<Integer> a(Context context, JobScheduler jobScheduler, String str) {
        List<JobInfo> a = a(context, jobScheduler);
        if (a == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(2);
        for (JobInfo next : a) {
            if (str.equals(a(next))) {
                arrayList.add(Integer.valueOf(next.getId()));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static String a(JobInfo jobInfo) {
        PersistableBundle extras = jobInfo.getExtras();
        if (extras == null) {
            return null;
        }
        try {
            if (extras.containsKey("EXTRA_WORK_SPEC_ID")) {
                return extras.getString("EXTRA_WORK_SPEC_ID");
            }
            return null;
        } catch (NullPointerException unused) {
            return null;
        }
    }
}
