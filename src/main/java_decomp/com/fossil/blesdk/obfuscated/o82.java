package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class o82 extends n82 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j u; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray v; // = new SparseIntArray();
    @DexIgnore
    public /* final */ LinearLayout s;
    @DexIgnore
    public long t;

    /*
    static {
        v.put(R.id.ivRightButton, 1);
        v.put(R.id.ftvTitle, 2);
        v.put(R.id.webView, 3);
    }
    */

    @DexIgnore
    public o82(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 4, u, v));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.t = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.t != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.t = 1;
        }
        g();
    }

    @DexIgnore
    public o82(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[2], objArr[1], objArr[3]);
        this.t = -1;
        this.s = objArr[0];
        this.s.setTag((Object) null);
        a(view);
        f();
    }
}
