package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.he0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i01 extends l21<f11> {
    @DexIgnore
    public static /* final */ ee0.g<i01> E; // = new ee0.g<>();
    @DexIgnore
    public static /* final */ ee0<Object> F; // = new ee0<>("Fitness.SESSIONS_API", new l01(), E);
    @DexIgnore
    public static /* final */ ee0<to0> G; // = new ee0<>("Fitness.SESSIONS_CLIENT", new n01(), E);

    @DexIgnore
    public i01(Context context, Looper looper, lj0 lj0, he0.b bVar, he0.c cVar) {
        super(context, looper, 58, bVar, cVar, lj0);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitSessionsApi");
        if (queryLocalInterface instanceof f11) {
            return (f11) queryLocalInterface;
        }
        return new g11(iBinder);
    }

    @DexIgnore
    public final int i() {
        return ae0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.fitness.internal.IGoogleFitSessionsApi";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.fitness.SessionsApi";
    }
}
