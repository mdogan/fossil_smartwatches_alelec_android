package com.fossil.blesdk.obfuscated;

import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class gm1 {
    @DexIgnore
    public static volatile Handler d;
    @DexIgnore
    public /* final */ wi1 a;
    @DexIgnore
    public /* final */ Runnable b;
    @DexIgnore
    public volatile long c;

    @DexIgnore
    public gm1(wi1 wi1) {
        ck0.a(wi1);
        this.a = wi1;
        this.b = new hm1(this, wi1);
    }

    @DexIgnore
    public final void a(long j) {
        a();
        if (j >= 0) {
            this.c = this.a.c().b();
            if (!b().postDelayed(this.b, j)) {
                this.a.d().s().a("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }

    @DexIgnore
    public final Handler b() {
        Handler handler;
        if (d != null) {
            return d;
        }
        synchronized (gm1.class) {
            if (d == null) {
                d = new n51(this.a.getContext().getMainLooper());
            }
            handler = d;
        }
        return handler;
    }

    @DexIgnore
    public abstract void c();

    @DexIgnore
    public final boolean d() {
        return this.c != 0;
    }

    @DexIgnore
    public final void a() {
        this.c = 0;
        b().removeCallbacks(this.b);
    }
}
