package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i13 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;

    @DexIgnore
    public i13() {
        this((String) null, (String) null, (String) null, (String) null, (String) null, 31, (rd4) null);
    }

    @DexIgnore
    public i13(String str, String str2, String str3, String str4, String str5) {
        wd4.b(str, "id");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.d;
    }

    @DexIgnore
    public final String d() {
        return this.a;
    }

    @DexIgnore
    public final String e() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof i13)) {
            return false;
        }
        i13 i13 = (i13) obj;
        return wd4.a((Object) this.a, (Object) i13.a) && wd4.a((Object) this.b, (Object) i13.b) && wd4.a((Object) this.c, (Object) i13.c) && wd4.a((Object) this.d, (Object) i13.d) && wd4.a((Object) this.e, (Object) i13.e);
    }

    @DexIgnore
    public final String f() {
        return this.d;
    }

    @DexIgnore
    public final String g() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public String toString() {
        return "PresetConfig(id=" + this.a + ", icon=" + this.b + ", name=" + this.c + ", position=" + this.d + ", value=" + this.e + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ i13(String str, String str2, String str3, String str4, String str5, int i, rd4 rd4) {
        this(r11, (i & 2) != 0 ? "" : str2, (i & 4) != 0 ? "" : str3, (i & 8) != 0 ? "" : str4, (i & 16) != 0 ? "" : str5);
        String str6 = (i & 1) != 0 ? "" : str;
    }
}
