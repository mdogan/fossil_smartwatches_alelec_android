package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ft0 extends zs0<String> {
    @DexIgnore
    public ft0(jt0 jt0, String str, String str2) {
        super(jt0, str, str2, (dt0) null);
    }

    @DexIgnore
    public final /* synthetic */ Object a(String str) {
        return str;
    }

    @DexIgnore
    /* renamed from: b */
    public final String a(SharedPreferences sharedPreferences) {
        try {
            return sharedPreferences.getString(this.b, (String) null);
        } catch (ClassCastException e) {
            String valueOf = String.valueOf(this.b);
            Log.e("PhenotypeFlag", valueOf.length() != 0 ? "Invalid string value in SharedPreferences for ".concat(valueOf) : new String("Invalid string value in SharedPreferences for "), e);
            return null;
        }
    }
}
