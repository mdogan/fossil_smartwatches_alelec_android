package com.fossil.blesdk.obfuscated;

import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class nh4 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[CoroutineStart.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[CoroutineStart.values().length];

    /*
    static {
        a[CoroutineStart.DEFAULT.ordinal()] = 1;
        a[CoroutineStart.ATOMIC.ordinal()] = 2;
        a[CoroutineStart.UNDISPATCHED.ordinal()] = 3;
        a[CoroutineStart.LAZY.ordinal()] = 4;
        b[CoroutineStart.DEFAULT.ordinal()] = 1;
        b[CoroutineStart.ATOMIC.ordinal()] = 2;
        b[CoroutineStart.UNDISPATCHED.ordinal()] = 3;
        b[CoroutineStart.LAZY.ordinal()] = 4;
    }
    */
}
