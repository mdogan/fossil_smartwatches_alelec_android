package com.fossil.blesdk.obfuscated;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qz1 {
    @DexIgnore
    public /* final */ Field a;

    @DexIgnore
    public qz1(Field field) {
        j02.a(field);
        this.a = field;
    }

    @DexIgnore
    public <T extends Annotation> T a(Class<T> cls) {
        return this.a.getAnnotation(cls);
    }
}
