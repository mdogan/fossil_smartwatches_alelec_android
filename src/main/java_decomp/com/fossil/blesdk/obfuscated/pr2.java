package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pr2 implements Factory<or2> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public pr2(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static pr2 a(Provider<UserRepository> provider) {
        return new pr2(provider);
    }

    @DexIgnore
    public static or2 b(Provider<UserRepository> provider) {
        return new or2(provider.get());
    }

    @DexIgnore
    public or2 get() {
        return b(this.a);
    }
}
