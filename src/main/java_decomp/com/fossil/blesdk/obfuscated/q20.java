package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.data.file.FileType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q20 extends t20<cb4, DeviceInformation> {
    @DexIgnore
    public static /* final */ l20<cb4>[] a; // = new l20[0];
    @DexIgnore
    public static /* final */ m20<DeviceInformation>[] b; // = {new a(FileType.DEVICE_INFO), new b(FileType.DEVICE_INFO)};
    @DexIgnore
    public static /* final */ q20 c; // = new q20();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends u20<DeviceInformation> {
        @DexIgnore
        public a(FileType fileType) {
            super(fileType);
        }

        @DexIgnore
        public DeviceInformation b(byte[] bArr) {
            wd4.b(bArr, "data");
            return q20.c.b(bArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v20<DeviceInformation> {
        @DexIgnore
        public b(FileType fileType) {
            super(fileType);
        }

        @DexIgnore
        public DeviceInformation b(byte[] bArr) {
            wd4.b(bArr, "data");
            return q20.c.b(bArr);
        }
    }

    @DexIgnore
    public m20<DeviceInformation>[] b() {
        return b;
    }

    @DexIgnore
    public l20<cb4>[] a() {
        return a;
    }

    @DexIgnore
    public final DeviceInformation b(byte[] bArr) {
        return DeviceInformation.Companion.a(kb4.a(bArr, 12, bArr.length - 4));
    }
}
