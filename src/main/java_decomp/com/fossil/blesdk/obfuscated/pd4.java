package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pd4 implements te4<Object>, od4 {
    @DexIgnore
    public /* final */ Class<?> e;

    @DexIgnore
    public pd4(Class<?> cls) {
        wd4.b(cls, "jClass");
        this.e = cls;
    }

    @DexIgnore
    public Class<?> a() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof pd4) && wd4.a((Object) hd4.a(this), (Object) hd4.a((te4) obj));
    }

    @DexIgnore
    public int hashCode() {
        return hd4.a(this).hashCode();
    }

    @DexIgnore
    public String toString() {
        return a().toString() + " (Kotlin reflection is not available)";
    }
}
