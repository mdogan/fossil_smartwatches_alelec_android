package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a14 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ l04 f;

    @DexIgnore
    public a14(Context context, l04 l04) {
        this.e = context;
        this.f = l04;
    }

    @DexIgnore
    public final void run() {
        try {
            k04.a(this.e, false, this.f);
        } catch (Throwable th) {
            k04.m.a(th);
        }
    }
}
