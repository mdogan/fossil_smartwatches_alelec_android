package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.wb1;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xb1<M extends wb1<M>, T> {
    @DexIgnore
    public /* final */ Class<T> a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public final int a(Object obj) {
        throw null;
    }

    @DexIgnore
    public final T a(List<dc1> list) {
        throw null;
    }

    @DexIgnore
    public final void a(Object obj, vb1 vb1) {
        throw null;
    }
}
