package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class j54 implements k54 {
    @DexIgnore
    public /* final */ Context a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ServiceConnection {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public /* final */ LinkedBlockingQueue<IBinder> b;

        @DexIgnore
        public b() {
            this.a = false;
            this.b = new LinkedBlockingQueue<>(1);
        }

        @DexIgnore
        public IBinder a() {
            if (this.a) {
                r44.g().e("Fabric", "getBinder already called");
            }
            this.a = true;
            try {
                return this.b.poll(200, TimeUnit.MILLISECONDS);
            } catch (InterruptedException unused) {
                return null;
            }
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.b.put(iBinder);
            } catch (InterruptedException unused) {
            }
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            this.b.clear();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements IInterface {
        @DexIgnore
        public /* final */ IBinder e;

        @DexIgnore
        public c(IBinder iBinder) {
            this.e = iBinder;
        }

        @DexIgnore
        public IBinder asBinder() {
            return this.e;
        }

        /* JADX INFO: finally extract failed */
        /* JADX WARNING: Can't wrap try/catch for region: R(4:5|6|7|10) */
        /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:4:0x0022, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
            com.fossil.blesdk.obfuscated.r44.g().d("Fabric", "Could not get parcel from Google Play Service to capture AdvertisingId");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x002f, code lost:
            r1.recycle();
            r0.recycle();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0037, code lost:
            r1.recycle();
            r0.recycle();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x003d, code lost:
            throw r2;
         */
        @DexIgnore
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0024 */
        public String getId() throws RemoteException {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
            this.e.transact(1, obtain, obtain2, 0);
            obtain2.readException();
            String readString = obtain2.readString();
            obtain2.recycle();
            obtain.recycle();
            return readString;
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(2:6|7) */
        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0037, code lost:
            r1.recycle();
            r0.recycle();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x003d, code lost:
            throw r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:5:0x0023, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            com.fossil.blesdk.obfuscated.r44.g().d("Fabric", "Could not get parcel from Google Play Service to capture Advertising limitAdTracking");
         */
        @DexIgnore
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0025 */
        public boolean o() throws RemoteException {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            boolean z = false;
            obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
            obtain.writeInt(1);
            this.e.transact(2, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() != 0) {
                z = true;
            }
            obtain2.recycle();
            obtain.recycle();
            return z;
        }
    }

    @DexIgnore
    public j54(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public g54 a() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            r44.g().d("Fabric", "AdvertisingInfoServiceStrategy cannot be called on the main thread");
            return null;
        }
        try {
            this.a.getPackageManager().getPackageInfo("com.android.vending", 0);
            b bVar = new b();
            Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
            intent.setPackage("com.google.android.gms");
            try {
                if (this.a.bindService(intent, bVar, 1)) {
                    try {
                        c cVar = new c(bVar.a());
                        g54 g54 = new g54(cVar.getId(), cVar.o());
                        this.a.unbindService(bVar);
                        return g54;
                    } catch (Exception e) {
                        r44.g().a("Fabric", "Exception in binding to Google Play Service to capture AdvertisingId", (Throwable) e);
                        this.a.unbindService(bVar);
                    }
                } else {
                    r44.g().d("Fabric", "Could not bind to Google Play Service to capture AdvertisingId");
                    return null;
                }
            } catch (Throwable th) {
                r44.g().b("Fabric", "Could not bind to Google Play Service to capture AdvertisingId", th);
            }
        } catch (PackageManager.NameNotFoundException unused) {
            r44.g().d("Fabric", "Unable to find Google Play Services package name");
            return null;
        } catch (Exception e2) {
            r44.g().b("Fabric", "Unable to determine if Google Play Services is available", e2);
            return null;
        }
    }
}
