package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jw0 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public int b; // = 0;

    @DexIgnore
    public jw0(String str) {
        this.a = str;
    }

    @DexIgnore
    public final boolean a() {
        return this.b < this.a.length();
    }

    @DexIgnore
    public final int b() {
        String str = this.a;
        int i = this.b;
        this.b = i + 1;
        char charAt = str.charAt(i);
        if (charAt < 55296) {
            return charAt;
        }
        char c = charAt & 8191;
        int i2 = 13;
        while (true) {
            String str2 = this.a;
            int i3 = this.b;
            this.b = i3 + 1;
            char charAt2 = str2.charAt(i3);
            if (charAt2 < 55296) {
                return c | (charAt2 << i2);
            }
            c |= (charAt2 & 8191) << i2;
            i2 += 13;
        }
    }
}
