package com.fossil.blesdk.obfuscated;

import com.google.common.collect.Lists;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lu1 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends eu1<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable f;
        @DexIgnore
        public /* final */ /* synthetic */ ut1 g;

        @DexIgnore
        public a(Iterable iterable, ut1 ut1) {
            this.f = iterable;
            this.g = ut1;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            return mu1.c(this.f.iterator(), this.g);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends eu1<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable f;
        @DexIgnore
        public /* final */ /* synthetic */ pt1 g;

        @DexIgnore
        public b(Iterable iterable, pt1 pt1) {
            this.f = iterable;
            this.g = pt1;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            return mu1.a(this.f.iterator(), this.g);
        }
    }

    @DexIgnore
    public static <T> T[] a(Iterable<? extends T> iterable, T[] tArr) {
        return a(iterable).toArray(tArr);
    }

    @DexIgnore
    public static <T> T b(Iterable<T> iterable) {
        return mu1.b(iterable.iterator());
    }

    @DexIgnore
    public static Object[] c(Iterable<?> iterable) {
        return a(iterable).toArray();
    }

    @DexIgnore
    public static String d(Iterable<?> iterable) {
        return mu1.d(iterable.iterator());
    }

    @DexIgnore
    public static <T> Iterable<T> b(Iterable<T> iterable, ut1<? super T> ut1) {
        tt1.a(iterable);
        tt1.a(ut1);
        return new a(iterable, ut1);
    }

    @DexIgnore
    public static <E> Collection<E> a(Iterable<E> iterable) {
        return iterable instanceof Collection ? (Collection) iterable : Lists.a(iterable.iterator());
    }

    @DexIgnore
    public static <T> boolean a(Collection<T> collection, Iterable<? extends T> iterable) {
        if (iterable instanceof Collection) {
            return collection.addAll(du1.a(iterable));
        }
        tt1.a(iterable);
        return mu1.a(collection, iterable.iterator());
    }

    @DexIgnore
    public static <T> boolean a(Iterable<T> iterable, ut1<? super T> ut1) {
        return mu1.a(iterable.iterator(), ut1);
    }

    @DexIgnore
    public static <F, T> Iterable<T> a(Iterable<F> iterable, pt1<? super F, ? extends T> pt1) {
        tt1.a(iterable);
        tt1.a(pt1);
        return new b(iterable, pt1);
    }

    @DexIgnore
    public static <T> T a(Iterable<? extends T> iterable, T t) {
        return mu1.b(iterable.iterator(), t);
    }
}
