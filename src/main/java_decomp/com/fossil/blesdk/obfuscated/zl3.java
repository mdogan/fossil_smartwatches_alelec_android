package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zl3 implements Factory<ProfileSetupPresenter> {
    @DexIgnore
    public static ProfileSetupPresenter a(wl3 wl3, GetRecommendedGoalUseCase getRecommendedGoalUseCase, UserRepository userRepository) {
        return new ProfileSetupPresenter(wl3, getRecommendedGoalUseCase, userRepository);
    }
}
