package com.fossil.blesdk.obfuscated;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.fossil.blesdk.obfuscated.kg1;
import java.lang.reflect.InvocationTargetException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yl1 extends ui1 {
    @DexIgnore
    public Boolean b;
    @DexIgnore
    public am1 c; // = zl1.a;
    @DexIgnore
    public Boolean d;

    @DexIgnore
    public yl1(yh1 yh1) {
        super(yh1);
        kg1.a(yh1);
    }

    @DexIgnore
    public static String s() {
        return kg1.k.a();
    }

    @DexIgnore
    public static long t() {
        return kg1.N.a().longValue();
    }

    @DexIgnore
    public static long u() {
        return kg1.n.a().longValue();
    }

    @DexIgnore
    public static boolean v() {
        return kg1.j.a().booleanValue();
    }

    @DexIgnore
    public static boolean w() {
        return kg1.e0.a().booleanValue();
    }

    @DexIgnore
    public final void a(am1 am1) {
        this.c = am1;
    }

    @DexIgnore
    public final int b(String str, kg1.a<Integer> aVar) {
        if (str == null) {
            return aVar.a().intValue();
        }
        String a = this.c.a(str, aVar.b());
        if (TextUtils.isEmpty(a)) {
            return aVar.a().intValue();
        }
        try {
            return aVar.a(Integer.valueOf(Integer.parseInt(a))).intValue();
        } catch (NumberFormatException unused) {
            return aVar.a().intValue();
        }
    }

    @DexIgnore
    public final boolean c(String str, kg1.a<Boolean> aVar) {
        if (str == null) {
            return aVar.a().booleanValue();
        }
        String a = this.c.a(str, aVar.b());
        if (TextUtils.isEmpty(a)) {
            return aVar.a().booleanValue();
        }
        return aVar.a(Boolean.valueOf(Boolean.parseBoolean(a))).booleanValue();
    }

    @DexIgnore
    public final boolean d(String str, kg1.a<Boolean> aVar) {
        return c(str, aVar);
    }

    @DexIgnore
    public final boolean e(String str) {
        return c(str, kg1.S);
    }

    @DexIgnore
    public final boolean f(String str) {
        return c(str, kg1.U);
    }

    @DexIgnore
    public final boolean g(String str) {
        return c(str, kg1.V);
    }

    @DexIgnore
    public final boolean h(String str) {
        return c(str, kg1.W);
    }

    @DexIgnore
    public final boolean i(String str) {
        return c(str, kg1.X);
    }

    @DexIgnore
    public final boolean j(String str) {
        return c(str, kg1.Z);
    }

    @DexIgnore
    public final boolean k(String str) {
        return c(str, kg1.a0);
    }

    @DexIgnore
    public final boolean l(String str) {
        return c(str, kg1.b0);
    }

    @DexIgnore
    public final boolean m() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    ApplicationInfo applicationInfo = getContext().getApplicationInfo();
                    String a = rm0.a();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        this.d = Boolean.valueOf(str != null && str.equals(a));
                    }
                    if (this.d == null) {
                        this.d = Boolean.TRUE;
                        d().s().a("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.d.booleanValue();
    }

    @DexIgnore
    public final long n() {
        b();
        return 14711;
    }

    @DexIgnore
    public final boolean o() {
        b();
        Boolean b2 = b("firebase_analytics_collection_deactivated");
        return b2 != null && b2.booleanValue();
    }

    @DexIgnore
    public final Boolean p() {
        b();
        return b("firebase_analytics_collection_enabled");
    }

    @DexIgnore
    public final String q() {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", new Class[]{String.class, String.class}).invoke((Object) null, new Object[]{"debug.firebase.analytics.app", ""});
        } catch (ClassNotFoundException e) {
            d().s().a("Could not find SystemProperties class", e);
            return "";
        } catch (NoSuchMethodException e2) {
            d().s().a("Could not find SystemProperties.get() method", e2);
            return "";
        } catch (IllegalAccessException e3) {
            d().s().a("Could not access SystemProperties.get()", e3);
            return "";
        } catch (InvocationTargetException e4) {
            d().s().a("SystemProperties.get() threw an exception", e4);
            return "";
        }
    }

    @DexIgnore
    public final boolean r() {
        if (this.b == null) {
            this.b = b("app_measurement_lite");
            if (this.b == null) {
                this.b = false;
            }
        }
        if (this.b.booleanValue() || !this.a.D()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final int a(String str) {
        return b(str, kg1.y);
    }

    @DexIgnore
    public final boolean d(String str) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(this.c.a(str, "measurement.event_sampling_enabled"));
    }

    @DexIgnore
    public final boolean n(String str) {
        return c(str, kg1.c0);
    }

    @DexIgnore
    public final boolean s(String str) {
        return c(str, kg1.l0);
    }

    @DexIgnore
    public final long a(String str, kg1.a<Long> aVar) {
        if (str == null) {
            return aVar.a().longValue();
        }
        String a = this.c.a(str, aVar.b());
        if (TextUtils.isEmpty(a)) {
            return aVar.a().longValue();
        }
        try {
            return aVar.a(Long.valueOf(Long.parseLong(a))).longValue();
        } catch (NumberFormatException unused) {
            return aVar.a().longValue();
        }
    }

    @DexIgnore
    public final boolean p(String str) {
        return c(str, kg1.g0);
    }

    @DexIgnore
    public final boolean o(String str) {
        return c(str, kg1.f0);
    }

    @DexIgnore
    public final boolean r(String str) {
        return c(str, kg1.i0);
    }

    @DexIgnore
    public final boolean c(String str) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(this.c.a(str, "gaia_collection_enabled"));
    }

    @DexIgnore
    public final Boolean b(String str) {
        ck0.b(str);
        try {
            if (getContext().getPackageManager() == null) {
                d().s().a("Failed to load metadata: PackageManager is null");
                return null;
            }
            ApplicationInfo a = cn0.b(getContext()).a(getContext().getPackageName(), 128);
            if (a == null) {
                d().s().a("Failed to load metadata: ApplicationInfo is null");
                return null;
            } else if (a.metaData == null) {
                d().s().a("Failed to load metadata: Metadata bundle is null");
                return null;
            } else if (!a.metaData.containsKey(str)) {
                return null;
            } else {
                return Boolean.valueOf(a.metaData.getBoolean(str));
            }
        } catch (PackageManager.NameNotFoundException e) {
            d().s().a("Failed to load metadata: Package name not found", e);
            return null;
        }
    }

    @DexIgnore
    public final boolean q(String str) {
        return c(str, kg1.h0);
    }

    @DexIgnore
    public final boolean a(kg1.a<Boolean> aVar) {
        return c((String) null, aVar);
    }

    @DexIgnore
    public final boolean m(String str) {
        return c(str, kg1.d0);
    }
}
