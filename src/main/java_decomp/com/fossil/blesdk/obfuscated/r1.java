package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r1 {
    @DexIgnore
    public static Menu a(Context context, h7 h7Var) {
        return new s1(context, h7Var);
    }

    @DexIgnore
    public static MenuItem a(Context context, i7 i7Var) {
        if (Build.VERSION.SDK_INT >= 16) {
            return new m1(context, i7Var);
        }
        return new l1(context, i7Var);
    }

    @DexIgnore
    public static SubMenu a(Context context, j7 j7Var) {
        return new w1(context, j7Var);
    }
}
