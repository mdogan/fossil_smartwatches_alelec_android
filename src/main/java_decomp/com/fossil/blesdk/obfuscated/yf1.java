package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.maps.StreetViewPanoramaView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yf1 extends xe1 {
    @DexIgnore
    public /* final */ /* synthetic */ fe1 e;

    @DexIgnore
    public yf1(StreetViewPanoramaView.a aVar, fe1 fe1) {
        this.e = fe1;
    }

    @DexIgnore
    public final void a(ne1 ne1) throws RemoteException {
        this.e.a(new he1(ne1));
    }
}
