package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kc {
    @DexIgnore
    public static final lh4 a(jc jcVar) {
        wd4.b(jcVar, "$this$viewModelScope");
        lh4 lh4 = (lh4) jcVar.a("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY");
        if (lh4 != null) {
            return lh4;
        }
        Object a = jcVar.a("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY", new pb(lj4.a((ri4) null, 1, (Object) null).plus(zh4.c().C())));
        wd4.a(a, "setTagIfAbsent(JOB_KEY,\n\u2026patchers.Main.immediate))");
        return (lh4) a;
    }
}
