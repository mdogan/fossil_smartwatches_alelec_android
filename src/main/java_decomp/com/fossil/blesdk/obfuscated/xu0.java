package com.fossil.blesdk.obfuscated;

import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface xu0<E> extends List<E>, RandomAccess {
    @DexIgnore
    xu0<E> c(int i);

    @DexIgnore
    boolean y();

    @DexIgnore
    void z();
}
