package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r03 implements Factory<m03> {
    @DexIgnore
    public static m03 a(o03 o03) {
        m03 d = o03.d();
        o44.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
