package com.fossil.blesdk.obfuscated;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.util.Log;
import com.fossil.blesdk.obfuscated.f7;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g7 extends f7 {
    @DexIgnore
    public static Method l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends f7.a {
        @DexIgnore
        public a(f7.a aVar, Resources resources) {
            super(aVar, resources);
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            return new g7(this, resources);
        }
    }

    @DexIgnore
    public g7(Drawable drawable) {
        super(drawable);
        d();
    }

    @DexIgnore
    public boolean b() {
        if (Build.VERSION.SDK_INT != 21) {
            return false;
        }
        Drawable drawable = this.j;
        if ((drawable instanceof GradientDrawable) || (drawable instanceof DrawableContainer) || (drawable instanceof InsetDrawable) || (drawable instanceof RippleDrawable)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public f7.a c() {
        return new a(this.h, (Resources) null);
    }

    @DexIgnore
    public final void d() {
        if (l == null) {
            try {
                l = Drawable.class.getDeclaredMethod("isProjected", new Class[0]);
            } catch (Exception e) {
                Log.w("WrappedDrawableApi21", "Failed to retrieve Drawable#isProjected() method", e);
            }
        }
    }

    @DexIgnore
    public Rect getDirtyBounds() {
        return this.j.getDirtyBounds();
    }

    @DexIgnore
    public void getOutline(Outline outline) {
        this.j.getOutline(outline);
    }

    @DexIgnore
    public boolean isProjected() {
        Drawable drawable = this.j;
        if (drawable != null) {
            Method method = l;
            if (method != null) {
                try {
                    return ((Boolean) method.invoke(drawable, new Object[0])).booleanValue();
                } catch (Exception e) {
                    Log.w("WrappedDrawableApi21", "Error calling Drawable#isProjected() method", e);
                }
            }
        }
        return false;
    }

    @DexIgnore
    public void setHotspot(float f, float f2) {
        this.j.setHotspot(f, f2);
    }

    @DexIgnore
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        this.j.setHotspotBounds(i, i2, i3, i4);
    }

    @DexIgnore
    public boolean setState(int[] iArr) {
        if (!super.setState(iArr)) {
            return false;
        }
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public void setTint(int i) {
        if (b()) {
            super.setTint(i);
        } else {
            this.j.setTint(i);
        }
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        if (b()) {
            super.setTintList(colorStateList);
        } else {
            this.j.setTintList(colorStateList);
        }
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        if (b()) {
            super.setTintMode(mode);
        } else {
            this.j.setTintMode(mode);
        }
    }

    @DexIgnore
    public g7(f7.a aVar, Resources resources) {
        super(aVar, resources);
        d();
    }
}
