package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileFormatException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class m20<T> {
    @DexIgnore
    public /* final */ Version a;

    @DexIgnore
    public m20(Version version) {
        wd4.b(version, "baseVersion");
        this.a = version;
    }

    @DexIgnore
    public final Version a() {
        return this.a;
    }

    @DexIgnore
    public abstract T a(byte[] bArr) throws FileFormatException;
}
