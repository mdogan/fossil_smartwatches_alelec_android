package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class zl0 extends az0 implements yl0 {
    @DexIgnore
    public static yl0 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGoogleCertificatesApi");
        if (queryLocalInterface instanceof yl0) {
            return (yl0) queryLocalInterface;
        }
        return new am0(iBinder);
    }
}
