package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.facebook.GraphRequest;
import com.facebook.internal.ServerProtocol;
import com.zendesk.sdk.network.Constants;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class r24 {
    @DexIgnore
    public static u14 e; // = f24.b();
    @DexIgnore
    public static r24 f; // = null;
    @DexIgnore
    public static Context g; // = null;
    @DexIgnore
    public DefaultHttpClient a; // = null;
    @DexIgnore
    public z14 b; // = null;
    @DexIgnore
    public StringBuilder c; // = new StringBuilder(4096);
    @DexIgnore
    public long d; // = 0;

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(7:0|1|2|(3:4|5|6)|7|8|9) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0066 */
    public r24(Context context) {
        try {
            g = context.getApplicationContext();
            this.d = System.currentTimeMillis() / 1000;
            this.b = new z14();
            if (i04.q()) {
                Logger.getLogger("org.apache.http.wire").setLevel(Level.FINER);
                Logger.getLogger("org.apache.http.headers").setLevel(Level.FINER);
                System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
                System.setProperty("org.apache.commons.logging.simplelog.showdatetime", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
                System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
                System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http", "debug");
                System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "debug");
            }
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 10000);
            this.a = new DefaultHttpClient(basicHttpParams);
            this.a.setKeepAliveStrategy(new s24(this));
        } catch (Throwable th) {
            e.a(th);
        }
    }

    @DexIgnore
    public static Context a() {
        return g;
    }

    @DexIgnore
    public static void a(Context context) {
        g = context.getApplicationContext();
    }

    @DexIgnore
    public static r24 b(Context context) {
        if (f == null) {
            synchronized (r24.class) {
                if (f == null) {
                    f = new r24(context);
                }
            }
        }
        return f;
    }

    @DexIgnore
    public void a(p04 p04, q24 q24) {
        b(Arrays.asList(new String[]{p04.f()}), q24);
    }

    @DexIgnore
    public void a(List<?> list, q24 q24) {
        Throwable th;
        List<?> list2 = list;
        if (list2 != null && !list.isEmpty()) {
            int size = list.size();
            list2.get(0);
            try {
                this.c.delete(0, this.c.length());
                this.c.append("[");
                for (int i = 0; i < size; i++) {
                    this.c.append(list2.get(i).toString());
                    if (i != size - 1) {
                        this.c.append(",");
                    }
                }
                this.c.append("]");
                String sb = this.c.toString();
                int length = sb.length();
                String str = i04.n() + "/?index=" + this.d;
                this.d++;
                if (i04.q()) {
                    e.e("[" + str + "]Send request(" + length + "bytes), content:" + sb);
                }
                HttpPost httpPost = new HttpPost(str);
                httpPost.addHeader("Accept-Encoding", "gzip");
                httpPost.setHeader("Connection", "Keep-Alive");
                httpPost.removeHeaders(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER);
                HttpHost a2 = u04.a(g).a();
                httpPost.addHeader(GraphRequest.CONTENT_ENCODING_HEADER, "rc4");
                if (a2 == null) {
                    this.a.getParams().removeParameter("http.route.default-proxy");
                } else {
                    if (i04.q()) {
                        e.a((Object) "proxy:" + a2.toHostString());
                    }
                    httpPost.addHeader("X-Content-Encoding", "rc4");
                    this.a.getParams().setParameter("http.route.default-proxy", a2);
                    httpPost.addHeader("X-Online-Host", i04.B);
                    httpPost.addHeader(Constants.ACCEPT_HEADER, "*/*");
                    httpPost.addHeader(GraphRequest.CONTENT_TYPE_HEADER, "json");
                }
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(length);
                byte[] bytes = sb.getBytes("UTF-8");
                int length2 = bytes.length;
                if (length > i04.L) {
                    httpPost.removeHeaders(GraphRequest.CONTENT_ENCODING_HEADER);
                    String str2 = "rc4" + ",gzip";
                    httpPost.addHeader(GraphRequest.CONTENT_ENCODING_HEADER, str2);
                    if (a2 != null) {
                        httpPost.removeHeaders("X-Content-Encoding");
                        httpPost.addHeader("X-Content-Encoding", str2);
                    }
                    byteArrayOutputStream.write(new byte[4]);
                    GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                    gZIPOutputStream.write(bytes);
                    gZIPOutputStream.close();
                    bytes = byteArrayOutputStream.toByteArray();
                    ByteBuffer.wrap(bytes, 0, 4).putInt(length2);
                    if (i04.q()) {
                        e.a((Object) "before Gzip:" + length2 + " bytes, after Gzip:" + bytes.length + " bytes");
                    }
                }
                httpPost.setEntity(new ByteArrayEntity(a24.a(bytes)));
                HttpResponse execute = this.a.execute(httpPost);
                HttpEntity entity = execute.getEntity();
                int statusCode = execute.getStatusLine().getStatusCode();
                long contentLength = entity.getContentLength();
                if (i04.q()) {
                    e.e("http recv response status code:" + statusCode + ", content length:" + contentLength);
                }
                int i2 = (contentLength > 0 ? 1 : (contentLength == 0 ? 0 : -1));
                if (i2 <= 0) {
                    e.c("Server response no data.");
                    if (q24 != null) {
                        q24.b();
                    }
                    EntityUtils.toString(entity);
                    return;
                }
                if (i2 > 0) {
                    InputStream content = entity.getContent();
                    DataInputStream dataInputStream = new DataInputStream(content);
                    byte[] bArr = new byte[((int) entity.getContentLength())];
                    dataInputStream.readFully(bArr);
                    content.close();
                    dataInputStream.close();
                    Header firstHeader = execute.getFirstHeader(GraphRequest.CONTENT_ENCODING_HEADER);
                    if (firstHeader != null) {
                        if (firstHeader.getValue().equalsIgnoreCase("gzip,rc4")) {
                            bArr = a24.b(f24.a(bArr));
                        } else if (firstHeader.getValue().equalsIgnoreCase("rc4,gzip")) {
                            bArr = f24.a(a24.b(bArr));
                        } else if (firstHeader.getValue().equalsIgnoreCase("gzip")) {
                            bArr = f24.a(bArr);
                        } else if (firstHeader.getValue().equalsIgnoreCase("rc4")) {
                            bArr = a24.b(bArr);
                        }
                    }
                    String str3 = new String(bArr, "UTF-8");
                    if (i04.q()) {
                        e.e("http get response data:" + str3);
                    }
                    JSONObject jSONObject = new JSONObject(str3);
                    if (statusCode == 200) {
                        a(jSONObject);
                        if (q24 != null) {
                            if (jSONObject.optInt("ret") == 0) {
                                q24.a();
                            } else {
                                e.d("response error data.");
                            }
                        }
                        content.close();
                    } else {
                        e.d("Server response error code:" + statusCode + ", error:" + new String(bArr, "UTF-8"));
                        if (q24 != null) {
                        }
                        content.close();
                    }
                    q24.b();
                    content.close();
                } else {
                    EntityUtils.toString(entity);
                }
                byteArrayOutputStream.close();
                th = null;
                if (th != null) {
                    e.b(th);
                    if (q24 != null) {
                        try {
                            q24.b();
                        } catch (Throwable th2) {
                            e.a(th2);
                        }
                    }
                    if (th instanceof OutOfMemoryError) {
                        System.gc();
                        this.c = null;
                        this.c = new StringBuilder(2048);
                    }
                    u04.a(g).d();
                }
            } catch (Throwable th3) {
                th = th3;
            }
        }
    }

    @DexIgnore
    public final void a(JSONObject jSONObject) {
        try {
            String optString = jSONObject.optString("mid");
            if (xy3.b(optString)) {
                if (i04.q()) {
                    u14 u14 = e;
                    u14.e("update mid:" + optString);
                }
                wy3.a(g).a(optString);
            }
            if (!jSONObject.isNull("cfg")) {
                i04.a(g, jSONObject.getJSONObject("cfg"));
            }
            if (!jSONObject.isNull("ncts")) {
                int i = jSONObject.getInt("ncts");
                int currentTimeMillis = (int) (((long) i) - (System.currentTimeMillis() / 1000));
                if (i04.q()) {
                    u14 u142 = e;
                    u142.e("server time:" + i + ", diff time:" + currentTimeMillis);
                }
                f24.C(g);
                f24.a(g, currentTimeMillis);
            }
        } catch (Throwable th) {
            e.g(th);
        }
    }

    @DexIgnore
    public void b(List<?> list, q24 q24) {
        z14 z14 = this.b;
        if (z14 != null) {
            z14.a(new t24(this, list, q24));
        }
    }
}
