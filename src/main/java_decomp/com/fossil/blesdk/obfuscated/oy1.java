package com.fossil.blesdk.obfuscated;

import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class oy1 implements ThreadFactory {
    @DexIgnore
    public static /* final */ ThreadFactory a; // = new oy1();

    @DexIgnore
    public final Thread newThread(Runnable runnable) {
        return ny1.a(runnable);
    }
}
