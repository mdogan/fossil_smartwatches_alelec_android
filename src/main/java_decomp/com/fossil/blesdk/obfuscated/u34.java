package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;
import android.content.Context;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u34 {
    @DexIgnore
    public static void a(Activity activity) {
        o44.a(activity, Constants.ACTIVITY);
        Application application = activity.getApplication();
        if (application instanceof d44) {
            v34<Activity> d = ((d44) application).d();
            o44.a(d, "%s.activityInjector() returned null", application.getClass());
            d.a(activity);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", new Object[]{application.getClass().getCanonicalName(), d44.class.getCanonicalName()}));
    }

    @DexIgnore
    public static void a(Service service) {
        o44.a(service, Constants.SERVICE);
        Application application = service.getApplication();
        if (application instanceof h44) {
            v34<Service> b = ((h44) application).b();
            o44.a(b, "%s.serviceInjector() returned null", application.getClass());
            b.a(service);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", new Object[]{application.getClass().getCanonicalName(), h44.class.getCanonicalName()}));
    }

    @DexIgnore
    public static void a(BroadcastReceiver broadcastReceiver, Context context) {
        o44.a(broadcastReceiver, "broadcastReceiver");
        o44.a(context, "context");
        Application application = (Application) context.getApplicationContext();
        if (application instanceof e44) {
            v34<BroadcastReceiver> a = ((e44) application).a();
            o44.a(a, "%s.broadcastReceiverInjector() returned null", application.getClass());
            a.a(broadcastReceiver);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", new Object[]{application.getClass().getCanonicalName(), e44.class.getCanonicalName()}));
    }

    @DexIgnore
    public static void a(ContentProvider contentProvider) {
        o44.a(contentProvider, "contentProvider");
        Application application = (Application) contentProvider.getContext().getApplicationContext();
        if (application instanceof f44) {
            v34<ContentProvider> c = ((f44) application).c();
            o44.a(c, "%s.contentProviderInjector() returned null", application.getClass());
            c.a(contentProvider);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", new Object[]{application.getClass().getCanonicalName(), f44.class.getCanonicalName()}));
    }
}
