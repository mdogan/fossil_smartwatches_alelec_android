package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h52 implements Factory<kn2> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public h52(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static h52 a(o42 o42) {
        return new h52(o42);
    }

    @DexIgnore
    public static kn2 b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static kn2 c(o42 o42) {
        kn2 j = o42.j();
        o44.a(j, "Cannot return null from a non-@Nullable @Provides method");
        return j;
    }

    @DexIgnore
    public kn2 get() {
        return b(this.a);
    }
}
