package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wk extends vk<hk> {
    @DexIgnore
    public static /* final */ String j; // = ej.a("NetworkStateTracker");
    @DexIgnore
    public /* final */ ConnectivityManager g; // = ((ConnectivityManager) this.b.getSystemService("connectivity"));
    @DexIgnore
    public b h;
    @DexIgnore
    public a i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null && intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                ej.a().a(wk.j, "Network broadcast received", new Throwable[0]);
                wk wkVar = wk.this;
                wkVar.a(wkVar.d());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ConnectivityManager.NetworkCallback {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
            ej.a().a(wk.j, String.format("Network capabilities changed: %s", new Object[]{networkCapabilities}), new Throwable[0]);
            wk wkVar = wk.this;
            wkVar.a(wkVar.d());
        }

        @DexIgnore
        public void onLost(Network network) {
            ej.a().a(wk.j, "Network connection lost", new Throwable[0]);
            wk wkVar = wk.this;
            wkVar.a(wkVar.d());
        }
    }

    @DexIgnore
    public wk(Context context, am amVar) {
        super(context, amVar);
        if (f()) {
            this.h = new b();
        } else {
            this.i = new a();
        }
    }

    @DexIgnore
    public static boolean f() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @DexIgnore
    public void b() {
        if (f()) {
            try {
                ej.a().a(j, "Registering network callback", new Throwable[0]);
                this.g.registerDefaultNetworkCallback(this.h);
            } catch (IllegalArgumentException e) {
                ej.a().b(j, "Received exception while unregistering network callback", e);
            }
        } else {
            ej.a().a(j, "Registering broadcast receiver", new Throwable[0]);
            this.b.registerReceiver(this.i, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    @DexIgnore
    public void c() {
        if (f()) {
            try {
                ej.a().a(j, "Unregistering network callback", new Throwable[0]);
                this.g.unregisterNetworkCallback(this.h);
            } catch (IllegalArgumentException e) {
                ej.a().b(j, "Received exception while unregistering network callback", e);
            }
        } else {
            ej.a().a(j, "Unregistering broadcast receiver", new Throwable[0]);
            this.b.unregisterReceiver(this.i);
        }
    }

    @DexIgnore
    public hk d() {
        NetworkInfo activeNetworkInfo = this.g.getActiveNetworkInfo();
        boolean z = true;
        boolean z2 = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        boolean e = e();
        boolean a2 = l7.a(this.g);
        if (activeNetworkInfo == null || activeNetworkInfo.isRoaming()) {
            z = false;
        }
        return new hk(z2, e, a2, z);
    }

    @DexIgnore
    public final boolean e() {
        if (Build.VERSION.SDK_INT < 23) {
            return false;
        }
        NetworkCapabilities networkCapabilities = this.g.getNetworkCapabilities(this.g.getActiveNetwork());
        if (networkCapabilities == null || !networkCapabilities.hasCapability(16)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public hk a() {
        return d();
    }
}
