package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import com.zendesk.belvedere.BelvedereSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class d34 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<d34> CREATOR; // = new a();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ Intent f;
    @DexIgnore
    public /* final */ BelvedereSource g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<d34> {
        @DexIgnore
        public d34 createFromParcel(Parcel parcel) {
            return new d34(parcel, (a) null);
        }

        @DexIgnore
        public d34[] newArray(int i) {
            return new d34[i];
        }
    }

    @DexIgnore
    public /* synthetic */ d34(Parcel parcel, a aVar) {
        this(parcel);
    }

    @DexIgnore
    public BelvedereSource a() {
        return this.g;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.e);
        parcel.writeParcelable(this.f, i);
        parcel.writeSerializable(this.g);
    }

    @DexIgnore
    public d34(Intent intent, int i, BelvedereSource belvedereSource) {
        this.f = intent;
        this.e = i;
        this.g = belvedereSource;
    }

    @DexIgnore
    public void a(Activity activity) {
        activity.startActivityForResult(this.f, this.e);
    }

    @DexIgnore
    public void a(Fragment fragment) {
        fragment.startActivityForResult(this.f, this.e);
    }

    @DexIgnore
    public d34(Parcel parcel) {
        this.e = parcel.readInt();
        this.f = (Intent) parcel.readParcelable(d34.class.getClassLoader());
        this.g = (BelvedereSource) parcel.readSerializable();
    }
}
