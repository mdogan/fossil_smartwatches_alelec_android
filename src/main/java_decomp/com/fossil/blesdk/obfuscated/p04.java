package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.a.f;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class p04 {
    @DexIgnore
    public static String l;
    @DexIgnore
    public String a; // = null;
    @DexIgnore
    public long b;
    @DexIgnore
    public int c;
    @DexIgnore
    public v14 d; // = null;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = null;
    @DexIgnore
    public String g; // = null;
    @DexIgnore
    public String h; // = null;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public Context j;
    @DexIgnore
    public l04 k; // = null;

    @DexIgnore
    public p04(Context context, int i2, l04 l04) {
        this.j = context;
        this.b = System.currentTimeMillis() / 1000;
        this.c = i2;
        this.g = i04.d(context);
        this.h = f24.m(context);
        this.a = i04.b(context);
        if (l04 != null) {
            this.k = l04;
            if (f24.c(l04.a())) {
                this.a = l04.a();
            }
            if (f24.c(l04.b())) {
                this.g = l04.b();
            }
            if (f24.c(l04.c())) {
                this.h = l04.c();
            }
            this.i = l04.d();
        }
        this.f = i04.c(context);
        this.d = h14.b(context).a(context);
        f a2 = a();
        f fVar = f.NETWORK_DETECTOR;
        this.e = a2 != fVar ? f24.v(context).intValue() : -fVar.a();
        if (!xy3.b(l)) {
            String e2 = i04.e(context);
            l = e2;
            if (!f24.c(e2)) {
                l = "0";
            }
        }
    }

    @DexIgnore
    public abstract f a();

    @DexIgnore
    public abstract boolean a(JSONObject jSONObject);

    @DexIgnore
    public long b() {
        return this.b;
    }

    @DexIgnore
    public boolean b(JSONObject jSONObject) {
        try {
            k24.a(jSONObject, "ky", this.a);
            jSONObject.put("et", a().a());
            if (this.d != null) {
                jSONObject.put("ui", this.d.b());
                k24.a(jSONObject, "mc", this.d.c());
                int d2 = this.d.d();
                jSONObject.put("ut", d2);
                if (d2 == 0 && f24.z(this.j) == 1) {
                    jSONObject.put("ia", 1);
                }
            }
            k24.a(jSONObject, "cui", this.f);
            if (a() != f.SESSION_ENV) {
                k24.a(jSONObject, "av", this.h);
                k24.a(jSONObject, "ch", this.g);
            }
            if (this.i) {
                jSONObject.put("impt", 1);
            }
            k24.a(jSONObject, "mid", l);
            jSONObject.put("idx", this.e);
            jSONObject.put("si", this.c);
            jSONObject.put("ts", this.b);
            jSONObject.put("dts", f24.a(this.j, false));
            return a(jSONObject);
        } catch (Throwable unused) {
            return false;
        }
    }

    @DexIgnore
    public l04 c() {
        return this.k;
    }

    @DexIgnore
    public Context d() {
        return this.j;
    }

    @DexIgnore
    public boolean e() {
        return this.i;
    }

    @DexIgnore
    public String f() {
        try {
            JSONObject jSONObject = new JSONObject();
            b(jSONObject);
            return jSONObject.toString();
        } catch (Throwable unused) {
            return "";
        }
    }
}
