package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oq2 extends mq2 {
    @DexIgnore
    public boolean d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oq2(String str, String str2, boolean z) {
        super(str, str2);
        wd4.b(str, "tagName");
        wd4.b(str2, "title");
        this.d = z;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }
}
