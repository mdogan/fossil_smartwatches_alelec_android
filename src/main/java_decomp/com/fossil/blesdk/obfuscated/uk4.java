package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface uk4 {
    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(tk4<?> tk4);

    @DexIgnore
    tk4<?> i();

    @DexIgnore
    int j();
}
