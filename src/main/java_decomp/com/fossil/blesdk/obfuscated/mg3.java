package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.underamour.UAWebViewActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mg3 extends as2 implements ku2 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public ur3<pa2> j;
    @DexIgnore
    public ju2 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return mg3.m;
        }

        @DexIgnore
        public final mg3 b() {
            return new mg3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ mg3 a;

        @DexIgnore
        public b(mg3 mg3) {
            this.a = mg3;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            wd4.b(compoundButton, "button");
            if (compoundButton.isPressed()) {
                mg3.a(this.a).h();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ mg3 a;

        @DexIgnore
        public c(mg3 mg3) {
            this.a = mg3;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            wd4.b(compoundButton, "button");
            if (compoundButton.isPressed()) {
                mg3.a(this.a).j();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mg3 e;

        @DexIgnore
        public d(mg3 mg3) {
            this.e = mg3;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.e.getActivity() != null) {
                FragmentActivity activity = this.e.getActivity();
                if (activity != null) {
                    activity.finish();
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }
    }

    /*
    static {
        String simpleName = mg3.class.getSimpleName();
        if (simpleName != null) {
            wd4.a((Object) simpleName, "ConnectedAppsFragment::class.java.simpleName!!");
            m = simpleName;
            return;
        }
        wd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ju2 a(mg3 mg3) {
        ju2 ju2 = mg3.k;
        if (ju2 != null) {
            return ju2;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void G(String str) {
        wd4.b(str, "authorizationUrl");
        UAWebViewActivity.a aVar = UAWebViewActivity.H;
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wd4.a((Object) activity, "activity!!");
            aVar.a(activity, str);
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return m;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void i() {
        a();
    }

    @DexIgnore
    public void k() {
        b();
    }

    @DexIgnore
    public void o(int i) {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(i, "", childFragmentManager);
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 3534) {
            if (i2 == -1) {
                String stringExtra = intent != null ? intent.getStringExtra("code") : null;
                if (stringExtra != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = m;
                    local.d(str, "We got a UA code! " + stringExtra);
                    ju2 ju2 = this.k;
                    if (ju2 != null) {
                        ju2.a(stringExtra);
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
            } else {
                FLogger.INSTANCE.getLocal().e(m, "Something went wrong with UA login");
            }
        } else if (i == 1 && i2 == -1) {
            ju2 ju22 = this.k;
            if (ju22 != null) {
                ju22.i();
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new ur3<>(this, (pa2) ra.a(layoutInflater, R.layout.fragment_connected_apps, viewGroup, false, O0()));
        ur3<pa2> ur3 = this.j;
        if (ur3 != null) {
            pa2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        ju2 ju2 = this.k;
        if (ju2 != null) {
            ju2.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ju2 ju2 = this.k;
        if (ju2 != null) {
            ju2.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<pa2> ur3 = this.j;
        if (ur3 != null) {
            pa2 a2 = ur3.a();
            if (a2 != null) {
                a2.q.setOnCheckedChangeListener(new b(this));
                a2.r.setOnCheckedChangeListener(new c(this));
                a2.s.setOnClickListener(new d(this));
            }
            R("connected_app_view");
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void x(boolean z) {
        if (isActive()) {
            ur3<pa2> ur3 = this.j;
            if (ur3 != null) {
                pa2 a2 = ur3.a();
                if (a2 != null) {
                    SwitchCompat switchCompat = a2.r;
                    wd4.a((Object) switchCompat, "it.cbUnderArmour");
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void y(boolean z) {
        if (isActive()) {
            ur3<pa2> ur3 = this.j;
            if (ur3 != null) {
                pa2 a2 = ur3.a();
                if (a2 != null) {
                    SwitchCompat switchCompat = a2.q;
                    wd4.a((Object) switchCompat, "it.cbGooglefit");
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(ju2 ju2) {
        wd4.b(ju2, "presenter");
        this.k = ju2;
    }

    @DexIgnore
    public void a(vd0 vd0) {
        wd4.b(vd0, "connectionResult");
        vd0.a(getActivity(), 1);
    }
}
