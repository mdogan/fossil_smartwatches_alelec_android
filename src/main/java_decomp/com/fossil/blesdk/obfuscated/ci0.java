package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ci0 extends LifecycleCallback implements DialogInterface.OnCancelListener {
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public /* final */ AtomicReference<di0> g;
    @DexIgnore
    public /* final */ Handler h;
    @DexIgnore
    public /* final */ yd0 i;

    @DexIgnore
    public ci0(ze0 ze0) {
        this(ze0, yd0.a());
    }

    @DexIgnore
    public void a(Bundle bundle) {
        super.a(bundle);
        if (bundle != null) {
            this.g.set(bundle.getBoolean("resolving_error", false) ? new di0(new vd0(bundle.getInt("failed_status"), (PendingIntent) bundle.getParcelable("failed_resolution")), bundle.getInt("failed_client_id", -1)) : null);
        }
    }

    @DexIgnore
    public abstract void a(vd0 vd0, int i2);

    @DexIgnore
    public void b(Bundle bundle) {
        super.b(bundle);
        di0 di0 = this.g.get();
        if (di0 != null) {
            bundle.putBoolean("resolving_error", true);
            bundle.putInt("failed_client_id", di0.b());
            bundle.putInt("failed_status", di0.a().H());
            bundle.putParcelable("failed_resolution", di0.a().J());
        }
    }

    @DexIgnore
    public void d() {
        super.d();
        this.f = true;
    }

    @DexIgnore
    public void e() {
        super.e();
        this.f = false;
    }

    @DexIgnore
    public abstract void f();

    @DexIgnore
    public final void g() {
        this.g.set((Object) null);
        f();
    }

    @DexIgnore
    public void onCancel(DialogInterface dialogInterface) {
        a(new vd0(13, (PendingIntent) null), a(this.g.get()));
        g();
    }

    @DexIgnore
    public ci0(ze0 ze0, yd0 yd0) {
        super(ze0);
        this.g = new AtomicReference<>((Object) null);
        this.h = new ts0(Looper.getMainLooper());
        this.i = yd0;
    }

    @DexIgnore
    public final void b(vd0 vd0, int i2) {
        di0 di0 = new di0(vd0, i2);
        if (this.g.compareAndSet((Object) null, di0)) {
            this.h.post(new ei0(this, di0));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005c  */
    public void a(int i2, int i3, Intent intent) {
        di0 di0 = this.g.get();
        boolean z = true;
        if (i2 == 1) {
            if (i3 != -1) {
                if (i3 == 0) {
                    int i4 = 13;
                    if (intent != null) {
                        i4 = intent.getIntExtra("<<ResolutionFailureErrorDetail>>", 13);
                    }
                    di0 di02 = new di0(new vd0(i4, (PendingIntent) null), a(di0));
                    this.g.set(di02);
                    di0 = di02;
                }
            }
            if (z) {
            }
        } else if (i2 == 2) {
            int c = this.i.c((Context) a());
            if (c != 0) {
                z = false;
            }
            if (di0 != null) {
                if (di0.a().H() == 18 && c == 18) {
                    return;
                }
                if (z) {
                    g();
                    return;
                } else if (di0 != null) {
                    a(di0.a(), di0.b());
                    return;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
        z = false;
        if (z) {
        }
    }

    @DexIgnore
    public static int a(di0 di0) {
        if (di0 == null) {
            return -1;
        }
        return di0.b();
    }
}
