package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kb0 {
    @DexIgnore
    public static /* final */ kb0 a; // = new kb0();

    @DexIgnore
    public final boolean a(Context context, String[] strArr) {
        wd4.b(context, "context");
        wd4.b(strArr, "permissions");
        boolean z = true;
        for (String a2 : strArr) {
            z = z && k6.a(context, a2) == 0;
        }
        return z;
    }
}
