package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.fossil.blesdk.obfuscated.we0;
import java.util.Collections;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yg0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ vd0 e;
    @DexIgnore
    public /* final */ /* synthetic */ we0.c f;

    @DexIgnore
    public yg0(we0.c cVar, vd0 vd0) {
        this.f = cVar;
        this.e = vd0;
    }

    @DexIgnore
    public final void run() {
        if (this.e.L()) {
            boolean unused = this.f.e = true;
            if (this.f.a.l()) {
                this.f.a();
                return;
            }
            try {
                this.f.a.a((uj0) null, Collections.emptySet());
            } catch (SecurityException e2) {
                Log.e("GoogleApiManager", "Failed to get service from broker. ", e2);
                ((we0.a) we0.this.i.get(this.f.b)).a(new vd0(10));
            }
        } else {
            ((we0.a) we0.this.i.get(this.f.b)).a(this.e);
        }
    }
}
