package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ig1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ig1> CREATOR; // = new jg1();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ fg1 f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ long h;

    @DexIgnore
    public ig1(String str, fg1 fg1, String str2, long j) {
        this.e = str;
        this.f = fg1;
        this.g = str2;
        this.h = j;
    }

    @DexIgnore
    public final String toString() {
        String str = this.g;
        String str2 = this.e;
        String valueOf = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("origin=");
        sb.append(str);
        sb.append(",name=");
        sb.append(str2);
        sb.append(",params=");
        sb.append(valueOf);
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 2, this.e, false);
        lk0.a(parcel, 3, (Parcelable) this.f, i, false);
        lk0.a(parcel, 4, this.g, false);
        lk0.a(parcel, 5, this.h);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public ig1(ig1 ig1, long j) {
        ck0.a(ig1);
        this.e = ig1.e;
        this.f = ig1.f;
        this.g = ig1.g;
        this.h = j;
    }
}
