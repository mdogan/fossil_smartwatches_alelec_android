package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.concurrent.locks.LockSupport;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ig4<T> extends gg4<T> {
    @DexIgnore
    public /* final */ Thread h;
    @DexIgnore
    public /* final */ ei4 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ig4(CoroutineContext coroutineContext, Thread thread, ei4 ei4) {
        super(coroutineContext, true);
        wd4.b(coroutineContext, "parentContext");
        wd4.b(thread, "blockedThread");
        this.h = thread;
        this.i = ei4;
    }

    @DexIgnore
    public void a(Object obj, int i2) {
        if (!wd4.a((Object) Thread.currentThread(), (Object) this.h)) {
            LockSupport.unpark(this.h);
        }
    }

    @DexIgnore
    public boolean f() {
        return true;
    }

    @DexIgnore
    public final T m() {
        oj4 a = pj4.a();
        if (a != null) {
            a.b();
        }
        try {
            ei4 ei4 = this.i;
            T t = null;
            if (ei4 != null) {
                ei4.b(ei4, false, 1, (Object) null);
            }
            while (!Thread.interrupted()) {
                ei4 ei42 = this.i;
                long F = ei42 != null ? ei42.F() : ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
                if (e()) {
                    ei4 ei43 = this.i;
                    if (ei43 != null) {
                        ei4.a(ei43, false, 1, (Object) null);
                    }
                    oj4 a2 = pj4.a();
                    if (a2 != null) {
                        a2.d();
                    }
                    T b = yi4.b(d());
                    if (b instanceof zg4) {
                        t = b;
                    }
                    zg4 zg4 = (zg4) t;
                    if (zg4 == null) {
                        return b;
                    }
                    throw zg4.a;
                }
                oj4 a3 = pj4.a();
                if (a3 != null) {
                    a3.a(this, F);
                } else {
                    LockSupport.parkNanos(this, F);
                }
            }
            InterruptedException interruptedException = new InterruptedException();
            a((Throwable) interruptedException);
            throw interruptedException;
        } catch (Throwable th) {
            oj4 a4 = pj4.a();
            if (a4 != null) {
                a4.d();
            }
            throw th;
        }
    }
}
