package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.he0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class s41 extends pj0<v31> {
    @DexIgnore
    public /* final */ String E;
    @DexIgnore
    public /* final */ n41<v31> F; // = new t41(this);

    @DexIgnore
    public s41(Context context, Looper looper, he0.b bVar, he0.c cVar, String str, lj0 lj0) {
        super(context, looper, 23, lj0, bVar, cVar);
        this.E = str;
    }

    @DexIgnore
    public /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.internal.IGoogleLocationManagerService");
        return queryLocalInterface instanceof v31 ? (v31) queryLocalInterface : new w31(iBinder);
    }

    @DexIgnore
    public int i() {
        return 11925000;
    }

    @DexIgnore
    public Bundle u() {
        Bundle bundle = new Bundle();
        bundle.putString("client_name", this.E);
        return bundle;
    }

    @DexIgnore
    public String y() {
        return "com.google.android.gms.location.internal.IGoogleLocationManagerService";
    }

    @DexIgnore
    public String z() {
        return "com.google.android.location.internal.GoogleLocationManagerService.START";
    }
}
