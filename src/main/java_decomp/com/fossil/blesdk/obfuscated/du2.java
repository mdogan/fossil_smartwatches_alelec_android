package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class du2 implements Factory<String> {
    @DexIgnore
    public static String a(bu2 bu2) {
        String c = bu2.c();
        o44.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
