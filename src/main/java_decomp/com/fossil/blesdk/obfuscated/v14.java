package com.fossil.blesdk.obfuscated;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class v14 {
    @DexIgnore
    public String a; // = null;
    @DexIgnore
    public String b; // = null;
    @DexIgnore
    public String c; // = null;
    @DexIgnore
    public String d; // = "0";
    @DexIgnore
    public int e;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public long g; // = 0;

    @DexIgnore
    public v14(String str, String str2, int i) {
        this.a = str;
        this.b = str2;
        this.e = i;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            k24.a(jSONObject, "ui", this.a);
            k24.a(jSONObject, "mc", this.b);
            k24.a(jSONObject, "mid", this.d);
            k24.a(jSONObject, "aid", this.c);
            jSONObject.put("ts", this.g);
            jSONObject.put("ver", this.f);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    @DexIgnore
    public void a(int i) {
        this.e = i;
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public String c() {
        return this.b;
    }

    @DexIgnore
    public int d() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        return a().toString();
    }
}
