package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wd2 extends vd2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public long z;

    /*
    static {
        B.put(R.id.iv_back, 1);
        B.put(R.id.ftv_title, 2);
        B.put(R.id.iv_done, 3);
        B.put(R.id.ftv_assign_section, 4);
        B.put(R.id.rv_assign, 5);
        B.put(R.id.cl_sections_container, 6);
        B.put(R.id.ftv_select_section, 7);
        B.put(R.id.ll_my_contacts, 8);
        B.put(R.id.ftv_my_contacts_overview, 9);
        B.put(R.id.v_line1, 10);
        B.put(R.id.ll_my_apps, 11);
        B.put(R.id.ftv_my_apps_overview, 12);
        B.put(R.id.v_line2, 13);
        B.put(R.id.ll_everyone, 14);
        B.put(R.id.ftv_everyone_overview, 15);
        B.put(R.id.v_line3, 16);
    }
    */

    @DexIgnore
    public wd2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 17, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public wd2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[6], objArr[4], objArr[15], objArr[12], objArr[9], objArr[7], objArr[2], objArr[1], objArr[3], objArr[14], objArr[11], objArr[8], objArr[5], objArr[10], objArr[13], objArr[16]);
        this.z = -1;
        this.y = objArr[0];
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
