package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class pf2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FloatingActionButton A;
    @DexIgnore
    public /* final */ FloatingActionButton B;
    @DexIgnore
    public /* final */ DashBar C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ TextInputEditText r;
    @DexIgnore
    public /* final */ TextInputEditText s;
    @DexIgnore
    public /* final */ TextInputLayout t;
    @DexIgnore
    public /* final */ TextInputLayout u;
    @DexIgnore
    public /* final */ ImageView v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ ImageView x;
    @DexIgnore
    public /* final */ ImageView y;
    @DexIgnore
    public /* final */ ImageView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pf2(Object obj, View view, int i, Barrier barrier, Barrier barrier2, FlexibleButton flexibleButton, TextInputEditText textInputEditText, TextInputEditText textInputEditText2, TextInputLayout textInputLayout, TextInputLayout textInputLayout2, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, ImageView imageView5, FloatingActionButton floatingActionButton, FloatingActionButton floatingActionButton2, ConstraintLayout constraintLayout, DashBar dashBar, ScrollView scrollView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = textInputEditText;
        this.s = textInputEditText2;
        this.t = textInputLayout;
        this.u = textInputLayout2;
        this.v = imageView;
        this.w = imageView2;
        this.x = imageView3;
        this.y = imageView4;
        this.z = imageView5;
        this.A = floatingActionButton;
        this.B = floatingActionButton2;
        this.C = dashBar;
        this.D = flexibleTextView2;
        this.E = flexibleTextView3;
        this.F = flexibleTextView4;
        this.G = flexibleTextView5;
    }
}
