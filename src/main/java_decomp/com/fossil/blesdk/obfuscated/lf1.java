package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.tn0;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lf1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<lf1> CREATOR; // = new rf1();
    @DexIgnore
    public LatLng e;
    @DexIgnore
    public String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public if1 h;
    @DexIgnore
    public float i; // = 0.5f;
    @DexIgnore
    public float j; // = 1.0f;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l; // = true;
    @DexIgnore
    public boolean m; // = false;
    @DexIgnore
    public float n; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public float o; // = 0.5f;
    @DexIgnore
    public float p; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public float q; // = 1.0f;
    @DexIgnore
    public float r;

    @DexIgnore
    public lf1() {
    }

    @DexIgnore
    public final float H() {
        return this.q;
    }

    @DexIgnore
    public final float I() {
        return this.i;
    }

    @DexIgnore
    public final float J() {
        return this.j;
    }

    @DexIgnore
    public final float K() {
        return this.o;
    }

    @DexIgnore
    public final float L() {
        return this.p;
    }

    @DexIgnore
    public final LatLng M() {
        return this.e;
    }

    @DexIgnore
    public final float N() {
        return this.n;
    }

    @DexIgnore
    public final String O() {
        return this.g;
    }

    @DexIgnore
    public final String P() {
        return this.f;
    }

    @DexIgnore
    public final float Q() {
        return this.r;
    }

    @DexIgnore
    public final boolean R() {
        return this.k;
    }

    @DexIgnore
    public final boolean S() {
        return this.m;
    }

    @DexIgnore
    public final boolean T() {
        return this.l;
    }

    @DexIgnore
    public final lf1 a(LatLng latLng) {
        if (latLng != null) {
            this.e = latLng;
            return this;
        }
        throw new IllegalArgumentException("latlng cannot be null - a position is required.");
    }

    @DexIgnore
    public final lf1 e(String str) {
        this.g = str;
        return this;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        IBinder iBinder;
        int a = lk0.a(parcel);
        lk0.a(parcel, 2, (Parcelable) M(), i2, false);
        lk0.a(parcel, 3, P(), false);
        lk0.a(parcel, 4, O(), false);
        if1 if1 = this.h;
        if (if1 == null) {
            iBinder = null;
        } else {
            iBinder = if1.a().asBinder();
        }
        lk0.a(parcel, 5, iBinder, false);
        lk0.a(parcel, 6, I());
        lk0.a(parcel, 7, J());
        lk0.a(parcel, 8, R());
        lk0.a(parcel, 9, T());
        lk0.a(parcel, 10, S());
        lk0.a(parcel, 11, N());
        lk0.a(parcel, 12, K());
        lk0.a(parcel, 13, L());
        lk0.a(parcel, 14, H());
        lk0.a(parcel, 15, Q());
        lk0.a(parcel, a);
    }

    @DexIgnore
    public final lf1 a(if1 if1) {
        this.h = if1;
        return this;
    }

    @DexIgnore
    public lf1(LatLng latLng, String str, String str2, IBinder iBinder, float f2, float f3, boolean z, boolean z2, boolean z3, float f4, float f5, float f6, float f7, float f8) {
        this.e = latLng;
        this.f = str;
        this.g = str2;
        if (iBinder == null) {
            this.h = null;
        } else {
            this.h = new if1(tn0.a.a(iBinder));
        }
        this.i = f2;
        this.j = f3;
        this.k = z;
        this.l = z2;
        this.m = z3;
        this.n = f4;
        this.o = f5;
        this.p = f6;
        this.q = f7;
        this.r = f8;
    }
}
