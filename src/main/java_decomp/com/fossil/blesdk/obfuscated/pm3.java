package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.fossil.blesdk.obfuscated.hs3;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pm3 extends zm3 implements gn3, xs3.g {
    @DexIgnore
    public static /* final */ String q; // = q;
    @DexIgnore
    public static /* final */ a r; // = new a((rd4) null);
    @DexIgnore
    public en3 k;
    @DexIgnore
    public nn3 l;
    @DexIgnore
    public ur3<fg2> m;
    @DexIgnore
    public t62 n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final pm3 a(String str, boolean z, int i) {
            wd4.b(str, "serial");
            Bundle bundle = new Bundle();
            bundle.putString("SERIAL", str);
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            bundle.putInt(pm3.q, i);
            pm3 pm3 = new pm3();
            pm3.setArguments(bundle);
            return pm3;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pm3 e;

        @DexIgnore
        public b(pm3 pm3, String str) {
            this.e = pm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pm3 e;

        @DexIgnore
        public c(pm3 pm3, String str) {
            this.e = pm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().m();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pm3 e;
        @DexIgnore
        public /* final */ /* synthetic */ String f;

        @DexIgnore
        public d(pm3 pm3, String str) {
            this.e = pm3;
            this.f = str;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.e.getActivity() != null) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
                Context context = this.e.getContext();
                if (context != null) {
                    wd4.a((Object) context, "context!!");
                    aVar.a(context, this.f);
                    return;
                }
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ViewPager.i {
        @DexIgnore
        public void a(int i) {
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
        }

        @DexIgnore
        public void b(int i) {
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final en3 T0() {
        en3 en3 = this.k;
        if (en3 != null) {
            return en3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void U0() {
        ur3<fg2> ur3 = this.m;
        if (ur3 != null) {
            fg2 a2 = ur3.a();
            if (a2 != null) {
                DashBar dashBar = a2.z;
                if (dashBar != null) {
                    dashBar.setVisibility(0);
                    hs3.a aVar = hs3.a;
                    wd4.a((Object) dashBar, "this");
                    aVar.c(dashBar, this.o, 500);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void b(boolean z) {
        if (isActive()) {
            ur3<fg2> ur3 = this.m;
            if (ur3 != null) {
                fg2 a2 = ur3.a();
                if (a2 == null) {
                    return;
                }
                if (z) {
                    ConstraintLayout constraintLayout = a2.q;
                    wd4.a((Object) constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    wd4.a((Object) constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    FlexibleButton flexibleButton = a2.s;
                    wd4.a((Object) flexibleButton, "it.fbContinue");
                    flexibleButton.setVisibility(0);
                    FlexibleTextView flexibleTextView = a2.x;
                    wd4.a((Object) flexibleTextView, "it.ftvUpdateWarning");
                    flexibleTextView.setVisibility(4);
                    ProgressBar progressBar = a2.A;
                    wd4.a((Object) progressBar, "it.progressUpdate");
                    progressBar.setProgress(1000);
                    FlexibleTextView flexibleTextView2 = a2.w;
                    wd4.a((Object) flexibleTextView2, "it.ftvUpdate");
                    flexibleTextView2.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_UpdateCompleted_Title__UpdateCompleted));
                    return;
                }
                ConstraintLayout constraintLayout3 = a2.q;
                wd4.a((Object) constraintLayout3, "it.clUpdateFwFail");
                constraintLayout3.setVisibility(0);
                ConstraintLayout constraintLayout4 = a2.r;
                wd4.a((Object) constraintLayout4, "it.clUpdatingFw");
                constraintLayout4.setVisibility(8);
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void g(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i);
        ur3<fg2> ur3 = this.m;
        if (ur3 != null) {
            fg2 a2 = ur3.a();
            if (a2 != null) {
                ProgressBar progressBar = a2.A;
                if (progressBar != null) {
                    progressBar.setProgress(i);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void i(List<? extends Explore> list) {
        wd4.b(list, "data");
        ur3<fg2> ur3 = this.m;
        if (ur3 != null) {
            fg2 a2 = ur3.a();
            if (a2 != null) {
                if (FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.W.c().e())) {
                    FlexibleTextView flexibleTextView = a2.w;
                    wd4.a((Object) flexibleTextView, "it.ftvUpdate");
                    flexibleTextView.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Title__ExploreYourWatch));
                } else {
                    FlexibleTextView flexibleTextView2 = a2.w;
                    wd4.a((Object) flexibleTextView2, "it.ftvUpdate");
                    flexibleTextView2.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Title__ExploreYourWatch));
                }
            }
            t62 t62 = this.n;
            if (t62 != null) {
                t62.a(list);
            } else {
                wd4.d("mAdapterUpdateFirmware");
                throw null;
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void n0() {
        if (isActive()) {
            ur3<fg2> ur3 = this.m;
            if (ur3 != null) {
                fg2 a2 = ur3.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    wd4.a((Object) constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    wd4.a((Object) constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        fg2 fg2 = (fg2) ra.a(layoutInflater, R.layout.fragment_update_firmware, viewGroup, false, O0());
        this.m = new ur3<>(this, fg2);
        wd4.a((Object) fg2, "binding");
        return fg2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        nn3 nn3 = this.l;
        if (nn3 != null) {
            nn3.e();
        } else {
            wd4.d("mSubPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        nn3 nn3 = this.l;
        if (nn3 != null) {
            nn3.d();
        } else {
            wd4.d("mSubPresenter");
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0027, code lost:
        if (r8 != null) goto L_0x002b;
     */
    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        String str;
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        this.o = arguments != null ? arguments.getBoolean("IS_ONBOARDING_FLOW") : false;
        Bundle arguments2 = getArguments();
        if (arguments2 != null) {
            str = arguments2.getString("SERIAL", "");
        }
        str = "";
        Bundle arguments3 = getArguments();
        int i = arguments3 != null ? arguments3.getInt(q) : 0;
        this.l = new nn3(str, this);
        this.n = new t62(new ArrayList());
        ur3<fg2> ur3 = this.m;
        if (ur3 != null) {
            fg2 a2 = ur3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                wd4.a((Object) constraintLayout, "binding.clUpdateFwFail");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.r;
                wd4.a((Object) constraintLayout2, "binding.clUpdatingFw");
                constraintLayout2.setVisibility(0);
                ProgressBar progressBar = a2.A;
                wd4.a((Object) progressBar, "binding.progressUpdate");
                progressBar.setMax(1000);
                FlexibleButton flexibleButton = a2.s;
                wd4.a((Object) flexibleButton, "binding.fbContinue");
                flexibleButton.setVisibility(8);
                FlexibleTextView flexibleTextView = a2.x;
                wd4.a((Object) flexibleTextView, "binding.ftvUpdateWarning");
                flexibleTextView.setVisibility(0);
                a2.s.setOnClickListener(new b(this, str));
                RecyclerViewPager recyclerViewPager = a2.B;
                wd4.a((Object) recyclerViewPager, "binding.rvpTutorial");
                recyclerViewPager.setLayoutManager(new LinearLayoutManager(getActivity(), 0, false));
                RecyclerViewPager recyclerViewPager2 = a2.B;
                wd4.a((Object) recyclerViewPager2, "binding.rvpTutorial");
                t62 t62 = this.n;
                if (t62 != null) {
                    recyclerViewPager2.setAdapter(t62);
                    a2.y.a((RecyclerView) a2.B, 0);
                    a2.y.setOnPageChangeListener(new e());
                    a2.t.setOnClickListener(new c(this, str));
                    a2.v.setOnClickListener(new d(this, str));
                } else {
                    wd4.d("mAdapterUpdateFirmware");
                    throw null;
                }
            }
            if (i == 1) {
                v0();
            } else if (i == 2) {
                b(true);
            } else if (i == 3) {
                b(false);
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void v0() {
        if (isActive()) {
            ur3<fg2> ur3 = this.m;
            if (ur3 != null) {
                fg2 a2 = ur3.a();
                if (a2 != null) {
                    nn3 nn3 = this.l;
                    if (nn3 != null) {
                        nn3.f();
                        ConstraintLayout constraintLayout = a2.q;
                        wd4.a((Object) constraintLayout, "it.clUpdateFwFail");
                        constraintLayout.setVisibility(8);
                        ConstraintLayout constraintLayout2 = a2.r;
                        wd4.a((Object) constraintLayout2, "it.clUpdatingFw");
                        constraintLayout2.setVisibility(0);
                        FlexibleButton flexibleButton = a2.s;
                        wd4.a((Object) flexibleButton, "it.fbContinue");
                        flexibleButton.setVisibility(0);
                        FlexibleTextView flexibleTextView = a2.x;
                        wd4.a((Object) flexibleTextView, "it.ftvUpdateWarning");
                        flexibleTextView.setVisibility(4);
                        ProgressBar progressBar = a2.A;
                        wd4.a((Object) progressBar, "it.progressUpdate");
                        progressBar.setVisibility(8);
                        FlexibleTextView flexibleTextView2 = a2.u;
                        wd4.a((Object) flexibleTextView2, "it.ftvCountdownTime");
                        flexibleTextView2.setVisibility(8);
                        FlexibleTextView flexibleTextView3 = a2.w;
                        wd4.a((Object) flexibleTextView3, "it.ftvUpdate");
                        flexibleTextView3.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_AndroidQuickAccessPanel_FirmwareUpdateComplete_Text__FirmwareUpdateComplete));
                        return;
                    }
                    wd4.d("mSubPresenter");
                    throw null;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(en3 en3) {
        wd4.b(en3, "presenter");
        this.k = en3;
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        if (str.hashCode() == 927511079 && str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("UpdateFirmwareFragment", "Update firmware fail isOnboardingFlow " + this.o);
            if (i == R.id.fb_try_again) {
                en3 en3 = this.k;
                if (en3 != null) {
                    en3.m();
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else if (i == R.id.ftv_contact_cs) {
                if (getActivity() != null) {
                    HelpActivity.a aVar = HelpActivity.C;
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        wd4.a((Object) activity, "activity!!");
                        aVar.a(activity);
                        return;
                    }
                    wd4.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void g() {
        ur3<fg2> ur3 = this.m;
        if (ur3 != null) {
            fg2 a2 = ur3.a();
            if (a2 != null) {
                DashBar dashBar = a2.z;
                if (dashBar != null) {
                    dashBar.setVisibility(0);
                    hs3.a aVar = hs3.a;
                    wd4.a((Object) dashBar, "this");
                    aVar.f(dashBar, this.o, 500);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }
}
