package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzbb;
import com.google.android.gms.internal.clearcut.zzco;
import com.google.android.gms.internal.clearcut.zzfq;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yv0<T> implements kw0<T> {
    @DexIgnore
    public /* final */ tv0 a;
    @DexIgnore
    public /* final */ bx0<?, ?> b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ hu0<?> d;

    @DexIgnore
    public yv0(bx0<?, ?> bx0, hu0<?> hu0, tv0 tv0) {
        this.b = bx0;
        this.c = hu0.a(tv0);
        this.d = hu0;
        this.a = tv0;
    }

    @DexIgnore
    public static <T> yv0<T> a(bx0<?, ?> bx0, hu0<?> hu0, tv0 tv0) {
        return new yv0<>(bx0, hu0, tv0);
    }

    @DexIgnore
    public final int a(T t) {
        int hashCode = this.b.c(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.a((Object) t).hashCode() : hashCode;
    }

    @DexIgnore
    public final T a() {
        return this.a.e().v();
    }

    @DexIgnore
    public final void a(T t, px0 px0) throws IOException {
        Iterator<Map.Entry<?, Object>> e = this.d.a((Object) t).e();
        while (e.hasNext()) {
            Map.Entry next = e.next();
            ou0 ou0 = (ou0) next.getKey();
            if (ou0.b() != zzfq.MESSAGE || ou0.c() || ou0.a()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            }
            px0.zza(ou0.zzc(), next instanceof av0 ? ((av0) next).a().b() : next.getValue());
        }
        bx0<?, ?> bx0 = this.b;
        bx0.b(bx0.c(t), px0);
    }

    @DexIgnore
    public final void a(T t, byte[] bArr, int i, int i2, qt0 qt0) throws IOException {
        su0 su0 = (su0) t;
        cx0 cx0 = su0.zzjp;
        if (cx0 == cx0.d()) {
            cx0 = cx0.e();
            su0.zzjp = cx0;
        }
        cx0 cx02 = cx0;
        while (i < i2) {
            int a2 = pt0.a(bArr, i, qt0);
            int i3 = qt0.a;
            if (i3 != 11) {
                i = (i3 & 7) == 2 ? pt0.a(i3, bArr, a2, i2, cx02, qt0) : pt0.a(i3, bArr, a2, i2, qt0);
            } else {
                int i4 = 0;
                zzbb zzbb = null;
                while (a2 < i2) {
                    a2 = pt0.a(bArr, a2, qt0);
                    int i5 = qt0.a;
                    int i6 = i5 >>> 3;
                    int i7 = i5 & 7;
                    if (i6 != 2) {
                        if (i6 == 3 && i7 == 2) {
                            a2 = pt0.e(bArr, a2, qt0);
                            zzbb = (zzbb) qt0.c;
                        }
                    } else if (i7 == 0) {
                        a2 = pt0.a(bArr, a2, qt0);
                        i4 = qt0.a;
                    }
                    if (i5 == 12) {
                        break;
                    }
                    a2 = pt0.a(i5, bArr, a2, i2, qt0);
                }
                if (zzbb != null) {
                    cx02.a((i4 << 3) | 2, (Object) zzbb);
                }
                i = a2;
            }
        }
        if (i != i2) {
            throw zzco.zzbo();
        }
    }

    @DexIgnore
    public final boolean a(T t, T t2) {
        if (!this.b.c(t).equals(this.b.c(t2))) {
            return false;
        }
        if (this.c) {
            return this.d.a((Object) t).equals(this.d.a((Object) t2));
        }
        return true;
    }

    @DexIgnore
    public final int b(T t) {
        bx0<?, ?> bx0 = this.b;
        int d2 = bx0.d(bx0.c(t)) + 0;
        return this.c ? d2 + this.d.a((Object) t).g() : d2;
    }

    @DexIgnore
    public final void b(T t, T t2) {
        mw0.a(this.b, t, t2);
        if (this.c) {
            mw0.a(this.d, t, t2);
        }
    }

    @DexIgnore
    public final boolean c(T t) {
        return this.d.a((Object) t).d();
    }

    @DexIgnore
    public final void zzc(T t) {
        this.b.a(t);
        this.d.c(t);
    }
}
