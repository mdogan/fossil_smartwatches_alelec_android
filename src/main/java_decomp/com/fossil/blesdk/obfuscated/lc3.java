package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lc3 {
    @DexIgnore
    public /* final */ gc3 a;
    @DexIgnore
    public /* final */ vc3 b;
    @DexIgnore
    public /* final */ qc3 c;

    @DexIgnore
    public lc3(gc3 gc3, vc3 vc3, qc3 qc3) {
        wd4.b(gc3, "mHeartRateOverviewDayView");
        wd4.b(vc3, "mHeartRateOverviewWeekView");
        wd4.b(qc3, "mHeartRateOverviewMonthView");
        this.a = gc3;
        this.b = vc3;
        this.c = qc3;
    }

    @DexIgnore
    public final gc3 a() {
        return this.a;
    }

    @DexIgnore
    public final qc3 b() {
        return this.c;
    }

    @DexIgnore
    public final vc3 c() {
        return this.b;
    }
}
