package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface tl4 {
    @DexIgnore
    public static final tl4 a = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements tl4 {
        @DexIgnore
        public pm4 authenticate(rm4 rm4, Response response) {
            return null;
        }
    }

    @DexIgnore
    pm4 authenticate(rm4 rm4, Response response) throws IOException;
}
