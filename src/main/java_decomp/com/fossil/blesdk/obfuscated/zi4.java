package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.DeferredCoroutine;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zi4<T> extends DeferredCoroutine<T> {
    @DexIgnore
    public kd4<? super lh4, ? super kc4<? super T>, ? extends Object> h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zi4(CoroutineContext coroutineContext, kd4<? super lh4, ? super kc4<? super T>, ? extends Object> kd4) {
        super(coroutineContext, false);
        wd4.b(coroutineContext, "parentContext");
        wd4.b(kd4, "block");
        this.h = kd4;
    }

    @DexIgnore
    public void l() {
        kd4<? super lh4, ? super kc4<? super T>, ? extends Object> kd4 = this.h;
        if (kd4 != null) {
            this.h = null;
            wk4.a(kd4, this, this);
            return;
        }
        throw new IllegalStateException("Already started".toString());
    }
}
