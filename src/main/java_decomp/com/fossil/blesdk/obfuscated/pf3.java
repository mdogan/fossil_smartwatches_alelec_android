package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.uirenew.home.details.activity.WorkoutPagedAdapter;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pf3 extends as2 implements of3, View.OnClickListener {
    @DexIgnore
    public static /* final */ a p; // = new a((rd4) null);
    @DexIgnore
    public ur3<hc2> j;
    @DexIgnore
    public nf3 k;
    @DexIgnore
    public Date l; // = new Date();
    @DexIgnore
    public WorkoutPagedAdapter m;
    @DexIgnore
    public /* final */ Calendar n; // = Calendar.getInstance();
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final pf3 a(Date date) {
            wd4.b(date, "date");
            pf3 pf3 = new pf3();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            pf3.setArguments(bundle);
            return pf3;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends AppBarLayout.Behavior.a {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ rd b;

        @DexIgnore
        public b(pf3 pf3, boolean z, rd rdVar, Unit unit) {
            this.a = z;
            this.b = rdVar;
        }

        @DexIgnore
        public boolean a(AppBarLayout appBarLayout) {
            wd4.b(appBarLayout, "appBarLayout");
            return this.a && (this.b.isEmpty() ^ true);
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HeartRateDetailFragment";
    }

    @DexIgnore
    public void c(int i, int i2) {
        ur3<hc2> ur3 = this.j;
        if (ur3 != null) {
            hc2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (i == 0 && i2 == 0) {
                FlexibleTextView flexibleTextView = a2.w;
                wd4.a((Object) flexibleTextView, "it.ftvNoRecord");
                flexibleTextView.setVisibility(0);
                ConstraintLayout constraintLayout = a2.r;
                wd4.a((Object) constraintLayout, "it.clContainer");
                constraintLayout.setVisibility(8);
                return;
            }
            FlexibleTextView flexibleTextView2 = a2.w;
            wd4.a((Object) flexibleTextView2, "it.ftvNoRecord");
            flexibleTextView2.setVisibility(8);
            ConstraintLayout constraintLayout2 = a2.r;
            wd4.a((Object) constraintLayout2, "it.clContainer");
            constraintLayout2.setVisibility(0);
            FlexibleTextView flexibleTextView3 = a2.y;
            wd4.a((Object) flexibleTextView3, "it.ftvRestingValue");
            flexibleTextView3.setText(String.valueOf(i));
            FlexibleTextView flexibleTextView4 = a2.v;
            wd4.a((Object) flexibleTextView4, "it.ftvMaxValue");
            flexibleTextView4.setText(String.valueOf(i2));
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        MFLogger.d("HeartRateDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case R.id.iv_back /*2131362398*/:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case R.id.iv_back_date /*2131362399*/:
                    nf3 nf3 = this.k;
                    if (nf3 != null) {
                        nf3.j();
                        return;
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                case R.id.iv_next_date /*2131362447*/:
                    nf3 nf32 = this.k;
                    if (nf32 != null) {
                        nf32.i();
                        return;
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        hc2 hc2 = (hc2) ra.a(layoutInflater, R.layout.fragment_heartrate_detail, viewGroup, false, O0());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            wd4.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        this.l = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.l = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        wd4.a((Object) hc2, "binding");
        a(hc2);
        nf3 nf3 = this.k;
        if (nf3 != null) {
            nf3.a(this.l);
            this.j = new ur3<>(this, hc2);
            ur3<hc2> ur3 = this.j;
            if (ur3 != null) {
                hc2 a2 = ur3.a();
                if (a2 != null) {
                    return a2.d();
                }
                return null;
            }
            wd4.d("mBinding");
            throw null;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onDestroyView() {
        nf3 nf3 = this.k;
        if (nf3 != null) {
            nf3.h();
            super.onDestroyView();
            N0();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        nf3 nf3 = this.k;
        if (nf3 != null) {
            nf3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        nf3 nf3 = this.k;
        if (nf3 != null) {
            nf3.b(this.l);
            nf3 nf32 = this.k;
            if (nf32 != null) {
                nf32.f();
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wd4.b(bundle, "outState");
        nf3 nf3 = this.k;
        if (nf3 != null) {
            nf3.a(bundle);
            super.onSaveInstanceState(bundle);
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void a(hc2 hc2) {
        hc2.z.setOnClickListener(this);
        hc2.A.setOnClickListener(this);
        hc2.B.setOnClickListener(this);
        this.m = new WorkoutPagedAdapter(WorkoutPagedAdapter.WorkoutItem.HEART_RATE, Unit.IMPERIAL, new WorkoutSessionDifference());
        RecyclerView recyclerView = hc2.D;
        wd4.a((Object) recyclerView, "it");
        recyclerView.setAdapter(this.m);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable c = k6.c(recyclerView.getContext(), R.drawable.bg_item_decoration_eggshell_80a_line_1w);
        if (c != null) {
            zd3 zd3 = new zd3(linearLayoutManager.M(), false, false, 6, (rd4) null);
            wd4.a((Object) c, ResourceManager.DRAWABLE);
            zd3.a(c);
            recyclerView.a((RecyclerView.l) zd3);
        }
    }

    @DexIgnore
    public void a(nf3 nf3) {
        wd4.b(nf3, "presenter");
        this.k = nf3;
    }

    @DexIgnore
    public void a(Date date, boolean z, boolean z2, boolean z3) {
        wd4.b(date, "date");
        this.l = date;
        Calendar calendar = this.n;
        wd4.a((Object) calendar, "calendar");
        calendar.setTime(date);
        int i = this.n.get(7);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z + " - isToday - " + z2 + " - isDateAfter: " + z3 + " - calendar: " + this.n);
        ur3<hc2> ur3 = this.j;
        if (ur3 != null) {
            hc2 a2 = ur3.a();
            if (a2 != null) {
                a2.q.a(true, true);
                FlexibleTextView flexibleTextView = a2.t;
                wd4.a((Object) flexibleTextView, "binding.ftvDayOfMonth");
                flexibleTextView.setText(String.valueOf(this.n.get(5)));
                if (z) {
                    ImageView imageView = a2.A;
                    wd4.a((Object) imageView, "binding.ivBackDate");
                    imageView.setVisibility(4);
                } else {
                    ImageView imageView2 = a2.A;
                    wd4.a((Object) imageView2, "binding.ivBackDate");
                    imageView2.setVisibility(0);
                }
                if (z2 || z3) {
                    ImageView imageView3 = a2.B;
                    wd4.a((Object) imageView3, "binding.ivNextDate");
                    imageView3.setVisibility(8);
                    if (z2) {
                        FlexibleTextView flexibleTextView2 = a2.u;
                        wd4.a((Object) flexibleTextView2, "binding.ftvDayOfWeek");
                        flexibleTextView2.setText(tm2.a(getContext(), (int) R.string.DashboardDiana_Main_Steps7days_CTA__Today));
                        return;
                    }
                    FlexibleTextView flexibleTextView3 = a2.u;
                    wd4.a((Object) flexibleTextView3, "binding.ftvDayOfWeek");
                    flexibleTextView3.setText(ml2.b.b(i));
                    return;
                }
                ImageView imageView4 = a2.B;
                wd4.a((Object) imageView4, "binding.ivNextDate");
                imageView4.setVisibility(0);
                FlexibleTextView flexibleTextView4 = a2.u;
                wd4.a((Object) flexibleTextView4, "binding.ftvDayOfWeek");
                flexibleTextView4.setText(ml2.b.b(i));
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(int i, List<st3> list, List<Triple<Integer, Pair<Integer, Float>, String>> list2) {
        wd4.b(list, "listTodayHeartRateModel");
        wd4.b(list2, "listTimeZoneChange");
        ur3<hc2> ur3 = this.j;
        if (ur3 != null) {
            hc2 a2 = ur3.a();
            if (a2 != null) {
                TodayHeartRateChart todayHeartRateChart = a2.s;
                if (todayHeartRateChart != null) {
                    todayHeartRateChart.setDayInMinuteWithTimeZone(i);
                    todayHeartRateChart.setListTimeZoneChange(list2);
                    todayHeartRateChart.a(list);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(boolean z, Unit unit, rd<WorkoutSession> rdVar) {
        wd4.b(unit, MFUser.DISTANCE_UNIT);
        wd4.b(rdVar, "workoutSessions");
        ur3<hc2> ur3 = this.j;
        if (ur3 != null) {
            hc2 a2 = ur3.a();
            if (a2 != null) {
                if (z) {
                    LinearLayout linearLayout = a2.C;
                    wd4.a((Object) linearLayout, "it.llWorkout");
                    linearLayout.setVisibility(0);
                    if (!rdVar.isEmpty()) {
                        FlexibleTextView flexibleTextView = a2.x;
                        wd4.a((Object) flexibleTextView, "it.ftvNoWorkoutRecorded");
                        flexibleTextView.setVisibility(8);
                        RecyclerView recyclerView = a2.D;
                        wd4.a((Object) recyclerView, "it.rvWorkout");
                        recyclerView.setVisibility(0);
                        WorkoutPagedAdapter workoutPagedAdapter = this.m;
                        if (workoutPagedAdapter != null) {
                            workoutPagedAdapter.a(unit, rdVar);
                        }
                    } else {
                        FlexibleTextView flexibleTextView2 = a2.x;
                        wd4.a((Object) flexibleTextView2, "it.ftvNoWorkoutRecorded");
                        flexibleTextView2.setVisibility(0);
                        RecyclerView recyclerView2 = a2.D;
                        wd4.a((Object) recyclerView2, "it.rvWorkout");
                        recyclerView2.setVisibility(8);
                        WorkoutPagedAdapter workoutPagedAdapter2 = this.m;
                        if (workoutPagedAdapter2 != null) {
                            workoutPagedAdapter2.a(unit, rdVar);
                        }
                    }
                } else {
                    LinearLayout linearLayout2 = a2.C;
                    wd4.a((Object) linearLayout2, "it.llWorkout");
                    linearLayout2.setVisibility(8);
                }
                AppBarLayout appBarLayout = a2.q;
                wd4.a((Object) appBarLayout, "it.appBarLayout");
                ViewGroup.LayoutParams layoutParams = appBarLayout.getLayoutParams();
                if (layoutParams != null) {
                    CoordinatorLayout.e eVar = (CoordinatorLayout.e) layoutParams;
                    AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) eVar.d();
                    if (behavior == null) {
                        behavior = new AppBarLayout.Behavior();
                    }
                    behavior.setDragCallback(new b(this, z, rdVar, unit));
                    eVar.a((CoordinatorLayout.Behavior) behavior);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }
}
