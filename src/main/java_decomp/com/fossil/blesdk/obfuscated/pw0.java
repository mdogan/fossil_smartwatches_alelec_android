package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pw0 implements Iterator<Map.Entry<K, V>> {
    @DexIgnore
    public int e;
    @DexIgnore
    public Iterator<Map.Entry<K, V>> f;
    @DexIgnore
    public /* final */ /* synthetic */ nw0 g;

    @DexIgnore
    public pw0(nw0 nw0) {
        this.g = nw0;
        this.e = this.g.f.size();
    }

    @DexIgnore
    public /* synthetic */ pw0(nw0 nw0, ow0 ow0) {
        this(nw0);
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> a() {
        if (this.f == null) {
            this.f = this.g.j.entrySet().iterator();
        }
        return this.f;
    }

    @DexIgnore
    public final boolean hasNext() {
        int i = this.e;
        return (i > 0 && i <= this.g.f.size()) || a().hasNext();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        Object obj;
        if (a().hasNext()) {
            obj = a().next();
        } else {
            List b = this.g.f;
            int i = this.e - 1;
            this.e = i;
            obj = b.get(i);
        }
        return (Map.Entry) obj;
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
