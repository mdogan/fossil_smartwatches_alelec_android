package com.fossil.blesdk.obfuscated;

import android.app.Activity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class f04 extends Activity {
    @DexIgnore
    public void onPause() {
        super.onPause();
        j04.a(this);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        j04.b(this);
    }
}
