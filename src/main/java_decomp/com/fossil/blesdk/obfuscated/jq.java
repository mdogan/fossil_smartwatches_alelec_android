package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.sq;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class jq<T extends sq> {
    @DexIgnore
    public /* final */ Queue<T> a; // = vw.a(20);

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public void a(T t) {
        if (this.a.size() < 20) {
            this.a.offer(t);
        }
    }

    @DexIgnore
    public T b() {
        T t = (sq) this.a.poll();
        return t == null ? a() : t;
    }
}
