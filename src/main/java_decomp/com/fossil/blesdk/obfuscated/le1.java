package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface le1 extends IInterface {
    @DexIgnore
    tn0 a(tn0 tn0, tn0 tn02, Bundle bundle) throws RemoteException;

    @DexIgnore
    void a() throws RemoteException;

    @DexIgnore
    void a(Bundle bundle) throws RemoteException;

    @DexIgnore
    void a(te1 te1) throws RemoteException;

    @DexIgnore
    void a(tn0 tn0, GoogleMapOptions googleMapOptions, Bundle bundle) throws RemoteException;

    @DexIgnore
    void b() throws RemoteException;

    @DexIgnore
    void b(Bundle bundle) throws RemoteException;

    @DexIgnore
    void c() throws RemoteException;

    @DexIgnore
    void d() throws RemoteException;

    @DexIgnore
    void e() throws RemoteException;

    @DexIgnore
    void onLowMemory() throws RemoteException;

    @DexIgnore
    void onPause() throws RemoteException;
}
