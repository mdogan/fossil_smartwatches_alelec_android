package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.af0;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.ee0.d;
import com.fossil.blesdk.obfuscated.lj0;
import com.fossil.blesdk.obfuscated.we0;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.Scope;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ge0<O extends ee0.d> {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ee0<O> b;
    @DexIgnore
    public /* final */ O c;
    @DexIgnore
    public /* final */ zh0<O> d;
    @DexIgnore
    public /* final */ Looper e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ he0 g;
    @DexIgnore
    public /* final */ ef0 h;
    @DexIgnore
    public /* final */ we0 i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ a c; // = new C0015a().a();
        @DexIgnore
        public /* final */ ef0 a;
        @DexIgnore
        public /* final */ Looper b;

        @DexIgnore
        public a(ef0 ef0, Account account, Looper looper) {
            this.a = ef0;
            this.b = looper;
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ge0$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ge0$a$a  reason: collision with other inner class name */
        public static class C0015a {
            @DexIgnore
            public ef0 a;
            @DexIgnore
            public Looper b;

            @DexIgnore
            public C0015a a(ef0 ef0) {
                ck0.a(ef0, (Object) "StatusExceptionMapper must not be null.");
                this.a = ef0;
                return this;
            }

            @DexIgnore
            public C0015a a(Looper looper) {
                ck0.a(looper, (Object) "Looper must not be null.");
                this.b = looper;
                return this;
            }

            @DexIgnore
            public a a() {
                if (this.a == null) {
                    this.a = new se0();
                }
                if (this.b == null) {
                    this.b = Looper.getMainLooper();
                }
                return new a(this.a, this.b);
            }
        }
    }

    @DexIgnore
    public ge0(Context context, ee0<O> ee0, Looper looper) {
        ck0.a(context, (Object) "Null context is not permitted.");
        ck0.a(ee0, (Object) "Api must not be null.");
        ck0.a(looper, (Object) "Looper must not be null.");
        this.a = context.getApplicationContext();
        this.b = ee0;
        this.c = null;
        this.e = looper;
        this.d = zh0.a(ee0);
        this.g = new zg0(this);
        this.i = we0.a(this.a);
        this.f = this.i.b();
        this.h = new se0();
    }

    @DexIgnore
    public final <A extends ee0.b, T extends ue0<? extends ne0, A>> T a(int i2, T t) {
        t.g();
        this.i.a(this, i2, (ue0<? extends ne0, ee0.b>) t);
        return t;
    }

    @DexIgnore
    public <A extends ee0.b, T extends ue0<? extends ne0, A>> T b(T t) {
        a(0, t);
        return t;
    }

    @DexIgnore
    public <A extends ee0.b, T extends ue0<? extends ne0, A>> T c(T t) {
        a(1, t);
        return t;
    }

    @DexIgnore
    public O d() {
        return this.c;
    }

    @DexIgnore
    public Context e() {
        return this.a;
    }

    @DexIgnore
    public final int f() {
        return this.f;
    }

    @DexIgnore
    public Looper g() {
        return this.e;
    }

    @DexIgnore
    public final zh0<O> h() {
        return this.d;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    public lj0.a b() {
        Account account;
        O o;
        Set<Scope> set;
        lj0.a aVar = new lj0.a();
        O o2 = this.c;
        if (o2 instanceof ee0.d.b) {
            GoogleSignInAccount a2 = ((ee0.d.b) o2).a();
            if (a2 != null) {
                account = a2.H();
                aVar.a(account);
                o = this.c;
                if (o instanceof ee0.d.b) {
                    GoogleSignInAccount a3 = ((ee0.d.b) o).a();
                    if (a3 != null) {
                        set = a3.Q();
                        aVar.a((Collection<Scope>) set);
                        aVar.a(this.a.getClass().getName());
                        aVar.b(this.a.getPackageName());
                        return aVar;
                    }
                }
                set = Collections.emptySet();
                aVar.a((Collection<Scope>) set);
                aVar.a(this.a.getClass().getName());
                aVar.b(this.a.getPackageName());
                return aVar;
            }
        }
        O o3 = this.c;
        account = o3 instanceof ee0.d.a ? ((ee0.d.a) o3).h() : null;
        aVar.a(account);
        o = this.c;
        if (o instanceof ee0.d.b) {
        }
        set = Collections.emptySet();
        aVar.a((Collection<Scope>) set);
        aVar.a(this.a.getClass().getName());
        aVar.b(this.a.getPackageName());
        return aVar;
    }

    @DexIgnore
    public final ee0<O> c() {
        return this.b;
    }

    @DexIgnore
    public final <TResult, A extends ee0.b> xn1<TResult> a(int i2, gf0<A, TResult> gf0) {
        yn1 yn1 = new yn1();
        this.i.a(this, i2, gf0, yn1, this.h);
        return yn1.a();
    }

    @DexIgnore
    public <TResult, A extends ee0.b> xn1<TResult> a(gf0<A, TResult> gf0) {
        return a(0, gf0);
    }

    @DexIgnore
    public <A extends ee0.b, T extends ue0<? extends ne0, A>> T a(T t) {
        a(2, t);
        return t;
    }

    @DexIgnore
    @Deprecated
    public <A extends ee0.b, T extends cf0<A, ?>, U extends if0<A, ?>> xn1<Void> a(T t, U u) {
        ck0.a(t);
        ck0.a(u);
        ck0.a(t.b(), (Object) "Listener has already been released.");
        ck0.a(u.a(), (Object) "Listener has already been released.");
        ck0.a(t.b().equals(u.a()), (Object) "Listener registration and unregistration methods must be constructed with the same ListenerHolder.");
        return this.i.a(this, (cf0<ee0.b, ?>) t, (if0<ee0.b, ?>) u);
    }

    @DexIgnore
    public ge0(Activity activity, ee0<O> ee0, O o, a aVar) {
        ck0.a(activity, (Object) "Null activity is not permitted.");
        ck0.a(ee0, (Object) "Api must not be null.");
        ck0.a(aVar, (Object) "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.a = activity.getApplicationContext();
        this.b = ee0;
        this.c = o;
        this.e = aVar.b;
        this.d = zh0.a(this.b, this.c);
        this.g = new zg0(this);
        this.i = we0.a(this.a);
        this.f = this.i.b();
        this.h = aVar.a;
        if (!(activity instanceof GoogleApiActivity)) {
            nf0.a(activity, this.i, this.d);
        }
        this.i.a((ge0<?>) this);
    }

    @DexIgnore
    public xn1<Boolean> a(af0.a<?> aVar) {
        ck0.a(aVar, (Object) "Listener key cannot be null.");
        return this.i.a(this, aVar);
    }

    @DexIgnore
    public ee0.f a(Looper looper, we0.a<O> aVar) {
        return this.b.d().a(this.a, looper, b().a(), this.c, aVar, aVar);
    }

    @DexIgnore
    public he0 a() {
        return this.g;
    }

    @DexIgnore
    public ih0 a(Context context, Handler handler) {
        return new ih0(context, handler, b().a());
    }

    @DexIgnore
    public ge0(Context context, ee0<O> ee0, O o, a aVar) {
        ck0.a(context, (Object) "Null context is not permitted.");
        ck0.a(ee0, (Object) "Api must not be null.");
        ck0.a(aVar, (Object) "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.a = context.getApplicationContext();
        this.b = ee0;
        this.c = o;
        this.e = aVar.b;
        this.d = zh0.a(this.b, this.c);
        this.g = new zg0(this);
        this.i = we0.a(this.a);
        this.f = this.i.b();
        this.h = aVar.a;
        this.i.a((ge0<?>) this);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    @Deprecated
    public ge0(Activity activity, ee0<O> ee0, O o, ef0 ef0) {
        this(activity, ee0, o, r0.a());
        a.C0015a aVar = new a.C0015a();
        aVar.a(ef0);
        aVar.a(activity.getMainLooper());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    @Deprecated
    public ge0(Context context, ee0<O> ee0, O o, ef0 ef0) {
        this(context, ee0, o, r0.a());
        a.C0015a aVar = new a.C0015a();
        aVar.a(ef0);
    }
}
