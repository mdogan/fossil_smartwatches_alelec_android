package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qg3 implements Factory<HomeProfilePresenter> {
    @DexIgnore
    public static HomeProfilePresenter a(pg3 pg3, PortfolioApp portfolioApp, or2 or2, UpdateUser updateUser, DeviceRepository deviceRepository, UserRepository userRepository, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        return new HomeProfilePresenter(pg3, portfolioApp, or2, updateUser, deviceRepository, userRepository, summariesRepository, sleepSummariesRepository, deleteLogoutUserUseCase);
    }
}
