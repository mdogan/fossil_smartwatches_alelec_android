package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.ee0.d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zh0<O extends ee0.d> {
    @DexIgnore
    public /* final */ boolean a; // = true;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ee0<O> c;
    @DexIgnore
    public /* final */ O d;

    @DexIgnore
    public zh0(ee0<O> ee0, O o) {
        this.c = ee0;
        this.d = o;
        this.b = ak0.a(this.c, this.d);
    }

    @DexIgnore
    public static <O extends ee0.d> zh0<O> a(ee0<O> ee0, O o) {
        return new zh0<>(ee0, o);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zh0)) {
            return false;
        }
        zh0 zh0 = (zh0) obj;
        return !this.a && !zh0.a && ak0.a(this.c, zh0.c) && ak0.a(this.d, zh0.d);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b;
    }

    @DexIgnore
    public static <O extends ee0.d> zh0<O> a(ee0<O> ee0) {
        return new zh0<>(ee0);
    }

    @DexIgnore
    public final String a() {
        return this.c.b();
    }

    @DexIgnore
    public zh0(ee0<O> ee0) {
        this.c = ee0;
        this.d = null;
        this.b = System.identityHashCode(this);
    }
}
