package com.fossil.blesdk.obfuscated;

import com.facebook.internal.Utility;
import com.facebook.places.model.PlaceFields;
import com.fossil.blesdk.device.data.file.FileType;
import com.misfit.frameworks.common.constants.Constants;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ew3 {
    @DexIgnore
    public static /* final */ dw3[] a; // = {new dw3(dw3.h, ""), new dw3(dw3.e, "GET"), new dw3(dw3.e, "POST"), new dw3(dw3.f, (String) ZendeskConfig.SLASH), new dw3(dw3.f, "/index.html"), new dw3(dw3.g, "http"), new dw3(dw3.g, (String) Utility.URL_SCHEME), new dw3(dw3.d, "200"), new dw3(dw3.d, "204"), new dw3(dw3.d, "206"), new dw3(dw3.d, "304"), new dw3(dw3.d, "400"), new dw3(dw3.d, "404"), new dw3(dw3.d, "500"), new dw3("accept-charset", ""), new dw3("accept-encoding", "gzip, deflate"), new dw3("accept-language", ""), new dw3("accept-ranges", ""), new dw3("accept", ""), new dw3("access-control-allow-origin", ""), new dw3("age", ""), new dw3("allow", ""), new dw3((String) Constants.IF_AUTHORIZATION, ""), new dw3("cache-control", ""), new dw3("content-disposition", ""), new dw3("content-encoding", ""), new dw3("content-language", ""), new dw3("content-length", ""), new dw3("content-location", ""), new dw3("content-range", ""), new dw3("content-type", ""), new dw3("cookie", ""), new dw3("date", ""), new dw3((String) Constants.JSON_KEY_ETAG, ""), new dw3("expect", ""), new dw3("expires", ""), new dw3("from", ""), new dw3("host", ""), new dw3("if-match", ""), new dw3("if-modified-since", ""), new dw3("if-none-match", ""), new dw3("if-range", ""), new dw3("if-unmodified-since", ""), new dw3("last-modified", ""), new dw3("link", ""), new dw3((String) PlaceFields.LOCATION, ""), new dw3("max-forwards", ""), new dw3("proxy-authenticate", ""), new dw3("proxy-authorization", ""), new dw3("range", ""), new dw3("referer", ""), new dw3("refresh", ""), new dw3("retry-after", ""), new dw3("server", ""), new dw3("set-cookie", ""), new dw3("strict-transport-security", ""), new dw3("transfer-encoding", ""), new dw3("user-agent", ""), new dw3("vary", ""), new dw3("via", ""), new dw3("www-authenticate", "")};
    @DexIgnore
    public static /* final */ Map<ByteString, Integer> b; // = c();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ List<dw3> a; // = new ArrayList();
        @DexIgnore
        public /* final */ xo4 b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public dw3[] e; // = new dw3[8];
        @DexIgnore
        public int f; // = (this.e.length - 1);
        @DexIgnore
        public int g; // = 0;
        @DexIgnore
        public int h; // = 0;

        @DexIgnore
        public a(int i, kp4 kp4) {
            this.c = i;
            this.d = i;
            this.b = ep4.a(kp4);
        }

        @DexIgnore
        public final void a() {
            int i = this.d;
            int i2 = this.h;
            if (i >= i2) {
                return;
            }
            if (i == 0) {
                b();
            } else {
                b(i2 - i);
            }
        }

        @DexIgnore
        public final void b() {
            this.a.clear();
            Arrays.fill(this.e, (Object) null);
            this.f = this.e.length - 1;
            this.g = 0;
            this.h = 0;
        }

        @DexIgnore
        public List<dw3> c() {
            ArrayList arrayList = new ArrayList(this.a);
            this.a.clear();
            return arrayList;
        }

        @DexIgnore
        public void d(int i) {
            this.c = i;
            this.d = i;
            a();
        }

        @DexIgnore
        public final boolean e(int i) {
            return i >= 0 && i <= ew3.a.length - 1;
        }

        @DexIgnore
        public void f() throws IOException {
            while (!this.b.g()) {
                byte readByte = this.b.readByte() & FileType.MASKED_INDEX;
                if (readByte == 128) {
                    throw new IOException("index == 0");
                } else if ((readByte & 128) == 128) {
                    f(a((int) readByte, 127) - 1);
                } else if (readByte == 64) {
                    g();
                } else if ((readByte & 64) == 64) {
                    g(a((int) readByte, 63) - 1);
                } else if ((readByte & 32) == 32) {
                    this.d = a((int) readByte, 31);
                    int i = this.d;
                    if (i < 0 || i > this.c) {
                        throw new IOException("Invalid dynamic table size update " + this.d);
                    }
                    a();
                } else if (readByte == 16 || readByte == 0) {
                    h();
                } else {
                    h(a((int) readByte, 15) - 1);
                }
            }
        }

        @DexIgnore
        public final void g(int i) throws IOException {
            a(-1, new dw3(c(i), e()));
        }

        @DexIgnore
        public final void h(int i) throws IOException {
            this.a.add(new dw3(c(i), e()));
        }

        @DexIgnore
        public ByteString e() throws IOException {
            int d2 = d();
            boolean z = (d2 & 128) == 128;
            int a2 = a(d2, 127);
            if (z) {
                return ByteString.of(gw3.b().a(this.b.f((long) a2)));
            }
            return this.b.d((long) a2);
        }

        @DexIgnore
        public final ByteString c(int i) {
            if (e(i)) {
                return ew3.a[i].a;
            }
            return this.e[a(i - ew3.a.length)].a;
        }

        @DexIgnore
        public final int a(int i) {
            return this.f + 1 + i;
        }

        @DexIgnore
        public final int d() throws IOException {
            return this.b.readByte() & FileType.MASKED_INDEX;
        }

        @DexIgnore
        public final void g() throws IOException {
            ByteString e2 = e();
            ByteString unused = ew3.b(e2);
            a(-1, new dw3(e2, e()));
        }

        @DexIgnore
        public final void h() throws IOException {
            ByteString e2 = e();
            ByteString unused = ew3.b(e2);
            this.a.add(new dw3(e2, e()));
        }

        @DexIgnore
        public final void a(int i, dw3 dw3) {
            this.a.add(dw3);
            int i2 = dw3.c;
            if (i != -1) {
                i2 -= this.e[a(i)].c;
            }
            int i3 = this.d;
            if (i2 > i3) {
                b();
                return;
            }
            int b2 = b((this.h + i2) - i3);
            if (i == -1) {
                int i4 = this.g + 1;
                dw3[] dw3Arr = this.e;
                if (i4 > dw3Arr.length) {
                    dw3[] dw3Arr2 = new dw3[(dw3Arr.length * 2)];
                    System.arraycopy(dw3Arr, 0, dw3Arr2, dw3Arr.length, dw3Arr.length);
                    this.f = this.e.length - 1;
                    this.e = dw3Arr2;
                }
                int i5 = this.f;
                this.f = i5 - 1;
                this.e[i5] = dw3;
                this.g++;
            } else {
                this.e[i + a(i) + b2] = dw3;
            }
            this.h += i2;
        }

        @DexIgnore
        public final int b(int i) {
            int i2 = 0;
            if (i > 0) {
                int length = this.e.length;
                while (true) {
                    length--;
                    if (length < this.f || i <= 0) {
                        dw3[] dw3Arr = this.e;
                        int i3 = this.f;
                        System.arraycopy(dw3Arr, i3 + 1, dw3Arr, i3 + 1 + i2, this.g);
                        this.f += i2;
                    } else {
                        dw3[] dw3Arr2 = this.e;
                        i -= dw3Arr2[length].c;
                        this.h -= dw3Arr2[length].c;
                        this.g--;
                        i2++;
                    }
                }
                dw3[] dw3Arr3 = this.e;
                int i32 = this.f;
                System.arraycopy(dw3Arr3, i32 + 1, dw3Arr3, i32 + 1 + i2, this.g);
                this.f += i2;
            }
            return i2;
        }

        @DexIgnore
        public final void f(int i) throws IOException {
            if (e(i)) {
                this.a.add(ew3.a[i]);
                return;
            }
            int a2 = a(i - ew3.a.length);
            if (a2 >= 0) {
                dw3[] dw3Arr = this.e;
                if (a2 <= dw3Arr.length - 1) {
                    this.a.add(dw3Arr[a2]);
                    return;
                }
            }
            throw new IOException("Header index too large " + (i + 1));
        }

        @DexIgnore
        public int a(int i, int i2) throws IOException {
            int i3 = i & i2;
            if (i3 < i2) {
                return i3;
            }
            int i4 = 0;
            while (true) {
                int d2 = d();
                if ((d2 & 128) == 0) {
                    return i2 + (d2 << i4);
                }
                i2 += (d2 & 127) << i4;
                i4 += 7;
            }
        }
    }

    @DexIgnore
    public static Map<ByteString, Integer> c() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(a.length);
        int i = 0;
        while (true) {
            dw3[] dw3Arr = a;
            if (i >= dw3Arr.length) {
                return Collections.unmodifiableMap(linkedHashMap);
            }
            if (!linkedHashMap.containsKey(dw3Arr[i].a)) {
                linkedHashMap.put(a[i].a, Integer.valueOf(i));
            }
            i++;
        }
    }

    @DexIgnore
    public static ByteString b(ByteString byteString) throws IOException {
        int size = byteString.size();
        int i = 0;
        while (i < size) {
            byte b2 = byteString.getByte(i);
            if (b2 < 65 || b2 > 90) {
                i++;
            } else {
                throw new IOException("PROTOCOL_ERROR response malformed: mixed case name: " + byteString.utf8());
            }
        }
        return byteString;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ vo4 a;

        @DexIgnore
        public b(vo4 vo4) {
            this.a = vo4;
        }

        @DexIgnore
        public void a(List<dw3> list) throws IOException {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                ByteString asciiLowercase = list.get(i).a.toAsciiLowercase();
                Integer num = (Integer) ew3.b.get(asciiLowercase);
                if (num != null) {
                    a(num.intValue() + 1, 15, 0);
                    a(list.get(i).b);
                } else {
                    this.a.writeByte(0);
                    a(asciiLowercase);
                    a(list.get(i).b);
                }
            }
        }

        @DexIgnore
        public void a(int i, int i2, int i3) throws IOException {
            if (i < i2) {
                this.a.writeByte(i | i3);
                return;
            }
            this.a.writeByte(i3 | i2);
            int i4 = i - i2;
            while (i4 >= 128) {
                this.a.writeByte(128 | (i4 & 127));
                i4 >>>= 7;
            }
            this.a.writeByte(i4);
        }

        @DexIgnore
        public void a(ByteString byteString) throws IOException {
            a(byteString.size(), 127, 0);
            this.a.a(byteString);
        }
    }
}
