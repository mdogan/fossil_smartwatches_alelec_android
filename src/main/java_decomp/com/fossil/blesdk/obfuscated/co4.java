package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.obfuscated.wn4;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import okhttp3.internal.http2.ErrorCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class co4 implements Closeable {
    @DexIgnore
    public static /* final */ Logger k; // = Logger.getLogger(xn4.class.getName());
    @DexIgnore
    public /* final */ wo4 e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ vo4 g; // = new vo4();
    @DexIgnore
    public int h; // = RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ wn4.b j; // = new wn4.b(this.g);

    @DexIgnore
    public co4(wo4 wo4, boolean z) {
        this.e = wo4;
        this.f = z;
    }

    @DexIgnore
    public synchronized void a(fo4 fo4) throws IOException {
        if (!this.i) {
            this.h = fo4.c(this.h);
            if (fo4.b() != -1) {
                this.j.b(fo4.b());
            }
            a(0, 0, (byte) 4, (byte) 1);
            this.e.flush();
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void b(fo4 fo4) throws IOException {
        if (!this.i) {
            int i2 = 0;
            a(0, fo4.d() * 6, (byte) 4, (byte) 0);
            while (i2 < 10) {
                if (fo4.d(i2)) {
                    this.e.writeShort(i2 == 4 ? 3 : i2 == 7 ? 4 : i2);
                    this.e.writeInt(fo4.a(i2));
                }
                i2++;
            }
            this.e.flush();
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public final void c(int i2, long j2) throws IOException {
        while (j2 > 0) {
            int min = (int) Math.min((long) this.h, j2);
            long j3 = (long) min;
            j2 -= j3;
            a(i2, min, (byte) 9, j2 == 0 ? (byte) 4 : 0);
            this.e.a(this.g, j3);
        }
    }

    @DexIgnore
    public synchronized void close() throws IOException {
        this.i = true;
        this.e.close();
    }

    @DexIgnore
    public synchronized void flush() throws IOException {
        if (!this.i) {
            this.e.flush();
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void p() throws IOException {
        if (this.i) {
            throw new IOException("closed");
        } else if (this.f) {
            if (k.isLoggable(Level.FINE)) {
                k.fine(vm4.a(">> CONNECTION %s", xn4.a.hex()));
            }
            this.e.write(xn4.a.toByteArray());
            this.e.flush();
        }
    }

    @DexIgnore
    public int r() {
        return this.h;
    }

    @DexIgnore
    public synchronized void a(int i2, int i3, List<vn4> list) throws IOException {
        if (!this.i) {
            this.j.a(list);
            long B = this.g.B();
            int min = (int) Math.min((long) (this.h - 4), B);
            long j2 = (long) min;
            int i4 = (B > j2 ? 1 : (B == j2 ? 0 : -1));
            a(i2, min + 4, (byte) 5, i4 == 0 ? (byte) 4 : 0);
            this.e.writeInt(i3 & Integer.MAX_VALUE);
            this.e.a(this.g, j2);
            if (i4 > 0) {
                c(i2, B - j2);
            }
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void a(boolean z, int i2, int i3, List<vn4> list) throws IOException {
        if (!this.i) {
            a(z, i2, list);
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void a(int i2, ErrorCode errorCode) throws IOException {
        if (this.i) {
            throw new IOException("closed");
        } else if (errorCode.httpCode != -1) {
            a(i2, 4, (byte) 3, (byte) 0);
            this.e.writeInt(errorCode.httpCode);
            this.e.flush();
        } else {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    public synchronized void a(boolean z, int i2, vo4 vo4, int i3) throws IOException {
        if (!this.i) {
            byte b = 0;
            if (z) {
                b = (byte) 1;
            }
            a(i2, b, vo4, i3);
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public void a(int i2, byte b, vo4 vo4, int i3) throws IOException {
        a(i2, i3, (byte) 0, b);
        if (i3 > 0) {
            this.e.a(vo4, (long) i3);
        }
    }

    @DexIgnore
    public synchronized void a(boolean z, int i2, int i3) throws IOException {
        if (!this.i) {
            a(0, 8, (byte) 6, z ? (byte) 1 : 0);
            this.e.writeInt(i2);
            this.e.writeInt(i3);
            this.e.flush();
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void a(int i2, ErrorCode errorCode, byte[] bArr) throws IOException {
        if (this.i) {
            throw new IOException("closed");
        } else if (errorCode.httpCode != -1) {
            a(0, bArr.length + 8, (byte) 7, (byte) 0);
            this.e.writeInt(i2);
            this.e.writeInt(errorCode.httpCode);
            if (bArr.length > 0) {
                this.e.write(bArr);
            }
            this.e.flush();
        } else {
            xn4.a("errorCode.httpCode == -1", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public synchronized void a(int i2, long j2) throws IOException {
        if (this.i) {
            throw new IOException("closed");
        } else if (j2 == 0 || j2 > 2147483647L) {
            xn4.a("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", Long.valueOf(j2));
            throw null;
        } else {
            a(i2, 4, (byte) 8, (byte) 0);
            this.e.writeInt((int) j2);
            this.e.flush();
        }
    }

    @DexIgnore
    public void a(int i2, int i3, byte b, byte b2) throws IOException {
        if (k.isLoggable(Level.FINE)) {
            k.fine(xn4.a(false, i2, i3, b, b2));
        }
        int i4 = this.h;
        if (i3 > i4) {
            xn4.a("FRAME_SIZE_ERROR length > %d: %d", Integer.valueOf(i4), Integer.valueOf(i3));
            throw null;
        } else if ((Integer.MIN_VALUE & i2) == 0) {
            a(this.e, i3);
            this.e.writeByte(b & FileType.MASKED_INDEX);
            this.e.writeByte(b2 & FileType.MASKED_INDEX);
            this.e.writeInt(i2 & Integer.MAX_VALUE);
        } else {
            xn4.a("reserved bit set: %s", Integer.valueOf(i2));
            throw null;
        }
    }

    @DexIgnore
    public static void a(wo4 wo4, int i2) throws IOException {
        wo4.writeByte((i2 >>> 16) & 255);
        wo4.writeByte((i2 >>> 8) & 255);
        wo4.writeByte(i2 & 255);
    }

    @DexIgnore
    public void a(boolean z, int i2, List<vn4> list) throws IOException {
        if (!this.i) {
            this.j.a(list);
            long B = this.g.B();
            int min = (int) Math.min((long) this.h, B);
            long j2 = (long) min;
            int i3 = (B > j2 ? 1 : (B == j2 ? 0 : -1));
            byte b = i3 == 0 ? (byte) 4 : 0;
            if (z) {
                b = (byte) (b | 1);
            }
            a(i2, min, (byte) 1, b);
            this.e.a(this.g, j2);
            if (i3 > 0) {
                c(i2, B - j2);
                return;
            }
            return;
        }
        throw new IOException("closed");
    }
}
