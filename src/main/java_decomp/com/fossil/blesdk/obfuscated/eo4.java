package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.util.List;
import okhttp3.internal.http2.ErrorCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface eo4 {
    @DexIgnore
    public static final eo4 a = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements eo4 {
        @DexIgnore
        public void a(int i, ErrorCode errorCode) {
        }

        @DexIgnore
        public boolean a(int i, xo4 xo4, int i2, boolean z) throws IOException {
            xo4.skip((long) i2);
            return true;
        }

        @DexIgnore
        public boolean a(int i, List<vn4> list) {
            return true;
        }

        @DexIgnore
        public boolean a(int i, List<vn4> list, boolean z) {
            return true;
        }
    }

    @DexIgnore
    void a(int i, ErrorCode errorCode);

    @DexIgnore
    boolean a(int i, xo4 xo4, int i2, boolean z) throws IOException;

    @DexIgnore
    boolean a(int i, List<vn4> list);

    @DexIgnore
    boolean a(int i, List<vn4> list, boolean z);
}
