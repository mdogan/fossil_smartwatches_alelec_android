package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.a.f;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class t04 extends p04 {
    @DexIgnore
    public w14 m;
    @DexIgnore
    public JSONObject n; // = null;

    @DexIgnore
    public t04(Context context, int i, JSONObject jSONObject, l04 l04) {
        super(context, i, l04);
        this.m = new w14(context);
        this.n = jSONObject;
    }

    @DexIgnore
    public f a() {
        return f.SESSION_ENV;
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        v14 v14 = this.d;
        if (v14 != null) {
            jSONObject.put("ut", v14.d());
        }
        JSONObject jSONObject2 = this.n;
        if (jSONObject2 != null) {
            jSONObject.put("cfg", jSONObject2);
        }
        if (f24.B(this.j)) {
            jSONObject.put("ncts", 1);
        }
        this.m.a(jSONObject, (Thread) null);
        return true;
    }
}
