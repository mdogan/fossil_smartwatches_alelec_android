package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.he0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vk0 extends pj0<yk0> {
    @DexIgnore
    public vk0(Context context, Looper looper, lj0 lj0, he0.b bVar, he0.c cVar) {
        super(context, looper, 39, lj0, bVar, cVar);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.service.ICommonService");
        if (queryLocalInterface instanceof yk0) {
            return (yk0) queryLocalInterface;
        }
        return new zk0(iBinder);
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.common.internal.service.ICommonService";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.common.service.START";
    }
}
