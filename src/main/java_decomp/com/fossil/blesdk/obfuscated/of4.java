package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class of4 implements df4<ke4> {
    @DexIgnore
    public /* final */ CharSequence a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ kd4<CharSequence, Integer, Pair<Integer, Integer>> d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<ke4>, de4 {
        @DexIgnore
        public int e; // = -1;
        @DexIgnore
        public int f;
        @DexIgnore
        public int g;
        @DexIgnore
        public ke4 h;
        @DexIgnore
        public int i;
        @DexIgnore
        public /* final */ /* synthetic */ of4 j;

        @DexIgnore
        public a(of4 of4) {
            this.j = of4;
            this.f = qe4.a(of4.b, 0, of4.a.length());
            this.g = this.f;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0023, code lost:
            if (r6.i < com.fossil.blesdk.obfuscated.of4.c(r6.j)) goto L_0x0025;
         */
        @DexIgnore
        public final void a() {
            int i2 = 0;
            if (this.g < 0) {
                this.e = 0;
                this.h = null;
                return;
            }
            if (this.j.c > 0) {
                this.i++;
            }
            if (this.g <= this.j.a.length()) {
                Pair pair = (Pair) this.j.d.invoke(this.j.a, Integer.valueOf(this.g));
                if (pair == null) {
                    this.h = new ke4(this.f, StringsKt__StringsKt.c(this.j.a));
                    this.g = -1;
                } else {
                    int intValue = ((Number) pair.component1()).intValue();
                    int intValue2 = ((Number) pair.component2()).intValue();
                    this.h = qe4.d(this.f, intValue);
                    this.f = intValue + intValue2;
                    int i3 = this.f;
                    if (intValue2 == 0) {
                        i2 = 1;
                    }
                    this.g = i3 + i2;
                }
                this.e = 1;
            }
            this.h = new ke4(this.f, StringsKt__StringsKt.c(this.j.a));
            this.g = -1;
            this.e = 1;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.e == -1) {
                a();
            }
            return this.e == 1;
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @DexIgnore
        public ke4 next() {
            if (this.e == -1) {
                a();
            }
            if (this.e != 0) {
                ke4 ke4 = this.h;
                if (ke4 != null) {
                    this.h = null;
                    this.e = -1;
                    return ke4;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.ranges.IntRange");
            }
            throw new NoSuchElementException();
        }
    }

    @DexIgnore
    public of4(CharSequence charSequence, int i, int i2, kd4<? super CharSequence, ? super Integer, Pair<Integer, Integer>> kd4) {
        wd4.b(charSequence, "input");
        wd4.b(kd4, "getNextMatch");
        this.a = charSequence;
        this.b = i;
        this.c = i2;
        this.d = kd4;
    }

    @DexIgnore
    public Iterator<ke4> iterator() {
        return new a(this);
    }
}
