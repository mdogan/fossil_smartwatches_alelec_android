package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface cx1 {
    @DexIgnore
    <T> void a(Class<T> cls, ax1<? super T> ax1);

    @DexIgnore
    <T> void a(Class<T> cls, Executor executor, ax1<? super T> ax1);
}
