package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import com.fossil.blesdk.obfuscated.r6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class z2 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ TypedArray b;
    @DexIgnore
    public TypedValue c;

    @DexIgnore
    public z2(Context context, TypedArray typedArray) {
        this.a = context;
        this.b = typedArray;
    }

    @DexIgnore
    public static z2 a(Context context, AttributeSet attributeSet, int[] iArr) {
        return new z2(context, context.obtainStyledAttributes(attributeSet, iArr));
    }

    @DexIgnore
    public Drawable b(int i) {
        if (this.b.hasValue(i)) {
            int resourceId = this.b.getResourceId(i, 0);
            if (resourceId != 0) {
                return m0.c(this.a, resourceId);
            }
        }
        return this.b.getDrawable(i);
    }

    @DexIgnore
    public Drawable c(int i) {
        if (!this.b.hasValue(i)) {
            return null;
        }
        int resourceId = this.b.getResourceId(i, 0);
        if (resourceId != 0) {
            return c2.a().a(this.a, resourceId, true);
        }
        return null;
    }

    @DexIgnore
    public String d(int i) {
        return this.b.getString(i);
    }

    @DexIgnore
    public CharSequence e(int i) {
        return this.b.getText(i);
    }

    @DexIgnore
    public int f(int i, int i2) {
        return this.b.getLayoutDimension(i, i2);
    }

    @DexIgnore
    public int g(int i, int i2) {
        return this.b.getResourceId(i, i2);
    }

    @DexIgnore
    public static z2 a(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2) {
        return new z2(context, context.obtainStyledAttributes(attributeSet, iArr, i, i2));
    }

    @DexIgnore
    public int d(int i, int i2) {
        return this.b.getInt(i, i2);
    }

    @DexIgnore
    public int e(int i, int i2) {
        return this.b.getInteger(i, i2);
    }

    @DexIgnore
    public CharSequence[] f(int i) {
        return this.b.getTextArray(i);
    }

    @DexIgnore
    public boolean g(int i) {
        return this.b.hasValue(i);
    }

    @DexIgnore
    public static z2 a(Context context, int i, int[] iArr) {
        return new z2(context, context.obtainStyledAttributes(i, iArr));
    }

    @DexIgnore
    public int c(int i, int i2) {
        return this.b.getDimensionPixelSize(i, i2);
    }

    @DexIgnore
    public Typeface a(int i, int i2, r6.a aVar) {
        int resourceId = this.b.getResourceId(i, 0);
        if (resourceId == 0) {
            return null;
        }
        if (this.c == null) {
            this.c = new TypedValue();
        }
        return r6.a(this.a, resourceId, this.c, i2, aVar);
    }

    @DexIgnore
    public float b(int i, float f) {
        return this.b.getFloat(i, f);
    }

    @DexIgnore
    public int b(int i, int i2) {
        return this.b.getDimensionPixelOffset(i, i2);
    }

    @DexIgnore
    public boolean a(int i, boolean z) {
        return this.b.getBoolean(i, z);
    }

    @DexIgnore
    public int a(int i, int i2) {
        return this.b.getColor(i, i2);
    }

    @DexIgnore
    public ColorStateList a(int i) {
        if (this.b.hasValue(i)) {
            int resourceId = this.b.getResourceId(i, 0);
            if (resourceId != 0) {
                ColorStateList b2 = m0.b(this.a, resourceId);
                if (b2 != null) {
                    return b2;
                }
            }
        }
        return this.b.getColorStateList(i);
    }

    @DexIgnore
    public float a(int i, float f) {
        return this.b.getDimension(i, f);
    }

    @DexIgnore
    public void a() {
        this.b.recycle();
    }
}
