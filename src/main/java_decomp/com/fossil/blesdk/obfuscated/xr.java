package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.Registry;
import com.fossil.blesdk.obfuscated.tr;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xr {
    @DexIgnore
    public static /* final */ c e; // = new c();
    @DexIgnore
    public static /* final */ tr<Object, Object> f; // = new a();
    @DexIgnore
    public /* final */ List<b<?, ?>> a;
    @DexIgnore
    public /* final */ c b;
    @DexIgnore
    public /* final */ Set<b<?, ?>> c;
    @DexIgnore
    public /* final */ h8<List<Throwable>> d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements tr<Object, Object> {
        @DexIgnore
        public tr.a<Object> a(Object obj, int i, int i2, mo moVar) {
            return null;
        }

        @DexIgnore
        public boolean a(Object obj) {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Model, Data> {
        @DexIgnore
        public /* final */ Class<Model> a;
        @DexIgnore
        public /* final */ Class<Data> b;
        @DexIgnore
        public /* final */ ur<? extends Model, ? extends Data> c;

        @DexIgnore
        public b(Class<Model> cls, Class<Data> cls2, ur<? extends Model, ? extends Data> urVar) {
            this.a = cls;
            this.b = cls2;
            this.c = urVar;
        }

        @DexIgnore
        public boolean a(Class<?> cls, Class<?> cls2) {
            return a(cls) && this.b.isAssignableFrom(cls2);
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public <Model, Data> wr<Model, Data> a(List<tr<Model, Data>> list, h8<List<Throwable>> h8Var) {
            return new wr<>(list, h8Var);
        }
    }

    @DexIgnore
    public xr(h8<List<Throwable>> h8Var) {
        this(h8Var, e);
    }

    @DexIgnore
    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, ur<? extends Model, ? extends Data> urVar) {
        a(cls, cls2, urVar, true);
    }

    @DexIgnore
    public synchronized List<Class<?>> b(Class<?> cls) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (b next : this.a) {
            if (!arrayList.contains(next.b) && next.a(cls)) {
                arrayList.add(next.b);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public xr(h8<List<Throwable>> h8Var, c cVar) {
        this.a = new ArrayList();
        this.c = new HashSet();
        this.d = h8Var;
        this.b = cVar;
    }

    @DexIgnore
    public final <Model, Data> void a(Class<Model> cls, Class<Data> cls2, ur<? extends Model, ? extends Data> urVar, boolean z) {
        b bVar = new b(cls, cls2, urVar);
        List<b<?, ?>> list = this.a;
        list.add(z ? list.size() : 0, bVar);
    }

    @DexIgnore
    public synchronized <Model> List<tr<Model, ?>> a(Class<Model> cls) {
        ArrayList arrayList;
        try {
            arrayList = new ArrayList();
            for (b next : this.a) {
                if (!this.c.contains(next)) {
                    if (next.a(cls)) {
                        this.c.add(next);
                        arrayList.add(a((b<?, ?>) next));
                        this.c.remove(next);
                    }
                }
            }
        } catch (Throwable th) {
            this.c.clear();
            throw th;
        }
        return arrayList;
    }

    @DexIgnore
    public synchronized <Model, Data> tr<Model, Data> a(Class<Model> cls, Class<Data> cls2) {
        try {
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (b next : this.a) {
                if (this.c.contains(next)) {
                    z = true;
                } else if (next.a(cls, cls2)) {
                    this.c.add(next);
                    arrayList.add(a((b<?, ?>) next));
                    this.c.remove(next);
                }
            }
            if (arrayList.size() > 1) {
                return this.b.a(arrayList, this.d);
            } else if (arrayList.size() == 1) {
                return (tr) arrayList.get(0);
            } else if (z) {
                return a();
            } else {
                throw new Registry.NoModelLoaderAvailableException(cls, cls2);
            }
        } catch (Throwable th) {
            this.c.clear();
            throw th;
        }
    }

    @DexIgnore
    public final <Model, Data> tr<Model, Data> a(b<?, ?> bVar) {
        tr<? extends Model, ? extends Data> a2 = bVar.c.a(this);
        uw.a(a2);
        return a2;
    }

    @DexIgnore
    public static <Model, Data> tr<Model, Data> a() {
        return f;
    }
}
