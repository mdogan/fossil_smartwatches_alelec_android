package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.nd0;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sd0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<sd0> CREATOR; // = new td0();
    @DexIgnore
    public vy0 e;
    @DexIgnore
    public byte[] f;
    @DexIgnore
    public int[] g;
    @DexIgnore
    public String[] h;
    @DexIgnore
    public int[] i;
    @DexIgnore
    public byte[][] j;
    @DexIgnore
    public im1[] k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public /* final */ ky0 m;
    @DexIgnore
    public /* final */ nd0.c n;
    @DexIgnore
    public /* final */ nd0.c o;

    @DexIgnore
    public sd0(vy0 vy0, ky0 ky0, nd0.c cVar, nd0.c cVar2, int[] iArr, String[] strArr, int[] iArr2, byte[][] bArr, im1[] im1Arr, boolean z) {
        this.e = vy0;
        this.m = ky0;
        this.n = cVar;
        this.o = null;
        this.g = iArr;
        this.h = null;
        this.i = iArr2;
        this.j = null;
        this.k = null;
        this.l = z;
    }

    @DexIgnore
    public sd0(vy0 vy0, byte[] bArr, int[] iArr, String[] strArr, int[] iArr2, byte[][] bArr2, boolean z, im1[] im1Arr) {
        this.e = vy0;
        this.f = bArr;
        this.g = iArr;
        this.h = strArr;
        this.m = null;
        this.n = null;
        this.o = null;
        this.i = iArr2;
        this.j = bArr2;
        this.k = im1Arr;
        this.l = z;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof sd0) {
            sd0 sd0 = (sd0) obj;
            return ak0.a(this.e, sd0.e) && Arrays.equals(this.f, sd0.f) && Arrays.equals(this.g, sd0.g) && Arrays.equals(this.h, sd0.h) && ak0.a(this.m, sd0.m) && ak0.a(this.n, sd0.n) && ak0.a(this.o, sd0.o) && Arrays.equals(this.i, sd0.i) && Arrays.deepEquals(this.j, sd0.j) && Arrays.equals(this.k, sd0.k) && this.l == sd0.l;
        }
    }

    @DexIgnore
    public final int hashCode() {
        return ak0.a(this.e, this.f, this.g, this.h, this.m, this.n, this.o, this.i, this.j, this.k, Boolean.valueOf(this.l));
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder("LogEventParcelable[");
        sb.append(this.e);
        sb.append(", LogEventBytes: ");
        byte[] bArr = this.f;
        sb.append(bArr == null ? null : new String(bArr));
        sb.append(", TestCodes: ");
        sb.append(Arrays.toString(this.g));
        sb.append(", MendelPackages: ");
        sb.append(Arrays.toString(this.h));
        sb.append(", LogEvent: ");
        sb.append(this.m);
        sb.append(", ExtensionProducer: ");
        sb.append(this.n);
        sb.append(", VeProducer: ");
        sb.append(this.o);
        sb.append(", ExperimentIDs: ");
        sb.append(Arrays.toString(this.i));
        sb.append(", ExperimentTokens: ");
        sb.append(Arrays.toString(this.j));
        sb.append(", ExperimentTokensParcelables: ");
        sb.append(Arrays.toString(this.k));
        sb.append(", AddPhenotypeExperimentTokens: ");
        sb.append(this.l);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 2, (Parcelable) this.e, i2, false);
        lk0.a(parcel, 3, this.f, false);
        lk0.a(parcel, 4, this.g, false);
        lk0.a(parcel, 5, this.h, false);
        lk0.a(parcel, 6, this.i, false);
        lk0.a(parcel, 7, this.j, false);
        lk0.a(parcel, 8, this.l);
        lk0.a(parcel, 9, (T[]) this.k, i2, false);
        lk0.a(parcel, a);
    }
}
