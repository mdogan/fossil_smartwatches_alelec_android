package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xx2 implements Factory<InactivityNudgeTimePresenter> {
    @DexIgnore
    public static InactivityNudgeTimePresenter a(vx2 vx2, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new InactivityNudgeTimePresenter(vx2, remindersSettingsDatabase);
    }
}
