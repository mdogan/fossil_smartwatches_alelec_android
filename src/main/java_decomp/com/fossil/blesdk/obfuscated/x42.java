package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x42 implements Factory<ContentResolver> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public x42(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static x42 a(o42 o42) {
        return new x42(o42);
    }

    @DexIgnore
    public static ContentResolver b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static ContentResolver c(o42 o42) {
        ContentResolver e = o42.e();
        o44.a(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }

    @DexIgnore
    public ContentResolver get() {
        return b(this.a);
    }
}
