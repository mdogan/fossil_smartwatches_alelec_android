package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ym1 extends kk0 implements ne0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ym1> CREATOR; // = new zm1();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Intent g;

    @DexIgnore
    public ym1(int i, int i2, Intent intent) {
        this.e = i;
        this.f = i2;
        this.g = intent;
    }

    @DexIgnore
    public final Status G() {
        if (this.f == 0) {
            return Status.i;
        }
        return Status.m;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, this.f);
        lk0.a(parcel, 3, (Parcelable) this.g, i, false);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public ym1() {
        this(0, (Intent) null);
    }

    @DexIgnore
    public ym1(int i, Intent intent) {
        this(2, 0, (Intent) null);
    }
}
