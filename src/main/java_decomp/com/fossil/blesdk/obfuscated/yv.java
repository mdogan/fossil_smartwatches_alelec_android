package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yv extends zv<Drawable> {
    @DexIgnore
    public yv(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* renamed from: e */
    public void c(Drawable drawable) {
        ((ImageView) this.e).setImageDrawable(drawable);
    }
}
