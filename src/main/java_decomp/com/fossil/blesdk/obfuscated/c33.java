package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c33 implements Factory<CommuteTimeSettingsDefaultAddressPresenter> {
    @DexIgnore
    public static CommuteTimeSettingsDefaultAddressPresenter a(z23 z23, fn2 fn2, UserRepository userRepository) {
        return new CommuteTimeSettingsDefaultAddressPresenter(z23, fn2, userRepository);
    }
}
