package com.fossil.blesdk.obfuscated;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.IdManager;
import java.util.Map;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fy {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ IdManager b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public fy(Context context, IdManager idManager, String str, String str2) {
        this.a = context;
        this.b = idManager;
        this.c = str;
        this.d = str2;
    }

    @DexIgnore
    public dy a() {
        Map<IdManager.DeviceIdentifierType, String> f = this.b.f();
        String n = CommonUtils.n(this.a);
        String k = this.b.k();
        String h = this.b.h();
        return new dy(this.b.d(), UUID.randomUUID().toString(), this.b.e(), this.b.l(), f.get(IdManager.DeviceIdentifierType.FONT_TOKEN), n, k, h, this.c, this.d);
    }
}
