package com.fossil.blesdk.obfuscated;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import com.misfit.frameworks.buttonservice.model.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xj {
    @DexIgnore
    public static /* final */ String a; // = ej.a("Alarms");

    @DexIgnore
    public static void a(Context context, uj ujVar, String str, long j) {
        dl b = ujVar.g().b();
        cl a2 = b.a(str);
        if (a2 != null) {
            a(context, str, a2.b);
            a(context, str, a2.b, j);
            return;
        }
        int b2 = new rl(context).b();
        b.a(new cl(str, b2));
        a(context, str, b2, j);
    }

    @DexIgnore
    public static void a(Context context, uj ujVar, String str) {
        dl b = ujVar.g().b();
        cl a2 = b.a(str);
        if (a2 != null) {
            a(context, str, a2.b);
            ej.a().a(a, String.format("Removing SystemIdInfo for workSpecId (%s)", new Object[]{str}), new Throwable[0]);
            b.b(str);
        }
    }

    @DexIgnore
    public static void a(Context context, String str, int i) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Alarm.TABLE_NAME);
        PendingIntent service = PendingIntent.getService(context, i, yj.a(context, str), 536870912);
        if (service != null && alarmManager != null) {
            ej.a().a(a, String.format("Cancelling existing alarm with (workSpecId, systemId) (%s, %s)", new Object[]{str, Integer.valueOf(i)}), new Throwable[0]);
            alarmManager.cancel(service);
        }
    }

    @DexIgnore
    public static void a(Context context, String str, int i, long j) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Alarm.TABLE_NAME);
        PendingIntent service = PendingIntent.getService(context, i, yj.a(context, str), 1073741824);
        if (alarmManager == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(0, j, service);
        } else {
            alarmManager.set(0, j, service);
        }
    }
}
