package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dh1 implements Runnable {
    @DexIgnore
    public /* final */ URL e;
    @DexIgnore
    public /* final */ byte[] f;
    @DexIgnore
    public /* final */ ah1 g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ Map<String, String> i;
    @DexIgnore
    public /* final */ /* synthetic */ yg1 j;

    @DexIgnore
    public dh1(yg1 yg1, String str, URL url, byte[] bArr, Map<String, String> map, ah1 ah1) {
        this.j = yg1;
        ck0.b(str);
        ck0.a(url);
        ck0.a(ah1);
        this.e = url;
        this.f = bArr;
        this.g = ah1;
        this.h = str;
        this.i = map;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00c6 A[SYNTHETIC, Splitter:B:44:0x00c6] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0101 A[SYNTHETIC, Splitter:B:57:0x0101] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x011b  */
    public final void run() {
        Map map;
        IOException iOException;
        int i2;
        HttpURLConnection httpURLConnection;
        Map map2;
        this.j.g();
        OutputStream outputStream = null;
        try {
            httpURLConnection = this.j.a(this.e);
            try {
                if (this.i != null) {
                    for (Map.Entry next : this.i.entrySet()) {
                        httpURLConnection.addRequestProperty((String) next.getKey(), (String) next.getValue());
                    }
                }
                if (this.f != null) {
                    byte[] b = this.j.m().b(this.f);
                    this.j.d().A().a("Uploading data. size", Integer.valueOf(b.length));
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.addRequestProperty(GraphRequest.CONTENT_ENCODING_HEADER, "gzip");
                    httpURLConnection.setFixedLengthStreamingMode(b.length);
                    httpURLConnection.connect();
                    OutputStream outputStream2 = httpURLConnection.getOutputStream();
                    try {
                        outputStream2.write(b);
                        outputStream2.close();
                    } catch (IOException e2) {
                        map2 = null;
                        iOException = e2;
                        outputStream = outputStream2;
                    } catch (Throwable th) {
                        th = th;
                        map = null;
                        outputStream = outputStream2;
                        i2 = 0;
                        if (outputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        this.j.a().a((Runnable) new bh1(this.h, this.g, i2, (Throwable) null, (byte[]) null, map));
                        throw th;
                    }
                }
                i2 = httpURLConnection.getResponseCode();
                try {
                    map = httpURLConnection.getHeaderFields();
                    try {
                        byte[] a = yg1.a(httpURLConnection);
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        this.j.a().a((Runnable) new bh1(this.h, this.g, i2, (Throwable) null, a, map));
                    } catch (IOException e3) {
                        e = e3;
                        iOException = e;
                        if (outputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        this.j.a().a((Runnable) new bh1(this.h, this.g, i2, iOException, (byte[]) null, map));
                    } catch (Throwable th2) {
                        th = th2;
                        if (outputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        this.j.a().a((Runnable) new bh1(this.h, this.g, i2, (Throwable) null, (byte[]) null, map));
                        throw th;
                    }
                } catch (IOException e4) {
                    e = e4;
                    map = null;
                    iOException = e;
                    if (outputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    this.j.a().a((Runnable) new bh1(this.h, this.g, i2, iOException, (byte[]) null, map));
                } catch (Throwable th3) {
                    th = th3;
                    map = null;
                    if (outputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    this.j.a().a((Runnable) new bh1(this.h, this.g, i2, (Throwable) null, (byte[]) null, map));
                    throw th;
                }
            } catch (IOException e5) {
                e = e5;
                map2 = null;
                iOException = e;
                i2 = 0;
                if (outputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                this.j.a().a((Runnable) new bh1(this.h, this.g, i2, iOException, (byte[]) null, map));
            } catch (Throwable th4) {
                th = th4;
                map = null;
                i2 = 0;
                if (outputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                this.j.a().a((Runnable) new bh1(this.h, this.g, i2, (Throwable) null, (byte[]) null, map));
                throw th;
            }
        } catch (IOException e6) {
            e = e6;
            httpURLConnection = null;
            map2 = null;
            iOException = e;
            i2 = 0;
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e7) {
                    this.j.d().s().a("Error closing HTTP compressed POST connection output stream. appId", ug1.a(this.h), e7);
                }
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            this.j.a().a((Runnable) new bh1(this.h, this.g, i2, iOException, (byte[]) null, map));
        } catch (Throwable th5) {
            th = th5;
            httpURLConnection = null;
            map = null;
            i2 = 0;
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e8) {
                    this.j.d().s().a("Error closing HTTP compressed POST connection output stream. appId", ug1.a(this.h), e8);
                }
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            this.j.a().a((Runnable) new bh1(this.h, this.g, i2, (Throwable) null, (byte[]) null, map));
            throw th;
        }
    }
}
