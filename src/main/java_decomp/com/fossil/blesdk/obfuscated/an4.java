package com.fossil.blesdk.obfuscated;

import com.facebook.appevents.AppEventsConstants;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Flushable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class an4 implements Closeable, Flushable {
    @DexIgnore
    public static /* final */ Pattern y; // = Pattern.compile("[a-z0-9_-]{1,120}");
    @DexIgnore
    public /* final */ go4 e;
    @DexIgnore
    public /* final */ File f;
    @DexIgnore
    public /* final */ File g;
    @DexIgnore
    public /* final */ File h;
    @DexIgnore
    public /* final */ File i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public long k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public long m; // = 0;
    @DexIgnore
    public wo4 n;
    @DexIgnore
    public /* final */ LinkedHashMap<String, d> o; // = new LinkedHashMap<>(0, 0.75f, true);
    @DexIgnore
    public int p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public long v; // = 0;
    @DexIgnore
    public /* final */ Executor w;
    @DexIgnore
    public /* final */ Runnable x; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            r5.e.u = true;
            r5.e.n = com.fossil.blesdk.obfuscated.ep4.a(com.fossil.blesdk.obfuscated.ep4.a());
         */
        @DexIgnore
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0033 */
        public void run() {
            synchronized (an4.this) {
                if (!(!an4.this.r) && !an4.this.s) {
                    an4.this.H();
                    an4.this.t = true;
                    if (an4.this.C()) {
                        an4.this.G();
                        an4.this.p = 0;
                    }
                } else {
                    return;
                }
            }
            return;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends bn4 {
        /*
        static {
            Class<an4> cls = an4.class;
        }
        */

        @DexIgnore
        public b(jp4 jp4) {
            super(jp4);
        }

        @DexIgnore
        public void a(IOException iOException) {
            an4.this.q = true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements Closeable {
        @DexIgnore
        public /* final */ String e;
        @DexIgnore
        public /* final */ long f;
        @DexIgnore
        public /* final */ kp4[] g;

        @DexIgnore
        public e(String str, long j, kp4[] kp4Arr, long[] jArr) {
            this.e = str;
            this.f = j;
            this.g = kp4Arr;
        }

        @DexIgnore
        public kp4 b(int i) {
            return this.g[i];
        }

        @DexIgnore
        public void close() {
            for (kp4 a : this.g) {
                vm4.a((Closeable) a);
            }
        }

        @DexIgnore
        public c y() throws IOException {
            return an4.this.a(this.e, this.f);
        }
    }

    /*
    static {
        Class<an4> cls = an4.class;
    }
    */

    @DexIgnore
    public an4(go4 go4, File file, int i2, int i3, long j2, Executor executor) {
        this.e = go4;
        this.f = file;
        this.j = i2;
        this.g = new File(file, "journal");
        this.h = new File(file, "journal.tmp");
        this.i = new File(file, "journal.bkp");
        this.l = i3;
        this.k = j2;
        this.w = executor;
    }

    @DexIgnore
    public static an4 a(go4 go4, File file, int i2, int i3, long j2) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 > 0) {
            return new an4(go4, file, i2, i3, j2, new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), vm4.a("OkHttp DiskLruCache", true)));
        } else {
            throw new IllegalArgumentException("valueCount <= 0");
        }
    }

    @DexIgnore
    public synchronized void A() throws IOException {
        B();
        for (d a2 : (d[]) this.o.values().toArray(new d[this.o.size()])) {
            a(a2);
        }
        this.t = false;
    }

    @DexIgnore
    public synchronized void B() throws IOException {
        if (!this.r) {
            if (this.e.d(this.i)) {
                if (this.e.d(this.g)) {
                    this.e.e(this.i);
                } else {
                    this.e.a(this.i, this.g);
                }
            }
            if (this.e.d(this.g)) {
                try {
                    F();
                    E();
                    this.r = true;
                    return;
                } catch (IOException e2) {
                    mo4 d2 = mo4.d();
                    d2.a(5, "DiskLruCache " + this.f + " is corrupt: " + e2.getMessage() + ", removing", (Throwable) e2);
                    z();
                    this.s = false;
                } catch (Throwable th) {
                    this.s = false;
                    throw th;
                }
            }
            G();
            this.r = true;
        }
    }

    @DexIgnore
    public boolean C() {
        int i2 = this.p;
        return i2 >= 2000 && i2 >= this.o.size();
    }

    @DexIgnore
    public final wo4 D() throws FileNotFoundException {
        return ep4.a((jp4) new b(this.e.f(this.g)));
    }

    @DexIgnore
    public final void E() throws IOException {
        this.e.e(this.h);
        Iterator<d> it = this.o.values().iterator();
        while (it.hasNext()) {
            d next = it.next();
            int i2 = 0;
            if (next.f == null) {
                while (i2 < this.l) {
                    this.m += next.b[i2];
                    i2++;
                }
            } else {
                next.f = null;
                while (i2 < this.l) {
                    this.e.e(next.c[i2]);
                    this.e.e(next.d[i2]);
                    i2++;
                }
                it.remove();
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:16|17|(1:19)(1:20)|21|22) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r9.p = r0 - r9.o.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006a, code lost:
        if (r1.g() == false) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006c, code lost:
        G();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0070, code lost:
        r9.n = D();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0079, code lost:
        return;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x005d */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x007a=Splitter:B:23:0x007a, B:16:0x005d=Splitter:B:16:0x005d} */
    public final void F() throws IOException {
        xo4 a2 = ep4.a(this.e.a(this.g));
        try {
            String i2 = a2.i();
            String i3 = a2.i();
            String i4 = a2.i();
            String i5 = a2.i();
            String i6 = a2.i();
            if (!"libcore.io.DiskLruCache".equals(i2) || !AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(i3) || !Integer.toString(this.j).equals(i4) || !Integer.toString(this.l).equals(i5) || !"".equals(i6)) {
                throw new IOException("unexpected journal header: [" + i2 + ", " + i3 + ", " + i5 + ", " + i6 + "]");
            }
            int i7 = 0;
            while (true) {
                g(a2.i());
                i7++;
            }
        } finally {
            vm4.a((Closeable) a2);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public synchronized void G() throws IOException {
        if (this.n != null) {
            this.n.close();
        }
        wo4 a2 = ep4.a(this.e.b(this.h));
        try {
            a2.a("libcore.io.DiskLruCache").writeByte(10);
            a2.a(AppEventsConstants.EVENT_PARAM_VALUE_YES).writeByte(10);
            a2.b((long) this.j).writeByte(10);
            a2.b((long) this.l).writeByte(10);
            a2.writeByte(10);
            for (d next : this.o.values()) {
                if (next.f != null) {
                    a2.a("DIRTY").writeByte(32);
                    a2.a(next.a);
                    a2.writeByte(10);
                } else {
                    a2.a("CLEAN").writeByte(32);
                    a2.a(next.a);
                    next.a(a2);
                    a2.writeByte(10);
                }
            }
            a2.close();
            if (this.e.d(this.g)) {
                this.e.a(this.g, this.i);
            }
            this.e.a(this.h, this.g);
            this.e.e(this.i);
            this.n = D();
            this.q = false;
            this.u = false;
        } catch (Throwable th) {
            a2.close();
            throw th;
        }
    }

    @DexIgnore
    public void H() throws IOException {
        while (this.m > this.k) {
            a(this.o.values().iterator().next());
        }
        this.t = false;
    }

    @DexIgnore
    public synchronized void close() throws IOException {
        if (this.r) {
            if (!this.s) {
                for (d dVar : (d[]) this.o.values().toArray(new d[this.o.size()])) {
                    if (dVar.f != null) {
                        dVar.f.a();
                    }
                }
                H();
                this.n.close();
                this.n = null;
                this.s = true;
                return;
            }
        }
        this.s = true;
    }

    @DexIgnore
    public c e(String str) throws IOException {
        return a(str, -1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004d, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004f, code lost:
        return null;
     */
    @DexIgnore
    public synchronized e f(String str) throws IOException {
        B();
        y();
        i(str);
        d dVar = this.o.get(str);
        if (dVar != null) {
            if (dVar.e) {
                e a2 = dVar.a();
                if (a2 == null) {
                    return null;
                }
                this.p++;
                this.n.a("READ").writeByte(32).a(str).writeByte(10);
                if (C()) {
                    this.w.execute(this.x);
                }
            }
        }
    }

    @DexIgnore
    public synchronized void flush() throws IOException {
        if (this.r) {
            y();
            H();
            this.n.flush();
        }
    }

    @DexIgnore
    public final void g(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i2 = indexOf + 1;
            int indexOf2 = str.indexOf(32, i2);
            if (indexOf2 == -1) {
                str2 = str.substring(i2);
                if (indexOf == 6 && str.startsWith("REMOVE")) {
                    this.o.remove(str2);
                    return;
                }
            } else {
                str2 = str.substring(i2, indexOf2);
            }
            d dVar = this.o.get(str2);
            if (dVar == null) {
                dVar = new d(str2);
                this.o.put(str2, dVar);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(" ");
                dVar.e = true;
                dVar.f = null;
                dVar.b(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                dVar.f = new c(dVar);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        return r7;
     */
    @DexIgnore
    public synchronized boolean h(String str) throws IOException {
        B();
        y();
        i(str);
        d dVar = this.o.get(str);
        if (dVar == null) {
            return false;
        }
        boolean a2 = a(dVar);
        if (a2 && this.m <= this.k) {
            this.t = false;
        }
    }

    @DexIgnore
    public final void i(String str) {
        if (!y.matcher(str).matches()) {
            throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,120}: \"" + str + "\"");
        }
    }

    @DexIgnore
    public synchronized boolean isClosed() {
        return this.s;
    }

    @DexIgnore
    public final synchronized void y() {
        if (isClosed()) {
            throw new IllegalStateException("cache is closed");
        }
    }

    @DexIgnore
    public void z() throws IOException {
        close();
        this.e.c(this.f);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ long[] b;
        @DexIgnore
        public /* final */ File[] c;
        @DexIgnore
        public /* final */ File[] d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public c f;
        @DexIgnore
        public long g;

        @DexIgnore
        public d(String str) {
            this.a = str;
            int i = an4.this.l;
            this.b = new long[i];
            this.c = new File[i];
            this.d = new File[i];
            StringBuilder sb = new StringBuilder(str);
            sb.append('.');
            int length = sb.length();
            for (int i2 = 0; i2 < an4.this.l; i2++) {
                sb.append(i2);
                this.c[i2] = new File(an4.this.f, sb.toString());
                sb.append(".tmp");
                this.d[i2] = new File(an4.this.f, sb.toString());
                sb.setLength(length);
            }
        }

        @DexIgnore
        public void a(wo4 wo4) throws IOException {
            for (long b2 : this.b) {
                wo4.writeByte(32).b(b2);
            }
        }

        @DexIgnore
        public void b(String[] strArr) throws IOException {
            if (strArr.length == an4.this.l) {
                int i = 0;
                while (i < strArr.length) {
                    try {
                        this.b[i] = Long.parseLong(strArr[i]);
                        i++;
                    } catch (NumberFormatException unused) {
                        a(strArr);
                        throw null;
                    }
                }
                return;
            }
            a(strArr);
            throw null;
        }

        @DexIgnore
        public final IOException a(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        @DexIgnore
        public e a() {
            if (Thread.holdsLock(an4.this)) {
                kp4[] kp4Arr = new kp4[an4.this.l];
                long[] jArr = (long[]) this.b.clone();
                int i = 0;
                int i2 = 0;
                while (i2 < an4.this.l) {
                    try {
                        kp4Arr[i2] = an4.this.e.a(this.c[i2]);
                        i2++;
                    } catch (FileNotFoundException unused) {
                        while (i < an4.this.l && kp4Arr[i] != null) {
                            vm4.a((Closeable) kp4Arr[i]);
                            i++;
                        }
                        try {
                            an4.this.a(this);
                            return null;
                        } catch (IOException unused2) {
                            return null;
                        }
                    }
                }
                return new e(this.a, this.g, kp4Arr, jArr);
            }
            throw new AssertionError();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
        return null;
     */
    @DexIgnore
    public synchronized c a(String str, long j2) throws IOException {
        B();
        y();
        i(str);
        d dVar = this.o.get(str);
        if (j2 == -1 || (dVar != null && dVar.g == j2)) {
            if (dVar != null) {
                if (dVar.f != null) {
                    return null;
                }
            }
            if (!this.t) {
                if (!this.u) {
                    this.n.a("DIRTY").writeByte(32).a(str).writeByte(10);
                    this.n.flush();
                    if (this.q) {
                        return null;
                    }
                    if (dVar == null) {
                        dVar = new d(str);
                        this.o.put(str, dVar);
                    }
                    c cVar = new c(dVar);
                    dVar.f = cVar;
                    return cVar;
                }
            }
            this.w.execute(this.x);
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c {
        @DexIgnore
        public /* final */ d a;
        @DexIgnore
        public /* final */ boolean[] b;
        @DexIgnore
        public boolean c;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends bn4 {
            @DexIgnore
            public a(jp4 jp4) {
                super(jp4);
            }

            @DexIgnore
            public void a(IOException iOException) {
                synchronized (an4.this) {
                    c.this.c();
                }
            }
        }

        @DexIgnore
        public c(d dVar) {
            this.a = dVar;
            this.b = dVar.e ? null : new boolean[an4.this.l];
        }

        @DexIgnore
        public jp4 a(int i) {
            synchronized (an4.this) {
                if (this.c) {
                    throw new IllegalStateException();
                } else if (this.a.f != this) {
                    jp4 a2 = ep4.a();
                    return a2;
                } else {
                    if (!this.a.e) {
                        this.b[i] = true;
                    }
                    try {
                        a aVar = new a(an4.this.e.b(this.a.d[i]));
                        return aVar;
                    } catch (FileNotFoundException unused) {
                        return ep4.a();
                    }
                }
            }
        }

        @DexIgnore
        public void b() throws IOException {
            synchronized (an4.this) {
                if (!this.c) {
                    if (this.a.f == this) {
                        an4.this.a(this, true);
                    }
                    this.c = true;
                } else {
                    throw new IllegalStateException();
                }
            }
        }

        @DexIgnore
        public void c() {
            if (this.a.f == this) {
                int i = 0;
                while (true) {
                    an4 an4 = an4.this;
                    if (i < an4.l) {
                        try {
                            an4.e.e(this.a.d[i]);
                        } catch (IOException unused) {
                        }
                        i++;
                    } else {
                        this.a.f = null;
                        return;
                    }
                }
            }
        }

        @DexIgnore
        public void a() throws IOException {
            synchronized (an4.this) {
                if (!this.c) {
                    if (this.a.f == this) {
                        an4.this.a(this, false);
                    }
                    this.c = true;
                } else {
                    throw new IllegalStateException();
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00f4, code lost:
        return;
     */
    @DexIgnore
    public synchronized void a(c cVar, boolean z) throws IOException {
        d dVar = cVar.a;
        if (dVar.f == cVar) {
            if (z && !dVar.e) {
                int i2 = 0;
                while (i2 < this.l) {
                    if (!cVar.b[i2]) {
                        cVar.a();
                        throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                    } else if (!this.e.d(dVar.d[i2])) {
                        cVar.a();
                        return;
                    } else {
                        i2++;
                    }
                }
            }
            for (int i3 = 0; i3 < this.l; i3++) {
                File file = dVar.d[i3];
                if (!z) {
                    this.e.e(file);
                } else if (this.e.d(file)) {
                    File file2 = dVar.c[i3];
                    this.e.a(file, file2);
                    long j2 = dVar.b[i3];
                    long g2 = this.e.g(file2);
                    dVar.b[i3] = g2;
                    this.m = (this.m - j2) + g2;
                }
            }
            this.p++;
            dVar.f = null;
            if (dVar.e || z) {
                dVar.e = true;
                this.n.a("CLEAN").writeByte(32);
                this.n.a(dVar.a);
                dVar.a(this.n);
                this.n.writeByte(10);
                if (z) {
                    long j3 = this.v;
                    this.v = 1 + j3;
                    dVar.g = j3;
                }
            } else {
                this.o.remove(dVar.a);
                this.n.a("REMOVE").writeByte(32);
                this.n.a(dVar.a);
                this.n.writeByte(10);
            }
            this.n.flush();
            if (this.m > this.k || C()) {
                this.w.execute(this.x);
            }
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public boolean a(d dVar) throws IOException {
        c cVar = dVar.f;
        if (cVar != null) {
            cVar.c();
        }
        for (int i2 = 0; i2 < this.l; i2++) {
            this.e.e(dVar.c[i2]);
            long j2 = this.m;
            long[] jArr = dVar.b;
            this.m = j2 - jArr[i2];
            jArr[i2] = 0;
        }
        this.p++;
        this.n.a("REMOVE").writeByte(32).a(dVar.a).writeByte(10);
        this.o.remove(dVar.a);
        if (C()) {
            this.w.execute(this.x);
        }
        return true;
    }
}
