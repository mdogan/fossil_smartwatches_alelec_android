package com.fossil.blesdk.obfuscated;

import com.google.android.gms.tasks.RuntimeExecutionException;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qo1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ xn1 e;
    @DexIgnore
    public /* final */ /* synthetic */ po1 f;

    @DexIgnore
    public qo1(po1 po1, xn1 xn1) {
        this.f = po1;
        this.e = xn1;
    }

    @DexIgnore
    public final void run() {
        try {
            xn1 then = this.f.b.then(this.e.b());
            if (then == null) {
                this.f.onFailure(new NullPointerException("Continuation returned null"));
                return;
            }
            then.a(zn1.b, this.f);
            then.a(zn1.b, (tn1) this.f);
            then.a(zn1.b, (rn1) this.f);
        } catch (RuntimeExecutionException e2) {
            if (e2.getCause() instanceof Exception) {
                this.f.onFailure((Exception) e2.getCause());
            } else {
                this.f.onFailure(e2);
            }
        } catch (CancellationException unused) {
            this.f.onCanceled();
        } catch (Exception e3) {
            this.f.onFailure(e3);
        }
    }
}
