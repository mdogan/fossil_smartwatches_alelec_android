package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fk2<TranscodeType> extends xn<TranscodeType> implements Cloneable {
    @DexIgnore
    public fk2(sn snVar, yn ynVar, Class<TranscodeType> cls, Context context) {
        super(snVar, ynVar, cls, context);
    }

    @DexIgnore
    public fk2<TranscodeType> c() {
        return (fk2) super.c();
    }

    @DexIgnore
    public fk2<TranscodeType> P() {
        return (fk2) super.P();
    }

    @DexIgnore
    public fk2<TranscodeType> Q() {
        return (fk2) super.Q();
    }

    @DexIgnore
    public fk2<TranscodeType> R() {
        return (fk2) super.R();
    }

    @DexIgnore
    public fk2<TranscodeType> b(boolean z) {
        return (fk2) super.b(z);
    }

    @DexIgnore
    public fk2<TranscodeType> b(int i) {
        return (fk2) super.b(i);
    }

    @DexIgnore
    public fk2<TranscodeType> clone() {
        return (fk2) super.clone();
    }

    @DexIgnore
    public fk2<TranscodeType> b() {
        return (fk2) super.b();
    }

    @DexIgnore
    public fk2<TranscodeType> b(rv<TranscodeType> rvVar) {
        return (fk2) super.b(rvVar);
    }

    @DexIgnore
    public fk2<TranscodeType> a(float f) {
        return (fk2) super.a(f);
    }

    @DexIgnore
    public fk2<TranscodeType> a(qp qpVar) {
        return (fk2) super.a(qpVar);
    }

    @DexIgnore
    public fk2<TranscodeType> a(Priority priority) {
        return (fk2) super.a(priority);
    }

    @DexIgnore
    public fk2<TranscodeType> a(boolean z) {
        return (fk2) super.a(z);
    }

    @DexIgnore
    public fk2<TranscodeType> a(int i, int i2) {
        return (fk2) super.a(i, i2);
    }

    @DexIgnore
    public fk2<TranscodeType> a(ko koVar) {
        return (fk2) super.a(koVar);
    }

    @DexIgnore
    public <Y> fk2<TranscodeType> a(lo<Y> loVar, Y y) {
        return (fk2) super.a(loVar, y);
    }

    @DexIgnore
    public fk2<TranscodeType> a(Class<?> cls) {
        return (fk2) super.a(cls);
    }

    @DexIgnore
    public fk2<TranscodeType> a(DownsampleStrategy downsampleStrategy) {
        return (fk2) super.a(downsampleStrategy);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.fossil.blesdk.obfuscated.po, com.fossil.blesdk.obfuscated.po<android.graphics.Bitmap>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public fk2<TranscodeType> a(po<Bitmap> r1) {
        return (fk2) super.a((po<Bitmap>) r1);
    }

    @DexIgnore
    public fk2<TranscodeType> a(mv<?> mvVar) {
        return (fk2) super.a((mv) mvVar);
    }

    @DexIgnore
    public fk2<TranscodeType> a(rv<TranscodeType> rvVar) {
        super.a(rvVar);
        return this;
    }

    @DexIgnore
    public fk2<TranscodeType> a(xn<TranscodeType> xnVar) {
        super.a(xnVar);
        return this;
    }

    @DexIgnore
    public fk2<TranscodeType> a(Object obj) {
        super.a(obj);
        return this;
    }

    @DexIgnore
    public fk2<TranscodeType> a(String str) {
        super.a(str);
        return this;
    }

    @DexIgnore
    public fk2<TranscodeType> a(Uri uri) {
        super.a(uri);
        return this;
    }

    @DexIgnore
    public fk2<TranscodeType> a(Integer num) {
        return (fk2) super.a(num);
    }

    @DexIgnore
    public fk2<TranscodeType> a(byte[] bArr) {
        return (fk2) super.a(bArr);
    }
}
