package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ip4 {
    @DexIgnore
    public static hp4 a;
    @DexIgnore
    public static long b;

    @DexIgnore
    public static hp4 a() {
        synchronized (ip4.class) {
            if (a == null) {
                return new hp4();
            }
            hp4 hp4 = a;
            a = hp4.f;
            hp4.f = null;
            b -= 8192;
            return hp4;
        }
    }

    @DexIgnore
    public static void a(hp4 hp4) {
        if (hp4.f != null || hp4.g != null) {
            throw new IllegalArgumentException();
        } else if (!hp4.d) {
            synchronized (ip4.class) {
                if (b + 8192 <= 65536) {
                    b += 8192;
                    hp4.f = a;
                    hp4.c = 0;
                    hp4.b = 0;
                    a = hp4;
                }
            }
        }
    }
}
