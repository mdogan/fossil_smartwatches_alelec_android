package com.fossil.blesdk.obfuscated;

import android.view.ViewTreeObserver;
import android.widget.ImageView;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yx3 implements ViewTreeObserver.OnPreDrawListener {
    @DexIgnore
    public /* final */ iy3 e;
    @DexIgnore
    public /* final */ WeakReference<ImageView> f;
    @DexIgnore
    public vx3 g;

    @DexIgnore
    public yx3(iy3 iy3, ImageView imageView, vx3 vx3) {
        this.e = iy3;
        this.f = new WeakReference<>(imageView);
        this.g = vx3;
        imageView.getViewTreeObserver().addOnPreDrawListener(this);
    }

    @DexIgnore
    public void a() {
        this.g = null;
        ImageView imageView = (ImageView) this.f.get();
        if (imageView != null) {
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this);
            }
        }
    }

    @DexIgnore
    public boolean onPreDraw() {
        ImageView imageView = (ImageView) this.f.get();
        if (imageView == null) {
            return true;
        }
        ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
        if (!viewTreeObserver.isAlive()) {
            return true;
        }
        int width = imageView.getWidth();
        int height = imageView.getHeight();
        if (width > 0 && height > 0) {
            viewTreeObserver.removeOnPreDrawListener(this);
            iy3 iy3 = this.e;
            iy3.c();
            iy3.a(width, height);
            iy3.a(imageView, this.g);
        }
        return true;
    }
}
