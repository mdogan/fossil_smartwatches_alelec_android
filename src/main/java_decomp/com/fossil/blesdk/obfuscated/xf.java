package com.fossil.blesdk.obfuscated;

import androidx.room.RoomDatabase;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class xf {
    @DexIgnore
    public /* final */ RoomDatabase mDatabase;
    @DexIgnore
    public /* final */ AtomicBoolean mLock; // = new AtomicBoolean(false);
    @DexIgnore
    public volatile lg mStmt;

    @DexIgnore
    public xf(RoomDatabase roomDatabase) {
        this.mDatabase = roomDatabase;
    }

    @DexIgnore
    private lg createNewStatement() {
        return this.mDatabase.compileStatement(createQuery());
    }

    @DexIgnore
    private lg getStmt(boolean z) {
        if (!z) {
            return createNewStatement();
        }
        if (this.mStmt == null) {
            this.mStmt = createNewStatement();
        }
        return this.mStmt;
    }

    @DexIgnore
    public lg acquire() {
        assertNotMainThread();
        return getStmt(this.mLock.compareAndSet(false, true));
    }

    @DexIgnore
    public void assertNotMainThread() {
        this.mDatabase.assertNotMainThread();
    }

    @DexIgnore
    public abstract String createQuery();

    @DexIgnore
    public void release(lg lgVar) {
        if (lgVar == this.mStmt) {
            this.mLock.set(false);
        }
    }
}
