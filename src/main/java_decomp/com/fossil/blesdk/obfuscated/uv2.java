package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.n62;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uv2 extends bs2 implements tv2, ls2 {
    @DexIgnore
    public static /* final */ a q; // = new a((rd4) null);
    @DexIgnore
    public ur3<tc2> k;
    @DexIgnore
    public sv2 l;
    @DexIgnore
    public n62 m;
    @DexIgnore
    public qv2 n;
    @DexIgnore
    public DoNotDisturbScheduledTimePresenter o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final uv2 a() {
            return new uv2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements n62.b {
        @DexIgnore
        public /* final */ /* synthetic */ uv2 a;

        @DexIgnore
        public b(uv2 uv2) {
            this.a = uv2;
        }

        @DexIgnore
        public void a(Alarm alarm) {
            wd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            uv2.b(this.a).a(alarm, !alarm.isActive());
        }

        @DexIgnore
        public void b(Alarm alarm) {
            wd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            uv2.b(this.a).a(alarm);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ uv2 e;

        @DexIgnore
        public c(uv2 uv2) {
            this.e = uv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                wd4.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, 2, (Object) null);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ uv2 e;

        @DexIgnore
        public d(uv2 uv2) {
            this.e = uv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            uv2.b(this.e).a((Alarm) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ uv2 e;

        @DexIgnore
        public e(uv2 uv2) {
            this.e = uv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationWatchRemindersActivity.C.a(this.e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ uv2 e;

        @DexIgnore
        public f(uv2 uv2) {
            this.e = uv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            us3.a(view);
            NotificationAppsActivity.C.a(this.e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ uv2 e;

        @DexIgnore
        public g(uv2 uv2) {
            this.e = uv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (cn2.a(cn2.d, this.e.getContext(), "NOTIFICATION_CONTACTS", false, 4, (Object) null)) {
                NotificationCallsAndMessagesActivity.C.a(this.e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ uv2 e;

        @DexIgnore
        public h(uv2 uv2) {
            this.e = uv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            uv2.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ uv2 e;

        @DexIgnore
        public i(uv2 uv2) {
            this.e = uv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            qv2 a = this.e.n;
            if (a != null) {
                a.p(0);
            }
            qv2 a2 = this.e.n;
            if (a2 != null) {
                FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                wd4.a((Object) childFragmentManager, "childFragmentManager");
                a2.show(childFragmentManager, qv2.r.a());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ uv2 e;

        @DexIgnore
        public j(uv2 uv2) {
            this.e = uv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            qv2 a = this.e.n;
            if (a != null) {
                a.p(1);
            }
            qv2 a2 = this.e.n;
            if (a2 != null) {
                FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                wd4.a((Object) childFragmentManager, "childFragmentManager");
                a2.show(childFragmentManager, qv2.r.a());
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ sv2 b(uv2 uv2) {
        sv2 sv2 = uv2.l;
        if (sv2 != null) {
            return sv2;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N(boolean z) {
        if (z) {
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wl2 Q02 = Q0();
        if (Q02 != null) {
            Q02.a("");
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HomeAlertsFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public void d(List<Alarm> list) {
        wd4.b(list, "alarms");
        n62 n62 = this.m;
        if (n62 != null) {
            n62.a(list);
        } else {
            wd4.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void h(String str) {
        wd4.b(str, "notificationAppOverView");
        ur3<tc2> ur3 = this.k;
        if (ur3 != null) {
            tc2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void m(boolean z) {
        Context context;
        int i2;
        ur3<tc2> ur3 = this.k;
        if (ur3 != null) {
            tc2 a2 = ur3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.E;
                wd4.a((Object) switchCompat, "it.swScheduled");
                switchCompat.setChecked(z);
                ConstraintLayout constraintLayout = a2.r;
                wd4.a((Object) constraintLayout, "it.clScheduledTimeContainer");
                constraintLayout.setVisibility(z ? 0 : 8);
                FlexibleTextView flexibleTextView = a2.u;
                if (z) {
                    context = getContext();
                    if (context != null) {
                        i2 = R.color.primaryText;
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    context = getContext();
                    if (context != null) {
                        i2 = R.color.warmGrey;
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
                flexibleTextView.setTextColor(k6.a(context, i2));
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        tc2 tc2 = (tc2) ra.a(layoutInflater, R.layout.fragment_home_alerts, viewGroup, false, O0());
        this.n = (qv2) getChildFragmentManager().a(qv2.r.a());
        if (this.n == null) {
            this.n = qv2.r.b();
        }
        tc2.s.setOnClickListener(new d(this));
        tc2.C.setOnClickListener(new e(this));
        tc2.y.setOnClickListener(new f(this));
        tc2.z.setOnClickListener(new g(this));
        tc2.E.setOnClickListener(new h(this));
        tc2.B.setOnClickListener(new i(this));
        tc2.A.setOnClickListener(new j(this));
        vi2 vi2 = tc2.x;
        if (vi2 != null) {
            ConstraintLayout constraintLayout = vi2.q;
            wd4.a((Object) constraintLayout, "viewNoDeviceBinding.clRoot");
            constraintLayout.setVisibility(0);
            vi2.t.setImageResource(R.drawable.alerts_no_device);
            FlexibleTextView flexibleTextView = vi2.r;
            wd4.a((Object) flexibleTextView, "viewNoDeviceBinding.ftvDescription");
            flexibleTextView.setText(tm2.a(getContext(), (int) R.string.Onboarding_WithoutDevice_Alerts_Text__SetAlarmsAndGetNotificationUpdates));
            vi2.s.setOnClickListener(new c(this));
        }
        n62 n62 = new n62();
        n62.a((n62.b) new b(this));
        this.m = n62;
        RecyclerView recyclerView = tc2.D;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
        n62 n622 = this.m;
        if (n622 != null) {
            recyclerView.setAdapter(n622);
            this.k = new ur3<>(this, tc2);
            m42 g2 = PortfolioApp.W.c().g();
            qv2 qv2 = this.n;
            if (qv2 != null) {
                g2.a(new wv2(qv2)).a(this);
                ur3<tc2> ur3 = this.k;
                if (ur3 != null) {
                    tc2 a2 = ur3.a();
                    if (a2 != null) {
                        wd4.a((Object) a2, "mBinding.get()!!");
                        return a2.d();
                    }
                    wd4.a();
                    throw null;
                }
                wd4.d("mBinding");
                throw null;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimeContract.View");
        }
        wd4.d("mAlarmsAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        sv2 sv2 = this.l;
        if (sv2 != null) {
            if (sv2 != null) {
                sv2.g();
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
        wl2 Q0 = Q0();
        if (Q0 != null) {
            Q0.a("");
        }
        super.onPause();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        sv2 sv2 = this.l;
        if (sv2 != null) {
            if (sv2 != null) {
                sv2.f();
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
        wl2 Q0 = Q0();
        if (Q0 != null) {
            Q0.d();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        R("alert_view");
    }

    @DexIgnore
    public void r() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.y(childFragmentManager);
        }
    }

    @DexIgnore
    public void s() {
        FLogger.INSTANCE.getLocal().d("HomeAlertsFragment", "notifyListAlarm()");
        n62 n62 = this.m;
        if (n62 != null) {
            n62.notifyDataSetChanged();
        } else {
            wd4.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void w() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.G(childFragmentManager);
        }
    }

    @DexIgnore
    public void b(SpannableString spannableString) {
        wd4.b(spannableString, LogBuilder.KEY_TIME);
        ur3<tc2> ur3 = this.k;
        if (ur3 != null) {
            tc2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(spannableString.toString());
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(sv2 sv2) {
        wd4.b(sv2, "presenter");
        this.l = sv2;
    }

    @DexIgnore
    public void a(String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        wd4.b(str, "deviceId");
        wd4.b(arrayList, "mAlarms");
        AlarmActivity.a aVar = AlarmActivity.C;
        Context context = getContext();
        if (context != null) {
            wd4.a((Object) context, "context!!");
            aVar.a(context, str, arrayList, alarm);
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void a(boolean z) {
        ur3<tc2> ur3 = this.k;
        if (ur3 != null) {
            tc2 a2 = ur3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                if (constraintLayout != null) {
                    constraintLayout.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(SpannableString spannableString) {
        wd4.b(spannableString, LogBuilder.KEY_TIME);
        ur3<tc2> ur3 = this.k;
        if (ur3 != null) {
            tc2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(spannableString);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }
}
