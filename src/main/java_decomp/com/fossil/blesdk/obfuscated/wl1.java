package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wl1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<wl1> CREATOR; // = new xl1();
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public ll1 g;
    @DexIgnore
    public long h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public String j;
    @DexIgnore
    public ig1 k;
    @DexIgnore
    public long l;
    @DexIgnore
    public ig1 m;
    @DexIgnore
    public long n;
    @DexIgnore
    public ig1 o;

    @DexIgnore
    public wl1(wl1 wl1) {
        ck0.a(wl1);
        this.e = wl1.e;
        this.f = wl1.f;
        this.g = wl1.g;
        this.h = wl1.h;
        this.i = wl1.i;
        this.j = wl1.j;
        this.k = wl1.k;
        this.l = wl1.l;
        this.m = wl1.m;
        this.n = wl1.n;
        this.o = wl1.o;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 2, this.e, false);
        lk0.a(parcel, 3, this.f, false);
        lk0.a(parcel, 4, (Parcelable) this.g, i2, false);
        lk0.a(parcel, 5, this.h);
        lk0.a(parcel, 6, this.i);
        lk0.a(parcel, 7, this.j, false);
        lk0.a(parcel, 8, (Parcelable) this.k, i2, false);
        lk0.a(parcel, 9, this.l);
        lk0.a(parcel, 10, (Parcelable) this.m, i2, false);
        lk0.a(parcel, 11, this.n);
        lk0.a(parcel, 12, (Parcelable) this.o, i2, false);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public wl1(String str, String str2, ll1 ll1, long j2, boolean z, String str3, ig1 ig1, long j3, ig1 ig12, long j4, ig1 ig13) {
        this.e = str;
        this.f = str2;
        this.g = ll1;
        this.h = j2;
        this.i = z;
        this.j = str3;
        this.k = ig1;
        this.l = j3;
        this.m = ig12;
        this.n = j4;
        this.o = ig13;
    }
}
