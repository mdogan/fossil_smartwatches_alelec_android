package com.fossil.blesdk.obfuscated;

import android.os.SystemClock;
import android.text.TextUtils;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import retrofit.mime.MultipartTypedOutput;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class t44 implements Callable<Map<String, y44>> {
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public t44(String str) {
        this.e = str;
    }

    @DexIgnore
    public final Map<String, y44> a() {
        HashMap hashMap = new HashMap();
        try {
            Class.forName("com.google.android.gms.ads.AdView");
            y44 y44 = new y44("com.google.firebase.firebase-ads", "0.0.0", MultipartTypedOutput.DEFAULT_TRANSFER_ENCODING);
            hashMap.put(y44.b(), y44);
            r44.g().v("Fabric", "Found kit: com.google.firebase.firebase-ads");
        } catch (Exception unused) {
        }
        return hashMap;
    }

    @DexIgnore
    public final Map<String, y44> b() throws Exception {
        HashMap hashMap = new HashMap();
        ZipFile c = c();
        Enumeration<? extends ZipEntry> entries = c.entries();
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) entries.nextElement();
            if (zipEntry.getName().startsWith("fabric/") && zipEntry.getName().length() > 7) {
                y44 a = a(zipEntry, c);
                if (a != null) {
                    hashMap.put(a.b(), a);
                    r44.g().v("Fabric", String.format("Found kit:[%s] version:[%s]", new Object[]{a.b(), a.c()}));
                }
            }
        }
        if (c != null) {
            try {
                c.close();
            } catch (IOException unused) {
            }
        }
        return hashMap;
    }

    @DexIgnore
    public ZipFile c() throws IOException {
        return new ZipFile(this.e);
    }

    @DexIgnore
    public Map<String, y44> call() throws Exception {
        HashMap hashMap = new HashMap();
        long elapsedRealtime = SystemClock.elapsedRealtime();
        hashMap.putAll(a());
        hashMap.putAll(b());
        z44 g = r44.g();
        g.v("Fabric", "finish scanning in " + (SystemClock.elapsedRealtime() - elapsedRealtime));
        return hashMap;
    }

    @DexIgnore
    public final y44 a(ZipEntry zipEntry, ZipFile zipFile) {
        InputStream inputStream;
        try {
            inputStream = zipFile.getInputStream(zipEntry);
            try {
                Properties properties = new Properties();
                properties.load(inputStream);
                String property = properties.getProperty("fabric-identifier");
                String property2 = properties.getProperty("fabric-version");
                String property3 = properties.getProperty("fabric-build-type");
                if (TextUtils.isEmpty(property) || TextUtils.isEmpty(property2)) {
                    throw new IllegalStateException("Invalid format of fabric file," + zipEntry.getName());
                }
                y44 y44 = new y44(property, property2, property3);
                CommonUtils.a((Closeable) inputStream);
                return y44;
            } catch (IOException e2) {
                e = e2;
                try {
                    r44.g().e("Fabric", "Error when parsing fabric properties " + zipEntry.getName(), e);
                    CommonUtils.a((Closeable) inputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    CommonUtils.a((Closeable) inputStream);
                    throw th;
                }
            }
        } catch (IOException e3) {
            e = e3;
            inputStream = null;
            r44.g().e("Fabric", "Error when parsing fabric properties " + zipEntry.getName(), e);
            CommonUtils.a((Closeable) inputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            CommonUtils.a((Closeable) inputStream);
            throw th;
        }
    }
}
