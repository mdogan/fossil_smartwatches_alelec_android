package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface bn1 extends IInterface {
    @DexIgnore
    void a(hn1 hn1) throws RemoteException;

    @DexIgnore
    void a(vd0 vd0, ym1 ym1) throws RemoteException;

    @DexIgnore
    void a(Status status, GoogleSignInAccount googleSignInAccount) throws RemoteException;

    @DexIgnore
    void d(Status status) throws RemoteException;

    @DexIgnore
    void e(Status status) throws RemoteException;
}
