package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jh {
    @DexIgnore
    public ViewGroup a;
    @DexIgnore
    public Runnable b;

    @DexIgnore
    public void a() {
        if (a(this.a) == this) {
            Runnable runnable = this.b;
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    @DexIgnore
    public static void a(View view, jh jhVar) {
        view.setTag(hh.transition_current_scene, jhVar);
    }

    @DexIgnore
    public static jh a(View view) {
        return (jh) view.getTag(hh.transition_current_scene);
    }
}
