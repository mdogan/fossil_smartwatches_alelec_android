package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uz2 implements Factory<LoaderManager> {
    @DexIgnore
    public static LoaderManager a(tz2 tz2) {
        LoaderManager a = tz2.a();
        o44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
