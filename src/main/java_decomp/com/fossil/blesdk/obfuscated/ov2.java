package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ov2 implements Factory<hg3> {
    @DexIgnore
    public static hg3 a(hv2 hv2) {
        hg3 g = hv2.g();
        o44.a(g, "Cannot return null from a non-@Nullable @Provides method");
        return g;
    }
}
