package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.qt2;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ow2 extends bs2 implements nw2, xs3.g {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a((rd4) null);
    @DexIgnore
    public mw2 k;
    @DexIgnore
    public ur3<td2> l;
    @DexIgnore
    public qt2 m;
    @DexIgnore
    public jx2 n;
    @DexIgnore
    public nx2 o;
    @DexIgnore
    public ex2 p;
    @DexIgnore
    public k42 q;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ow2.s;
        }

        @DexIgnore
        public final ow2 b() {
            return new ow2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements qt2.b {
        @DexIgnore
        public /* final */ /* synthetic */ ow2 a;

        @DexIgnore
        public b(ow2 ow2) {
            this.a = ow2;
        }

        @DexIgnore
        public void a() {
            NotificationContactsActivity.C.a(this.a);
        }

        @DexIgnore
        public void a(ContactGroup contactGroup) {
            wd4.b(contactGroup, "contactGroup");
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(childFragmentManager, contactGroup);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ow2 e;

        @DexIgnore
        public c(ow2 ow2) {
            this.e = ow2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(ow2.t.a(), "press on button back");
            ow2.a(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ow2 e;

        @DexIgnore
        public d(ow2 ow2) {
            this.e = ow2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ow2.a(this.e).a(true);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ow2 e;

        @DexIgnore
        public e(ow2 ow2) {
            this.e = ow2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ow2.a(this.e).a(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ow2 e;

        @DexIgnore
        public f(ow2 ow2) {
            this.e = ow2;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationContactsActivity.C.a(this.e);
        }
    }

    /*
    static {
        String simpleName = ow2.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationCallsAndMess\u2026nt::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ mw2 a(ow2 ow2) {
        mw2 mw2 = ow2.k;
        if (mw2 != null) {
            return mw2;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    public int E() {
        CharSequence charSequence;
        ur3<td2> ur3 = this.l;
        CharSequence charSequence2 = null;
        if (ur3 != null) {
            td2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.r;
                if (flexibleTextView != null) {
                    charSequence = flexibleTextView.getText();
                    if (!wd4.a((Object) charSequence, (Object) tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__Everyone))) {
                        return 0;
                    }
                    ur3<td2> ur32 = this.l;
                    if (ur32 != null) {
                        td2 a3 = ur32.a();
                        if (a3 != null) {
                            FlexibleTextView flexibleTextView2 = a3.r;
                            if (flexibleTextView2 != null) {
                                charSequence2 = flexibleTextView2.getText();
                            }
                        }
                        return wd4.a((Object) charSequence2, (Object) tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__FavoriteContacts)) ? 1 : 2;
                    }
                    wd4.d("mBinding");
                    throw null;
                }
            }
            charSequence = null;
            if (!wd4.a((Object) charSequence, (Object) tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__Everyone))) {
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    public int J() {
        CharSequence charSequence;
        ur3<td2> ur3 = this.l;
        CharSequence charSequence2 = null;
        if (ur3 != null) {
            td2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                if (flexibleTextView != null) {
                    charSequence = flexibleTextView.getText();
                    if (!wd4.a((Object) charSequence, (Object) tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__Everyone))) {
                        return 0;
                    }
                    ur3<td2> ur32 = this.l;
                    if (ur32 != null) {
                        td2 a3 = ur32.a();
                        if (a3 != null) {
                            FlexibleTextView flexibleTextView2 = a3.s;
                            if (flexibleTextView2 != null) {
                                charSequence2 = flexibleTextView2.getText();
                            }
                        }
                        return wd4.a((Object) charSequence2, (Object) tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__FavoriteContacts)) ? 1 : 2;
                    }
                    wd4.d("mBinding");
                    throw null;
                }
            }
            charSequence = null;
            if (!wd4.a((Object) charSequence, (Object) tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__Everyone))) {
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void N() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Window window = activity.getWindow();
            if (window != null) {
                window.clearFlags(16);
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void P() {
        FLogger.INSTANCE.getLocal().d(s, "showNotificationSettingDialog");
        if (isActive()) {
            jx2 jx2 = this.n;
            if (jx2 != null) {
                FragmentManager childFragmentManager = getChildFragmentManager();
                wd4.a((Object) childFragmentManager, "childFragmentManager");
                jx2.show(childFragmentManager, jx2.t.a());
            }
        }
    }

    @DexIgnore
    public List<ContactGroup> Q() {
        qt2 qt2 = this.m;
        if (qt2 != null) {
            return qt2.b();
        }
        wd4.d("mNotificationFavoriteContactsAdapter");
        throw null;
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d(s, "onActivityBackPressed -");
        mw2 mw2 = this.k;
        if (mw2 != null) {
            mw2.i();
            return true;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void T() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Window window = activity.getWindow();
            if (window != null) {
                window.setFlags(16, 16);
            }
        }
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void i(String str) {
        wd4.b(str, "settingsTypeName");
        ur3<td2> ur3 = this.l;
        if (ur3 != null) {
            td2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void l(List<ContactGroup> list) {
        wd4.b(list, "mListContactGroup");
        qt2 qt2 = this.m;
        if (qt2 != null) {
            qt2.a(list);
        } else {
            wd4.d("mNotificationFavoriteContactsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void m(String str) {
        wd4.b(str, "settingsTypeName");
        ur3<td2> ur3 = this.l;
        if (ur3 != null) {
            td2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.r;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        m42 g = PortfolioApp.W.c().g();
        jx2 jx2 = this.n;
        if (jx2 != null) {
            g.a(new lx2(jx2)).a(this);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity = (NotificationCallsAndMessagesActivity) activity;
                k42 k42 = this.q;
                if (k42 != null) {
                    jc a2 = mc.a((FragmentActivity) notificationCallsAndMessagesActivity, (lc.b) k42).a(ex2.class);
                    wd4.a((Object) a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                    this.p = (ex2) a2;
                    mw2 mw2 = this.k;
                    if (mw2 != null) {
                        ex2 ex2 = this.p;
                        if (ex2 != null) {
                            mw2.a(ex2);
                        } else {
                            wd4.d("mNotificationSettingViewModel");
                            throw null;
                        }
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                } else {
                    wd4.d("viewModelFactory");
                    throw null;
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity");
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        td2 td2 = (td2) ra.a(layoutInflater, R.layout.fragment_notification_calls_and_messages, viewGroup, false, O0());
        this.n = (jx2) getChildFragmentManager().a(jx2.t.a());
        if (this.n == null) {
            this.n = jx2.t.b();
        }
        td2.t.setOnClickListener(new c(this));
        td2.u.setOnClickListener(new d(this));
        td2.v.setOnClickListener(new e(this));
        td2.q.setOnClickListener(new f(this));
        qt2 qt2 = new qt2();
        qt2.a((qt2.b) new b(this));
        this.m = qt2;
        RecyclerView recyclerView = td2.w;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        qt2 qt22 = this.m;
        if (qt22 != null) {
            recyclerView.setAdapter(qt22);
            recyclerView.setHasFixedSize(true);
            m42 g = PortfolioApp.W.c().g();
            jx2 jx2 = this.n;
            if (jx2 != null) {
                g.a(new lx2(jx2)).a(this);
                this.l = new ur3<>(this, td2);
                R("call_message_view");
                wd4.a((Object) td2, "binding");
                return td2.d();
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
        }
        wd4.d("mNotificationFavoriteContactsAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        mw2 mw2 = this.k;
        if (mw2 != null) {
            mw2.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
            }
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        mw2 mw2 = this.k;
        if (mw2 != null) {
            mw2.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void a(mw2 mw2) {
        wd4.b(mw2, "presenter");
        this.k = mw2;
    }

    @DexIgnore
    public void a(ContactGroup contactGroup) {
        wd4.b(contactGroup, "contactGroup");
        qt2 qt2 = this.m;
        if (qt2 != null) {
            qt2.a(contactGroup);
        } else {
            wd4.d("mNotificationFavoriteContactsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -638196028) {
            if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i == R.id.tv_ok) {
                mw2 mw2 = this.k;
                if (mw2 != null) {
                    mw2.h();
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        } else if (str.equals("CONFIRM_REMOVE_CONTACT") && i == R.id.tv_ok) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null) {
                ContactGroup contactGroup = (ContactGroup) extras.getSerializable("CONFIRM_REMOVE_CONTACT_BUNDLE");
                if (contactGroup != null) {
                    mw2 mw22 = this.k;
                    if (mw22 != null) {
                        mw22.a(contactGroup);
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        }
    }
}
