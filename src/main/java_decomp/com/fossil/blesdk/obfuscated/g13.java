package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g13 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<i13> c;
    @DexIgnore
    public ArrayList<i13> d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public WatchFaceWrapper f;

    @DexIgnore
    public g13(String str, String str2, ArrayList<i13> arrayList, ArrayList<i13> arrayList2, boolean z, WatchFaceWrapper watchFaceWrapper) {
        wd4.b(str, "mPresetId");
        wd4.b(str2, "mPresetName");
        wd4.b(arrayList, "mComplications");
        wd4.b(arrayList2, "mWatchApps");
        this.a = str;
        this.b = str2;
        this.c = arrayList;
        this.d = arrayList2;
        this.e = z;
        this.f = watchFaceWrapper;
    }

    @DexIgnore
    public final ArrayList<i13> a() {
        return this.c;
    }

    @DexIgnore
    public final boolean b() {
        return this.e;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<i13> e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof g13) {
                g13 g13 = (g13) obj;
                if (wd4.a((Object) this.a, (Object) g13.a) && wd4.a((Object) this.b, (Object) g13.b) && wd4.a((Object) this.c, (Object) g13.c) && wd4.a((Object) this.d, (Object) g13.d)) {
                    if (!(this.e == g13.e) || !wd4.a((Object) this.f, (Object) g13.f)) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final WatchFaceWrapper f() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        ArrayList<i13> arrayList = this.c;
        int hashCode3 = (hashCode2 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<i13> arrayList2 = this.d;
        int hashCode4 = (hashCode3 + (arrayList2 != null ? arrayList2.hashCode() : 0)) * 31;
        boolean z = this.e;
        if (z) {
            z = true;
        }
        int i2 = (hashCode4 + (z ? 1 : 0)) * 31;
        WatchFaceWrapper watchFaceWrapper = this.f;
        if (watchFaceWrapper != null) {
            i = watchFaceWrapper.hashCode();
        }
        return i2 + i;
    }

    @DexIgnore
    public String toString() {
        return "DianaPresetConfigWrapper(mPresetId=" + this.a + ", mPresetName=" + this.b + ", mComplications=" + this.c + ", mWatchApps=" + this.d + ", mIsActive=" + this.e + ", mWatchFaceWrapper=" + this.f + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ g13(String str, String str2, ArrayList arrayList, ArrayList arrayList2, boolean z, WatchFaceWrapper watchFaceWrapper, int i, rd4 rd4) {
        this(str, str2, arrayList, arrayList2, z, (i & 32) != 0 ? null : watchFaceWrapper);
    }
}
