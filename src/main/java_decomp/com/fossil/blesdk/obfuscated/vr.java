package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vr {
    @DexIgnore
    public /* final */ xr a;
    @DexIgnore
    public /* final */ a b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Map<Class<?>, C0036a<?>> a; // = new HashMap();

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vr$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.vr$a$a  reason: collision with other inner class name */
        public static class C0036a<Model> {
            @DexIgnore
            public /* final */ List<tr<Model, ?>> a;

            @DexIgnore
            public C0036a(List<tr<Model, ?>> list) {
                this.a = list;
            }
        }

        @DexIgnore
        public void a() {
            this.a.clear();
        }

        @DexIgnore
        public <Model> void a(Class<Model> cls, List<tr<Model, ?>> list) {
            if (this.a.put(cls, new C0036a(list)) != null) {
                throw new IllegalStateException("Already cached loaders for model: " + cls);
            }
        }

        @DexIgnore
        public <Model> List<tr<Model, ?>> a(Class<Model> cls) {
            C0036a aVar = this.a.get(cls);
            if (aVar == null) {
                return null;
            }
            return aVar.a;
        }
    }

    @DexIgnore
    public vr(h8<List<Throwable>> h8Var) {
        this(new xr(h8Var));
    }

    @DexIgnore
    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, ur<? extends Model, ? extends Data> urVar) {
        this.a.a(cls, cls2, urVar);
        this.b.a();
    }

    @DexIgnore
    public final synchronized <A> List<tr<A, ?>> b(Class<A> cls) {
        List<tr<A, ?>> a2;
        a2 = this.b.a(cls);
        if (a2 == null) {
            a2 = Collections.unmodifiableList(this.a.a(cls));
            this.b.a(cls, a2);
        }
        return a2;
    }

    @DexIgnore
    public vr(xr xrVar) {
        this.b = new a();
        this.a = xrVar;
    }

    @DexIgnore
    public <A> List<tr<A, ?>> a(A a2) {
        List b2 = b(b(a2));
        int size = b2.size();
        List<tr<A, ?>> emptyList = Collections.emptyList();
        boolean z = true;
        for (int i = 0; i < size; i++) {
            tr trVar = (tr) b2.get(i);
            if (trVar.a(a2)) {
                if (z) {
                    emptyList = new ArrayList<>(size - i);
                    z = false;
                }
                emptyList.add(trVar);
            }
        }
        return emptyList;
    }

    @DexIgnore
    public static <A> Class<A> b(A a2) {
        return a2.getClass();
    }

    @DexIgnore
    public synchronized List<Class<?>> a(Class<?> cls) {
        return this.a.b(cls);
    }
}
