package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iy0 extends rx0<iy0> implements Cloneable {
    @DexIgnore
    public byte[] g; // = zx0.e;
    @DexIgnore
    public String h; // = "";
    @DexIgnore
    public byte[][] i; // = zx0.d;

    @DexIgnore
    public iy0() {
        this.f = null;
        this.e = -1;
    }

    @DexIgnore
    public final void a(qx0 qx0) throws IOException {
        if (!Arrays.equals(this.g, zx0.e)) {
            qx0.a(1, this.g);
        }
        byte[][] bArr = this.i;
        if (bArr != null && bArr.length > 0) {
            int i2 = 0;
            while (true) {
                byte[][] bArr2 = this.i;
                if (i2 >= bArr2.length) {
                    break;
                }
                byte[] bArr3 = bArr2[i2];
                if (bArr3 != null) {
                    qx0.a(2, bArr3);
                }
                i2++;
            }
        }
        String str = this.h;
        if (str != null && !str.equals("")) {
            qx0.a(4, this.h);
        }
        super.a(qx0);
    }

    @DexIgnore
    public final int b() {
        int b = super.b();
        if (!Arrays.equals(this.g, zx0.e)) {
            b += qx0.b(1, this.g);
        }
        byte[][] bArr = this.i;
        if (bArr != null && bArr.length > 0) {
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            while (true) {
                byte[][] bArr2 = this.i;
                if (i2 >= bArr2.length) {
                    break;
                }
                byte[] bArr3 = bArr2[i2];
                if (bArr3 != null) {
                    i4++;
                    i3 += qx0.b(bArr3);
                }
                i2++;
            }
            b = b + i3 + (i4 * 1);
        }
        String str = this.h;
        return (str == null || str.equals("")) ? b : b + qx0.b(4, this.h);
    }

    @DexIgnore
    public final /* synthetic */ wx0 c() throws CloneNotSupportedException {
        return (iy0) clone();
    }

    @DexIgnore
    public final /* synthetic */ rx0 d() throws CloneNotSupportedException {
        return (iy0) clone();
    }

    @DexIgnore
    /* renamed from: e */
    public final iy0 clone() {
        try {
            iy0 iy0 = (iy0) super.clone();
            byte[][] bArr = this.i;
            if (bArr != null && bArr.length > 0) {
                iy0.i = (byte[][]) bArr.clone();
            }
            return iy0;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof iy0)) {
            return false;
        }
        iy0 iy0 = (iy0) obj;
        if (!Arrays.equals(this.g, iy0.g)) {
            return false;
        }
        String str = this.h;
        if (str == null) {
            if (iy0.h != null) {
                return false;
            }
        } else if (!str.equals(iy0.h)) {
            return false;
        }
        if (!vx0.a(this.i, iy0.i)) {
            return false;
        }
        tx0 tx0 = this.f;
        if (tx0 != null && !tx0.a()) {
            return this.f.equals(iy0.f);
        }
        tx0 tx02 = iy0.f;
        return tx02 == null || tx02.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (((iy0.class.getName().hashCode() + 527) * 31) + Arrays.hashCode(this.g)) * 31;
        String str = this.h;
        int i2 = 0;
        int hashCode2 = (((((hashCode + (str == null ? 0 : str.hashCode())) * 31) + vx0.a(this.i)) * 31) + 1237) * 31;
        tx0 tx0 = this.f;
        if (tx0 != null && !tx0.a()) {
            i2 = this.f.hashCode();
        }
        return hashCode2 + i2;
    }
}
