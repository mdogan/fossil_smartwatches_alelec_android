package com.fossil.blesdk.obfuscated;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lw3 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ int[] d; // = new int[10];

    @DexIgnore
    public void a() {
        this.c = 0;
        this.b = 0;
        this.a = 0;
        Arrays.fill(this.d, 0);
    }

    @DexIgnore
    public int b(int i) {
        return this.d[i];
    }

    @DexIgnore
    public int c() {
        return Integer.bitCount(this.a);
    }

    @DexIgnore
    public int d(int i) {
        return (this.a & 32) != 0 ? this.d[5] : i;
    }

    @DexIgnore
    public boolean e(int i) {
        return ((1 << i) & this.c) != 0;
    }

    @DexIgnore
    public boolean f(int i) {
        return ((1 << i) & this.a) != 0;
    }

    @DexIgnore
    public boolean g(int i) {
        return ((1 << i) & this.b) != 0;
    }

    @DexIgnore
    public int b() {
        if ((this.a & 2) != 0) {
            return this.d[1];
        }
        return -1;
    }

    @DexIgnore
    public int c(int i) {
        return (this.a & 128) != 0 ? this.d[7] : i;
    }

    @DexIgnore
    public lw3 a(int i, int i2, int i3) {
        if (i >= this.d.length) {
            return this;
        }
        int i4 = 1 << i;
        this.a |= i4;
        if ((i2 & 1) != 0) {
            this.b |= i4;
        } else {
            this.b &= ~i4;
        }
        if ((i2 & 2) != 0) {
            this.c |= i4;
        } else {
            this.c &= ~i4;
        }
        this.d[i] = i3;
        return this;
    }

    @DexIgnore
    public int a(int i) {
        int i2 = e(i) ? 2 : 0;
        return g(i) ? i2 | 1 : i2;
    }

    @DexIgnore
    public void a(lw3 lw3) {
        for (int i = 0; i < 10; i++) {
            if (lw3.f(i)) {
                a(i, lw3.a(i), lw3.b(i));
            }
        }
    }
}
