package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class xd2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleEditText r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ AlphabetFastScrollRecyclerView u;

    @DexIgnore
    public xd2(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, ImageView imageView, ImageView imageView2, AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleEditText;
        this.s = imageView;
        this.t = imageView2;
        this.u = alphabetFastScrollRecyclerView;
    }
}
