package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.enums.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ps3<T> {
    @DexIgnore
    public static /* final */ a e; // = new a((rd4) null);
    @DexIgnore
    public /* final */ Status a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ Integer c;
    @DexIgnore
    public /* final */ String d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final <T> ps3<T> a(int i, String str, T t) {
            wd4.b(str, "msg");
            return new ps3<>(Status.ERROR, t, Integer.valueOf(i), str);
        }

        @DexIgnore
        public final <T> ps3<T> b(T t) {
            return new ps3<>(Status.NETWORK_LOADING, t, (Integer) null, (String) null);
        }

        @DexIgnore
        public final <T> ps3<T> c(T t) {
            return new ps3<>(Status.SUCCESS, t, (Integer) null, (String) null);
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final <T> ps3<T> a(T t) {
            return new ps3<>(Status.DATABASE_LOADING, t, (Integer) null, (String) null);
        }
    }

    @DexIgnore
    public ps3(Status status, T t, Integer num, String str) {
        wd4.b(status, "status");
        this.a = status;
        this.b = t;
        this.c = num;
        this.d = str;
    }

    @DexIgnore
    public final Status a() {
        return this.a;
    }

    @DexIgnore
    public final T b() {
        return this.b;
    }

    @DexIgnore
    public final Integer c() {
        return this.c;
    }

    @DexIgnore
    public final T d() {
        return this.b;
    }

    @DexIgnore
    public final String e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ps3)) {
            return false;
        }
        ps3 ps3 = (ps3) obj;
        return wd4.a((Object) this.a, (Object) ps3.a) && wd4.a((Object) this.b, (Object) ps3.b) && wd4.a((Object) this.c, (Object) ps3.c) && wd4.a((Object) this.d, (Object) ps3.d);
    }

    @DexIgnore
    public final Status f() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        Status status = this.a;
        int i = 0;
        int hashCode = (status != null ? status.hashCode() : 0) * 31;
        T t = this.b;
        int hashCode2 = (hashCode + (t != null ? t.hashCode() : 0)) * 31;
        Integer num = this.c;
        int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
        String str = this.d;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "Resource(status=" + this.a + ", data=" + this.b + ", code=" + this.c + ", message=" + this.d + ")";
    }
}
