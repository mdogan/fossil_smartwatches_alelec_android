package com.fossil.blesdk.obfuscated;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fw0 {
    @DexIgnore
    public static /* final */ fw0 c; // = new fw0();
    @DexIgnore
    public /* final */ lw0 a;
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, kw0<?>> b; // = new ConcurrentHashMap();

    @DexIgnore
    public fw0() {
        String[] strArr = {"com.google.protobuf.AndroidProto3SchemaFactory"};
        lw0 lw0 = null;
        for (int i = 0; i <= 0; i++) {
            lw0 = a(strArr[0]);
            if (lw0 != null) {
                break;
            }
        }
        this.a = lw0 == null ? new kv0() : lw0;
    }

    @DexIgnore
    public static fw0 a() {
        return c;
    }

    @DexIgnore
    public static lw0 a(String str) {
        try {
            return (lw0) Class.forName(str).getConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public final <T> kw0<T> a(Class<T> cls) {
        uu0.a(cls, "messageType");
        kw0<T> kw0 = (kw0) this.b.get(cls);
        if (kw0 != null) {
            return kw0;
        }
        kw0<T> a2 = this.a.a(cls);
        uu0.a(cls, "messageType");
        uu0.a(a2, "schema");
        kw0<T> putIfAbsent = this.b.putIfAbsent(cls, a2);
        return putIfAbsent != null ? putIfAbsent : a2;
    }

    @DexIgnore
    public final <T> kw0<T> a(T t) {
        return a(t.getClass());
    }
}
