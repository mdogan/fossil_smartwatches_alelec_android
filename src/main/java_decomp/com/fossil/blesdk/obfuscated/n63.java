package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.o13;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n63 extends bs2 implements m63, CustomizeWidget.c, xs3.g, View.OnClickListener {
    @DexIgnore
    public l63 k;
    @DexIgnore
    public ur3<jd2> l;
    @DexIgnore
    public /* final */ ArrayList<Fragment> m; // = new ArrayList<>();
    @DexIgnore
    public h63 n;
    @DexIgnore
    public MicroAppPresenter o;
    @DexIgnore
    public k42 p;
    @DexIgnore
    public HybridCustomizeViewModel q;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ n63 a;

        @DexIgnore
        public b(n63 n63) {
            this.a = n63;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionCancel()");
            if (transition != null) {
                transition.removeListener(this);
            }
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionEnd()");
            if (transition != null) {
                transition.removeListener(this);
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null && !activity.hasWindowFocus()) {
                wd4.a((Object) activity, "it");
                String stringExtra = activity.getIntent().getStringExtra("KEY_PRESET_ID");
                String stringExtra2 = activity.getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
                activity.finishAfterTransition();
                HybridCustomizeEditActivity.a aVar = HybridCustomizeEditActivity.E;
                wd4.a((Object) stringExtra, "presetId");
                wd4.a((Object) stringExtra2, "microAppPos");
                aVar.a(activity, stringExtra, stringExtra2);
            }
            jd2 a2 = this.a.T0().a();
            if (a2 != null) {
                TextView textView = a2.w;
                wd4.a((Object) textView, "binding.tvPresetName");
                textView.setEllipsize(TextUtils.TruncateAt.END);
                a2.w.setSingleLine(true);
                TextView textView2 = a2.w;
                wd4.a((Object) textView2, "binding.tvPresetName");
                textView2.setMaxLines(1);
            }
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionPause()");
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionResume()");
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionStart()");
            jd2 a2 = this.a.T0().a();
            if (a2 != null) {
                bq2 bq2 = bq2.a;
                jd2 a3 = this.a.T0().a();
                if (a3 != null) {
                    CardView cardView = a3.r;
                    wd4.a((Object) cardView, "mBinding.get()!!.cvGroup");
                    bq2.a((View) cardView);
                    ViewPropertyAnimator animate = a2.z.animate();
                    if (animate != null) {
                        ViewPropertyAnimator duration = animate.setDuration(500);
                        if (duration != null) {
                            duration.alpha(1.0f);
                        }
                    }
                    ViewPropertyAnimator animate2 = a2.y.animate();
                    if (animate2 != null) {
                        ViewPropertyAnimator duration2 = animate2.setDuration(500);
                        if (duration2 != null) {
                            duration2.alpha(1.0f);
                        }
                    }
                    ViewPropertyAnimator animate3 = a2.x.animate();
                    if (animate3 != null) {
                        ViewPropertyAnimator duration3 = animate3.setDuration(500);
                        if (duration3 != null) {
                            duration3.alpha(1.0f);
                            return;
                        }
                        return;
                    }
                    return;
                }
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerView.p {
        @DexIgnore
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            wd4.b(recyclerView, "p0");
            wd4.b(motionEvent, "p1");
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public boolean b(RecyclerView recyclerView, MotionEvent motionEvent) {
            wd4.b(recyclerView, "p0");
            wd4.b(motionEvent, "p1");
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements o13.b {
        @DexIgnore
        public /* final */ /* synthetic */ n63 a;

        @DexIgnore
        public d(n63 n63) {
            this.a = n63;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wd4.b(view, "view");
            wd4.b(str, "id");
            return this.a.a(ViewHierarchy.DIMENSION_TOP_KEY, view, str);
        }

        @DexIgnore
        public void b(String str) {
            wd4.b(str, "label");
            this.a.e(ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        public boolean c(String str) {
            wd4.b(str, "fromPos");
            this.a.g(str, ViewHierarchy.DIMENSION_TOP_KEY);
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, "label");
            this.a.f(ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        public void a() {
            this.a.U(ViewHierarchy.DIMENSION_TOP_KEY);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements o13.b {
        @DexIgnore
        public /* final */ /* synthetic */ n63 a;

        @DexIgnore
        public e(n63 n63) {
            this.a = n63;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wd4.b(view, "view");
            wd4.b(str, "id");
            return this.a.a("middle", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wd4.b(str, "label");
            this.a.e("middle", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wd4.b(str, "fromPos");
            this.a.g(str, "middle");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, "label");
            this.a.f("middle", str);
        }

        @DexIgnore
        public void a() {
            this.a.U("middle");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements o13.b {
        @DexIgnore
        public /* final */ /* synthetic */ n63 a;

        @DexIgnore
        public f(n63 n63) {
            this.a = n63;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wd4.b(view, "view");
            wd4.b(str, "id");
            return this.a.a("bottom", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wd4.b(str, "label");
            this.a.e("bottom", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wd4.b(str, "fromPos");
            this.a.g(str, "bottom");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, "label");
            this.a.f("bottom", str);
        }

        @DexIgnore
        public void a() {
            this.a.U("bottom");
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void D() {
        if (isActive()) {
            ur3<jd2> ur3 = this.l;
            if (ur3 != null) {
                jd2 a2 = ur3.a();
                if (a2 != null) {
                    ImageView imageView = a2.t;
                    if (imageView != null) {
                        imageView.setImageResource(R.drawable.hybrid_default_watch_background);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "DianaCustomizeEditFragment";
    }

    @DexIgnore
    public boolean S0() {
        l63 l63 = this.k;
        if (l63 != null) {
            l63.h();
            return false;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final ur3<jd2> T0() {
        ur3<jd2> ur3 = this.l;
        if (ur3 != null) {
            return ur3;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void U(String str) {
        wd4.b(str, "position");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "cancelDrag - position=" + str);
        ur3<jd2> ur3 = this.l;
        if (ur3 != null) {
            jd2 a2 = ur3.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.C.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.B.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.A.e();
                }
                a2.C.setDragMode(false);
                a2.B.setDragMode(false);
                a2.A.setDragMode(false);
                h63 h63 = this.n;
                if (h63 != null) {
                    h63.T0();
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void U0() {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "Inside .showNoActiveDeviceFlow");
        this.n = (h63) getChildFragmentManager().a("MicroAppsFragment");
        if (this.n == null) {
            this.n = new h63();
        }
        h63 h63 = this.n;
        if (h63 != null) {
            this.m.add(h63);
        }
        m42 g = PortfolioApp.W.c().g();
        h63 h632 = this.n;
        if (h632 != null) {
            g.a(new e63(h632)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppContract.View");
    }

    @DexIgnore
    public final void V(String str) {
        ur3<jd2> ur3 = this.l;
        if (ur3 != null) {
            jd2 a2 = ur3.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.C.setSelectedWc(true);
                            a2.B.setSelectedWc(false);
                            a2.A.setSelectedWc(false);
                        }
                    } else if (str.equals("middle")) {
                        a2.C.setSelectedWc(false);
                        a2.B.setSelectedWc(true);
                        a2.A.setSelectedWc(false);
                    }
                } else if (str.equals("bottom")) {
                    a2.C.setSelectedWc(false);
                    a2.B.setSelectedWc(false);
                    a2.A.setSelectedWc(true);
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void V0() {
        ur3<jd2> ur3 = this.l;
        if (ur3 != null) {
            jd2 a2 = ur3.a();
            if (a2 != null) {
                CustomizeWidget customizeWidget = a2.C;
                Intent putExtra = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_TOP_KEY);
                wd4.a((Object) putExtra, "Intent().putExtra(Custom\u2026, WatchAppPos.TOP_BUTTON)");
                CustomizeWidget.a(customizeWidget, "SWAP_PRESET_WATCH_APP", putExtra, new o13(new d(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget2 = a2.B;
                Intent putExtra2 = new Intent().putExtra("KEY_POSITION", "middle");
                wd4.a((Object) putExtra2, "Intent().putExtra(Custom\u2026atchAppPos.MIDDLE_BUTTON)");
                CustomizeWidget.a(customizeWidget2, "SWAP_PRESET_WATCH_APP", putExtra2, new o13(new e(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget3 = a2.A;
                Intent putExtra3 = new Intent().putExtra("KEY_POSITION", "bottom");
                wd4.a((Object) putExtra3, "Intent().putExtra(Custom\u2026atchAppPos.BOTTOM_BUTTON)");
                CustomizeWidget.a(customizeWidget3, "SWAP_PRESET_WATCH_APP", putExtra3, new o13(new f(this)), (CustomizeWidget.b) null, 8, (Object) null);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(String str, String str2, String str3) {
        wd4.b(str, "message");
        wd4.b(str2, "microAppId");
        wd4.b(str3, "microAppPos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str2);
            bundle.putString("TO_POS", str3);
            xs3.f fVar = new xs3.f(R.layout.dialog_confirmation_one_action);
            fVar.a((int) R.id.tv_description, str);
            fVar.a((int) R.id.tv_ok, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
            fVar.a((int) R.id.tv_ok);
            fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_SETTING", bundle);
        }
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public final void e(String str, String str2) {
        wd4.b(str, "position");
        wd4.b(str2, "label");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragEnter - position=" + str + ", label=" + str2);
        ur3<jd2> ur3 = this.l;
        if (ur3 != null) {
            jd2 a2 = ur3.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.C.d();
                        }
                    } else if (str.equals("middle")) {
                        a2.B.d();
                    }
                } else if (str.equals("bottom")) {
                    a2.A.d();
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void f(String str, String str2) {
        wd4.b(str, "position");
        wd4.b(str2, "label");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragExit - position=" + str + ", label=" + str2);
        ur3<jd2> ur3 = this.l;
        if (ur3 != null) {
            jd2 a2 = ur3.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.C.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.B.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.A.e();
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void g(String str, String str2) {
        wd4.b(str, "fromPosition");
        wd4.b(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "swapControl - fromPosition=" + str + ", toPosition=" + str2);
        ur3<jd2> ur3 = this.l;
        if (ur3 != null) {
            jd2 a2 = ur3.a();
            if (a2 != null) {
                int hashCode = str2.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.C.e();
                        }
                    } else if (str2.equals("middle")) {
                        a2.B.e();
                    }
                } else if (str2.equals("bottom")) {
                    a2.A.e();
                }
                a2.C.setDragMode(false);
                a2.B.setDragMode(false);
                a2.A.setDragMode(false);
                l63 l63 = this.k;
                if (l63 != null) {
                    l63.b(str, str2);
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void l() {
        String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SetComplication_SettingComplication_Text__ApplyingToWatch);
        wd4.a((Object) a2, "LanguageHelper.getString\u2026on_Text__ApplyingToWatch)");
        S(a2);
    }

    @DexIgnore
    public void m() {
        a();
    }

    @DexIgnore
    public void n(String str) {
        wd4.b(str, "buttonsPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "updateSelectedWatchApp position=" + str);
        V(str);
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HybridCustomizeEditActivity hybridCustomizeEditActivity = (HybridCustomizeEditActivity) activity;
            k42 k42 = this.p;
            if (k42 != null) {
                jc a2 = mc.a((FragmentActivity) hybridCustomizeEditActivity, (lc.b) k42).a(HybridCustomizeViewModel.class);
                wd4.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.q = (HybridCustomizeViewModel) a2;
                l63 l63 = this.k;
                if (l63 != null) {
                    HybridCustomizeViewModel hybridCustomizeViewModel = this.q;
                    if (hybridCustomizeViewModel != null) {
                        l63.a(hybridCustomizeViewModel);
                    } else {
                        wd4.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else {
                wd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == R.id.ftv_set_to_watch) {
                l63 l63 = this.k;
                if (l63 != null) {
                    l63.i();
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else if (id != R.id.tv_cancel) {
                switch (id) {
                    case R.id.wa_bottom /*2131363029*/:
                        l63 l632 = this.k;
                        if (l632 != null) {
                            l632.a("bottom");
                            return;
                        } else {
                            wd4.d("mPresenter");
                            throw null;
                        }
                    case R.id.wa_middle /*2131363030*/:
                        l63 l633 = this.k;
                        if (l633 != null) {
                            l633.a("middle");
                            return;
                        } else {
                            wd4.d("mPresenter");
                            throw null;
                        }
                    case R.id.wa_top /*2131363031*/:
                        l63 l634 = this.k;
                        if (l634 != null) {
                            l634.a(ViewHierarchy.DIMENSION_TOP_KEY);
                            return;
                        } else {
                            wd4.d("mPresenter");
                            throw null;
                        }
                    default:
                        return;
                }
            } else {
                l63 l635 = this.k;
                if (l635 != null) {
                    l635.h();
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        jd2 jd2 = (jd2) ra.a(layoutInflater, R.layout.fragment_hybrid_customize_edit, viewGroup, false, O0());
        U0();
        this.l = new ur3<>(this, jd2);
        wd4.a((Object) jd2, "binding");
        return jd2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        l63 l63 = this.k;
        if (l63 != null) {
            l63.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        l63 l63 = this.k;
        if (l63 != null) {
            l63.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        R("set_watch_apps_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wd4.a((Object) activity, Constants.ACTIVITY);
            Window window = activity.getWindow();
            wd4.a((Object) window, "activity.window");
            window.setEnterTransition(bq2.a.a());
            Window window2 = activity.getWindow();
            wd4.a((Object) window2, "activity.window");
            window2.setSharedElementEnterTransition(bq2.a.a(PortfolioApp.W.c()));
            Intent intent = activity.getIntent();
            wd4.a((Object) intent, "activity.intent");
            activity.setEnterSharedElementCallback(new aq2(intent, PortfolioApp.W.c()));
            a(activity);
        }
        ur3<jd2> ur3 = this.l;
        if (ur3 != null) {
            jd2 a2 = ur3.a();
            if (a2 != null) {
                a2.C.setOnClickListener(this);
                a2.B.setOnClickListener(this);
                a2.A.setOnClickListener(this);
                a2.v.setOnClickListener(this);
                a2.s.setOnClickListener(this);
                RecyclerViewPager recyclerViewPager = a2.u;
                wd4.a((Object) recyclerViewPager, "it.rvPreset");
                recyclerViewPager.setAdapter(new du3(getChildFragmentManager(), this.m));
                a2.u.setItemViewCacheSize(2);
                a2.u.a((RecyclerView.p) new c());
            }
            V0();
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void p() {
        xs3.f fVar = new xs3.f(R.layout.dialog_confirmation_two_action);
        fVar.a((int) R.id.tv_description, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_UnsavedChanges_Text__SaveChangesToThePresetBefore));
        fVar.a((int) R.id.tv_cancel, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_UnsavedChanges_CTA__Discard));
        fVar.a((int) R.id.tv_ok, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_UnsavedChanges_CTA__Save));
        fVar.a((int) R.id.tv_cancel);
        fVar.a((int) R.id.tv_ok);
        fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH");
    }

    @DexIgnore
    public void q() {
        xs3.f fVar = new xs3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_description, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Connectivity_Error_Text__ThereWasAProblemProcessingThat));
        fVar.a((int) R.id.tv_ok, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Connectivity_Error_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(getChildFragmentManager(), "");
    }

    @DexIgnore
    public final Transition a(FragmentActivity fragmentActivity) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener()");
        Window window = fragmentActivity.getWindow();
        wd4.a((Object) window, "it.window");
        return window.getSharedElementEnterTransition().addListener(new b(this));
    }

    @DexIgnore
    public final boolean a(String str, View view, String str2) {
        wd4.b(str, "position");
        wd4.b(view, "view");
        wd4.b(str2, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dropControl - pos=" + str + ", view=" + view.getId() + ", id=" + str2);
        ur3<jd2> ur3 = this.l;
        if (ur3 != null) {
            jd2 a2 = ur3.a();
            if (a2 == null) {
                return true;
            }
            int hashCode = str.hashCode();
            if (hashCode != -1383228885) {
                if (hashCode != -1074341483) {
                    if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        a2.C.e();
                    }
                } else if (str.equals("middle")) {
                    a2.B.e();
                }
            } else if (str.equals("bottom")) {
                a2.A.e();
            }
            a2.C.setDragMode(false);
            a2.B.setDragMode(false);
            a2.A.setDragMode(false);
            h63 h63 = this.n;
            if (h63 != null) {
                h63.T0();
            }
            l63 l63 = this.k;
            if (l63 != null) {
                l63.a(str2, str);
                return true;
            }
            wd4.d("mPresenter");
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(String str, String str2) {
        wd4.b(str, "microAppId");
        wd4.b(str2, "microAppPos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str);
            bundle.putString("TO_POS", str2);
            xs3.f fVar = new xs3.f(R.layout.dialog_confirmation_one_action);
            be4 be4 = be4.a;
            String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.set_to_watch_fail_app_permission);
            wd4.a((Object) a2, "LanguageHelper.getString\u2026atch_fail_app_permission)");
            Object[] objArr = {str, str2};
            String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            fVar.a((int) R.id.tv_description, format);
            fVar.a((int) R.id.tv_ok, tm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
            fVar.a((int) R.id.tv_ok);
            fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_PERMISSION", bundle);
        }
    }

    @DexIgnore
    public void f(boolean z) {
        ur3<jd2> ur3 = this.l;
        if (ur3 != null) {
            jd2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageButton imageButton = a2.s;
                wd4.a((Object) imageButton, "it.ftvSetToWatch");
                imageButton.setEnabled(true);
                ImageButton imageButton2 = a2.s;
                wd4.a((Object) imageButton2, "it.ftvSetToWatch");
                imageButton2.setClickable(true);
                a2.s.setBackgroundResource(R.drawable.preset_saved);
                return;
            }
            ImageButton imageButton3 = a2.s;
            wd4.a((Object) imageButton3, "it.ftvSetToWatch");
            imageButton3.setClickable(false);
            ImageButton imageButton4 = a2.s;
            wd4.a((Object) imageButton4, "it.ftvSetToWatch");
            imageButton4.setEnabled(false);
            a2.s.setBackgroundResource(R.drawable.preset_unsaved);
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void g(boolean z) {
        ur3<jd2> ur3 = this.l;
        if (ur3 != null) {
            jd2 a2 = ur3.a();
            if (a2 != null) {
                TextView textView = a2.w;
                if (textView != null) {
                    textView.setSingleLine(false);
                }
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                if (z) {
                    activity.setResult(-1);
                } else {
                    activity.setResult(0);
                }
                activity.supportFinishAfterTransition();
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(h13 h13) {
        wd4.b(h13, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "showCurrentPreset  microApps=" + h13.a());
        ur3<jd2> ur3 = this.l;
        if (ur3 != null) {
            jd2 a2 = ur3.a();
            if (a2 != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(h13.a());
                TextView textView = a2.w;
                wd4.a((Object) textView, "it.tvPresetName");
                textView.setText(h13.c());
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    i13 i13 = (i13) it.next();
                    String a3 = i13.a();
                    String b2 = i13.b();
                    String c2 = i13.c();
                    if (c2 != null) {
                        int hashCode = c2.hashCode();
                        if (hashCode != -1383228885) {
                            if (hashCode != -1074341483) {
                                if (hashCode == 115029 && c2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                    FlexibleTextView flexibleTextView = a2.z;
                                    wd4.a((Object) flexibleTextView, "it.tvWaTop");
                                    flexibleTextView.setText(b2);
                                    a2.C.c(a3);
                                    a2.C.h();
                                    CustomizeWidget customizeWidget = a2.C;
                                    wd4.a((Object) customizeWidget, "it.waTop");
                                    a(customizeWidget, a3);
                                }
                            } else if (c2.equals("middle")) {
                                FlexibleTextView flexibleTextView2 = a2.y;
                                wd4.a((Object) flexibleTextView2, "it.tvWaMiddle");
                                flexibleTextView2.setText(b2);
                                a2.B.c(a3);
                                a2.B.h();
                                CustomizeWidget customizeWidget2 = a2.B;
                                wd4.a((Object) customizeWidget2, "it.waMiddle");
                                a(customizeWidget2, a3);
                            }
                        } else if (c2.equals("bottom")) {
                            FlexibleTextView flexibleTextView3 = a2.x;
                            wd4.a((Object) flexibleTextView3, "it.tvWaBottom");
                            flexibleTextView3.setText(b2);
                            a2.A.c(a3);
                            a2.A.h();
                            CustomizeWidget customizeWidget3 = a2.A;
                            wd4.a((Object) customizeWidget3, "it.waBottom");
                            a(customizeWidget3, a3);
                        }
                    }
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public void a(l63 l63) {
        wd4.b(l63, "presenter");
        this.k = l63;
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1395717072) {
            if (hashCode != -523101473) {
                if (hashCode != 291193711 || !str.equals("DIALOG_SET_TO_WATCH_FAIL_SETTING")) {
                    return;
                }
            } else if (!str.equals("DIALOG_SET_TO_WATCH")) {
                return;
            } else {
                if (i == R.id.tv_cancel) {
                    g(false);
                    return;
                } else if (i == R.id.tv_ok) {
                    l63 l63 = this.k;
                    if (l63 != null) {
                        l63.i();
                        return;
                    } else {
                        wd4.d("mPresenter");
                        throw null;
                    }
                } else {
                    return;
                }
            }
        } else if (!str.equals("DIALOG_SET_TO_WATCH_FAIL_PERMISSION")) {
            return;
        }
        if (i == R.id.tv_ok && intent != null) {
            String stringExtra = intent.getStringExtra("TO_POS");
            String stringExtra2 = intent.getStringExtra("TO_ID");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "onUserConfirmToSetUpSetting " + stringExtra2 + " of " + stringExtra);
            if (wd4.a((Object) stringExtra2, (Object) MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
                cn2 cn2 = cn2.d;
                Context context = getContext();
                if (context != null) {
                    cn2.b(context, stringExtra2);
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                l63 l632 = this.k;
                if (l632 != null) {
                    wd4.a((Object) stringExtra, "toPos");
                    l632.a(stringExtra);
                    return;
                }
                wd4.d("mPresenter");
                throw null;
            }
        }
    }
}
