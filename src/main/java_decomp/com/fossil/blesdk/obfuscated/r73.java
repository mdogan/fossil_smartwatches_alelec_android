package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r73 implements Factory<aa3> {
    @DexIgnore
    public static aa3 a(o73 o73) {
        aa3 c = o73.c();
        o44.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
