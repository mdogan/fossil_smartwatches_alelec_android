package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kg4 {
    @DexIgnore
    public static final <T> sh4<T> a(lh4 lh4, CoroutineContext coroutineContext, CoroutineStart coroutineStart, kd4<? super lh4, ? super kc4<? super T>, ? extends Object> kd4) {
        return mg4.a(lh4, coroutineContext, coroutineStart, kd4);
    }

    @DexIgnore
    public static final ri4 b(lh4 lh4, CoroutineContext coroutineContext, CoroutineStart coroutineStart, kd4<? super lh4, ? super kc4<? super cb4>, ? extends Object> kd4) {
        return mg4.b(lh4, coroutineContext, coroutineStart, kd4);
    }

    @DexIgnore
    public static final <T> T a(CoroutineContext coroutineContext, kd4<? super lh4, ? super kc4<? super T>, ? extends Object> kd4) throws InterruptedException {
        return lg4.a(coroutineContext, kd4);
    }

    @DexIgnore
    public static final <T> Object a(CoroutineContext coroutineContext, kd4<? super lh4, ? super kc4<? super T>, ? extends Object> kd4, kc4<? super T> kc4) {
        return mg4.a(coroutineContext, kd4, kc4);
    }
}
