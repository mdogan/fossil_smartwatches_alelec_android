package com.fossil.blesdk.obfuscated;

import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xu {
    @DexIgnore
    public /* final */ Set<pv> a; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public /* final */ List<pv> b; // = new ArrayList();
    @DexIgnore
    public boolean c;

    @DexIgnore
    public boolean a(pv pvVar) {
        boolean z = true;
        if (pvVar == null) {
            return true;
        }
        boolean remove = this.a.remove(pvVar);
        if (!this.b.remove(pvVar) && !remove) {
            z = false;
        }
        if (z) {
            pvVar.clear();
        }
        return z;
    }

    @DexIgnore
    public void b(pv pvVar) {
        this.a.add(pvVar);
        if (!this.c) {
            pvVar.c();
            return;
        }
        pvVar.clear();
        if (Log.isLoggable("RequestTracker", 2)) {
            Log.v("RequestTracker", "Paused, delaying request");
        }
        this.b.add(pvVar);
    }

    @DexIgnore
    public void c() {
        this.c = true;
        for (T t : vw.a(this.a)) {
            if (t.isRunning()) {
                t.d();
                this.b.add(t);
            }
        }
    }

    @DexIgnore
    public void d() {
        for (T t : vw.a(this.a)) {
            if (!t.f() && !t.e()) {
                t.clear();
                if (!this.c) {
                    t.c();
                } else {
                    this.b.add(t);
                }
            }
        }
    }

    @DexIgnore
    public void e() {
        this.c = false;
        for (T t : vw.a(this.a)) {
            if (!t.f() && !t.isRunning()) {
                t.c();
            }
        }
        this.b.clear();
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "{numRequests=" + this.a.size() + ", isPaused=" + this.c + "}";
    }

    @DexIgnore
    public void a() {
        for (T a2 : vw.a(this.a)) {
            a(a2);
        }
        this.b.clear();
    }

    @DexIgnore
    public void b() {
        this.c = true;
        for (T t : vw.a(this.a)) {
            if (t.isRunning() || t.f()) {
                t.clear();
                this.b.add(t);
            }
        }
    }
}
