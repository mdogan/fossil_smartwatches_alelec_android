package com.fossil.blesdk.obfuscated;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.text.TextUtils;
import androidx.work.WorkInfo;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wj implements qj, ik, nj {
    @DexIgnore
    public static /* final */ String l; // = ej.a("GreedyScheduler");
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ uj f;
    @DexIgnore
    public /* final */ jk g;
    @DexIgnore
    public List<il> h; // = new ArrayList();
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ Object j;
    @DexIgnore
    public Boolean k;

    @DexIgnore
    public wj(Context context, am amVar, uj ujVar) {
        this.e = context;
        this.f = ujVar;
        this.g = new jk(context, amVar, this);
        this.j = new Object();
    }

    @DexIgnore
    public void a(il... ilVarArr) {
        if (this.k == null) {
            this.k = Boolean.valueOf(TextUtils.equals(this.e.getPackageName(), a()));
        }
        if (!this.k.booleanValue()) {
            ej.a().c(l, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        b();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (il ilVar : ilVarArr) {
            if (ilVar.b == WorkInfo.State.ENQUEUED && !ilVar.d() && ilVar.g == 0 && !ilVar.c()) {
                if (!ilVar.b()) {
                    ej.a().a(l, String.format("Starting work for %s", new Object[]{ilVar.a}), new Throwable[0]);
                    this.f.a(ilVar.a);
                } else if (Build.VERSION.SDK_INT >= 23 && ilVar.j.h()) {
                    ej.a().a(l, String.format("Ignoring WorkSpec %s, Requires device idle.", new Object[]{ilVar}), new Throwable[0]);
                } else if (Build.VERSION.SDK_INT < 24 || !ilVar.j.e()) {
                    arrayList.add(ilVar);
                    arrayList2.add(ilVar.a);
                } else {
                    ej.a().a(l, String.format("Ignoring WorkSpec %s, Requires ContentUri triggers.", new Object[]{ilVar}), new Throwable[0]);
                }
            }
        }
        synchronized (this.j) {
            if (!arrayList.isEmpty()) {
                ej.a().a(l, String.format("Starting tracking for [%s]", new Object[]{TextUtils.join(",", arrayList2)}), new Throwable[0]);
                this.h.addAll(arrayList);
                this.g.c(this.h);
            }
        }
    }

    @DexIgnore
    public void b(List<String> list) {
        for (String next : list) {
            ej.a().a(l, String.format("Constraints met: Scheduling work ID %s", new Object[]{next}), new Throwable[0]);
            this.f.a(next);
        }
    }

    @DexIgnore
    public final void b(String str) {
        synchronized (this.j) {
            int size = this.h.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    break;
                } else if (this.h.get(i2).a.equals(str)) {
                    ej.a().a(l, String.format("Stopping tracking for %s", new Object[]{str}), new Throwable[0]);
                    this.h.remove(i2);
                    this.g.c(this.h);
                    break;
                } else {
                    i2++;
                }
            }
        }
    }

    @DexIgnore
    public final void b() {
        if (!this.i) {
            this.f.e().a((nj) this);
            this.i = true;
        }
    }

    @DexIgnore
    public void a(String str) {
        if (this.k == null) {
            this.k = Boolean.valueOf(TextUtils.equals(this.e.getPackageName(), a()));
        }
        if (!this.k.booleanValue()) {
            ej.a().c(l, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        b();
        ej.a().a(l, String.format("Cancelling work ID %s", new Object[]{str}), new Throwable[0]);
        this.f.b(str);
    }

    @DexIgnore
    public void a(List<String> list) {
        for (String next : list) {
            ej.a().a(l, String.format("Constraints not met: Cancelling work ID %s", new Object[]{next}), new Throwable[0]);
            this.f.b(next);
        }
    }

    @DexIgnore
    public void a(String str, boolean z) {
        b(str);
    }

    @DexIgnore
    public final String a() {
        int myPid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) this.e.getSystemService(Constants.ACTIVITY);
        if (activityManager == null) {
            return null;
        }
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        if (runningAppProcesses == null || runningAppProcesses.isEmpty()) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.pid == myPid) {
                return next.processName;
            }
        }
        return null;
    }
}
