package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h54 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ h74 b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends m54 {
        @DexIgnore
        public /* final */ /* synthetic */ g54 e;

        @DexIgnore
        public a(g54 g54) {
            this.e = g54;
        }

        @DexIgnore
        public void a() {
            g54 a = h54.this.b();
            if (!this.e.equals(a)) {
                r44.g().d("Fabric", "Asychronously getting Advertising Info and storing it to preferences");
                h54.this.c(a);
            }
        }
    }

    @DexIgnore
    public h54(Context context) {
        this.a = context.getApplicationContext();
        this.b = new i74(context, "TwitterAdvertisingInfoPreferences");
    }

    @DexIgnore
    public final void b(g54 g54) {
        new Thread(new a(g54)).start();
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public final void c(g54 g54) {
        if (a(g54)) {
            h74 h74 = this.b;
            h74.a(h74.edit().putString("advertising_id", g54.a).putBoolean("limit_ad_tracking_enabled", g54.b));
            return;
        }
        h74 h742 = this.b;
        h742.a(h742.edit().remove("advertising_id").remove("limit_ad_tracking_enabled"));
    }

    @DexIgnore
    public k54 d() {
        return new i54(this.a);
    }

    @DexIgnore
    public k54 e() {
        return new j54(this.a);
    }

    @DexIgnore
    public g54 a() {
        g54 c = c();
        if (a(c)) {
            r44.g().d("Fabric", "Using AdvertisingInfo from Preference Store");
            b(c);
            return c;
        }
        g54 b2 = b();
        c(b2);
        return b2;
    }

    @DexIgnore
    public final g54 b() {
        g54 a2 = d().a();
        if (!a(a2)) {
            a2 = e().a();
            if (!a(a2)) {
                r44.g().d("Fabric", "AdvertisingInfo not present");
            } else {
                r44.g().d("Fabric", "Using AdvertisingInfo from Service Provider");
            }
        } else {
            r44.g().d("Fabric", "Using AdvertisingInfo from Reflection Provider");
        }
        return a2;
    }

    @DexIgnore
    public final boolean a(g54 g54) {
        return g54 != null && !TextUtils.isEmpty(g54.a);
    }

    @DexIgnore
    public g54 c() {
        return new g54(this.b.get().getString("advertising_id", ""), this.b.get().getBoolean("limit_ad_tracking_enabled", false));
    }
}
