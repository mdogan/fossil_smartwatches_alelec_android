package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ie0;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bk0 {
    @DexIgnore
    public static /* final */ b a; // = new hl0();

    @DexIgnore
    public interface a<R extends ne0, T> {
        @DexIgnore
        T a(R r);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        ApiException a(Status status);
    }

    @DexIgnore
    public static <R extends ne0, T> xn1<T> a(ie0<R> ie0, a<R, T> aVar) {
        b bVar = a;
        yn1 yn1 = new yn1();
        ie0.a((ie0.a) new il0(ie0, yn1, aVar, bVar));
        return yn1.a();
    }

    @DexIgnore
    public static <R extends ne0, T extends me0<R>> xn1<T> a(ie0<R> ie0, T t) {
        return a(ie0, new jl0(t));
    }

    @DexIgnore
    public static <R extends ne0> xn1<Void> a(ie0<R> ie0) {
        return a(ie0, new kl0());
    }
}
