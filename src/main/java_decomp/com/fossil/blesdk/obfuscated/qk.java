package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.work.NetworkType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qk extends mk<hk> {
    @DexIgnore
    public qk(Context context, am amVar) {
        super(yk.a(context, amVar).c());
    }

    @DexIgnore
    public boolean a(il ilVar) {
        return ilVar.j.b() == NetworkType.UNMETERED;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(hk hkVar) {
        return !hkVar.a() || hkVar.b();
    }
}
