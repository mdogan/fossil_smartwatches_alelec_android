package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w41 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<w41> CREATOR; // = new x41();
    @DexIgnore
    public int e;
    @DexIgnore
    public u41 f;
    @DexIgnore
    public rd1 g;
    @DexIgnore
    public s31 h;

    @DexIgnore
    public w41(int i, u41 u41, IBinder iBinder, IBinder iBinder2) {
        this.e = i;
        this.f = u41;
        s31 s31 = null;
        this.g = iBinder == null ? null : sd1.a(iBinder);
        if (!(iBinder2 == null || iBinder2 == null)) {
            IInterface queryLocalInterface = iBinder2.queryLocalInterface("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
            s31 = queryLocalInterface instanceof s31 ? (s31) queryLocalInterface : new u31(iBinder2);
        }
        this.h = s31;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, (Parcelable) this.f, i, false);
        rd1 rd1 = this.g;
        IBinder iBinder = null;
        lk0.a(parcel, 3, rd1 == null ? null : rd1.asBinder(), false);
        s31 s31 = this.h;
        if (s31 != null) {
            iBinder = s31.asBinder();
        }
        lk0.a(parcel, 4, iBinder, false);
        lk0.a(parcel, a);
    }
}
