package com.fossil.blesdk.obfuscated;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vt0 implements wt0 {
    @DexIgnore
    public vt0() {
    }

    @DexIgnore
    public /* synthetic */ vt0(ut0 ut0) {
        this();
    }

    @DexIgnore
    public final byte[] a(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }
}
