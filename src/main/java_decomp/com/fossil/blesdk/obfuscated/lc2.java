package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class lc2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ fi2 q;
    @DexIgnore
    public /* final */ fi2 r;
    @DexIgnore
    public /* final */ LinearLayout s;
    @DexIgnore
    public /* final */ LinearLayout t;
    @DexIgnore
    public /* final */ TodayHeartRateChart u;

    @DexIgnore
    public lc2(Object obj, View view, int i, FlexibleTextView flexibleTextView, fi2 fi2, fi2 fi22, ImageView imageView, LinearLayout linearLayout, LinearLayout linearLayout2, TodayHeartRateChart todayHeartRateChart) {
        super(obj, view, i);
        this.q = fi2;
        a((ViewDataBinding) this.q);
        this.r = fi22;
        a((ViewDataBinding) this.r);
        this.s = linearLayout;
        this.t = linearLayout2;
        this.u = todayHeartRateChart;
    }
}
