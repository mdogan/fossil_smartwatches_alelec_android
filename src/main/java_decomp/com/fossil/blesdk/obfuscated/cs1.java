package com.fossil.blesdk.obfuscated;

import android.animation.TypeEvaluator;
import android.graphics.drawable.Drawable;
import android.util.Property;
import com.fossil.blesdk.obfuscated.bs1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface cs1 extends bs1.a {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements TypeEvaluator<e> {
        @DexIgnore
        public static /* final */ TypeEvaluator<e> b; // = new b();
        @DexIgnore
        public /* final */ e a; // = new e();

        @DexIgnore
        /* renamed from: a */
        public e evaluate(float f, e eVar, e eVar2) {
            this.a.a(ws1.b(eVar.a, eVar2.a, f), ws1.b(eVar.b, eVar2.b, f), ws1.b(eVar.c, eVar2.c, f));
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends Property<cs1, e> {
        @DexIgnore
        public static /* final */ Property<cs1, e> a; // = new c("circularReveal");

        @DexIgnore
        public c(String str) {
            super(e.class, str);
        }

        @DexIgnore
        /* renamed from: a */
        public e get(cs1 cs1) {
            return cs1.getRevealInfo();
        }

        @DexIgnore
        /* renamed from: a */
        public void set(cs1 cs1, e eVar) {
            cs1.setRevealInfo(eVar);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends Property<cs1, Integer> {
        @DexIgnore
        public static /* final */ Property<cs1, Integer> a; // = new d("circularRevealScrimColor");

        @DexIgnore
        public d(String str) {
            super(Integer.class, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Integer get(cs1 cs1) {
            return Integer.valueOf(cs1.getCircularRevealScrimColor());
        }

        @DexIgnore
        /* renamed from: a */
        public void set(cs1 cs1, Integer num) {
            cs1.setCircularRevealScrimColor(num.intValue());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public float a;
        @DexIgnore
        public float b;
        @DexIgnore
        public float c;

        @DexIgnore
        public void a(float f, float f2, float f3) {
            this.a = f;
            this.b = f2;
            this.c = f3;
        }

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public e(float f, float f2, float f3) {
            this.a = f;
            this.b = f2;
            this.c = f3;
        }

        @DexIgnore
        public void a(e eVar) {
            a(eVar.a, eVar.b, eVar.c);
        }

        @DexIgnore
        public boolean a() {
            return this.c == Float.MAX_VALUE;
        }

        @DexIgnore
        public e(e eVar) {
            this(eVar.a, eVar.b, eVar.c);
        }
    }

    @DexIgnore
    void a();

    @DexIgnore
    void b();

    @DexIgnore
    int getCircularRevealScrimColor();

    @DexIgnore
    e getRevealInfo();

    @DexIgnore
    void setCircularRevealOverlayDrawable(Drawable drawable);

    @DexIgnore
    void setCircularRevealScrimColor(int i);

    @DexIgnore
    void setRevealInfo(e eVar);
}
