package com.fossil.blesdk.obfuscated;

import android.view.ViewGroup;
import androidx.transition.Transition;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class oh {
    @DexIgnore
    public abstract long a(ViewGroup viewGroup, Transition transition, qh qhVar, qh qhVar2);

    @DexIgnore
    public abstract void a(qh qhVar);

    @DexIgnore
    public abstract String[] a();
}
