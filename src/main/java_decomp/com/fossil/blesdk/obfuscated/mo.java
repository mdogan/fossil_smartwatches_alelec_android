package com.fossil.blesdk.obfuscated;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mo implements ko {
    @DexIgnore
    public /* final */ g4<lo<?>, Object> b; // = new mw();

    @DexIgnore
    public void a(mo moVar) {
        this.b.a(moVar.b);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof mo) {
            return this.b.equals(((mo) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Options{values=" + this.b + '}';
    }

    @DexIgnore
    public <T> mo a(lo<T> loVar, T t) {
        this.b.put(loVar, t);
        return this;
    }

    @DexIgnore
    public <T> T a(lo<T> loVar) {
        return this.b.containsKey(loVar) ? this.b.get(loVar) : loVar.a();
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        for (int i = 0; i < this.b.size(); i++) {
            a(this.b.c(i), this.b.e(i), messageDigest);
        }
    }

    @DexIgnore
    public static <T> void a(lo<T> loVar, Object obj, MessageDigest messageDigest) {
        loVar.a(obj, messageDigest);
    }
}
