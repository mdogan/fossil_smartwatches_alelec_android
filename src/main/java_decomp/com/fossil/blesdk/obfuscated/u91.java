package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u91 {
    @DexIgnore
    public static /* final */ s91 a; // = c();
    @DexIgnore
    public static /* final */ s91 b; // = new t91();

    @DexIgnore
    public static s91 a() {
        return a;
    }

    @DexIgnore
    public static s91 b() {
        return b;
    }

    @DexIgnore
    public static s91 c() {
        try {
            return (s91) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
