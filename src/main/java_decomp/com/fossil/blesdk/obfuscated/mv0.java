package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mv0 implements sv0 {
    @DexIgnore
    public sv0[] a;

    @DexIgnore
    public mv0(sv0... sv0Arr) {
        this.a = sv0Arr;
    }

    @DexIgnore
    public final boolean zza(Class<?> cls) {
        for (sv0 zza : this.a) {
            if (zza.zza(cls)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final rv0 zzb(Class<?> cls) {
        for (sv0 sv0 : this.a) {
            if (sv0.zza(cls)) {
                return sv0.zzb(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }
}
