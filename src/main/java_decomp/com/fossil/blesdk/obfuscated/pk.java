package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import androidx.work.NetworkType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pk extends mk<hk> {
    @DexIgnore
    public static /* final */ String e; // = ej.a("NetworkNotRoamingCtrlr");

    @DexIgnore
    public pk(Context context, am amVar) {
        super(yk.a(context, amVar).c());
    }

    @DexIgnore
    public boolean a(il ilVar) {
        return ilVar.j.b() == NetworkType.NOT_ROAMING;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(hk hkVar) {
        if (Build.VERSION.SDK_INT < 24) {
            ej.a().a(e, "Not-roaming network constraint is not supported before API 24, only checking for connected state.", new Throwable[0]);
            return !hkVar.a();
        } else if (!hkVar.a() || !hkVar.c()) {
            return true;
        } else {
            return false;
        }
    }
}
