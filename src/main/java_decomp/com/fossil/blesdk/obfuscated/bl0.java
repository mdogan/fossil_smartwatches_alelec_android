package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bl0 extends nj0 {
    @DexIgnore
    public /* final */ /* synthetic */ Intent e;
    @DexIgnore
    public /* final */ /* synthetic */ Activity f;
    @DexIgnore
    public /* final */ /* synthetic */ int g;

    @DexIgnore
    public bl0(Intent intent, Activity activity, int i) {
        this.e = intent;
        this.f = activity;
        this.g = i;
    }

    @DexIgnore
    public final void a() {
        Intent intent = this.e;
        if (intent != null) {
            this.f.startActivityForResult(intent, this.g);
        }
    }
}
