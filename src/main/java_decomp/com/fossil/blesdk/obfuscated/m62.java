package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.j62;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class m62 implements l62 {
    @DexIgnore
    public static /* final */ int c; // = Runtime.getRuntime().availableProcessors();
    @DexIgnore
    public static /* final */ int d; // = Math.max(2, Math.min(c - 1, 4));
    @DexIgnore
    public static /* final */ int e; // = ((c * 2) + 1);
    @DexIgnore
    public static /* final */ ThreadFactory f; // = new a();
    @DexIgnore
    public static /* final */ BlockingQueue<Runnable> g; // = new LinkedBlockingQueue(Integer.MAX_VALUE);
    @DexIgnore
    public /* final */ Handler a; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ ThreadPoolExecutor b; // = new ThreadPoolExecutor(d, e, 30, TimeUnit.SECONDS, g, f);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ThreadFactory {
        @DexIgnore
        public /* final */ AtomicInteger a; // = new AtomicInteger(1);

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "AsyncTask #" + this.a.getAndIncrement());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ j62.d e;
        @DexIgnore
        public /* final */ /* synthetic */ j62.a f;

        @DexIgnore
        public b(m62 m62, j62.d dVar, j62.a aVar) {
            this.e = dVar;
            this.f = aVar;
        }

        @DexIgnore
        public void run() {
            j62.d dVar = this.e;
            if (dVar != null) {
                dVar.a(this.f);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ j62.d e;
        @DexIgnore
        public /* final */ /* synthetic */ j62.c f;

        @DexIgnore
        public c(m62 m62, j62.d dVar, j62.c cVar) {
            this.e = dVar;
            this.f = cVar;
        }

        @DexIgnore
        public void run() {
            j62.d dVar = this.e;
            if (dVar != null) {
                dVar.onSuccess(this.f);
            }
        }
    }

    @DexIgnore
    public m62() {
        this.b.allowCoreThreadTimeOut(true);
    }

    @DexIgnore
    public <P extends j62.c, E extends j62.a> void a(E e2, j62.d<P, E> dVar) {
        this.a.post(new b(this, dVar, e2));
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        this.b.execute(runnable);
    }

    @DexIgnore
    public <P extends j62.c, E extends j62.a> void a(P p, j62.d<P, E> dVar) {
        this.a.post(new c(this, dVar, p));
    }
}
