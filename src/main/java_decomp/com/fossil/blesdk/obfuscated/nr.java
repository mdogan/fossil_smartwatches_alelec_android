package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.pr;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface nr {
    @DexIgnore
    public static final nr a = new pr.a().a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements nr {
        @DexIgnore
        public Map<String, String> a() {
            return Collections.emptyMap();
        }
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    Map<String, String> a();
}
