package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gh3 extends as2 implements fh3 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a((rd4) null);
    @DexIgnore
    public ur3<jf2> j;
    @DexIgnore
    public HashMap k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final gh3 a() {
            return new gh3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gh3 e;

        @DexIgnore
        public b(gh3 gh3) {
            this.e = gh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            be4 be4 = be4.a;
            Locale a = tm2.a();
            wd4.a((Object) a, "LanguageHelper.getLocale()");
            Object[] objArr = {a.getLanguage()};
            String format = String.format("https://c.fossil.com/web/shop_battery", Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Purchase Battery URL = " + format);
            this.e.U(format);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gh3 e;

        @DexIgnore
        public c(gh3 gh3) {
            this.e = gh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            be4 be4 = be4.a;
            Locale a = tm2.a();
            wd4.a((Object) a, "LanguageHelper.getLocale()");
            Object[] objArr = {a.getLanguage()};
            String format = String.format("https://c.fossil.com/web/low_battery", Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Need Help URL = " + format);
            this.e.U(format);
        }
    }

    /*
    static {
        String simpleName = gh3.class.getSimpleName();
        if (simpleName != null) {
            wd4.a((Object) simpleName, "ReplaceBatteryFragment::class.java.simpleName!!");
            l = simpleName;
            return;
        }
        wd4.a();
        throw null;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void U(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), l);
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        jf2 jf2 = (jf2) ra.a(LayoutInflater.from(getContext()), R.layout.fragment_replace_battery, (ViewGroup) null, false, O0());
        this.j = new ur3<>(this, jf2);
        wd4.a((Object) jf2, "binding");
        return jf2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<jf2> ur3 = this.j;
        if (ur3 != null) {
            jf2 a2 = ur3.a();
            if (a2 != null) {
                a2.r.setOnClickListener(new b(this));
                a2.q.setOnClickListener(new c(this));
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(eh3 eh3) {
        wd4.b(eh3, "presenter");
        tt1.a(eh3);
        wd4.a((Object) eh3, "Preconditions.checkNotNull(presenter)");
        eh3 eh32 = eh3;
    }
}
