package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.ruler.RulerValuePicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class bf2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RulerValuePicker A;
    @DexIgnore
    public /* final */ ProgressButton B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FossilCircleImageView q;
    @DexIgnore
    public /* final */ TextInputEditText r;
    @DexIgnore
    public /* final */ TextInputEditText s;
    @DexIgnore
    public /* final */ FlexibleButton t;
    @DexIgnore
    public /* final */ FlexibleButton u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ RTLImageView y;
    @DexIgnore
    public /* final */ RulerValuePicker z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bf2(Object obj, View view, int i, FossilCircleImageView fossilCircleImageView, ConstraintLayout constraintLayout, TextInputEditText textInputEditText, TextInputEditText textInputEditText2, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleButton flexibleButton3, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, TextInputLayout textInputLayout, TextInputLayout textInputLayout2, RTLImageView rTLImageView, FossilCircleImageView fossilCircleImageView2, RulerValuePicker rulerValuePicker, RulerValuePicker rulerValuePicker2, ProgressButton progressButton, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7) {
        super(obj, view, i);
        this.q = fossilCircleImageView;
        this.r = textInputEditText;
        this.s = textInputEditText2;
        this.t = flexibleButton;
        this.u = flexibleButton2;
        this.v = flexibleButton3;
        this.w = flexibleTextView3;
        this.x = flexibleTextView5;
        this.y = rTLImageView;
        this.z = rulerValuePicker;
        this.A = rulerValuePicker2;
        this.B = progressButton;
        this.C = flexibleTextView6;
        this.D = flexibleTextView7;
    }
}
