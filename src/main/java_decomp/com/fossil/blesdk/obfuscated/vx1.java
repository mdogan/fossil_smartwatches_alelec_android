package com.fossil.blesdk.obfuscated;

import android.util.Log;
import android.util.Pair;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vx1 {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Map<Pair<String, String>, xn1<dx1>> b; // = new g4();

    @DexIgnore
    public vx1(Executor executor) {
        this.a = executor;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003e, code lost:
        return r4;
     */
    @DexIgnore
    public final synchronized xn1<dx1> a(String str, String str2, xx1 xx1) {
        Pair pair = new Pair(str, str2);
        xn1<dx1> xn1 = this.b.get(pair);
        if (xn1 == null) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(pair);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 24);
                sb.append("Making new request for: ");
                sb.append(valueOf);
                Log.d("FirebaseInstanceId", sb.toString());
            }
            xn1<TContinuationResult> b2 = xx1.a().b(this.a, new wx1(this, pair));
            this.b.put(pair, b2);
            return b2;
        } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf2 = String.valueOf(pair);
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 29);
            sb2.append("Joining ongoing request for: ");
            sb2.append(valueOf2);
            Log.d("FirebaseInstanceId", sb2.toString());
        }
    }

    @DexIgnore
    public final /* synthetic */ xn1 a(Pair pair, xn1 xn1) throws Exception {
        synchronized (this) {
            this.b.remove(pair);
        }
        return xn1;
    }
}
