package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.concurrency.Priority;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class f64<V> extends FutureTask<V> implements b64<j64>, g64, j64, a64 {
    @DexIgnore
    public /* final */ Object e;

    @DexIgnore
    public f64(Callable<V> callable) {
        super(callable);
        this.e = b(callable);
    }

    @DexIgnore
    public boolean b() {
        return ((b64) ((g64) d())).b();
    }

    @DexIgnore
    public Collection<j64> c() {
        return ((b64) ((g64) d())).c();
    }

    @DexIgnore
    public int compareTo(Object obj) {
        return ((g64) d()).compareTo(obj);
    }

    @DexIgnore
    public <T extends b64<j64> & g64 & j64> T d() {
        return (b64) this.e;
    }

    @DexIgnore
    public Priority getPriority() {
        return ((g64) d()).getPriority();
    }

    @DexIgnore
    public void a(j64 j64) {
        ((b64) ((g64) d())).a(j64);
    }

    @DexIgnore
    public <T extends b64<j64> & g64 & j64> T b(Object obj) {
        if (h64.b(obj)) {
            return (b64) obj;
        }
        return new h64();
    }

    @DexIgnore
    public f64(Runnable runnable, V v) {
        super(runnable, v);
        this.e = b(runnable);
    }

    @DexIgnore
    public void a(boolean z) {
        ((j64) ((g64) d())).a(z);
    }

    @DexIgnore
    public boolean a() {
        return ((j64) ((g64) d())).a();
    }

    @DexIgnore
    public void a(Throwable th) {
        ((j64) ((g64) d())).a(th);
    }
}
