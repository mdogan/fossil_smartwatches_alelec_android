package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.fossil.blesdk.obfuscated.bk;
import com.fossil.blesdk.obfuscated.dk;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ak implements ik, nj, dk.b {
    @DexIgnore
    public static /* final */ String n; // = ej.a("DelayMetCommandHandler");
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ bk h;
    @DexIgnore
    public /* final */ jk i;
    @DexIgnore
    public /* final */ Object j; // = new Object();
    @DexIgnore
    public int k; // = 0;
    @DexIgnore
    public PowerManager.WakeLock l;
    @DexIgnore
    public boolean m; // = false;

    @DexIgnore
    public ak(Context context, int i2, String str, bk bkVar) {
        this.e = context;
        this.f = i2;
        this.h = bkVar;
        this.g = str;
        this.i = new jk(this.e, bkVar.d(), this);
    }

    @DexIgnore
    public void a(String str, boolean z) {
        ej.a().a(n, String.format("onExecuted %s, %s", new Object[]{str, Boolean.valueOf(z)}), new Throwable[0]);
        a();
        if (z) {
            Intent b = yj.b(this.e, this.g);
            bk bkVar = this.h;
            bkVar.a((Runnable) new bk.b(bkVar, b, this.f));
        }
        if (this.m) {
            Intent a = yj.a(this.e);
            bk bkVar2 = this.h;
            bkVar2.a((Runnable) new bk.b(bkVar2, a, this.f));
        }
    }

    @DexIgnore
    public void b(List<String> list) {
        if (list.contains(this.g)) {
            synchronized (this.j) {
                if (this.k == 0) {
                    this.k = 1;
                    ej.a().a(n, String.format("onAllConstraintsMet for %s", new Object[]{this.g}), new Throwable[0]);
                    if (this.h.c().c(this.g)) {
                        this.h.f().a(this.g, 600000, this);
                    } else {
                        a();
                    }
                } else {
                    ej.a().a(n, String.format("Already started work for %s", new Object[]{this.g}), new Throwable[0]);
                }
            }
        }
    }

    @DexIgnore
    public final void c() {
        synchronized (this.j) {
            if (this.k < 2) {
                this.k = 2;
                ej.a().a(n, String.format("Stopping work for WorkSpec %s", new Object[]{this.g}), new Throwable[0]);
                this.h.a((Runnable) new bk.b(this.h, yj.c(this.e, this.g), this.f));
                if (this.h.c().b(this.g)) {
                    ej.a().a(n, String.format("WorkSpec %s needs to be rescheduled", new Object[]{this.g}), new Throwable[0]);
                    this.h.a((Runnable) new bk.b(this.h, yj.b(this.e, this.g), this.f));
                } else {
                    ej.a().a(n, String.format("Processor does not have WorkSpec %s. No need to reschedule ", new Object[]{this.g}), new Throwable[0]);
                }
            } else {
                ej.a().a(n, String.format("Already stopped work for %s", new Object[]{this.g}), new Throwable[0]);
            }
        }
    }

    @DexIgnore
    public void a(String str) {
        ej.a().a(n, String.format("Exceeded time limits on execution for %s", new Object[]{str}), new Throwable[0]);
        c();
    }

    @DexIgnore
    public void a(List<String> list) {
        c();
    }

    @DexIgnore
    public void b() {
        this.l = xl.a(this.e, String.format("%s (%s)", new Object[]{this.g, Integer.valueOf(this.f)}));
        ej.a().a(n, String.format("Acquiring wakelock %s for WorkSpec %s", new Object[]{this.l, this.g}), new Throwable[0]);
        this.l.acquire();
        il e2 = this.h.e().g().d().e(this.g);
        if (e2 == null) {
            c();
            return;
        }
        this.m = e2.b();
        if (!this.m) {
            ej.a().a(n, String.format("No constraints for %s", new Object[]{this.g}), new Throwable[0]);
            b(Collections.singletonList(this.g));
            return;
        }
        this.i.c(Collections.singletonList(e2));
    }

    @DexIgnore
    public final void a() {
        synchronized (this.j) {
            this.i.a();
            this.h.f().a(this.g);
            if (this.l != null && this.l.isHeld()) {
                ej.a().a(n, String.format("Releasing wakelock %s for WorkSpec %s", new Object[]{this.l, this.g}), new Throwable[0]);
                this.l.release();
            }
        }
    }
}
