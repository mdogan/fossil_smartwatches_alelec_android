package com.fossil.blesdk.obfuscated;

import android.os.Looper;
import com.fossil.blesdk.obfuscated.af0;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bf0 {
    @DexIgnore
    public /* final */ Set<af0<?>> a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public final void a() {
        for (af0<?> a2 : this.a) {
            a2.a();
        }
        this.a.clear();
    }

    @DexIgnore
    public static <L> af0<L> a(L l, Looper looper, String str) {
        ck0.a(l, (Object) "Listener must not be null");
        ck0.a(looper, (Object) "Looper must not be null");
        ck0.a(str, (Object) "Listener type must not be null");
        return new af0<>(looper, l, str);
    }

    @DexIgnore
    public static <L> af0.a<L> a(L l, String str) {
        ck0.a(l, (Object) "Listener must not be null");
        ck0.a(str, (Object) "Listener type must not be null");
        ck0.a(str, (Object) "Listener type must not be empty");
        return new af0.a<>(l, str);
    }
}
