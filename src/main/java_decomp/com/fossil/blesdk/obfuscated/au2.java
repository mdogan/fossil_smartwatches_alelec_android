package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class au2 extends bs2 implements zt2, xs3.g {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((rd4) null);
    @DexIgnore
    public ur3<l92> k;
    @DexIgnore
    public yt2 l;
    @DexIgnore
    public /* final */ int m; // = k6.a((Context) PortfolioApp.W.c(), (int) R.color.white);
    @DexIgnore
    public /* final */ int n; // = k6.a((Context) PortfolioApp.W.c(), (int) R.color.activeColorPrimary);
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return au2.p;
        }

        @DexIgnore
        public final au2 b() {
            return new au2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ au2 e;

        @DexIgnore
        public b(au2 au2) {
            this.e = au2;
        }

        @DexIgnore
        public final void onClick(View view) {
            wd4.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            au2 au2 = this.e;
            FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(h62.day_friday);
            wd4.a((Object) flexibleTextView, "view.day_friday");
            au2.a((TextView) flexibleTextView);
            this.e.b(6, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ au2 e;

        @DexIgnore
        public c(au2 au2) {
            this.e = au2;
        }

        @DexIgnore
        public final void onClick(View view) {
            wd4.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            au2 au2 = this.e;
            FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(h62.day_saturday);
            wd4.a((Object) flexibleTextView, "view.day_saturday");
            au2.a((TextView) flexibleTextView);
            this.e.b(7, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ au2 a;
        @DexIgnore
        public /* final */ /* synthetic */ l92 b;

        @DexIgnore
        public d(au2 au2, l92 l92) {
            this.a = au2;
            this.b = l92;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            yt2 a2 = au2.a(this.a);
            String valueOf = String.valueOf(i2);
            NumberPicker numberPicker2 = this.b.E;
            wd4.a((Object) numberPicker2, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.D;
            wd4.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ au2 a;
        @DexIgnore
        public /* final */ /* synthetic */ l92 b;

        @DexIgnore
        public e(au2 au2, l92 l92) {
            this.a = au2;
            this.b = l92;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            yt2 a2 = au2.a(this.a);
            NumberPicker numberPicker2 = this.b.C;
            wd4.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            String valueOf2 = String.valueOf(i2);
            NumberPicker numberPicker3 = this.b.D;
            wd4.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ au2 a;
        @DexIgnore
        public /* final */ /* synthetic */ l92 b;

        @DexIgnore
        public f(au2 au2, l92 l92) {
            this.a = au2;
            this.b = l92;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            yt2 a2 = au2.a(this.a);
            NumberPicker numberPicker2 = this.b.C;
            wd4.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.E;
            wd4.a((Object) numberPicker3, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker3.getValue());
            boolean z = true;
            if (i2 != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ au2 e;

        @DexIgnore
        public g(au2 au2) {
            this.e = au2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ au2 e;

        @DexIgnore
        public h(au2 au2) {
            this.e = au2;
        }

        @DexIgnore
        public final void onClick(View view) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = this.e.getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.g(childFragmentManager);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ au2 a;
        @DexIgnore
        public /* final */ /* synthetic */ l92 b;

        @DexIgnore
        public i(au2 au2, l92 l92) {
            this.a = au2;
            this.b = l92;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            au2.a(this.a).a(z);
            ConstraintLayout constraintLayout = this.b.q;
            wd4.a((Object) constraintLayout, "binding.clDaysRepeat");
            constraintLayout.setVisibility(z ? 0 : 8);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ au2 e;

        @DexIgnore
        public j(au2 au2) {
            this.e = au2;
        }

        @DexIgnore
        public final void onClick(View view) {
            au2.a(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ au2 e;

        @DexIgnore
        public k(au2 au2) {
            this.e = au2;
        }

        @DexIgnore
        public final void onClick(View view) {
            wd4.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            au2 au2 = this.e;
            FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(h62.day_sunday);
            wd4.a((Object) flexibleTextView, "view.day_sunday");
            au2.a((TextView) flexibleTextView);
            this.e.b(1, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ au2 e;

        @DexIgnore
        public l(au2 au2) {
            this.e = au2;
        }

        @DexIgnore
        public final void onClick(View view) {
            wd4.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            au2 au2 = this.e;
            FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(h62.day_monday);
            wd4.a((Object) flexibleTextView, "view.day_monday");
            au2.a((TextView) flexibleTextView);
            this.e.b(2, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ au2 e;

        @DexIgnore
        public m(au2 au2) {
            this.e = au2;
        }

        @DexIgnore
        public final void onClick(View view) {
            wd4.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            au2 au2 = this.e;
            FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(h62.day_tuesday);
            wd4.a((Object) flexibleTextView, "view.day_tuesday");
            au2.a((TextView) flexibleTextView);
            this.e.b(3, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ au2 e;

        @DexIgnore
        public n(au2 au2) {
            this.e = au2;
        }

        @DexIgnore
        public final void onClick(View view) {
            wd4.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            au2 au2 = this.e;
            FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(h62.day_wednesday);
            wd4.a((Object) flexibleTextView, "view.day_wednesday");
            au2.a((TextView) flexibleTextView);
            this.e.b(4, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ au2 e;

        @DexIgnore
        public o(au2 au2) {
            this.e = au2;
        }

        @DexIgnore
        public final void onClick(View view) {
            wd4.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            au2 au2 = this.e;
            FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(h62.day_thursday);
            wd4.a((Object) flexibleTextView, "view.day_thursday");
            au2.a((TextView) flexibleTextView);
            this.e.b(5, z);
        }
    }

    /*
    static {
        String simpleName = au2.class.getSimpleName();
        wd4.a((Object) simpleName, "AlarmFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ yt2 a(au2 au2) {
        yt2 yt2 = au2.l;
        if (yt2 != null) {
            return yt2;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void K() {
        ur3<l92> ur3 = this.k;
        if (ur3 != null) {
            l92 a2 = ur3.a();
            if (a2 != null) {
                ImageView imageView = a2.B;
                if (imageView != null) {
                    imageView.setVisibility(0);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void O(boolean z) {
        if (!z) {
            R("add_alarm_view");
        } else {
            R("edit_alarm_view");
        }
    }

    @DexIgnore
    public void R() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.G(childFragmentManager);
        }
    }

    @DexIgnore
    public String R0() {
        return p;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final void b(int i2, boolean z) {
        yt2 yt2 = this.l;
        if (yt2 != null) {
            yt2.a(z, i2);
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void c(int i2, boolean z) {
        ur3<l92> ur3 = this.k;
        if (ur3 != null) {
            l92 a2 = ur3.a();
            if (a2 != null) {
                switch (i2) {
                    case 1:
                        FlexibleTextView flexibleTextView = a2.u;
                        wd4.a((Object) flexibleTextView, "binding.daySunday");
                        flexibleTextView.setSelected(z);
                        FlexibleTextView flexibleTextView2 = a2.u;
                        wd4.a((Object) flexibleTextView2, "binding.daySunday");
                        a((TextView) flexibleTextView2);
                        return;
                    case 2:
                        FlexibleTextView flexibleTextView3 = a2.s;
                        wd4.a((Object) flexibleTextView3, "binding.dayMonday");
                        flexibleTextView3.setSelected(z);
                        FlexibleTextView flexibleTextView4 = a2.s;
                        wd4.a((Object) flexibleTextView4, "binding.dayMonday");
                        a((TextView) flexibleTextView4);
                        return;
                    case 3:
                        FlexibleTextView flexibleTextView5 = a2.w;
                        wd4.a((Object) flexibleTextView5, "binding.dayTuesday");
                        flexibleTextView5.setSelected(z);
                        FlexibleTextView flexibleTextView6 = a2.w;
                        wd4.a((Object) flexibleTextView6, "binding.dayTuesday");
                        a((TextView) flexibleTextView6);
                        return;
                    case 4:
                        FlexibleTextView flexibleTextView7 = a2.x;
                        wd4.a((Object) flexibleTextView7, "binding.dayWednesday");
                        flexibleTextView7.setSelected(z);
                        FlexibleTextView flexibleTextView8 = a2.x;
                        wd4.a((Object) flexibleTextView8, "binding.dayWednesday");
                        a((TextView) flexibleTextView8);
                        return;
                    case 5:
                        FlexibleTextView flexibleTextView9 = a2.v;
                        wd4.a((Object) flexibleTextView9, "binding.dayThursday");
                        flexibleTextView9.setSelected(z);
                        FlexibleTextView flexibleTextView10 = a2.v;
                        wd4.a((Object) flexibleTextView10, "binding.dayThursday");
                        a((TextView) flexibleTextView10);
                        return;
                    case 6:
                        FlexibleTextView flexibleTextView11 = a2.r;
                        wd4.a((Object) flexibleTextView11, "binding.dayFriday");
                        flexibleTextView11.setSelected(z);
                        FlexibleTextView flexibleTextView12 = a2.r;
                        wd4.a((Object) flexibleTextView12, "binding.dayFriday");
                        a((TextView) flexibleTextView12);
                        return;
                    case 7:
                        FlexibleTextView flexibleTextView13 = a2.t;
                        wd4.a((Object) flexibleTextView13, "binding.daySaturday");
                        flexibleTextView13.setSelected(z);
                        FlexibleTextView flexibleTextView14 = a2.t;
                        wd4.a((Object) flexibleTextView14, "binding.daySaturday");
                        a((TextView) flexibleTextView14);
                        return;
                    default:
                        return;
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void e(boolean z) {
        int i2;
        ur3<l92> ur3 = this.k;
        if (ur3 != null) {
            l92 a2 = ur3.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.y;
                if (flexibleButton != null) {
                    flexibleButton.setEnabled(z);
                }
            }
            ur3<l92> ur32 = this.k;
            if (ur32 != null) {
                l92 a3 = ur32.a();
                if (a3 != null) {
                    FlexibleButton flexibleButton2 = a3.y;
                    if (flexibleButton2 != null) {
                        if (z) {
                            i2 = k6.a((Context) PortfolioApp.W.c(), (int) R.color.activeColorPrimary);
                        } else {
                            i2 = k6.a((Context) PortfolioApp.W.c(), (int) R.color.fossilCoolGray);
                        }
                        flexibleButton2.setBackgroundColor(i2);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void k(boolean z) {
        ur3<l92> ur3 = this.k;
        if (ur3 != null) {
            l92 a2 = ur3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.F;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                }
            }
            ur3<l92> ur32 = this.k;
            if (ur32 != null) {
                l92 a3 = ur32.a();
                if (a3 != null) {
                    ConstraintLayout constraintLayout = a3.q;
                    if (constraintLayout != null) {
                        constraintLayout.setVisibility(z ? 0 : 8);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void l(boolean z) {
        ur3<l92> ur3 = this.k;
        if (ur3 != null) {
            l92 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.z;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(tm2.a((Context) PortfolioApp.W.c(), !z ? R.string.AlertsDiana_AddAlarm_AddAlarm_Title__AddAlarm : R.string.AlertsDiana_EditAlarm_EditAlarm_Title__EditAlarm));
                }
            }
            O(z);
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        l92 l92 = (l92) ra.a(layoutInflater, R.layout.fragment_alarm, viewGroup, false, O0());
        wd4.a((Object) l92, "binding");
        a(l92);
        this.k = new ur3<>(this, l92);
        ur3<l92> ur3 = this.k;
        if (ur3 != null) {
            l92 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        yt2 yt2 = this.l;
        if (yt2 != null) {
            yt2.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        yt2 yt2 = this.l;
        if (yt2 != null) {
            yt2.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void x() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void a(yt2 yt2) {
        wd4.b(yt2, "presenter");
        this.l = yt2;
    }

    @DexIgnore
    public final void a(l92 l92) {
        l92.A.setOnClickListener(new g(this));
        l92.B.setOnClickListener(new h(this));
        l92.F.setOnCheckedChangeListener(new i(this, l92));
        l92.y.setOnClickListener(new j(this));
        l92.u.setOnClickListener(new k(this));
        l92.s.setOnClickListener(new l(this));
        l92.w.setOnClickListener(new m(this));
        l92.x.setOnClickListener(new n(this));
        l92.v.setOnClickListener(new o(this));
        l92.r.setOnClickListener(new b(this));
        l92.t.setOnClickListener(new c(this));
        NumberPicker numberPicker = l92.C;
        wd4.a((Object) numberPicker, "binding.numberPickerOne");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = l92.C;
        wd4.a((Object) numberPicker2, "binding.numberPickerOne");
        numberPicker2.setMaxValue(12);
        l92.C.setOnValueChangedListener(new d(this, l92));
        NumberPicker numberPicker3 = l92.E;
        wd4.a((Object) numberPicker3, "binding.numberPickerTwo");
        numberPicker3.setMinValue(0);
        NumberPicker numberPicker4 = l92.E;
        wd4.a((Object) numberPicker4, "binding.numberPickerTwo");
        numberPicker4.setMaxValue(59);
        l92.E.setOnValueChangedListener(new e(this, l92));
        String[] strArr = new String[2];
        String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Am);
        wd4.a((Object) a2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
        if (a2 != null) {
            String upperCase = a2.toUpperCase();
            wd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            strArr[0] = upperCase;
            String a3 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Pm);
            wd4.a((Object) a3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
            if (a3 != null) {
                String upperCase2 = a3.toUpperCase();
                wd4.a((Object) upperCase2, "(this as java.lang.String).toUpperCase()");
                strArr[1] = upperCase2;
                NumberPicker numberPicker5 = l92.D;
                wd4.a((Object) numberPicker5, "binding.numberPickerThree");
                numberPicker5.setMinValue(0);
                NumberPicker numberPicker6 = l92.D;
                wd4.a((Object) numberPicker6, "binding.numberPickerThree");
                numberPicker6.setMaxValue(1);
                l92.D.setDisplayedValues(strArr);
                l92.D.setOnValueChangedListener(new f(this, l92));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public final void a(TextView textView) {
        wd4.b(textView, "textView");
        if (textView.isSelected()) {
            textView.setTextColor(this.m);
        } else {
            textView.setTextColor(this.n);
        }
    }

    @DexIgnore
    public void a(int i2) {
        int i3;
        int i4 = i2 / 60;
        int i5 = i2 % 60;
        if (i4 >= 12) {
            i3 = 1;
            i4 -= 12;
        } else {
            i3 = 0;
        }
        if (i4 == 0) {
            i4 = 12;
        }
        ur3<l92> ur3 = this.k;
        if (ur3 != null) {
            l92 a2 = ur3.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.C;
                wd4.a((Object) numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i4);
                NumberPicker numberPicker2 = a2.E;
                wd4.a((Object) numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i5);
                NumberPicker numberPicker3 = a2.D;
                wd4.a((Object) numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i3);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(SparseIntArray sparseIntArray) {
        wd4.b(sparseIntArray, "daysRepeat");
        int size = sparseIntArray.size();
        for (int i2 = 1; i2 <= 7; i2++) {
            boolean z = false;
            if (size == 0) {
                c(i2, false);
            } else {
                if (sparseIntArray.get(i2) == i2) {
                    z = true;
                }
                c(i2, z);
            }
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wd4.b(str, "tag");
        super.a(str, i2, intent);
        int hashCode = str.hashCode();
        if (hashCode != 1038249436) {
            if (hashCode == 1185284775 && str.equals("CONFIRM_SET_ALARM_FAILED") && i2 == R.id.fb_ok) {
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                }
            }
        } else if (str.equals("CONFIRM_DELETE_ALARM") && i2 == R.id.tv_ok) {
            yt2 yt2 = this.l;
            if (yt2 != null) {
                yt2.h();
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(Alarm alarm, boolean z) {
        wd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "showSetAlarmComplete: alarm = " + alarm + ", isSuccess = " + z);
        if (z) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
            }
        } else if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.F(childFragmentManager);
        }
    }
}
