package com.fossil.blesdk.obfuscated;

import java.net.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rn4 {
    @DexIgnore
    public static String a(pm4 pm4, Proxy.Type type) {
        StringBuilder sb = new StringBuilder();
        sb.append(pm4.e());
        sb.append(' ');
        if (b(pm4, type)) {
            sb.append(pm4.g());
        } else {
            sb.append(a(pm4.g()));
        }
        sb.append(" HTTP/1.1");
        return sb.toString();
    }

    @DexIgnore
    public static boolean b(pm4 pm4, Proxy.Type type) {
        return !pm4.d() && type == Proxy.Type.HTTP;
    }

    @DexIgnore
    public static String a(lm4 lm4) {
        String c = lm4.c();
        String e = lm4.e();
        if (e == null) {
            return c;
        }
        return c + '?' + e;
    }
}
