package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class om2 extends dm2 {
    @DexIgnore
    public om2(im2 im2, gm2 gm2) {
        super(im2, gm2);
    }

    @DexIgnore
    public bm2 a() {
        return this.a.a(137);
    }

    @DexIgnore
    public byte[] b() {
        return this.a.e(132);
    }
}
