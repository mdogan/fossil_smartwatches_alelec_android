package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepWeekChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class zf2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ OverviewSleepWeekChart q;

    @DexIgnore
    public zf2(Object obj, View view, int i, OverviewSleepWeekChart overviewSleepWeekChart) {
        super(obj, view, i);
        this.q = overviewSleepWeekChart;
    }
}
