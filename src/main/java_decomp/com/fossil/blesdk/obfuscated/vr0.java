package com.fossil.blesdk.obfuscated;

import android.os.Parcel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vr0 {
    /*
    static {
        vr0.class.getClassLoader();
    }
    */

    @DexIgnore
    public static void a(Parcel parcel, boolean z) {
        parcel.writeInt(1);
    }

    @DexIgnore
    public static boolean a(Parcel parcel) {
        return parcel.readInt() != 0;
    }
}
