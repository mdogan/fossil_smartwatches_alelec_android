package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.jy3;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sx3 extends jy3 {
    @DexIgnore
    public static /* final */ int b; // = 22;
    @DexIgnore
    public /* final */ AssetManager a;

    @DexIgnore
    public sx3(Context context) {
        this.a = context.getAssets();
    }

    @DexIgnore
    public static String c(hy3 hy3) {
        return hy3.d.toString().substring(b);
    }

    @DexIgnore
    public boolean a(hy3 hy3) {
        Uri uri = hy3.d;
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public jy3.a a(hy3 hy3, int i) throws IOException {
        return new jy3.a(this.a.open(c(hy3)), Picasso.LoadedFrom.DISK);
    }
}
