package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface mj4<S> extends CoroutineContext.a {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <S, R> R a(mj4<S> mj4, R r, kd4<? super R, ? super CoroutineContext.a, ? extends R> kd4) {
            wd4.b(kd4, "operation");
            return CoroutineContext.a.C0169a.a(mj4, r, kd4);
        }

        @DexIgnore
        public static <S, E extends CoroutineContext.a> E a(mj4<S> mj4, CoroutineContext.b<E> bVar) {
            wd4.b(bVar, "key");
            return CoroutineContext.a.C0169a.a((CoroutineContext.a) mj4, bVar);
        }

        @DexIgnore
        public static <S> CoroutineContext a(mj4<S> mj4, CoroutineContext coroutineContext) {
            wd4.b(coroutineContext, "context");
            return CoroutineContext.a.C0169a.a((CoroutineContext.a) mj4, coroutineContext);
        }

        @DexIgnore
        public static <S> CoroutineContext b(mj4<S> mj4, CoroutineContext.b<?> bVar) {
            wd4.b(bVar, "key");
            return CoroutineContext.a.C0169a.b(mj4, bVar);
        }
    }

    @DexIgnore
    S a(CoroutineContext coroutineContext);

    @DexIgnore
    void a(CoroutineContext coroutineContext, S s);
}
