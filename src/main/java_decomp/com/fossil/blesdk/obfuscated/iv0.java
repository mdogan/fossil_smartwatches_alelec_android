package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iv0 extends fv0 {
    @DexIgnore
    public iv0() {
        super();
    }

    @DexIgnore
    public static <E> xu0<E> b(Object obj, long j) {
        return (xu0) ix0.f(obj, j);
    }

    @DexIgnore
    public final void a(Object obj, long j) {
        b(obj, j).z();
    }

    @DexIgnore
    public final <E> void a(Object obj, Object obj2, long j) {
        xu0 b = b(obj, j);
        xu0 b2 = b(obj2, j);
        int size = b.size();
        int size2 = b2.size();
        if (size > 0 && size2 > 0) {
            if (!b.y()) {
                b = b.c(size2 + size);
            }
            b.addAll(b2);
        }
        if (size > 0) {
            b2 = b;
        }
        ix0.a(obj, j, (Object) b2);
    }
}
