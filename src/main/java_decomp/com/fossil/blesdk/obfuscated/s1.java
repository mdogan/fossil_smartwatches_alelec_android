package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class s1 extends c1<h7> implements Menu {
    @DexIgnore
    public s1(Context context, h7 h7Var) {
        super(context, h7Var);
    }

    @DexIgnore
    public MenuItem add(CharSequence charSequence) {
        return a(((h7) this.a).add(charSequence));
    }

    @DexIgnore
    public int addIntentOptions(int i, int i2, int i3, ComponentName componentName, Intent[] intentArr, Intent intent, int i4, MenuItem[] menuItemArr) {
        MenuItem[] menuItemArr2 = menuItemArr;
        MenuItem[] menuItemArr3 = menuItemArr2 != null ? new MenuItem[menuItemArr2.length] : null;
        int addIntentOptions = ((h7) this.a).addIntentOptions(i, i2, i3, componentName, intentArr, intent, i4, menuItemArr3);
        if (menuItemArr3 != null) {
            int length = menuItemArr3.length;
            for (int i5 = 0; i5 < length; i5++) {
                menuItemArr2[i5] = a(menuItemArr3[i5]);
            }
        }
        return addIntentOptions;
    }

    @DexIgnore
    public SubMenu addSubMenu(CharSequence charSequence) {
        return a(((h7) this.a).addSubMenu(charSequence));
    }

    @DexIgnore
    public void clear() {
        b();
        ((h7) this.a).clear();
    }

    @DexIgnore
    public void close() {
        ((h7) this.a).close();
    }

    @DexIgnore
    public MenuItem findItem(int i) {
        return a(((h7) this.a).findItem(i));
    }

    @DexIgnore
    public MenuItem getItem(int i) {
        return a(((h7) this.a).getItem(i));
    }

    @DexIgnore
    public boolean hasVisibleItems() {
        return ((h7) this.a).hasVisibleItems();
    }

    @DexIgnore
    public boolean isShortcutKey(int i, KeyEvent keyEvent) {
        return ((h7) this.a).isShortcutKey(i, keyEvent);
    }

    @DexIgnore
    public boolean performIdentifierAction(int i, int i2) {
        return ((h7) this.a).performIdentifierAction(i, i2);
    }

    @DexIgnore
    public boolean performShortcut(int i, KeyEvent keyEvent, int i2) {
        return ((h7) this.a).performShortcut(i, keyEvent, i2);
    }

    @DexIgnore
    public void removeGroup(int i) {
        a(i);
        ((h7) this.a).removeGroup(i);
    }

    @DexIgnore
    public void removeItem(int i) {
        b(i);
        ((h7) this.a).removeItem(i);
    }

    @DexIgnore
    public void setGroupCheckable(int i, boolean z, boolean z2) {
        ((h7) this.a).setGroupCheckable(i, z, z2);
    }

    @DexIgnore
    public void setGroupEnabled(int i, boolean z) {
        ((h7) this.a).setGroupEnabled(i, z);
    }

    @DexIgnore
    public void setGroupVisible(int i, boolean z) {
        ((h7) this.a).setGroupVisible(i, z);
    }

    @DexIgnore
    public void setQwertyMode(boolean z) {
        ((h7) this.a).setQwertyMode(z);
    }

    @DexIgnore
    public int size() {
        return ((h7) this.a).size();
    }

    @DexIgnore
    public MenuItem add(int i) {
        return a(((h7) this.a).add(i));
    }

    @DexIgnore
    public SubMenu addSubMenu(int i) {
        return a(((h7) this.a).addSubMenu(i));
    }

    @DexIgnore
    public MenuItem add(int i, int i2, int i3, CharSequence charSequence) {
        return a(((h7) this.a).add(i, i2, i3, charSequence));
    }

    @DexIgnore
    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        return a(((h7) this.a).addSubMenu(i, i2, i3, charSequence));
    }

    @DexIgnore
    public MenuItem add(int i, int i2, int i3, int i4) {
        return a(((h7) this.a).add(i, i2, i3, i4));
    }

    @DexIgnore
    public SubMenu addSubMenu(int i, int i2, int i3, int i4) {
        return a(((h7) this.a).addSubMenu(i, i2, i3, i4));
    }
}
