package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ou0;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class hu0<T extends ou0<T>> {
    @DexIgnore
    public abstract int a(Map.Entry<?, ?> entry);

    @DexIgnore
    public abstract lu0<T> a(Object obj);

    @DexIgnore
    public abstract void a(px0 px0, Map.Entry<?, ?> entry) throws IOException;

    @DexIgnore
    public abstract boolean a(tv0 tv0);

    @DexIgnore
    public abstract lu0<T> b(Object obj);

    @DexIgnore
    public abstract void c(Object obj);
}
