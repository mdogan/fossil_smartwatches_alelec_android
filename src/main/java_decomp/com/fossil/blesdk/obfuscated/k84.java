package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k84 {
    @DexIgnore
    public /* final */ View a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ AttributeSet d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public View a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Context c;
        @DexIgnore
        public AttributeSet d;

        @DexIgnore
        public a(k84 k84) {
            wd4.b(k84, Constants.RESULT);
            this.a = k84.e();
            this.b = k84.c();
            this.c = k84.b();
            this.d = k84.a();
        }

        @DexIgnore
        public final a a(View view) {
            this.a = view;
            return this;
        }

        @DexIgnore
        public final k84 a() {
            String str = this.b;
            if (str != null) {
                View view = this.a;
                if (view == null) {
                    view = null;
                } else if (!wd4.a((Object) str, (Object) view.getClass().getName())) {
                    throw new IllegalStateException(("name (" + str + ") must be the view's fully qualified name (" + view.getClass().getName() + ')').toString());
                }
                Context context = this.c;
                if (context != null) {
                    return new k84(view, str, context, this.d);
                }
                throw new IllegalStateException("context == null");
            }
            throw new IllegalStateException("name == null".toString());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new b((rd4) null);
    }
    */

    @DexIgnore
    public k84(View view, String str, Context context, AttributeSet attributeSet) {
        wd4.b(str, "name");
        wd4.b(context, "context");
        this.a = view;
        this.b = str;
        this.c = context;
        this.d = attributeSet;
    }

    @DexIgnore
    public final AttributeSet a() {
        return this.d;
    }

    @DexIgnore
    public final Context b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final a d() {
        return new a(this);
    }

    @DexIgnore
    public final View e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof k84)) {
            return false;
        }
        k84 k84 = (k84) obj;
        return wd4.a((Object) this.a, (Object) k84.a) && wd4.a((Object) this.b, (Object) k84.b) && wd4.a((Object) this.c, (Object) k84.c) && wd4.a((Object) this.d, (Object) k84.d);
    }

    @DexIgnore
    public int hashCode() {
        View view = this.a;
        int i = 0;
        int hashCode = (view != null ? view.hashCode() : 0) * 31;
        String str = this.b;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        Context context = this.c;
        int hashCode3 = (hashCode2 + (context != null ? context.hashCode() : 0)) * 31;
        AttributeSet attributeSet = this.d;
        if (attributeSet != null) {
            i = attributeSet.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "InflateResult(view=" + this.a + ", name=" + this.b + ", context=" + this.c + ", attrs=" + this.d + ")";
    }
}
