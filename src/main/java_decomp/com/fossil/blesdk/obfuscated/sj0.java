package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sj0 {
    @DexIgnore
    public sj0(String str, String str2) {
        ck0.a(str, (Object) "log tag cannot be null");
        ck0.a(str.length() <= 23, "tag \"%s\" is longer than the %d character maximum", str, 23);
        if (str2 == null || str2.length() <= 0) {
        }
    }

    @DexIgnore
    public sj0(String str) {
        this(str, (String) null);
    }
}
