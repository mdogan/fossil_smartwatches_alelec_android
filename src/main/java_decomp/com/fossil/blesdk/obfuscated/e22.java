package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e22 {
    @DexIgnore
    public /* final */ d22 a;
    @DexIgnore
    public /* final */ int[] b;

    @DexIgnore
    public e22(d22 d22, int[] iArr) {
        if (iArr.length != 0) {
            this.a = d22;
            int length = iArr.length;
            if (length <= 1 || iArr[0] != 0) {
                this.b = iArr;
                return;
            }
            int i = 1;
            while (i < length && iArr[i] == 0) {
                i++;
            }
            if (i == length) {
                this.b = new int[]{0};
                return;
            }
            this.b = new int[(length - i)];
            int[] iArr2 = this.b;
            System.arraycopy(iArr, i, iArr2, 0, iArr2.length);
            return;
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public int[] a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.b.length - 1;
    }

    @DexIgnore
    public boolean c() {
        return this.b[0] == 0;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(b() * 8);
        for (int b2 = b(); b2 >= 0; b2--) {
            int a2 = a(b2);
            if (a2 != 0) {
                if (a2 < 0) {
                    sb.append(" - ");
                    a2 = -a2;
                } else if (sb.length() > 0) {
                    sb.append(" + ");
                }
                if (b2 == 0 || a2 != 1) {
                    int c = this.a.c(a2);
                    if (c == 0) {
                        sb.append('1');
                    } else if (c == 1) {
                        sb.append('a');
                    } else {
                        sb.append("a^");
                        sb.append(c);
                    }
                }
                if (b2 != 0) {
                    if (b2 == 1) {
                        sb.append('x');
                    } else {
                        sb.append("x^");
                        sb.append(b2);
                    }
                }
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public int a(int i) {
        int[] iArr = this.b;
        return iArr[(iArr.length - 1) - i];
    }

    @DexIgnore
    public e22[] b(e22 e22) {
        if (!this.a.equals(e22.a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (!e22.c()) {
            e22 b2 = this.a.b();
            int b3 = this.a.b(e22.a(e22.b()));
            e22 e222 = b2;
            e22 e223 = this;
            while (e223.b() >= e22.b() && !e223.c()) {
                int b4 = e223.b() - e22.b();
                int b5 = this.a.b(e223.a(e223.b()), b3);
                e22 a2 = e22.a(b4, b5);
                e222 = e222.a(this.a.a(b4, b5));
                e223 = e223.a(a2);
            }
            return new e22[]{e222, e223};
        } else {
            throw new IllegalArgumentException("Divide by 0");
        }
    }

    @DexIgnore
    public e22 c(e22 e22) {
        if (!this.a.equals(e22.a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (c() || e22.c()) {
            return this.a.b();
        } else {
            int[] iArr = this.b;
            int length = iArr.length;
            int[] iArr2 = e22.b;
            int length2 = iArr2.length;
            int[] iArr3 = new int[((length + length2) - 1)];
            for (int i = 0; i < length; i++) {
                int i2 = iArr[i];
                for (int i3 = 0; i3 < length2; i3++) {
                    int i4 = i + i3;
                    iArr3[i4] = d22.c(iArr3[i4], this.a.b(i2, iArr2[i3]));
                }
            }
            return new e22(this.a, iArr3);
        }
    }

    @DexIgnore
    public e22 a(e22 e22) {
        if (!this.a.equals(e22.a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (c()) {
            return e22;
        } else {
            if (e22.c()) {
                return this;
            }
            int[] iArr = this.b;
            int[] iArr2 = e22.b;
            if (iArr.length > iArr2.length) {
                int[] iArr3 = iArr;
                iArr = iArr2;
                iArr2 = iArr3;
            }
            int[] iArr4 = new int[iArr2.length];
            int length = iArr2.length - iArr.length;
            System.arraycopy(iArr2, 0, iArr4, 0, length);
            for (int i = length; i < iArr2.length; i++) {
                iArr4[i] = d22.c(iArr[i - length], iArr2[i]);
            }
            return new e22(this.a, iArr4);
        }
    }

    @DexIgnore
    public e22 a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.a.b();
        } else {
            int length = this.b.length;
            int[] iArr = new int[(i + length)];
            for (int i3 = 0; i3 < length; i3++) {
                iArr[i3] = this.a.b(this.b[i3], i2);
            }
            return new e22(this.a, iArr);
        }
    }
}
