package com.fossil.blesdk.obfuscated;

import com.facebook.internal.NativeProtocol;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yc4 {
    @DexIgnore
    public static /* final */ xc4 a;

    /*
    static {
        xc4 xc4;
        Object newInstance;
        Object newInstance2;
        Class<xc4> cls = xc4.class;
        int a2 = a();
        if (a2 >= 65544) {
            try {
                newInstance2 = Class.forName("kotlin.internal.jdk8.JDK8PlatformImplementations").newInstance();
                wd4.a((Object) newInstance2, "Class.forName(\"kotlin.in\u2026entations\").newInstance()");
                if (newInstance2 != null) {
                    xc4 = (xc4) newInstance2;
                    a = xc4;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
            } catch (ClassCastException e) {
                ClassLoader classLoader = newInstance2.getClass().getClassLoader();
                ClassLoader classLoader2 = cls.getClassLoader();
                Throwable initCause = new ClassCastException("Instance classloader: " + classLoader + ", base type classloader: " + classLoader2).initCause(e);
                wd4.a((Object) initCause, "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)");
                throw initCause;
            } catch (ClassNotFoundException unused) {
                try {
                    Object newInstance3 = Class.forName("kotlin.internal.JRE8PlatformImplementations").newInstance();
                    wd4.a((Object) newInstance3, "Class.forName(\"kotlin.in\u2026entations\").newInstance()");
                    if (newInstance3 != null) {
                        try {
                            xc4 = (xc4) newInstance3;
                        } catch (ClassCastException e2) {
                            ClassLoader classLoader3 = newInstance3.getClass().getClassLoader();
                            ClassLoader classLoader4 = cls.getClassLoader();
                            Throwable initCause2 = new ClassCastException("Instance classloader: " + classLoader3 + ", base type classloader: " + classLoader4).initCause(e2);
                            wd4.a((Object) initCause2, "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)");
                            throw initCause2;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
                    }
                } catch (ClassNotFoundException unused2) {
                }
            }
        }
        if (a2 >= 65543) {
            try {
                newInstance = Class.forName("com.fossil.blesdk.obfuscated.ad4").newInstance();
                wd4.a(newInstance, "Class.forName(\"kotlin.in\u2026entations\").newInstance()");
                if (newInstance != null) {
                    xc4 = (xc4) newInstance;
                    a = xc4;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
            } catch (ClassCastException e3) {
                ClassLoader classLoader5 = newInstance.getClass().getClassLoader();
                ClassLoader classLoader6 = cls.getClassLoader();
                Throwable initCause3 = new ClassCastException("Instance classloader: " + classLoader5 + ", base type classloader: " + classLoader6).initCause(e3);
                wd4.a((Object) initCause3, "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)");
                throw initCause3;
            } catch (ClassNotFoundException unused3) {
                try {
                    Object newInstance4 = Class.forName("kotlin.internal.JRE7PlatformImplementations").newInstance();
                    wd4.a(newInstance4, "Class.forName(\"kotlin.in\u2026entations\").newInstance()");
                    if (newInstance4 != null) {
                        try {
                            xc4 = (xc4) newInstance4;
                        } catch (ClassCastException e4) {
                            ClassLoader classLoader7 = newInstance4.getClass().getClassLoader();
                            ClassLoader classLoader8 = cls.getClassLoader();
                            Throwable initCause4 = new ClassCastException("Instance classloader: " + classLoader7 + ", base type classloader: " + classLoader8).initCause(e4);
                            wd4.a((Object) initCause4, "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)");
                            throw initCause4;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
                    }
                } catch (ClassNotFoundException unused4) {
                }
            }
        }
        xc4 = new xc4();
        a = xc4;
    }
    */

    @DexIgnore
    public static final int a() {
        String property = System.getProperty("java.specification.version");
        if (property == null) {
            return NativeProtocol.MESSAGE_GET_LIKE_STATUS_REQUEST;
        }
        int a2 = StringsKt__StringsKt.a((CharSequence) property, '.', 0, false, 6, (Object) null);
        if (a2 < 0) {
            try {
                return Integer.parseInt(property) * 65536;
            } catch (NumberFormatException unused) {
                return NativeProtocol.MESSAGE_GET_LIKE_STATUS_REQUEST;
            }
        } else {
            int i = a2 + 1;
            int a3 = StringsKt__StringsKt.a((CharSequence) property, '.', i, false, 4, (Object) null);
            if (a3 < 0) {
                a3 = property.length();
            }
            if (property != null) {
                String substring = property.substring(0, a2);
                wd4.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                if (property != null) {
                    String substring2 = property.substring(i, a3);
                    wd4.a((Object) substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    try {
                        return (Integer.parseInt(substring) * 65536) + Integer.parseInt(substring2);
                    } catch (NumberFormatException unused2) {
                        return NativeProtocol.MESSAGE_GET_LIKE_STATUS_REQUEST;
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
    }
}
