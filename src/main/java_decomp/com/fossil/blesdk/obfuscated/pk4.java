package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pk4 {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public pk4(String str) {
        wd4.b(str, "symbol");
        this.a = str;
    }

    @DexIgnore
    public String toString() {
        return this.a;
    }
}
