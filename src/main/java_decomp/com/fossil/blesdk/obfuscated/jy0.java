package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import android.util.Log;
import com.fossil.blesdk.obfuscated.ee0;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jy0 extends ue0<Status, ny0> {
    @DexIgnore
    public /* final */ sd0 s;

    @DexIgnore
    public jy0(sd0 sd0, he0 he0) {
        super(nd0.o, he0);
        this.s = sd0;
    }

    @DexIgnore
    public final /* synthetic */ ne0 a(Status status) {
        return status;
    }

    @DexIgnore
    public final /* synthetic */ void a(ee0.b bVar) throws RemoteException {
        ny0 ny0 = (ny0) bVar;
        my0 my0 = new my0(this);
        try {
            sd0 sd0 = this.s;
            if (sd0.n != null && sd0.m.o.length == 0) {
                sd0.m.o = sd0.n.zza();
            }
            if (sd0.o != null && sd0.m.v.length == 0) {
                sd0.m.v = sd0.o.zza();
            }
            ky0 ky0 = sd0.m;
            byte[] bArr = new byte[ky0.a()];
            wx0.a(ky0, bArr, 0, bArr.length);
            sd0.f = bArr;
            ((ry0) ny0.x()).a(my0, this.s);
        } catch (RuntimeException e) {
            Log.e("ClearcutLoggerApiImpl", "derived ClearcutLogger.MessageProducer ", e);
            c(new Status(10, "MessageProducer"));
        }
    }
}
