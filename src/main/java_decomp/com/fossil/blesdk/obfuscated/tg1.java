package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.jj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tg1 extends jj0<lg1> {
    @DexIgnore
    public tg1(Context context, Looper looper, jj0.a aVar, jj0.b bVar) {
        super(context, looper, 93, aVar, bVar, (String) null);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
        if (queryLocalInterface instanceof lg1) {
            return (lg1) queryLocalInterface;
        }
        return new ng1(iBinder);
    }

    @DexIgnore
    public final int i() {
        return ae0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.measurement.START";
    }
}
