package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w24 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ Throwable f;

    @DexIgnore
    public w24(Context context, Throwable th) {
        this.e = context;
        this.f = th;
    }

    @DexIgnore
    public final void run() {
        try {
            if (i04.s()) {
                new d14(new o04(this.e, k04.a(this.e, false, (l04) null), 99, this.f, q04.m)).a();
            }
        } catch (Throwable th) {
            u14 f2 = k04.m;
            f2.c("reportSdkSelfException error: " + th);
        }
    }
}
