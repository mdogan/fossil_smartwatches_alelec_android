package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.enumerate.Priority;
import com.fossil.blesdk.device.logic.phase.GetFilePhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f50 extends g50 {
    @DexIgnore
    public Priority R; // = Priority.LOW;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f50(Peripheral peripheral, Phase.a aVar, HashMap<GetFilePhase.GetFileOption, Object> hashMap) {
        super(peripheral, aVar, PhaseId.GET_DATA_COLLECTION_FILE_IN_BACKGROUND, hashMap, (String) null, 16, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(hashMap, "options");
    }

    @DexIgnore
    public void a(Priority priority) {
        wd4.b(priority, "<set-?>");
        this.R = priority;
    }

    @DexIgnore
    public Priority m() {
        return this.R;
    }
}
