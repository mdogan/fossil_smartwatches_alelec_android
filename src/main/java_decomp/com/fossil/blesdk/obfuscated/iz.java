package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface iz {
    @DexIgnore
    void a();

    @DexIgnore
    void a(long j, String str);

    @DexIgnore
    py b();

    @DexIgnore
    byte[] c();

    @DexIgnore
    void d();
}
