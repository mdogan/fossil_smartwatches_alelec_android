package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mr3 implements Factory<lr3> {
    @DexIgnore
    public /* final */ Provider<fn2> a;

    @DexIgnore
    public mr3(Provider<fn2> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static mr3 a(Provider<fn2> provider) {
        return new mr3(provider);
    }

    @DexIgnore
    public static lr3 b(Provider<fn2> provider) {
        return new lr3(provider.get());
    }

    @DexIgnore
    public lr3 get() {
        return b(this.a);
    }
}
