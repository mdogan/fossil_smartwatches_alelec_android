package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.remote.ShortcutApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n52 implements Factory<ShortcutApiService> {
    @DexIgnore
    public /* final */ o42 a;
    @DexIgnore
    public /* final */ Provider<yo2> b;
    @DexIgnore
    public /* final */ Provider<cp2> c;

    @DexIgnore
    public n52(o42 o42, Provider<yo2> provider, Provider<cp2> provider2) {
        this.a = o42;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static n52 a(o42 o42, Provider<yo2> provider, Provider<cp2> provider2) {
        return new n52(o42, provider, provider2);
    }

    @DexIgnore
    public static ShortcutApiService b(o42 o42, Provider<yo2> provider, Provider<cp2> provider2) {
        return a(o42, provider.get(), provider2.get());
    }

    @DexIgnore
    public static ShortcutApiService a(o42 o42, yo2 yo2, cp2 cp2) {
        ShortcutApiService d = o42.d(yo2, cp2);
        o44.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }

    @DexIgnore
    public ShortcutApiService get() {
        return b(this.a, this.b, this.c);
    }
}
