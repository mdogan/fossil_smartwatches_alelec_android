package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p63 {
    @DexIgnore
    public /* final */ m63 a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public p63(m63 m63, String str, String str2) {
        wd4.b(m63, "mView");
        wd4.b(str, "mPresetId");
        wd4.b(str2, "mMicroAppPos");
        this.a = m63;
    }

    @DexIgnore
    public final m63 a() {
        return this.a;
    }
}
