package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;
import com.portfolio.platform.data.model.Range;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uo2 {
    @DexIgnore
    public List<SavedPreset> a; // = new ArrayList();
    @DexIgnore
    public Range b;

    @DexIgnore
    public void a(yz1 yz1) {
        this.a = new ArrayList();
        if (yz1.d(CloudLogWriter.ITEMS_PARAM)) {
            try {
                uz1 b2 = yz1.b(CloudLogWriter.ITEMS_PARAM);
                if (b2.size() > 0) {
                    for (int i = 0; i < b2.size(); i++) {
                        yz1 d = b2.get(i).d();
                        SavedPreset savedPreset = new SavedPreset();
                        if (d.d("id")) {
                            savedPreset.setId(d.a("id").f());
                        }
                        if (d.d("name")) {
                            savedPreset.setName(d.a("name").f());
                        }
                        if (d.d("buttons")) {
                            uz1 c = d.a("buttons").c();
                            if (c != null && c.size() > 0) {
                                savedPreset.setButtons(c.toString());
                            }
                        }
                        savedPreset.setPinType(0);
                        this.a.add(savedPreset);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (yz1.d("_range")) {
            try {
                this.b = (Range) new Gson().a(yz1.c("_range").toString(), Range.class);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    @DexIgnore
    public List<SavedPreset> b() {
        return this.a;
    }

    @DexIgnore
    public Range a() {
        return this.b;
    }
}
