package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p31 extends kk0 implements ne0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<p31> CREATOR; // = new q31();
    @DexIgnore
    public /* final */ Status e;

    /*
    static {
        new p31(Status.i);
    }
    */

    @DexIgnore
    public p31(Status status) {
        this.e = status;
    }

    @DexIgnore
    public final Status G() {
        return this.e;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, (Parcelable) G(), i, false);
        lk0.a(parcel, a);
    }
}
