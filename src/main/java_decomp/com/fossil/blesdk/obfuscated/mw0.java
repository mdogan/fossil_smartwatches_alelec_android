package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzbb;
import com.google.android.gms.internal.clearcut.zzbn;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mw0 {
    @DexIgnore
    public static /* final */ Class<?> a; // = d();
    @DexIgnore
    public static /* final */ bx0<?, ?> b; // = a(false);
    @DexIgnore
    public static /* final */ bx0<?, ?> c; // = a(true);
    @DexIgnore
    public static /* final */ bx0<?, ?> d; // = new dx0();

    @DexIgnore
    public static int a(int i, Object obj, kw0 kw0) {
        return obj instanceof cv0 ? zzbn.a(i, (cv0) obj) : zzbn.b(i, (tv0) obj, kw0);
    }

    @DexIgnore
    public static int a(int i, List<?> list) {
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        int e = zzbn.e(i) * size;
        if (list instanceof ev0) {
            ev0 ev0 = (ev0) list;
            while (i2 < size) {
                Object e2 = ev0.e(i2);
                e += e2 instanceof zzbb ? zzbn.a((zzbb) e2) : zzbn.a((String) e2);
                i2++;
            }
        } else {
            while (i2 < size) {
                Object obj = list.get(i2);
                e += obj instanceof zzbb ? zzbn.a((zzbb) obj) : zzbn.a((String) obj);
                i2++;
            }
        }
        return e;
    }

    @DexIgnore
    public static int a(int i, List<?> list, kw0 kw0) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = zzbn.e(i) * size;
        for (int i2 = 0; i2 < size; i2++) {
            Object obj = list.get(i2);
            e += obj instanceof cv0 ? zzbn.a((cv0) obj) : zzbn.a((tv0) obj, kw0);
        }
        return e;
    }

    @DexIgnore
    public static int a(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return a(list) + (list.size() * zzbn.e(i));
    }

    @DexIgnore
    public static int a(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof jv0) {
            jv0 jv0 = (jv0) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.d(jv0.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzbn.d(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static bx0<?, ?> a() {
        return b;
    }

    @DexIgnore
    public static bx0<?, ?> a(boolean z) {
        try {
            Class<?> e = e();
            if (e == null) {
                return null;
            }
            return (bx0) e.getConstructor(new Class[]{Boolean.TYPE}).newInstance(new Object[]{Boolean.valueOf(z)});
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static <UT, UB> UB a(int i, int i2, UB ub, bx0<UT, UB> bx0) {
        if (ub == null) {
            ub = bx0.a();
        }
        bx0.a(ub, i, (long) i2);
        return ub;
    }

    @DexIgnore
    public static <UT, UB> UB a(int i, List<Integer> list, wu0<?> wu0, UB ub, bx0<UT, UB> bx0) {
        UB ub2;
        if (wu0 == null) {
            return ub;
        }
        if (!(list instanceof RandomAccess)) {
            Iterator<Integer> it = list.iterator();
            loop1:
            while (true) {
                ub2 = ub;
                while (it.hasNext()) {
                    int intValue = it.next().intValue();
                    if (wu0.zzb(intValue) == null) {
                        ub = a(i, intValue, ub2, bx0);
                        it.remove();
                    }
                }
                break loop1;
            }
        } else {
            int size = list.size();
            ub2 = ub;
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue2 = list.get(i3).intValue();
                if (wu0.zzb(intValue2) != null) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue2));
                    }
                    i2++;
                } else {
                    ub2 = a(i, intValue2, ub2, bx0);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
            }
        }
        return ub2;
    }

    @DexIgnore
    public static void a(int i, List<String> list, px0 px0) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zza(i, list);
        }
    }

    @DexIgnore
    public static void a(int i, List<?> list, px0 px0, kw0 kw0) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.b(i, list, kw0);
        }
    }

    @DexIgnore
    public static void a(int i, List<Double> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zzg(i, list, z);
        }
    }

    @DexIgnore
    public static <T, UT, UB> void a(bx0<UT, UB> bx0, T t, T t2) {
        bx0.a((Object) t, bx0.c(bx0.c(t), bx0.c(t2)));
    }

    @DexIgnore
    public static <T, FT extends ou0<FT>> void a(hu0<FT> hu0, T t, T t2) {
        lu0<FT> a2 = hu0.a((Object) t2);
        if (!a2.b()) {
            hu0.b(t).a(a2);
        }
    }

    @DexIgnore
    public static <T> void a(ov0 ov0, T t, T t2, long j) {
        ix0.a((Object) t, j, ov0.zzb(ix0.f(t, j), ix0.f(t2, j)));
    }

    @DexIgnore
    public static void a(Class<?> cls) {
        if (!su0.class.isAssignableFrom(cls)) {
            Class<?> cls2 = a;
            if (cls2 != null && !cls2.isAssignableFrom(cls)) {
                throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
            }
        }
    }

    @DexIgnore
    public static boolean a(int i, int i2, int i3) {
        if (i2 < 40) {
            return true;
        }
        long j = (long) i3;
        return ((((long) i2) - ((long) i)) + 1) + 9 <= ((2 * j) + 3) + ((j + 3) * 3);
    }

    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    @DexIgnore
    public static int b(int i, List<zzbb> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = size * zzbn.e(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            e += zzbn.a(list.get(i2));
        }
        return e;
    }

    @DexIgnore
    public static int b(int i, List<tv0> list, kw0 kw0) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += zzbn.c(i, list.get(i3), kw0);
        }
        return i2;
    }

    @DexIgnore
    public static int b(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return b(list) + (size * zzbn.e(i));
    }

    @DexIgnore
    public static int b(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof jv0) {
            jv0 jv0 = (jv0) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.e(jv0.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzbn.e(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static bx0<?, ?> b() {
        return c;
    }

    @DexIgnore
    public static void b(int i, List<zzbb> list, px0 px0) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zzb(i, list);
        }
    }

    @DexIgnore
    public static void b(int i, List<?> list, px0 px0, kw0 kw0) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.a(i, list, kw0);
        }
    }

    @DexIgnore
    public static void b(int i, List<Float> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zzf(i, list, z);
        }
    }

    @DexIgnore
    public static int c(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return c(list) + (size * zzbn.e(i));
    }

    @DexIgnore
    public static int c(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof jv0) {
            jv0 jv0 = (jv0) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.f(jv0.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzbn.f(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static bx0<?, ?> c() {
        return d;
    }

    @DexIgnore
    public static void c(int i, List<Long> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zzc(i, list, z);
        }
    }

    @DexIgnore
    public static int d(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return d(list) + (size * zzbn.e(i));
    }

    @DexIgnore
    public static int d(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof tu0) {
            tu0 tu0 = (tu0) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.k(tu0.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzbn.k(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static Class<?> d() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static void d(int i, List<Long> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zzd(i, list, z);
        }
    }

    @DexIgnore
    public static int e(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return e(list) + (size * zzbn.e(i));
    }

    @DexIgnore
    public static int e(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof tu0) {
            tu0 tu0 = (tu0) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.f(tu0.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzbn.f(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static Class<?> e() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static void e(int i, List<Long> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zzn(i, list, z);
        }
    }

    @DexIgnore
    public static int f(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return f(list) + (size * zzbn.e(i));
    }

    @DexIgnore
    public static int f(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof tu0) {
            tu0 tu0 = (tu0) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.g(tu0.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzbn.g(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static void f(int i, List<Long> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zze(i, list, z);
        }
    }

    @DexIgnore
    public static int g(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return g(list) + (size * zzbn.e(i));
    }

    @DexIgnore
    public static int g(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof tu0) {
            tu0 tu0 = (tu0) list;
            i = 0;
            while (i2 < size) {
                i += zzbn.h(tu0.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzbn.h(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static void g(int i, List<Long> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zzl(i, list, z);
        }
    }

    @DexIgnore
    public static int h(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzbn.i(i, 0);
    }

    @DexIgnore
    public static int h(List<?> list) {
        return list.size() << 2;
    }

    @DexIgnore
    public static void h(int i, List<Integer> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zza(i, list, z);
        }
    }

    @DexIgnore
    public static int i(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzbn.g(i, 0);
    }

    @DexIgnore
    public static int i(List<?> list) {
        return list.size() << 3;
    }

    @DexIgnore
    public static void i(int i, List<Integer> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zzj(i, list, z);
        }
    }

    @DexIgnore
    public static int j(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzbn.b(i, true);
    }

    @DexIgnore
    public static int j(List<?> list) {
        return list.size();
    }

    @DexIgnore
    public static void j(int i, List<Integer> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zzm(i, list, z);
        }
    }

    @DexIgnore
    public static void k(int i, List<Integer> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zzb(i, list, z);
        }
    }

    @DexIgnore
    public static void l(int i, List<Integer> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zzk(i, list, z);
        }
    }

    @DexIgnore
    public static void m(int i, List<Integer> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zzh(i, list, z);
        }
    }

    @DexIgnore
    public static void n(int i, List<Boolean> list, px0 px0, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            px0.zzi(i, list, z);
        }
    }
}
