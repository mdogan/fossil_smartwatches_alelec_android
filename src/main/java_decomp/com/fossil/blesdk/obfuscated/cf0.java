package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.af0;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.ee0.b;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class cf0<A extends ee0.b, L> {
    @DexIgnore
    public /* final */ af0<L> a;
    @DexIgnore
    public /* final */ xd0[] b; // = null;
    @DexIgnore
    public /* final */ boolean c; // = false;

    @DexIgnore
    public cf0(af0<L> af0) {
        this.a = af0;
    }

    @DexIgnore
    public void a() {
        this.a.a();
    }

    @DexIgnore
    public abstract void a(A a2, yn1<Void> yn1) throws RemoteException;

    @DexIgnore
    public af0.a<L> b() {
        return this.a.b();
    }

    @DexIgnore
    public xd0[] c() {
        return this.b;
    }

    @DexIgnore
    public final boolean d() {
        return this.c;
    }
}
