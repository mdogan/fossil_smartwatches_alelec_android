package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hl4 extends fl4 {
    @DexIgnore
    public /* final */ Runnable g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hl4(Runnable runnable, long j, gl4 gl4) {
        super(j, gl4);
        wd4.b(runnable, "block");
        wd4.b(gl4, "taskContext");
        this.g = runnable;
    }

    @DexIgnore
    public void run() {
        try {
            this.g.run();
        } finally {
            this.f.A();
        }
    }

    @DexIgnore
    public String toString() {
        return "Task[" + ph4.a((Object) this.g) + '@' + ph4.b(this.g) + ", " + this.e + ", " + this.f + ']';
    }
}
