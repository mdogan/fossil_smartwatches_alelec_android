package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tz2 {
    @DexIgnore
    public /* final */ rz2 a;
    @DexIgnore
    public /* final */ LoaderManager b;

    @DexIgnore
    public tz2(rz2 rz2, LoaderManager loaderManager) {
        wd4.b(rz2, "mView");
        wd4.b(loaderManager, "mLoaderManager");
        this.a = rz2;
        this.b = loaderManager;
    }

    @DexIgnore
    public final LoaderManager a() {
        return this.b;
    }

    @DexIgnore
    public final rz2 b() {
        return this.a;
    }
}
