package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import com.fossil.blesdk.obfuscated.tn0;
import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vn0<T> extends tn0.a {
    @DexIgnore
    public /* final */ T e;

    @DexIgnore
    public vn0(T t) {
        this.e = t;
    }

    @DexIgnore
    public static <T> tn0 a(T t) {
        return new vn0(t);
    }

    @DexIgnore
    public static <T> T d(tn0 tn0) {
        if (tn0 instanceof vn0) {
            return ((vn0) tn0).e;
        }
        IBinder asBinder = tn0.asBinder();
        Field[] declaredFields = asBinder.getClass().getDeclaredFields();
        Field field = null;
        int i = 0;
        for (Field field2 : declaredFields) {
            if (!field2.isSynthetic()) {
                i++;
                field = field2;
            }
        }
        if (i != 1) {
            int length = declaredFields.length;
            StringBuilder sb = new StringBuilder(64);
            sb.append("Unexpected number of IObjectWrapper declared fields: ");
            sb.append(length);
            throw new IllegalArgumentException(sb.toString());
        } else if (!field.isAccessible()) {
            field.setAccessible(true);
            try {
                return field.get(asBinder);
            } catch (NullPointerException e2) {
                throw new IllegalArgumentException("Binder object is null.", e2);
            } catch (IllegalAccessException e3) {
                throw new IllegalArgumentException("Could not access the field in remoteBinder.", e3);
            }
        } else {
            throw new IllegalArgumentException("IObjectWrapper declared field not private!");
        }
    }
}
