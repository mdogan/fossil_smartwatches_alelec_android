package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uh4 {
    @DexIgnore
    public static final Object a(long j, kc4<? super cb4> kc4) {
        if (j <= 0) {
            return cb4.a;
        }
        qg4 qg4 = new qg4(IntrinsicsKt__IntrinsicsJvmKt.a(kc4), 1);
        a(qg4.getContext()).a(j, (pg4<? super cb4>) qg4);
        Object e = qg4.e();
        if (e == oc4.a()) {
            uc4.c(kc4);
        }
        return e;
    }

    @DexIgnore
    public static final th4 a(CoroutineContext coroutineContext) {
        wd4.b(coroutineContext, "$this$delay");
        CoroutineContext.a aVar = coroutineContext.get(lc4.b);
        if (!(aVar instanceof th4)) {
            aVar = null;
        }
        th4 th4 = (th4) aVar;
        return th4 != null ? th4 : rh4.a();
    }
}
