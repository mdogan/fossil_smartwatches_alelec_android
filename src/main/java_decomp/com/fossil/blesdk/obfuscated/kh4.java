package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kh4 extends jc4 {
    @DexIgnore
    public static /* final */ a f; // = new a((rd4) null);
    @DexIgnore
    public /* final */ String e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineContext.b<kh4> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public final String C() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof kh4) && wd4.a((Object) this.e, (Object) ((kh4) obj).e);
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.e;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "CoroutineName(" + this.e + ')';
    }
}
