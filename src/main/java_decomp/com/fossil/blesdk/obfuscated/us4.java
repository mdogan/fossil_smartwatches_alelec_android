package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class us4 implements sr4<qm4, Double> {
    @DexIgnore
    public static /* final */ us4 a; // = new us4();

    @DexIgnore
    public Double a(qm4 qm4) throws IOException {
        return Double.valueOf(qm4.F());
    }
}
