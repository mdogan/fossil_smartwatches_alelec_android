package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class yj4<T> extends kk4 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(yj4.class, Object.class, "_consensus");
    @DexIgnore
    public volatile Object _consensus; // = xj4.a;

    @DexIgnore
    public final Object a(Object obj) {
        Object obj2 = this._consensus;
        if (obj2 == xj4.a) {
            obj2 = b(c(obj));
        }
        a(obj, obj2);
        return obj2;
    }

    @DexIgnore
    public abstract void a(T t, Object obj);

    @DexIgnore
    public final Object b(Object obj) {
        return d(obj) ? obj : this._consensus;
    }

    @DexIgnore
    public abstract Object c(T t);

    @DexIgnore
    public final boolean d(Object obj) {
        if (oh4.a()) {
            if (!(obj != xj4.a)) {
                throw new AssertionError();
            }
        }
        return a.compareAndSet(this, xj4.a, obj);
    }
}
