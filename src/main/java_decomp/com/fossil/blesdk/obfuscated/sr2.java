package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.BaseActivity;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sr2 extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((rd4) null);
    @DexIgnore
    public /* final */ jn2 d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return sr2.e;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ WeakReference<BaseActivity> a;

        @DexIgnore
        public b(WeakReference<BaseActivity> weakReference) {
            wd4.b(weakReference, "activityContext");
            this.a = weakReference;
        }

        @DexIgnore
        public final WeakReference<BaseActivity> a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, vd0 vd0) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ SignUpSocialAuth a;

        @DexIgnore
        public d(SignUpSocialAuth signUpSocialAuth) {
            wd4.b(signUpSocialAuth, "auth");
            this.a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = sr2.class.getSimpleName();
        wd4.a((Object) simpleName, "LoginFacebookUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public sr2(jn2 jn2) {
        wd4.b(jn2, "mLoginFacebookManager");
        this.d = jn2;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements mn2 {
        @DexIgnore
        public /* final */ /* synthetic */ sr2 a;

        @DexIgnore
        public e(sr2 sr2) {
            this.a = sr2;
        }

        @DexIgnore
        public void a(SignUpSocialAuth signUpSocialAuth) {
            wd4.b(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = sr2.f.a();
            local.d(a2, "Inside .onLoginSuccess with result=" + signUpSocialAuth);
            this.a.a(new d(signUpSocialAuth));
        }

        @DexIgnore
        public void a(int i, vd0 vd0, String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = sr2.f.a();
            local.d(a2, "Inside .onLoginFailed with errorCode=" + i + ", connectionResult=" + vd0);
            this.a.a(new c(i, vd0));
        }
    }

    @DexIgnore
    public Object a(b bVar, kc4<Object> kc4) {
        try {
            FLogger.INSTANCE.getLocal().d(e, "running UseCase");
            if (!PortfolioApp.W.c().w()) {
                return a(new c(601, (vd0) null));
            }
            jn2 jn2 = this.d;
            WeakReference<BaseActivity> a2 = bVar != null ? bVar.a() : null;
            if (a2 != null) {
                Object obj = a2.get();
                if (obj != null) {
                    wd4.a(obj, "requestValues?.activityContext!!.get()!!");
                    jn2.a((Activity) obj, (mn2) new e(this));
                    return cb4.a;
                }
                wd4.a();
                throw null;
            }
            wd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "Inside .run failed with exception=" + e2);
            return new c(600, (vd0) null);
        }
    }
}
