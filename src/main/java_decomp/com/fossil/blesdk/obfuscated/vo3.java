package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.usecase.RequestEmailOtp;
import com.portfolio.platform.usecase.VerifyEmailOtp;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vo3 implements MembersInjector<to3> {
    @DexIgnore
    public static void a(to3 to3, RequestEmailOtp requestEmailOtp) {
        to3.f = requestEmailOtp;
    }

    @DexIgnore
    public static void a(to3 to3, VerifyEmailOtp verifyEmailOtp) {
        to3.g = verifyEmailOtp;
    }

    @DexIgnore
    public static void a(to3 to3) {
        to3.m();
    }
}
