package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zzuv;
import com.google.android.gms.internal.measurement.zzxx;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class da1<T> implements oa1<T> {
    @DexIgnore
    public /* final */ x91 a;
    @DexIgnore
    public /* final */ fb1<?, ?> b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ k81<?> d;

    @DexIgnore
    public da1(fb1<?, ?> fb1, k81<?> k81, x91 x91) {
        this.b = fb1;
        this.c = k81.a(x91);
        this.d = k81;
        this.a = x91;
    }

    @DexIgnore
    public static <T> da1<T> a(fb1<?, ?> fb1, k81<?> k81, x91 x91) {
        return new da1<>(fb1, k81, x91);
    }

    @DexIgnore
    public final void b(T t, T t2) {
        qa1.a(this.b, t, t2);
        if (this.c) {
            qa1.a(this.d, t, t2);
        }
    }

    @DexIgnore
    public final boolean c(T t) {
        return this.d.a((Object) t).d();
    }

    @DexIgnore
    public final void d(T t) {
        this.b.f(t);
        this.d.c(t);
    }

    @DexIgnore
    public final T a() {
        return this.a.f().u();
    }

    @DexIgnore
    public final boolean a(T t, T t2) {
        if (!this.b.c(t).equals(this.b.c(t2))) {
            return false;
        }
        if (this.c) {
            return this.d.a((Object) t).equals(this.d.a((Object) t2));
        }
        return true;
    }

    @DexIgnore
    public final int b(T t) {
        fb1<?, ?> fb1 = this.b;
        int e = fb1.e(fb1.c(t)) + 0;
        return this.c ? e + this.d.a((Object) t).h() : e;
    }

    @DexIgnore
    public final int a(T t) {
        int hashCode = this.b.c(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.a((Object) t).hashCode() : hashCode;
    }

    @DexIgnore
    public final void a(T t, tb1 tb1) throws IOException {
        Iterator<Map.Entry<?, Object>> e = this.d.a((Object) t).e();
        while (e.hasNext()) {
            Map.Entry next = e.next();
            p81 p81 = (p81) next.getKey();
            if (p81.h() != zzxx.MESSAGE || p81.g() || p81.e()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof e91) {
                tb1.zza(p81.zzc(), (Object) ((e91) next).a().a());
            } else {
                tb1.zza(p81.zzc(), next.getValue());
            }
        }
        fb1<?, ?> fb1 = this.b;
        fb1.b(fb1.c(t), tb1);
    }

    @DexIgnore
    public final void a(T t, na1 na1, j81 j81) throws IOException {
        boolean z;
        fb1<?, ?> fb1 = this.b;
        k81<?> k81 = this.d;
        Object d2 = fb1.d(t);
        n81<?> b2 = k81.b(t);
        do {
            try {
                if (na1.j() == Integer.MAX_VALUE) {
                    fb1.b((Object) t, d2);
                    return;
                }
                int a2 = na1.a();
                if (a2 == 11) {
                    Object obj = null;
                    zzte zzte = null;
                    int i = 0;
                    while (na1.j() != Integer.MAX_VALUE) {
                        int a3 = na1.a();
                        if (a3 == 16) {
                            i = na1.b();
                            obj = k81.a(j81, this.a, i);
                        } else if (a3 == 26) {
                            if (obj == null) {
                                zzte = na1.d();
                            } else {
                                k81.a(na1, obj, j81, b2);
                                throw null;
                            }
                        } else if (!na1.n()) {
                            break;
                        }
                    }
                    if (na1.a() == 12) {
                        if (zzte != null) {
                            if (obj == null) {
                                fb1.a(d2, i, zzte);
                            } else {
                                k81.a(zzte, obj, j81, b2);
                                throw null;
                            }
                        }
                        z = true;
                        continue;
                    } else {
                        throw zzuv.zzwt();
                    }
                } else if ((a2 & 7) == 2) {
                    Object a4 = k81.a(j81, this.a, a2 >>> 3);
                    if (a4 == null) {
                        z = fb1.a(d2, na1);
                        continue;
                    } else {
                        k81.a(na1, a4, j81, b2);
                        throw null;
                    }
                } else {
                    z = na1.n();
                    continue;
                }
            } finally {
                fb1.b((Object) t, d2);
            }
        } while (z);
    }
}
