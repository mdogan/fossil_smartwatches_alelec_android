package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ou0;
import com.google.android.gms.internal.clearcut.zzfl;
import com.google.android.gms.internal.clearcut.zzfq;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ou0<T extends ou0<T>> extends Comparable<T> {
    @DexIgnore
    uv0 a(uv0 uv0, tv0 tv0);

    @DexIgnore
    zv0 a(zv0 zv0, zv0 zv02);

    @DexIgnore
    boolean a();

    @DexIgnore
    zzfq b();

    @DexIgnore
    boolean c();

    @DexIgnore
    zzfl d();

    @DexIgnore
    int zzc();
}
