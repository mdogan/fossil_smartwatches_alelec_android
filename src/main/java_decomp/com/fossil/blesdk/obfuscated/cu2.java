package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.alarm.Alarm;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cu2 implements Factory<ArrayList<Alarm>> {
    @DexIgnore
    public static ArrayList<Alarm> a(bu2 bu2) {
        ArrayList<Alarm> b = bu2.b();
        o44.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
