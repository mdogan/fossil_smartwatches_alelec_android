package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hn1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<hn1> CREATOR; // = new in1();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ vd0 f;
    @DexIgnore
    public /* final */ ek0 g;

    @DexIgnore
    public hn1(int i, vd0 vd0, ek0 ek0) {
        this.e = i;
        this.f = vd0;
        this.g = ek0;
    }

    @DexIgnore
    public final vd0 H() {
        return this.f;
    }

    @DexIgnore
    public final ek0 I() {
        return this.g;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, (Parcelable) this.f, i, false);
        lk0.a(parcel, 3, (Parcelable) this.g, i, false);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public hn1(int i) {
        this(new vd0(8, (PendingIntent) null), (ek0) null);
    }

    @DexIgnore
    public hn1(vd0 vd0, ek0 ek0) {
        this(1, vd0, (ek0) null);
    }
}
