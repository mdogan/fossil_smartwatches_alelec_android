package com.fossil.blesdk.obfuscated;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qr0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ nr0 e;
    @DexIgnore
    public /* final */ /* synthetic */ pr0 f;

    @DexIgnore
    public qr0(pr0 pr0, nr0 nr0) {
        this.f = pr0;
        this.e = nr0;
    }

    @DexIgnore
    public final void run() {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "bg processing of the intent starting now");
        }
        this.f.e.handleIntent(this.e.a);
        this.e.a();
    }
}
