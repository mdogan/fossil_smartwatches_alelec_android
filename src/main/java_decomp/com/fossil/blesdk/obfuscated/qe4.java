package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ie4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qe4 extends pe4 {
    @DexIgnore
    public static final int a(int i, int i2) {
        return i < i2 ? i2 : i;
    }

    @DexIgnore
    public static final long a(long j, long j2) {
        return j < j2 ? j2 : j;
    }

    @DexIgnore
    public static final ie4 a(ie4 ie4, int i) {
        wd4.b(ie4, "$this$step");
        pe4.a(i > 0, Integer.valueOf(i));
        ie4.a aVar = ie4.h;
        int a = ie4.a();
        int b = ie4.b();
        if (ie4.c() <= 0) {
            i = -i;
        }
        return aVar.a(a, b, i);
    }

    @DexIgnore
    public static final int b(int i, int i2) {
        return i > i2 ? i2 : i;
    }

    @DexIgnore
    public static final long b(long j, long j2) {
        return j > j2 ? j2 : j;
    }

    @DexIgnore
    public static final ie4 c(int i, int i2) {
        return ie4.h.a(i, i2, -1);
    }

    @DexIgnore
    public static final ke4 d(int i, int i2) {
        if (i2 <= Integer.MIN_VALUE) {
            return ke4.j.a();
        }
        return new ke4(i, i2 - 1);
    }

    @DexIgnore
    public static final ne4 a(long j, int i) {
        return new ne4(j, ((long) i) - 1);
    }

    @DexIgnore
    public static final int a(int i, int i2, int i3) {
        if (i2 > i3) {
            throw new IllegalArgumentException("Cannot coerce value to an empty range: maximum " + i3 + " is less than minimum " + i2 + '.');
        } else if (i < i2) {
            return i2;
        } else {
            return i > i3 ? i3 : i;
        }
    }
}
