package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ek4;
import com.fossil.blesdk.obfuscated.pg4;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Result;
import kotlin.TypeCastException;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ql4 implements pl4, nl4<Object, pl4> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(ql4.class, Object.class, "_state");
    @DexIgnore
    public volatile Object _state;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends b {
        @DexIgnore
        public /* final */ pg4<cb4> i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Object obj, pg4<? super cb4> pg4) {
            super(obj);
            wd4.b(pg4, "cont");
            this.i = pg4;
        }

        @DexIgnore
        public void a(Object obj) {
            wd4.b(obj, "token");
            this.i.b(obj);
        }

        @DexIgnore
        public Object m() {
            return pg4.a.a(this.i, cb4.a, (Object) null, 2, (Object) null);
        }

        @DexIgnore
        public String toString() {
            return "LockCont[" + this.h + ", " + this.i + ']';
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b extends ek4 implements ai4 {
        @DexIgnore
        public /* final */ Object h;

        @DexIgnore
        public b(Object obj) {
            this.h = obj;
        }

        @DexIgnore
        public abstract void a(Object obj);

        @DexIgnore
        public final void dispose() {
            j();
        }

        @DexIgnore
        public abstract Object m();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ck4 {
        @DexIgnore
        public Object h;

        @DexIgnore
        public c(Object obj) {
            wd4.b(obj, "owner");
            this.h = obj;
        }

        @DexIgnore
        public String toString() {
            return "LockedQueue[" + this.h + ']';
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends kk4 {
        @DexIgnore
        public /* final */ c a;

        @DexIgnore
        public d(c cVar) {
            wd4.b(cVar, "queue");
            this.a = cVar;
        }

        @DexIgnore
        public Object a(Object obj) {
            Object b = this.a.m() ? rl4.e : this.a;
            if (obj != null) {
                ql4 ql4 = (ql4) obj;
                ql4.a.compareAndSet(ql4, this, b);
                if (ql4._state == this.a) {
                    return rl4.a;
                }
                return null;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.sync.MutexImpl");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends ek4.a {
        @DexIgnore
        public /* final */ /* synthetic */ Object d;
        @DexIgnore
        public /* final */ /* synthetic */ ql4 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ek4 ek4, ek4 ek42, Object obj, pg4 pg4, a aVar, ql4 ql4, Object obj2) {
            super(ek42);
            this.d = obj;
            this.e = ql4;
        }

        @DexIgnore
        /* renamed from: a */
        public Object c(ek4 ek4) {
            wd4.b(ek4, "affected");
            if (this.e._state == this.d) {
                return null;
            }
            return dk4.a();
        }
    }

    @DexIgnore
    public ql4(boolean z) {
        this._state = z ? rl4.d : rl4.e;
    }

    @DexIgnore
    public Object a(Object obj, kc4<? super cb4> kc4) {
        if (b(obj)) {
            return cb4.a;
        }
        return b(obj, kc4);
    }

    @DexIgnore
    public boolean b(Object obj) {
        while (true) {
            Object obj2 = this._state;
            boolean z = true;
            if (obj2 instanceof ol4) {
                if (((ol4) obj2).a != rl4.c) {
                    return false;
                }
                if (a.compareAndSet(this, obj2, obj == null ? rl4.d : new ol4(obj))) {
                    return true;
                }
            } else if (obj2 instanceof c) {
                if (((c) obj2).h == obj) {
                    z = false;
                }
                if (z) {
                    return false;
                }
                throw new IllegalStateException(("Already locked by " + obj).toString());
            } else if (obj2 instanceof kk4) {
                ((kk4) obj2).a(this);
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
    }

    @DexIgnore
    public String toString() {
        while (true) {
            Object obj = this._state;
            if (obj instanceof ol4) {
                return "Mutex[" + ((ol4) obj).a + ']';
            } else if (obj instanceof kk4) {
                ((kk4) obj).a(this);
            } else if (obj instanceof c) {
                return "Mutex[" + ((c) obj).h + ']';
            } else {
                throw new IllegalStateException(("Illegal state " + obj).toString());
            }
        }
    }

    @DexIgnore
    public void a(Object obj) {
        while (true) {
            Object obj2 = this._state;
            boolean z = true;
            if (obj2 instanceof ol4) {
                if (obj == null) {
                    if (((ol4) obj2).a == rl4.c) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalStateException("Mutex is not locked".toString());
                    }
                } else {
                    ol4 ol4 = (ol4) obj2;
                    if (ol4.a != obj) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalStateException(("Mutex is locked by " + ol4.a + " but expected " + obj).toString());
                    }
                }
                if (a.compareAndSet(this, obj2, rl4.e)) {
                    return;
                }
            } else if (obj2 instanceof kk4) {
                ((kk4) obj2).a(this);
            } else if (obj2 instanceof c) {
                if (obj != null) {
                    c cVar = (c) obj2;
                    if (cVar.h != obj) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalStateException(("Mutex is locked by " + cVar.h + " but expected " + obj).toString());
                    }
                }
                c cVar2 = (c) obj2;
                ek4 k = cVar2.k();
                if (k == null) {
                    d dVar = new d(cVar2);
                    if (a.compareAndSet(this, obj2, dVar) && dVar.a(this) == null) {
                        return;
                    }
                } else {
                    b bVar = (b) k;
                    Object m = bVar.m();
                    if (m != null) {
                        Object obj3 = bVar.h;
                        if (obj3 == null) {
                            obj3 = rl4.b;
                        }
                        cVar2.h = obj3;
                        bVar.a(m);
                        return;
                    }
                }
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
    }

    @DexIgnore
    public final /* synthetic */ Object b(Object obj, kc4<? super cb4> kc4) {
        Object obj2 = obj;
        qg4 qg4 = new qg4(IntrinsicsKt__IntrinsicsJvmKt.a(kc4), 0);
        a aVar = new a(obj2, qg4);
        while (true) {
            Object obj3 = this._state;
            if (obj3 instanceof ol4) {
                ol4 ol4 = (ol4) obj3;
                if (ol4.a != rl4.c) {
                    a.compareAndSet(this, obj3, new c(ol4.a));
                } else {
                    if (a.compareAndSet(this, obj3, obj2 == null ? rl4.d : new ol4(obj2))) {
                        cb4 cb4 = cb4.a;
                        Result.a aVar2 = Result.Companion;
                        qg4.resumeWith(Result.m3constructorimpl(cb4));
                        break;
                    }
                }
            } else if (obj3 instanceof c) {
                c cVar = (c) obj3;
                boolean z = true;
                if (cVar.h != obj2) {
                    e eVar = new e(aVar, aVar, obj3, qg4, aVar, this, obj);
                    while (true) {
                        Object e2 = cVar.e();
                        if (e2 != null) {
                            int a2 = ((ek4) e2).a(aVar, cVar, eVar);
                            if (a2 != 1) {
                                if (a2 == 2) {
                                    z = false;
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                        }
                    }
                    if (z) {
                        rg4.a((pg4<?>) qg4, (ek4) aVar);
                        break;
                    }
                } else {
                    throw new IllegalStateException(("Already locked by " + obj2).toString());
                }
            } else if (obj3 instanceof kk4) {
                ((kk4) obj3).a(this);
            } else {
                throw new IllegalStateException(("Illegal state " + obj3).toString());
            }
        }
        Object e3 = qg4.e();
        if (e3 == oc4.a()) {
            uc4.c(kc4);
        }
        return e3;
    }
}
