package com.fossil.blesdk.obfuscated;

import android.util.Log;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sa extends oa {
    @DexIgnore
    public Set<Class<? extends oa>> a; // = new HashSet();
    @DexIgnore
    public List<oa> b; // = new CopyOnWriteArrayList();
    @DexIgnore
    public List<String> c; // = new CopyOnWriteArrayList();

    @DexIgnore
    public void a(oa oaVar) {
        if (this.a.add(oaVar.getClass())) {
            this.b.add(oaVar);
            for (oa a2 : oaVar.a()) {
                a(a2);
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        boolean z = false;
        for (String next : this.c) {
            try {
                Class<?> cls = Class.forName(next);
                if (oa.class.isAssignableFrom(cls)) {
                    a((oa) cls.newInstance());
                    this.c.remove(next);
                    z = true;
                }
            } catch (ClassNotFoundException unused) {
            } catch (IllegalAccessException e) {
                Log.e("MergedDataBinderMapper", "unable to add feature mapper for " + next, e);
            } catch (InstantiationException e2) {
                Log.e("MergedDataBinderMapper", "unable to add feature mapper for " + next, e2);
            }
        }
        return z;
    }

    @DexIgnore
    public ViewDataBinding a(qa qaVar, View view, int i) {
        for (oa a2 : this.b) {
            ViewDataBinding a3 = a2.a(qaVar, view, i);
            if (a3 != null) {
                return a3;
            }
        }
        if (b()) {
            return a(qaVar, view, i);
        }
        return null;
    }

    @DexIgnore
    public ViewDataBinding a(qa qaVar, View[] viewArr, int i) {
        for (oa a2 : this.b) {
            ViewDataBinding a3 = a2.a(qaVar, viewArr, i);
            if (a3 != null) {
                return a3;
            }
        }
        if (b()) {
            return a(qaVar, viewArr, i);
        }
        return null;
    }
}
