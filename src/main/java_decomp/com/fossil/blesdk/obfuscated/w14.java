package com.fossil.blesdk.obfuscated;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class w14 {
    @DexIgnore
    public static y14 c;
    @DexIgnore
    public static u14 d; // = f24.b();
    @DexIgnore
    public static JSONObject e; // = new JSONObject();
    @DexIgnore
    public Integer a; // = null;
    @DexIgnore
    public String b; // = null;

    @DexIgnore
    public w14(Context context) {
        try {
            a(context);
            this.a = f24.p(context.getApplicationContext());
            this.b = u04.a(context).b();
        } catch (Throwable th) {
            d.a(th);
        }
    }

    @DexIgnore
    public static synchronized y14 a(Context context) {
        y14 y14;
        synchronized (w14.class) {
            if (c == null) {
                c = new y14(context.getApplicationContext());
            }
            y14 = c;
        }
        return y14;
    }

    @DexIgnore
    public void a(JSONObject jSONObject, Thread thread) {
        String str;
        Object obj;
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (c != null) {
                c.a(jSONObject2, thread);
            }
            k24.a(jSONObject2, "cn", this.b);
            if (this.a != null) {
                jSONObject2.put("tn", this.a);
            }
            if (thread == null) {
                str = "ev";
                obj = jSONObject2;
            } else {
                str = "errkv";
                obj = jSONObject2.toString();
            }
            jSONObject.put(str, obj);
            if (e != null && e.length() > 0) {
                jSONObject.put("eva", e);
            }
        } catch (Throwable th) {
            d.a(th);
        }
    }
}
