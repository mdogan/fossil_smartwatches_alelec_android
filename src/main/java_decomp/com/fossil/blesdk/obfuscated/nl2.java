package com.fossil.blesdk.obfuscated;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nl2 {
    /*
    static {
        TimeUnit.DAYS.toMillis(1);
    }
    */

    @DexIgnore
    public static Date a(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        return instance.getTime();
    }

    @DexIgnore
    public static int a() {
        return TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000;
    }

    @DexIgnore
    public static String a(int i) {
        int i2 = i == 0 ? 12 : i % 12;
        String str = i >= 12 ? "p" : "a";
        return i2 + str;
    }
}
