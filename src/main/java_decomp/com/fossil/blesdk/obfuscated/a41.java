package com.fossil.blesdk.obfuscated;

import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a41 extends vd1 {
    @DexIgnore
    public /* final */ af0<rc1> e;

    @DexIgnore
    public a41(af0<rc1> af0) {
        this.e = af0;
    }

    @DexIgnore
    public final void a(LocationAvailability locationAvailability) {
        this.e.a(new c41(this, locationAvailability));
    }

    @DexIgnore
    public final void a(LocationResult locationResult) {
        this.e.a(new b41(this, locationResult));
    }

    @DexIgnore
    public final synchronized void o() {
        this.e.a();
    }
}
