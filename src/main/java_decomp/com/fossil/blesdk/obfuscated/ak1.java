package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ak1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ rj1 e;
    @DexIgnore
    public /* final */ /* synthetic */ wj1 f;

    @DexIgnore
    public ak1(wj1 wj1, rj1 rj1) {
        this.f = wj1;
        this.e = rj1;
    }

    @DexIgnore
    public final void run() {
        lg1 d = this.f.d;
        if (d == null) {
            this.f.d().s().a("Failed to send current screen to service");
            return;
        }
        try {
            if (this.e == null) {
                d.a(0, (String) null, (String) null, this.f.getContext().getPackageName());
            } else {
                d.a(this.e.c, this.e.a, this.e.b, this.f.getContext().getPackageName());
            }
            this.f.C();
        } catch (RemoteException e2) {
            this.f.d().s().a("Failed to send current screen to the service", e2);
        }
    }
}
