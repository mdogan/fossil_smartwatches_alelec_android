package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zh implements bi {
    @DexIgnore
    public a a;

    @DexIgnore
    public zh(Context context, ViewGroup viewGroup, View view) {
        this.a = new a(context, viewGroup, view, this);
    }

    @DexIgnore
    public static zh c(View view) {
        ViewGroup d = d(view);
        if (d == null) {
            return null;
        }
        int childCount = d.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = d.getChildAt(i);
            if (childAt instanceof a) {
                return ((a) childAt).h;
            }
        }
        return new th(d.getContext(), d, view);
    }

    @DexIgnore
    public static ViewGroup d(View view) {
        while (view != null) {
            if (view.getId() == 16908290 && (view instanceof ViewGroup)) {
                return (ViewGroup) view;
            }
            if (view.getParent() instanceof ViewGroup) {
                view = (ViewGroup) view.getParent();
            }
        }
        return null;
    }

    @DexIgnore
    public void a(Drawable drawable) {
        this.a.a(drawable);
    }

    @DexIgnore
    public void b(Drawable drawable) {
        this.a.b(drawable);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ViewGroup {
        @DexIgnore
        public ViewGroup e;
        @DexIgnore
        public View f;
        @DexIgnore
        public ArrayList<Drawable> g; // = null;
        @DexIgnore
        public zh h;

        /*
        static {
            Class<ViewGroup> cls = ViewGroup.class;
            try {
                cls.getDeclaredMethod("invalidateChildInParentFast", new Class[]{Integer.TYPE, Integer.TYPE, Rect.class});
            } catch (NoSuchMethodException unused) {
            }
        }
        */

        @DexIgnore
        public a(Context context, ViewGroup viewGroup, View view, zh zhVar) {
            super(context);
            this.e = viewGroup;
            this.f = view;
            setRight(viewGroup.getWidth());
            setBottom(viewGroup.getHeight());
            viewGroup.addView(this);
            this.h = zhVar;
        }

        @DexIgnore
        public void a(Drawable drawable) {
            if (this.g == null) {
                this.g = new ArrayList<>();
            }
            if (!this.g.contains(drawable)) {
                this.g.add(drawable);
                invalidate(drawable.getBounds());
                drawable.setCallback(this);
            }
        }

        @DexIgnore
        public void b(Drawable drawable) {
            ArrayList<Drawable> arrayList = this.g;
            if (arrayList != null) {
                arrayList.remove(drawable);
                invalidate(drawable.getBounds());
                drawable.setCallback((Drawable.Callback) null);
            }
        }

        @DexIgnore
        public void dispatchDraw(Canvas canvas) {
            int[] iArr = new int[2];
            int[] iArr2 = new int[2];
            this.e.getLocationOnScreen(iArr);
            this.f.getLocationOnScreen(iArr2);
            canvas.translate((float) (iArr2[0] - iArr[0]), (float) (iArr2[1] - iArr[1]));
            canvas.clipRect(new Rect(0, 0, this.f.getWidth(), this.f.getHeight()));
            super.dispatchDraw(canvas);
            ArrayList<Drawable> arrayList = this.g;
            int size = arrayList == null ? 0 : arrayList.size();
            for (int i = 0; i < size; i++) {
                this.g.get(i).draw(canvas);
            }
        }

        @DexIgnore
        public boolean dispatchTouchEvent(MotionEvent motionEvent) {
            return false;
        }

        @DexIgnore
        public ViewParent invalidateChildInParent(int[] iArr, Rect rect) {
            if (this.e == null) {
                return null;
            }
            rect.offset(iArr[0], iArr[1]);
            if (this.e instanceof ViewGroup) {
                iArr[0] = 0;
                iArr[1] = 0;
                int[] iArr2 = new int[2];
                a(iArr2);
                rect.offset(iArr2[0], iArr2[1]);
                return super.invalidateChildInParent(iArr, rect);
            }
            invalidate(rect);
            return null;
        }

        @DexIgnore
        public void invalidateDrawable(Drawable drawable) {
            invalidate(drawable.getBounds());
        }

        @DexIgnore
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        }

        @DexIgnore
        public boolean verifyDrawable(Drawable drawable) {
            if (!super.verifyDrawable(drawable)) {
                ArrayList<Drawable> arrayList = this.g;
                if (arrayList == null || !arrayList.contains(drawable)) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public void b(View view) {
            super.removeView(view);
            if (a()) {
                this.e.removeView(this);
            }
        }

        @DexIgnore
        public void a(View view) {
            if (view.getParent() instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view.getParent();
                if (!(viewGroup == this.e || viewGroup.getParent() == null || !g9.y(viewGroup))) {
                    int[] iArr = new int[2];
                    int[] iArr2 = new int[2];
                    viewGroup.getLocationOnScreen(iArr);
                    this.e.getLocationOnScreen(iArr2);
                    g9.c(view, iArr[0] - iArr2[0]);
                    g9.d(view, iArr[1] - iArr2[1]);
                }
                viewGroup.removeView(view);
                if (view.getParent() != null) {
                    viewGroup.removeView(view);
                }
            }
            super.addView(view, getChildCount() - 1);
        }

        @DexIgnore
        public boolean a() {
            if (getChildCount() == 0) {
                ArrayList<Drawable> arrayList = this.g;
                if (arrayList == null || arrayList.size() == 0) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public final void a(int[] iArr) {
            int[] iArr2 = new int[2];
            int[] iArr3 = new int[2];
            this.e.getLocationOnScreen(iArr2);
            this.f.getLocationOnScreen(iArr3);
            iArr[0] = iArr3[0] - iArr2[0];
            iArr[1] = iArr3[1] - iArr2[1];
        }
    }
}
