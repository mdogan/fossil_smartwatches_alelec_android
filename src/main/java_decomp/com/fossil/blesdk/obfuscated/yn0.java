package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.os.Bundle;
import com.fossil.blesdk.obfuscated.sn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yn0 implements sn0.a {
    @DexIgnore
    public /* final */ /* synthetic */ Activity a;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle b;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle c;
    @DexIgnore
    public /* final */ /* synthetic */ sn0 d;

    @DexIgnore
    public yn0(sn0 sn0, Activity activity, Bundle bundle, Bundle bundle2) {
        this.d = sn0;
        this.a = activity;
        this.b = bundle;
        this.c = bundle2;
    }

    @DexIgnore
    public final void a(un0 un0) {
        this.d.a.a(this.a, this.b, this.c);
    }

    @DexIgnore
    public final int getState() {
        return 0;
    }
}
