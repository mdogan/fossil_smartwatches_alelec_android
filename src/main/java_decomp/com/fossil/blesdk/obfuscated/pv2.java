package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pv2 extends ya implements View.OnClickListener {
    @DexIgnore
    public static /* final */ a k; // = new a((rd4) null);
    @DexIgnore
    public b e;
    @DexIgnore
    public String f;
    @DexIgnore
    public FlexibleTextView g;
    @DexIgnore
    public FlexibleEditText h;
    @DexIgnore
    public ImageView i;
    @DexIgnore
    public HashMap j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final pv2 a(String str, b bVar) {
            wd4.b(bVar, "listener");
            pv2 pv2 = new pv2();
            pv2.e = bVar;
            pv2.f = str;
            return pv2;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str);

        @DexIgnore
        void onCancel();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ pv2 e;

        @DexIgnore
        public c(pv2 pv2) {
            this.e = pv2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            wd4.b(editable, "text");
            pv2 pv2 = this.e;
            String obj = editable.toString();
            int length = obj.length() - 1;
            int i = 0;
            boolean z = false;
            while (i <= length) {
                boolean z2 = obj.charAt(!z ? i : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            pv2.f = obj.subSequence(i, length + 1).toString();
            boolean z3 = !TextUtils.isEmpty(this.e.f);
            pv2.a(this.e).setEnabled(z3);
            if (z3) {
                pv2.b(this.e).setVisibility(0);
            } else {
                pv2.b(this.e).setVisibility(8);
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wd4.b(charSequence, "text");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wd4.b(charSequence, "text");
        }
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView a(pv2 pv2) {
        FlexibleTextView flexibleTextView = pv2.g;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        wd4.d("ftvRename");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ImageView b(pv2 pv2) {
        ImageView imageView = pv2.i;
        if (imageView != null) {
            return imageView;
        }
        wd4.d("ivClearName");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void O(boolean z) {
        b bVar = this.e;
        if (bVar != null) {
            if (z) {
                bVar.onCancel();
            } else {
                String str = this.f;
                if (str != null) {
                    bVar.a(str);
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }
        dismiss();
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == R.id.ftv_cancel) {
                O(true);
            } else if (id != R.id.ftv_rename) {
                if (id == R.id.iv_clear_name) {
                    FlexibleEditText flexibleEditText = this.h;
                    if (flexibleEditText != null) {
                        flexibleEditText.setText("");
                    } else {
                        wd4.d("fetRename");
                        throw null;
                    }
                }
            } else if (!TextUtils.isEmpty(this.f)) {
                O(false);
            }
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, 16973830);
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        Dialog onCreateDialog = super.onCreateDialog(bundle);
        wd4.a((Object) onCreateDialog, "super.onCreateDialog(savedInstanceState)");
        onCreateDialog.requestWindowFeature(1);
        Window window = onCreateDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setLayout(-1, -1);
        }
        return onCreateDialog;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.layout.fragment_rename_preset, viewGroup);
        View findViewById = inflate.findViewById(R.id.fet_rename);
        wd4.a((Object) findViewById, "view.findViewById(R.id.fet_rename)");
        this.h = (FlexibleEditText) findViewById;
        View findViewById2 = inflate.findViewById(R.id.ftv_rename);
        wd4.a((Object) findViewById2, "view.findViewById(R.id.ftv_rename)");
        this.g = (FlexibleTextView) findViewById2;
        View findViewById3 = inflate.findViewById(R.id.iv_clear_name);
        wd4.a((Object) findViewById3, "view.findViewById(R.id.iv_clear_name)");
        this.i = (ImageView) findViewById3;
        String str = this.f;
        if (str != null) {
            FlexibleEditText flexibleEditText = this.h;
            if (flexibleEditText != null) {
                flexibleEditText.setText(str);
                FlexibleEditText flexibleEditText2 = this.h;
                if (flexibleEditText2 != null) {
                    flexibleEditText2.setSelection(str.length());
                    if (str.length() == 0) {
                        ImageView imageView = this.i;
                        if (imageView != null) {
                            imageView.setVisibility(8);
                        } else {
                            wd4.d("ivClearName");
                            throw null;
                        }
                    } else {
                        ImageView imageView2 = this.i;
                        if (imageView2 != null) {
                            imageView2.setVisibility(0);
                        } else {
                            wd4.d("ivClearName");
                            throw null;
                        }
                    }
                } else {
                    wd4.d("fetRename");
                    throw null;
                }
            } else {
                wd4.d("fetRename");
                throw null;
            }
        }
        FlexibleEditText flexibleEditText3 = this.h;
        if (flexibleEditText3 != null) {
            flexibleEditText3.addTextChangedListener(new c(this));
            ImageView imageView3 = this.i;
            if (imageView3 != null) {
                imageView3.setOnClickListener(this);
                FlexibleTextView flexibleTextView = this.g;
                if (flexibleTextView != null) {
                    flexibleTextView.setOnClickListener(this);
                    inflate.findViewById(R.id.ftv_cancel).setOnClickListener(this);
                    return inflate;
                }
                wd4.d("ftvRename");
                throw null;
            }
            wd4.d("ivClearName");
            throw null;
        }
        wd4.d("fetRename");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }
}
