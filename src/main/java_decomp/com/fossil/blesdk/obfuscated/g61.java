package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzfe$zzb;
import com.google.android.gms.internal.measurement.zztv;
import com.misfit.frameworks.common.enums.Action;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g61 extends wb1<g61> {
    @DexIgnore
    public static volatile g61[] R;
    @DexIgnore
    public String A; // = null;
    @DexIgnore
    public Boolean B; // = null;
    @DexIgnore
    public b61[] C; // = b61.e();
    @DexIgnore
    public String D; // = null;
    @DexIgnore
    public Integer E; // = null;
    @DexIgnore
    public Integer F; // = null;
    @DexIgnore
    public Integer G; // = null;
    @DexIgnore
    public String H; // = null;
    @DexIgnore
    public Long I; // = null;
    @DexIgnore
    public Long J; // = null;
    @DexIgnore
    public String K; // = null;
    @DexIgnore
    public String L; // = null;
    @DexIgnore
    public Integer M; // = null;
    @DexIgnore
    public String N; // = null;
    @DexIgnore
    public zzfe$zzb O; // = null;
    @DexIgnore
    public int[] P; // = ec1.a;
    @DexIgnore
    public Long Q; // = null;
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public d61[] d; // = d61.e();
    @DexIgnore
    public j61[] e; // = j61.e();
    @DexIgnore
    public Long f; // = null;
    @DexIgnore
    public Long g; // = null;
    @DexIgnore
    public Long h; // = null;
    @DexIgnore
    public Long i; // = null;
    @DexIgnore
    public Long j; // = null;
    @DexIgnore
    public String k; // = null;
    @DexIgnore
    public String l; // = null;
    @DexIgnore
    public String m; // = null;
    @DexIgnore
    public String n; // = null;
    @DexIgnore
    public Integer o; // = null;
    @DexIgnore
    public String p; // = null;
    @DexIgnore
    public String q; // = null;
    @DexIgnore
    public String r; // = null;
    @DexIgnore
    public Long s; // = null;
    @DexIgnore
    public Long t; // = null;
    @DexIgnore
    public String u; // = null;
    @DexIgnore
    public Boolean v; // = null;
    @DexIgnore
    public String w; // = null;
    @DexIgnore
    public Long x; // = null;
    @DexIgnore
    public Integer y; // = null;
    @DexIgnore
    public String z; // = null;

    @DexIgnore
    public g61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static g61[] e() {
        if (R == null) {
            synchronized (ac1.b) {
                if (R == null) {
                    R = new g61[0];
                }
            }
        }
        return R;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            vb1.b(1, num.intValue());
        }
        d61[] d61Arr = this.d;
        if (d61Arr != null && d61Arr.length > 0) {
            int i2 = 0;
            while (true) {
                d61[] d61Arr2 = this.d;
                if (i2 >= d61Arr2.length) {
                    break;
                }
                d61 d61 = d61Arr2[i2];
                if (d61 != null) {
                    vb1.a(2, (bc1) d61);
                }
                i2++;
            }
        }
        j61[] j61Arr = this.e;
        if (j61Arr != null && j61Arr.length > 0) {
            int i3 = 0;
            while (true) {
                j61[] j61Arr2 = this.e;
                if (i3 >= j61Arr2.length) {
                    break;
                }
                j61 j61 = j61Arr2[i3];
                if (j61 != null) {
                    vb1.a(3, (bc1) j61);
                }
                i3++;
            }
        }
        Long l2 = this.f;
        if (l2 != null) {
            vb1.b(4, l2.longValue());
        }
        Long l3 = this.g;
        if (l3 != null) {
            vb1.b(5, l3.longValue());
        }
        Long l4 = this.h;
        if (l4 != null) {
            vb1.b(6, l4.longValue());
        }
        Long l5 = this.j;
        if (l5 != null) {
            vb1.b(7, l5.longValue());
        }
        String str = this.k;
        if (str != null) {
            vb1.a(8, str);
        }
        String str2 = this.l;
        if (str2 != null) {
            vb1.a(9, str2);
        }
        String str3 = this.m;
        if (str3 != null) {
            vb1.a(10, str3);
        }
        String str4 = this.n;
        if (str4 != null) {
            vb1.a(11, str4);
        }
        Integer num2 = this.o;
        if (num2 != null) {
            vb1.b(12, num2.intValue());
        }
        String str5 = this.p;
        if (str5 != null) {
            vb1.a(13, str5);
        }
        String str6 = this.q;
        if (str6 != null) {
            vb1.a(14, str6);
        }
        String str7 = this.r;
        if (str7 != null) {
            vb1.a(16, str7);
        }
        Long l6 = this.s;
        if (l6 != null) {
            vb1.b(17, l6.longValue());
        }
        Long l7 = this.t;
        if (l7 != null) {
            vb1.b(18, l7.longValue());
        }
        String str8 = this.u;
        if (str8 != null) {
            vb1.a(19, str8);
        }
        Boolean bool = this.v;
        if (bool != null) {
            vb1.a(20, bool.booleanValue());
        }
        String str9 = this.w;
        if (str9 != null) {
            vb1.a(21, str9);
        }
        Long l8 = this.x;
        if (l8 != null) {
            vb1.b(22, l8.longValue());
        }
        Integer num3 = this.y;
        if (num3 != null) {
            vb1.b(23, num3.intValue());
        }
        String str10 = this.z;
        if (str10 != null) {
            vb1.a(24, str10);
        }
        String str11 = this.A;
        if (str11 != null) {
            vb1.a(25, str11);
        }
        Long l9 = this.i;
        if (l9 != null) {
            vb1.b(26, l9.longValue());
        }
        Boolean bool2 = this.B;
        if (bool2 != null) {
            vb1.a(28, bool2.booleanValue());
        }
        b61[] b61Arr = this.C;
        if (b61Arr != null && b61Arr.length > 0) {
            int i4 = 0;
            while (true) {
                b61[] b61Arr2 = this.C;
                if (i4 >= b61Arr2.length) {
                    break;
                }
                b61 b61 = b61Arr2[i4];
                if (b61 != null) {
                    vb1.a(29, (bc1) b61);
                }
                i4++;
            }
        }
        String str12 = this.D;
        if (str12 != null) {
            vb1.a(30, str12);
        }
        Integer num4 = this.E;
        if (num4 != null) {
            vb1.b(31, num4.intValue());
        }
        Integer num5 = this.F;
        if (num5 != null) {
            vb1.b(32, num5.intValue());
        }
        Integer num6 = this.G;
        if (num6 != null) {
            vb1.b(33, num6.intValue());
        }
        String str13 = this.H;
        if (str13 != null) {
            vb1.a(34, str13);
        }
        Long l10 = this.I;
        if (l10 != null) {
            vb1.b(35, l10.longValue());
        }
        Long l11 = this.J;
        if (l11 != null) {
            vb1.b(36, l11.longValue());
        }
        String str14 = this.K;
        if (str14 != null) {
            vb1.a(37, str14);
        }
        String str15 = this.L;
        if (str15 != null) {
            vb1.a(38, str15);
        }
        Integer num7 = this.M;
        if (num7 != null) {
            vb1.b(39, num7.intValue());
        }
        String str16 = this.N;
        if (str16 != null) {
            vb1.a(41, str16);
        }
        zzfe$zzb zzfe_zzb = this.O;
        if (zzfe_zzb != null) {
            vb1.a(44, (x91) zzfe_zzb);
        }
        int[] iArr = this.P;
        if (iArr != null && iArr.length > 0) {
            int i5 = 0;
            while (true) {
                int[] iArr2 = this.P;
                if (i5 >= iArr2.length) {
                    break;
                }
                int i6 = iArr2[i5];
                vb1.a(45, 0);
                vb1.b(i6);
                i5++;
            }
        }
        Long l12 = this.Q;
        if (l12 != null) {
            vb1.b(46, l12.longValue());
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof g61)) {
            return false;
        }
        g61 g61 = (g61) obj;
        Integer num = this.c;
        if (num == null) {
            if (g61.c != null) {
                return false;
            }
        } else if (!num.equals(g61.c)) {
            return false;
        }
        if (!ac1.a((Object[]) this.d, (Object[]) g61.d) || !ac1.a((Object[]) this.e, (Object[]) g61.e)) {
            return false;
        }
        Long l2 = this.f;
        if (l2 == null) {
            if (g61.f != null) {
                return false;
            }
        } else if (!l2.equals(g61.f)) {
            return false;
        }
        Long l3 = this.g;
        if (l3 == null) {
            if (g61.g != null) {
                return false;
            }
        } else if (!l3.equals(g61.g)) {
            return false;
        }
        Long l4 = this.h;
        if (l4 == null) {
            if (g61.h != null) {
                return false;
            }
        } else if (!l4.equals(g61.h)) {
            return false;
        }
        Long l5 = this.i;
        if (l5 == null) {
            if (g61.i != null) {
                return false;
            }
        } else if (!l5.equals(g61.i)) {
            return false;
        }
        Long l6 = this.j;
        if (l6 == null) {
            if (g61.j != null) {
                return false;
            }
        } else if (!l6.equals(g61.j)) {
            return false;
        }
        String str = this.k;
        if (str == null) {
            if (g61.k != null) {
                return false;
            }
        } else if (!str.equals(g61.k)) {
            return false;
        }
        String str2 = this.l;
        if (str2 == null) {
            if (g61.l != null) {
                return false;
            }
        } else if (!str2.equals(g61.l)) {
            return false;
        }
        String str3 = this.m;
        if (str3 == null) {
            if (g61.m != null) {
                return false;
            }
        } else if (!str3.equals(g61.m)) {
            return false;
        }
        String str4 = this.n;
        if (str4 == null) {
            if (g61.n != null) {
                return false;
            }
        } else if (!str4.equals(g61.n)) {
            return false;
        }
        Integer num2 = this.o;
        if (num2 == null) {
            if (g61.o != null) {
                return false;
            }
        } else if (!num2.equals(g61.o)) {
            return false;
        }
        String str5 = this.p;
        if (str5 == null) {
            if (g61.p != null) {
                return false;
            }
        } else if (!str5.equals(g61.p)) {
            return false;
        }
        String str6 = this.q;
        if (str6 == null) {
            if (g61.q != null) {
                return false;
            }
        } else if (!str6.equals(g61.q)) {
            return false;
        }
        String str7 = this.r;
        if (str7 == null) {
            if (g61.r != null) {
                return false;
            }
        } else if (!str7.equals(g61.r)) {
            return false;
        }
        Long l7 = this.s;
        if (l7 == null) {
            if (g61.s != null) {
                return false;
            }
        } else if (!l7.equals(g61.s)) {
            return false;
        }
        Long l8 = this.t;
        if (l8 == null) {
            if (g61.t != null) {
                return false;
            }
        } else if (!l8.equals(g61.t)) {
            return false;
        }
        String str8 = this.u;
        if (str8 == null) {
            if (g61.u != null) {
                return false;
            }
        } else if (!str8.equals(g61.u)) {
            return false;
        }
        Boolean bool = this.v;
        if (bool == null) {
            if (g61.v != null) {
                return false;
            }
        } else if (!bool.equals(g61.v)) {
            return false;
        }
        String str9 = this.w;
        if (str9 == null) {
            if (g61.w != null) {
                return false;
            }
        } else if (!str9.equals(g61.w)) {
            return false;
        }
        Long l9 = this.x;
        if (l9 == null) {
            if (g61.x != null) {
                return false;
            }
        } else if (!l9.equals(g61.x)) {
            return false;
        }
        Integer num3 = this.y;
        if (num3 == null) {
            if (g61.y != null) {
                return false;
            }
        } else if (!num3.equals(g61.y)) {
            return false;
        }
        String str10 = this.z;
        if (str10 == null) {
            if (g61.z != null) {
                return false;
            }
        } else if (!str10.equals(g61.z)) {
            return false;
        }
        String str11 = this.A;
        if (str11 == null) {
            if (g61.A != null) {
                return false;
            }
        } else if (!str11.equals(g61.A)) {
            return false;
        }
        Boolean bool2 = this.B;
        if (bool2 == null) {
            if (g61.B != null) {
                return false;
            }
        } else if (!bool2.equals(g61.B)) {
            return false;
        }
        if (!ac1.a((Object[]) this.C, (Object[]) g61.C)) {
            return false;
        }
        String str12 = this.D;
        if (str12 == null) {
            if (g61.D != null) {
                return false;
            }
        } else if (!str12.equals(g61.D)) {
            return false;
        }
        Integer num4 = this.E;
        if (num4 == null) {
            if (g61.E != null) {
                return false;
            }
        } else if (!num4.equals(g61.E)) {
            return false;
        }
        Integer num5 = this.F;
        if (num5 == null) {
            if (g61.F != null) {
                return false;
            }
        } else if (!num5.equals(g61.F)) {
            return false;
        }
        Integer num6 = this.G;
        if (num6 == null) {
            if (g61.G != null) {
                return false;
            }
        } else if (!num6.equals(g61.G)) {
            return false;
        }
        String str13 = this.H;
        if (str13 == null) {
            if (g61.H != null) {
                return false;
            }
        } else if (!str13.equals(g61.H)) {
            return false;
        }
        Long l10 = this.I;
        if (l10 == null) {
            if (g61.I != null) {
                return false;
            }
        } else if (!l10.equals(g61.I)) {
            return false;
        }
        Long l11 = this.J;
        if (l11 == null) {
            if (g61.J != null) {
                return false;
            }
        } else if (!l11.equals(g61.J)) {
            return false;
        }
        String str14 = this.K;
        if (str14 == null) {
            if (g61.K != null) {
                return false;
            }
        } else if (!str14.equals(g61.K)) {
            return false;
        }
        String str15 = this.L;
        if (str15 == null) {
            if (g61.L != null) {
                return false;
            }
        } else if (!str15.equals(g61.L)) {
            return false;
        }
        Integer num7 = this.M;
        if (num7 == null) {
            if (g61.M != null) {
                return false;
            }
        } else if (!num7.equals(g61.M)) {
            return false;
        }
        String str16 = this.N;
        if (str16 == null) {
            if (g61.N != null) {
                return false;
            }
        } else if (!str16.equals(g61.N)) {
            return false;
        }
        zzfe$zzb zzfe_zzb = this.O;
        if (zzfe_zzb == null) {
            if (g61.O != null) {
                return false;
            }
        } else if (!zzfe_zzb.equals(g61.O)) {
            return false;
        }
        if (!ac1.a(this.P, g61.P)) {
            return false;
        }
        Long l12 = this.Q;
        if (l12 == null) {
            if (g61.Q != null) {
                return false;
            }
        } else if (!l12.equals(g61.Q)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(g61.b);
        }
        yb1 yb12 = g61.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int i2;
        int hashCode = (g61.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i3 = 0;
        int hashCode2 = (((((hashCode + (num == null ? 0 : num.hashCode())) * 31) + ac1.a((Object[]) this.d)) * 31) + ac1.a((Object[]) this.e)) * 31;
        Long l2 = this.f;
        int hashCode3 = (hashCode2 + (l2 == null ? 0 : l2.hashCode())) * 31;
        Long l3 = this.g;
        int hashCode4 = (hashCode3 + (l3 == null ? 0 : l3.hashCode())) * 31;
        Long l4 = this.h;
        int hashCode5 = (hashCode4 + (l4 == null ? 0 : l4.hashCode())) * 31;
        Long l5 = this.i;
        int hashCode6 = (hashCode5 + (l5 == null ? 0 : l5.hashCode())) * 31;
        Long l6 = this.j;
        int hashCode7 = (hashCode6 + (l6 == null ? 0 : l6.hashCode())) * 31;
        String str = this.k;
        int hashCode8 = (hashCode7 + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.l;
        int hashCode9 = (hashCode8 + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.m;
        int hashCode10 = (hashCode9 + (str3 == null ? 0 : str3.hashCode())) * 31;
        String str4 = this.n;
        int hashCode11 = (hashCode10 + (str4 == null ? 0 : str4.hashCode())) * 31;
        Integer num2 = this.o;
        int hashCode12 = (hashCode11 + (num2 == null ? 0 : num2.hashCode())) * 31;
        String str5 = this.p;
        int hashCode13 = (hashCode12 + (str5 == null ? 0 : str5.hashCode())) * 31;
        String str6 = this.q;
        int hashCode14 = (hashCode13 + (str6 == null ? 0 : str6.hashCode())) * 31;
        String str7 = this.r;
        int hashCode15 = (hashCode14 + (str7 == null ? 0 : str7.hashCode())) * 31;
        Long l7 = this.s;
        int hashCode16 = (hashCode15 + (l7 == null ? 0 : l7.hashCode())) * 31;
        Long l8 = this.t;
        int hashCode17 = (hashCode16 + (l8 == null ? 0 : l8.hashCode())) * 31;
        String str8 = this.u;
        int hashCode18 = (hashCode17 + (str8 == null ? 0 : str8.hashCode())) * 31;
        Boolean bool = this.v;
        int hashCode19 = (hashCode18 + (bool == null ? 0 : bool.hashCode())) * 31;
        String str9 = this.w;
        int hashCode20 = (hashCode19 + (str9 == null ? 0 : str9.hashCode())) * 31;
        Long l9 = this.x;
        int hashCode21 = (hashCode20 + (l9 == null ? 0 : l9.hashCode())) * 31;
        Integer num3 = this.y;
        int hashCode22 = (hashCode21 + (num3 == null ? 0 : num3.hashCode())) * 31;
        String str10 = this.z;
        int hashCode23 = (hashCode22 + (str10 == null ? 0 : str10.hashCode())) * 31;
        String str11 = this.A;
        int hashCode24 = (hashCode23 + (str11 == null ? 0 : str11.hashCode())) * 31;
        Boolean bool2 = this.B;
        int hashCode25 = (((hashCode24 + (bool2 == null ? 0 : bool2.hashCode())) * 31) + ac1.a((Object[]) this.C)) * 31;
        String str12 = this.D;
        int hashCode26 = (hashCode25 + (str12 == null ? 0 : str12.hashCode())) * 31;
        Integer num4 = this.E;
        int hashCode27 = (hashCode26 + (num4 == null ? 0 : num4.hashCode())) * 31;
        Integer num5 = this.F;
        int hashCode28 = (hashCode27 + (num5 == null ? 0 : num5.hashCode())) * 31;
        Integer num6 = this.G;
        int hashCode29 = (hashCode28 + (num6 == null ? 0 : num6.hashCode())) * 31;
        String str13 = this.H;
        int hashCode30 = (hashCode29 + (str13 == null ? 0 : str13.hashCode())) * 31;
        Long l10 = this.I;
        int hashCode31 = (hashCode30 + (l10 == null ? 0 : l10.hashCode())) * 31;
        Long l11 = this.J;
        int hashCode32 = (hashCode31 + (l11 == null ? 0 : l11.hashCode())) * 31;
        String str14 = this.K;
        int hashCode33 = (hashCode32 + (str14 == null ? 0 : str14.hashCode())) * 31;
        String str15 = this.L;
        int hashCode34 = (hashCode33 + (str15 == null ? 0 : str15.hashCode())) * 31;
        Integer num7 = this.M;
        int hashCode35 = (hashCode34 + (num7 == null ? 0 : num7.hashCode())) * 31;
        String str16 = this.N;
        int hashCode36 = hashCode35 + (str16 == null ? 0 : str16.hashCode());
        zzfe$zzb zzfe_zzb = this.O;
        int i4 = hashCode36 * 31;
        if (zzfe_zzb == null) {
            i2 = 0;
        } else {
            i2 = zzfe_zzb.hashCode();
        }
        int a = (((i4 + i2) * 31) + ac1.a(this.P)) * 31;
        Long l12 = this.Q;
        int hashCode37 = (a + (l12 == null ? 0 : l12.hashCode())) * 31;
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            i3 = this.b.hashCode();
        }
        return hashCode37 + i3;
    }

    @DexIgnore
    public final int a() {
        int[] iArr;
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += vb1.c(1, num.intValue());
        }
        d61[] d61Arr = this.d;
        int i2 = 0;
        if (d61Arr != null && d61Arr.length > 0) {
            int i3 = a;
            int i4 = 0;
            while (true) {
                d61[] d61Arr2 = this.d;
                if (i4 >= d61Arr2.length) {
                    break;
                }
                d61 d61 = d61Arr2[i4];
                if (d61 != null) {
                    i3 += vb1.b(2, (bc1) d61);
                }
                i4++;
            }
            a = i3;
        }
        j61[] j61Arr = this.e;
        if (j61Arr != null && j61Arr.length > 0) {
            int i5 = a;
            int i6 = 0;
            while (true) {
                j61[] j61Arr2 = this.e;
                if (i6 >= j61Arr2.length) {
                    break;
                }
                j61 j61 = j61Arr2[i6];
                if (j61 != null) {
                    i5 += vb1.b(3, (bc1) j61);
                }
                i6++;
            }
            a = i5;
        }
        Long l2 = this.f;
        if (l2 != null) {
            a += vb1.c(4, l2.longValue());
        }
        Long l3 = this.g;
        if (l3 != null) {
            a += vb1.c(5, l3.longValue());
        }
        Long l4 = this.h;
        if (l4 != null) {
            a += vb1.c(6, l4.longValue());
        }
        Long l5 = this.j;
        if (l5 != null) {
            a += vb1.c(7, l5.longValue());
        }
        String str = this.k;
        if (str != null) {
            a += vb1.b(8, str);
        }
        String str2 = this.l;
        if (str2 != null) {
            a += vb1.b(9, str2);
        }
        String str3 = this.m;
        if (str3 != null) {
            a += vb1.b(10, str3);
        }
        String str4 = this.n;
        if (str4 != null) {
            a += vb1.b(11, str4);
        }
        Integer num2 = this.o;
        if (num2 != null) {
            a += vb1.c(12, num2.intValue());
        }
        String str5 = this.p;
        if (str5 != null) {
            a += vb1.b(13, str5);
        }
        String str6 = this.q;
        if (str6 != null) {
            a += vb1.b(14, str6);
        }
        String str7 = this.r;
        if (str7 != null) {
            a += vb1.b(16, str7);
        }
        Long l6 = this.s;
        if (l6 != null) {
            a += vb1.c(17, l6.longValue());
        }
        Long l7 = this.t;
        if (l7 != null) {
            a += vb1.c(18, l7.longValue());
        }
        String str8 = this.u;
        if (str8 != null) {
            a += vb1.b(19, str8);
        }
        Boolean bool = this.v;
        if (bool != null) {
            bool.booleanValue();
            a += vb1.c(20) + 1;
        }
        String str9 = this.w;
        if (str9 != null) {
            a += vb1.b(21, str9);
        }
        Long l8 = this.x;
        if (l8 != null) {
            a += vb1.c(22, l8.longValue());
        }
        Integer num3 = this.y;
        if (num3 != null) {
            a += vb1.c(23, num3.intValue());
        }
        String str10 = this.z;
        if (str10 != null) {
            a += vb1.b(24, str10);
        }
        String str11 = this.A;
        if (str11 != null) {
            a += vb1.b(25, str11);
        }
        Long l9 = this.i;
        if (l9 != null) {
            a += vb1.c(26, l9.longValue());
        }
        Boolean bool2 = this.B;
        if (bool2 != null) {
            bool2.booleanValue();
            a += vb1.c(28) + 1;
        }
        b61[] b61Arr = this.C;
        if (b61Arr != null && b61Arr.length > 0) {
            int i7 = a;
            int i8 = 0;
            while (true) {
                b61[] b61Arr2 = this.C;
                if (i8 >= b61Arr2.length) {
                    break;
                }
                b61 b61 = b61Arr2[i8];
                if (b61 != null) {
                    i7 += vb1.b(29, (bc1) b61);
                }
                i8++;
            }
            a = i7;
        }
        String str12 = this.D;
        if (str12 != null) {
            a += vb1.b(30, str12);
        }
        Integer num4 = this.E;
        if (num4 != null) {
            a += vb1.c(31, num4.intValue());
        }
        Integer num5 = this.F;
        if (num5 != null) {
            a += vb1.c(32, num5.intValue());
        }
        Integer num6 = this.G;
        if (num6 != null) {
            a += vb1.c(33, num6.intValue());
        }
        String str13 = this.H;
        if (str13 != null) {
            a += vb1.b(34, str13);
        }
        Long l10 = this.I;
        if (l10 != null) {
            a += vb1.c(35, l10.longValue());
        }
        Long l11 = this.J;
        if (l11 != null) {
            a += vb1.c(36, l11.longValue());
        }
        String str14 = this.K;
        if (str14 != null) {
            a += vb1.b(37, str14);
        }
        String str15 = this.L;
        if (str15 != null) {
            a += vb1.b(38, str15);
        }
        Integer num7 = this.M;
        if (num7 != null) {
            a += vb1.c(39, num7.intValue());
        }
        String str16 = this.N;
        if (str16 != null) {
            a += vb1.b(41, str16);
        }
        zzfe$zzb zzfe_zzb = this.O;
        if (zzfe_zzb != null) {
            a += zztv.c(44, (x91) zzfe_zzb);
        }
        int[] iArr2 = this.P;
        if (iArr2 != null && iArr2.length > 0) {
            int i9 = 0;
            while (true) {
                iArr = this.P;
                if (i2 >= iArr.length) {
                    break;
                }
                i9 += vb1.e(iArr[i2]);
                i2++;
            }
            a = a + i9 + (iArr.length * 2);
        }
        Long l12 = this.Q;
        return l12 != null ? a + vb1.c(46, l12.longValue()) : a;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            switch (c2) {
                case 0:
                    return this;
                case 8:
                    this.c = Integer.valueOf(ub1.e());
                    break;
                case 18:
                    int a = ec1.a(ub1, 18);
                    d61[] d61Arr = this.d;
                    int length = d61Arr == null ? 0 : d61Arr.length;
                    d61[] d61Arr2 = new d61[(a + length)];
                    if (length != 0) {
                        System.arraycopy(this.d, 0, d61Arr2, 0, length);
                    }
                    while (length < d61Arr2.length - 1) {
                        d61Arr2[length] = new d61();
                        ub1.a((bc1) d61Arr2[length]);
                        ub1.c();
                        length++;
                    }
                    d61Arr2[length] = new d61();
                    ub1.a((bc1) d61Arr2[length]);
                    this.d = d61Arr2;
                    break;
                case 26:
                    int a2 = ec1.a(ub1, 26);
                    j61[] j61Arr = this.e;
                    int length2 = j61Arr == null ? 0 : j61Arr.length;
                    j61[] j61Arr2 = new j61[(a2 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.e, 0, j61Arr2, 0, length2);
                    }
                    while (length2 < j61Arr2.length - 1) {
                        j61Arr2[length2] = new j61();
                        ub1.a((bc1) j61Arr2[length2]);
                        ub1.c();
                        length2++;
                    }
                    j61Arr2[length2] = new j61();
                    ub1.a((bc1) j61Arr2[length2]);
                    this.e = j61Arr2;
                    break;
                case 32:
                    this.f = Long.valueOf(ub1.f());
                    break;
                case 40:
                    this.g = Long.valueOf(ub1.f());
                    break;
                case 48:
                    this.h = Long.valueOf(ub1.f());
                    break;
                case 56:
                    this.j = Long.valueOf(ub1.f());
                    break;
                case 66:
                    this.k = ub1.b();
                    break;
                case 74:
                    this.l = ub1.b();
                    break;
                case 82:
                    this.m = ub1.b();
                    break;
                case 90:
                    this.n = ub1.b();
                    break;
                case 96:
                    this.o = Integer.valueOf(ub1.e());
                    break;
                case 106:
                    this.p = ub1.b();
                    break;
                case 114:
                    this.q = ub1.b();
                    break;
                case 130:
                    this.r = ub1.b();
                    break;
                case 136:
                    this.s = Long.valueOf(ub1.f());
                    break;
                case 144:
                    this.t = Long.valueOf(ub1.f());
                    break;
                case 154:
                    this.u = ub1.b();
                    break;
                case 160:
                    this.v = Boolean.valueOf(ub1.d());
                    break;
                case 170:
                    this.w = ub1.b();
                    break;
                case 176:
                    this.x = Long.valueOf(ub1.f());
                    break;
                case 184:
                    this.y = Integer.valueOf(ub1.e());
                    break;
                case 194:
                    this.z = ub1.b();
                    break;
                case Action.Selfie.TAKE_BURST /*202*/:
                    this.A = ub1.b();
                    break;
                case 208:
                    this.i = Long.valueOf(ub1.f());
                    break;
                case 224:
                    this.B = Boolean.valueOf(ub1.d());
                    break;
                case 234:
                    int a3 = ec1.a(ub1, 234);
                    b61[] b61Arr = this.C;
                    int length3 = b61Arr == null ? 0 : b61Arr.length;
                    b61[] b61Arr2 = new b61[(a3 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.C, 0, b61Arr2, 0, length3);
                    }
                    while (length3 < b61Arr2.length - 1) {
                        b61Arr2[length3] = new b61();
                        ub1.a((bc1) b61Arr2[length3]);
                        ub1.c();
                        length3++;
                    }
                    b61Arr2[length3] = new b61();
                    ub1.a((bc1) b61Arr2[length3]);
                    this.C = b61Arr2;
                    break;
                case 242:
                    this.D = ub1.b();
                    break;
                case 248:
                    this.E = Integer.valueOf(ub1.e());
                    break;
                case 256:
                    this.F = Integer.valueOf(ub1.e());
                    break;
                case 264:
                    this.G = Integer.valueOf(ub1.e());
                    break;
                case 274:
                    this.H = ub1.b();
                    break;
                case 280:
                    this.I = Long.valueOf(ub1.f());
                    break;
                case 288:
                    this.J = Long.valueOf(ub1.f());
                    break;
                case 298:
                    this.K = ub1.b();
                    break;
                case 306:
                    this.L = ub1.b();
                    break;
                case 312:
                    this.M = Integer.valueOf(ub1.e());
                    break;
                case 330:
                    this.N = ub1.b();
                    break;
                case 354:
                    zzfe$zzb zzfe_zzb = (zzfe$zzb) ub1.a(zzfe$zzb.j());
                    zzfe$zzb zzfe_zzb2 = this.O;
                    if (zzfe_zzb2 != null) {
                        zzfe$zzb.a aVar = (zzfe$zzb.a) zzfe_zzb2.h();
                        aVar.a(zzfe_zzb);
                        zzfe_zzb = (zzfe$zzb) aVar.t();
                    }
                    this.O = zzfe_zzb;
                    break;
                case 360:
                    int a4 = ec1.a(ub1, 360);
                    int[] iArr = this.P;
                    int length4 = iArr == null ? 0 : iArr.length;
                    int[] iArr2 = new int[(a4 + length4)];
                    if (length4 != 0) {
                        System.arraycopy(this.P, 0, iArr2, 0, length4);
                    }
                    while (length4 < iArr2.length - 1) {
                        iArr2[length4] = ub1.e();
                        ub1.c();
                        length4++;
                    }
                    iArr2[length4] = ub1.e();
                    this.P = iArr2;
                    break;
                case 362:
                    int c3 = ub1.c(ub1.e());
                    int a5 = ub1.a();
                    int i2 = 0;
                    while (ub1.l() > 0) {
                        ub1.e();
                        i2++;
                    }
                    ub1.f(a5);
                    int[] iArr3 = this.P;
                    int length5 = iArr3 == null ? 0 : iArr3.length;
                    int[] iArr4 = new int[(i2 + length5)];
                    if (length5 != 0) {
                        System.arraycopy(this.P, 0, iArr4, 0, length5);
                    }
                    while (length5 < iArr4.length) {
                        iArr4[length5] = ub1.e();
                        length5++;
                    }
                    this.P = iArr4;
                    ub1.d(c3);
                    break;
                case 368:
                    this.Q = Long.valueOf(ub1.f());
                    break;
                default:
                    if (super.a(ub1, c2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }
}
