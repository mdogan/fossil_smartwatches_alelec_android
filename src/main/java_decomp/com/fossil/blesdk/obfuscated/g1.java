package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import androidx.appcompat.view.menu.ListMenuItemView;
import com.fossil.blesdk.obfuscated.q1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g1 extends BaseAdapter {
    @DexIgnore
    public h1 e;
    @DexIgnore
    public int f; // = -1;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ LayoutInflater i;
    @DexIgnore
    public /* final */ int j;

    @DexIgnore
    public g1(h1 h1Var, LayoutInflater layoutInflater, boolean z, int i2) {
        this.h = z;
        this.i = layoutInflater;
        this.e = h1Var;
        this.j = i2;
        a();
    }

    @DexIgnore
    public void a(boolean z) {
        this.g = z;
    }

    @DexIgnore
    public h1 b() {
        return this.e;
    }

    @DexIgnore
    public int getCount() {
        ArrayList<k1> j2 = this.h ? this.e.j() : this.e.n();
        if (this.f < 0) {
            return j2.size();
        }
        return j2.size() - 1;
    }

    @DexIgnore
    public long getItemId(int i2) {
        return (long) i2;
    }

    @DexIgnore
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.i.inflate(this.j, viewGroup, false);
        }
        int groupId = getItem(i2).getGroupId();
        int i3 = i2 - 1;
        ListMenuItemView listMenuItemView = (ListMenuItemView) view;
        listMenuItemView.setGroupDividerEnabled(this.e.o() && groupId != (i3 >= 0 ? getItem(i3).getGroupId() : groupId));
        q1.a aVar = (q1.a) view;
        if (this.g) {
            listMenuItemView.setForceShowIcon(true);
        }
        aVar.a(getItem(i2), 0);
        return view;
    }

    @DexIgnore
    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }

    @DexIgnore
    public void a() {
        k1 f2 = this.e.f();
        if (f2 != null) {
            ArrayList<k1> j2 = this.e.j();
            int size = j2.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (j2.get(i2) == f2) {
                    this.f = i2;
                    return;
                }
            }
        }
        this.f = -1;
    }

    @DexIgnore
    public k1 getItem(int i2) {
        ArrayList<k1> j2 = this.h ? this.e.j() : this.e.n();
        int i3 = this.f;
        if (i3 >= 0 && i2 >= i3) {
            i2++;
        }
        return j2.get(i2);
    }
}
