package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.me;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jt2 extends me.d<GoalTrackingSummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        wd4.b(goalTrackingSummary, "oldItem");
        wd4.b(goalTrackingSummary2, "newItem");
        return wd4.a((Object) goalTrackingSummary, (Object) goalTrackingSummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        wd4.b(goalTrackingSummary, "oldItem");
        wd4.b(goalTrackingSummary2, "newItem");
        return sk2.d(goalTrackingSummary.getDate(), goalTrackingSummary2.getDate());
    }
}
