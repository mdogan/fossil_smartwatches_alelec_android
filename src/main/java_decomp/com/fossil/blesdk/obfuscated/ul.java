package com.fossil.blesdk.obfuscated;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ul implements Executor {
    @DexIgnore
    public /* final */ ArrayDeque<a> e; // = new ArrayDeque<>();
    @DexIgnore
    public /* final */ Executor f;
    @DexIgnore
    public /* final */ Object g; // = new Object();
    @DexIgnore
    public volatile Runnable h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public /* final */ ul e;
        @DexIgnore
        public /* final */ Runnable f;

        @DexIgnore
        public a(ul ulVar, Runnable runnable) {
            this.e = ulVar;
            this.f = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.f.run();
            } finally {
                this.e.b();
            }
        }
    }

    @DexIgnore
    public ul(Executor executor) {
        this.f = executor;
    }

    @DexIgnore
    public boolean a() {
        boolean z;
        synchronized (this.g) {
            z = !this.e.isEmpty();
        }
        return z;
    }

    @DexIgnore
    public void b() {
        synchronized (this.g) {
            Runnable poll = this.e.poll();
            this.h = poll;
            if (poll != null) {
                this.f.execute(this.h);
            }
        }
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        synchronized (this.g) {
            this.e.add(new a(this, runnable));
            if (this.h == null) {
                b();
            }
        }
    }
}
