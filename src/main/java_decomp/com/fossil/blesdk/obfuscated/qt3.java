package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qt3 {
    @DexIgnore
    public short a;
    @DexIgnore
    public short b;

    @DexIgnore
    public final short a() {
        return this.a;
    }

    @DexIgnore
    public final short b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof qt3) {
                qt3 qt3 = (qt3) obj;
                if (this.a == qt3.a) {
                    if (this.b == qt3.b) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 31) + this.b;
    }

    @DexIgnore
    public String toString() {
        return "HeartRateDWMModel(minValue=" + this.a + ", maxValue=" + this.b + ")";
    }
}
