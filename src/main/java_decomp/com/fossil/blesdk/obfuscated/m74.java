package com.fossil.blesdk.obfuscated;

import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class m74 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ x74 j;
    @DexIgnore
    public /* final */ Collection<y44> k;

    @DexIgnore
    public m74(String str, String str2, String str3, String str4, String str5, String str6, int i2, String str7, String str8, x74 x74, Collection<y44> collection) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = i2;
        this.h = str7;
        this.i = str8;
        this.j = x74;
        this.k = collection;
    }
}
