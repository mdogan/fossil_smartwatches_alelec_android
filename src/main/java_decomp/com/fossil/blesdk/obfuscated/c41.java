package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.af0;
import com.google.android.gms.location.LocationAvailability;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c41 implements af0.b<rc1> {
    @DexIgnore
    public /* final */ /* synthetic */ LocationAvailability a;

    @DexIgnore
    public c41(a41 a41, LocationAvailability locationAvailability) {
        this.a = locationAvailability;
    }

    @DexIgnore
    public final void a() {
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj) {
        ((rc1) obj).onLocationAvailability(this.a);
    }
}
