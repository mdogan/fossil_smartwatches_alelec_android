package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.iv3;
import com.fossil.blesdk.obfuscated.tu3;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.NetworkPolicy;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ey3 implements Downloader {
    @DexIgnore
    public /* final */ hv3 a;

    @DexIgnore
    public ey3(Context context) {
        this(py3.b(context));
    }

    @DexIgnore
    public static hv3 a() {
        hv3 hv3 = new hv3();
        hv3.a(15000, TimeUnit.MILLISECONDS);
        hv3.b(20000, TimeUnit.MILLISECONDS);
        hv3.c(20000, TimeUnit.MILLISECONDS);
        return hv3;
    }

    @DexIgnore
    public Downloader.Response load(Uri uri, int i) throws IOException {
        tu3 tu3;
        if (i == 0) {
            tu3 = null;
        } else if (NetworkPolicy.isOfflineOnly(i)) {
            tu3 = tu3.m;
        } else {
            tu3.b bVar = new tu3.b();
            if (!NetworkPolicy.shouldReadFromDiskCache(i)) {
                bVar.b();
            }
            if (!NetworkPolicy.shouldWriteToDiskCache(i)) {
                bVar.c();
            }
            tu3 = bVar.a();
        }
        iv3.b bVar2 = new iv3.b();
        bVar2.b(uri.toString());
        if (tu3 != null) {
            bVar2.a(tu3);
        }
        kv3 a2 = this.a.a(bVar2.a()).a();
        int e = a2.e();
        if (e < 300) {
            boolean z = a2.c() != null;
            lv3 a3 = a2.a();
            return new Downloader.Response(a3.y(), z, a3.z());
        }
        a2.a().close();
        throw new Downloader.ResponseException(e + " " + a2.h(), i, e);
    }

    @DexIgnore
    public ey3(File file) {
        this(file, py3.a(file));
    }

    @DexIgnore
    public ey3(File file, long j) {
        this(a());
        try {
            this.a.a(new su3(file, j));
        } catch (IOException unused) {
        }
    }

    @DexIgnore
    public ey3(hv3 hv3) {
        this.a = hv3;
    }
}
