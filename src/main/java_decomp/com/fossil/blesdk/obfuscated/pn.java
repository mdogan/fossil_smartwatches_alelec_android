package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.http.AndroidHttpClient;
import android.os.Build;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pn {
    @DexIgnore
    public static um a(Context context, an anVar) {
        bn bnVar;
        String str;
        if (anVar != null) {
            bnVar = new bn(anVar);
        } else if (Build.VERSION.SDK_INT >= 9) {
            bnVar = new bn((an) new in());
        } else {
            try {
                String packageName = context.getPackageName();
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
                str = packageName + ZendeskConfig.SLASH + packageInfo.versionCode;
            } catch (PackageManager.NameNotFoundException unused) {
                str = "volley/0";
            }
            bnVar = new bn((hn) new en(AndroidHttpClient.newInstance(str)));
        }
        return a(context, (rm) bnVar);
    }

    @DexIgnore
    public static um a(Context context, rm rmVar) {
        um umVar = new um(new dn(new File(context.getCacheDir(), "volley")), rmVar);
        umVar.b();
        return umVar;
    }

    @DexIgnore
    public static um a(Context context) {
        return a(context, (an) null);
    }
}
