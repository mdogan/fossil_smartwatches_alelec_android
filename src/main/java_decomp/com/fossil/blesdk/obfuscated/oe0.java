package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ne0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface oe0<R extends ne0> {
    @DexIgnore
    void onResult(R r);
}
