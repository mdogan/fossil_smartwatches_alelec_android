package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a61 extends wb1<a61> {
    @DexIgnore
    public static volatile a61[] e;
    @DexIgnore
    public String c; // = null;
    @DexIgnore
    public String d; // = null;

    @DexIgnore
    public a61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static a61[] e() {
        if (e == null) {
            synchronized (ac1.b) {
                if (e == null) {
                    e = new a61[0];
                }
            }
        }
        return e;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        String str = this.c;
        if (str != null) {
            vb1.a(1, str);
        }
        String str2 = this.d;
        if (str2 != null) {
            vb1.a(2, str2);
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof a61)) {
            return false;
        }
        a61 a61 = (a61) obj;
        String str = this.c;
        if (str == null) {
            if (a61.c != null) {
                return false;
            }
        } else if (!str.equals(a61.c)) {
            return false;
        }
        String str2 = this.d;
        if (str2 == null) {
            if (a61.d != null) {
                return false;
            }
        } else if (!str2.equals(a61.d)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(a61.b);
        }
        yb1 yb12 = a61.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (a61.class.getName().hashCode() + 527) * 31;
        String str = this.c;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.d;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        String str = this.c;
        if (str != null) {
            a += vb1.b(1, str);
        }
        String str2 = this.d;
        return str2 != null ? a + vb1.b(2, str2) : a;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 10) {
                this.c = ub1.b();
            } else if (c2 == 18) {
                this.d = ub1.b();
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
