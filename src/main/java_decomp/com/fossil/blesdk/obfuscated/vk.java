package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class vk<T> {
    @DexIgnore
    public static /* final */ String f; // = ej.a("ConstraintTracker");
    @DexIgnore
    public /* final */ am a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public /* final */ Set<gk<T>> d; // = new LinkedHashSet();
    @DexIgnore
    public T e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ List e;

        @DexIgnore
        public a(List list) {
            this.e = list;
        }

        @DexIgnore
        public void run() {
            for (gk a : this.e) {
                a.a(vk.this.e);
            }
        }
    }

    @DexIgnore
    public vk(Context context, am amVar) {
        this.b = context.getApplicationContext();
        this.a = amVar;
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public void a(gk<T> gkVar) {
        synchronized (this.c) {
            if (this.d.add(gkVar)) {
                if (this.d.size() == 1) {
                    this.e = a();
                    ej.a().a(f, String.format("%s: initial state = %s", new Object[]{getClass().getSimpleName(), this.e}), new Throwable[0]);
                    b();
                }
                gkVar.a(this.e);
            }
        }
    }

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public void b(gk<T> gkVar) {
        synchronized (this.c) {
            if (this.d.remove(gkVar) && this.d.isEmpty()) {
                c();
            }
        }
    }

    @DexIgnore
    public abstract void c();

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        return;
     */
    @DexIgnore
    public void a(T t) {
        synchronized (this.c) {
            if (this.e != t) {
                if (this.e == null || !this.e.equals(t)) {
                    this.e = t;
                    this.a.a().execute(new a(new ArrayList(this.d)));
                }
            }
        }
    }
}
