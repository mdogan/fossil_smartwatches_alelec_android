package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface da4<T> {
    @DexIgnore
    void clear();

    @DexIgnore
    boolean isEmpty();

    @DexIgnore
    boolean offer(T t);

    @DexIgnore
    T poll() throws Exception;
}
