package com.fossil.blesdk.obfuscated;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.blesdk.obfuscated.tr;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yr<Data> implements tr<Integer, Data> {
    @DexIgnore
    public /* final */ tr<Uri, Data> a;
    @DexIgnore
    public /* final */ Resources b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ur<Integer, AssetFileDescriptor> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public a(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        public tr<Integer, AssetFileDescriptor> a(xr xrVar) {
            return new yr(this.a, xrVar.a(Uri.class, AssetFileDescriptor.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements ur<Integer, ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public b(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        public tr<Integer, ParcelFileDescriptor> a(xr xrVar) {
            return new yr(this.a, xrVar.a(Uri.class, ParcelFileDescriptor.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements ur<Integer, InputStream> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public c(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        public tr<Integer, InputStream> a(xr xrVar) {
            return new yr(this.a, xrVar.a(Uri.class, InputStream.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements ur<Integer, Uri> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public d(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        public tr<Integer, Uri> a(xr xrVar) {
            return new yr(this.a, bs.a());
        }
    }

    @DexIgnore
    public yr(Resources resources, tr<Uri, Data> trVar) {
        this.b = resources;
        this.a = trVar;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean a(Integer num) {
        return true;
    }

    @DexIgnore
    public tr.a<Data> a(Integer num, int i, int i2, mo moVar) {
        Uri a2 = a(num);
        if (a2 == null) {
            return null;
        }
        return this.a.a(a2, i, i2, moVar);
    }

    @DexIgnore
    public final Uri a(Integer num) {
        try {
            return Uri.parse("android.resource://" + this.b.getResourcePackageName(num.intValue()) + '/' + this.b.getResourceTypeName(num.intValue()) + '/' + this.b.getResourceEntryName(num.intValue()));
        } catch (Resources.NotFoundException e) {
            if (!Log.isLoggable("ResourceLoader", 5)) {
                return null;
            }
            Log.w("ResourceLoader", "Received invalid resource id: " + num, e);
            return null;
        }
    }
}
