package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.internal.ThreadContextKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vh4<T> extends yh4<T> implements rc4, kc4<T> {
    @DexIgnore
    public Object h; // = xh4.a;
    @DexIgnore
    public /* final */ rc4 i;
    @DexIgnore
    public /* final */ Object j;
    @DexIgnore
    public /* final */ gh4 k;
    @DexIgnore
    public /* final */ kc4<T> l;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vh4(gh4 gh4, kc4<? super T> kc4) {
        super(0);
        wd4.b(gh4, "dispatcher");
        wd4.b(kc4, "continuation");
        this.k = gh4;
        this.l = kc4;
        kc4<T> kc42 = this.l;
        this.i = (rc4) (!(kc42 instanceof rc4) ? null : kc42);
        this.j = ThreadContextKt.a(getContext());
    }

    @DexIgnore
    public kc4<T> b() {
        return this;
    }

    @DexIgnore
    public Object c() {
        Object obj = this.h;
        if (oh4.a()) {
            if (!(obj != xh4.a)) {
                throw new AssertionError();
            }
        }
        this.h = xh4.a;
        return obj;
    }

    @DexIgnore
    public final void d(T t) {
        CoroutineContext context = this.l.getContext();
        this.h = t;
        this.g = 1;
        this.k.b(context, this);
    }

    @DexIgnore
    public rc4 getCallerFrame() {
        return this.i;
    }

    @DexIgnore
    public CoroutineContext getContext() {
        return this.l.getContext();
    }

    @DexIgnore
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        CoroutineContext context;
        Object b;
        CoroutineContext context2 = this.l.getContext();
        Object a = ah4.a(obj);
        if (this.k.b(context2)) {
            this.h = a;
            this.g = 0;
            this.k.a(context2, this);
            return;
        }
        ei4 b2 = nj4.b.b();
        if (b2.D()) {
            this.h = a;
            this.g = 0;
            b2.a((yh4<?>) this);
            return;
        }
        b2.c(true);
        try {
            context = getContext();
            b = ThreadContextKt.b(context, this.j);
            this.l.resumeWith(obj);
            cb4 cb4 = cb4.a;
            ThreadContextKt.a(context, b);
            do {
            } while (b2.G());
        } catch (Throwable th) {
            try {
                a(th, (Throwable) null);
            } catch (Throwable th2) {
                b2.a(true);
                throw th2;
            }
        }
        b2.a(true);
    }

    @DexIgnore
    public String toString() {
        return "DispatchedContinuation[" + this.k + ", " + ph4.a((kc4<?>) this.l) + ']';
    }
}
