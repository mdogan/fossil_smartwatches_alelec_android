package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewFragment;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import java.util.Date;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ba3 extends as2 implements aa3, kt2, ls2 {
    @DexIgnore
    public ur3<za2> j;
    @DexIgnore
    public z93 k;
    @DexIgnore
    public gt2 l;
    @DexIgnore
    public CaloriesOverviewFragment m;
    @DexIgnore
    public cu3 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends cu3 {
        @DexIgnore
        public /* final */ /* synthetic */ ba3 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager, ba3 ba3, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager);
            this.e = ba3;
        }

        @DexIgnore
        public void a(int i) {
            ba3.a(this.e).j();
        }

        @DexIgnore
        public void a(int i, int i2) {
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ z93 a(ba3 ba3) {
        z93 z93 = ba3.k;
        if (z93 != null) {
            return z93;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("DashboardCaloriesFragment visible=");
        sb.append(z);
        sb.append(", tracer=");
        sb.append(Q0());
        sb.append(", isRunning=");
        wl2 Q0 = Q0();
        sb.append(Q0 != null ? Boolean.valueOf(Q0.b()) : null);
        local.d("onVisibleChanged", sb.toString());
        if (z) {
            wl2 Q02 = Q0();
            if (Q02 != null) {
                Q02.d();
            }
            if (isVisible() && this.j != null) {
                za2 T0 = T0();
                if (T0 != null) {
                    RecyclerView recyclerView = T0.q;
                    if (recyclerView != null) {
                        RecyclerView.ViewHolder c = recyclerView.c(0);
                        if (c != null) {
                            View view = c.itemView;
                            if (view != null && view.getY() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                return;
                            }
                        }
                        recyclerView.j(0);
                        cu3 cu3 = this.n;
                        if (cu3 != null) {
                            cu3.a();
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        wl2 Q03 = Q0();
        if (Q03 != null) {
            Q03.a("");
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "DashboardCaloriesFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final za2 T0() {
        ur3<za2> ur3 = this.j;
        if (ur3 != null) {
            return ur3.a();
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(Date date) {
        wd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardCaloriesFragment", "onDayClicked: " + date);
        Context context = getContext();
        if (context != null) {
            CaloriesDetailActivity.a aVar = CaloriesDetailActivity.D;
            wd4.a((Object) context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    public void f() {
        cu3 cu3 = this.n;
        if (cu3 != null) {
            cu3.a();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new ur3<>(this, (za2) ra.a(layoutInflater, R.layout.fragment_dashboard_calories, viewGroup, false, O0()));
        ur3<za2> ur3 = this.j;
        if (ur3 != null) {
            za2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d("DashboardCaloriesFragment", "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    public void onDestroyView() {
        z93 z93 = this.k;
        if (z93 != null) {
            z93.i();
            super.onDestroyView();
            N0();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        z93 z93 = this.k;
        if (z93 != null) {
            z93.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        z93 z93 = this.k;
        if (z93 != null) {
            z93.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        this.m = (CaloriesOverviewFragment) getChildFragmentManager().a("CaloriesOverviewFragment");
        if (this.m == null) {
            this.m = new CaloriesOverviewFragment();
        }
        dt2 dt2 = new dt2();
        PortfolioApp c = PortfolioApp.W.c();
        FragmentManager childFragmentManager = getChildFragmentManager();
        wd4.a((Object) childFragmentManager, "childFragmentManager");
        CaloriesOverviewFragment caloriesOverviewFragment = this.m;
        if (caloriesOverviewFragment != null) {
            this.l = new gt2(dt2, c, this, childFragmentManager, caloriesOverviewFragment);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            za2 T0 = T0();
            if (T0 != null) {
                RecyclerView recyclerView = T0.q;
                if (recyclerView != null) {
                    wd4.a((Object) recyclerView, "it");
                    recyclerView.setLayoutManager(linearLayoutManager);
                    gt2 gt2 = this.l;
                    if (gt2 != null) {
                        recyclerView.setAdapter(gt2);
                        RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                        if (layoutManager != null) {
                            this.n = new b(recyclerView, (LinearLayoutManager) layoutManager, this, linearLayoutManager);
                            cu3 cu3 = this.n;
                            if (cu3 != null) {
                                recyclerView.a((RecyclerView.q) cu3);
                                recyclerView.setItemViewCacheSize(0);
                                m73 m73 = new m73(linearLayoutManager.M());
                                Drawable c2 = k6.c(recyclerView.getContext(), R.drawable.bg_item_decoration_dashboard_line_1w);
                                if (c2 != null) {
                                    wd4.a((Object) c2, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                                    m73.a(c2);
                                    recyclerView.a((RecyclerView.l) m73);
                                    z93 z93 = this.k;
                                    if (z93 != null) {
                                        z93.h();
                                    } else {
                                        wd4.d("mPresenter");
                                        throw null;
                                    }
                                } else {
                                    wd4.a();
                                    throw null;
                                }
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                        }
                    } else {
                        wd4.d("mDashboardCaloriesAdapter");
                        throw null;
                    }
                }
            }
            za2 T02 = T0();
            if (T02 != null) {
                RecyclerView recyclerView2 = T02.q;
                if (recyclerView2 != null) {
                    wd4.a((Object) recyclerView2, "recyclerView");
                    RecyclerView.j itemAnimator = recyclerView2.getItemAnimator();
                    if (itemAnimator instanceof bf) {
                        ((bf) itemAnimator).setSupportsChangeAnimations(false);
                    }
                }
            }
            R("calories_view");
            FragmentActivity activity = getActivity();
            if (activity != null) {
                jc a2 = mc.a(activity).a(ju3.class);
                wd4.a((Object) a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
                ju3 ju3 = (ju3) a2;
                return;
            }
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public void a(rd<ActivitySummary> rdVar) {
        gt2 gt2 = this.l;
        if (gt2 != null) {
            gt2.c(rdVar);
        } else {
            wd4.d("mDashboardCaloriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(z93 z93) {
        wd4.b(z93, "presenter");
        this.k = z93;
    }

    @DexIgnore
    public void b(Date date, Date date2) {
        wd4.b(date, "startWeekDate");
        wd4.b(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardCaloriesFragment", "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }
}
