package com.fossil.blesdk.obfuscated;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n90 {
    @DexIgnore
    public static final JSONObject a(JSONObject jSONObject, JSONObject jSONObject2) {
        wd4.b(jSONObject, "$this$combine");
        wd4.b(jSONObject2, FacebookRequestErrorClassification.KEY_OTHER);
        JSONObject jSONObject3 = new JSONObject();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            try {
                jSONObject3.putOpt(next, jSONObject.get(next));
            } catch (JSONException e) {
                ea0.l.a(e);
            }
        }
        Iterator<String> keys2 = jSONObject2.keys();
        while (keys2.hasNext()) {
            String next2 = keys2.next();
            try {
                jSONObject3.putOpt(next2, jSONObject2.get(next2));
            } catch (JSONException e2) {
                ea0.l.a(e2);
            }
        }
        return jSONObject3;
    }

    @DexIgnore
    public static /* synthetic */ JSONObject a(JSONObject jSONObject, JSONObject jSONObject2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = true;
        }
        a(jSONObject, jSONObject2, z);
        return jSONObject;
    }

    @DexIgnore
    public static final JSONObject a(JSONObject jSONObject, JSONObject jSONObject2, boolean z) {
        wd4.b(jSONObject, "$this$add");
        wd4.b(jSONObject2, FacebookRequestErrorClassification.KEY_OTHER);
        Iterator<String> keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            if (z || !jSONObject.has(next)) {
                try {
                    jSONObject.putOpt(next, jSONObject2.get(next));
                } catch (JSONException e) {
                    ea0.l.a(e);
                }
            }
        }
        return jSONObject;
    }
}
