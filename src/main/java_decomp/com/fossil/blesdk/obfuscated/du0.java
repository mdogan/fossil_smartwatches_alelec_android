package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.su0;
import com.google.android.gms.internal.clearcut.zzbb;
import com.google.android.gms.internal.clearcut.zzbn;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class du0 implements px0 {
    @DexIgnore
    public /* final */ zzbn a;

    @DexIgnore
    public du0(zzbn zzbn) {
        uu0.a(zzbn, "output");
        this.a = zzbn;
        this.a.a = this;
    }

    @DexIgnore
    public static du0 a(zzbn zzbn) {
        du0 du0 = zzbn.a;
        return du0 != null ? du0 : new du0(zzbn);
    }

    @DexIgnore
    public final int a() {
        return su0.e.k;
    }

    @DexIgnore
    public final void a(int i) throws IOException {
        this.a.a(i, 4);
    }

    @DexIgnore
    public final void a(int i, int i2) throws IOException {
        this.a.b(i, i2);
    }

    @DexIgnore
    public final void a(int i, long j) throws IOException {
        this.a.c(i, j);
    }

    @DexIgnore
    public final void a(int i, zzbb zzbb) throws IOException {
        this.a.a(i, zzbb);
    }

    @DexIgnore
    public final void a(int i, Object obj, kw0 kw0) throws IOException {
        this.a.a(i, (tv0) obj, kw0);
    }

    @DexIgnore
    public final void a(int i, List<?> list, kw0 kw0) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            b(i, (Object) list.get(i2), kw0);
        }
    }

    @DexIgnore
    public final void a(int i, boolean z) throws IOException {
        this.a.a(i, z);
    }

    @DexIgnore
    public final void b(int i) throws IOException {
        this.a.a(i, 3);
    }

    @DexIgnore
    public final void b(int i, int i2) throws IOException {
        this.a.e(i, i2);
    }

    @DexIgnore
    public final void b(int i, long j) throws IOException {
        this.a.a(i, j);
    }

    @DexIgnore
    public final void b(int i, Object obj, kw0 kw0) throws IOException {
        zzbn zzbn = this.a;
        zzbn.a(i, 3);
        kw0.a((tv0) obj, (px0) zzbn.a);
        zzbn.a(i, 4);
    }

    @DexIgnore
    public final void b(int i, List<?> list, kw0 kw0) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            a(i, (Object) list.get(i2), kw0);
        }
    }

    @DexIgnore
    public final void zza(int i, double d) throws IOException {
        this.a.a(i, d);
    }

    @DexIgnore
    public final void zza(int i, float f) throws IOException {
        this.a.a(i, f);
    }

    @DexIgnore
    public final void zza(int i, long j) throws IOException {
        this.a.a(i, j);
    }

    @DexIgnore
    public final void zza(int i, Object obj) throws IOException {
        if (obj instanceof zzbb) {
            this.a.b(i, (zzbb) obj);
        } else {
            this.a.b(i, (tv0) obj);
        }
    }

    @DexIgnore
    public final void zza(int i, String str) throws IOException {
        this.a.a(i, str);
    }

    @DexIgnore
    public final void zza(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof ev0) {
            ev0 ev0 = (ev0) list;
            while (i2 < list.size()) {
                Object e = ev0.e(i2);
                if (e instanceof String) {
                    this.a.a(i, (String) e);
                } else {
                    this.a.a(i, (zzbb) e);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2));
            i2++;
        }
    }

    @DexIgnore
    public final void zza(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.f(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.b(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzb(int i, long j) throws IOException {
        this.a.b(i, j);
    }

    @DexIgnore
    public final void zzb(int i, List<zzbb> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.a.a(i, list.get(i2));
        }
    }

    @DexIgnore
    public final void zzb(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.i(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.d(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.e(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzc(int i, int i2) throws IOException {
        this.a.b(i, i2);
    }

    @DexIgnore
    public final void zzc(int i, long j) throws IOException {
        this.a.c(i, j);
    }

    @DexIgnore
    public final void zzc(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.d(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzd(int i, int i2) throws IOException {
        this.a.c(i, i2);
    }

    @DexIgnore
    public final void zzd(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.e(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zze(int i, int i2) throws IOException {
        this.a.d(i, i2);
    }

    @DexIgnore
    public final void zze(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.g(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.c(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.c(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzf(int i, int i2) throws IOException {
        this.a.e(i, i2);
    }

    @DexIgnore
    public final void zzf(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.b(list.get(i4).floatValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).floatValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzg(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.b(list.get(i4).doubleValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzh(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.k(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.b(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzi(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.b(list.get(i4).booleanValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzj(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.g(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.b(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.c(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzk(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.j(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.d(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.e(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzl(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.h(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.c(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.c(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzm(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.h(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.c(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.d(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzn(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzbn.f(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.b(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.b(i, list.get(i2).longValue());
            i2++;
        }
    }
}
