package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class nt<T extends Drawable> implements bq<T>, xp {
    @DexIgnore
    public /* final */ T e;

    @DexIgnore
    public nt(T t) {
        uw.a(t);
        this.e = (Drawable) t;
    }

    @DexIgnore
    public void d() {
        T t = this.e;
        if (t instanceof BitmapDrawable) {
            ((BitmapDrawable) t).getBitmap().prepareToDraw();
        } else if (t instanceof vt) {
            ((vt) t).e().prepareToDraw();
        }
    }

    @DexIgnore
    public final T get() {
        Drawable.ConstantState constantState = this.e.getConstantState();
        if (constantState == null) {
            return this.e;
        }
        return constantState.newDrawable();
    }
}
