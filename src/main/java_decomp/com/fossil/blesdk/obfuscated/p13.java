package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.k73;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p13 extends as2 {
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public k42 j;
    @DexIgnore
    public ur3<ta2> k;
    @DexIgnore
    public k73 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final p13 a(String str) {
            wd4.b(str, "watchAppId");
            p13 p13 = new p13();
            Bundle bundle = new Bundle();
            bundle.putString("EXTRA_WATCHAPP_ID", str);
            p13.setArguments(bundle);
            return p13;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p13 e;

        @DexIgnore
        public b(p13 p13) {
            this.e = p13;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements dc<k73.b> {
        @DexIgnore
        public /* final */ /* synthetic */ p13 a;
        @DexIgnore
        public /* final */ /* synthetic */ ta2 b;

        @DexIgnore
        public c(p13 p13, ta2 ta2) {
            this.a = p13;
            this.b = ta2;
        }

        @DexIgnore
        public final void a(k73.b bVar) {
            Integer a2 = bVar.a();
            if (a2 != null) {
                ((xn) ((xn) sn.a((Fragment) this.a).a(Integer.valueOf(a2.intValue())).a(qp.a)).a(true)).a(this.b.r);
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        ta2 ta2 = (ta2) ra.a(layoutInflater, R.layout.fragment_customize_tutorial, viewGroup, false, O0());
        PortfolioApp.W.c().g().a(new j73()).a(this);
        k42 k42 = this.j;
        if (k42 != null) {
            jc a2 = mc.a((Fragment) this, (lc.b) k42).a(k73.class);
            wd4.a((Object) a2, "ViewModelProviders.of(th\u2026ialViewModel::class.java)");
            this.l = (k73) a2;
            ta2.q.setOnClickListener(new b(this));
            Bundle arguments = getArguments();
            if (arguments != null) {
                k73 k73 = this.l;
                if (k73 != null) {
                    String string = arguments.getString("EXTRA_WATCHAPP_ID");
                    if (string != null) {
                        wd4.a((Object) string, "it.getString(EXTRA_WATCHAPP_ID)!!");
                        k73.b(string);
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.d("mViewModel");
                    throw null;
                }
            }
            k73 k732 = this.l;
            if (k732 != null) {
                k732.c().a(getViewLifecycleOwner(), new c(this, ta2));
                this.k = new ur3<>(this, ta2);
                ur3<ta2> ur3 = this.k;
                if (ur3 != null) {
                    ta2 a3 = ur3.a();
                    if (a3 != null) {
                        wd4.a((Object) a3, "mBinding.get()!!");
                        return a3.d();
                    }
                    wd4.a();
                    throw null;
                }
                wd4.d("mBinding");
                throw null;
            }
            wd4.d("mViewModel");
            throw null;
        }
        wd4.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }
}
