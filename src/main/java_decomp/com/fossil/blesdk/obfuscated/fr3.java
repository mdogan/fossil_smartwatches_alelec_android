package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.hr3;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ua.UAAccessToken;
import com.portfolio.platform.underamour.UASharePref;
import com.portfolio.platform.underamour.UAValues;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fr3 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public dr3 a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements p94<yz1> {
        @DexIgnore
        public /* final */ /* synthetic */ hr3.b a;

        @DexIgnore
        public b(hr3.b bVar) {
            this.a = bVar;
        }

        @DexIgnore
        public final void a(yz1 yz1) {
            if (yz1 != null) {
                FLogger.INSTANCE.getLocal().d(fr3.d + "_login", "jsonObject = " + yz1);
                UASharePref.a(UASharePref.c.a(), (UAAccessToken) new Gson().a((JsonElement) yz1, UAAccessToken.class), false, 2, (Object) null);
                hr3.b bVar = this.a;
                if (bVar != null) {
                    bVar.a();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements p94<Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ hr3.b a;

        @DexIgnore
        public c(hr3.b bVar) {
            this.a = bVar;
        }

        @DexIgnore
        public final void a(Throwable th) {
            if (th != null) {
                hr3.b bVar = this.a;
                if (bVar != null) {
                    bVar.a(th.getMessage());
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements p94<yz1> {
        @DexIgnore
        public /* final */ /* synthetic */ hr3.c a;

        @DexIgnore
        public d(hr3.c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public final void a(yz1 yz1) {
            if (yz1 != null) {
                UASharePref.a(UASharePref.c.a(), (UAAccessToken) null, false, 2, (Object) null);
                hr3.c cVar = this.a;
                if (cVar != null) {
                    cVar.a();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements p94<Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ hr3.c a;

        @DexIgnore
        public e(hr3.c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public final void a(Throwable th) {
            if (th != null) {
                FLogger.INSTANCE.getLocal().d(fr3.d, th.getMessage());
                UASharePref.a(UASharePref.c.a(), (UAAccessToken) null, false, 2, (Object) null);
                hr3.c cVar = this.a;
                if (cVar != null) {
                    cVar.a();
                }
            }
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = fr3.class.getSimpleName();
        wd4.a((Object) simpleName, "UAAuthorizationManager::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public fr3(dr3 dr3, String str, String str2) {
        wd4.b(dr3, "uaApi");
        wd4.b(str, "clientId");
        wd4.b(str2, "clientSecret");
        this.a = dr3;
        this.b = str;
        this.c = str2;
    }

    @DexIgnore
    public final void a(String str, hr3.b bVar) {
        wd4.b(str, "authorizationCode");
        this.a.a(UAValues.GrantType.AUTHORIZATION_CODE.getValue(), this.b, this.c, str, UAValues.ContentType.X_WWW_FORM_URLENCODED.getValue(), this.b).a(3).a(h94.a()).a(new b(bVar), (p94<? super Throwable>) new c(bVar));
    }

    @DexIgnore
    public final boolean b() {
        UAAccessToken b2 = UASharePref.c.a().b();
        Long expiresAt = b2 != null ? b2.getExpiresAt() : null;
        if (expiresAt != null) {
            return expiresAt.longValue() - System.currentTimeMillis() <= UAValues.b.a();
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(hr3.c cVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        StringBuilder sb = new StringBuilder();
        String a2 = UASharePref.c.a().a();
        if (a2 != null) {
            sb.append(a2);
            sb.append("|");
            UAAccessToken b2 = UASharePref.c.a().b();
            sb.append(b2 != null ? b2.getAccessToken() : null);
            sb.append("|");
            UAAccessToken b3 = UASharePref.c.a().b();
            sb.append(b3 != null ? b3.getRefreshToken() : null);
            local.d(str, sb.toString());
            UAAccessToken b4 = UASharePref.c.a().b();
            if ((b4 != null ? b4.getUserId() : null) != null) {
                dr3 dr3 = this.a;
                UAAccessToken b5 = UASharePref.c.a().b();
                String userId = b5 != null ? b5.getUserId() : null;
                if (userId != null) {
                    String str2 = this.b;
                    be4 be4 = be4.a;
                    Locale locale = Locale.US;
                    wd4.a((Object) locale, "Locale.US");
                    String value = UAValues.Authorization.BEARER.getValue();
                    Object[] objArr = {UASharePref.c.a().a()};
                    String format = String.format(locale, value, Arrays.copyOf(objArr, objArr.length));
                    wd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
                    dr3.a(userId, str2, str2, format).a(3).a(h94.a()).a(new d(cVar), (p94<? super Throwable>) new e(cVar));
                    return;
                }
                wd4.a();
                throw null;
            }
            UASharePref.a(UASharePref.c.a(), (UAAccessToken) null, false, 2, (Object) null);
            if (cVar != null) {
                cVar.a();
                return;
            }
            return;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public final z84<yz1> a() {
        if (b()) {
            FLogger.INSTANCE.getLocal().d(d, "access token expired. Need to get new access token");
            dr3 dr3 = this.a;
            String value = UAValues.GrantType.REFRESH_TOKEN.getValue();
            String str = this.b;
            String str2 = this.c;
            UAAccessToken b2 = UASharePref.c.a().b();
            String refreshToken = b2 != null ? b2.getRefreshToken() : null;
            if (refreshToken != null) {
                String value2 = UAValues.ContentType.X_WWW_FORM_URLENCODED.getValue();
                String str3 = this.b;
                be4 be4 = be4.a;
                Locale locale = Locale.US;
                wd4.a((Object) locale, "Locale.US");
                String value3 = UAValues.Authorization.BEARER.getValue();
                Object[] objArr = {UASharePref.c.a().a()};
                String format = String.format(locale, value3, Arrays.copyOf(objArr, objArr.length));
                wd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
                z84<yz1> a2 = dr3.a(value, str, str2, refreshToken, value2, str3, format).a(3).a(h94.a());
                wd4.a((Object) a2, "uaApi.refreshAccessToken\u2026dSchedulers.mainThread())");
                return a2;
            }
            wd4.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(d, "access token does not expire. Use old access token");
        JsonElement a3 = new zz1().a(new Gson().a((Object) UASharePref.c.a().b()));
        wd4.a((Object) a3, "JsonParser().parse(Gson(\u2026ance.getUAAccessToken()))");
        z84<yz1> a4 = z84.a(a3.d());
        wd4.a((Object) a4, "Observable.just(JsonPars\u2026ssToken())).asJsonObject)");
        return a4;
    }
}
