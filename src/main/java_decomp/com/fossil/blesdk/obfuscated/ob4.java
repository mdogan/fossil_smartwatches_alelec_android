package com.fossil.blesdk.obfuscated;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.collections.EmptyList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ob4 extends nb4 {
    @DexIgnore
    public static final <T> List<T> a() {
        return EmptyList.INSTANCE;
    }

    @DexIgnore
    public static final <T> Collection<T> b(T[] tArr) {
        wd4.b(tArr, "$this$asCollection");
        return new hb4(tArr, false);
    }

    @DexIgnore
    public static final <T> List<T> c(T... tArr) {
        wd4.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return tArr.length > 0 ? kb4.a(tArr) : a();
    }

    @DexIgnore
    public static final <T> List<T> d(T... tArr) {
        wd4.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return tArr.length == 0 ? new ArrayList() : new ArrayList(new hb4(tArr, true));
    }

    @DexIgnore
    public static final <T> ArrayList<T> a(T... tArr) {
        wd4.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return tArr.length == 0 ? new ArrayList<>() : new ArrayList<>(new hb4(tArr, true));
    }

    @DexIgnore
    public static final <T> List<T> b(List<? extends T> list) {
        wd4.b(list, "$this$optimizeReadOnlyList");
        int size = list.size();
        if (size == 0) {
            return a();
        }
        if (size != 1) {
            return list;
        }
        return nb4.a(list.get(0));
    }

    @DexIgnore
    public static final void c() {
        throw new ArithmeticException("Index overflow has happened.");
    }

    @DexIgnore
    public static final ke4 a(Collection<?> collection) {
        wd4.b(collection, "$this$indices");
        return new ke4(0, collection.size() - 1);
    }

    @DexIgnore
    public static final <T> int a(List<? extends T> list) {
        wd4.b(list, "$this$lastIndex");
        return list.size() - 1;
    }

    @DexIgnore
    public static final void b() {
        throw new ArithmeticException("Count overflow has happened.");
    }
}
