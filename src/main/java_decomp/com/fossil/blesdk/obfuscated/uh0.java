package com.fossil.blesdk.obfuscated;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.we0;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class uh0<T> extends gh0 {
    @DexIgnore
    public /* final */ yn1<T> a;

    @DexIgnore
    public uh0(int i, yn1<T> yn1) {
        super(i);
        this.a = yn1;
    }

    @DexIgnore
    public void a(Status status) {
        this.a.b((Exception) new ApiException(status));
    }

    @DexIgnore
    public abstract void d(we0.a<?> aVar) throws RemoteException;

    @DexIgnore
    public void a(RuntimeException runtimeException) {
        this.a.b((Exception) runtimeException);
    }

    @DexIgnore
    public final void a(we0.a<?> aVar) throws DeadObjectException {
        try {
            d(aVar);
        } catch (DeadObjectException e) {
            a(jg0.a((RemoteException) e));
            throw e;
        } catch (RemoteException e2) {
            a(jg0.a(e2));
        } catch (RuntimeException e3) {
            a(e3);
        }
    }
}
