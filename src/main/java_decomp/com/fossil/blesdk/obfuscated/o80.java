package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.LegacyFileControlStatusCode;
import com.fossil.blesdk.device.logic.request.legacy.LegacyFileControlOperationCode;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o80 extends g70 {
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId G;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId H;
    @DexIgnore
    public /* final */ byte[] I;
    @DexIgnore
    public byte[] J;
    @DexIgnore
    public /* final */ boolean K;
    @DexIgnore
    public GattCharacteristic.CharacteristicId L;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ o80(short s, Peripheral peripheral, int i, int i2, rd4 rd4) {
        this(s, peripheral, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    public GattCharacteristic.CharacteristicId B() {
        return this.H;
    }

    @DexIgnore
    public byte[] D() {
        return this.I;
    }

    @DexIgnore
    public GattCharacteristic.CharacteristicId E() {
        return this.G;
    }

    @DexIgnore
    public boolean F() {
        return this.K;
    }

    @DexIgnore
    public byte[] G() {
        return this.J;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        wd4.b(bArr, "responseData");
        JSONObject jSONObject = new JSONObject();
        c(n().getResultCode() != Request.Result.ResultCode.SUCCESS);
        return jSONObject;
    }

    @DexIgnore
    public p70 b(byte b) {
        return LegacyFileControlStatusCode.Companion.a(b);
    }

    @DexIgnore
    public boolean c(d20 d20) {
        wd4.b(d20, "characteristicChangeNotification");
        return d20.a() == this.L;
    }

    @DexIgnore
    public void g(d20 d20) {
        byte[] bArr;
        wd4.b(d20, "characteristicChangedNotification");
        if (g()) {
            bArr = e90.b.a(i().k(), this.L, d20.b());
        } else {
            bArr = d20.b();
        }
        if (bArr.length == 0) {
            b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.RESPONSE_ERROR, (BluetoothCommand.Result) null, (p70) null, 13, (Object) null));
        }
        c(true);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o80(short s, Peripheral peripheral, int i) {
        super(RequestId.LEGACY_CLOSE_CURRENT_ACTIVITY_FILE, peripheral, i);
        wd4.b(peripheral, "peripheral");
        GattCharacteristic.CharacteristicId characteristicId = GattCharacteristic.CharacteristicId.FTC;
        this.G = characteristicId;
        this.H = characteristicId;
        byte[] array = ByteBuffer.allocate(11).order(ByteOrder.LITTLE_ENDIAN).put(LegacyFileControlOperationCode.LEGACY_GET_FILE.getCode()).putShort(s).putInt(0).putInt(1).array();
        wd4.a((Object) array, "ByteBuffer.allocate(11)\n\u2026ILE)\n            .array()");
        this.I = array;
        byte[] array2 = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).put(LegacyFileControlOperationCode.LEGACY_GET_FILE.responseCode()).put((byte) 0).putShort(s).array();
        wd4.a((Object) array2, "ByteBuffer.allocate(4)\n \u2026dle)\n            .array()");
        this.J = array2;
        this.L = GattCharacteristic.CharacteristicId.FTD;
    }
}
