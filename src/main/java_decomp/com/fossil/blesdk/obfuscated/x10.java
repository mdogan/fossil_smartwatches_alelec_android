package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x10 extends GattOperationResult {
    @DexIgnore
    public /* final */ UUID[] b;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId[] c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x10(GattOperationResult.GattResult gattResult, UUID[] uuidArr, GattCharacteristic.CharacteristicId[] characteristicIdArr) {
        super(gattResult);
        wd4.b(gattResult, "gattResult");
        wd4.b(uuidArr, "discoveredServiceUUIDs");
        wd4.b(characteristicIdArr, "discoveredCharacteristicIds");
        this.b = uuidArr;
        this.c = characteristicIdArr;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId[] b() {
        return this.c;
    }

    @DexIgnore
    public final UUID[] c() {
        return this.b;
    }
}
