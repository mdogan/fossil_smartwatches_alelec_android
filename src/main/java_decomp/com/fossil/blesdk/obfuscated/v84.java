package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class v84<T> implements lq4<T> {
    @DexIgnore
    public static /* final */ int a; // = Math.max(1, Integer.getInteger("rx2.buffer-size", 128).intValue());

    @DexIgnore
    public static int d() {
        return a;
    }

    @DexIgnore
    public final v84<T> a() {
        return a(d(), false, true);
    }

    @DexIgnore
    public final v84<T> b() {
        return ta4.a(new ha4(this));
    }

    @DexIgnore
    public final v84<T> c() {
        return ta4.a(new ja4(this));
    }

    @DexIgnore
    public final v84<T> a(int i, boolean z, boolean z2) {
        v94.a(i, "bufferSize");
        return ta4.a(new ga4(this, i, z2, z, u94.a));
    }
}
