package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class po1<TResult, TContinuationResult> implements rn1, tn1, un1<TContinuationResult>, ro1<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ wn1<TResult, TContinuationResult> b;
    @DexIgnore
    public /* final */ vo1<TContinuationResult> c;

    @DexIgnore
    public po1(Executor executor, wn1<TResult, TContinuationResult> wn1, vo1<TContinuationResult> vo1) {
        this.a = executor;
        this.b = wn1;
        this.c = vo1;
    }

    @DexIgnore
    public final void onCanceled() {
        this.c.f();
    }

    @DexIgnore
    public final void onComplete(xn1<TResult> xn1) {
        this.a.execute(new qo1(this, xn1));
    }

    @DexIgnore
    public final void onFailure(Exception exc) {
        this.c.a(exc);
    }

    @DexIgnore
    public final void onSuccess(TContinuationResult tcontinuationresult) {
        this.c.a(tcontinuationresult);
    }
}
