package com.fossil.blesdk.obfuscated;

import bolts.UnobservedTaskException;
import com.fossil.blesdk.obfuscated.jm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lm {
    @DexIgnore
    public jm<?> a;

    @DexIgnore
    public lm(jm<?> jmVar) {
        this.a = jmVar;
    }

    @DexIgnore
    public void a() {
        this.a = null;
    }

    @DexIgnore
    public void finalize() throws Throwable {
        try {
            jm<?> jmVar = this.a;
            if (jmVar != null) {
                jm.g j = jm.j();
                if (j != null) {
                    j.a(jmVar, new UnobservedTaskException(jmVar.a()));
                }
            }
        } finally {
            super.finalize();
        }
    }
}
