package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class la3 {
    @DexIgnore
    public /* final */ ga3 a;
    @DexIgnore
    public /* final */ va3 b;
    @DexIgnore
    public /* final */ qa3 c;

    @DexIgnore
    public la3(ga3 ga3, va3 va3, qa3 qa3) {
        wd4.b(ga3, "mCaloriesOverviewDayView");
        wd4.b(va3, "mCaloriesOverviewWeekView");
        wd4.b(qa3, "mCaloriesOverviewMonthView");
        this.a = ga3;
        this.b = va3;
        this.c = qa3;
    }

    @DexIgnore
    public final ga3 a() {
        return this.a;
    }

    @DexIgnore
    public final qa3 b() {
        return this.c;
    }

    @DexIgnore
    public final va3 c() {
        return this.b;
    }
}
