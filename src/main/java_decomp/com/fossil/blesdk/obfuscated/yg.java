package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@SuppressLint({"ViewConstructor"})
public class yg extends View implements ah {
    @DexIgnore
    public /* final */ View e;
    @DexIgnore
    public ViewGroup f;
    @DexIgnore
    public View g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public Matrix k;
    @DexIgnore
    public /* final */ Matrix l; // = new Matrix();
    @DexIgnore
    public /* final */ ViewTreeObserver.OnPreDrawListener m; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ViewTreeObserver.OnPreDrawListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onPreDraw() {
            yg ygVar = yg.this;
            ygVar.k = ygVar.e.getMatrix();
            g9.C(yg.this);
            yg ygVar2 = yg.this;
            ViewGroup viewGroup = ygVar2.f;
            if (viewGroup == null) {
                return true;
            }
            View view = ygVar2.g;
            if (view == null) {
                return true;
            }
            viewGroup.endViewTransition(view);
            g9.C(yg.this.f);
            yg ygVar3 = yg.this;
            ygVar3.f = null;
            ygVar3.g = null;
            return true;
        }
    }

    @DexIgnore
    public yg(View view) {
        super(view.getContext());
        this.e = view;
        setLayerType(2, (Paint) null);
    }

    @DexIgnore
    public static ah a(View view, ViewGroup viewGroup) {
        yg a2 = a(view);
        if (a2 == null) {
            FrameLayout a3 = a(viewGroup);
            if (a3 == null) {
                return null;
            }
            a2 = new yg(view);
            a3.addView(a2);
        }
        a2.h++;
        return a2;
    }

    @DexIgnore
    public static void b(View view) {
        yg a2 = a(view);
        if (a2 != null) {
            a2.h--;
            if (a2.h <= 0) {
                ViewParent parent = a2.getParent();
                if (parent instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) parent;
                    viewGroup.endViewTransition(a2);
                    viewGroup.removeView(a2);
                }
            }
        }
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        a(this.e, this);
        int[] iArr = new int[2];
        int[] iArr2 = new int[2];
        getLocationOnScreen(iArr);
        this.e.getLocationOnScreen(iArr2);
        iArr2[0] = (int) (((float) iArr2[0]) - this.e.getTranslationX());
        iArr2[1] = (int) (((float) iArr2[1]) - this.e.getTranslationY());
        this.i = iArr2[0] - iArr[0];
        this.j = iArr2[1] - iArr[1];
        this.e.getViewTreeObserver().addOnPreDrawListener(this.m);
        this.e.setVisibility(4);
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.e.getViewTreeObserver().removeOnPreDrawListener(this.m);
        this.e.setVisibility(0);
        a(this.e, (yg) null);
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        this.l.set(this.k);
        this.l.postTranslate((float) this.i, (float) this.j);
        canvas.setMatrix(this.l);
        this.e.draw(canvas);
    }

    @DexIgnore
    public void setVisibility(int i2) {
        super.setVisibility(i2);
        this.e.setVisibility(i2 == 0 ? 4 : 0);
    }

    @DexIgnore
    public static FrameLayout a(ViewGroup viewGroup) {
        while (!(viewGroup instanceof FrameLayout)) {
            ViewParent parent = viewGroup.getParent();
            if (!(parent instanceof ViewGroup)) {
                return null;
            }
            viewGroup = (ViewGroup) parent;
        }
        return (FrameLayout) viewGroup;
    }

    @DexIgnore
    public void a(ViewGroup viewGroup, View view) {
        this.f = viewGroup;
        this.g = view;
    }

    @DexIgnore
    public static void a(View view, yg ygVar) {
        view.setTag(hh.ghost_view, ygVar);
    }

    @DexIgnore
    public static yg a(View view) {
        return (yg) view.getTag(hh.ghost_view);
    }
}
