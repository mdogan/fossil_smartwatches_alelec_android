package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.rd;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wd<T> extends rd<T> {
    @DexIgnore
    public /* final */ boolean s;
    @DexIgnore
    public /* final */ Object t;
    @DexIgnore
    public /* final */ md<?, T> u;

    @DexIgnore
    public wd(rd<T> rdVar) {
        super(rdVar.i.n(), rdVar.e, rdVar.f, (rd.c) null, rdVar.h);
        this.u = rdVar.d();
        this.s = rdVar.g();
        this.j = rdVar.j;
        this.t = rdVar.e();
    }

    @DexIgnore
    public void a(rd<T> rdVar, rd.e eVar) {
    }

    @DexIgnore
    public md<?, T> d() {
        return this.u;
    }

    @DexIgnore
    public Object e() {
        return this.t;
    }

    @DexIgnore
    public boolean g() {
        return this.s;
    }

    @DexIgnore
    public void h(int i) {
    }

    @DexIgnore
    public boolean h() {
        return true;
    }

    @DexIgnore
    public boolean i() {
        return true;
    }
}
