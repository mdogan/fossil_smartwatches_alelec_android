package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.content.Context;
import androidx.work.ListenableWorker;
import com.fossil.blesdk.obfuscated.n44;
import com.google.common.collect.ImmutableMap;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.CloudImageHelper_MembersInjector;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper_MembersInjector;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.LocationSource_Factory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository_Factory;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.ActivitiesRepository_Factory;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.AlarmsRepository_Factory;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.CategoryRepository_Factory;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository_Factory;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.ComplicationRepository_Factory;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceDatabase;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DeviceRepository_Factory;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaPresetRepository_Factory;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.FitnessDataRepository_Factory;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository_Factory;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository_Factory;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository_Factory;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository_Factory;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository_Factory;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.MicroAppRepository_Factory;
import com.portfolio.platform.data.source.NotificationsDataSource;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.NotificationsRepositoryModule;
import com.portfolio.platform.data.source.NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory;
import com.portfolio.platform.data.source.NotificationsRepository_Factory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideActivitySampleDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideActivitySummaryDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAlarmsLocalDataSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAlarmsRemoteDataSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCategoryDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCategoryDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideComplicationDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideComplicationSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDeviceDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDeviceDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDianaCustomizeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDianaPresetDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFitnessDataDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFitnessDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFitnessHelperFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideGoalTrackingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideGoalTrackingDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideHeartRateDailySummaryDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideHeartRateDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideMicroAppDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidePresetDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSampleRawDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSkuDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSleepDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSleepDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideThirdPartyDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideWatchAppDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideWatchAppSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideWorkoutDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesAlarmDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesAlarmDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesWatchFaceDaoFactory;
import com.portfolio.platform.data.source.RepositoriesModule;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideComplicationRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideServerSettingLocalDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideUserLocalDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideUserRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.SkuDao;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository_Factory;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository_Factory;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository_Factory;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository_Factory;
import com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule;
import com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory;
import com.portfolio.platform.data.source.UserDataSource;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.UserRepository_Factory;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository_Factory;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchAppRepository_Factory;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchFaceRepository_Factory;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository_Factory;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository_Factory;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository_Factory;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import com.portfolio.platform.data.source.loader.NotificationsLoader_Factory;
import com.portfolio.platform.data.source.local.AlarmsLocalDataSource;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.local.CategoryDatabase;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.alarm.AlarmDao;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import com.portfolio.platform.data.source.local.diana.ComplicationDao;
import com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.DianaPresetDao;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao;
import com.portfolio.platform.data.source.local.diana.WatchFaceDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.fitness.ActivitySampleDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.fitness.SampleRawDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository_Factory;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import com.portfolio.platform.data.source.remote.DeviceRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource_Factory;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.helper.WatchParamHelper;
import com.portfolio.platform.manager.LightAndHapticsManager;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.manager.WeatherManager;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.migration.MigrationHelper;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.portfolio.platform.receiver.AppPackageRemoveReceiver;
import com.portfolio.platform.receiver.BootReceiver;
import com.portfolio.platform.receiver.LocaleChangedReceiver;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import com.portfolio.platform.service.fcm.FossilFirebaseInstanceIDService;
import com.portfolio.platform.service.fcm.FossilFirebaseMessagingService;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import com.portfolio.platform.service.notification.HybridMessageNotificationComponent;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginEmailUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.alarm.AlarmPresenter;
import com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsActivity;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.HomePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.uirenew.home.profile.about.AboutActivity;
import com.portfolio.platform.uirenew.home.profile.battery.ReplaceBatteryActivity;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditActivity;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpPresenter;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountActivity;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInActivity;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter;
import com.portfolio.platform.uirenew.home.profile.password.ProfileChangePasswordActivity;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitActivity;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter;
import com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;
import com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationActivity;
import com.portfolio.platform.uirenew.splash.SplashPresenter;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.usecase.CheckAuthenticationEmailExisting;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import com.portfolio.platform.usecase.GetWeather;
import com.portfolio.platform.usecase.RequestEmailOtp;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import com.portfolio.platform.usecase.VerifyEmailOtp;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import com.portfolio.platform.util.DeviceUtils;
import com.portfolio.platform.util.UserUtils;
import com.portfolio.platform.workers.PushPendingDataWorker;
import com.portfolio.platform.workers.TimeChangeReceiver;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y52 implements m42 {
    @DexIgnore
    public Provider<AlarmsRemoteDataSource> A;
    @DexIgnore
    public Provider<WorkoutDao> A0;
    @DexIgnore
    public ComplicationLastSettingRepository_Factory A1;
    @DexIgnore
    public Provider<AlarmsRepository> B;
    @DexIgnore
    public Provider<WorkoutSessionRepository> B0;
    @DexIgnore
    public WatchAppLastSettingRepository_Factory B1;
    @DexIgnore
    public Provider<AlarmHelper> C;
    @DexIgnore
    public Provider<RemindersSettingsDatabase> C0;
    @DexIgnore
    public c23 C1;
    @DexIgnore
    public Provider<FitnessDatabase> D;
    @DexIgnore
    public Provider<ThirdPartyDatabase> D0;
    @DexIgnore
    public g63 D1;
    @DexIgnore
    public Provider<ActivitySummaryDao> E;
    @DexIgnore
    public Provider<AnalyticsHelper> E0;
    @DexIgnore
    public qr2 E1;
    @DexIgnore
    public Provider<FitnessDataDao> F;
    @DexIgnore
    public Provider<CategoryDatabase> F0;
    @DexIgnore
    public pr2 F1;
    @DexIgnore
    public Provider<yk2> G;
    @DexIgnore
    public Provider<CategoryDao> G0;
    @DexIgnore
    public sh3 G1;
    @DexIgnore
    public Provider<SummariesRepository> H;
    @DexIgnore
    public CategoryRemoteDataSource_Factory H0;
    @DexIgnore
    public xq2 H1;
    @DexIgnore
    public Provider<SleepDatabase> I;
    @DexIgnore
    public CategoryRepository_Factory I0;
    @DexIgnore
    public cr2 I1;
    @DexIgnore
    public Provider<SleepDao> J;
    @DexIgnore
    public WatchAppRepository_Factory J0;
    @DexIgnore
    public yq2 J1;
    @DexIgnore
    public Provider<SleepSummariesRepository> K;
    @DexIgnore
    public ComplicationRepository_Factory K0;
    @DexIgnore
    public yp3 K1;
    @DexIgnore
    public Provider<GuestApiService> L;
    @DexIgnore
    public Provider<WatchFaceDao> L0;
    @DexIgnore
    public h53 L1;
    @DexIgnore
    public Provider<i42> M;
    @DexIgnore
    public WatchFaceRemoteDataSource_Factory M0;
    @DexIgnore
    public e53 M1;
    @DexIgnore
    public Provider<NotificationsDataSource> N;
    @DexIgnore
    public WatchFaceRepository_Factory N0;
    @DexIgnore
    public Provider<Map<Class<? extends jc>, Provider<jc>>> N1;
    @DexIgnore
    public Provider<NotificationsRepository> O;
    @DexIgnore
    public xy2 O0;
    @DexIgnore
    public Provider<k42> O1;
    @DexIgnore
    public Provider<DeviceDatabase> P;
    @DexIgnore
    public WatchLocalizationRepository_Factory P0;
    @DexIgnore
    public Provider<NotificationSettingsDao> P1;
    @DexIgnore
    public Provider<DeviceDao> Q;
    @DexIgnore
    public rn3 Q0;
    @DexIgnore
    public Provider<c62> Q1;
    @DexIgnore
    public Provider<SkuDao> R;
    @DexIgnore
    public sn3 R0;
    @DexIgnore
    public Provider<MigrationHelper> R1;
    @DexIgnore
    public DeviceRemoteDataSource_Factory S;
    @DexIgnore
    public sq2 S0;
    @DexIgnore
    public Provider<ServerSettingDataSource> S1;
    @DexIgnore
    public Provider<DeviceRepository> T;
    @DexIgnore
    public mr3 T0;
    @DexIgnore
    public Provider<ServerSettingDataSource> T1;
    @DexIgnore
    public Provider<ContentResolver> U;
    @DexIgnore
    public rr3 U0;
    @DexIgnore
    public Provider<UserUtils> U1;
    @DexIgnore
    public Provider<k62> V;
    @DexIgnore
    public er2 V0;
    @DexIgnore
    public Provider<HybridCustomizeDatabase> W;
    @DexIgnore
    public rq2 W0;
    @DexIgnore
    public Provider<HybridPresetDao> X;
    @DexIgnore
    public xj2 X0;
    @DexIgnore
    public HybridPresetRemoteDataSource_Factory Y;
    @DexIgnore
    public Provider<ApplicationEventListener> Y0;
    @DexIgnore
    public Provider<HybridPresetRepository> Z;
    @DexIgnore
    public Provider<ShakeFeedbackService> Z0;
    @DexIgnore
    public o42 a;
    @DexIgnore
    public Provider<SampleRawDao> a0;
    @DexIgnore
    public FitnessDataRepository_Factory a1;
    @DexIgnore
    public Provider<Context> b;
    @DexIgnore
    public Provider<ActivitySampleDao> b0;
    @DexIgnore
    public b52 b1;
    @DexIgnore
    public Provider<fn2> c;
    @DexIgnore
    public Provider<ActivitiesRepository> c0;
    @DexIgnore
    public Provider<ThirdPartyRepository> c1;
    @DexIgnore
    public Provider<PortfolioApp> d;
    @DexIgnore
    public Provider<ShortcutApiService> d0;
    @DexIgnore
    public ou3 d1;
    @DexIgnore
    public Provider<DNDSettingsDatabase> e;
    @DexIgnore
    public Provider<MicroAppSettingDataSource> e0;
    @DexIgnore
    public Provider<ko2> e1;
    @DexIgnore
    public Provider<NotificationSettingsDatabase> f;
    @DexIgnore
    public Provider<MicroAppSettingDataSource> f0;
    @DexIgnore
    public Provider<SmsMmsReceiver> f1;
    @DexIgnore
    public Provider<DianaNotificationComponent> g;
    @DexIgnore
    public Provider<MicroAppSettingRepository> g0;
    @DexIgnore
    public qr3 g1;
    @DexIgnore
    public Provider<DianaCustomizeDatabase> h;
    @DexIgnore
    public Provider<SleepSessionsRepository> h0;
    @DexIgnore
    public Provider<LinkStreamingManager> h1;
    @DexIgnore
    public Provider<DianaPresetDao> i;
    @DexIgnore
    public Provider<GoalTrackingDatabase> i0;
    @DexIgnore
    public Provider<WatchParamHelper> i1;
    @DexIgnore
    public Provider<en2> j;
    @DexIgnore
    public Provider<GoalTrackingDao> j0;
    @DexIgnore
    public Provider<HybridMessageNotificationComponent> j1;
    @DexIgnore
    public Provider<AuthApiGuestService> k;
    @DexIgnore
    public Provider<GoalTrackingRepository> k0;
    @DexIgnore
    public Provider<jn2> k1;
    @DexIgnore
    public Provider<yo2> l;
    @DexIgnore
    public Provider<kn2> l0;
    @DexIgnore
    public Provider<ln2> l1;
    @DexIgnore
    public Provider<cp2> m;
    @DexIgnore
    public Provider<WatchAppDao> m0;
    @DexIgnore
    public Provider<MFLoginWechatManager> m1;
    @DexIgnore
    public Provider<ApiServiceV2> n;
    @DexIgnore
    public Provider<WatchAppRemoteDataSource> n0;
    @DexIgnore
    public MicroAppLastSettingRepository_Factory n1;
    @DexIgnore
    public Provider<DianaPresetRemoteDataSource> o;
    @DexIgnore
    public Provider<ComplicationDao> o0;
    @DexIgnore
    public Provider<MigrationManager> o1;
    @DexIgnore
    public Provider<DianaPresetRepository> p;
    @DexIgnore
    public Provider<ComplicationRemoteDataSource> p0;
    @DexIgnore
    public Provider<FirmwareFileRepository> p1;
    @DexIgnore
    public Provider<CustomizeRealDataDatabase> q;
    @DexIgnore
    public Provider<MicroAppDao> q0;
    @DexIgnore
    public Provider<mr2> q1;
    @DexIgnore
    public Provider<CustomizeRealDataDao> r;
    @DexIgnore
    public MicroAppRemoteDataSource_Factory r0;
    @DexIgnore
    public Provider<LocationSource> r1;
    @DexIgnore
    public Provider<CustomizeRealDataRepository> s;
    @DexIgnore
    public Provider<MicroAppRepository> s0;
    @DexIgnore
    public Provider<InAppNotificationDatabase> s1;
    @DexIgnore
    public Provider<AuthApiUserService> t;
    @DexIgnore
    public Provider<MicroAppLastSettingDao> t0;
    @DexIgnore
    public Provider<InAppNotificationDao> t1;
    @DexIgnore
    public Provider<UserDataSource> u;
    @DexIgnore
    public Provider<ComplicationLastSettingDao> u0;
    @DexIgnore
    public Provider<InAppNotificationRepository> u1;
    @DexIgnore
    public Provider<UserDataSource> v;
    @DexIgnore
    public Provider<WatchAppLastSettingDao> v0;
    @DexIgnore
    public Provider<bk3> v1;
    @DexIgnore
    public Provider<UserRepository> w;
    @DexIgnore
    public Provider<HeartRateSampleDao> w0;
    @DexIgnore
    public Provider<sc> w1;
    @DexIgnore
    public Provider<AlarmDatabase> x;
    @DexIgnore
    public Provider<HeartRateSampleRepository> x0;
    @DexIgnore
    public Provider<GoogleApiService> x1;
    @DexIgnore
    public Provider<AlarmDao> y;
    @DexIgnore
    public Provider<HeartRateDailySummaryDao> y0;
    @DexIgnore
    public Provider<rl2> y1;
    @DexIgnore
    public Provider<AlarmsLocalDataSource> z;
    @DexIgnore
    public Provider<HeartRateSummaryRepository> z0;
    @DexIgnore
    public Provider<g42> z1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a0 implements nk3 {
        @DexIgnore
        public qk3 a;

        @DexIgnore
        public final ExploreWatchPresenter a() {
            ExploreWatchPresenter a2 = sk3.a(rk3.a(this.a), y52.this.l(), (DeviceRepository) y52.this.T.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ExploreWatchActivity b(ExploreWatchActivity exploreWatchActivity) {
            eq2.a((BaseActivity) exploreWatchActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) exploreWatchActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) exploreWatchActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) exploreWatchActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) exploreWatchActivity, new hr2());
            mk3.a(exploreWatchActivity, a());
            return exploreWatchActivity;
        }

        @DexIgnore
        public a0(qk3 qk3) {
            a(qk3);
        }

        @DexIgnore
        public final void a(qk3 qk3) {
            o44.a(qk3);
            this.a = qk3;
        }

        @DexIgnore
        public void a(ExploreWatchActivity exploreWatchActivity) {
            b(exploreWatchActivity);
        }

        @DexIgnore
        public final ExploreWatchPresenter a(ExploreWatchPresenter exploreWatchPresenter) {
            tk3.a(exploreWatchPresenter);
            return exploreWatchPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a1 implements bn3 {
        @DexIgnore
        public in3 a;

        @DexIgnore
        public final DownloadFirmwareByDeviceModelUsecase a() {
            return new DownloadFirmwareByDeviceModelUsecase((PortfolioApp) y52.this.d.get(), (GuestApiService) y52.this.L.get());
        }

        @DexIgnore
        public final LinkDeviceUseCase b() {
            return new LinkDeviceUseCase((DeviceRepository) y52.this.T.get(), a(), (fn2) y52.this.c.get(), y52.this.l());
        }

        @DexIgnore
        public final PairingPresenter c() {
            PairingPresenter a2 = ln3.a(jn3.a(this.a), b(), (DeviceRepository) y52.this.T.get(), (NotificationsRepository) y52.this.O.get(), y52.this.y(), new qx2(), y52.this.l(), (NotificationSettingsDatabase) y52.this.f.get(), y52.this.P(), (fn2) y52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public a1(in3 in3) {
            a(in3);
        }

        @DexIgnore
        public final void a(in3 in3) {
            o44.a(in3);
            this.a = in3;
        }

        @DexIgnore
        public void a(PairingActivity pairingActivity) {
            b(pairingActivity);
        }

        @DexIgnore
        public final PairingPresenter a(PairingPresenter pairingPresenter) {
            mn3.a(pairingPresenter);
            return pairingPresenter;
        }

        @DexIgnore
        public final PairingActivity b(PairingActivity pairingActivity) {
            eq2.a((BaseActivity) pairingActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) pairingActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) pairingActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) pairingActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) pairingActivity, new hr2());
            an3.a(pairingActivity, c());
            return pairingActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements tg3 {
        @DexIgnore
        public xg3 a;

        @DexIgnore
        public final zg3 a() {
            zg3 a2 = ah3.a(yg3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final AboutActivity b(AboutActivity aboutActivity) {
            eq2.a((BaseActivity) aboutActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) aboutActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) aboutActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) aboutActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) aboutActivity, new hr2());
            sg3.a(aboutActivity, a());
            return aboutActivity;
        }

        @DexIgnore
        public b(xg3 xg3) {
            a(xg3);
        }

        @DexIgnore
        public final void a(xg3 xg3) {
            o44.a(xg3);
            this.a = xg3;
        }

        @DexIgnore
        public void a(AboutActivity aboutActivity) {
            b(aboutActivity);
        }

        @DexIgnore
        public final zg3 a(zg3 zg3) {
            bh3.a(zg3);
            return zg3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b0 implements kq3 {
        @DexIgnore
        public nq3 a;

        @DexIgnore
        public final FindDevicePresenter a() {
            FindDevicePresenter a2 = pq3.a((sc) y52.this.w1.get(), (DeviceRepository) y52.this.T.get(), (fn2) y52.this.c.get(), oq3.a(this.a), new fr2(), c(), b(), new hr2(), (PortfolioApp) y52.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final GetAddress b() {
            return new GetAddress((GoogleApiService) y52.this.x1.get());
        }

        @DexIgnore
        public final gr2 c() {
            return new gr2((g42) y52.this.z1.get());
        }

        @DexIgnore
        public b0(nq3 nq3) {
            a(nq3);
        }

        @DexIgnore
        public final FindDeviceActivity b(FindDeviceActivity findDeviceActivity) {
            eq2.a((BaseActivity) findDeviceActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) findDeviceActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) findDeviceActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) findDeviceActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) findDeviceActivity, new hr2());
            jq3.a(findDeviceActivity, a());
            return findDeviceActivity;
        }

        @DexIgnore
        public final void a(nq3 nq3) {
            o44.a(nq3);
            this.a = nq3;
        }

        @DexIgnore
        public void a(FindDeviceActivity findDeviceActivity) {
            b(findDeviceActivity);
        }

        @DexIgnore
        public final FindDevicePresenter a(FindDevicePresenter findDevicePresenter) {
            qq3.a(findDevicePresenter);
            return findDevicePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b1 implements rm3 {
        @DexIgnore
        public um3 a;

        @DexIgnore
        public final wm3 a() {
            wm3 a2 = xm3.a(vm3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final PairingInstructionsActivity b(PairingInstructionsActivity pairingInstructionsActivity) {
            eq2.a((BaseActivity) pairingInstructionsActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) pairingInstructionsActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) pairingInstructionsActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) pairingInstructionsActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) pairingInstructionsActivity, new hr2());
            qm3.a(pairingInstructionsActivity, a());
            return pairingInstructionsActivity;
        }

        @DexIgnore
        public b1(um3 um3) {
            a(um3);
        }

        @DexIgnore
        public final void a(um3 um3) {
            o44.a(um3);
            this.a = um3;
        }

        @DexIgnore
        public void a(PairingInstructionsActivity pairingInstructionsActivity) {
            b(pairingInstructionsActivity);
        }

        @DexIgnore
        public final wm3 a(wm3 wm3) {
            ym3.a(wm3);
            return wm3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements be3 {
        @DexIgnore
        public fe3 a;

        @DexIgnore
        public final ActiveTimeDetailPresenter a() {
            ActiveTimeDetailPresenter a2 = he3.a(ge3.a(this.a), (SummariesRepository) y52.this.H.get(), (ActivitiesRepository) y52.this.c0.get(), (UserRepository) y52.this.w.get(), (WorkoutSessionRepository) y52.this.B0.get(), (FitnessDataDao) y52.this.F.get(), (WorkoutDao) y52.this.A0.get(), (FitnessDatabase) y52.this.D.get(), (i42) y52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeDetailActivity b(ActiveTimeDetailActivity activeTimeDetailActivity) {
            eq2.a((BaseActivity) activeTimeDetailActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) activeTimeDetailActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) activeTimeDetailActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) activeTimeDetailActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) activeTimeDetailActivity, new hr2());
            ae3.a(activeTimeDetailActivity, a());
            return activeTimeDetailActivity;
        }

        @DexIgnore
        public c(fe3 fe3) {
            a(fe3);
        }

        @DexIgnore
        public final void a(fe3 fe3) {
            o44.a(fe3);
            this.a = fe3;
        }

        @DexIgnore
        public void a(ActiveTimeDetailActivity activeTimeDetailActivity) {
            b(activeTimeDetailActivity);
        }

        @DexIgnore
        public final ActiveTimeDetailPresenter a(ActiveTimeDetailPresenter activeTimeDetailPresenter) {
            ie3.a(activeTimeDetailPresenter);
            return activeTimeDetailPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c0 implements vk3 {
        @DexIgnore
        public yk3 a;

        @DexIgnore
        public final al3 a() {
            al3 a2 = bl3.a(zk3.a(this.a), b());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ResetPasswordUseCase b() {
            return new ResetPasswordUseCase((AuthApiGuestService) y52.this.k.get());
        }

        @DexIgnore
        public c0(yk3 yk3) {
            a(yk3);
        }

        @DexIgnore
        public final ForgotPasswordActivity b(ForgotPasswordActivity forgotPasswordActivity) {
            eq2.a((BaseActivity) forgotPasswordActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) forgotPasswordActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) forgotPasswordActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) forgotPasswordActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) forgotPasswordActivity, new hr2());
            uk3.a(forgotPasswordActivity, a());
            return forgotPasswordActivity;
        }

        @DexIgnore
        public final void a(yk3 yk3) {
            o44.a(yk3);
            this.a = yk3;
        }

        @DexIgnore
        public void a(ForgotPasswordActivity forgotPasswordActivity) {
            b(forgotPasswordActivity);
        }

        @DexIgnore
        public final al3 a(al3 al3) {
            cl3.a(al3);
            return al3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c1 implements vn3 {
        @DexIgnore
        public zn3 a;

        @DexIgnore
        public final co3 a() {
            co3 a2 = do3.a(bo3.a(this.a), ao3.a(this.a), (fn2) y52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final PermissionActivity b(PermissionActivity permissionActivity) {
            eq2.a((BaseActivity) permissionActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) permissionActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) permissionActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) permissionActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) permissionActivity, new hr2());
            un3.a(permissionActivity, a());
            return permissionActivity;
        }

        @DexIgnore
        public c1(zn3 zn3) {
            a(zn3);
        }

        @DexIgnore
        public final void a(zn3 zn3) {
            o44.a(zn3);
            this.a = zn3;
        }

        @DexIgnore
        public void a(PermissionActivity permissionActivity) {
            b(permissionActivity);
        }

        @DexIgnore
        public final co3 a(co3 co3) {
            eo3.a(co3);
            return co3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements e83 {
        @DexIgnore
        public l83 a;

        @DexIgnore
        public final ActiveTimeOverviewDayPresenter a() {
            ActiveTimeOverviewDayPresenter a2 = i83.a(m83.a(this.a), (SummariesRepository) y52.this.H.get(), (ActivitiesRepository) y52.this.c0.get(), (WorkoutSessionRepository) y52.this.B0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeOverviewMonthPresenter b() {
            ActiveTimeOverviewMonthPresenter a2 = s83.a(n83.a(this.a), (UserRepository) y52.this.w.get(), (SummariesRepository) y52.this.H.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeOverviewWeekPresenter c() {
            ActiveTimeOverviewWeekPresenter a2 = x83.a(o83.a(this.a), (UserRepository) y52.this.w.get(), (SummariesRepository) y52.this.H.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public d(l83 l83) {
            a(l83);
        }

        @DexIgnore
        public final ActiveTimeOverviewFragment b(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            k83.a(activeTimeOverviewFragment, a());
            k83.a(activeTimeOverviewFragment, c());
            k83.a(activeTimeOverviewFragment, b());
            return activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void a(l83 l83) {
            o44.a(l83);
            this.a = l83;
        }

        @DexIgnore
        public void a(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            b(activeTimeOverviewFragment);
        }

        @DexIgnore
        public final ActiveTimeOverviewDayPresenter a(ActiveTimeOverviewDayPresenter activeTimeOverviewDayPresenter) {
            j83.a(activeTimeOverviewDayPresenter);
            return activeTimeOverviewDayPresenter;
        }

        @DexIgnore
        public final ActiveTimeOverviewWeekPresenter a(ActiveTimeOverviewWeekPresenter activeTimeOverviewWeekPresenter) {
            y83.a(activeTimeOverviewWeekPresenter);
            return activeTimeOverviewWeekPresenter;
        }

        @DexIgnore
        public final ActiveTimeOverviewMonthPresenter a(ActiveTimeOverviewMonthPresenter activeTimeOverviewMonthPresenter) {
            t83.a(activeTimeOverviewMonthPresenter);
            return activeTimeOverviewMonthPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d0 implements df3 {
        @DexIgnore
        public hf3 a;

        @DexIgnore
        public final GoalTrackingDetailPresenter a() {
            GoalTrackingDetailPresenter a2 = jf3.a(if3.a(this.a), (GoalTrackingRepository) y52.this.k0.get(), (GoalTrackingDao) y52.this.j0.get(), (GoalTrackingDatabase) y52.this.i0.get(), (i42) y52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingDetailActivity b(GoalTrackingDetailActivity goalTrackingDetailActivity) {
            eq2.a((BaseActivity) goalTrackingDetailActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) goalTrackingDetailActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) goalTrackingDetailActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) goalTrackingDetailActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) goalTrackingDetailActivity, new hr2());
            cf3.a(goalTrackingDetailActivity, a());
            return goalTrackingDetailActivity;
        }

        @DexIgnore
        public d0(hf3 hf3) {
            a(hf3);
        }

        @DexIgnore
        public final void a(hf3 hf3) {
            o44.a(hf3);
            this.a = hf3;
        }

        @DexIgnore
        public void a(GoalTrackingDetailActivity goalTrackingDetailActivity) {
            b(goalTrackingDetailActivity);
        }

        @DexIgnore
        public final GoalTrackingDetailPresenter a(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
            kf3.a(goalTrackingDetailPresenter);
            return goalTrackingDetailPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d1 implements qj3 {
        @DexIgnore
        public tj3 a;

        @DexIgnore
        public final vj3 a() {
            vj3 a2 = wj3.a(uj3.a(this.a), y52.this.V(), y52.this.C());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final PreferredUnitActivity b(PreferredUnitActivity preferredUnitActivity) {
            eq2.a((BaseActivity) preferredUnitActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) preferredUnitActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) preferredUnitActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) preferredUnitActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) preferredUnitActivity, new hr2());
            pj3.a(preferredUnitActivity, a());
            return preferredUnitActivity;
        }

        @DexIgnore
        public d1(tj3 tj3) {
            a(tj3);
        }

        @DexIgnore
        public final void a(tj3 tj3) {
            o44.a(tj3);
            this.a = tj3;
        }

        @DexIgnore
        public void a(PreferredUnitActivity preferredUnitActivity) {
            b(preferredUnitActivity);
        }

        @DexIgnore
        public final vj3 a(vj3 vj3) {
            xj3.a(vj3);
            return vj3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements ke3 {
        @DexIgnore
        public oe3 a;

        @DexIgnore
        public final ActivityDetailPresenter a() {
            ActivityDetailPresenter a2 = qe3.a(pe3.a(this.a), (SummariesRepository) y52.this.H.get(), (ActivitiesRepository) y52.this.c0.get(), (UserRepository) y52.this.w.get(), (WorkoutSessionRepository) y52.this.B0.get(), (FitnessDataDao) y52.this.F.get(), (WorkoutDao) y52.this.A0.get(), (FitnessDatabase) y52.this.D.get(), (i42) y52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityDetailActivity b(ActivityDetailActivity activityDetailActivity) {
            eq2.a((BaseActivity) activityDetailActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) activityDetailActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) activityDetailActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) activityDetailActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) activityDetailActivity, new hr2());
            je3.a(activityDetailActivity, a());
            return activityDetailActivity;
        }

        @DexIgnore
        public e(oe3 oe3) {
            a(oe3);
        }

        @DexIgnore
        public final void a(oe3 oe3) {
            o44.a(oe3);
            this.a = oe3;
        }

        @DexIgnore
        public void a(ActivityDetailActivity activityDetailActivity) {
            b(activityDetailActivity);
        }

        @DexIgnore
        public final ActivityDetailPresenter a(ActivityDetailPresenter activityDetailPresenter) {
            re3.a(activityDetailPresenter);
            return activityDetailPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e0 implements eb3 {
        @DexIgnore
        public lb3 a;

        @DexIgnore
        public final GoalTrackingOverviewDayPresenter a() {
            GoalTrackingOverviewDayPresenter a2 = ib3.a(mb3.a(this.a), (fn2) y52.this.c.get(), (GoalTrackingRepository) y52.this.k0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingOverviewMonthPresenter b() {
            GoalTrackingOverviewMonthPresenter a2 = sb3.a(nb3.a(this.a), (UserRepository) y52.this.w.get(), (fn2) y52.this.c.get(), (GoalTrackingRepository) y52.this.k0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingOverviewWeekPresenter c() {
            GoalTrackingOverviewWeekPresenter a2 = xb3.a(ob3.a(this.a), (UserRepository) y52.this.w.get(), (fn2) y52.this.c.get(), (GoalTrackingRepository) y52.this.k0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public e0(lb3 lb3) {
            a(lb3);
        }

        @DexIgnore
        public final void a(lb3 lb3) {
            o44.a(lb3);
            this.a = lb3;
        }

        @DexIgnore
        public void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            b(goalTrackingOverviewFragment);
        }

        @DexIgnore
        public final GoalTrackingOverviewFragment b(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            kb3.a(goalTrackingOverviewFragment, a());
            kb3.a(goalTrackingOverviewFragment, c());
            kb3.a(goalTrackingOverviewFragment, b());
            return goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final GoalTrackingOverviewDayPresenter a(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
            jb3.a(goalTrackingOverviewDayPresenter);
            return goalTrackingOverviewDayPresenter;
        }

        @DexIgnore
        public final GoalTrackingOverviewWeekPresenter a(GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter) {
            yb3.a(goalTrackingOverviewWeekPresenter);
            return goalTrackingOverviewWeekPresenter;
        }

        @DexIgnore
        public final GoalTrackingOverviewMonthPresenter a(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
            tb3.a(goalTrackingOverviewMonthPresenter);
            return goalTrackingOverviewMonthPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e1 implements gj3 {
        @DexIgnore
        public kj3 a;

        @DexIgnore
        public final kr2 a() {
            return new kr2((AuthApiUserService) y52.this.t.get());
        }

        @DexIgnore
        public final mj3 b() {
            mj3 a2 = nj3.a(lj3.a(this.a), a(), (k62) y52.this.V.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public e1(kj3 kj3) {
            a(kj3);
        }

        @DexIgnore
        public final void a(kj3 kj3) {
            o44.a(kj3);
            this.a = kj3;
        }

        @DexIgnore
        public void a(ProfileChangePasswordActivity profileChangePasswordActivity) {
            b(profileChangePasswordActivity);
        }

        @DexIgnore
        public final mj3 a(mj3 mj3) {
            oj3.a(mj3);
            return mj3;
        }

        @DexIgnore
        public final ProfileChangePasswordActivity b(ProfileChangePasswordActivity profileChangePasswordActivity) {
            eq2.a((BaseActivity) profileChangePasswordActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) profileChangePasswordActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) profileChangePasswordActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) profileChangePasswordActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) profileChangePasswordActivity, new hr2());
            fj3.a(profileChangePasswordActivity, b());
            return profileChangePasswordActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f implements e93 {
        @DexIgnore
        public l93 a;

        @DexIgnore
        public final ActivityOverviewDayPresenter a() {
            ActivityOverviewDayPresenter a2 = i93.a(m93.a(this.a), (SummariesRepository) y52.this.H.get(), (ActivitiesRepository) y52.this.c0.get(), (WorkoutSessionRepository) y52.this.B0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityOverviewMonthPresenter b() {
            ActivityOverviewMonthPresenter a2 = s93.a(n93.a(this.a), (UserRepository) y52.this.w.get(), (SummariesRepository) y52.this.H.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityOverviewWeekPresenter c() {
            ActivityOverviewWeekPresenter a2 = x93.a(o93.a(this.a), (UserRepository) y52.this.w.get(), (SummariesRepository) y52.this.H.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public f(l93 l93) {
            a(l93);
        }

        @DexIgnore
        public final ActivityOverviewFragment b(ActivityOverviewFragment activityOverviewFragment) {
            k93.a(activityOverviewFragment, a());
            k93.a(activityOverviewFragment, c());
            k93.a(activityOverviewFragment, b());
            return activityOverviewFragment;
        }

        @DexIgnore
        public final void a(l93 l93) {
            o44.a(l93);
            this.a = l93;
        }

        @DexIgnore
        public void a(ActivityOverviewFragment activityOverviewFragment) {
            b(activityOverviewFragment);
        }

        @DexIgnore
        public final ActivityOverviewDayPresenter a(ActivityOverviewDayPresenter activityOverviewDayPresenter) {
            j93.a(activityOverviewDayPresenter);
            return activityOverviewDayPresenter;
        }

        @DexIgnore
        public final ActivityOverviewWeekPresenter a(ActivityOverviewWeekPresenter activityOverviewWeekPresenter) {
            y93.a(activityOverviewWeekPresenter);
            return activityOverviewWeekPresenter;
        }

        @DexIgnore
        public final ActivityOverviewMonthPresenter a(ActivityOverviewMonthPresenter activityOverviewMonthPresenter) {
            t93.a(activityOverviewMonthPresenter);
            return activityOverviewMonthPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f0 implements mf3 {
        @DexIgnore
        public qf3 a;

        @DexIgnore
        public final HeartRateDetailPresenter a() {
            HeartRateDetailPresenter a2 = sf3.a(rf3.a(this.a), (HeartRateSummaryRepository) y52.this.z0.get(), (HeartRateSampleRepository) y52.this.x0.get(), (UserRepository) y52.this.w.get(), (WorkoutSessionRepository) y52.this.B0.get(), (FitnessDataDao) y52.this.F.get(), (WorkoutDao) y52.this.A0.get(), (FitnessDatabase) y52.this.D.get(), (i42) y52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateDetailActivity b(HeartRateDetailActivity heartRateDetailActivity) {
            eq2.a((BaseActivity) heartRateDetailActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) heartRateDetailActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) heartRateDetailActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) heartRateDetailActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) heartRateDetailActivity, new hr2());
            lf3.a(heartRateDetailActivity, a());
            return heartRateDetailActivity;
        }

        @DexIgnore
        public f0(qf3 qf3) {
            a(qf3);
        }

        @DexIgnore
        public final void a(qf3 qf3) {
            o44.a(qf3);
            this.a = qf3;
        }

        @DexIgnore
        public void a(HeartRateDetailActivity heartRateDetailActivity) {
            b(heartRateDetailActivity);
        }

        @DexIgnore
        public final HeartRateDetailPresenter a(HeartRateDetailPresenter heartRateDetailPresenter) {
            tf3.a(heartRateDetailPresenter);
            return heartRateDetailPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f1 implements mh3 {
        @DexIgnore
        public void a(nh3 nh3) {
            b(nh3);
        }

        @DexIgnore
        public final nh3 b(nh3 nh3) {
            ph3.a(nh3, (k42) y52.this.O1.get());
            return nh3;
        }

        @DexIgnore
        public f1(qh3 qh3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g implements xt2 {
        @DexIgnore
        public bu2 a;

        @DexIgnore
        public final AlarmPresenter a() {
            AlarmPresenter a2 = fu2.a(eu2.a(this.a), du2.a(this.a), cu2.a(this.a), this.a.a(), c(), (AlarmHelper) y52.this.C.get(), b(), (UserRepository) y52.this.w.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DeleteAlarm b() {
            return new DeleteAlarm((AlarmsRepository) y52.this.B.get());
        }

        @DexIgnore
        public final SetAlarms c() {
            return new SetAlarms((PortfolioApp) y52.this.d.get(), (AlarmsRepository) y52.this.B.get());
        }

        @DexIgnore
        public g(bu2 bu2) {
            a(bu2);
        }

        @DexIgnore
        public final AlarmActivity b(AlarmActivity alarmActivity) {
            eq2.a((BaseActivity) alarmActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) alarmActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) alarmActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) alarmActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) alarmActivity, new hr2());
            wt2.a(alarmActivity, a());
            return alarmActivity;
        }

        @DexIgnore
        public final void a(bu2 bu2) {
            o44.a(bu2);
            this.a = bu2;
        }

        @DexIgnore
        public void a(AlarmActivity alarmActivity) {
            b(alarmActivity);
        }

        @DexIgnore
        public final AlarmPresenter a(AlarmPresenter alarmPresenter) {
            gu2.a(alarmPresenter);
            return alarmPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g0 implements ec3 {
        @DexIgnore
        public lc3 a;

        @DexIgnore
        public final HeartRateOverviewDayPresenter a() {
            HeartRateOverviewDayPresenter a2 = ic3.a(mc3.a(this.a), (HeartRateSampleRepository) y52.this.x0.get(), (WorkoutSessionRepository) y52.this.B0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateOverviewMonthPresenter b() {
            HeartRateOverviewMonthPresenter a2 = sc3.a(nc3.a(this.a), (UserRepository) y52.this.w.get(), (HeartRateSummaryRepository) y52.this.z0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateOverviewWeekPresenter c() {
            HeartRateOverviewWeekPresenter a2 = xc3.a(oc3.a(this.a), (UserRepository) y52.this.w.get(), (HeartRateSummaryRepository) y52.this.z0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public g0(lc3 lc3) {
            a(lc3);
        }

        @DexIgnore
        public final void a(lc3 lc3) {
            o44.a(lc3);
            this.a = lc3;
        }

        @DexIgnore
        public final HeartRateOverviewFragment b(HeartRateOverviewFragment heartRateOverviewFragment) {
            kc3.a(heartRateOverviewFragment, a());
            kc3.a(heartRateOverviewFragment, c());
            kc3.a(heartRateOverviewFragment, b());
            return heartRateOverviewFragment;
        }

        @DexIgnore
        public void a(HeartRateOverviewFragment heartRateOverviewFragment) {
            b(heartRateOverviewFragment);
        }

        @DexIgnore
        public final HeartRateOverviewDayPresenter a(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
            jc3.a(heartRateOverviewDayPresenter);
            return heartRateOverviewDayPresenter;
        }

        @DexIgnore
        public final HeartRateOverviewWeekPresenter a(HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter) {
            yc3.a(heartRateOverviewWeekPresenter);
            return heartRateOverviewWeekPresenter;
        }

        @DexIgnore
        public final HeartRateOverviewMonthPresenter a(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
            tc3.a(heartRateOverviewMonthPresenter);
            return heartRateOverviewMonthPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g1 implements uh3 {
        @DexIgnore
        public zh3 a;

        @DexIgnore
        public final ProfileGoalEditPresenter a() {
            ProfileGoalEditPresenter a2 = ci3.a(ai3.a(this.a), (SummariesRepository) y52.this.H.get(), (SleepSummariesRepository) y52.this.K.get(), (GoalTrackingRepository) y52.this.k0.get(), (UserRepository) y52.this.w.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ProfileGoalEditActivity b(ProfileGoalEditActivity profileGoalEditActivity) {
            eq2.a((BaseActivity) profileGoalEditActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) profileGoalEditActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) profileGoalEditActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) profileGoalEditActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) profileGoalEditActivity, new hr2());
            th3.a(profileGoalEditActivity, a());
            return profileGoalEditActivity;
        }

        @DexIgnore
        public g1(zh3 zh3) {
            a(zh3);
        }

        @DexIgnore
        public final void a(zh3 zh3) {
            o44.a(zh3);
            this.a = zh3;
        }

        @DexIgnore
        public void a(ProfileGoalEditActivity profileGoalEditActivity) {
            b(profileGoalEditActivity);
        }

        @DexIgnore
        public final ProfileGoalEditPresenter a(ProfileGoalEditPresenter profileGoalEditPresenter) {
            di3.a(profileGoalEditPresenter);
            return profileGoalEditPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h implements bm3 {
        @DexIgnore
        public em3 a;

        @DexIgnore
        public final gm3 a() {
            gm3 a2 = hm3.a(fm3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final is2 b(is2 is2) {
            ks2.a(is2, a());
            return is2;
        }

        @DexIgnore
        public h(y52 y52, em3 em3) {
            a(em3);
        }

        @DexIgnore
        public final void a(em3 em3) {
            o44.a(em3);
            this.a = em3;
        }

        @DexIgnore
        public void a(is2 is2) {
            b(is2);
        }

        @DexIgnore
        public final gm3 a(gm3 gm3) {
            im3.a(gm3);
            return gm3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h0 implements gi3 {
        @DexIgnore
        public ki3 a;

        @DexIgnore
        public final lr2 a() {
            return new lr2((UserRepository) y52.this.w.get(), (DeviceRepository) y52.this.T.get(), (fn2) y52.this.c.get());
        }

        @DexIgnore
        public final HelpPresenter b() {
            HelpPresenter a2 = mi3.a(li3.a(this.a), (DeviceRepository) y52.this.T.get(), a(), (AnalyticsHelper) y52.this.E0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public h0(ki3 ki3) {
            a(ki3);
        }

        @DexIgnore
        public final void a(ki3 ki3) {
            o44.a(ki3);
            this.a = ki3;
        }

        @DexIgnore
        public void a(HelpActivity helpActivity) {
            b(helpActivity);
        }

        @DexIgnore
        public final HelpPresenter a(HelpPresenter helpPresenter) {
            ni3.a(helpPresenter);
            return helpPresenter;
        }

        @DexIgnore
        public final HelpActivity b(HelpActivity helpActivity) {
            eq2.a((BaseActivity) helpActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) helpActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) helpActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) helpActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) helpActivity, new hr2());
            fi3.a(helpActivity, b());
            return helpActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h1 implements xi3 {
        @DexIgnore
        public bj3 a;

        @DexIgnore
        public final ProfileOptInPresenter a() {
            ProfileOptInPresenter a2 = dj3.a(cj3.a(this.a), y52.this.V(), (UserRepository) y52.this.w.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ProfileOptInActivity b(ProfileOptInActivity profileOptInActivity) {
            eq2.a((BaseActivity) profileOptInActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) profileOptInActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) profileOptInActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) profileOptInActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) profileOptInActivity, new hr2());
            wi3.a(profileOptInActivity, a());
            return profileOptInActivity;
        }

        @DexIgnore
        public h1(bj3 bj3) {
            a(bj3);
        }

        @DexIgnore
        public final void a(bj3 bj3) {
            o44.a(bj3);
            this.a = bj3;
        }

        @DexIgnore
        public void a(ProfileOptInActivity profileOptInActivity) {
            b(profileOptInActivity);
        }

        @DexIgnore
        public final ProfileOptInPresenter a(ProfileOptInPresenter profileOptInPresenter) {
            ej3.a(profileOptInPresenter);
            return profileOptInPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i {
        @DexIgnore
        public o42 a;
        @DexIgnore
        public PortfolioDatabaseModule b;
        @DexIgnore
        public RepositoriesModule c;
        @DexIgnore
        public NotificationsRepositoryModule d;
        @DexIgnore
        public MicroAppSettingRepositoryModule e;
        @DexIgnore
        public UAppSystemVersionRepositoryModule f;

        @DexIgnore
        public i() {
        }

        @DexIgnore
        public m42 a() {
            if (this.a != null) {
                if (this.b == null) {
                    this.b = new PortfolioDatabaseModule();
                }
                if (this.c == null) {
                    this.c = new RepositoriesModule();
                }
                if (this.d == null) {
                    this.d = new NotificationsRepositoryModule();
                }
                if (this.e == null) {
                    this.e = new MicroAppSettingRepositoryModule();
                }
                if (this.f == null) {
                    this.f = new UAppSystemVersionRepositoryModule();
                }
                return new y52(this);
            }
            throw new IllegalStateException(o42.class.getCanonicalName() + " must be set");
        }

        @DexIgnore
        public i a(o42 o42) {
            o44.a(o42);
            this.a = o42;
            return this;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i0 implements rv2 {
        @DexIgnore
        public wv2 a;

        @DexIgnore
        public final DoNotDisturbScheduledTimePresenter a() {
            DoNotDisturbScheduledTimePresenter a2 = uy2.a(xv2.a(this.a), (DNDSettingsDatabase) y52.this.e.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final uv2 b(uv2 uv2) {
            vv2.a(uv2, a());
            return uv2;
        }

        @DexIgnore
        public i0(wv2 wv2) {
            a(wv2);
        }

        @DexIgnore
        public final void a(wv2 wv2) {
            o44.a(wv2);
            this.a = wv2;
        }

        @DexIgnore
        public void a(uv2 uv2) {
            b(uv2);
        }

        @DexIgnore
        public final DoNotDisturbScheduledTimePresenter a(DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter) {
            vy2.a(doNotDisturbScheduledTimePresenter);
            return doNotDisturbScheduledTimePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i1 implements ul3 {
        @DexIgnore
        public xl3 a;

        @DexIgnore
        public final GetRecommendedGoalUseCase a() {
            return new GetRecommendedGoalUseCase((SummariesRepository) y52.this.H.get(), (SleepSummariesRepository) y52.this.K.get(), (SleepSessionsRepository) y52.this.h0.get());
        }

        @DexIgnore
        public final ProfileSetupPresenter b() {
            ProfileSetupPresenter a2 = zl3.a(yl3.a(this.a), a(), (UserRepository) y52.this.w.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public i1(xl3 xl3) {
            a(xl3);
        }

        @DexIgnore
        public final void a(xl3 xl3) {
            o44.a(xl3);
            this.a = xl3;
        }

        @DexIgnore
        public void a(ProfileSetupActivity profileSetupActivity) {
            b(profileSetupActivity);
        }

        @DexIgnore
        public final ProfileSetupPresenter a(ProfileSetupPresenter profileSetupPresenter) {
            am3.a(profileSetupPresenter, y52.this.Q());
            am3.a(profileSetupPresenter, y52.this.R());
            am3.a(profileSetupPresenter, y52.this.C());
            am3.a(profileSetupPresenter, (AnalyticsHelper) y52.this.E0.get());
            am3.a(profileSetupPresenter);
            return profileSetupPresenter;
        }

        @DexIgnore
        public final ProfileSetupActivity b(ProfileSetupActivity profileSetupActivity) {
            eq2.a((BaseActivity) profileSetupActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) profileSetupActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) profileSetupActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) profileSetupActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) profileSetupActivity, new hr2());
            tl3.a(profileSetupActivity, b());
            return profileSetupActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j implements aq3 {
        @DexIgnore
        public dq3 a;

        @DexIgnore
        public final fq3 a() {
            fq3 a2 = hq3.a((PortfolioApp) y52.this.d.get(), eq3.a(this.a), (sc) y52.this.w1.get(), (rl2) y52.this.y1.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CalibrationActivity b(CalibrationActivity calibrationActivity) {
            eq2.a((BaseActivity) calibrationActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) calibrationActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) calibrationActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) calibrationActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) calibrationActivity, new hr2());
            zp3.a(calibrationActivity, a());
            return calibrationActivity;
        }

        @DexIgnore
        public j(dq3 dq3) {
            a(dq3);
        }

        @DexIgnore
        public final void a(dq3 dq3) {
            o44.a(dq3);
            this.a = dq3;
        }

        @DexIgnore
        public void a(CalibrationActivity calibrationActivity) {
            b(calibrationActivity);
        }

        @DexIgnore
        public final fq3 a(fq3 fq3) {
            iq3.a(fq3);
            return fq3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j0 implements qu2 {
        @DexIgnore
        public bv2 a;

        @DexIgnore
        public final rr2 a() {
            return new rr2(d());
        }

        @DexIgnore
        public final lr2 b() {
            return new lr2((UserRepository) y52.this.w.get(), (DeviceRepository) y52.this.T.get(), (fn2) y52.this.c.get());
        }

        @DexIgnore
        public final HomePresenter c() {
            HomePresenter a2 = dv2.a(cv2.a(this.a), (fn2) y52.this.c.get(), (DeviceRepository) y52.this.T.get(), (PortfolioApp) y52.this.d.get(), y52.this.U(), (k62) y52.this.V.get(), y52.this.l(), a(), (UserUtils) y52.this.U1.get(), d(), b(), (bk3) y52.this.v1.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ServerSettingRepository d() {
            return new ServerSettingRepository((ServerSettingDataSource) y52.this.S1.get(), (ServerSettingDataSource) y52.this.T1.get());
        }

        @DexIgnore
        public j0(bv2 bv2) {
            a(bv2);
        }

        @DexIgnore
        public final void a(bv2 bv2) {
            o44.a(bv2);
            this.a = bv2;
        }

        @DexIgnore
        public void a(HomeActivity homeActivity) {
            b(homeActivity);
        }

        @DexIgnore
        public final HomePresenter a(HomePresenter homePresenter) {
            ev2.a(homePresenter);
            return homePresenter;
        }

        @DexIgnore
        public final HomeActivity b(HomeActivity homeActivity) {
            eq2.a((BaseActivity) homeActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) homeActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) homeActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) homeActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) homeActivity, new hr2());
            pu2.a(homeActivity, c());
            return homeActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j1 implements oy2 {
        @DexIgnore
        public py2 a;

        @DexIgnore
        public final InactivityNudgeTimePresenter a() {
            InactivityNudgeTimePresenter a2 = xx2.a(qy2.a(this.a), (RemindersSettingsDatabase) y52.this.C0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final RemindTimePresenter b() {
            RemindTimePresenter a2 = my2.a(ry2.a(this.a), (RemindersSettingsDatabase) y52.this.C0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public j1(py2 py2) {
            a(py2);
        }

        @DexIgnore
        public final void a(py2 py2) {
            o44.a(py2);
            this.a = py2;
        }

        @DexIgnore
        public final dy2 b(dy2 dy2) {
            ey2.a(dy2, a());
            ey2.a(dy2, b());
            return dy2;
        }

        @DexIgnore
        public void a(dy2 dy2) {
            b(dy2);
        }

        @DexIgnore
        public final InactivityNudgeTimePresenter a(InactivityNudgeTimePresenter inactivityNudgeTimePresenter) {
            yx2.a(inactivityNudgeTimePresenter);
            return inactivityNudgeTimePresenter;
        }

        @DexIgnore
        public final RemindTimePresenter a(RemindTimePresenter remindTimePresenter) {
            ny2.a(remindTimePresenter);
            return remindTimePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k implements ue3 {
        @DexIgnore
        public ye3 a;

        @DexIgnore
        public final CaloriesDetailPresenter a() {
            CaloriesDetailPresenter a2 = af3.a(ze3.a(this.a), (SummariesRepository) y52.this.H.get(), (ActivitiesRepository) y52.this.c0.get(), (UserRepository) y52.this.w.get(), (WorkoutSessionRepository) y52.this.B0.get(), (FitnessDataDao) y52.this.F.get(), (WorkoutDao) y52.this.A0.get(), (FitnessDatabase) y52.this.D.get(), (i42) y52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesDetailActivity b(CaloriesDetailActivity caloriesDetailActivity) {
            eq2.a((BaseActivity) caloriesDetailActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) caloriesDetailActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) caloriesDetailActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) caloriesDetailActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) caloriesDetailActivity, new hr2());
            te3.a(caloriesDetailActivity, a());
            return caloriesDetailActivity;
        }

        @DexIgnore
        public k(ye3 ye3) {
            a(ye3);
        }

        @DexIgnore
        public final void a(ye3 ye3) {
            o44.a(ye3);
            this.a = ye3;
        }

        @DexIgnore
        public void a(CaloriesDetailActivity caloriesDetailActivity) {
            b(caloriesDetailActivity);
        }

        @DexIgnore
        public final CaloriesDetailPresenter a(CaloriesDetailPresenter caloriesDetailPresenter) {
            bf3.a(caloriesDetailPresenter);
            return caloriesDetailPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k0 implements gv2 {
        @DexIgnore
        public hv2 a;

        @DexIgnore
        public final HomeAlertsHybridPresenter a() {
            HomeAlertsHybridPresenter a2 = bz2.a(iv2.a(this.a), (AlarmHelper) y52.this.C.get(), i(), (AlarmsRepository) y52.this.B.get(), (fn2) y52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeAlertsPresenter b() {
            HomeAlertsPresenter a2 = yv2.a(jv2.a(this.a), (k62) y52.this.V.get(), (AlarmHelper) y52.this.C.get(), y52.this.y(), new qx2(), h(), (NotificationSettingsDatabase) y52.this.f.get(), i(), (AlarmsRepository) y52.this.B.get(), (fn2) y52.this.c.get(), (DNDSettingsDatabase) y52.this.e.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeDashboardPresenter c() {
            HomeDashboardPresenter a2 = x73.a(kv2.a(this.a), (PortfolioApp) y52.this.d.get(), (DeviceRepository) y52.this.T.get(), y52.this.l(), (SummariesRepository) y52.this.H.get(), (GoalTrackingRepository) y52.this.k0.get(), (SleepSummariesRepository) y52.this.K.get(), (HeartRateSampleRepository) y52.this.x0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeDianaCustomizePresenter d() {
            HomeDianaCustomizePresenter a2 = f23.a(lv2.a(this.a), y52.this.Y(), y52.this.f(), (DianaPresetRepository) y52.this.p.get(), j(), new xm2(), (CustomizeRealDataRepository) y52.this.s.get(), (UserRepository) y52.this.w.get(), y52.this.a0(), (PortfolioApp) y52.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeHybridCustomizePresenter e() {
            HomeHybridCustomizePresenter a2 = b63.a((PortfolioApp) y52.this.d.get(), mv2.a(this.a), (MicroAppRepository) y52.this.s0.get(), (HybridPresetRepository) y52.this.Z.get(), k(), (fn2) y52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeProfilePresenter f() {
            HomeProfilePresenter a2 = qg3.a(nv2.a(this.a), (PortfolioApp) y52.this.d.get(), y52.this.C(), y52.this.V(), (DeviceRepository) y52.this.T.get(), (UserRepository) y52.this.w.get(), (SummariesRepository) y52.this.H.get(), (SleepSummariesRepository) y52.this.K.get(), y52.this.k());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final jg3 g() {
            jg3 a2 = kg3.a(ov2.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final jw2 h() {
            return new jw2((NotificationsRepository) y52.this.O.get());
        }

        @DexIgnore
        public final SetAlarms i() {
            return new SetAlarms((PortfolioApp) y52.this.d.get(), (AlarmsRepository) y52.this.B.get());
        }

        @DexIgnore
        public final SetDianaPresetToWatchUseCase j() {
            return new SetDianaPresetToWatchUseCase((DianaPresetRepository) y52.this.p.get(), y52.this.e(), y52.this.X(), y52.this.a0());
        }

        @DexIgnore
        public final SetHybridPresetToWatchUseCase k() {
            return new SetHybridPresetToWatchUseCase((DeviceRepository) y52.this.T.get(), (HybridPresetRepository) y52.this.Z.get(), (MicroAppRepository) y52.this.s0.get(), y52.this.N());
        }

        @DexIgnore
        public k0(hv2 hv2) {
            a(hv2);
        }

        @DexIgnore
        public final void a(hv2 hv2) {
            o44.a(hv2);
            this.a = hv2;
        }

        @DexIgnore
        public void a(xu2 xu2) {
            b(xu2);
        }

        @DexIgnore
        public final HomeDashboardPresenter a(HomeDashboardPresenter homeDashboardPresenter) {
            y73.a(homeDashboardPresenter);
            return homeDashboardPresenter;
        }

        @DexIgnore
        public final HomeDianaCustomizePresenter a(HomeDianaCustomizePresenter homeDianaCustomizePresenter) {
            g23.a(homeDianaCustomizePresenter);
            return homeDianaCustomizePresenter;
        }

        @DexIgnore
        public final HomeHybridCustomizePresenter a(HomeHybridCustomizePresenter homeHybridCustomizePresenter) {
            c63.a(homeHybridCustomizePresenter);
            return homeHybridCustomizePresenter;
        }

        @DexIgnore
        public final HomeProfilePresenter a(HomeProfilePresenter homeProfilePresenter) {
            rg3.a(homeProfilePresenter);
            return homeProfilePresenter;
        }

        @DexIgnore
        public final xu2 b(xu2 xu2) {
            zu2.a(xu2, c());
            zu2.a(xu2, d());
            zu2.a(xu2, e());
            zu2.a(xu2, f());
            zu2.a(xu2, b());
            zu2.a(xu2, a());
            zu2.a(xu2, g());
            return xu2;
        }

        @DexIgnore
        public final HomeAlertsPresenter a(HomeAlertsPresenter homeAlertsPresenter) {
            zv2.a(homeAlertsPresenter);
            return homeAlertsPresenter;
        }

        @DexIgnore
        public final HomeAlertsHybridPresenter a(HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
            cz2.a(homeAlertsHybridPresenter);
            return homeAlertsHybridPresenter;
        }

        @DexIgnore
        public final jg3 a(jg3 jg3) {
            lg3.a(jg3);
            return jg3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k1 implements dh3 {
        @DexIgnore
        public hh3 a;

        @DexIgnore
        public final jh3 a() {
            jh3 a2 = kh3.a(ih3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ReplaceBatteryActivity b(ReplaceBatteryActivity replaceBatteryActivity) {
            eq2.a((BaseActivity) replaceBatteryActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) replaceBatteryActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) replaceBatteryActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) replaceBatteryActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) replaceBatteryActivity, new hr2());
            ch3.a(replaceBatteryActivity, a());
            return replaceBatteryActivity;
        }

        @DexIgnore
        public k1(hh3 hh3) {
            a(hh3);
        }

        @DexIgnore
        public final void a(hh3 hh3) {
            o44.a(hh3);
            this.a = hh3;
        }

        @DexIgnore
        public void a(ReplaceBatteryActivity replaceBatteryActivity) {
            b(replaceBatteryActivity);
        }

        @DexIgnore
        public final jh3 a(jh3 jh3) {
            lh3.a(jh3);
            return jh3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l implements ea3 {
        @DexIgnore
        public la3 a;

        @DexIgnore
        public final CaloriesOverviewDayPresenter a() {
            CaloriesOverviewDayPresenter a2 = ia3.a(ma3.a(this.a), (SummariesRepository) y52.this.H.get(), (ActivitiesRepository) y52.this.c0.get(), (WorkoutSessionRepository) y52.this.B0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesOverviewMonthPresenter b() {
            CaloriesOverviewMonthPresenter a2 = sa3.a(na3.a(this.a), (UserRepository) y52.this.w.get(), (SummariesRepository) y52.this.H.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesOverviewWeekPresenter c() {
            CaloriesOverviewWeekPresenter a2 = xa3.a(oa3.a(this.a), (UserRepository) y52.this.w.get(), (SummariesRepository) y52.this.H.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public l(la3 la3) {
            a(la3);
        }

        @DexIgnore
        public final CaloriesOverviewFragment b(CaloriesOverviewFragment caloriesOverviewFragment) {
            ka3.a(caloriesOverviewFragment, a());
            ka3.a(caloriesOverviewFragment, c());
            ka3.a(caloriesOverviewFragment, b());
            return caloriesOverviewFragment;
        }

        @DexIgnore
        public final void a(la3 la3) {
            o44.a(la3);
            this.a = la3;
        }

        @DexIgnore
        public void a(CaloriesOverviewFragment caloriesOverviewFragment) {
            b(caloriesOverviewFragment);
        }

        @DexIgnore
        public final CaloriesOverviewDayPresenter a(CaloriesOverviewDayPresenter caloriesOverviewDayPresenter) {
            ja3.a(caloriesOverviewDayPresenter);
            return caloriesOverviewDayPresenter;
        }

        @DexIgnore
        public final CaloriesOverviewWeekPresenter a(CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter) {
            ya3.a(caloriesOverviewWeekPresenter);
            return caloriesOverviewWeekPresenter;
        }

        @DexIgnore
        public final CaloriesOverviewMonthPresenter a(CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter) {
            ta3.a(caloriesOverviewMonthPresenter);
            return caloriesOverviewMonthPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l0 implements k63 {
        @DexIgnore
        public p63 a;

        @DexIgnore
        public final HybridCustomizeEditPresenter a() {
            HybridCustomizeEditPresenter a2 = r63.a(q63.a(this.a), b(), (fn2) y52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SetHybridPresetToWatchUseCase b() {
            return new SetHybridPresetToWatchUseCase((DeviceRepository) y52.this.T.get(), (HybridPresetRepository) y52.this.Z.get(), (MicroAppRepository) y52.this.s0.get(), y52.this.N());
        }

        @DexIgnore
        public l0(p63 p63) {
            a(p63);
        }

        @DexIgnore
        public final HybridCustomizeEditActivity b(HybridCustomizeEditActivity hybridCustomizeEditActivity) {
            eq2.a((BaseActivity) hybridCustomizeEditActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) hybridCustomizeEditActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) hybridCustomizeEditActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) hybridCustomizeEditActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) hybridCustomizeEditActivity, new hr2());
            j63.a(hybridCustomizeEditActivity, a());
            j63.a(hybridCustomizeEditActivity, (k42) y52.this.O1.get());
            return hybridCustomizeEditActivity;
        }

        @DexIgnore
        public final void a(p63 p63) {
            o44.a(p63);
            this.a = p63;
        }

        @DexIgnore
        public void a(HybridCustomizeEditActivity hybridCustomizeEditActivity) {
            b(hybridCustomizeEditActivity);
        }

        @DexIgnore
        public final HybridCustomizeEditPresenter a(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
            s63.a(hybridCustomizeEditPresenter);
            return hybridCustomizeEditPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l1 implements a73 {
        @DexIgnore
        public e73 a;

        @DexIgnore
        public final SearchMicroAppPresenter a() {
            SearchMicroAppPresenter a2 = g73.a(f73.a(this.a), (MicroAppRepository) y52.this.s0.get(), (fn2) y52.this.c.get(), (PortfolioApp) y52.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SearchMicroAppActivity b(SearchMicroAppActivity searchMicroAppActivity) {
            eq2.a((BaseActivity) searchMicroAppActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) searchMicroAppActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) searchMicroAppActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) searchMicroAppActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) searchMicroAppActivity, new hr2());
            z63.a(searchMicroAppActivity, a());
            return searchMicroAppActivity;
        }

        @DexIgnore
        public l1(e73 e73) {
            a(e73);
        }

        @DexIgnore
        public final void a(e73 e73) {
            o44.a(e73);
            this.a = e73;
        }

        @DexIgnore
        public void a(SearchMicroAppActivity searchMicroAppActivity) {
            b(searchMicroAppActivity);
        }

        @DexIgnore
        public final SearchMicroAppPresenter a(SearchMicroAppPresenter searchMicroAppPresenter) {
            h73.a(searchMicroAppPresenter);
            return searchMicroAppPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m implements t23 {
        @DexIgnore
        public e33 a;

        @DexIgnore
        public final CommuteTimeSettingsPresenter a() {
            CommuteTimeSettingsPresenter a2 = g33.a(f33.a(this.a), (fn2) y52.this.c.get(), (UserRepository) y52.this.w.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CommuteTimeSettingsActivity b(CommuteTimeSettingsActivity commuteTimeSettingsActivity) {
            eq2.a((BaseActivity) commuteTimeSettingsActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) commuteTimeSettingsActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) commuteTimeSettingsActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) commuteTimeSettingsActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) commuteTimeSettingsActivity, new hr2());
            s23.a(commuteTimeSettingsActivity, a());
            return commuteTimeSettingsActivity;
        }

        @DexIgnore
        public m(e33 e33) {
            a(e33);
        }

        @DexIgnore
        public final void a(e33 e33) {
            o44.a(e33);
            this.a = e33;
        }

        @DexIgnore
        public void a(CommuteTimeSettingsActivity commuteTimeSettingsActivity) {
            b(commuteTimeSettingsActivity);
        }

        @DexIgnore
        public final CommuteTimeSettingsPresenter a(CommuteTimeSettingsPresenter commuteTimeSettingsPresenter) {
            h33.a(commuteTimeSettingsPresenter);
            return commuteTimeSettingsPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m0 implements d63 {
        @DexIgnore
        public e63 a;

        @DexIgnore
        public final MicroAppPresenter a() {
            MicroAppPresenter a2 = x63.a(f63.a(this.a), y52.this.b());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final n63 b(n63 n63) {
            o63.a(n63, a());
            o63.a(n63, (k42) y52.this.O1.get());
            return n63;
        }

        @DexIgnore
        public m0(e63 e63) {
            a(e63);
        }

        @DexIgnore
        public final void a(e63 e63) {
            o44.a(e63);
            this.a = e63;
        }

        @DexIgnore
        public void a(n63 n63) {
            b(n63);
        }

        @DexIgnore
        public final MicroAppPresenter a(MicroAppPresenter microAppPresenter) {
            y63.a(microAppPresenter);
            return microAppPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m1 implements j33 {
        @DexIgnore
        public n33 a;

        @DexIgnore
        public final SearchRingPhonePresenter a() {
            SearchRingPhonePresenter a2 = p33.a(o33.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SearchRingPhoneActivity b(SearchRingPhoneActivity searchRingPhoneActivity) {
            eq2.a((BaseActivity) searchRingPhoneActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) searchRingPhoneActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) searchRingPhoneActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) searchRingPhoneActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) searchRingPhoneActivity, new hr2());
            i33.a(searchRingPhoneActivity, a());
            return searchRingPhoneActivity;
        }

        @DexIgnore
        public m1(n33 n33) {
            a(n33);
        }

        @DexIgnore
        public final void a(n33 n33) {
            o44.a(n33);
            this.a = n33;
        }

        @DexIgnore
        public void a(SearchRingPhoneActivity searchRingPhoneActivity) {
            b(searchRingPhoneActivity);
        }

        @DexIgnore
        public final SearchRingPhonePresenter a(SearchRingPhonePresenter searchRingPhonePresenter) {
            q33.a(searchRingPhonePresenter);
            return searchRingPhonePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n implements x23 {
        @DexIgnore
        public a33 a;

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressPresenter a() {
            CommuteTimeSettingsDefaultAddressPresenter a2 = c33.a(b33.a(this.a), (fn2) y52.this.c.get(), (UserRepository) y52.this.w.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressActivity b(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity) {
            eq2.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, new hr2());
            w23.a(commuteTimeSettingsDefaultAddressActivity, a());
            return commuteTimeSettingsDefaultAddressActivity;
        }

        @DexIgnore
        public n(a33 a33) {
            a(a33);
        }

        @DexIgnore
        public final void a(a33 a33) {
            o44.a(a33);
            this.a = a33;
        }

        @DexIgnore
        public void a(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity) {
            b(commuteTimeSettingsDefaultAddressActivity);
        }

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressPresenter a(CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter) {
            d33.a(commuteTimeSettingsDefaultAddressPresenter);
            return commuteTimeSettingsDefaultAddressPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n0 implements dk3 {
        @DexIgnore
        public hk3 a;

        @DexIgnore
        public final LoginPresenter a() {
            LoginPresenter a2 = kk3.a(jk3.a(this.a), ik3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final LoginActivity b(LoginActivity loginActivity) {
            eq2.a((BaseActivity) loginActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) loginActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) loginActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) loginActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) loginActivity, new hr2());
            ck3.a(loginActivity, (jn2) y52.this.k1.get());
            ck3.a(loginActivity, (kn2) y52.this.l0.get());
            ck3.a(loginActivity, (ln2) y52.this.l1.get());
            ck3.a(loginActivity, (MFLoginWechatManager) y52.this.m1.get());
            ck3.a(loginActivity, a());
            return loginActivity;
        }

        @DexIgnore
        public n0(hk3 hk3) {
            a(hk3);
        }

        @DexIgnore
        public final void a(hk3 hk3) {
            o44.a(hk3);
            this.a = hk3;
        }

        @DexIgnore
        public void a(LoginActivity loginActivity) {
            b(loginActivity);
        }

        @DexIgnore
        public final LoginPresenter a(LoginPresenter loginPresenter) {
            lk3.a(loginPresenter, y52.this.G());
            lk3.a(loginPresenter, y52.this.J());
            lk3.a(loginPresenter, y52.this.n());
            lk3.a(loginPresenter, new uq2());
            lk3.a(loginPresenter, y52.this.l());
            lk3.a(loginPresenter, (UserRepository) y52.this.w.get());
            lk3.a(loginPresenter, (DeviceRepository) y52.this.T.get());
            lk3.a(loginPresenter, (fn2) y52.this.c.get());
            lk3.a(loginPresenter, y52.this.p());
            lk3.a(loginPresenter, y52.this.w());
            lk3.a(loginPresenter, (k62) y52.this.V.get());
            lk3.a(loginPresenter, y52.this.u());
            lk3.a(loginPresenter, y52.this.v());
            lk3.a(loginPresenter, y52.this.t());
            lk3.a(loginPresenter, y52.this.r());
            lk3.a(loginPresenter, y52.this.H());
            lk3.a(loginPresenter, (ln2) y52.this.l1.get());
            lk3.a(loginPresenter, y52.this.I());
            lk3.a(loginPresenter, y52.this.L());
            lk3.a(loginPresenter, y52.this.K());
            lk3.a(loginPresenter, y52.this.d());
            lk3.a(loginPresenter, (AnalyticsHelper) y52.this.E0.get());
            lk3.a(loginPresenter, (SummariesRepository) y52.this.H.get());
            lk3.a(loginPresenter, (SleepSummariesRepository) y52.this.K.get());
            lk3.a(loginPresenter, (GoalTrackingRepository) y52.this.k0.get());
            lk3.a(loginPresenter, y52.this.q());
            lk3.a(loginPresenter, y52.this.s());
            lk3.a(loginPresenter, y52.this.B());
            lk3.a(loginPresenter, y52.this.b0());
            lk3.a(loginPresenter, (AlarmsRepository) y52.this.B.get());
            lk3.a(loginPresenter);
            return loginPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n1 implements s33 {
        @DexIgnore
        public v33 a;

        @DexIgnore
        public final SearchSecondTimezonePresenter a() {
            SearchSecondTimezonePresenter a2 = x33.a(w33.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SearchSecondTimezoneActivity b(SearchSecondTimezoneActivity searchSecondTimezoneActivity) {
            eq2.a((BaseActivity) searchSecondTimezoneActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) searchSecondTimezoneActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) searchSecondTimezoneActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) searchSecondTimezoneActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) searchSecondTimezoneActivity, new hr2());
            r33.a(searchSecondTimezoneActivity, a());
            return searchSecondTimezoneActivity;
        }

        @DexIgnore
        public n1(v33 v33) {
            a(v33);
        }

        @DexIgnore
        public final void a(v33 v33) {
            o44.a(v33);
            this.a = v33;
        }

        @DexIgnore
        public void a(SearchSecondTimezoneActivity searchSecondTimezoneActivity) {
            b(searchSecondTimezoneActivity);
        }

        @DexIgnore
        public final SearchSecondTimezonePresenter a(SearchSecondTimezonePresenter searchSecondTimezonePresenter) {
            y33.a(searchSecondTimezonePresenter);
            return searchSecondTimezonePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o implements c53 {
        @DexIgnore
        public void a(x43 x43) {
            b(x43);
        }

        @DexIgnore
        public final x43 b(x43 x43) {
            y43.a(x43, (k42) y52.this.O1.get());
            return x43;
        }

        @DexIgnore
        public o(d53 d53) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o0 implements t63 {
        @DexIgnore
        public void a(h63 h63) {
            b(h63);
        }

        @DexIgnore
        public final h63 b(h63 h63) {
            i63.a(h63, (k42) y52.this.O1.get());
            return h63;
        }

        @DexIgnore
        public o0(w63 w63) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o1 implements go3 {
        @DexIgnore
        public ko3 a;

        @DexIgnore
        public final SignUpPresenter a() {
            SignUpPresenter a2 = no3.a(mo3.a(this.a), lo3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SignUpActivity b(SignUpActivity signUpActivity) {
            eq2.a((BaseActivity) signUpActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) signUpActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) signUpActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) signUpActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) signUpActivity, new hr2());
            fo3.a(signUpActivity, a());
            fo3.a(signUpActivity, (jn2) y52.this.k1.get());
            fo3.a(signUpActivity, (kn2) y52.this.l0.get());
            fo3.a(signUpActivity, (ln2) y52.this.l1.get());
            fo3.a(signUpActivity, (MFLoginWechatManager) y52.this.m1.get());
            return signUpActivity;
        }

        @DexIgnore
        public o1(ko3 ko3) {
            a(ko3);
        }

        @DexIgnore
        public final void a(ko3 ko3) {
            o44.a(ko3);
            this.a = ko3;
        }

        @DexIgnore
        public void a(SignUpActivity signUpActivity) {
            b(signUpActivity);
        }

        @DexIgnore
        public final SignUpPresenter a(SignUpPresenter signUpPresenter) {
            oo3.a(signUpPresenter, y52.this.H());
            oo3.a(signUpPresenter, (ln2) y52.this.l1.get());
            oo3.a(signUpPresenter, y52.this.I());
            oo3.a(signUpPresenter, y52.this.L());
            oo3.a(signUpPresenter, y52.this.K());
            oo3.a(signUpPresenter, y52.this.J());
            oo3.a(signUpPresenter, (UserRepository) y52.this.w.get());
            oo3.a(signUpPresenter, (DeviceRepository) y52.this.T.get());
            oo3.a(signUpPresenter, (k62) y52.this.V.get());
            oo3.a(signUpPresenter, y52.this.u());
            oo3.a(signUpPresenter, y52.this.v());
            oo3.a(signUpPresenter, y52.this.p());
            oo3.a(signUpPresenter, y52.this.w());
            oo3.a(signUpPresenter, y52.this.t());
            oo3.a(signUpPresenter, y52.this.r());
            oo3.a(signUpPresenter, (AlarmsRepository) y52.this.B.get());
            oo3.a(signUpPresenter, new uq2());
            oo3.a(signUpPresenter, y52.this.l());
            oo3.a(signUpPresenter, y52.this.n());
            oo3.a(signUpPresenter, (fn2) y52.this.c.get());
            oo3.a(signUpPresenter, y52.this.c());
            oo3.a(signUpPresenter, y52.this.d());
            oo3.a(signUpPresenter, (AnalyticsHelper) y52.this.E0.get());
            oo3.a(signUpPresenter, y52.this.C());
            oo3.a(signUpPresenter, (SummariesRepository) y52.this.H.get());
            oo3.a(signUpPresenter, (SleepSummariesRepository) y52.this.K.get());
            oo3.a(signUpPresenter, (GoalTrackingRepository) y52.this.k0.get());
            oo3.a(signUpPresenter, y52.this.q());
            oo3.a(signUpPresenter, y52.this.s());
            oo3.a(signUpPresenter, y52.this.O());
            oo3.a(signUpPresenter, y52.this.B());
            oo3.a(signUpPresenter, y52.this.b0());
            oo3.a(signUpPresenter);
            return signUpPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p implements f53 {
        @DexIgnore
        public void a(z43 z43) {
            b(z43);
        }

        @DexIgnore
        public final z43 b(z43 z43) {
            a53.a(z43, (k42) y52.this.O1.get());
            return z43;
        }

        @DexIgnore
        public p(g53 g53) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p0 implements bw2 {
        @DexIgnore
        public fw2 a;

        @DexIgnore
        public final NotificationAppsPresenter a() {
            NotificationAppsPresenter a2 = hw2.a(gw2.a(this.a), (k62) y52.this.V.get(), y52.this.y(), new qx2(), b(), (fn2) y52.this.c.get(), (NotificationSettingsDatabase) y52.this.f.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final jw2 b() {
            return new jw2((NotificationsRepository) y52.this.O.get());
        }

        @DexIgnore
        public p0(fw2 fw2) {
            a(fw2);
        }

        @DexIgnore
        public final NotificationAppsActivity b(NotificationAppsActivity notificationAppsActivity) {
            eq2.a((BaseActivity) notificationAppsActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) notificationAppsActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) notificationAppsActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) notificationAppsActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) notificationAppsActivity, new hr2());
            aw2.a(notificationAppsActivity, a());
            return notificationAppsActivity;
        }

        @DexIgnore
        public final void a(fw2 fw2) {
            o44.a(fw2);
            this.a = fw2;
        }

        @DexIgnore
        public void a(NotificationAppsActivity notificationAppsActivity) {
            b(notificationAppsActivity);
        }

        @DexIgnore
        public final NotificationAppsPresenter a(NotificationAppsPresenter notificationAppsPresenter) {
            iw2.a(notificationAppsPresenter);
            return notificationAppsPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p1 implements vf3 {
        @DexIgnore
        public zf3 a;

        @DexIgnore
        public final SleepDetailPresenter a() {
            SleepDetailPresenter a2 = bg3.a(ag3.a(this.a), (SleepSummariesRepository) y52.this.K.get(), (SleepSessionsRepository) y52.this.h0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SleepDetailActivity b(SleepDetailActivity sleepDetailActivity) {
            eq2.a((BaseActivity) sleepDetailActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) sleepDetailActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) sleepDetailActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) sleepDetailActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) sleepDetailActivity, new hr2());
            uf3.a(sleepDetailActivity, a());
            return sleepDetailActivity;
        }

        @DexIgnore
        public p1(zf3 zf3) {
            a(zf3);
        }

        @DexIgnore
        public final void a(zf3 zf3) {
            o44.a(zf3);
            this.a = zf3;
        }

        @DexIgnore
        public void a(SleepDetailActivity sleepDetailActivity) {
            b(sleepDetailActivity);
        }

        @DexIgnore
        public final SleepDetailPresenter a(SleepDetailPresenter sleepDetailPresenter) {
            cg3.a(sleepDetailPresenter);
            return sleepDetailPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q implements a43 {
        @DexIgnore
        public e43 a;

        @DexIgnore
        public final ComplicationSearchPresenter a() {
            ComplicationSearchPresenter a2 = g43.a(f43.a(this.a), y52.this.f(), (fn2) y52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ComplicationSearchActivity b(ComplicationSearchActivity complicationSearchActivity) {
            eq2.a((BaseActivity) complicationSearchActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) complicationSearchActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) complicationSearchActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) complicationSearchActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) complicationSearchActivity, new hr2());
            z33.a(complicationSearchActivity, a());
            return complicationSearchActivity;
        }

        @DexIgnore
        public q(e43 e43) {
            a(e43);
        }

        @DexIgnore
        public final void a(e43 e43) {
            o44.a(e43);
            this.a = e43;
        }

        @DexIgnore
        public void a(ComplicationSearchActivity complicationSearchActivity) {
            b(complicationSearchActivity);
        }

        @DexIgnore
        public final ComplicationSearchPresenter a(ComplicationSearchPresenter complicationSearchPresenter) {
            h43.a(complicationSearchPresenter);
            return complicationSearchPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q0 implements lw2 {
        @DexIgnore
        public qw2 a;

        @DexIgnore
        public final NotificationCallsAndMessagesPresenter a() {
            NotificationCallsAndMessagesPresenter a2 = sw2.a(rw2.a(this.a), (k62) y52.this.V.get(), new qx2(), b(), c(), y52.this.y(), (NotificationSettingsDao) y52.this.P1.get(), (NotificationSettingsDatabase) y52.this.f.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final sx2 b() {
            return new sx2((NotificationsRepository) y52.this.O.get());
        }

        @DexIgnore
        public final tx2 c() {
            return new tx2((NotificationsRepository) y52.this.O.get());
        }

        @DexIgnore
        public q0(qw2 qw2) {
            a(qw2);
        }

        @DexIgnore
        public final NotificationCallsAndMessagesActivity b(NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity) {
            eq2.a((BaseActivity) notificationCallsAndMessagesActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) notificationCallsAndMessagesActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) notificationCallsAndMessagesActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) notificationCallsAndMessagesActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) notificationCallsAndMessagesActivity, new hr2());
            kw2.a(notificationCallsAndMessagesActivity, a());
            return notificationCallsAndMessagesActivity;
        }

        @DexIgnore
        public final void a(qw2 qw2) {
            o44.a(qw2);
            this.a = qw2;
        }

        @DexIgnore
        public void a(NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity) {
            b(notificationCallsAndMessagesActivity);
        }

        @DexIgnore
        public final NotificationCallsAndMessagesPresenter a(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter) {
            tw2.a(notificationCallsAndMessagesPresenter);
            return notificationCallsAndMessagesPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q1 implements ed3 {
        @DexIgnore
        public ld3 a;

        @DexIgnore
        public final SleepOverviewDayPresenter a() {
            SleepOverviewDayPresenter a2 = id3.a(md3.a(this.a), (SleepSummariesRepository) y52.this.K.get(), (SleepSessionsRepository) y52.this.h0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SleepOverviewMonthPresenter b() {
            SleepOverviewMonthPresenter a2 = sd3.a(nd3.a(this.a), (UserRepository) y52.this.w.get(), (SleepSummariesRepository) y52.this.K.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SleepOverviewWeekPresenter c() {
            SleepOverviewWeekPresenter a2 = xd3.a(od3.a(this.a), (UserRepository) y52.this.w.get(), (SleepSummariesRepository) y52.this.K.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public q1(ld3 ld3) {
            a(ld3);
        }

        @DexIgnore
        public final void a(ld3 ld3) {
            o44.a(ld3);
            this.a = ld3;
        }

        @DexIgnore
        public final SleepOverviewFragment b(SleepOverviewFragment sleepOverviewFragment) {
            kd3.a(sleepOverviewFragment, a());
            kd3.a(sleepOverviewFragment, c());
            kd3.a(sleepOverviewFragment, b());
            return sleepOverviewFragment;
        }

        @DexIgnore
        public void a(SleepOverviewFragment sleepOverviewFragment) {
            b(sleepOverviewFragment);
        }

        @DexIgnore
        public final SleepOverviewDayPresenter a(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
            jd3.a(sleepOverviewDayPresenter);
            return sleepOverviewDayPresenter;
        }

        @DexIgnore
        public final SleepOverviewWeekPresenter a(SleepOverviewWeekPresenter sleepOverviewWeekPresenter) {
            yd3.a(sleepOverviewWeekPresenter);
            return sleepOverviewWeekPresenter;
        }

        @DexIgnore
        public final SleepOverviewMonthPresenter a(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
            td3.a(sleepOverviewMonthPresenter);
            return sleepOverviewMonthPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r implements j13 {
        @DexIgnore
        public k13 a;

        @DexIgnore
        public final ComplicationsPresenter a() {
            ComplicationsPresenter a2 = n23.a(l13.a(this.a), y52.this.b());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CustomizeThemePresenter b() {
            CustomizeThemePresenter a2 = n43.a(m13.a(this.a), y52.this.a0());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WatchAppsPresenter c() {
            WatchAppsPresenter a2 = v43.a(n13.a(this.a), y52.this.b(), (fn2) y52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public r(k13 k13) {
            a(k13);
        }

        @DexIgnore
        public final void a(k13 k13) {
            o44.a(k13);
            this.a = k13;
        }

        @DexIgnore
        public final v13 b(v13 v13) {
            w13.a(v13, a());
            w13.a(v13, c());
            w13.a(v13, b());
            w13.a(v13, (k42) y52.this.O1.get());
            return v13;
        }

        @DexIgnore
        public void a(v13 v13) {
            b(v13);
        }

        @DexIgnore
        public final ComplicationsPresenter a(ComplicationsPresenter complicationsPresenter) {
            o23.a(complicationsPresenter);
            return complicationsPresenter;
        }

        @DexIgnore
        public final WatchAppsPresenter a(WatchAppsPresenter watchAppsPresenter) {
            w43.a(watchAppsPresenter);
            return watchAppsPresenter;
        }

        @DexIgnore
        public final CustomizeThemePresenter a(CustomizeThemePresenter customizeThemePresenter) {
            o43.a(customizeThemePresenter);
            return customizeThemePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r0 implements fz2 {
        @DexIgnore
        public jz2 a;

        @DexIgnore
        public final i03 a() {
            return new i03((Context) y52.this.b.get());
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedPresenter b() {
            NotificationContactsAndAppsAssignedPresenter a2 = mz2.a(kz2.a(this.a), lz2.a(this.a), this.a.a(), (k62) y52.this.V.get(), new u03(), a(), c(), d());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final f13 c() {
            return new f13((NotificationsRepository) y52.this.O.get());
        }

        @DexIgnore
        public final wq2 d() {
            return new wq2((NotificationsRepository) y52.this.O.get(), (DeviceRepository) y52.this.T.get());
        }

        @DexIgnore
        public r0(jz2 jz2) {
            a(jz2);
        }

        @DexIgnore
        public final void a(jz2 jz2) {
            o44.a(jz2);
            this.a = jz2;
        }

        @DexIgnore
        public void a(NotificationContactsAndAppsAssignedActivity notificationContactsAndAppsAssignedActivity) {
            b(notificationContactsAndAppsAssignedActivity);
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedPresenter a(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
            nz2.a(notificationContactsAndAppsAssignedPresenter);
            return notificationContactsAndAppsAssignedPresenter;
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedActivity b(NotificationContactsAndAppsAssignedActivity notificationContactsAndAppsAssignedActivity) {
            eq2.a((BaseActivity) notificationContactsAndAppsAssignedActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) notificationContactsAndAppsAssignedActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) notificationContactsAndAppsAssignedActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) notificationContactsAndAppsAssignedActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) notificationContactsAndAppsAssignedActivity, new hr2());
            ez2.a(notificationContactsAndAppsAssignedActivity, b());
            return notificationContactsAndAppsAssignedActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r1 implements zo3 {
        @DexIgnore
        public cp3 a;

        @DexIgnore
        public final SplashPresenter a() {
            SplashPresenter a2 = ep3.a(dp3.a(this.a), (UserRepository) y52.this.w.get(), (MigrationHelper) y52.this.R1.get(), (fn2) y52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SplashScreenActivity b(SplashScreenActivity splashScreenActivity) {
            eq2.a((BaseActivity) splashScreenActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) splashScreenActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) splashScreenActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) splashScreenActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) splashScreenActivity, new hr2());
            gp3.a(splashScreenActivity, a());
            return splashScreenActivity;
        }

        @DexIgnore
        public r1(cp3 cp3) {
            a(cp3);
        }

        @DexIgnore
        public final void a(cp3 cp3) {
            o44.a(cp3);
            this.a = cp3;
        }

        @DexIgnore
        public void a(SplashScreenActivity splashScreenActivity) {
            b(splashScreenActivity);
        }

        @DexIgnore
        public final SplashPresenter a(SplashPresenter splashPresenter) {
            fp3.a(splashPresenter);
            return splashPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s implements h23 {
        @DexIgnore
        public void a(k23 k23) {
            b(k23);
        }

        @DexIgnore
        public final k23 b(k23 k23) {
            l23.a(k23, (k42) y52.this.O1.get());
            return k23;
        }

        @DexIgnore
        public s(m23 m23) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s0 implements vw2 {
        @DexIgnore
        public zw2 a;

        @DexIgnore
        public final NotificationContactsPresenter a() {
            NotificationContactsPresenter a2 = cx2.a(bx2.a(this.a), (k62) y52.this.V.get(), new qx2(), b(), ax2.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final tx2 b() {
            return new tx2((NotificationsRepository) y52.this.O.get());
        }

        @DexIgnore
        public s0(zw2 zw2) {
            a(zw2);
        }

        @DexIgnore
        public final NotificationContactsActivity b(NotificationContactsActivity notificationContactsActivity) {
            eq2.a((BaseActivity) notificationContactsActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) notificationContactsActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) notificationContactsActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) notificationContactsActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) notificationContactsActivity, new hr2());
            uw2.a(notificationContactsActivity, a());
            return notificationContactsActivity;
        }

        @DexIgnore
        public final void a(zw2 zw2) {
            o44.a(zw2);
            this.a = zw2;
        }

        @DexIgnore
        public void a(NotificationContactsActivity notificationContactsActivity) {
            b(notificationContactsActivity);
        }

        @DexIgnore
        public final NotificationContactsPresenter a(NotificationContactsPresenter notificationContactsPresenter) {
            dx2.a(notificationContactsPresenter);
            return notificationContactsPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s1 implements ip3 {
        @DexIgnore
        public mp3 a;

        @DexIgnore
        public final op3 a() {
            op3 a2 = pp3.a(np3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final TroubleshootingActivity b(TroubleshootingActivity troubleshootingActivity) {
            eq2.a((BaseActivity) troubleshootingActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) troubleshootingActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) troubleshootingActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) troubleshootingActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) troubleshootingActivity, new hr2());
            hp3.a(troubleshootingActivity, a());
            return troubleshootingActivity;
        }

        @DexIgnore
        public s1(mp3 mp3) {
            a(mp3);
        }

        @DexIgnore
        public final void a(mp3 mp3) {
            o44.a(mp3);
            this.a = mp3;
        }

        @DexIgnore
        public void a(TroubleshootingActivity troubleshootingActivity) {
            b(troubleshootingActivity);
        }

        @DexIgnore
        public final op3 a(op3 op3) {
            qp3.a(op3);
            return op3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t implements iu2 {
        @DexIgnore
        public lu2 a;

        @DexIgnore
        public final ConnectedAppsPresenter a() {
            ConnectedAppsPresenter a2 = nu2.a(mu2.a(this.a), (UserRepository) y52.this.w.get(), (PortfolioApp) y52.this.d.get(), y52.this.E());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ConnectedAppsActivity b(ConnectedAppsActivity connectedAppsActivity) {
            eq2.a((BaseActivity) connectedAppsActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) connectedAppsActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) connectedAppsActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) connectedAppsActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) connectedAppsActivity, new hr2());
            hu2.a(connectedAppsActivity, a());
            return connectedAppsActivity;
        }

        @DexIgnore
        public t(lu2 lu2) {
            a(lu2);
        }

        @DexIgnore
        public final void a(lu2 lu2) {
            o44.a(lu2);
            this.a = lu2;
        }

        @DexIgnore
        public void a(ConnectedAppsActivity connectedAppsActivity) {
            b(connectedAppsActivity);
        }

        @DexIgnore
        public final ConnectedAppsPresenter a(ConnectedAppsPresenter connectedAppsPresenter) {
            ou2.a(connectedAppsPresenter);
            return connectedAppsPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t0 implements pz2 {
        @DexIgnore
        public tz2 a;

        @DexIgnore
        public final NotificationDialLandingPresenter a() {
            NotificationDialLandingPresenter a2 = wz2.a(vz2.a(this.a), b(), uz2.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationsLoader b() {
            return NotificationsLoader_Factory.newNotificationsLoader((Context) y52.this.b.get(), (NotificationsRepository) y52.this.O.get());
        }

        @DexIgnore
        public t0(tz2 tz2) {
            a(tz2);
        }

        @DexIgnore
        public final NotificationDialLandingActivity b(NotificationDialLandingActivity notificationDialLandingActivity) {
            eq2.a((BaseActivity) notificationDialLandingActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) notificationDialLandingActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) notificationDialLandingActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) notificationDialLandingActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) notificationDialLandingActivity, new hr2());
            oz2.a(notificationDialLandingActivity, a());
            return notificationDialLandingActivity;
        }

        @DexIgnore
        public final void a(tz2 tz2) {
            o44.a(tz2);
            this.a = tz2;
        }

        @DexIgnore
        public void a(NotificationDialLandingActivity notificationDialLandingActivity) {
            b(notificationDialLandingActivity);
        }

        @DexIgnore
        public final NotificationDialLandingPresenter a(NotificationDialLandingPresenter notificationDialLandingPresenter) {
            xz2.a(notificationDialLandingPresenter);
            return notificationDialLandingPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t1 implements ll3 {
        @DexIgnore
        public ol3 a;

        @DexIgnore
        public final DownloadFirmwareByDeviceModelUsecase a() {
            return new DownloadFirmwareByDeviceModelUsecase((PortfolioApp) y52.this.d.get(), (GuestApiService) y52.this.L.get());
        }

        @DexIgnore
        public final UpdateFirmwarePresenter b() {
            UpdateFirmwarePresenter a2 = rl3.a(pl3.a(this.a), (DeviceRepository) y52.this.T.get(), (UserRepository) y52.this.w.get(), y52.this.l(), (fn2) y52.this.c.get(), y52.this.U(), a());
            a(a2);
            return a2;
        }

        @DexIgnore
        public t1(ol3 ol3) {
            a(ol3);
        }

        @DexIgnore
        public final void a(ol3 ol3) {
            o44.a(ol3);
            this.a = ol3;
        }

        @DexIgnore
        public void a(UpdateFirmwareActivity updateFirmwareActivity) {
            b(updateFirmwareActivity);
        }

        @DexIgnore
        public final UpdateFirmwarePresenter a(UpdateFirmwarePresenter updateFirmwarePresenter) {
            sl3.a(updateFirmwarePresenter);
            return updateFirmwarePresenter;
        }

        @DexIgnore
        public final UpdateFirmwareActivity b(UpdateFirmwareActivity updateFirmwareActivity) {
            eq2.a((BaseActivity) updateFirmwareActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) updateFirmwareActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) updateFirmwareActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) updateFirmwareActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) updateFirmwareActivity, new hr2());
            jm3.a(updateFirmwareActivity, b());
            return updateFirmwareActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u implements i43 {
        @DexIgnore
        public void a(CustomizeThemeFragment customizeThemeFragment) {
            b(customizeThemeFragment);
        }

        @DexIgnore
        public final CustomizeThemeFragment b(CustomizeThemeFragment customizeThemeFragment) {
            l43.a(customizeThemeFragment, (k42) y52.this.O1.get());
            return customizeThemeFragment;
        }

        @DexIgnore
        public u(m43 m43) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u0 implements zz2 {
        @DexIgnore
        public d03 a;

        @DexIgnore
        public final i03 a() {
            return new i03((Context) y52.this.b.get());
        }

        @DexIgnore
        public final NotificationHybridAppPresenter b() {
            NotificationHybridAppPresenter a2 = g03.a(f03.a(this.a), this.a.a(), e03.a(this.a), (k62) y52.this.V.get(), a());
            a(a2);
            return a2;
        }

        @DexIgnore
        public u0(d03 d03) {
            a(d03);
        }

        @DexIgnore
        public final void a(d03 d03) {
            o44.a(d03);
            this.a = d03;
        }

        @DexIgnore
        public void a(NotificationHybridAppActivity notificationHybridAppActivity) {
            b(notificationHybridAppActivity);
        }

        @DexIgnore
        public final NotificationHybridAppPresenter a(NotificationHybridAppPresenter notificationHybridAppPresenter) {
            h03.a(notificationHybridAppPresenter);
            return notificationHybridAppPresenter;
        }

        @DexIgnore
        public final NotificationHybridAppActivity b(NotificationHybridAppActivity notificationHybridAppActivity) {
            eq2.a((BaseActivity) notificationHybridAppActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) notificationHybridAppActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) notificationHybridAppActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) notificationHybridAppActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) notificationHybridAppActivity, new hr2());
            yz2.a(notificationHybridAppActivity, b());
            return notificationHybridAppActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u1 implements r53 {
        @DexIgnore
        public v53 a;

        @DexIgnore
        public final WatchAppSearchPresenter a() {
            WatchAppSearchPresenter a2 = x53.a(w53.a(this.a), y52.this.Y(), (fn2) y52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WatchAppSearchActivity b(WatchAppSearchActivity watchAppSearchActivity) {
            eq2.a((BaseActivity) watchAppSearchActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) watchAppSearchActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) watchAppSearchActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) watchAppSearchActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) watchAppSearchActivity, new hr2());
            q53.a(watchAppSearchActivity, a());
            return watchAppSearchActivity;
        }

        @DexIgnore
        public u1(v53 v53) {
            a(v53);
        }

        @DexIgnore
        public final void a(v53 v53) {
            o44.a(v53);
            this.a = v53;
        }

        @DexIgnore
        public void a(WatchAppSearchActivity watchAppSearchActivity) {
            b(watchAppSearchActivity);
        }

        @DexIgnore
        public final WatchAppSearchPresenter a(WatchAppSearchPresenter watchAppSearchPresenter) {
            y53.a(watchAppSearchPresenter);
            return watchAppSearchPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v implements i73 {
        @DexIgnore
        public void a(p13 p13) {
            b(p13);
        }

        @DexIgnore
        public final p13 b(p13 p13) {
            q13.a(p13, (k42) y52.this.O1.get());
            return p13;
        }

        @DexIgnore
        public v(j73 j73) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v0 implements k03 {
        @DexIgnore
        public o03 a;

        @DexIgnore
        public final NotificationHybridContactPresenter a() {
            NotificationHybridContactPresenter a2 = s03.a(r03.a(this.a), this.a.b(), p03.a(this.a), q03.a(this.a), (k62) y52.this.V.get(), new u03());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridContactActivity b(NotificationHybridContactActivity notificationHybridContactActivity) {
            eq2.a((BaseActivity) notificationHybridContactActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) notificationHybridContactActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) notificationHybridContactActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) notificationHybridContactActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) notificationHybridContactActivity, new hr2());
            j03.a(notificationHybridContactActivity, a());
            return notificationHybridContactActivity;
        }

        @DexIgnore
        public v0(o03 o03) {
            a(o03);
        }

        @DexIgnore
        public final void a(o03 o03) {
            o44.a(o03);
            this.a = o03;
        }

        @DexIgnore
        public void a(NotificationHybridContactActivity notificationHybridContactActivity) {
            b(notificationHybridContactActivity);
        }

        @DexIgnore
        public final NotificationHybridContactPresenter a(NotificationHybridContactPresenter notificationHybridContactPresenter) {
            t03.a(notificationHybridContactPresenter);
            return notificationHybridContactPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v1 implements p43 {
        @DexIgnore
        public void a(s43 s43) {
            b(s43);
        }

        @DexIgnore
        public final s43 b(s43 s43) {
            t43.a(s43, (k42) y52.this.O1.get());
            return s43;
        }

        @DexIgnore
        public v1(u43 u43) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w implements n73 {
        @DexIgnore
        public o73 a;

        @DexIgnore
        public final DashboardActiveTimePresenter a() {
            DashboardActiveTimePresenter a2 = c83.a(p73.a(this.a), (SummariesRepository) y52.this.H.get(), y52.this.x(), (ActivitySummaryDao) y52.this.E.get(), (FitnessDatabase) y52.this.D.get(), (UserRepository) y52.this.w.get(), (i42) y52.this.M.get(), (yk2) y52.this.G.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardActivityPresenter b() {
            DashboardActivityPresenter a2 = c93.a(q73.a(this.a), (SummariesRepository) y52.this.H.get(), y52.this.x(), (ActivitySummaryDao) y52.this.E.get(), (FitnessDatabase) y52.this.D.get(), (UserRepository) y52.this.w.get(), (i42) y52.this.M.get(), (yk2) y52.this.G.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardCaloriesPresenter c() {
            DashboardCaloriesPresenter a2 = ca3.a(r73.a(this.a), (SummariesRepository) y52.this.H.get(), y52.this.x(), (ActivitySummaryDao) y52.this.E.get(), (FitnessDatabase) y52.this.D.get(), (UserRepository) y52.this.w.get(), (i42) y52.this.M.get(), (yk2) y52.this.G.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardGoalTrackingPresenter d() {
            DashboardGoalTrackingPresenter a2 = cb3.a(s73.a(this.a), (GoalTrackingRepository) y52.this.k0.get(), (UserRepository) y52.this.w.get(), (GoalTrackingDao) y52.this.j0.get(), (GoalTrackingDatabase) y52.this.i0.get(), (i42) y52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardHeartRatePresenter e() {
            DashboardHeartRatePresenter a2 = cc3.a(t73.a(this.a), (HeartRateSummaryRepository) y52.this.z0.get(), y52.this.x(), (HeartRateDailySummaryDao) y52.this.y0.get(), (FitnessDatabase) y52.this.D.get(), (UserRepository) y52.this.w.get(), (i42) y52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardSleepPresenter f() {
            DashboardSleepPresenter a2 = cd3.a(u73.a(this.a), (SleepSummariesRepository) y52.this.K.get(), (SleepSessionsRepository) y52.this.h0.get(), y52.this.x(), (SleepDao) y52.this.J.get(), (SleepDatabase) y52.this.I.get(), (UserRepository) y52.this.w.get(), (i42) y52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public w(o73 o73) {
            a(o73);
        }

        @DexIgnore
        public final void a(o73 o73) {
            o44.a(o73);
            this.a = o73;
        }

        @DexIgnore
        public final tu2 b(tu2 tu2) {
            vu2.a(tu2, b());
            vu2.a(tu2, a());
            vu2.a(tu2, c());
            vu2.a(tu2, e());
            vu2.a(tu2, f());
            vu2.a(tu2, d());
            return tu2;
        }

        @DexIgnore
        public void a(tu2 tu2) {
            b(tu2);
        }

        @DexIgnore
        public final DashboardActivityPresenter a(DashboardActivityPresenter dashboardActivityPresenter) {
            d93.a(dashboardActivityPresenter);
            return dashboardActivityPresenter;
        }

        @DexIgnore
        public final DashboardActiveTimePresenter a(DashboardActiveTimePresenter dashboardActiveTimePresenter) {
            d83.a(dashboardActiveTimePresenter);
            return dashboardActiveTimePresenter;
        }

        @DexIgnore
        public final DashboardCaloriesPresenter a(DashboardCaloriesPresenter dashboardCaloriesPresenter) {
            da3.a(dashboardCaloriesPresenter);
            return dashboardCaloriesPresenter;
        }

        @DexIgnore
        public final DashboardHeartRatePresenter a(DashboardHeartRatePresenter dashboardHeartRatePresenter) {
            dc3.a(dashboardHeartRatePresenter);
            return dashboardHeartRatePresenter;
        }

        @DexIgnore
        public final DashboardSleepPresenter a(DashboardSleepPresenter dashboardSleepPresenter) {
            dd3.a(dashboardSleepPresenter);
            return dashboardSleepPresenter;
        }

        @DexIgnore
        public final DashboardGoalTrackingPresenter a(DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter) {
            db3.a(dashboardGoalTrackingPresenter);
            return dashboardGoalTrackingPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w0 implements w03 {
        @DexIgnore
        public a13 a;

        @DexIgnore
        public final NotificationHybridEveryonePresenter a() {
            NotificationHybridEveryonePresenter a2 = d13.a(c13.a(this.a), this.a.b(), b13.a(this.a), (k62) y52.this.V.get(), new u03());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridEveryoneActivity b(NotificationHybridEveryoneActivity notificationHybridEveryoneActivity) {
            eq2.a((BaseActivity) notificationHybridEveryoneActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) notificationHybridEveryoneActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) notificationHybridEveryoneActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) notificationHybridEveryoneActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) notificationHybridEveryoneActivity, new hr2());
            v03.a(notificationHybridEveryoneActivity, a());
            return notificationHybridEveryoneActivity;
        }

        @DexIgnore
        public w0(a13 a13) {
            a(a13);
        }

        @DexIgnore
        public final void a(a13 a13) {
            o44.a(a13);
            this.a = a13;
        }

        @DexIgnore
        public void a(NotificationHybridEveryoneActivity notificationHybridEveryoneActivity) {
            b(notificationHybridEveryoneActivity);
        }

        @DexIgnore
        public final NotificationHybridEveryonePresenter a(NotificationHybridEveryonePresenter notificationHybridEveryonePresenter) {
            e13.a(notificationHybridEveryonePresenter);
            return notificationHybridEveryonePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w1 implements tp3 {
        @DexIgnore
        public void a(up3 up3) {
            b(up3);
        }

        @DexIgnore
        public final up3 b(up3 up3) {
            vp3.a(up3, (k42) y52.this.O1.get());
            return up3;
        }

        @DexIgnore
        public w1(wp3 wp3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x implements pi3 {
        @DexIgnore
        public si3 a;

        @DexIgnore
        public final DeleteAccountPresenter a() {
            DeleteAccountPresenter a2 = ui3.a(ti3.a(this.a), (DeviceRepository) y52.this.T.get(), (AnalyticsHelper) y52.this.E0.get(), b(), y52.this.k());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final lr2 b() {
            return new lr2((UserRepository) y52.this.w.get(), (DeviceRepository) y52.this.T.get(), (fn2) y52.this.c.get());
        }

        @DexIgnore
        public x(si3 si3) {
            a(si3);
        }

        @DexIgnore
        public final DeleteAccountActivity b(DeleteAccountActivity deleteAccountActivity) {
            eq2.a((BaseActivity) deleteAccountActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) deleteAccountActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) deleteAccountActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) deleteAccountActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) deleteAccountActivity, new hr2());
            oi3.a(deleteAccountActivity, a());
            return deleteAccountActivity;
        }

        @DexIgnore
        public final void a(si3 si3) {
            o44.a(si3);
            this.a = si3;
        }

        @DexIgnore
        public void a(DeleteAccountActivity deleteAccountActivity) {
            b(deleteAccountActivity);
        }

        @DexIgnore
        public final DeleteAccountPresenter a(DeleteAccountPresenter deleteAccountPresenter) {
            vi3.a(deleteAccountPresenter);
            return deleteAccountPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x0 implements gx2 {
        @DexIgnore
        public lx2 a;

        @DexIgnore
        public final nx2 a() {
            nx2 a2 = ox2.a(mx2.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public void a(ew2 ew2) {
        }

        @DexIgnore
        public final ow2 b(ow2 ow2) {
            pw2.a(ow2, a());
            pw2.a(ow2, (k42) y52.this.O1.get());
            return ow2;
        }

        @DexIgnore
        public x0(lx2 lx2) {
            a(lx2);
        }

        @DexIgnore
        public final void a(lx2 lx2) {
            o44.a(lx2);
            this.a = lx2;
        }

        @DexIgnore
        public void a(ow2 ow2) {
            b(ow2);
        }

        @DexIgnore
        public final jx2 b(jx2 jx2) {
            kx2.a(jx2, (k42) y52.this.O1.get());
            return jx2;
        }

        @DexIgnore
        public void a(jx2 jx2) {
            b(jx2);
        }

        @DexIgnore
        public final nx2 a(nx2 nx2) {
            px2.a(nx2);
            return nx2;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x1 implements j53 {
        @DexIgnore
        public m53 a;

        @DexIgnore
        public final WeatherSettingPresenter a() {
            WeatherSettingPresenter a2 = o53.a(n53.a(this.a), (GoogleApiService) y52.this.x1.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WeatherSettingActivity b(WeatherSettingActivity weatherSettingActivity) {
            eq2.a((BaseActivity) weatherSettingActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) weatherSettingActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) weatherSettingActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) weatherSettingActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) weatherSettingActivity, new hr2());
            i53.a(weatherSettingActivity, a());
            return weatherSettingActivity;
        }

        @DexIgnore
        public x1(m53 m53) {
            a(m53);
        }

        @DexIgnore
        public final void a(m53 m53) {
            o44.a(m53);
            this.a = m53;
        }

        @DexIgnore
        public void a(WeatherSettingActivity weatherSettingActivity) {
            b(weatherSettingActivity);
        }

        @DexIgnore
        public final WeatherSettingPresenter a(WeatherSettingPresenter weatherSettingPresenter) {
            p53.a(weatherSettingPresenter);
            return weatherSettingPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y implements s13 {
        @DexIgnore
        public x13 a;

        @DexIgnore
        public final DianaCustomizeEditPresenter a() {
            DianaCustomizeEditPresenter a2 = z13.a(y13.a(this.a), (UserRepository) y52.this.w.get(), this.a.a(), b(), new xm2(), (fn2) y52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SetDianaPresetToWatchUseCase b() {
            return new SetDianaPresetToWatchUseCase((DianaPresetRepository) y52.this.p.get(), y52.this.e(), y52.this.X(), y52.this.a0());
        }

        @DexIgnore
        public y(x13 x13) {
            a(x13);
        }

        @DexIgnore
        public final DianaCustomizeEditActivity b(DianaCustomizeEditActivity dianaCustomizeEditActivity) {
            eq2.a((BaseActivity) dianaCustomizeEditActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) dianaCustomizeEditActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) dianaCustomizeEditActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) dianaCustomizeEditActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) dianaCustomizeEditActivity, new hr2());
            r13.a(dianaCustomizeEditActivity, a());
            r13.a(dianaCustomizeEditActivity, (k42) y52.this.O1.get());
            return dianaCustomizeEditActivity;
        }

        @DexIgnore
        public final void a(x13 x13) {
            o44.a(x13);
            this.a = x13;
        }

        @DexIgnore
        public void a(DianaCustomizeEditActivity dianaCustomizeEditActivity) {
            b(dianaCustomizeEditActivity);
        }

        @DexIgnore
        public final DianaCustomizeEditPresenter a(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            a23.a(dianaCustomizeEditPresenter);
            return dianaCustomizeEditPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y0 implements ay2 {
        @DexIgnore
        public fy2 a;

        @DexIgnore
        public final NotificationWatchRemindersPresenter a() {
            NotificationWatchRemindersPresenter a2 = hy2.a(gy2.a(this.a), (fn2) y52.this.c.get(), (RemindersSettingsDatabase) y52.this.C0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationWatchRemindersActivity b(NotificationWatchRemindersActivity notificationWatchRemindersActivity) {
            eq2.a((BaseActivity) notificationWatchRemindersActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) notificationWatchRemindersActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) notificationWatchRemindersActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) notificationWatchRemindersActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) notificationWatchRemindersActivity, new hr2());
            zx2.a(notificationWatchRemindersActivity, a());
            return notificationWatchRemindersActivity;
        }

        @DexIgnore
        public y0(fy2 fy2) {
            a(fy2);
        }

        @DexIgnore
        public final void a(fy2 fy2) {
            o44.a(fy2);
            this.a = fy2;
        }

        @DexIgnore
        public void a(NotificationWatchRemindersActivity notificationWatchRemindersActivity) {
            b(notificationWatchRemindersActivity);
        }

        @DexIgnore
        public final NotificationWatchRemindersPresenter a(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            iy2.a(notificationWatchRemindersPresenter);
            return notificationWatchRemindersPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y1 implements uq3 {
        @DexIgnore
        public zq3 a;

        @DexIgnore
        public final yq3 a() {
            yq3 a2 = br3.a(ar3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WelcomeActivity b(WelcomeActivity welcomeActivity) {
            eq2.a((BaseActivity) welcomeActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) welcomeActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) welcomeActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) welcomeActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) welcomeActivity, new hr2());
            tq3.a(welcomeActivity, a());
            return welcomeActivity;
        }

        @DexIgnore
        public y1(zq3 zq3) {
            a(zq3);
        }

        @DexIgnore
        public final void a(zq3 zq3) {
            o44.a(zq3);
            this.a = zq3;
        }

        @DexIgnore
        public void a(WelcomeActivity welcomeActivity) {
            b(welcomeActivity);
        }

        @DexIgnore
        public final yq3 a(yq3 yq3) {
            cr3.a(yq3);
            return yq3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z implements wo3 {
        @DexIgnore
        public xo3 a;

        @DexIgnore
        public final to3 a() {
            to3 a2 = uo3.a(yo3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final VerifyEmailOtp b() {
            return new VerifyEmailOtp((UserRepository) y52.this.w.get());
        }

        @DexIgnore
        public z(xo3 xo3) {
            a(xo3);
        }

        @DexIgnore
        public final EmailOtpVerificationActivity b(EmailOtpVerificationActivity emailOtpVerificationActivity) {
            eq2.a((BaseActivity) emailOtpVerificationActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) emailOtpVerificationActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) emailOtpVerificationActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) emailOtpVerificationActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) emailOtpVerificationActivity, new hr2());
            po3.a(emailOtpVerificationActivity, a());
            return emailOtpVerificationActivity;
        }

        @DexIgnore
        public final void a(xo3 xo3) {
            o44.a(xo3);
            this.a = xo3;
        }

        @DexIgnore
        public void a(EmailOtpVerificationActivity emailOtpVerificationActivity) {
            b(emailOtpVerificationActivity);
        }

        @DexIgnore
        public final to3 a(to3 to3) {
            vo3.a(to3, y52.this.O());
            vo3.a(to3, b());
            vo3.a(to3);
            return to3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z0 implements el3 {
        @DexIgnore
        public hl3 a;

        @DexIgnore
        public final GetRecommendedGoalUseCase a() {
            return new GetRecommendedGoalUseCase((SummariesRepository) y52.this.H.get(), (SleepSummariesRepository) y52.this.K.get(), (SleepSessionsRepository) y52.this.h0.get());
        }

        @DexIgnore
        public final OnboardingHeightWeightPresenter b() {
            OnboardingHeightWeightPresenter a2 = jl3.a(il3.a(this.a), (UserRepository) y52.this.w.get(), a());
            a(a2);
            return a2;
        }

        @DexIgnore
        public z0(hl3 hl3) {
            a(hl3);
        }

        @DexIgnore
        public final void a(hl3 hl3) {
            o44.a(hl3);
            this.a = hl3;
        }

        @DexIgnore
        public void a(OnboardingHeightWeightActivity onboardingHeightWeightActivity) {
            b(onboardingHeightWeightActivity);
        }

        @DexIgnore
        public final OnboardingHeightWeightPresenter a(OnboardingHeightWeightPresenter onboardingHeightWeightPresenter) {
            kl3.a(onboardingHeightWeightPresenter);
            return onboardingHeightWeightPresenter;
        }

        @DexIgnore
        public final OnboardingHeightWeightActivity b(OnboardingHeightWeightActivity onboardingHeightWeightActivity) {
            eq2.a((BaseActivity) onboardingHeightWeightActivity, (UserRepository) y52.this.w.get());
            eq2.a((BaseActivity) onboardingHeightWeightActivity, (fn2) y52.this.c.get());
            eq2.a((BaseActivity) onboardingHeightWeightActivity, (DeviceRepository) y52.this.T.get());
            eq2.a((BaseActivity) onboardingHeightWeightActivity, (MigrationManager) y52.this.o1.get());
            eq2.a((BaseActivity) onboardingHeightWeightActivity, new hr2());
            dl3.a(onboardingHeightWeightActivity, b());
            return onboardingHeightWeightActivity;
        }
    }

    @DexIgnore
    public void a(LocaleChangedReceiver localeChangedReceiver) {
    }

    @DexIgnore
    public y52(i iVar) {
        a(iVar);
        b(iVar);
    }

    @DexIgnore
    public static i c0() {
        return new i();
    }

    @DexIgnore
    public final GetHybridDeviceSettingUseCase A() {
        return new GetHybridDeviceSettingUseCase(this.Z.get(), this.s0.get(), this.T.get(), this.O.get(), b(), this.B.get());
    }

    @DexIgnore
    public final GetSecretKeyUseCase B() {
        return new GetSecretKeyUseCase(o(), this.T.get());
    }

    @DexIgnore
    public final or2 C() {
        return new or2(this.w.get());
    }

    @DexIgnore
    public final GetWeather D() {
        return new GetWeather(this.n.get());
    }

    @DexIgnore
    public final al2 E() {
        return b52.a(this.a, this.b.get(), this.M.get(), this.c.get());
    }

    @DexIgnore
    public final HybridSyncUseCase F() {
        return new HybridSyncUseCase(this.s0.get(), this.c.get(), this.T.get(), this.d.get(), this.Z.get(), this.O.get(), this.E0.get(), this.B.get());
    }

    @DexIgnore
    public final LoginEmailUseCase G() {
        return new LoginEmailUseCase(this.w.get());
    }

    @DexIgnore
    public final sr2 H() {
        return new sr2(this.k1.get());
    }

    @DexIgnore
    public final tr2 I() {
        return new tr2(this.l0.get());
    }

    @DexIgnore
    public final LoginSocialUseCase J() {
        return new LoginSocialUseCase(this.w.get());
    }

    @DexIgnore
    public final ur2 K() {
        return new ur2(this.m1.get());
    }

    @DexIgnore
    public final vr2 L() {
        return new vr2(this.l1.get());
    }

    @DexIgnore
    public final Map<Class<? extends ListenableWorker>, Provider<mu3<? extends ListenableWorker>>> M() {
        return ImmutableMap.of(PushPendingDataWorker.class, this.d1);
    }

    @DexIgnore
    public final MicroAppLastSettingRepository N() {
        return new MicroAppLastSettingRepository(this.t0.get());
    }

    @DexIgnore
    public final RequestEmailOtp O() {
        return new RequestEmailOtp(this.w.get());
    }

    @DexIgnore
    public final SetNotificationUseCase P() {
        return new SetNotificationUseCase(y(), new qx2(), this.f.get(), this.O.get(), this.T.get(), this.c.get());
    }

    @DexIgnore
    public final SignUpEmailUseCase Q() {
        return new SignUpEmailUseCase(this.w.get(), this.c.get());
    }

    @DexIgnore
    public final SignUpSocialUseCase R() {
        return new SignUpSocialUseCase(this.w.get(), this.c.get());
    }

    @DexIgnore
    public final ir2 S() {
        return jr2.a(this.O.get());
    }

    @DexIgnore
    public final dr2 T() {
        return new dr2(this.T.get(), this.c.get());
    }

    @DexIgnore
    public final UpdateFirmwareUsecase U() {
        return new UpdateFirmwareUsecase(this.T.get(), this.c.get());
    }

    @DexIgnore
    public final UpdateUser V() {
        return new UpdateUser(this.w.get());
    }

    @DexIgnore
    public final VerifySecretKeyUseCase W() {
        return new VerifySecretKeyUseCase(this.T.get(), this.w.get(), j(), this.d.get());
    }

    @DexIgnore
    public final WatchAppLastSettingRepository X() {
        return new WatchAppLastSettingRepository(this.v0.get());
    }

    @DexIgnore
    public final WatchAppRepository Y() {
        return new WatchAppRepository(this.m0.get(), this.n0.get(), this.d.get());
    }

    @DexIgnore
    public final WatchFaceRemoteDataSource Z() {
        return new WatchFaceRemoteDataSource(this.n.get());
    }

    @DexIgnore
    public final CategoryRemoteDataSource a() {
        return new CategoryRemoteDataSource(this.n.get());
    }

    @DexIgnore
    public final WatchFaceRepository a0() {
        return new WatchFaceRepository(this.b.get(), this.L0.get(), Z());
    }

    @DexIgnore
    public final CategoryRepository b() {
        return new CategoryRepository(this.G0.get(), a());
    }

    @DexIgnore
    public final WatchLocalizationRepository b0() {
        return new WatchLocalizationRepository(this.n.get(), this.c.get());
    }

    @DexIgnore
    public final CheckAuthenticationEmailExisting c() {
        return new CheckAuthenticationEmailExisting(this.w.get());
    }

    @DexIgnore
    public final CheckAuthenticationSocialExisting d() {
        return new CheckAuthenticationSocialExisting(this.w.get());
    }

    @DexIgnore
    public final ComplicationLastSettingRepository e() {
        return new ComplicationLastSettingRepository(this.u0.get());
    }

    @DexIgnore
    public final ComplicationRepository f() {
        return new ComplicationRepository(this.o0.get(), this.p0.get(), this.d.get());
    }

    @DexIgnore
    public final yr3 g() {
        yr3 a2 = zr3.a(this.d.get(), this.O.get(), this.T.get(), this.f.get(), this.M.get(), this.U.get(), S(), this.c.get());
        a(a2);
        return a2;
    }

    @DexIgnore
    public final jr3 h() {
        return new jr3(this.p.get(), this.s.get());
    }

    @DexIgnore
    public final lu3 i() {
        return new lu3(M());
    }

    @DexIgnore
    public final lr3 j() {
        return new lr3(this.c.get());
    }

    @DexIgnore
    public final DeleteLogoutUserUseCase k() {
        return new DeleteLogoutUserUseCase(this.w.get(), this.B.get(), this.c.get(), this.Z.get(), this.c0.get(), this.H.get(), this.g0.get(), this.O.get(), this.T.get(), this.h0.get(), this.k0.get(), this.l0.get(), E(), this.K.get(), this.p.get(), Y(), f(), this.f.get(), this.e.get(), this.s0.get(), N(), e(), X(), x(), this.x0.get(), this.z0.get(), this.B0.get(), this.C0.get(), this.D0.get(), this.d.get());
    }

    @DexIgnore
    public final wj2 l() {
        return new wj2(z(), A(), F(), m());
    }

    @DexIgnore
    public final DianaSyncUseCase m() {
        return new DianaSyncUseCase(this.p.get(), this.c.get(), y(), this.d.get(), this.T.get(), this.f.get(), new qx2(), W(), U(), this.E0.get(), a0(), this.B.get());
    }

    @DexIgnore
    public final DownloadUserInfoUseCase n() {
        return new DownloadUserInfoUseCase(this.w.get());
    }

    @DexIgnore
    public final nr3 o() {
        return new nr3(this.c.get());
    }

    @DexIgnore
    public final FetchActivities p() {
        return new FetchActivities(this.c0.get(), this.w.get(), x());
    }

    @DexIgnore
    public final FetchDailyGoalTrackingSummaries q() {
        return new FetchDailyGoalTrackingSummaries(this.k0.get(), this.w.get());
    }

    @DexIgnore
    public final FetchDailyHeartRateSummaries r() {
        return new FetchDailyHeartRateSummaries(this.z0.get(), this.w.get(), x());
    }

    @DexIgnore
    public final FetchGoalTrackingData s() {
        return new FetchGoalTrackingData(this.k0.get(), this.w.get());
    }

    @DexIgnore
    public final FetchHeartRateSamples t() {
        return new FetchHeartRateSamples(this.x0.get(), x(), this.w.get());
    }

    @DexIgnore
    public final FetchSleepSessions u() {
        return new FetchSleepSessions(this.h0.get(), this.w.get(), x());
    }

    @DexIgnore
    public final FetchSleepSummaries v() {
        return new FetchSleepSummaries(this.K.get(), this.w.get(), this.h0.get(), x());
    }

    @DexIgnore
    public final FetchSummaries w() {
        return new FetchSummaries(this.H.get(), x(), this.w.get(), this.c0.get());
    }

    @DexIgnore
    public final FitnessDataRepository x() {
        return new FitnessDataRepository(this.F.get(), this.n.get());
    }

    @DexIgnore
    public final wy2 y() {
        return new wy2(this.b.get());
    }

    @DexIgnore
    public final GetDianaDeviceSettingUseCase z() {
        return new GetDianaDeviceSettingUseCase(Y(), f(), this.p.get(), b(), a0(), y(), new qx2(), this.f.get(), this.c.get(), b0(), this.B.get());
    }

    @DexIgnore
    public final void a(i iVar) {
        this.b = m44.a(s42.a(iVar.a));
        this.c = m44.a(m52.a(iVar.a, this.b));
        this.d = m44.a(u42.a(iVar.a));
        this.e = m44.a(PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory.create(iVar.b, this.d));
        this.f = m44.a(PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory.create(iVar.b, this.d));
        this.g = m44.a(wp2.a(this.c, this.e, this.f));
        this.h = m44.a(PortfolioDatabaseModule_ProvideDianaCustomizeDatabaseFactory.create(iVar.b, this.d));
        this.i = m44.a(PortfolioDatabaseModule_ProvideDianaPresetDaoFactory.create(iVar.b, this.h));
        this.j = m44.a(l52.a(iVar.a));
        this.k = m44.a(v42.a(iVar.a));
        this.l = m44.a(zo2.a(this.d, this.j, this.k, this.c));
        this.m = m44.a(dp2.a(this.j, this.k, this.c));
        this.n = m44.a(r42.a(iVar.a, this.l, this.m));
        this.o = m44.a(RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory.create(iVar.c, this.n));
        this.p = m44.a(DianaPresetRepository_Factory.create(this.i, this.o));
        this.q = m44.a(PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory.create(iVar.b, this.d));
        this.r = m44.a(PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory.create(iVar.b, this.q));
        this.s = m44.a(CustomizeRealDataRepository_Factory.create(this.r));
        this.t = m44.a(w42.a(iVar.a, this.l, this.m));
        this.u = m44.a(RepositoriesModule_ProvideUserRemoteDataSourceFactory.create(iVar.c, this.n, this.k, this.t));
        this.v = m44.a(RepositoriesModule_ProvideUserLocalDataSourceFactory.create(iVar.c));
        this.w = m44.a(UserRepository_Factory.create(this.u, this.v, this.c));
        this.x = m44.a(PortfolioDatabaseModule_ProvidesAlarmDatabaseFactory.create(iVar.b, this.d));
        this.y = m44.a(PortfolioDatabaseModule_ProvidesAlarmDaoFactory.create(iVar.b, this.x));
        this.z = m44.a(PortfolioDatabaseModule_ProvideAlarmsLocalDataSourceFactory.create(iVar.b, this.y));
        this.A = m44.a(PortfolioDatabaseModule_ProvideAlarmsRemoteDataSourceFactory.create(iVar.b, this.n));
        this.B = m44.a(AlarmsRepository_Factory.create(this.z, this.A));
        this.C = m44.a(p42.a(iVar.a, this.b, this.c, this.w, this.B));
        this.D = m44.a(PortfolioDatabaseModule_ProvideFitnessDatabaseFactory.create(iVar.b, this.d));
        this.E = m44.a(PortfolioDatabaseModule_ProvideActivitySummaryDaoFactory.create(iVar.b, this.D));
        this.F = m44.a(PortfolioDatabaseModule_ProvideFitnessDataDaoFactory.create(iVar.b, this.D));
        this.G = m44.a(PortfolioDatabaseModule_ProvideFitnessHelperFactory.create(iVar.b, this.c, this.E));
        this.H = m44.a(SummariesRepository_Factory.create(this.n, this.E, this.F, this.G));
        this.I = m44.a(PortfolioDatabaseModule_ProvideSleepDatabaseFactory.create(iVar.b, this.d));
        this.J = m44.a(PortfolioDatabaseModule_ProvideSleepDaoFactory.create(iVar.b, this.I));
        this.K = m44.a(SleepSummariesRepository_Factory.create(this.J, this.n, this.F));
        this.L = m44.a(c52.a(iVar.a));
        this.M = m44.a(j42.a());
        this.N = m44.a(NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory.create(iVar.d));
        this.O = m44.a(NotificationsRepository_Factory.create(this.N));
        this.P = m44.a(PortfolioDatabaseModule_ProvideDeviceDatabaseFactory.create(iVar.b, this.d));
        this.Q = m44.a(PortfolioDatabaseModule_ProvideDeviceDaoFactory.create(iVar.b, this.P));
        this.R = m44.a(PortfolioDatabaseModule_ProvideSkuDaoFactory.create(iVar.b, this.P));
        this.S = DeviceRemoteDataSource_Factory.create(this.n);
        this.T = m44.a(DeviceRepository_Factory.create(this.Q, this.R, this.S));
        this.U = m44.a(x42.a(iVar.a));
        this.V = m44.a(o52.a(iVar.a));
        this.W = m44.a(PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory.create(iVar.b, this.d));
        this.X = m44.a(PortfolioDatabaseModule_ProvidePresetDaoFactory.create(iVar.b, this.W));
        this.Y = HybridPresetRemoteDataSource_Factory.create(this.n);
        this.Z = m44.a(HybridPresetRepository_Factory.create(this.X, this.Y));
        this.a0 = m44.a(PortfolioDatabaseModule_ProvideSampleRawDaoFactory.create(iVar.b, this.D));
        this.b0 = m44.a(PortfolioDatabaseModule_ProvideActivitySampleDaoFactory.create(iVar.b, this.D));
        this.c0 = m44.a(ActivitiesRepository_Factory.create(this.n, this.a0, this.b0, this.D, this.F, this.w, this.G));
        this.d0 = m44.a(n52.a(iVar.a, this.l, this.m));
        this.e0 = m44.a(MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory.create(iVar.e, this.d0, this.M));
        this.f0 = m44.a(MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory.create(iVar.e));
        this.g0 = m44.a(MicroAppSettingRepository_Factory.create(this.e0, this.f0, this.M));
        this.h0 = m44.a(SleepSessionsRepository_Factory.create(this.J, this.n, this.I, this.F));
        this.i0 = m44.a(PortfolioDatabaseModule_ProvideGoalTrackingDatabaseFactory.create(iVar.b, this.d));
        this.j0 = m44.a(PortfolioDatabaseModule_ProvideGoalTrackingDaoFactory.create(iVar.b, this.i0));
        this.k0 = m44.a(GoalTrackingRepository_Factory.create(this.i0, this.j0, this.w, this.c, this.n));
        this.l0 = m44.a(h52.a(iVar.a));
        this.a = iVar.a;
        this.m0 = m44.a(PortfolioDatabaseModule_ProvideWatchAppDaoFactory.create(iVar.b, this.h));
        this.n0 = m44.a(RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory.create(iVar.c, this.n));
        this.o0 = m44.a(PortfolioDatabaseModule_ProvideComplicationDaoFactory.create(iVar.b, this.h));
        this.p0 = m44.a(RepositoriesModule_ProvideComplicationRemoteDataSourceFactory.create(iVar.c, this.n));
        this.q0 = m44.a(PortfolioDatabaseModule_ProvideMicroAppDaoFactory.create(iVar.b, this.W));
        this.r0 = MicroAppRemoteDataSource_Factory.create(this.n);
        this.s0 = m44.a(MicroAppRepository_Factory.create(this.q0, this.r0, this.d));
        this.t0 = m44.a(PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory.create(iVar.b, this.W));
        this.u0 = m44.a(PortfolioDatabaseModule_ProvideComplicationSettingDaoFactory.create(iVar.b, this.h));
        this.v0 = m44.a(PortfolioDatabaseModule_ProvideWatchAppSettingDaoFactory.create(iVar.b, this.h));
        this.w0 = m44.a(PortfolioDatabaseModule_ProvideHeartRateDaoFactory.create(iVar.b, this.D));
        this.x0 = m44.a(HeartRateSampleRepository_Factory.create(this.w0, this.F, this.n));
        this.y0 = m44.a(PortfolioDatabaseModule_ProvideHeartRateDailySummaryDaoFactory.create(iVar.b, this.D));
        this.z0 = m44.a(HeartRateSummaryRepository_Factory.create(this.y0, this.F, this.n));
        this.A0 = m44.a(PortfolioDatabaseModule_ProvideWorkoutDaoFactory.create(iVar.b, this.D));
        this.B0 = m44.a(WorkoutSessionRepository_Factory.create(this.A0, this.F, this.n));
        this.C0 = m44.a(PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory.create(iVar.b, this.d));
        this.D0 = m44.a(PortfolioDatabaseModule_ProvideThirdPartyDatabaseFactory.create(iVar.b, this.d));
        this.E0 = m44.a(q42.a(iVar.a));
        this.F0 = m44.a(PortfolioDatabaseModule_ProvideCategoryDatabaseFactory.create(iVar.b, this.d));
        this.G0 = m44.a(PortfolioDatabaseModule_ProvideCategoryDaoFactory.create(iVar.b, this.F0));
        this.H0 = CategoryRemoteDataSource_Factory.create(this.n);
        this.I0 = CategoryRepository_Factory.create(this.G0, this.H0);
        this.J0 = WatchAppRepository_Factory.create(this.m0, this.n0, this.d);
        this.K0 = ComplicationRepository_Factory.create(this.o0, this.p0, this.d);
        this.L0 = m44.a(PortfolioDatabaseModule_ProvidesWatchFaceDaoFactory.create(iVar.b, this.h));
        this.M0 = WatchFaceRemoteDataSource_Factory.create(this.n);
        this.N0 = WatchFaceRepository_Factory.create(this.b, this.L0, this.M0);
        this.O0 = xy2.a(this.b);
        this.P0 = WatchLocalizationRepository_Factory.create(this.n, this.c);
        this.Q0 = rn3.a(this.J0, this.K0, this.p, this.I0, this.N0, this.O0, rx2.a(), this.f, this.c, this.P0, this.B);
        this.R0 = sn3.a(this.Z, this.s0, this.T, this.O, this.I0, this.B);
        this.S0 = sq2.a(this.s0, this.c, this.T, this.d, this.Z, this.O, this.E0, this.B);
        this.T0 = mr3.a(this.c);
        this.U0 = rr3.a(this.T, this.w, this.T0, this.d);
        this.V0 = er2.a(this.T, this.c);
    }

    @DexIgnore
    public final void b(i iVar) {
        this.W0 = rq2.a(this.p, this.c, this.O0, this.d, this.T, this.f, rx2.a(), this.U0, this.V0, this.E0, this.N0, this.B);
        this.X0 = xj2.a(this.Q0, this.R0, this.S0, this.W0);
        this.Y0 = m44.a(t42.a(iVar.a, this.d, this.c, this.Z, (Provider<CategoryRepository>) this.I0, (Provider<WatchAppRepository>) this.J0, (Provider<ComplicationRepository>) this.K0, this.s0, this.p, this.T, this.w, this.B, (Provider<wj2>) this.X0, (Provider<WatchFaceRepository>) this.N0, (Provider<WatchLocalizationRepository>) this.P0));
        this.Z0 = m44.a(jp2.a(this.w));
        this.a1 = FitnessDataRepository_Factory.create(this.F, this.n);
        this.b1 = b52.a(iVar.a, this.b, this.M, this.c);
        this.c1 = m44.a(ThirdPartyRepository_Factory.create(this.b1, this.D0, this.c0, this.d));
        this.d1 = ou3.a(this.c0, this.H, this.h0, this.K, this.k0, this.x0, this.z0, this.a1, this.B, this.c, this.p, this.Z, this.c1, this.d);
        this.e1 = m44.a(lo2.a(this.g, this.c));
        this.f1 = m44.a(no2.a(this.g, this.c));
        this.g1 = qr3.a(this.O0, rx2.a(), this.f, this.O, this.T, this.c);
        this.h1 = m44.a(f52.a(iVar.a, this.Z, (Provider<wy2>) this.O0, (Provider<qx2>) rx2.a(), this.f, this.O, this.T, this.c, this.B, (Provider<SetNotificationUseCase>) this.g1));
        this.i1 = m44.a(r52.a(iVar.a, this.T, this.d));
        this.j1 = m44.a(xp2.a());
        this.k1 = m44.a(y42.a(iVar.a));
        this.l1 = m44.a(t52.a(iVar.a));
        this.m1 = m44.a(s52.a(iVar.a));
        m44.a(UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory.create(iVar.f));
        this.n1 = MicroAppLastSettingRepository_Factory.create(this.t0);
        this.o1 = m44.a(k52.a(iVar.a, this.c, this.w, (Provider<GetHybridDeviceSettingUseCase>) this.R0, this.O, this.d, this.k0, this.i0, this.Q, this.W, (Provider<MicroAppLastSettingRepository>) this.n1, (Provider<wj2>) this.X0, this.B));
        this.p1 = m44.a(z42.a(iVar.a));
        this.q1 = m44.a(nr2.a());
        this.r1 = m44.a(LocationSource_Factory.create());
        this.s1 = m44.a(PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory.create(iVar.b, this.d));
        this.t1 = m44.a(PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory.create(iVar.b, this.s1));
        this.u1 = m44.a(InAppNotificationRepository_Factory.create(this.t1));
        this.v1 = m44.a(d52.a(iVar.a, this.u1));
        this.w1 = m44.a(g52.a(iVar.a));
        this.x1 = m44.a(a52.a(iVar.a, this.l, this.m));
        this.y1 = m44.a(q52.a(iVar.a));
        this.z1 = m44.a(i52.a(iVar.a));
        this.A1 = ComplicationLastSettingRepository_Factory.create(this.u0);
        this.B1 = WatchAppLastSettingRepository_Factory.create(this.v0);
        this.C1 = c23.a(this.p, this.K0, this.A1, this.J0, this.B1, this.N0);
        this.D1 = g63.a(this.Z, this.n1, this.s0);
        this.E1 = qr2.a(this.w);
        this.F1 = pr2.a(this.w);
        this.G1 = sh3.a(this.E1, this.F1);
        this.H1 = xq2.a(this.T, this.c);
        this.I1 = cr2.a(this.T, this.X0, this.d, this.N0);
        this.J1 = yq2.a(this.w, this.T, this.X0, this.d, this.c);
        this.K1 = yp3.a(this.T, this.H1, this.I1, this.X0, this.c, this.J1, vq2.a(), this.d);
        this.L1 = h53.a(this.c, this.w);
        this.M1 = e53.a(this.c, this.w);
        n44.b a2 = n44.a(8);
        a2.a(DianaCustomizeViewModel.class, this.C1);
        a2.a(HybridCustomizeViewModel.class, this.D1);
        a2.a(ProfileEditViewModel.class, this.G1);
        a2.a(WatchSettingViewModel.class, this.K1);
        a2.a(CommuteTimeWatchAppSettingsViewModel.class, this.L1);
        a2.a(CommuteTimeSettingsDetailViewModel.class, this.M1);
        a2.a(k73.class, l73.a());
        a2.a(ex2.class, fx2.a());
        this.N1 = a2.a();
        this.O1 = m44.a(l42.a(this.N1));
        this.P1 = m44.a(PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory.create(iVar.b, this.f));
        this.Q1 = m44.a(e52.a(iVar.a, this.c, this.w, this.X, this.O, this.Q, this.d));
        this.R1 = m44.a(j52.a(iVar.a, this.c, this.o1, this.Q1));
        this.S1 = m44.a(RepositoriesModule_ProvideServerSettingLocalDataSourceFactory.create(iVar.c));
        this.T1 = m44.a(RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory.create(iVar.c, this.n));
        this.U1 = m44.a(p52.a(iVar.a));
    }

    @DexIgnore
    public final on2 b(on2 on2) {
        pn2.a(on2, this.c.get());
        pn2.a(on2, h());
        pn2.a(on2, this.C.get());
        return on2;
    }

    @DexIgnore
    public final PortfolioApp b(PortfolioApp portfolioApp) {
        f62.a(portfolioApp, this.c.get());
        f62.a(portfolioApp, this.w.get());
        f62.a(portfolioApp, this.H.get());
        f62.a(portfolioApp, this.K.get());
        f62.a(portfolioApp, this.C.get());
        f62.a(portfolioApp, this.L.get());
        f62.a(portfolioApp, this.M.get());
        f62.a(portfolioApp, this.n.get());
        f62.a(portfolioApp, g());
        f62.a(portfolioApp, this.V.get());
        f62.a(portfolioApp, k());
        f62.a(portfolioApp, this.E0.get());
        f62.a(portfolioApp, this.Y0.get());
        f62.a(portfolioApp, this.T.get());
        f62.a(portfolioApp, this.G.get());
        f62.a(portfolioApp, this.Z0.get());
        f62.a(portfolioApp, this.p.get());
        f62.a(portfolioApp, i());
        f62.a(portfolioApp, b0());
        f62.a(portfolioApp, new hr3());
        f62.a(portfolioApp, a0());
        f62.a(portfolioApp, this.e1.get());
        f62.a(portfolioApp, this.f1.get());
        return portfolioApp;
    }

    @DexIgnore
    public final DeviceHelper b(DeviceHelper deviceHelper) {
        uk2.a(deviceHelper, this.c.get());
        uk2.a(deviceHelper, this.T.get());
        return deviceHelper;
    }

    @DexIgnore
    public final rl2 b(rl2 rl2) {
        sl2.a(rl2, this.c.get());
        return rl2;
    }

    @DexIgnore
    public final MFDeviceService b(MFDeviceService mFDeviceService) {
        hp2.a(mFDeviceService, this.c.get());
        hp2.a(mFDeviceService, this.T.get());
        hp2.a(mFDeviceService, this.c0.get());
        hp2.a(mFDeviceService, this.H.get());
        hp2.a(mFDeviceService, this.h0.get());
        hp2.a(mFDeviceService, this.K.get());
        hp2.a(mFDeviceService, this.w.get());
        hp2.a(mFDeviceService, this.Z.get());
        hp2.a(mFDeviceService, this.s0.get());
        hp2.a(mFDeviceService, this.x0.get());
        hp2.a(mFDeviceService, this.z0.get());
        hp2.a(mFDeviceService, this.B0.get());
        hp2.a(mFDeviceService, x());
        hp2.a(mFDeviceService, this.k0.get());
        hp2.a(mFDeviceService, this.M.get());
        hp2.a(mFDeviceService, this.E0.get());
        hp2.a(mFDeviceService, this.d.get());
        hp2.a(mFDeviceService, this.h1.get());
        hp2.a(mFDeviceService, this.c1.get());
        hp2.a(mFDeviceService, o());
        hp2.a(mFDeviceService, W());
        hp2.a(mFDeviceService, this.G.get());
        hp2.a(mFDeviceService, this.i1.get());
        return mFDeviceService;
    }

    @DexIgnore
    public final FossilNotificationListenerService b(FossilNotificationListenerService fossilNotificationListenerService) {
        fp2.a(fossilNotificationListenerService, this.g.get());
        fp2.a(fossilNotificationListenerService, this.j1.get());
        fp2.a(fossilNotificationListenerService, this.M.get());
        fp2.a(fossilNotificationListenerService, this.c.get());
        return fossilNotificationListenerService;
    }

    @DexIgnore
    public final AlarmReceiver b(AlarmReceiver alarmReceiver) {
        fo2.a(alarmReceiver, this.w.get());
        fo2.a(alarmReceiver, this.c.get());
        fo2.a(alarmReceiver, this.T.get());
        fo2.a(alarmReceiver, this.C.get());
        fo2.a(alarmReceiver, this.B.get());
        return alarmReceiver;
    }

    @DexIgnore
    public void a(on2 on2) {
        b(on2);
    }

    @DexIgnore
    public void a(PortfolioApp portfolioApp) {
        b(portfolioApp);
    }

    @DexIgnore
    public final NetworkChangedReceiver b(NetworkChangedReceiver networkChangedReceiver) {
        jo2.a(networkChangedReceiver, this.w.get());
        return networkChangedReceiver;
    }

    @DexIgnore
    public void a(DeviceHelper deviceHelper) {
        b(deviceHelper);
    }

    @DexIgnore
    public void a(rl2 rl2) {
        b(rl2);
    }

    @DexIgnore
    public void a(MFDeviceService mFDeviceService) {
        b(mFDeviceService);
    }

    @DexIgnore
    public final NotificationReceiver b(NotificationReceiver notificationReceiver) {
        nn2.a(notificationReceiver, this.c.get());
        nn2.a(notificationReceiver, l());
        return notificationReceiver;
    }

    @DexIgnore
    public void a(FossilNotificationListenerService fossilNotificationListenerService) {
        b(fossilNotificationListenerService);
    }

    @DexIgnore
    public void a(AlarmReceiver alarmReceiver) {
        b(alarmReceiver);
    }

    @DexIgnore
    public void a(NetworkChangedReceiver networkChangedReceiver) {
        b(networkChangedReceiver);
    }

    @DexIgnore
    public void a(NotificationReceiver notificationReceiver) {
        b(notificationReceiver);
    }

    @DexIgnore
    public void a(LoginPresenter loginPresenter) {
        b(loginPresenter);
    }

    @DexIgnore
    public final LoginPresenter b(LoginPresenter loginPresenter) {
        lk3.a(loginPresenter, G());
        lk3.a(loginPresenter, J());
        lk3.a(loginPresenter, n());
        lk3.a(loginPresenter, new uq2());
        lk3.a(loginPresenter, l());
        lk3.a(loginPresenter, this.w.get());
        lk3.a(loginPresenter, this.T.get());
        lk3.a(loginPresenter, this.c.get());
        lk3.a(loginPresenter, p());
        lk3.a(loginPresenter, w());
        lk3.a(loginPresenter, this.V.get());
        lk3.a(loginPresenter, u());
        lk3.a(loginPresenter, v());
        lk3.a(loginPresenter, t());
        lk3.a(loginPresenter, r());
        lk3.a(loginPresenter, H());
        lk3.a(loginPresenter, this.l1.get());
        lk3.a(loginPresenter, I());
        lk3.a(loginPresenter, L());
        lk3.a(loginPresenter, K());
        lk3.a(loginPresenter, d());
        lk3.a(loginPresenter, this.E0.get());
        lk3.a(loginPresenter, this.H.get());
        lk3.a(loginPresenter, this.K.get());
        lk3.a(loginPresenter, this.k0.get());
        lk3.a(loginPresenter, q());
        lk3.a(loginPresenter, s());
        lk3.a(loginPresenter, B());
        lk3.a(loginPresenter, b0());
        lk3.a(loginPresenter, this.B.get());
        lk3.a(loginPresenter);
        return loginPresenter;
    }

    @DexIgnore
    public void a(SignUpPresenter signUpPresenter) {
        b(signUpPresenter);
    }

    @DexIgnore
    public void a(ProfileSetupPresenter profileSetupPresenter) {
        b(profileSetupPresenter);
    }

    @DexIgnore
    public void a(BaseActivity baseActivity) {
        b(baseActivity);
    }

    @DexIgnore
    public void a(DebugActivity debugActivity) {
        b(debugActivity);
    }

    @DexIgnore
    public void a(CommuteTimeService commuteTimeService) {
        b(commuteTimeService);
    }

    @DexIgnore
    public void a(BootReceiver bootReceiver) {
        b(bootReceiver);
    }

    @DexIgnore
    public void a(URLRequestTaskHelper uRLRequestTaskHelper) {
        b(uRLRequestTaskHelper);
    }

    @DexIgnore
    public void a(ComplicationWeatherService complicationWeatherService) {
        b(complicationWeatherService);
    }

    @DexIgnore
    public void a(DeviceUtils deviceUtils) {
        b(deviceUtils);
    }

    @DexIgnore
    public void a(or3 or3) {
        b(or3);
    }

    @DexIgnore
    public void a(CloudImageHelper cloudImageHelper) {
        b(cloudImageHelper);
    }

    @DexIgnore
    public void a(UserUtils userUtils) {
        b(userUtils);
    }

    @DexIgnore
    public void a(FossilFirebaseInstanceIDService fossilFirebaseInstanceIDService) {
        b(fossilFirebaseInstanceIDService);
    }

    @DexIgnore
    public void a(FossilFirebaseMessagingService fossilFirebaseMessagingService) {
        b(fossilFirebaseMessagingService);
    }

    @DexIgnore
    public void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager) {
        b(watchAppCommuteTimeManager);
    }

    @DexIgnore
    public void a(AppHelper appHelper) {
        b(appHelper);
    }

    @DexIgnore
    public void a(AppPackageRemoveReceiver appPackageRemoveReceiver) {
        b(appPackageRemoveReceiver);
    }

    @DexIgnore
    public void a(LightAndHapticsManager lightAndHapticsManager) {
        b(lightAndHapticsManager);
    }

    @DexIgnore
    public void a(WeatherManager weatherManager) {
        b(weatherManager);
    }

    @DexIgnore
    public void a(TimeChangeReceiver timeChangeReceiver) {
        b(timeChangeReceiver);
    }

    @DexIgnore
    public aq3 a(dq3 dq3) {
        return new j(dq3);
    }

    @DexIgnore
    public kq3 a(nq3 nq3) {
        return new b0(nq3);
    }

    @DexIgnore
    public ll3 a(ol3 ol3) {
        return new t1(ol3);
    }

    @DexIgnore
    public s13 a(x13 x13) {
        return new y(x13);
    }

    @DexIgnore
    public a43 a(e43 e43) {
        return new q(e43);
    }

    @DexIgnore
    public r53 a(v53 v53) {
        return new u1(v53);
    }

    @DexIgnore
    public ke3 a(oe3 oe3) {
        return new e(oe3);
    }

    @DexIgnore
    public ue3 a(ye3 ye3) {
        return new k(ye3);
    }

    @DexIgnore
    public be3 a(fe3 fe3) {
        return new c(fe3);
    }

    @DexIgnore
    public vf3 a(zf3 zf3) {
        return new p1(zf3);
    }

    @DexIgnore
    public df3 a(hf3 hf3) {
        return new d0(hf3);
    }

    @DexIgnore
    public mf3 a(qf3 qf3) {
        return new f0(qf3);
    }

    @DexIgnore
    public bw2 a(fw2 fw2) {
        return new p0(fw2);
    }

    @DexIgnore
    public lw2 a(qw2 qw2) {
        return new q0(qw2);
    }

    @DexIgnore
    public gx2 a(lx2 lx2) {
        return new x0(lx2);
    }

    @DexIgnore
    public vw2 a(zw2 zw2) {
        return new s0(zw2);
    }

    @DexIgnore
    public rv2 a(wv2 wv2) {
        return new i0(wv2);
    }

    @DexIgnore
    public ay2 a(fy2 fy2) {
        return new y0(fy2);
    }

    @DexIgnore
    public oy2 a(py2 py2) {
        return new j1(py2);
    }

    @DexIgnore
    public e93 a(l93 l93) {
        return new f(l93);
    }

    @DexIgnore
    public ed3 a(ld3 ld3) {
        return new q1(ld3);
    }

    @DexIgnore
    public eb3 a(lb3 lb3) {
        return new e0(lb3);
    }

    @DexIgnore
    public ea3 a(la3 la3) {
        return new l(la3);
    }

    @DexIgnore
    public e83 a(l83 l83) {
        return new d(l83);
    }

    @DexIgnore
    public ec3 a(lc3 lc3) {
        return new g0(lc3);
    }

    @DexIgnore
    public vn3 a(zn3 zn3) {
        return new c1(zn3);
    }

    @DexIgnore
    public uh3 a(zh3 zh3) {
        return new g1(zh3);
    }

    @DexIgnore
    public mh3 a(qh3 qh3) {
        return new f1(qh3);
    }

    @DexIgnore
    public f53 a(g53 g53) {
        return new p(g53);
    }

    @DexIgnore
    public c53 a(d53 d53) {
        return new o(d53);
    }

    @DexIgnore
    public qj3 a(tj3 tj3) {
        return new d1(tj3);
    }

    @DexIgnore
    public gi3 a(ki3 ki3) {
        return new h0(ki3);
    }

    @DexIgnore
    public dh3 a(hh3 hh3) {
        return new k1(hh3);
    }

    @DexIgnore
    public tg3 a(xg3 xg3) {
        return new b(xg3);
    }

    @DexIgnore
    public pi3 a(si3 si3) {
        return new x(si3);
    }

    @DexIgnore
    public xi3 a(bj3 bj3) {
        return new h1(bj3);
    }

    @DexIgnore
    public final SignUpPresenter b(SignUpPresenter signUpPresenter) {
        oo3.a(signUpPresenter, H());
        oo3.a(signUpPresenter, this.l1.get());
        oo3.a(signUpPresenter, I());
        oo3.a(signUpPresenter, L());
        oo3.a(signUpPresenter, K());
        oo3.a(signUpPresenter, J());
        oo3.a(signUpPresenter, this.w.get());
        oo3.a(signUpPresenter, this.T.get());
        oo3.a(signUpPresenter, this.V.get());
        oo3.a(signUpPresenter, u());
        oo3.a(signUpPresenter, v());
        oo3.a(signUpPresenter, p());
        oo3.a(signUpPresenter, w());
        oo3.a(signUpPresenter, t());
        oo3.a(signUpPresenter, r());
        oo3.a(signUpPresenter, this.B.get());
        oo3.a(signUpPresenter, new uq2());
        oo3.a(signUpPresenter, l());
        oo3.a(signUpPresenter, n());
        oo3.a(signUpPresenter, this.c.get());
        oo3.a(signUpPresenter, c());
        oo3.a(signUpPresenter, d());
        oo3.a(signUpPresenter, this.E0.get());
        oo3.a(signUpPresenter, C());
        oo3.a(signUpPresenter, this.H.get());
        oo3.a(signUpPresenter, this.K.get());
        oo3.a(signUpPresenter, this.k0.get());
        oo3.a(signUpPresenter, q());
        oo3.a(signUpPresenter, s());
        oo3.a(signUpPresenter, O());
        oo3.a(signUpPresenter, B());
        oo3.a(signUpPresenter, b0());
        oo3.a(signUpPresenter);
        return signUpPresenter;
    }

    @DexIgnore
    public uq3 a(zq3 zq3) {
        return new y1(zq3);
    }

    @DexIgnore
    public pz2 a(tz2 tz2) {
        return new t0(tz2);
    }

    @DexIgnore
    public fz2 a(jz2 jz2) {
        return new r0(jz2);
    }

    @DexIgnore
    public k03 a(o03 o03) {
        return new v0(o03);
    }

    @DexIgnore
    public zz2 a(d03 d03) {
        return new u0(d03);
    }

    @DexIgnore
    public w03 a(a13 a13) {
        return new w0(a13);
    }

    @DexIgnore
    public i73 a(j73 j73) {
        return new v(j73);
    }

    @DexIgnore
    public rm3 a(um3 um3) {
        return new b1(um3);
    }

    @DexIgnore
    public bn3 a(in3 in3) {
        return new a1(in3);
    }

    @DexIgnore
    public zo3 a(cp3 cp3) {
        return new r1(cp3);
    }

    @DexIgnore
    public gv2 a(hv2 hv2) {
        return new k0(hv2);
    }

    @DexIgnore
    public qu2 a(bv2 bv2) {
        return new j0(bv2);
    }

    @DexIgnore
    public n73 a(o73 o73) {
        return new w(o73);
    }

    @DexIgnore
    public j13 a(k13 k13) {
        return new r(k13);
    }

    @DexIgnore
    public h23 a(m23 m23) {
        return new s(m23);
    }

    @DexIgnore
    public p43 a(u43 u43) {
        return new v1(u43);
    }

    @DexIgnore
    public i43 a(m43 m43) {
        return new u(m43);
    }

    @DexIgnore
    public t23 a(e33 e33) {
        return new m(e33);
    }

    @DexIgnore
    public x23 a(a33 a33) {
        return new n(a33);
    }

    @DexIgnore
    public s33 a(v33 v33) {
        return new n1(v33);
    }

    @DexIgnore
    public j53 a(m53 m53) {
        return new x1(m53);
    }

    @DexIgnore
    public xt2 a(bu2 bu2) {
        return new g(bu2);
    }

    @DexIgnore
    public ip3 a(mp3 mp3) {
        return new s1(mp3);
    }

    @DexIgnore
    public go3 a(ko3 ko3) {
        return new o1(ko3);
    }

    @DexIgnore
    public dk3 a(hk3 hk3) {
        return new n0(hk3);
    }

    @DexIgnore
    public vk3 a(yk3 yk3) {
        return new c0(yk3);
    }

    @DexIgnore
    public el3 a(hl3 hl3) {
        return new z0(hl3);
    }

    @DexIgnore
    public ul3 a(xl3 xl3) {
        return new i1(xl3);
    }

    @DexIgnore
    public bm3 a(em3 em3) {
        return new h(em3);
    }

    @DexIgnore
    public nk3 a(qk3 qk3) {
        return new a0(qk3);
    }

    @DexIgnore
    public tp3 a(wp3 wp3) {
        return new w1(wp3);
    }

    @DexIgnore
    public iu2 a(lu2 lu2) {
        return new t(lu2);
    }

    @DexIgnore
    public gj3 a(kj3 kj3) {
        return new e1(kj3);
    }

    @DexIgnore
    public k63 a(p63 p63) {
        return new l0(p63);
    }

    @DexIgnore
    public t63 a(w63 w63) {
        return new o0(w63);
    }

    @DexIgnore
    public d63 a(e63 e63) {
        return new m0(e63);
    }

    @DexIgnore
    public a73 a(e73 e73) {
        return new l1(e73);
    }

    @DexIgnore
    public j33 a(n33 n33) {
        return new m1(n33);
    }

    @DexIgnore
    public wo3 a(xo3 xo3) {
        return new z(xo3);
    }

    @DexIgnore
    public final yr3 a(yr3 yr3) {
        as3.a(yr3, this.T.get());
        as3.a(yr3, this.f.get());
        return yr3;
    }

    @DexIgnore
    public final ProfileSetupPresenter b(ProfileSetupPresenter profileSetupPresenter) {
        am3.a(profileSetupPresenter, Q());
        am3.a(profileSetupPresenter, R());
        am3.a(profileSetupPresenter, C());
        am3.a(profileSetupPresenter, this.E0.get());
        am3.a(profileSetupPresenter);
        return profileSetupPresenter;
    }

    @DexIgnore
    public final BaseActivity b(BaseActivity baseActivity) {
        eq2.a(baseActivity, this.w.get());
        eq2.a(baseActivity, this.c.get());
        eq2.a(baseActivity, this.T.get());
        eq2.a(baseActivity, this.o1.get());
        eq2.a(baseActivity, new hr2());
        return baseActivity;
    }

    @DexIgnore
    public final DebugActivity b(DebugActivity debugActivity) {
        eq2.a((BaseActivity) debugActivity, this.w.get());
        eq2.a((BaseActivity) debugActivity, this.c.get());
        eq2.a((BaseActivity) debugActivity, this.T.get());
        eq2.a((BaseActivity) debugActivity, this.o1.get());
        eq2.a((BaseActivity) debugActivity, new hr2());
        jq2.a(debugActivity, this.c.get());
        jq2.a(debugActivity, T());
        jq2.a(debugActivity, this.p1.get());
        jq2.a(debugActivity, this.L.get());
        jq2.a(debugActivity, this.p.get());
        jq2.a(debugActivity, this.Z0.get());
        jq2.a(debugActivity, l());
        return debugActivity;
    }

    @DexIgnore
    public final CommuteTimeService b(CommuteTimeService commuteTimeService) {
        rp2.a(commuteTimeService, this.q1.get());
        return commuteTimeService;
    }

    @DexIgnore
    public final BootReceiver b(BootReceiver bootReceiver) {
        io2.a(bootReceiver, this.C.get());
        return bootReceiver;
    }

    @DexIgnore
    public final URLRequestTaskHelper b(URLRequestTaskHelper uRLRequestTaskHelper) {
        URLRequestTaskHelper_MembersInjector.injectMApiService(uRLRequestTaskHelper, this.n.get());
        return uRLRequestTaskHelper;
    }

    @DexIgnore
    public final ComplicationWeatherService b(ComplicationWeatherService complicationWeatherService) {
        lp2.a(complicationWeatherService, this.r1.get());
        np2.a(complicationWeatherService, D());
        np2.a(complicationWeatherService, this.V.get());
        np2.a(complicationWeatherService, this.c.get());
        np2.a(complicationWeatherService, this.d.get());
        np2.a(complicationWeatherService, this.s.get());
        np2.a(complicationWeatherService, this.w.get());
        return complicationWeatherService;
    }

    @DexIgnore
    public final DeviceUtils b(DeviceUtils deviceUtils) {
        cs3.a(deviceUtils, this.T.get());
        cs3.a(deviceUtils, this.c.get());
        cs3.a(deviceUtils, this.L.get());
        cs3.a(deviceUtils, this.p1.get());
        return deviceUtils;
    }

    @DexIgnore
    public final or3 b(or3 or3) {
        pr3.a(or3, this.c0.get());
        return or3;
    }

    @DexIgnore
    public final CloudImageHelper b(CloudImageHelper cloudImageHelper) {
        CloudImageHelper_MembersInjector.injectMAppExecutors(cloudImageHelper, this.M.get());
        CloudImageHelper_MembersInjector.injectMApp(cloudImageHelper, this.d.get());
        return cloudImageHelper;
    }

    @DexIgnore
    public final UserUtils b(UserUtils userUtils) {
        ts3.a(userUtils, this.c.get());
        return userUtils;
    }

    @DexIgnore
    public final FossilFirebaseInstanceIDService b(FossilFirebaseInstanceIDService fossilFirebaseInstanceIDService) {
        op2.a(fossilFirebaseInstanceIDService, this.c.get());
        return fossilFirebaseInstanceIDService;
    }

    @DexIgnore
    public final FossilFirebaseMessagingService b(FossilFirebaseMessagingService fossilFirebaseMessagingService) {
        pp2.a(fossilFirebaseMessagingService, this.v1.get());
        return fossilFirebaseMessagingService;
    }

    @DexIgnore
    public final WatchAppCommuteTimeManager b(WatchAppCommuteTimeManager watchAppCommuteTimeManager) {
        zp2.a(watchAppCommuteTimeManager, this.d.get());
        zp2.a(watchAppCommuteTimeManager, this.n.get());
        zp2.a(watchAppCommuteTimeManager, this.r1.get());
        zp2.a(watchAppCommuteTimeManager, this.w.get());
        zp2.a(watchAppCommuteTimeManager, this.c.get());
        zp2.a(watchAppCommuteTimeManager, this.w1.get());
        zp2.a(watchAppCommuteTimeManager, this.q1.get());
        zp2.a(watchAppCommuteTimeManager, this.p.get());
        return watchAppCommuteTimeManager;
    }

    @DexIgnore
    public final AppHelper b(AppHelper appHelper) {
        lk2.a(appHelper, this.c.get());
        lk2.a(appHelper, this.T.get());
        lk2.a(appHelper, this.w.get());
        return appHelper;
    }

    @DexIgnore
    public final AppPackageRemoveReceiver b(AppPackageRemoveReceiver appPackageRemoveReceiver) {
        go2.a(appPackageRemoveReceiver, this.O.get());
        return appPackageRemoveReceiver;
    }

    @DexIgnore
    public final LightAndHapticsManager b(LightAndHapticsManager lightAndHapticsManager) {
        an2.a(lightAndHapticsManager, this.T.get());
        an2.a(lightAndHapticsManager, this.c.get());
        return lightAndHapticsManager;
    }

    @DexIgnore
    public final WeatherManager b(WeatherManager weatherManager) {
        in2.a(weatherManager, this.d.get());
        in2.a(weatherManager, this.n.get());
        in2.a(weatherManager, this.r1.get());
        in2.a(weatherManager, this.w.get());
        in2.a(weatherManager, this.s.get());
        in2.a(weatherManager, this.p.get());
        in2.a(weatherManager, this.x1.get());
        return weatherManager;
    }

    @DexIgnore
    public final TimeChangeReceiver b(TimeChangeReceiver timeChangeReceiver) {
        pu3.a(timeChangeReceiver, this.C.get());
        pu3.a(timeChangeReceiver, this.c.get());
        return timeChangeReceiver;
    }
}
