package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.he0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jc1 extends pj0<gc1> {
    @DexIgnore
    public jc1(Context context, Looper looper, lj0 lj0, he0.b bVar, he0.c cVar) {
        super(context, looper, 51, lj0, bVar, cVar);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.phenotype.internal.IPhenotypeService");
        return queryLocalInterface instanceof gc1 ? (gc1) queryLocalInterface : new hc1(iBinder);
    }

    @DexIgnore
    public final int i() {
        return 11925000;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.phenotype.internal.IPhenotypeService";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.phenotype.service.START";
    }
}
