package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class tb2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ TextInputEditText s;
    @DexIgnore
    public /* final */ FlexibleButton t;
    @DexIgnore
    public /* final */ FlexibleButton u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ TextInputLayout x;
    @DexIgnore
    public /* final */ ImageView y;
    @DexIgnore
    public /* final */ ImageView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public tb2(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, View view2, TextInputEditText textInputEditText, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, TextInputLayout textInputLayout, ImageView imageView, ImageView imageView2, ImageView imageView3, ScrollView scrollView, FlexibleTextView flexibleTextView6) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = textInputEditText;
        this.t = flexibleButton;
        this.u = flexibleButton2;
        this.v = flexibleTextView2;
        this.w = flexibleTextView4;
        this.x = textInputLayout;
        this.y = imageView;
        this.z = imageView2;
    }
}
