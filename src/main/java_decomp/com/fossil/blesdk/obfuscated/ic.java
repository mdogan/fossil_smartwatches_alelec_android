package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ic {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements dc<X> {
        @DexIgnore
        public /* final */ /* synthetic */ bc a;
        @DexIgnore
        public /* final */ /* synthetic */ m3 b;

        @DexIgnore
        public a(bc bcVar, m3 m3Var) {
            this.a = bcVar;
            this.b = m3Var;
        }

        @DexIgnore
        public void a(X x) {
            this.a.b(this.b.apply(x));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements dc<X> {
        @DexIgnore
        public LiveData<Y> a;
        @DexIgnore
        public /* final */ /* synthetic */ m3 b;
        @DexIgnore
        public /* final */ /* synthetic */ bc c;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements dc<Y> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void a(Y y) {
                b.this.c.b(y);
            }
        }

        @DexIgnore
        public b(m3 m3Var, bc bcVar) {
            this.b = m3Var;
            this.c = bcVar;
        }

        @DexIgnore
        public void a(X x) {
            LiveData<Y> liveData = (LiveData) this.b.apply(x);
            LiveData<Y> liveData2 = this.a;
            if (liveData2 != liveData) {
                if (liveData2 != null) {
                    this.c.a(liveData2);
                }
                this.a = liveData;
                LiveData<Y> liveData3 = this.a;
                if (liveData3 != null) {
                    this.c.a(liveData3, new a());
                }
            }
        }
    }

    @DexIgnore
    public static <X, Y> LiveData<Y> a(LiveData<X> liveData, m3<X, Y> m3Var) {
        bc bcVar = new bc();
        bcVar.a(liveData, new a(bcVar, m3Var));
        return bcVar;
    }

    @DexIgnore
    public static <X, Y> LiveData<Y> b(LiveData<X> liveData, m3<X, LiveData<Y>> m3Var) {
        bc bcVar = new bc();
        bcVar.a(liveData, new b(m3Var, bcVar));
        return bcVar;
    }
}
