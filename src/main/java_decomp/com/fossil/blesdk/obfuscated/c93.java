package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c93 implements Factory<DashboardActivityPresenter> {
    @DexIgnore
    public static DashboardActivityPresenter a(a93 a93, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, i42 i42, yk2 yk2) {
        return new DashboardActivityPresenter(a93, summariesRepository, fitnessDataRepository, activitySummaryDao, fitnessDatabase, userRepository, i42, yk2);
    }
}
