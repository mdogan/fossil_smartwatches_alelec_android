package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bo3 implements Factory<xn3> {
    @DexIgnore
    public static xn3 a(zn3 zn3) {
        xn3 b = zn3.b();
        o44.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
