package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ee0;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ch0 {
    @DexIgnore
    <A extends ee0.b, T extends ue0<? extends ne0, A>> T a(T t);

    @DexIgnore
    void a();

    @DexIgnore
    void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @DexIgnore
    boolean a(df0 df0);

    @DexIgnore
    <A extends ee0.b, R extends ne0, T extends ue0<R, A>> T b(T t);

    @DexIgnore
    void b();

    @DexIgnore
    boolean c();

    @DexIgnore
    void d();

    @DexIgnore
    void e();

    @DexIgnore
    vd0 f();
}
