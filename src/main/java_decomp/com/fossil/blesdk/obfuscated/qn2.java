package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.fossil.wearables.fsl.utils.TimeUtils;
import com.misfit.frameworks.common.log.MFLogger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qn2 extends BroadcastReceiver {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        MFLogger.d("TimeTickReceiver", "onReceive - action=" + action);
        if (!TextUtils.isEmpty(action) && wd4.a((Object) action, (Object) "android.intent.action.TIME_TICK")) {
            Calendar instance = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TimeUtils.SIMPLE_FORMAT_YYYY_MM_DD, Locale.US);
            wd4.a((Object) instance, "calendar");
            String format = simpleDateFormat.format(instance.getTime());
            fn2 fn2 = new fn2(context);
            String z = fn2.z();
            MFLogger.d("TimeTickReceiver", "onReceive - day= " + format + ", widgetsDateChanged= " + z);
            if (!cg4.b(z, format, false, 2, (Object) null) && context != null) {
                MFLogger.d("TimeTickReceiver", "onReceive - need to resetAllContentWidgetsUI");
                fn2.x(format);
            }
        }
    }
}
