package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ProgressBar;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.indicator.CirclePageIndicator;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class hd2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ CirclePageIndicator s;
    @DexIgnore
    public /* final */ ProgressBar t;
    @DexIgnore
    public /* final */ RecyclerViewPager u;

    @DexIgnore
    public hd2(Object obj, View view, int i, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, CirclePageIndicator circlePageIndicator, ProgressBar progressBar, RecyclerViewPager recyclerViewPager) {
        super(obj, view, i);
        this.q = flexibleTextView2;
        this.r = flexibleTextView3;
        this.s = circlePageIndicator;
        this.t = progressBar;
        this.u = recyclerViewPager;
    }
}
