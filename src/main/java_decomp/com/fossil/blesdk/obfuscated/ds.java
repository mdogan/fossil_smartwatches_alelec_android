package com.fossil.blesdk.obfuscated;

import android.net.Uri;
import com.facebook.internal.Utility;
import com.fossil.blesdk.obfuscated.tr;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ds<Data> implements tr<Uri, Data> {
    @DexIgnore
    public static /* final */ Set<String> b; // = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"http", Utility.URL_SCHEME})));
    @DexIgnore
    public /* final */ tr<mr, Data> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ur<Uri, InputStream> {
        @DexIgnore
        public tr<Uri, InputStream> a(xr xrVar) {
            return new ds(xrVar.a(mr.class, InputStream.class));
        }
    }

    @DexIgnore
    public ds(tr<mr, Data> trVar) {
        this.a = trVar;
    }

    @DexIgnore
    public tr.a<Data> a(Uri uri, int i, int i2, mo moVar) {
        return this.a.a(new mr(uri.toString()), i, i2, moVar);
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
