package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gu0 {
    @DexIgnore
    public static /* final */ gu0 a; // = new gu0(true);

    /*
    static {
        a();
    }
    */

    @DexIgnore
    public gu0() {
        new HashMap();
    }

    @DexIgnore
    public gu0(boolean z) {
        Collections.emptyMap();
    }

    @DexIgnore
    public static Class<?> a() {
        try {
            return Class.forName("com.google.protobuf.Extension");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public static gu0 b() {
        return fu0.b();
    }
}
