package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a70 extends h70 {
    @DexIgnore
    public long A; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public Peripheral.BondState B; // = Peripheral.BondState.BOND_NONE;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a70(Peripheral peripheral) {
        super(RequestId.REMOVE_BOND, peripheral);
        wd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new l10(i().h());
    }

    @DexIgnore
    public void a(long j) {
        this.A = j;
    }

    @DexIgnore
    public long m() {
        return this.A;
    }

    @DexIgnore
    public JSONObject t() {
        return xa0.a(super.t(), JSONKey.CURRENT_BOND_STATE, i().getBondState().getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.NEW_BOND_STATE, this.B.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public void a(BluetoothCommand bluetoothCommand) {
        wd4.b(bluetoothCommand, Constants.COMMAND);
        this.B = ((l10) bluetoothCommand).i();
        a(new Request.ResponseInfo(0, (GattCharacteristic.CharacteristicId) null, (byte[]) null, xa0.a(new JSONObject(), JSONKey.NEW_BOND_STATE, this.B.getLogName$blesdk_productionRelease()), 7, (rd4) null));
    }
}
