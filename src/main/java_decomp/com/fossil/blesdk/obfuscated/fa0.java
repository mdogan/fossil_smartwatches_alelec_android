package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fa0 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public int d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;

    @DexIgnore
    public fa0(String str, String str2, String str3, int i, String str4, String str5) {
        wd4.b(str, "osVersion");
        wd4.b(str2, "phoneModel");
        wd4.b(str3, ButtonService.USER_ID);
        wd4.b(str4, "sdkVersion");
        wd4.b(str5, "osName");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = i;
        this.e = str4;
        this.f = str5;
    }

    @DexIgnore
    public final void a(String str) {
        wd4.b(str, "<set-?>");
        this.c = str;
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final String d() {
        return this.e;
    }

    @DexIgnore
    public final int e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof fa0) {
                fa0 fa0 = (fa0) obj;
                if (wd4.a((Object) this.a, (Object) fa0.a) && wd4.a((Object) this.b, (Object) fa0.b) && wd4.a((Object) this.c, (Object) fa0.c)) {
                    if (!(this.d == fa0.d) || !wd4.a((Object) this.e, (Object) fa0.e) || !wd4.a((Object) this.f, (Object) fa0.f)) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (((hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31) + this.d) * 31;
        String str4 = this.e;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.f;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public String toString() {
        return "SystemInformation(osVersion=" + this.a + ", phoneModel=" + this.b + ", userId=" + this.c + ", timezoneOffset=" + this.d + ", sdkVersion=" + this.e + ", osName=" + this.f + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ fa0(String str, String str2, String str3, int i, String str4, String str5, int i2, rd4 rd4) {
        this(str, str2, str3, i, str4, (i2 & 32) != 0 ? "android" : str5);
    }

    @DexIgnore
    public final String a() {
        return this.f;
    }
}
