package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zztv;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qa1 {
    @DexIgnore
    public static /* final */ Class<?> a; // = d();
    @DexIgnore
    public static /* final */ fb1<?, ?> b; // = a(false);
    @DexIgnore
    public static /* final */ fb1<?, ?> c; // = a(true);
    @DexIgnore
    public static /* final */ fb1<?, ?> d; // = new hb1();

    @DexIgnore
    public static void a(Class<?> cls) {
        if (!u81.class.isAssignableFrom(cls)) {
            Class<?> cls2 = a;
            if (cls2 != null && !cls2.isAssignableFrom(cls)) {
                throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
            }
        }
    }

    @DexIgnore
    public static void b(int i, List<Float> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zzf(i, list, z);
        }
    }

    @DexIgnore
    public static void c(int i, List<Long> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zzc(i, list, z);
        }
    }

    @DexIgnore
    public static void d(int i, List<Long> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zzd(i, list, z);
        }
    }

    @DexIgnore
    public static void e(int i, List<Long> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zzn(i, list, z);
        }
    }

    @DexIgnore
    public static void f(int i, List<Long> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zze(i, list, z);
        }
    }

    @DexIgnore
    public static void g(int i, List<Long> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zzl(i, list, z);
        }
    }

    @DexIgnore
    public static void h(int i, List<Integer> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zza(i, list, z);
        }
    }

    @DexIgnore
    public static void i(int i, List<Integer> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zzj(i, list, z);
        }
    }

    @DexIgnore
    public static void j(int i, List<Integer> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zzm(i, list, z);
        }
    }

    @DexIgnore
    public static void k(int i, List<Integer> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zzb(i, list, z);
        }
    }

    @DexIgnore
    public static void l(int i, List<Integer> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zzk(i, list, z);
        }
    }

    @DexIgnore
    public static void m(int i, List<Integer> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zzh(i, list, z);
        }
    }

    @DexIgnore
    public static void n(int i, List<Boolean> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zzi(i, list, z);
        }
    }

    @DexIgnore
    public static void b(int i, List<zzte> list, tb1 tb1) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zzb(i, list);
        }
    }

    @DexIgnore
    public static int c(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return a(list) + (size * zztv.e(i));
    }

    @DexIgnore
    public static int d(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return b(list) + (size * zztv.e(i));
    }

    @DexIgnore
    public static int e(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return c(list) + (size * zztv.e(i));
    }

    @DexIgnore
    public static int f(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return d(list) + (size * zztv.e(i));
    }

    @DexIgnore
    public static int g(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return e(list) + (size * zztv.e(i));
    }

    @DexIgnore
    public static int h(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zztv.i(i, 0);
    }

    @DexIgnore
    public static int i(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof n91) {
            n91 n91 = (n91) list;
            i = 0;
            while (i2 < size) {
                i += zztv.d(n91.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.d(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int j(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof n91) {
            n91 n91 = (n91) list;
            i = 0;
            while (i2 < size) {
                i += zztv.e(n91.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.e(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static void a(int i, List<Double> list, tb1 tb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zzg(i, list, z);
        }
    }

    @DexIgnore
    public static void b(int i, List<?> list, tb1 tb1, oa1 oa1) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.a(i, list, oa1);
        }
    }

    @DexIgnore
    public static int h(List<?> list) {
        return list.size();
    }

    @DexIgnore
    public static void a(int i, List<String> list, tb1 tb1) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.zza(i, list);
        }
    }

    @DexIgnore
    public static int c(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof v81) {
            v81 v81 = (v81) list;
            i = 0;
            while (i2 < size) {
                i += zztv.f(v81.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.f(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int d(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof v81) {
            v81 v81 = (v81) list;
            i = 0;
            while (i2 < size) {
                i += zztv.g(v81.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.g(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int e(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof v81) {
            v81 v81 = (v81) list;
            i = 0;
            while (i2 < size) {
                i += zztv.h(v81.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.h(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int f(List<?> list) {
        return list.size() << 2;
    }

    @DexIgnore
    public static int g(List<?> list) {
        return list.size() << 3;
    }

    @DexIgnore
    public static int b(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return j(list) + (size * zztv.e(i));
    }

    @DexIgnore
    public static void a(int i, List<?> list, tb1 tb1, oa1 oa1) throws IOException {
        if (list != null && !list.isEmpty()) {
            tb1.b(i, list, oa1);
        }
    }

    @DexIgnore
    public static int i(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zztv.g(i, 0);
    }

    @DexIgnore
    public static int j(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zztv.b(i, true);
    }

    @DexIgnore
    public static int a(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return i(list) + (list.size() * zztv.e(i));
    }

    @DexIgnore
    public static int b(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof v81) {
            v81 v81 = (v81) list;
            i = 0;
            while (i2 < size) {
                i += zztv.k(v81.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.k(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static fb1<?, ?> c() {
        return d;
    }

    @DexIgnore
    public static Class<?> d() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static Class<?> e() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static int a(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof n91) {
            n91 n91 = (n91) list;
            i = 0;
            while (i2 < size) {
                i += zztv.f(n91.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.f(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int b(int i, List<zzte> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = size * zztv.e(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            e += zztv.a(list.get(i2));
        }
        return e;
    }

    @DexIgnore
    public static int a(int i, List<?> list) {
        int i2;
        int i3;
        int size = list.size();
        int i4 = 0;
        if (size == 0) {
            return 0;
        }
        int e = zztv.e(i) * size;
        if (list instanceof i91) {
            i91 i91 = (i91) list;
            while (i4 < size) {
                Object d2 = i91.d(i4);
                if (d2 instanceof zzte) {
                    i3 = zztv.a((zzte) d2);
                } else {
                    i3 = zztv.a((String) d2);
                }
                e += i3;
                i4++;
            }
        } else {
            while (i4 < size) {
                Object obj = list.get(i4);
                if (obj instanceof zzte) {
                    i2 = zztv.a((zzte) obj);
                } else {
                    i2 = zztv.a((String) obj);
                }
                e += i2;
                i4++;
            }
        }
        return e;
    }

    @DexIgnore
    public static int b(int i, List<x91> list, oa1 oa1) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += zztv.c(i, list.get(i3), oa1);
        }
        return i2;
    }

    @DexIgnore
    public static fb1<?, ?> b() {
        return c;
    }

    @DexIgnore
    public static int a(int i, Object obj, oa1 oa1) {
        if (obj instanceof g91) {
            return zztv.a(i, (g91) obj);
        }
        return zztv.b(i, (x91) obj, oa1);
    }

    @DexIgnore
    public static int a(int i, List<?> list, oa1 oa1) {
        int i2;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = zztv.e(i) * size;
        for (int i3 = 0; i3 < size; i3++) {
            Object obj = list.get(i3);
            if (obj instanceof g91) {
                i2 = zztv.a((g91) obj);
            } else {
                i2 = zztv.a((x91) obj, oa1);
            }
            e += i2;
        }
        return e;
    }

    @DexIgnore
    public static fb1<?, ?> a() {
        return b;
    }

    @DexIgnore
    public static fb1<?, ?> a(boolean z) {
        try {
            Class<?> e = e();
            if (e == null) {
                return null;
            }
            return (fb1) e.getConstructor(new Class[]{Boolean.TYPE}).newInstance(new Object[]{Boolean.valueOf(z)});
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    @DexIgnore
    public static <T> void a(s91 s91, T t, T t2, long j) {
        lb1.a((Object) t, j, s91.b(lb1.f(t, j), lb1.f(t2, j)));
    }

    @DexIgnore
    public static <T, FT extends p81<FT>> void a(k81<FT> k81, T t, T t2) {
        n81<FT> a2 = k81.a((Object) t2);
        if (!a2.b()) {
            k81.b(t).a(a2);
        }
    }

    @DexIgnore
    public static <T, UT, UB> void a(fb1<UT, UB> fb1, T t, T t2) {
        fb1.a((Object) t, fb1.c(fb1.c(t), fb1.c(t2)));
    }

    @DexIgnore
    public static <UT, UB> UB a(int i, List<Integer> list, z81 z81, UB ub, fb1<UT, UB> fb1) {
        UB ub2;
        if (z81 == null) {
            return ub;
        }
        if (!(list instanceof RandomAccess)) {
            Iterator<Integer> it = list.iterator();
            loop1:
            while (true) {
                ub2 = ub;
                while (it.hasNext()) {
                    int intValue = it.next().intValue();
                    if (!z81.zzb(intValue)) {
                        ub = a(i, intValue, ub2, fb1);
                        it.remove();
                    }
                }
                break loop1;
            }
        } else {
            int size = list.size();
            ub2 = ub;
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue2 = list.get(i3).intValue();
                if (z81.zzb(intValue2)) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue2));
                    }
                    i2++;
                } else {
                    ub2 = a(i, intValue2, ub2, fb1);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
            }
        }
        return ub2;
    }

    @DexIgnore
    public static <UT, UB> UB a(int i, int i2, UB ub, fb1<UT, UB> fb1) {
        if (ub == null) {
            ub = fb1.a();
        }
        fb1.a(ub, i, (long) i2);
        return ub;
    }
}
