package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class so3 extends as2 implements ro3, View.OnClickListener, TextWatcher, View.OnKeyListener, xs3.g {
    @DexIgnore
    public static /* final */ a u; // = new a((rd4) null);
    @DexIgnore
    public qo3 j;
    @DexIgnore
    public ur3<nb2> k;
    @DexIgnore
    public float l;
    @DexIgnore
    public float m;
    @DexIgnore
    public boolean n; // = true;
    @DexIgnore
    public ScaleAnimation o; // = new ScaleAnimation(1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public ScaleAnimation p; // = new ScaleAnimation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public ScaleAnimation q; // = new ScaleAnimation(1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public ScaleAnimation r; // = new ScaleAnimation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, 1, 0.5f, 1, 0.5f);
    @DexIgnore
    public TranslateAnimation s;
    @DexIgnore
    public HashMap t;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final so3 a() {
            return new so3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ so3 a;

        @DexIgnore
        public b(so3 so3) {
            this.a = so3;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            nb2 U0 = this.a.U0();
            if (U0 != null) {
                FlexibleTextView flexibleTextView = U0.F;
                wd4.a((Object) flexibleTextView, "it.tvTitle");
                flexibleTextView.setVisibility(8);
                ViewPropertyAnimator translationY = U0.r.animate().translationY(-this.a.Z0());
                wd4.a((Object) translationY, "it.clContainer.animate()\u2026slationY(-mtvTitleHeight)");
                translationY.setDuration(125);
                ViewPropertyAnimator translationY2 = U0.z.animate().translationY(-this.a.Y0());
                wd4.a((Object) translationY2, "it.ftvEmailNotification.\u2026slationY(-mivEmailHeight)");
                translationY2.setDuration(125);
                ViewPropertyAnimator translationY3 = U0.y.animate().translationY(-this.a.Y0());
                wd4.a((Object) translationY3, "it.ftvEmail.animate()\n  \u2026slationY(-mivEmailHeight)");
                translationY3.setDuration(125);
                ViewPropertyAnimator translationY4 = U0.t.animate().translationY(-this.a.Y0());
                wd4.a((Object) translationY4, "it.clVerificationCode.an\u2026slationY(-mivEmailHeight)");
                translationY4.setDuration(125);
                ViewPropertyAnimator translationY5 = U0.A.animate().translationY(-this.a.Y0());
                wd4.a((Object) translationY5, "it.ftvEnterCodesGuide.an\u2026slationY(-mivEmailHeight)");
                translationY5.setDuration(125);
                ViewPropertyAnimator translationY6 = U0.B.animate().translationY(-this.a.Y0());
                wd4.a((Object) translationY6, "it.ftvInvalidCode.animat\u2026slationY(-mivEmailHeight)");
                translationY6.setDuration(125);
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            nb2 U0 = this.a.U0();
            if (U0 != null) {
                FlexibleTextView flexibleTextView = U0.F;
                wd4.a((Object) flexibleTextView, "it.tvTitle");
                flexibleTextView.setVisibility(0);
                ImageView imageView = U0.E;
                wd4.a((Object) imageView, "it.ivEmail");
                imageView.setVisibility(0);
                this.a.W0().setDuration(250);
                U0.F.startAnimation(this.a.W0());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ so3 f;

        @DexIgnore
        public c(View view, so3 so3) {
            this.e = view;
            this.f = so3;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.e.getWindowVisibleDisplayFrame(rect);
            int height = this.e.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                nb2 U0 = this.f.U0();
                if (U0 != null && !this.f.n) {
                    U0.E.startAnimation(this.f.q);
                    this.f.n = true;
                    return;
                }
                return;
            }
            nb2 U02 = this.f.U0();
            if (U02 != null && this.f.n) {
                so3 so3 = this.f;
                FlexibleTextView flexibleTextView = U02.F;
                wd4.a((Object) flexibleTextView, "it.tvTitle");
                so3.b((float) flexibleTextView.getHeight());
                so3 so32 = this.f;
                ImageView imageView = U02.E;
                wd4.a((Object) imageView, "it.ivEmail");
                so32.a((float) imageView.getHeight());
                if (this.f.s == null) {
                    so3 so33 = this.f;
                    FlexibleTextView flexibleTextView2 = U02.F;
                    wd4.a((Object) flexibleTextView2, "it.tvTitle");
                    float x = flexibleTextView2.getX();
                    FlexibleTextView flexibleTextView3 = U02.F;
                    wd4.a((Object) flexibleTextView3, "it.tvTitle");
                    so33.s = new TranslateAnimation(1, x, 1, flexibleTextView3.getX(), 1, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1, this.f.Z0());
                    this.f.a1();
                }
                U02.r.startAnimation(this.f.s);
                this.f.n = false;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ so3 a;

        @DexIgnore
        public d(so3 so3) {
            this.a = so3;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            nb2 U0 = this.a.U0();
            if (U0 != null) {
                FlexibleTextView flexibleTextView = U0.F;
                wd4.a((Object) flexibleTextView, "it.tvTitle");
                flexibleTextView.setVisibility(0);
                ImageView imageView = U0.E;
                wd4.a((Object) imageView, "it.ivEmail");
                imageView.setVisibility(0);
                U0.E.startAnimation(this.a.V0());
                this.a.W0().setDuration(500);
                U0.F.startAnimation(this.a.X0());
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            nb2 U0 = this.a.U0();
            if (U0 != null) {
                FlexibleTextView flexibleTextView = U0.F;
                wd4.a((Object) flexibleTextView, "it.tvTitle");
                flexibleTextView.setVisibility(4);
                ImageView imageView = U0.E;
                wd4.a((Object) imageView, "it.ivEmail");
                imageView.setVisibility(4);
                ViewPropertyAnimator translationY = U0.r.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                wd4.a((Object) translationY, "it.clContainer.animate()\u2026        .translationY(0f)");
                translationY.setDuration(500);
                ViewPropertyAnimator translationY2 = U0.z.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                wd4.a((Object) translationY2, "it.ftvEmailNotification.\u2026        .translationY(0f)");
                translationY2.setDuration(500);
                ViewPropertyAnimator translationY3 = U0.y.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                wd4.a((Object) translationY3, "it.ftvEmail.animate()\n  \u2026        .translationY(0f)");
                translationY3.setDuration(500);
                ViewPropertyAnimator translationY4 = U0.t.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                wd4.a((Object) translationY4, "it.clVerificationCode.an\u2026        .translationY(0f)");
                translationY4.setDuration(500);
                ViewPropertyAnimator translationY5 = U0.A.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                wd4.a((Object) translationY5, "it.ftvEnterCodesGuide.an\u2026        .translationY(0f)");
                translationY5.setDuration(500);
                ViewPropertyAnimator translationY6 = U0.B.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                wd4.a((Object) translationY6, "it.ftvInvalidCode.animat\u2026        .translationY(0f)");
                translationY6.setDuration(500);
            }
        }
    }

    @DexIgnore
    public so3() {
    }

    @DexIgnore
    public void D0() {
        if (isActive()) {
            nb2 U0 = U0();
            if (U0 != null) {
                FlexibleTextView flexibleTextView = U0.F;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_SignUp_EmailResent_Title__EmailResent));
                }
            }
            nb2 U02 = U0();
            if (U02 != null) {
                ImageView imageView = U02.E;
                if (imageView != null) {
                    imageView.setImageDrawable(PortfolioApp.W.c().getDrawable(R.drawable.ic_email_resenting));
                }
            }
        }
    }

    @DexIgnore
    public void I(boolean z) {
        if (isActive()) {
            nb2 U0 = U0();
            if (U0 != null) {
                FlexibleTextView flexibleTextView = U0.B;
                if (flexibleTextView == null) {
                    return;
                }
                if (z) {
                    wd4.a((Object) flexibleTextView, "it");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                wd4.a((Object) flexibleTextView, "it");
                if (flexibleTextView.getVisibility() != 4) {
                    flexibleTextView.setVisibility(4);
                }
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void T0() {
        if (isActive()) {
            nb2 U0 = U0();
            if (U0 != null) {
                EditText editText = U0.u;
                wd4.a((Object) editText, "it.etFirstCode");
                EditText editText2 = U0.w;
                wd4.a((Object) editText2, "it.etSecondCode");
                EditText editText3 = U0.x;
                wd4.a((Object) editText3, "it.etThirdCode");
                EditText editText4 = U0.v;
                wd4.a((Object) editText4, "it.etFourthCode");
                String[] strArr = {editText.getText().toString(), editText2.getText().toString(), editText3.getText().toString(), editText4.getText().toString()};
                qo3 qo3 = this.j;
                if (qo3 != null) {
                    qo3.a(strArr);
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final nb2 U0() {
        ur3<nb2> ur3 = this.k;
        if (ur3 != null) {
            return ur3.a();
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final ScaleAnimation V0() {
        return this.r;
    }

    @DexIgnore
    public final ScaleAnimation W0() {
        return this.o;
    }

    @DexIgnore
    public final ScaleAnimation X0() {
        return this.p;
    }

    @DexIgnore
    public final float Y0() {
        return this.m;
    }

    @DexIgnore
    public final float Z0() {
        return this.l;
    }

    @DexIgnore
    public final void a1() {
        TranslateAnimation translateAnimation = this.s;
        if (translateAnimation != null) {
            translateAnimation.setAnimationListener(new d(this));
        }
    }

    @DexIgnore
    public void afterTextChanged(Editable editable) {
        FragmentActivity activity = getActivity();
        Integer num = null;
        View currentFocus = activity != null ? activity.getCurrentFocus() : null;
        nb2 U0 = U0();
        if (U0 != null) {
            if (currentFocus != null) {
                num = Integer.valueOf(currentFocus.getId());
            }
            boolean z = false;
            if (num != null && num.intValue() == R.id.et_first_code) {
                if (currentFocus != null) {
                    EditText editText = (EditText) currentFocus;
                    Editable text = editText.getText();
                    int length = text.length();
                    if (length == 0) {
                        FlexibleButton flexibleButton = U0.q;
                        wd4.a((Object) flexibleButton, "it.btContinue");
                        flexibleButton.setEnabled(false);
                    } else if (length == 1) {
                        editText.setSelection(1);
                        EditText editText2 = U0.w;
                        wd4.a((Object) editText2, "it.etSecondCode");
                        Editable text2 = editText2.getText();
                        wd4.a((Object) text2, "it.etSecondCode.text");
                        if (text2.length() == 0) {
                            z = true;
                        }
                        if (z) {
                            U0.w.requestFocus();
                        }
                        T0();
                    } else if (length == 2) {
                        char charAt = text.charAt(0);
                        char charAt2 = text.charAt(1);
                        EditText editText3 = U0.w;
                        wd4.a((Object) editText3, "it.etSecondCode");
                        Editable text3 = editText3.getText();
                        wd4.a((Object) text3, "it.etSecondCode.text");
                        if (text3.length() == 0) {
                            z = true;
                        }
                        if (z) {
                            U0.u.setTextKeepState(String.valueOf(charAt));
                            U0.u.clearFocus();
                            U0.w.requestFocus();
                            U0.w.setText(String.valueOf(charAt2));
                        } else {
                            U0.u.setTextKeepState(String.valueOf(charAt2));
                        }
                        editText.setSelection(1);
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type android.widget.EditText");
                }
            } else if (num != null && num.intValue() == R.id.et_second_code) {
                if (currentFocus != null) {
                    EditText editText4 = (EditText) currentFocus;
                    Editable text4 = editText4.getText();
                    int length2 = text4.length();
                    if (length2 == 0) {
                        FlexibleButton flexibleButton2 = U0.q;
                        wd4.a((Object) flexibleButton2, "it.btContinue");
                        flexibleButton2.setEnabled(false);
                    } else if (length2 == 1) {
                        editText4.setSelection(1);
                        EditText editText5 = U0.x;
                        wd4.a((Object) editText5, "it.etThirdCode");
                        Editable text5 = editText5.getText();
                        wd4.a((Object) text5, "it.etThirdCode.text");
                        if (text5.length() == 0) {
                            z = true;
                        }
                        if (z) {
                            U0.x.requestFocus();
                        }
                        T0();
                    } else if (length2 == 2) {
                        char charAt3 = text4.charAt(0);
                        char charAt4 = text4.charAt(1);
                        EditText editText6 = U0.x;
                        wd4.a((Object) editText6, "it.etThirdCode");
                        Editable text6 = editText6.getText();
                        wd4.a((Object) text6, "it.etThirdCode.text");
                        if (text6.length() == 0) {
                            z = true;
                        }
                        if (z) {
                            editText4.setTextKeepState(String.valueOf(charAt3));
                            editText4.clearFocus();
                            U0.x.requestFocus();
                            U0.x.setText(String.valueOf(charAt4));
                        } else {
                            editText4.setTextKeepState(String.valueOf(charAt4));
                        }
                        editText4.setSelection(1);
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type android.widget.EditText");
                }
            } else if (num != null && num.intValue() == R.id.et_third_code) {
                if (currentFocus != null) {
                    EditText editText7 = (EditText) currentFocus;
                    Editable text7 = editText7.getText();
                    int length3 = text7.length();
                    if (length3 == 0) {
                        FlexibleButton flexibleButton3 = U0.q;
                        wd4.a((Object) flexibleButton3, "it.btContinue");
                        flexibleButton3.setEnabled(false);
                    } else if (length3 == 1) {
                        editText7.setSelection(1);
                        EditText editText8 = U0.v;
                        wd4.a((Object) editText8, "it.etFourthCode");
                        Editable text8 = editText8.getText();
                        wd4.a((Object) text8, "it.etFourthCode.text");
                        if (text8.length() == 0) {
                            z = true;
                        }
                        if (z) {
                            U0.v.requestFocus();
                        }
                        T0();
                    } else if (length3 == 2) {
                        char charAt5 = text7.charAt(0);
                        char charAt6 = text7.charAt(1);
                        EditText editText9 = U0.v;
                        wd4.a((Object) editText9, "it.etFourthCode");
                        Editable text9 = editText9.getText();
                        wd4.a((Object) text9, "it.etFourthCode.text");
                        if (text9.length() == 0) {
                            z = true;
                        }
                        if (z) {
                            editText7.setText(String.valueOf(charAt5));
                            editText7.clearFocus();
                            U0.v.requestFocus();
                            U0.v.setText(String.valueOf(charAt6));
                        } else {
                            editText7.setText(String.valueOf(charAt6));
                        }
                        editText7.setSelection(1);
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type android.widget.EditText");
                }
            } else if (num == null || num.intValue() != R.id.et_fourth_code) {
            } else {
                if (currentFocus != null) {
                    EditText editText10 = (EditText) currentFocus;
                    int length4 = editText10.getText().length();
                    if (length4 == 0) {
                        FlexibleButton flexibleButton4 = U0.q;
                        wd4.a((Object) flexibleButton4, "it.btContinue");
                        flexibleButton4.setEnabled(false);
                    } else if (length4 == 1) {
                        editText10.setSelection(1);
                        T0();
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type android.widget.EditText");
                }
            }
        }
    }

    @DexIgnore
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @DexIgnore
    public void h0() {
        if (isActive()) {
            FragmentActivity activity = getActivity();
            if (activity == null) {
                return;
            }
            if (this.n) {
                dl2 dl2 = dl2.a;
                nb2 U0 = U0();
                ImageView imageView = U0 != null ? U0.D : null;
                if (imageView != null) {
                    wd4.a((Object) activity, "it");
                    dl2.a(imageView, activity);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.View");
            }
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
    }

    @DexIgnore
    public void i() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    public void k() {
        if (isActive()) {
            b();
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == R.id.bt_continue) {
                qo3 qo3 = this.j;
                if (qo3 != null) {
                    qo3.k();
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else if (id == R.id.ftv_resend_email) {
                qo3 qo32 = this.j;
                if (qo32 != null) {
                    qo32.i();
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else if (id == R.id.iv_back) {
                qo3 qo33 = this.j;
                if (qo33 != null) {
                    qo33.h();
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.o.setDuration(500);
        this.o.setFillAfter(true);
        this.p.setDuration(500);
        this.p.setFillAfter(true);
        this.q.setDuration(500);
        this.q.setFillAfter(true);
        this.r.setDuration(500);
        this.r.setFillAfter(true);
        this.q.setAnimationListener(new b(this));
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        nb2 nb2 = (nb2) ra.a(layoutInflater, R.layout.fragment_email_verification, viewGroup, false, O0());
        wd4.a((Object) nb2, "binding");
        View d2 = nb2.d();
        wd4.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new c(d2, this));
        this.k = new ur3<>(this, nb2);
        nb2 U0 = U0();
        if (U0 != null) {
            return U0.d();
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        wd4.b(view, "view");
        wd4.b(keyEvent, "keyEvent");
        if (i == 67 && keyEvent.getAction() == 0) {
            nb2 U0 = U0();
            if (U0 != null) {
                boolean z = true;
                if (view.getId() == R.id.et_second_code) {
                    EditText editText = U0.w;
                    wd4.a((Object) editText, "it.etSecondCode");
                    Editable text = editText.getText();
                    wd4.a((Object) text, "it.etSecondCode.text");
                    if (text.length() != 0) {
                        z = false;
                    }
                    if (z) {
                        U0.u.requestFocus();
                    }
                } else if (view.getId() == R.id.et_third_code) {
                    EditText editText2 = U0.x;
                    wd4.a((Object) editText2, "it.etThirdCode");
                    Editable text2 = editText2.getText();
                    wd4.a((Object) text2, "it.etThirdCode.text");
                    if (text2.length() != 0) {
                        z = false;
                    }
                    if (z) {
                        U0.w.requestFocus();
                    }
                } else if (view.getId() == R.id.et_fourth_code) {
                    EditText editText3 = U0.v;
                    wd4.a((Object) editText3, "it.etFourthCode");
                    Editable text3 = editText3.getText();
                    wd4.a((Object) text3, "it.etFourthCode.text");
                    if (text3.length() != 0) {
                        z = false;
                    }
                    if (z) {
                        U0.x.requestFocus();
                    }
                }
            }
        }
        return super.onKey(view, i, keyEvent);
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        if (!this.n) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                Window window = activity.getWindow();
                if (window != null) {
                    window.setSoftInputMode(3);
                }
            }
        }
        qo3 qo3 = this.j;
        if (qo3 != null) {
            qo3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        qo3 qo3 = this.j;
        if (qo3 != null) {
            qo3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        nb2 U0 = U0();
        if (U0 != null) {
            U0.D.setOnClickListener(this);
            U0.C.setOnClickListener(this);
            U0.q.setOnClickListener(this);
            U0.u.addTextChangedListener(this);
            U0.w.addTextChangedListener(this);
            U0.x.addTextChangedListener(this);
            U0.v.addTextChangedListener(this);
            U0.u.setOnKeyListener(this);
            U0.w.setOnKeyListener(this);
            U0.x.setOnKeyListener(this);
            U0.v.setOnKeyListener(this);
        }
    }

    @DexIgnore
    public void t(String str) {
        wd4.b(str, "emailAddress");
        if (isActive()) {
            nb2 U0 = U0();
            if (U0 != null) {
                FlexibleTextView flexibleTextView = U0.y;
                wd4.a((Object) flexibleTextView, "it.ftvEmail");
                flexibleTextView.setText(str);
            }
        }
    }

    @DexIgnore
    public void v(boolean z) {
        if (isActive()) {
            nb2 U0 = U0();
            if (U0 != null) {
                FlexibleButton flexibleButton = U0.q;
                if (flexibleButton != null) {
                    flexibleButton.setEnabled(z);
                }
            }
        }
    }

    @DexIgnore
    public final void b(float f) {
        this.l = f;
    }

    @DexIgnore
    public void d(int i, String str) {
        wd4.b(str, "errorMessage");
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(childFragmentManager, i, str);
        }
    }

    @DexIgnore
    public final void a(float f) {
        this.m = f;
    }

    @DexIgnore
    public void a(qo3 qo3) {
        wd4.b(qo3, "presenter");
        this.j = qo3;
    }

    @DexIgnore
    public void a(String str, int i, int i2) {
        wd4.b(str, "emailAddress");
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(childFragmentManager, i, i2, str);
            nb2 U0 = U0();
            if (U0 != null) {
                View d2 = U0.d();
                if (d2 != null) {
                    d2.setVisibility(8);
                }
            }
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        if (str.hashCode() == 766014770 && str.equals("EMAIL_OTP_VERIFICATION") && i == R.id.bt_continue_sign_up) {
            qo3 qo3 = this.j;
            if (qo3 != null) {
                qo3.j();
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(SignUpEmailAuth signUpEmailAuth) {
        wd4.b(signUpEmailAuth, "emailAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.C;
            wd4.a((Object) activity, "it");
            aVar.a((Context) activity, signUpEmailAuth);
        }
    }
}
