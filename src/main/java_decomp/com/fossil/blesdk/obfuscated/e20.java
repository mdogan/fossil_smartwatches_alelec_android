package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e20 extends c20 {
    @DexIgnore
    public /* final */ Peripheral.State a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public e20(Peripheral.State state, int i) {
        wd4.b(state, "newState");
        this.a = state;
        this.b = i;
    }

    @DexIgnore
    public final Peripheral.State a() {
        return this.a;
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }
}
