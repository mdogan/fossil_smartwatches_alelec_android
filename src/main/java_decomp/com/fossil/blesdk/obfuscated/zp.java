package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.engine.GlideException;
import com.fossil.blesdk.obfuscated.pp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zp<Data, ResourceType, Transcode> {
    @DexIgnore
    public /* final */ h8<List<Throwable>> a;
    @DexIgnore
    public /* final */ List<? extends pp<Data, ResourceType, Transcode>> b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public zp(Class<Data> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<pp<Data, ResourceType, Transcode>> list, h8<List<Throwable>> h8Var) {
        this.a = h8Var;
        uw.a(list);
        this.b = list;
        this.c = "Failed LoadPath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    public bq<Transcode> a(uo<Data> uoVar, mo moVar, int i, int i2, pp.a<ResourceType> aVar) throws GlideException {
        List<Throwable> a2 = this.a.a();
        uw.a(a2);
        List list = a2;
        try {
            return a(uoVar, moVar, i, i2, aVar, list);
        } finally {
            this.a.a(list);
        }
    }

    @DexIgnore
    public String toString() {
        return "LoadPath{decodePaths=" + Arrays.toString(this.b.toArray()) + '}';
    }

    @DexIgnore
    public final bq<Transcode> a(uo<Data> uoVar, mo moVar, int i, int i2, pp.a<ResourceType> aVar, List<Throwable> list) throws GlideException {
        List<Throwable> list2 = list;
        int size = this.b.size();
        bq<Transcode> bqVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            try {
                bqVar = ((pp) this.b.get(i3)).a(uoVar, i, i2, moVar, aVar);
            } catch (GlideException e) {
                list2.add(e);
            }
            if (bqVar != null) {
                break;
            }
        }
        if (bqVar != null) {
            return bqVar;
        }
        throw new GlideException(this.c, (List<Throwable>) new ArrayList(list2));
    }
}
