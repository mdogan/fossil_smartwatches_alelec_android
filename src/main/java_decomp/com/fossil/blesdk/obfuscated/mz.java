package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.ButtonService;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mz {
    @DexIgnore
    public static /* final */ Charset b; // = Charset.forName("UTF-8");
    @DexIgnore
    public /* final */ File a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends JSONObject {
        @DexIgnore
        public /* final */ /* synthetic */ g00 a;

        @DexIgnore
        public a(g00 g00) throws JSONException {
            this.a = g00;
            put(ButtonService.USER_ID, this.a.a);
            put("userName", this.a.b);
            put("userEmail", this.a.c);
        }
    }

    @DexIgnore
    public mz(File file) {
        this.a = file;
    }

    @DexIgnore
    public static g00 d(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        return new g00(a(jSONObject, ButtonService.USER_ID), a(jSONObject, "userName"), a(jSONObject, "userEmail"));
    }

    @DexIgnore
    public void a(String str, g00 g00) {
        File b2 = b(str);
        BufferedWriter bufferedWriter = null;
        try {
            String a2 = a(g00);
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(b2), b));
            try {
                bufferedWriter2.write(a2);
                bufferedWriter2.flush();
                CommonUtils.a((Closeable) bufferedWriter2, "Failed to close user metadata file.");
            } catch (Exception e) {
                e = e;
                bufferedWriter = bufferedWriter2;
                try {
                    r44.g().e("CrashlyticsCore", "Error serializing user metadata.", e);
                    CommonUtils.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
                } catch (Throwable th) {
                    th = th;
                    CommonUtils.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                CommonUtils.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            r44.g().e("CrashlyticsCore", "Error serializing user metadata.", e);
            CommonUtils.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
        }
    }

    @DexIgnore
    public File b(String str) {
        File file = this.a;
        return new File(file, str + "user" + ".meta");
    }

    @DexIgnore
    public g00 c(String str) {
        File b2 = b(str);
        if (!b2.exists()) {
            return g00.d;
        }
        FileInputStream fileInputStream = null;
        try {
            FileInputStream fileInputStream2 = new FileInputStream(b2);
            try {
                g00 d = d(CommonUtils.b((InputStream) fileInputStream2));
                CommonUtils.a((Closeable) fileInputStream2, "Failed to close user metadata file.");
                return d;
            } catch (Exception e) {
                e = e;
                fileInputStream = fileInputStream2;
                try {
                    r44.g().e("CrashlyticsCore", "Error deserializing user metadata.", e);
                    CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    return g00.d;
                } catch (Throwable th) {
                    th = th;
                    CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = fileInputStream2;
                CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            r44.g().e("CrashlyticsCore", "Error deserializing user metadata.", e);
            CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
            return g00.d;
        }
    }

    @DexIgnore
    public void a(String str, Map<String, String> map) {
        File a2 = a(str);
        BufferedWriter bufferedWriter = null;
        try {
            String a3 = a(map);
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(a2), b));
            try {
                bufferedWriter2.write(a3);
                bufferedWriter2.flush();
                CommonUtils.a((Closeable) bufferedWriter2, "Failed to close key/value metadata file.");
            } catch (Exception e) {
                e = e;
                bufferedWriter = bufferedWriter2;
                try {
                    r44.g().e("CrashlyticsCore", "Error serializing key/value metadata.", e);
                    CommonUtils.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
                } catch (Throwable th) {
                    th = th;
                    CommonUtils.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                CommonUtils.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            r44.g().e("CrashlyticsCore", "Error serializing key/value metadata.", e);
            CommonUtils.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
        }
    }

    @DexIgnore
    public File a(String str) {
        File file = this.a;
        return new File(file, str + "keys" + ".meta");
    }

    @DexIgnore
    public static String a(g00 g00) throws JSONException {
        return new a(g00).toString();
    }

    @DexIgnore
    public static String a(Map<String, String> map) throws JSONException {
        return new JSONObject(map).toString();
    }

    @DexIgnore
    public static String a(JSONObject jSONObject, String str) {
        if (!jSONObject.isNull(str)) {
            return jSONObject.optString(str, (String) null);
        }
        return null;
    }
}
