package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cj2 extends dj2 {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public cj2(boolean z, String str) {
        this.a = z;
        this.b = str;
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public boolean b() {
        return this.a;
    }
}
