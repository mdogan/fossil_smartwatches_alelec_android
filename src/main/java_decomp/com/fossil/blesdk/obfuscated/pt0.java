package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import com.google.android.gms.internal.clearcut.zzbb;
import com.google.android.gms.internal.clearcut.zzco;
import java.io.IOException;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pt0 {
    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3, cx0 cx0, qt0 qt0) throws IOException {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                int b = b(bArr, i2, qt0);
                cx0.a(i, (Object) Long.valueOf(qt0.b));
                return b;
            } else if (i4 == 1) {
                cx0.a(i, (Object) Long.valueOf(b(bArr, i2)));
                return i2 + 8;
            } else if (i4 == 2) {
                int a = a(bArr, i2, qt0);
                int i5 = qt0.a;
                cx0.a(i, (Object) i5 == 0 ? zzbb.zzfi : zzbb.zzb(bArr, a, i5));
                return a + i5;
            } else if (i4 == 3) {
                cx0 e = cx0.e();
                int i6 = (i & -8) | 4;
                int i7 = 0;
                while (true) {
                    if (i2 >= i3) {
                        break;
                    }
                    int a2 = a(bArr, i2, qt0);
                    int i8 = qt0.a;
                    i7 = i8;
                    if (i8 == i6) {
                        i2 = a2;
                        break;
                    }
                    int a3 = a(i7, bArr, a2, i3, e, qt0);
                    i7 = i8;
                    i2 = a3;
                }
                if (i2 > i3 || i7 != i6) {
                    throw zzco.zzbo();
                }
                cx0.a(i, (Object) e);
                return i2;
            } else if (i4 == 5) {
                cx0.a(i, (Object) Integer.valueOf(a(bArr, i2)));
                return i2 + 4;
            } else {
                throw zzco.zzbm();
            }
        } else {
            throw zzco.zzbm();
        }
    }

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3, qt0 qt0) throws zzco {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                return b(bArr, i2, qt0);
            }
            if (i4 == 1) {
                return i2 + 8;
            }
            if (i4 == 2) {
                return a(bArr, i2, qt0) + qt0.a;
            }
            if (i4 == 3) {
                int i5 = (i & -8) | 4;
                int i6 = 0;
                while (i2 < i3) {
                    i2 = a(bArr, i2, qt0);
                    i6 = qt0.a;
                    if (i6 == i5) {
                        break;
                    }
                    i2 = a(i6, bArr, i2, i3, qt0);
                }
                if (i2 <= i3 && i6 == i5) {
                    return i2;
                }
                throw zzco.zzbo();
            } else if (i4 == 5) {
                return i2 + 4;
            } else {
                throw zzco.zzbm();
            }
        } else {
            throw zzco.zzbm();
        }
    }

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3, xu0<?> xu0, qt0 qt0) {
        tu0 tu0 = (tu0) xu0;
        int a = a(bArr, i2, qt0);
        while (true) {
            tu0.f(qt0.a);
            if (a >= i3) {
                break;
            }
            int a2 = a(bArr, a, qt0);
            if (i != qt0.a) {
                break;
            }
            a = a(bArr, a2, qt0);
        }
        return a;
    }

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, qt0 qt0) {
        int i3;
        int i4;
        int i5 = i & 127;
        int i6 = i2 + 1;
        byte b = bArr[i2];
        if (b >= 0) {
            i4 = b << 7;
        } else {
            int i7 = i5 | ((b & Byte.MAX_VALUE) << 7);
            int i8 = i6 + 1;
            byte b2 = bArr[i6];
            if (b2 >= 0) {
                i3 = b2 << DateTimeFieldType.HOUR_OF_HALFDAY;
            } else {
                i5 = i7 | ((b2 & Byte.MAX_VALUE) << DateTimeFieldType.HOUR_OF_HALFDAY);
                i6 = i8 + 1;
                byte b3 = bArr[i8];
                if (b3 >= 0) {
                    i4 = b3 << DateTimeFieldType.SECOND_OF_MINUTE;
                } else {
                    i7 = i5 | ((b3 & Byte.MAX_VALUE) << DateTimeFieldType.SECOND_OF_MINUTE);
                    i8 = i6 + 1;
                    byte b4 = bArr[i6];
                    if (b4 >= 0) {
                        i3 = b4 << 28;
                    } else {
                        int i9 = i7 | ((b4 & Byte.MAX_VALUE) << 28);
                        while (true) {
                            int i10 = i8 + 1;
                            if (bArr[i8] >= 0) {
                                qt0.a = i9;
                                return i10;
                            }
                            i8 = i10;
                        }
                    }
                }
            }
            qt0.a = i7 | i3;
            return i8;
        }
        qt0.a = i5 | i4;
        return i6;
    }

    @DexIgnore
    public static int a(byte[] bArr, int i) {
        return ((bArr[i + 3] & FileType.MASKED_INDEX) << 24) | (bArr[i] & FileType.MASKED_INDEX) | ((bArr[i + 1] & FileType.MASKED_INDEX) << 8) | ((bArr[i + 2] & FileType.MASKED_INDEX) << DateTimeFieldType.CLOCKHOUR_OF_DAY);
    }

    @DexIgnore
    public static int a(byte[] bArr, int i, qt0 qt0) {
        int i2 = i + 1;
        byte b = bArr[i];
        if (b < 0) {
            return a((int) b, bArr, i2, qt0);
        }
        qt0.a = b;
        return i2;
    }

    @DexIgnore
    public static int a(byte[] bArr, int i, xu0<?> xu0, qt0 qt0) throws IOException {
        tu0 tu0 = (tu0) xu0;
        int a = a(bArr, i, qt0);
        int i2 = qt0.a + a;
        while (a < i2) {
            a = a(bArr, a, qt0);
            tu0.f(qt0.a);
        }
        if (a == i2) {
            return a;
        }
        throw zzco.zzbl();
    }

    @DexIgnore
    public static int b(byte[] bArr, int i, qt0 qt0) {
        int i2 = i + 1;
        long j = (long) bArr[i];
        if (j >= 0) {
            qt0.b = j;
            return i2;
        }
        int i3 = i2 + 1;
        byte b = bArr[i2];
        long j2 = (j & 127) | (((long) (b & Byte.MAX_VALUE)) << 7);
        int i4 = 7;
        while (b < 0) {
            int i5 = i3 + 1;
            byte b2 = bArr[i3];
            i4 += 7;
            j2 |= ((long) (b2 & Byte.MAX_VALUE)) << i4;
            int i6 = i5;
            b = b2;
            i3 = i6;
        }
        qt0.b = j2;
        return i3;
    }

    @DexIgnore
    public static long b(byte[] bArr, int i) {
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    @DexIgnore
    public static double c(byte[] bArr, int i) {
        return Double.longBitsToDouble(b(bArr, i));
    }

    @DexIgnore
    public static int c(byte[] bArr, int i, qt0 qt0) {
        int a = a(bArr, i, qt0);
        int i2 = qt0.a;
        if (i2 == 0) {
            qt0.c = "";
            return a;
        }
        qt0.c = new String(bArr, a, i2, uu0.a);
        return a + i2;
    }

    @DexIgnore
    public static float d(byte[] bArr, int i) {
        return Float.intBitsToFloat(a(bArr, i));
    }

    @DexIgnore
    public static int d(byte[] bArr, int i, qt0 qt0) throws IOException {
        int a = a(bArr, i, qt0);
        int i2 = qt0.a;
        if (i2 == 0) {
            qt0.c = "";
            return a;
        }
        int i3 = a + i2;
        if (kx0.a(bArr, a, i3)) {
            qt0.c = new String(bArr, a, i2, uu0.a);
            return i3;
        }
        throw zzco.zzbp();
    }

    @DexIgnore
    public static int e(byte[] bArr, int i, qt0 qt0) {
        int a = a(bArr, i, qt0);
        int i2 = qt0.a;
        if (i2 == 0) {
            qt0.c = zzbb.zzfi;
            return a;
        }
        qt0.c = zzbb.zzb(bArr, a, i2);
        return a + i2;
    }
}
