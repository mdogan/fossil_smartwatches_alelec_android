package com.fossil.blesdk.obfuscated;

import android.location.Location;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e41 extends yd1 {
    @DexIgnore
    public /* final */ af0<sc1> e;

    @DexIgnore
    public e41(af0<sc1> af0) {
        this.e = af0;
    }

    @DexIgnore
    public final synchronized void o() {
        this.e.a();
    }

    @DexIgnore
    public final synchronized void onLocationChanged(Location location) {
        this.e.a(new f41(this, location));
    }
}
