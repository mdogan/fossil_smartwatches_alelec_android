package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nv2 implements Factory<pg3> {
    @DexIgnore
    public static pg3 a(hv2 hv2) {
        pg3 f = hv2.f();
        o44.a(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }
}
