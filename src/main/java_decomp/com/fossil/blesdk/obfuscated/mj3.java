package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.kr2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mj3 extends hj3 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((rd4) null);
    @DexIgnore
    public /* final */ ij3 f;
    @DexIgnore
    public /* final */ kr2 g;
    @DexIgnore
    public /* final */ k62 h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return mj3.i;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements j62.d<kr2.d, kr2.c> {
        @DexIgnore
        public /* final */ /* synthetic */ mj3 a;

        @DexIgnore
        public b(mj3 mj3) {
            this.a = mj3;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(kr2.d dVar) {
            wd4.b(dVar, "successResponse");
            if (this.a.f.isActive()) {
                this.a.f.d();
                this.a.f.J0();
            }
        }

        @DexIgnore
        public void a(kr2.c cVar) {
            wd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = mj3.j.a();
            local.e(a2, "updateUserPassword onFailure: errorValue = " + cVar);
            if (this.a.f.isActive()) {
                this.a.f.d();
                int a3 = cVar.a();
                if (a3 == 400004) {
                    this.a.f.m0();
                } else if (a3 != 403005) {
                    this.a.f.e(cVar.a(), cVar.b());
                } else {
                    this.a.f.A0();
                }
            }
        }
    }

    /*
    static {
        String simpleName = mj3.class.getSimpleName();
        wd4.a((Object) simpleName, "ProfileChangePasswordPre\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public mj3(ij3 ij3, kr2 kr2, k62 k62) {
        wd4.b(ij3, "mView");
        wd4.b(kr2, "mChangePasswordUseCase");
        wd4.b(k62, "mUseCaseHandler");
        this.f = ij3;
        this.g = kr2;
        this.h = k62;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(i, "presenter starts");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(i, "presenter stop");
    }

    @DexIgnore
    public void h() {
        this.f.a(this);
    }

    @DexIgnore
    public void a(String str, String str2) {
        wd4.b(str, "oldPass");
        wd4.b(str2, "newPass");
        if (!bs3.b(PortfolioApp.W.c())) {
            this.f.e(601, (String) null);
            return;
        }
        this.f.e();
        this.h.a(this.g, new kr2.b(str, str2), new b(this));
    }
}
