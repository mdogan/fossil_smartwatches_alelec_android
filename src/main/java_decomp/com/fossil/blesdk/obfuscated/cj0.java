package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cj0 {
    @DexIgnore
    public /* final */ DataHolder a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public cj0(DataHolder dataHolder, int i) {
        ck0.a(dataHolder);
        this.a = dataHolder;
        a(i);
    }

    @DexIgnore
    public final void a(int i) {
        ck0.b(i >= 0 && i < this.a.getCount());
        this.b = i;
        this.c = this.a.f(this.b);
    }

    @DexIgnore
    public int b(String str) {
        return this.a.b(str, this.b, this.c);
    }

    @DexIgnore
    public String c(String str) {
        return this.a.c(str, this.b, this.c);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof cj0) {
            cj0 cj0 = (cj0) obj;
            if (!ak0.a(Integer.valueOf(cj0.b), Integer.valueOf(this.b)) || !ak0.a(Integer.valueOf(cj0.c), Integer.valueOf(this.c)) || cj0.a != this.a) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return ak0.a(Integer.valueOf(this.b), Integer.valueOf(this.c), this.a);
    }

    @DexIgnore
    public byte[] a(String str) {
        return this.a.a(str, this.b, this.c);
    }
}
