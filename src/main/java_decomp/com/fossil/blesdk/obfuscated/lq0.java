package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lq0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<lq0> CREATOR; // = new nq0();
    @DexIgnore
    public /* final */ ap0 e;
    @DexIgnore
    public /* final */ wp0 f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ long h;

    @DexIgnore
    public lq0(ap0 ap0, IBinder iBinder, long j, long j2) {
        this.e = ap0;
        this.f = xp0.a(iBinder);
        this.g = j;
        this.h = j2;
    }

    @DexIgnore
    public ap0 H() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof lq0)) {
            return false;
        }
        lq0 lq0 = (lq0) obj;
        return ak0.a(this.e, lq0.e) && this.g == lq0.g && this.h == lq0.h;
    }

    @DexIgnore
    public int hashCode() {
        return ak0.a(this.e, Long.valueOf(this.g), Long.valueOf(this.h));
    }

    @DexIgnore
    public String toString() {
        return String.format("FitnessSensorServiceRequest{%s}", new Object[]{this.e});
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, (Parcelable) H(), i, false);
        lk0.a(parcel, 2, this.f.asBinder(), false);
        lk0.a(parcel, 3, this.g);
        lk0.a(parcel, 4, this.h);
        lk0.a(parcel, a);
    }
}
