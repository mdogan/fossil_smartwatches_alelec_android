package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dq1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<dq1> CREATOR; // = new eq1();
    @DexIgnore
    public byte e;
    @DexIgnore
    public /* final */ byte f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public dq1(byte b, byte b2, String str) {
        this.e = b;
        this.f = b2;
        this.g = str;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || dq1.class != obj.getClass()) {
            return false;
        }
        dq1 dq1 = (dq1) obj;
        return this.e == dq1.e && this.f == dq1.f && this.g.equals(dq1.g);
    }

    @DexIgnore
    public final int hashCode() {
        return ((((this.e + 31) * 31) + this.f) * 31) + this.g.hashCode();
    }

    @DexIgnore
    public final String toString() {
        byte b = this.e;
        byte b2 = this.f;
        String str = this.g;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 73);
        sb.append("AmsEntityUpdateParcelable{, mEntityId=");
        sb.append(b);
        sb.append(", mAttributeId=");
        sb.append(b2);
        sb.append(", mValue='");
        sb.append(str);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 2, this.e);
        lk0.a(parcel, 3, this.f);
        lk0.a(parcel, 4, this.g, false);
        lk0.a(parcel, a);
    }
}
