package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.settings.SettingsCacheBehavior;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class t74 implements b84 {
    @DexIgnore
    public /* final */ f84 a;
    @DexIgnore
    public /* final */ e84 b;
    @DexIgnore
    public /* final */ o54 c;
    @DexIgnore
    public /* final */ q74 d;
    @DexIgnore
    public /* final */ g84 e;
    @DexIgnore
    public /* final */ w44 f;
    @DexIgnore
    public /* final */ h74 g; // = new i74(this.f);
    @DexIgnore
    public /* final */ p54 h;

    @DexIgnore
    public t74(w44 w44, f84 f84, o54 o54, e84 e84, q74 q74, g84 g84, p54 p54) {
        this.f = w44;
        this.a = f84;
        this.c = o54;
        this.b = e84;
        this.d = q74;
        this.e = g84;
        this.h = p54;
    }

    @DexIgnore
    public c84 a() {
        return a(SettingsCacheBehavior.USE_CACHE);
    }

    @DexIgnore
    public final c84 b(SettingsCacheBehavior settingsCacheBehavior) {
        c84 c84 = null;
        try {
            if (SettingsCacheBehavior.SKIP_CACHE_LOOKUP.equals(settingsCacheBehavior)) {
                return null;
            }
            JSONObject a2 = this.d.a();
            if (a2 != null) {
                c84 a3 = this.b.a(this.c, a2);
                if (a3 != null) {
                    a(a2, "Loaded cached settings: ");
                    long a4 = this.c.a();
                    if (!SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION.equals(settingsCacheBehavior)) {
                        if (a3.a(a4)) {
                            r44.g().d("Fabric", "Cached settings have expired.");
                            return null;
                        }
                    }
                    try {
                        r44.g().d("Fabric", "Returning cached settings.");
                        return a3;
                    } catch (Exception e2) {
                        e = e2;
                        c84 = a3;
                        r44.g().e("Fabric", "Failed to get cached settings", e);
                        return c84;
                    }
                } else {
                    r44.g().e("Fabric", "Failed to transform cached settings data.", (Throwable) null);
                    return null;
                }
            } else {
                r44.g().d("Fabric", "No cached settings data found.");
                return null;
            }
        } catch (Exception e3) {
            e = e3;
            r44.g().e("Fabric", "Failed to get cached settings", e);
            return c84;
        }
    }

    @DexIgnore
    public String c() {
        return CommonUtils.a(CommonUtils.n(this.f.l()));
    }

    @DexIgnore
    public String d() {
        return this.g.get().getString("existing_instance_identifier", "");
    }

    @DexIgnore
    public c84 a(SettingsCacheBehavior settingsCacheBehavior) {
        c84 c84 = null;
        if (!this.h.a()) {
            r44.g().d("Fabric", "Not fetching settings, because data collection is disabled by Firebase.");
            return null;
        }
        try {
            if (!r44.h() && !b()) {
                c84 = b(settingsCacheBehavior);
            }
            if (c84 == null) {
                JSONObject a2 = this.e.a(this.a);
                if (a2 != null) {
                    c84 = this.b.a(this.c, a2);
                    this.d.a(c84.f, a2);
                    a(a2, "Loaded settings: ");
                    a(c());
                }
            }
            if (c84 == null) {
                return b(SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION);
            }
            return c84;
        } catch (Exception e2) {
            r44.g().e("Fabric", "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved.", e2);
            return null;
        }
    }

    @DexIgnore
    public final void a(JSONObject jSONObject, String str) throws JSONException {
        z44 g2 = r44.g();
        g2.d("Fabric", str + jSONObject.toString());
    }

    @DexIgnore
    public boolean b() {
        return !d().equals(c());
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public boolean a(String str) {
        SharedPreferences.Editor edit = this.g.edit();
        edit.putString("existing_instance_identifier", str);
        return this.g.a(edit);
    }
}
