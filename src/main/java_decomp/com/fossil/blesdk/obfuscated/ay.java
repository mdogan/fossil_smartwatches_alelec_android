package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ay extends f54 implements u64 {
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public ay(w44 w44, String str, String str2, a74 a74, String str3) {
        super(w44, str, str2, a74, HttpMethod.POST);
        this.g = str3;
    }

    @DexIgnore
    public boolean a(List<File> list) {
        HttpRequest a = a();
        a.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        a.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.e.r());
        a.c("X-CRASHLYTICS-API-KEY", this.g);
        int i = 0;
        for (File next : list) {
            a.a("session_analytics_file_" + i, next.getName(), "application/vnd.crashlytics.android.events", next);
            i++;
        }
        z44 g2 = r44.g();
        g2.d("Answers", "Sending " + list.size() + " analytics files to " + b());
        int g3 = a.g();
        z44 g4 = r44.g();
        g4.d("Answers", "Response code for analytics file send is " + g3);
        if (x54.a(g3) == 0) {
            return true;
        }
        return false;
    }
}
