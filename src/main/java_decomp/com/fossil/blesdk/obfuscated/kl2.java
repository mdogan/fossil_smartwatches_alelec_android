package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kl2 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements PagingRequestHelper.a {
        @DexIgnore
        public /* final */ /* synthetic */ MutableLiveData a;

        @DexIgnore
        public a(MutableLiveData mutableLiveData) {
            this.a = mutableLiveData;
        }

        @DexIgnore
        public final void a(PagingRequestHelper.e eVar) {
            wd4.b(eVar, "report");
            if (eVar.b()) {
                this.a.a(NetworkState.Companion.getLOADING());
            } else if (eVar.a()) {
                this.a.a(NetworkState.Companion.error(kl2.b(eVar)));
            } else {
                this.a.a(NetworkState.Companion.getLOADED());
            }
        }
    }

    @DexIgnore
    public static final String b(PagingRequestHelper.e eVar) {
        PagingRequestHelper.RequestType[] values = PagingRequestHelper.RequestType.values();
        ArrayList arrayList = new ArrayList();
        for (PagingRequestHelper.RequestType a2 : values) {
            Throwable a3 = eVar.a(a2);
            String message = a3 != null ? a3.getMessage() : null;
            if (message != null) {
                arrayList.add(message);
            }
        }
        return (String) wb4.d(arrayList);
    }

    @DexIgnore
    public static final LiveData<NetworkState> a(PagingRequestHelper pagingRequestHelper) {
        wd4.b(pagingRequestHelper, "$this$createStatusLiveData");
        MutableLiveData mutableLiveData = new MutableLiveData();
        pagingRequestHelper.a((PagingRequestHelper.a) new a(mutableLiveData));
        return mutableLiveData;
    }
}
