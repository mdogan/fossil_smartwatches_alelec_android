package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.fossil.wearables.fossil.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.HashMap;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qv2 extends ys3 implements ty2 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((rd4) null);
    @DexIgnore
    public /* final */ qa m; // = new w62(this);
    @DexIgnore
    public ur3<lb2> n;
    @DexIgnore
    public sy2 o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return qv2.q;
        }

        @DexIgnore
        public final qv2 b() {
            return new qv2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ qv2 a;
        @DexIgnore
        public /* final */ /* synthetic */ lb2 b;

        @DexIgnore
        public b(qv2 qv2, lb2 lb2) {
            this.a = qv2;
            this.b = lb2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            sy2 a2 = qv2.a(this.a);
            String valueOf = String.valueOf(i2);
            NumberPicker numberPicker2 = this.b.v;
            wd4.a((Object) numberPicker2, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.u;
            wd4.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ qv2 a;
        @DexIgnore
        public /* final */ /* synthetic */ lb2 b;

        @DexIgnore
        public c(qv2 qv2, lb2 lb2) {
            this.a = qv2;
            this.b = lb2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            sy2 a2 = qv2.a(this.a);
            NumberPicker numberPicker2 = this.b.t;
            wd4.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            String valueOf2 = String.valueOf(i2);
            NumberPicker numberPicker3 = this.b.u;
            wd4.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ qv2 a;
        @DexIgnore
        public /* final */ /* synthetic */ lb2 b;

        @DexIgnore
        public d(qv2 qv2, lb2 lb2) {
            this.a = qv2;
            this.b = lb2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            sy2 a2 = qv2.a(this.a);
            NumberPicker numberPicker2 = this.b.t;
            wd4.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.v;
            wd4.a((Object) numberPicker3, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker3.getValue());
            boolean z = true;
            if (i2 != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qv2 e;

        @DexIgnore
        public e(qv2 qv2) {
            this.e = qv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            qv2.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ lb2 e;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef f;

        @DexIgnore
        public f(lb2 lb2, Ref$ObjectRef ref$ObjectRef) {
            this.e = lb2;
            this.f = ref$ObjectRef;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.e.q;
            wd4.a((Object) constraintLayout, "it.clRoot");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).d();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.c(3);
                        lb2 lb2 = this.e;
                        wd4.a((Object) lb2, "it");
                        View d = lb2.d();
                        wd4.a((Object) d, "it.root");
                        d.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) this.f.element);
                        return;
                    }
                    wd4.a();
                    throw null;
                }
                throw new TypeCastException("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new TypeCastException("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = qv2.class.getSimpleName();
        wd4.a((Object) simpleName, "DoNotDisturbScheduledTim\u2026nt::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ sy2 a(qv2 qv2) {
        sy2 sy2 = qv2.o;
        if (sy2 != null) {
            return sy2;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    public void d(String str) {
        wd4.b(str, "title");
        ur3<lb2> ur3 = this.n;
        if (ur3 != null) {
            lb2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        lb2 lb2 = (lb2) ra.a(layoutInflater, R.layout.fragment_do_not_disturb_scheduled_time, viewGroup, false, this.m);
        lb2.r.setOnClickListener(new e(this));
        wd4.a((Object) lb2, "binding");
        a(lb2);
        this.n = new ur3<>(this, lb2);
        return lb2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        sy2 sy2 = this.o;
        if (sy2 != null) {
            sy2.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        sy2 sy2 = this.o;
        if (sy2 != null) {
            sy2.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<lb2> ur3 = this.n;
        if (ur3 != null) {
            lb2 a2 = ur3.a();
            if (a2 != null) {
                Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = null;
                ref$ObjectRef.element = new f(a2, ref$ObjectRef);
                wd4.a((Object) a2, "it");
                View d2 = a2.d();
                wd4.a((Object) d2, "it.root");
                d2.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) ref$ObjectRef.element);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void p(int i) {
        sy2 sy2 = this.o;
        if (sy2 != null) {
            sy2.a(i);
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(sy2 sy2) {
        wd4.b(sy2, "presenter");
        this.o = sy2;
    }

    @DexIgnore
    public final void a(lb2 lb2) {
        NumberPicker numberPicker = lb2.t;
        wd4.a((Object) numberPicker, "binding.numberPickerOne");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = lb2.t;
        wd4.a((Object) numberPicker2, "binding.numberPickerOne");
        numberPicker2.setMaxValue(12);
        lb2.t.setOnValueChangedListener(new b(this, lb2));
        NumberPicker numberPicker3 = lb2.v;
        wd4.a((Object) numberPicker3, "binding.numberPickerTwo");
        numberPicker3.setMinValue(0);
        NumberPicker numberPicker4 = lb2.v;
        wd4.a((Object) numberPicker4, "binding.numberPickerTwo");
        numberPicker4.setMaxValue(59);
        lb2.v.setOnValueChangedListener(new c(this, lb2));
        String[] strArr = {tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Am), tm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Pm)};
        NumberPicker numberPicker5 = lb2.u;
        wd4.a((Object) numberPicker5, "binding.numberPickerThree");
        numberPicker5.setMinValue(0);
        NumberPicker numberPicker6 = lb2.u;
        wd4.a((Object) numberPicker6, "binding.numberPickerThree");
        numberPicker6.setMaxValue(1);
        lb2.u.setDisplayedValues(strArr);
        lb2.u.setOnValueChangedListener(new d(this, lb2));
    }

    @DexIgnore
    public void a(int i) {
        int i2;
        int i3 = i / 60;
        int i4 = i % 60;
        if (i3 >= 12) {
            i2 = 1;
            i3 -= 12;
        } else {
            i2 = 0;
        }
        if (i3 == 0) {
            i3 = 12;
        }
        ur3<lb2> ur3 = this.n;
        if (ur3 != null) {
            lb2 a2 = ur3.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.t;
                wd4.a((Object) numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i3);
                NumberPicker numberPicker2 = a2.v;
                wd4.a((Object) numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i4);
                NumberPicker numberPicker3 = a2.u;
                wd4.a((Object) numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i2);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }
}
