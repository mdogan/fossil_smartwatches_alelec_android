package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ft3 {
    @DexIgnore
    public CharSequence a;
    @DexIgnore
    public CharSequence b;
    @DexIgnore
    public CharSequence c;

    @DexIgnore
    public CharSequence a() {
        return this.b;
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        this.a = charSequence;
    }

    @DexIgnore
    public void c(CharSequence charSequence) {
        this.c = charSequence;
    }

    @DexIgnore
    public CharSequence a(boolean z) {
        return z ? this.a : this.c;
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        this.b = charSequence;
    }
}
