package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class l14 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ p04 e;
    @DexIgnore
    public /* final */ /* synthetic */ q24 f;
    @DexIgnore
    public /* final */ /* synthetic */ boolean g;
    @DexIgnore
    public /* final */ /* synthetic */ boolean h;
    @DexIgnore
    public /* final */ /* synthetic */ h14 i;

    @DexIgnore
    public l14(h14 h14, p04 p04, q24 q24, boolean z, boolean z2) {
        this.i = h14;
        this.e = p04;
        this.f = q24;
        this.g = z;
        this.h = z2;
    }

    @DexIgnore
    public void run() {
        this.i.b(this.e, this.f, this.g, this.h);
    }
}
