package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import com.facebook.internal.AnalyticsEvents;
import java.lang.ref.WeakReference;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wa0 {
    @DexIgnore
    public static WeakReference<Context> a;
    @DexIgnore
    public static String b; // = new String();
    @DexIgnore
    public static oa0 c; // = new oa0("", "", "");
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ wa0 f; // = new wa0();

    /*
    static {
        new oa0("", "", "");
        String str = Build.VERSION.RELEASE;
        if (str == null) {
            str = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        d = str;
        String str2 = Build.MODEL;
        if (str2 == null) {
            str2 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        e = str2;
    }
    */

    @DexIgnore
    public final void a(String str) {
        wd4.b(str, "<set-?>");
        b = str;
    }

    @DexIgnore
    public final String b() {
        return d;
    }

    @DexIgnore
    public final String c() {
        return e;
    }

    @DexIgnore
    public final int d() {
        return (TimeZone.getDefault().getOffset(System.currentTimeMillis()) / 1000) / 60;
    }

    @DexIgnore
    public final String e() {
        return b;
    }

    @DexIgnore
    public final void a(oa0 oa0) {
        wd4.b(oa0, "value");
        c = oa0;
        ea0 ea0 = ea0.l;
        ea0.a(new oa0(c.c() + "/sdk_log", c.a(), c.b()));
        aa0 aa0 = aa0.l;
        aa0.a(new oa0(c.c() + "/raw_minute_data", c.a(), c.b()));
        z90 z90 = z90.l;
        z90.a(new oa0(c.c() + "/raw_hardware_log", c.a(), c.b()));
    }

    @DexIgnore
    public final void a(Context context) {
        wd4.b(context, "context");
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            a = new WeakReference<>(applicationContext);
        }
    }

    @DexIgnore
    public final Context a() {
        WeakReference<Context> weakReference = a;
        if (weakReference != null) {
            return (Context) weakReference.get();
        }
        return null;
    }
}
