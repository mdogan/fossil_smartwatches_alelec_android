package com.fossil.blesdk.obfuscated;

import com.facebook.internal.FileLruCache;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lb4 extends kb4 {
    @DexIgnore
    public static final char a(char[] cArr) {
        wd4.b(cArr, "$this$single");
        int length = cArr.length;
        if (length == 0) {
            throw new NoSuchElementException("Array is empty.");
        } else if (length == 1) {
            return cArr[0];
        } else {
            throw new IllegalArgumentException("Array has more than one element.");
        }
    }

    @DexIgnore
    public static final <T> boolean b(T[] tArr, T t) {
        wd4.b(tArr, "$this$contains");
        return c(tArr, t) >= 0;
    }

    @DexIgnore
    public static final <T> int c(T[] tArr, T t) {
        wd4.b(tArr, "$this$indexOf");
        int i = 0;
        if (t == null) {
            int length = tArr.length;
            while (i < length) {
                if (tArr[i] == null) {
                    return i;
                }
                i++;
            }
            return -1;
        }
        int length2 = tArr.length;
        while (i < length2) {
            if (wd4.a((Object) t, (Object) tArr[i])) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public static final <T> T d(T[] tArr) {
        wd4.b(tArr, "$this$singleOrNull");
        if (tArr.length == 1) {
            return tArr[0];
        }
        return null;
    }

    @DexIgnore
    public static final <T> List<T> e(T[] tArr) {
        wd4.b(tArr, "$this$toList");
        int length = tArr.length;
        if (length == 0) {
            return ob4.a();
        }
        if (length != 1) {
            return f(tArr);
        }
        return nb4.a(tArr[0]);
    }

    @DexIgnore
    public static final <T> List<T> f(T[] tArr) {
        wd4.b(tArr, "$this$toMutableList");
        return new ArrayList(ob4.b(tArr));
    }

    @DexIgnore
    public static final <T> Set<T> g(T[] tArr) {
        wd4.b(tArr, "$this$toMutableSet");
        LinkedHashSet linkedHashSet = new LinkedHashSet(dc4.a(tArr.length));
        for (T add : tArr) {
            linkedHashSet.add(add);
        }
        return linkedHashSet;
    }

    @DexIgnore
    public static final <T> Set<T> h(T[] tArr) {
        wd4.b(tArr, "$this$toSet");
        int length = tArr.length;
        if (length == 0) {
            return hc4.a();
        }
        if (length == 1) {
            return gc4.a(tArr[0]);
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet(dc4.a(tArr.length));
        a(tArr, linkedHashSet);
        return linkedHashSet;
    }

    @DexIgnore
    public static final <T> T[] b(T[] tArr, Comparator<? super T> comparator) {
        wd4.b(tArr, "$this$sortedArrayWith");
        wd4.b(comparator, "comparator");
        if (tArr.length == 0) {
            return tArr;
        }
        T[] copyOf = Arrays.copyOf(tArr, tArr.length);
        wd4.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
        kb4.a(copyOf, comparator);
        return copyOf;
    }

    @DexIgnore
    public static final int b(byte[] bArr) {
        wd4.b(bArr, "$this$lastIndex");
        return bArr.length - 1;
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> T[] a(T[] tArr) {
        wd4.b(tArr, "$this$sortedArray");
        if (tArr.length == 0) {
            return tArr;
        }
        T[] copyOf = Arrays.copyOf(tArr, tArr.length);
        wd4.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
        T[] tArr2 = (Comparable[]) copyOf;
        if (tArr2 != null) {
            kb4.b(tArr2);
            return tArr2;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
    }

    @DexIgnore
    public static final byte[] c(byte[] bArr) {
        wd4.b(bArr, "$this$reversedArray");
        int i = 0;
        if (bArr.length == 0) {
            return bArr;
        }
        byte[] bArr2 = new byte[bArr.length];
        int b = b(bArr);
        if (b >= 0) {
            while (true) {
                bArr2[b - i] = bArr[i];
                if (i == b) {
                    break;
                }
                i++;
            }
        }
        return bArr2;
    }

    @DexIgnore
    public static final ke4 a(byte[] bArr) {
        wd4.b(bArr, "$this$indices");
        return new ke4(0, b(bArr));
    }

    @DexIgnore
    public static final <T, C extends Collection<? super T>> C a(T[] tArr, C c) {
        wd4.b(tArr, "$this$toCollection");
        wd4.b(c, ShareConstants.DESTINATION);
        for (T add : tArr) {
            c.add(add);
        }
        return c;
    }

    @DexIgnore
    public static final <T> List<T> c(T[] tArr, Comparator<? super T> comparator) {
        wd4.b(tArr, "$this$sortedWith");
        wd4.b(comparator, "comparator");
        return kb4.a((T[]) b(tArr, comparator));
    }

    @DexIgnore
    public static final <T, A extends Appendable> A a(T[] tArr, A a, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, jd4<? super T, ? extends CharSequence> jd4) {
        wd4.b(tArr, "$this$joinTo");
        wd4.b(a, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        wd4.b(charSequence, "separator");
        wd4.b(charSequence2, "prefix");
        wd4.b(charSequence3, "postfix");
        wd4.b(charSequence4, "truncated");
        a.append(charSequence2);
        int i2 = 0;
        for (T t : tArr) {
            i2++;
            if (i2 > 1) {
                a.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            zf4.a(a, t, jd4);
        }
        if (i >= 0 && i2 > i) {
            a.append(charSequence4);
        }
        a.append(charSequence3);
        return a;
    }

    @DexIgnore
    public static final <T> List<T> c(T[] tArr) {
        wd4.b(tArr, "$this$distinct");
        return wb4.k(g(tArr));
    }

    @DexIgnore
    public static /* synthetic */ String a(Object[] objArr, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, jd4 jd4, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            charSequence = ", ";
        }
        CharSequence charSequence5 = "";
        CharSequence charSequence6 = (i2 & 2) != 0 ? charSequence5 : charSequence2;
        if ((i2 & 4) == 0) {
            charSequence5 = charSequence3;
        }
        int i3 = (i2 & 8) != 0 ? -1 : i;
        if ((i2 & 16) != 0) {
            charSequence4 = "...";
        }
        CharSequence charSequence7 = charSequence4;
        if ((i2 & 32) != 0) {
            jd4 = null;
        }
        return a(objArr, charSequence, charSequence6, charSequence5, i3, charSequence7, jd4);
    }

    @DexIgnore
    public static final <T> String a(T[] tArr, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, jd4<? super T, ? extends CharSequence> jd4) {
        wd4.b(tArr, "$this$joinToString");
        wd4.b(charSequence, "separator");
        wd4.b(charSequence2, "prefix");
        wd4.b(charSequence3, "postfix");
        wd4.b(charSequence4, "truncated");
        StringBuilder sb = new StringBuilder();
        a(tArr, sb, charSequence, charSequence2, charSequence3, i, charSequence4, jd4);
        String sb2 = sb.toString();
        wd4.a((Object) sb2, "joinTo(StringBuilder(), \u2026ed, transform).toString()");
        return sb2;
    }
}
