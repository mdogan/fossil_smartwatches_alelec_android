package com.fossil.blesdk.obfuscated;

import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class il4 {
    @DexIgnore
    public static /* final */ long a; // = sk4.a("kotlinx.coroutines.scheduler.resolution.ns", 100000, 0, 0, 12, (Object) null);
    @DexIgnore
    public static /* final */ int b; // = sk4.a("kotlinx.coroutines.scheduler.offload.threshold", 96, 0, 128, 4, (Object) null);
    @DexIgnore
    public static /* final */ int c; // = sk4.a("kotlinx.coroutines.scheduler.core.pool.size", qe4.a(qk4.a(), 2), 1, 0, 8, (Object) null);
    @DexIgnore
    public static /* final */ int d; // = sk4.a("kotlinx.coroutines.scheduler.max.pool.size", qe4.a(qk4.a() * 128, c, 2097150), 0, 2097150, 4, (Object) null);
    @DexIgnore
    public static /* final */ long e; // = TimeUnit.SECONDS.toNanos(sk4.a("kotlinx.coroutines.scheduler.keep.alive.sec", 5, 0, 0, 12, (Object) null));
    @DexIgnore
    public static jl4 f; // = dl4.a;

    /*
    static {
        int unused = sk4.a("kotlinx.coroutines.scheduler.blocking.parallelism", 16, 0, 0, 12, (Object) null);
    }
    */
}
