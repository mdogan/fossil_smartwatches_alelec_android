package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ki4 implements lh4 {
    @DexIgnore
    public static /* final */ ki4 e; // = new ki4();

    @DexIgnore
    public CoroutineContext A() {
        return EmptyCoroutineContext.INSTANCE;
    }
}
