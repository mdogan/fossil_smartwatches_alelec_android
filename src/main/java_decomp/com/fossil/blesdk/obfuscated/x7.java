package com.fossil.blesdk.obfuscated;

import android.text.SpannableStringBuilder;
import java.util.Locale;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x7 {
    @DexIgnore
    public static /* final */ a8 d; // = b8.c;
    @DexIgnore
    public static /* final */ String e; // = Character.toString(8206);
    @DexIgnore
    public static /* final */ String f; // = Character.toString(8207);
    @DexIgnore
    public static /* final */ x7 g; // = new x7(false, 2, d);
    @DexIgnore
    public static /* final */ x7 h; // = new x7(true, 2, d);
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ a8 c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static /* final */ byte[] f; // = new byte[1792];
        @DexIgnore
        public /* final */ CharSequence a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public char e;

        /*
        static {
            for (int i = 0; i < 1792; i++) {
                f[i] = Character.getDirectionality(i);
            }
        }
        */

        @DexIgnore
        public b(CharSequence charSequence, boolean z) {
            this.a = charSequence;
            this.b = z;
            this.c = charSequence.length();
        }

        @DexIgnore
        public static byte a(char c2) {
            return c2 < 1792 ? f[c2] : Character.getDirectionality(c2);
        }

        @DexIgnore
        public byte b() {
            this.e = this.a.charAt(this.d);
            if (Character.isHighSurrogate(this.e)) {
                int codePointAt = Character.codePointAt(this.a, this.d);
                this.d += Character.charCount(codePointAt);
                return Character.getDirectionality(codePointAt);
            }
            this.d++;
            byte a2 = a(this.e);
            if (!this.b) {
                return a2;
            }
            char c2 = this.e;
            if (c2 == '<') {
                return h();
            }
            return c2 == '&' ? f() : a2;
        }

        @DexIgnore
        public int c() {
            this.d = 0;
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            while (this.d < this.c && i == 0) {
                byte b2 = b();
                if (b2 != 0) {
                    if (b2 == 1 || b2 == 2) {
                        if (i3 == 0) {
                            return 1;
                        }
                    } else if (b2 != 9) {
                        switch (b2) {
                            case 14:
                            case 15:
                                i3++;
                                i2 = -1;
                                continue;
                            case 16:
                            case 17:
                                i3++;
                                i2 = 1;
                                continue;
                            case 18:
                                i3--;
                                i2 = 0;
                                continue;
                        }
                    }
                } else if (i3 == 0) {
                    return -1;
                }
                i = i3;
            }
            if (i == 0) {
                return 0;
            }
            if (i2 != 0) {
                return i2;
            }
            while (this.d > 0) {
                switch (a()) {
                    case 14:
                    case 15:
                        if (i == i3) {
                            return -1;
                        }
                        break;
                    case 16:
                    case 17:
                        if (i == i3) {
                            return 1;
                        }
                        break;
                    case 18:
                        i3++;
                        continue;
                }
                i3--;
            }
            return 0;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:0x002b, code lost:
            r2 = r2 - 1;
         */
        @DexIgnore
        public int d() {
            this.d = this.c;
            int i = 0;
            int i2 = 0;
            while (this.d > 0) {
                byte a2 = a();
                if (a2 != 0) {
                    if (a2 == 1 || a2 == 2) {
                        if (i2 == 0) {
                            return 1;
                        }
                        if (i != 0) {
                        }
                    } else if (a2 != 9) {
                        switch (a2) {
                            case 14:
                            case 15:
                                if (i == i2) {
                                    return -1;
                                }
                                break;
                            case 16:
                            case 17:
                                if (i == i2) {
                                    return 1;
                                }
                                break;
                            case 18:
                                i2++;
                                break;
                            default:
                                if (i != 0) {
                                    break;
                                }
                                break;
                        }
                    } else {
                        continue;
                    }
                } else if (i2 == 0) {
                    return -1;
                } else {
                    if (i != 0) {
                    }
                }
                i = i2;
            }
            return 0;
        }

        @DexIgnore
        public final byte e() {
            char c2;
            int i = this.d;
            do {
                int i2 = this.d;
                if (i2 <= 0) {
                    break;
                }
                CharSequence charSequence = this.a;
                int i3 = i2 - 1;
                this.d = i3;
                this.e = charSequence.charAt(i3);
                c2 = this.e;
                if (c2 == '&') {
                    return 12;
                }
            } while (c2 != ';');
            this.d = i;
            this.e = ';';
            return DateTimeFieldType.HALFDAY_OF_DAY;
        }

        @DexIgnore
        public final byte f() {
            char charAt;
            do {
                int i = this.d;
                if (i >= this.c) {
                    return 12;
                }
                CharSequence charSequence = this.a;
                this.d = i + 1;
                charAt = charSequence.charAt(i);
                this.e = charAt;
            } while (charAt != ';');
            return 12;
        }

        @DexIgnore
        public final byte g() {
            char charAt;
            int i = this.d;
            while (true) {
                int i2 = this.d;
                if (i2 <= 0) {
                    break;
                }
                CharSequence charSequence = this.a;
                int i3 = i2 - 1;
                this.d = i3;
                this.e = charSequence.charAt(i3);
                char c2 = this.e;
                if (c2 == '<') {
                    return 12;
                }
                if (c2 == '>') {
                    break;
                } else if (c2 == '\"' || c2 == '\'') {
                    char c3 = this.e;
                    do {
                        int i4 = this.d;
                        if (i4 <= 0) {
                            break;
                        }
                        CharSequence charSequence2 = this.a;
                        int i5 = i4 - 1;
                        this.d = i5;
                        charAt = charSequence2.charAt(i5);
                        this.e = charAt;
                    } while (charAt != c3);
                }
            }
            this.d = i;
            this.e = '>';
            return DateTimeFieldType.HALFDAY_OF_DAY;
        }

        @DexIgnore
        public final byte h() {
            char charAt;
            int i = this.d;
            while (true) {
                int i2 = this.d;
                if (i2 < this.c) {
                    CharSequence charSequence = this.a;
                    this.d = i2 + 1;
                    this.e = charSequence.charAt(i2);
                    char c2 = this.e;
                    if (c2 == '>') {
                        return 12;
                    }
                    if (c2 == '\"' || c2 == '\'') {
                        char c3 = this.e;
                        do {
                            int i3 = this.d;
                            if (i3 >= this.c) {
                                break;
                            }
                            CharSequence charSequence2 = this.a;
                            this.d = i3 + 1;
                            charAt = charSequence2.charAt(i3);
                            this.e = charAt;
                        } while (charAt != c3);
                    }
                } else {
                    this.d = i;
                    this.e = '<';
                    return DateTimeFieldType.HALFDAY_OF_DAY;
                }
            }
        }

        @DexIgnore
        public byte a() {
            this.e = this.a.charAt(this.d - 1);
            if (Character.isLowSurrogate(this.e)) {
                int codePointBefore = Character.codePointBefore(this.a, this.d);
                this.d -= Character.charCount(codePointBefore);
                return Character.getDirectionality(codePointBefore);
            }
            this.d--;
            byte a2 = a(this.e);
            if (!this.b) {
                return a2;
            }
            char c2 = this.e;
            if (c2 == '>') {
                return g();
            }
            return c2 == ';' ? e() : a2;
        }
    }

    @DexIgnore
    public x7(boolean z, int i, a8 a8Var) {
        this.a = z;
        this.b = i;
        this.c = a8Var;
    }

    @DexIgnore
    public static x7 b() {
        return new a().a();
    }

    @DexIgnore
    public static int c(CharSequence charSequence) {
        return new b(charSequence, false).d();
    }

    @DexIgnore
    public boolean a() {
        return (this.b & 2) != 0;
    }

    @DexIgnore
    public final String a(CharSequence charSequence, a8 a8Var) {
        boolean a2 = a8Var.a(charSequence, 0, charSequence.length());
        if (!this.a && (a2 || c(charSequence) == 1)) {
            return e;
        }
        if (this.a) {
            return (!a2 || c(charSequence) == -1) ? f : "";
        }
        return "";
    }

    @DexIgnore
    public final String b(CharSequence charSequence, a8 a8Var) {
        boolean a2 = a8Var.a(charSequence, 0, charSequence.length());
        if (!this.a && (a2 || b(charSequence) == 1)) {
            return e;
        }
        if (this.a) {
            return (!a2 || b(charSequence) == -1) ? f : "";
        }
        return "";
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public int b;
        @DexIgnore
        public a8 c;

        @DexIgnore
        public a() {
            a(x7.a(Locale.getDefault()));
        }

        @DexIgnore
        public static x7 b(boolean z) {
            return z ? x7.h : x7.g;
        }

        @DexIgnore
        public final void a(boolean z) {
            this.a = z;
            this.c = x7.d;
            this.b = 2;
        }

        @DexIgnore
        public x7 a() {
            if (this.b == 2 && this.c == x7.d) {
                return b(this.a);
            }
            return new x7(this.a, this.b, this.c);
        }
    }

    @DexIgnore
    public static int b(CharSequence charSequence) {
        return new b(charSequence, false).c();
    }

    @DexIgnore
    public CharSequence a(CharSequence charSequence, a8 a8Var, boolean z) {
        if (charSequence == null) {
            return null;
        }
        boolean a2 = a8Var.a(charSequence, 0, charSequence.length());
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (a() && z) {
            spannableStringBuilder.append(b(charSequence, a2 ? b8.b : b8.a));
        }
        if (a2 != this.a) {
            spannableStringBuilder.append(a2 ? (char) 8235 : 8234);
            spannableStringBuilder.append(charSequence);
            spannableStringBuilder.append(8236);
        } else {
            spannableStringBuilder.append(charSequence);
        }
        if (z) {
            spannableStringBuilder.append(a(charSequence, a2 ? b8.b : b8.a));
        }
        return spannableStringBuilder;
    }

    @DexIgnore
    public CharSequence a(CharSequence charSequence) {
        return a(charSequence, this.c, true);
    }

    @DexIgnore
    public static boolean a(Locale locale) {
        return c8.b(locale) == 1;
    }
}
