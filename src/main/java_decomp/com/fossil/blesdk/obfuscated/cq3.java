package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface cq3 extends hq2<bq3> {
    @DexIgnore
    void B();

    @DexIgnore
    void a();

    @DexIgnore
    void a(int i, int i2);

    @DexIgnore
    void b();

    @DexIgnore
    void j(String str);

    @DexIgnore
    void k(String str);

    @DexIgnore
    void n();

    @DexIgnore
    void o();

    @DexIgnore
    void o(String str);
}
