package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface vl0 extends IInterface {
    @DexIgnore
    tn0 zzb() throws RemoteException;

    @DexIgnore
    int zzc() throws RemoteException;
}
