package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oi1 implements Callable<byte[]> {
    @DexIgnore
    public /* final */ /* synthetic */ ig1 e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ ai1 g;

    @DexIgnore
    public oi1(ai1 ai1, ig1 ig1, String str) {
        this.g = ai1;
        this.e = ig1;
        this.f = str;
    }

    @DexIgnore
    public final /* synthetic */ Object call() throws Exception {
        this.g.e.y();
        this.g.e.q().a(this.e, this.f);
        throw null;
    }
}
