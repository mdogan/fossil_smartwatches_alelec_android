package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.uq;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mp<DataType> implements uq.b {
    @DexIgnore
    public /* final */ io<DataType> a;
    @DexIgnore
    public /* final */ DataType b;
    @DexIgnore
    public /* final */ mo c;

    @DexIgnore
    public mp(io<DataType> ioVar, DataType datatype, mo moVar) {
        this.a = ioVar;
        this.b = datatype;
        this.c = moVar;
    }

    @DexIgnore
    public boolean a(File file) {
        return this.a.a(this.b, file, this.c);
    }
}
