package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import com.crashlytics.android.answers.SessionEvent;
import com.fossil.blesdk.obfuscated.mx;
import io.fabric.sdk.android.services.common.IdManager;
import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class by implements mx.b {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ gx b;
    @DexIgnore
    public /* final */ p44 c;
    @DexIgnore
    public /* final */ mx d;
    @DexIgnore
    public /* final */ jx e;

    @DexIgnore
    public by(gx gxVar, p44 p44, mx mxVar, jx jxVar, long j) {
        this.b = gxVar;
        this.c = p44;
        this.d = mxVar;
        this.e = jxVar;
        this.a = j;
    }

    @DexIgnore
    public static by a(w44 w44, Context context, IdManager idManager, String str, String str2, long j) {
        Context context2 = context;
        IdManager idManager2 = idManager;
        fy fyVar = new fy(context, idManager, str, str2);
        hx hxVar = new hx(context, new g74(w44));
        z64 z64 = new z64(r44.g());
        p44 p44 = new p44(context);
        ScheduledExecutorService b2 = r54.b("Answers Events Handler");
        mx mxVar = new mx(b2);
        return new by(new gx(w44, context, hxVar, fyVar, z64, b2, new sx(context)), p44, mxVar, jx.a(context), j);
    }

    @DexIgnore
    public void b() {
        this.c.a();
        this.b.a();
    }

    @DexIgnore
    public void c() {
        this.b.b();
        this.c.a(new ix(this, this.d));
        this.d.a((mx.b) this);
        if (d()) {
            a(this.a);
            this.e.b();
        }
    }

    @DexIgnore
    public boolean d() {
        return !this.e.a();
    }

    @DexIgnore
    public void a(nx nxVar) {
        z44 g = r44.g();
        g.d("Answers", "Logged custom event: " + nxVar);
        this.b.a(SessionEvent.a(nxVar));
    }

    @DexIgnore
    public void a(String str, String str2) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            r44.g().d("Answers", "Logged crash");
            this.b.c(SessionEvent.a(str, str2));
            return;
        }
        throw new IllegalStateException("onCrash called from main thread!!!");
    }

    @DexIgnore
    public void a(long j) {
        r44.g().d("Answers", "Logged install");
        this.b.b(SessionEvent.a(j));
    }

    @DexIgnore
    public void a(Activity activity, SessionEvent.Type type) {
        z44 g = r44.g();
        g.d("Answers", "Logged lifecycle event: " + type.name());
        this.b.a(SessionEvent.a(type, activity));
    }

    @DexIgnore
    public void a() {
        r44.g().d("Answers", "Flush events when app is backgrounded");
        this.b.c();
    }

    @DexIgnore
    public void a(k74 k74, String str) {
        this.d.a(k74.i);
        this.b.a(k74, str);
    }
}
