package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ek4 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater e;
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater f;
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater g;
    @DexIgnore
    public volatile Object _next; // = this;
    @DexIgnore
    public volatile Object _prev; // = this;
    @DexIgnore
    public volatile Object _removedRef; // = null;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends yj4<ek4> {
        @DexIgnore
        public ek4 b;
        @DexIgnore
        public /* final */ ek4 c;

        @DexIgnore
        public a(ek4 ek4) {
            wd4.b(ek4, "newNode");
            this.c = ek4;
        }

        @DexIgnore
        public void a(ek4 ek4, Object obj) {
            wd4.b(ek4, "affected");
            boolean z = obj == null;
            ek4 ek42 = z ? this.c : this.b;
            if (ek42 != null && ek4.e.compareAndSet(ek4, this, ek42) && z) {
                ek4 ek43 = this.c;
                ek4 ek44 = this.b;
                if (ek44 != null) {
                    ek43.b(ek44);
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }
    }

    /*
    static {
        Class<Object> cls = Object.class;
        Class<ek4> cls2 = ek4.class;
        e = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_next");
        f = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_prev");
        g = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_removedRef");
    }
    */

    @DexIgnore
    public final ek4 b() {
        ek4 ek4 = this;
        while (!(ek4 instanceof ck4)) {
            ek4 = ek4.d();
            if (oh4.a()) {
                if (!(ek4 != this)) {
                    throw new AssertionError();
                }
            }
        }
        return ek4;
    }

    @DexIgnore
    public final void c(ek4 ek4) {
        g();
        ek4.a(dk4.a(this._prev), (kk4) null);
    }

    @DexIgnore
    public final ek4 d() {
        return dk4.a(c());
    }

    @DexIgnore
    public final Object e() {
        while (true) {
            Object obj = this._prev;
            if (obj instanceof lk4) {
                return obj;
            }
            if (obj != null) {
                ek4 ek4 = (ek4) obj;
                if (ek4.c() == this) {
                    return obj;
                }
                a(ek4, (kk4) null);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        }
    }

    @DexIgnore
    public final ek4 f() {
        return dk4.a(e());
    }

    @DexIgnore
    public final void g() {
        Object c;
        ek4 i = i();
        Object obj = this._next;
        if (obj != null) {
            ek4 ek4 = ((lk4) obj).a;
            while (true) {
                ek4 ek42 = null;
                while (true) {
                    Object c2 = ek4.c();
                    if (c2 instanceof lk4) {
                        ek4.i();
                        ek4 = ((lk4) c2).a;
                    } else {
                        c = i.c();
                        if (c instanceof lk4) {
                            if (ek42 != null) {
                                break;
                            }
                            i = dk4.a(i._prev);
                        } else if (c != this) {
                            if (c != null) {
                                ek4 ek43 = (ek4) c;
                                if (ek43 != ek4) {
                                    ek4 ek44 = ek43;
                                    ek42 = i;
                                    i = ek44;
                                } else {
                                    return;
                                }
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                            }
                        } else if (e.compareAndSet(i, this, ek4)) {
                            return;
                        }
                    }
                }
                i.i();
                e.compareAndSet(ek42, i, ((lk4) c).a);
                i = ek42;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Removed");
        }
    }

    @DexIgnore
    public final boolean h() {
        return c() instanceof lk4;
    }

    @DexIgnore
    public final ek4 i() {
        Object obj;
        ek4 ek4;
        do {
            obj = this._prev;
            if (obj instanceof lk4) {
                return ((lk4) obj).a;
            }
            if (obj == this) {
                ek4 = b();
            } else if (obj != null) {
                ek4 = (ek4) obj;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        } while (!f.compareAndSet(this, obj, ek4.l()));
        return (ek4) obj;
    }

    @DexIgnore
    public boolean j() {
        Object c;
        ek4 ek4;
        do {
            c = c();
            if ((c instanceof lk4) || c == this) {
                return false;
            }
            if (c != null) {
                ek4 = (ek4) c;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        } while (!e.compareAndSet(this, c, ek4.l()));
        c(ek4);
        return true;
    }

    @DexIgnore
    public final ek4 k() {
        while (true) {
            Object c = c();
            if (c != null) {
                ek4 ek4 = (ek4) c;
                if (ek4 == this) {
                    return null;
                }
                if (ek4.j()) {
                    return ek4;
                }
                ek4.g();
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        }
    }

    @DexIgnore
    public final lk4 l() {
        lk4 lk4 = (lk4) this._removedRef;
        if (lk4 != null) {
            return lk4;
        }
        lk4 lk42 = new lk4(this);
        g.lazySet(this, lk42);
        return lk42;
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + '@' + Integer.toHexString(System.identityHashCode(this));
    }

    @DexIgnore
    public final boolean a(ek4 ek4) {
        wd4.b(ek4, "node");
        f.lazySet(ek4, this);
        e.lazySet(ek4, this);
        while (c() == this) {
            if (e.compareAndSet(this, this, ek4)) {
                ek4.b(this);
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final Object c() {
        while (true) {
            Object obj = this._next;
            if (!(obj instanceof kk4)) {
                return obj;
            }
            ((kk4) obj).a(this);
        }
    }

    @DexIgnore
    public final void b(ek4 ek4) {
        Object obj;
        do {
            obj = ek4._prev;
            if ((obj instanceof lk4) || c() != ek4) {
                return;
            }
        } while (!f.compareAndSet(ek4, obj, this));
        if (!(c() instanceof lk4)) {
            return;
        }
        if (obj != null) {
            ek4.a((ek4) obj, (kk4) null);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public final int a(ek4 ek4, ek4 ek42, a aVar) {
        wd4.b(ek4, "node");
        wd4.b(ek42, "next");
        wd4.b(aVar, "condAdd");
        f.lazySet(ek4, this);
        e.lazySet(ek4, ek42);
        aVar.b = ek42;
        if (!e.compareAndSet(this, ek42, aVar)) {
            return 0;
        }
        return aVar.a(this) == null ? 1 : 2;
    }

    @DexIgnore
    public final ek4 a(ek4 ek4, kk4 kk4) {
        Object obj;
        while (true) {
            ek4 ek42 = null;
            while (true) {
                obj = ek4._next;
                if (obj == kk4) {
                    return ek4;
                }
                if (obj instanceof kk4) {
                    ((kk4) obj).a(ek4);
                } else if (!(obj instanceof lk4)) {
                    Object obj2 = this._prev;
                    if (obj2 instanceof lk4) {
                        return null;
                    }
                    if (obj != this) {
                        if (obj != null) {
                            ek42 = ek4;
                            ek4 = (ek4) obj;
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                        }
                    } else if (obj2 == ek4) {
                        return null;
                    } else {
                        if (f.compareAndSet(this, obj2, ek4) && !(ek4._prev instanceof lk4)) {
                            return null;
                        }
                    }
                } else if (ek42 != null) {
                    break;
                } else {
                    ek4 = dk4.a(ek4._prev);
                }
            }
            ek4.i();
            e.compareAndSet(ek42, ek4, ((lk4) obj).a);
            ek4 = ek42;
        }
    }
}
