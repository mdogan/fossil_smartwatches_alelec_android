package com.fossil.blesdk.obfuscated;

import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobServiceEngine;
import android.app.job.JobWorkItem;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class a6 extends Service {
    @DexIgnore
    public static /* final */ HashMap<ComponentName, h> k; // = new HashMap<>();
    @DexIgnore
    public b e;
    @DexIgnore
    public h f;
    @DexIgnore
    public a g;
    @DexIgnore
    public boolean h; // = false;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public /* final */ ArrayList<d> j;

    @DexIgnore
    public interface b {
        @DexIgnore
        IBinder a();

        @DexIgnore
        e b();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends h {
        @DexIgnore
        public /* final */ PowerManager.WakeLock d;
        @DexIgnore
        public /* final */ PowerManager.WakeLock e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;

        @DexIgnore
        public c(Context context, ComponentName componentName) {
            super(context, componentName);
            context.getApplicationContext();
            PowerManager powerManager = (PowerManager) context.getSystemService("power");
            this.d = powerManager.newWakeLock(1, componentName.getClassName() + ":launch");
            this.d.setReferenceCounted(false);
            this.e = powerManager.newWakeLock(1, componentName.getClassName() + ":run");
            this.e.setReferenceCounted(false);
        }

        @DexIgnore
        public void a() {
            synchronized (this) {
                if (this.g) {
                    if (this.f) {
                        this.d.acquire(60000);
                    }
                    this.g = false;
                    this.e.release();
                }
            }
        }

        @DexIgnore
        public void b() {
            synchronized (this) {
                if (!this.g) {
                    this.g = true;
                    this.e.acquire(600000);
                    this.d.release();
                }
            }
        }

        @DexIgnore
        public void c() {
            synchronized (this) {
                this.f = false;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements e {
        @DexIgnore
        public /* final */ Intent a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public d(Intent intent, int i) {
            this.a = intent;
            this.b = i;
        }

        @DexIgnore
        public void a() {
            a6.this.stopSelf(this.b);
        }

        @DexIgnore
        public Intent getIntent() {
            return this.a;
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a();

        @DexIgnore
        Intent getIntent();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends JobServiceEngine implements b {
        @DexIgnore
        public /* final */ a6 a;
        @DexIgnore
        public /* final */ Object b; // = new Object();
        @DexIgnore
        public JobParameters c;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a implements e {
            @DexIgnore
            public /* final */ JobWorkItem a;

            @DexIgnore
            public a(JobWorkItem jobWorkItem) {
                this.a = jobWorkItem;
            }

            @DexIgnore
            public void a() {
                synchronized (f.this.b) {
                    if (f.this.c != null) {
                        f.this.c.completeWork(this.a);
                    }
                }
            }

            @DexIgnore
            public Intent getIntent() {
                return this.a.getIntent();
            }
        }

        @DexIgnore
        public f(a6 a6Var) {
            super(a6Var);
            this.a = a6Var;
        }

        @DexIgnore
        public IBinder a() {
            return getBinder();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0013, code lost:
            r1.getIntent().setExtrasClassLoader(r3.a.getClassLoader());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
            return new com.fossil.blesdk.obfuscated.a6.f.a(r3, r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0026, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
            if (r1 == null) goto L_0x0026;
         */
        @DexIgnore
        public e b() {
            synchronized (this.b) {
                if (this.c == null) {
                    return null;
                }
                JobWorkItem dequeueWork = this.c.dequeueWork();
            }
        }

        @DexIgnore
        public boolean onStartJob(JobParameters jobParameters) {
            this.c = jobParameters;
            this.a.a(false);
            return true;
        }

        @DexIgnore
        public boolean onStopJob(JobParameters jobParameters) {
            boolean b2 = this.a.b();
            synchronized (this.b) {
                this.c = null;
            }
            return b2;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends h {
        @DexIgnore
        public g(Context context, ComponentName componentName, int i) {
            super(context, componentName);
            a(i);
            new JobInfo.Builder(i, this.a).setOverrideDeadline(0).build();
            JobScheduler jobScheduler = (JobScheduler) context.getApplicationContext().getSystemService("jobscheduler");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class h {
        @DexIgnore
        public /* final */ ComponentName a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public int c;

        @DexIgnore
        public h(Context context, ComponentName componentName) {
            this.a = componentName;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(int i) {
            if (!this.b) {
                this.b = true;
                this.c = i;
            } else if (this.c != i) {
                throw new IllegalArgumentException("Given job ID " + i + " is different than previous " + this.c);
            }
        }

        @DexIgnore
        public void b() {
        }

        @DexIgnore
        public void c() {
        }
    }

    @DexIgnore
    public a6() {
        if (Build.VERSION.SDK_INT >= 26) {
            this.j = null;
        } else {
            this.j = new ArrayList<>();
        }
    }

    @DexIgnore
    public static h a(Context context, ComponentName componentName, boolean z, int i2) {
        h hVar;
        h hVar2 = k.get(componentName);
        if (hVar2 != null) {
            return hVar2;
        }
        if (Build.VERSION.SDK_INT < 26) {
            hVar = new c(context, componentName);
        } else if (z) {
            hVar = new g(context, componentName, i2);
        } else {
            throw new IllegalArgumentException("Can't be here without a job id");
        }
        h hVar3 = hVar;
        k.put(componentName, hVar3);
        return hVar3;
    }

    @DexIgnore
    public abstract void a(Intent intent);

    @DexIgnore
    public boolean b() {
        a aVar = this.g;
        if (aVar != null) {
            aVar.cancel(this.h);
        }
        return c();
    }

    @DexIgnore
    public boolean c() {
        return true;
    }

    @DexIgnore
    public void d() {
        ArrayList<d> arrayList = this.j;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.g = null;
                if (this.j != null && this.j.size() > 0) {
                    a(false);
                } else if (!this.i) {
                    this.f.a();
                }
            }
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        b bVar = this.e;
        if (bVar != null) {
            return bVar.a();
        }
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 26) {
            this.e = new f(this);
            this.f = null;
            return;
        }
        this.e = null;
        this.f = a(this, new ComponentName(this, a6.class), false, 0);
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        ArrayList<d> arrayList = this.j;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.i = true;
                this.f.a();
            }
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        if (this.j == null) {
            return 2;
        }
        this.f.c();
        synchronized (this.j) {
            ArrayList<d> arrayList = this.j;
            if (intent == null) {
                intent = new Intent();
            }
            arrayList.add(new d(intent, i3));
            a(true);
        }
        return 3;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends AsyncTask<Void, Void, Void> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            while (true) {
                e a2 = a6.this.a();
                if (a2 == null) {
                    return null;
                }
                a6.this.a(a2.getIntent());
                a2.a();
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void onPostExecute(Void voidR) {
            a6.this.d();
        }

        @DexIgnore
        /* renamed from: a */
        public void onCancelled(Void voidR) {
            a6.this.d();
        }
    }

    @DexIgnore
    public void a(boolean z) {
        if (this.g == null) {
            this.g = new a();
            h hVar = this.f;
            if (hVar != null && z) {
                hVar.b();
            }
            this.g.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    @DexIgnore
    public e a() {
        b bVar = this.e;
        if (bVar != null) {
            return bVar.b();
        }
        synchronized (this.j) {
            if (this.j.size() <= 0) {
                return null;
            }
            e remove = this.j.remove(0);
            return remove;
        }
    }
}
