package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qc4 implements kc4<Object> {
    @DexIgnore
    public static /* final */ qc4 e; // = new qc4();

    @DexIgnore
    public CoroutineContext getContext() {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    @DexIgnore
    public String toString() {
        return "This continuation is already complete";
    }
}
