package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class nf2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FloatingActionButton A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ TextInputEditText r;
    @DexIgnore
    public /* final */ TextInputEditText s;
    @DexIgnore
    public /* final */ TextInputLayout t;
    @DexIgnore
    public /* final */ TextInputLayout u;
    @DexIgnore
    public /* final */ ImageView v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ ImageView x;
    @DexIgnore
    public /* final */ ImageView y;
    @DexIgnore
    public /* final */ FloatingActionButton z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nf2(Object obj, View view, int i, Barrier barrier, Barrier barrier2, FlexibleButton flexibleButton, TextInputEditText textInputEditText, TextInputEditText textInputEditText2, TextInputLayout textInputLayout, TextInputLayout textInputLayout2, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, FloatingActionButton floatingActionButton, FloatingActionButton floatingActionButton2, ConstraintLayout constraintLayout, ScrollView scrollView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = textInputEditText;
        this.s = textInputEditText2;
        this.t = textInputLayout;
        this.u = textInputLayout2;
        this.v = imageView;
        this.w = imageView2;
        this.x = imageView3;
        this.y = imageView4;
        this.z = floatingActionButton;
        this.A = floatingActionButton2;
        this.B = flexibleTextView;
        this.C = flexibleTextView2;
        this.D = flexibleTextView3;
    }
}
