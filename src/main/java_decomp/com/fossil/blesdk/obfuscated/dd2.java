package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.indicator.CustomPageIndicator;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class dd2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ CustomPageIndicator r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ RecyclerViewPager u;
    @DexIgnore
    public /* final */ TextView v;

    @DexIgnore
    public dd2(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, CustomPageIndicator customPageIndicator, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, ImageView imageView, ProgressBar progressBar, RecyclerViewPager recyclerViewPager, FlexibleTextView flexibleTextView4, TextView textView, FlexibleTextView flexibleTextView5) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = customPageIndicator;
        this.s = flexibleTextView3;
        this.t = imageView;
        this.u = recyclerViewPager;
        this.v = textView;
    }
}
