package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.me;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ie<T> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ me.d<T> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public static /* final */ Object d; // = new Object();
        @DexIgnore
        public static Executor e;
        @DexIgnore
        public Executor a;
        @DexIgnore
        public Executor b;
        @DexIgnore
        public /* final */ me.d<T> c;

        @DexIgnore
        public a(me.d<T> dVar) {
            this.c = dVar;
        }

        @DexIgnore
        public ie<T> a() {
            if (this.b == null) {
                synchronized (d) {
                    if (e == null) {
                        e = Executors.newFixedThreadPool(2);
                    }
                }
                this.b = e;
            }
            return new ie<>(this.a, this.b, this.c);
        }
    }

    @DexIgnore
    public ie(Executor executor, Executor executor2, me.d<T> dVar) {
        this.a = executor2;
        this.b = dVar;
    }

    @DexIgnore
    public Executor a() {
        return this.a;
    }

    @DexIgnore
    public me.d<T> b() {
        return this.b;
    }
}
