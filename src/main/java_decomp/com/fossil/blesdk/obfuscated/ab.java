package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ab implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ab> CREATOR; // = new a();
    @DexIgnore
    public bb[] e;
    @DexIgnore
    public int[] f;
    @DexIgnore
    public xa[] g;
    @DexIgnore
    public int h; // = -1;
    @DexIgnore
    public int i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<ab> {
        @DexIgnore
        public ab createFromParcel(Parcel parcel) {
            return new ab(parcel);
        }

        @DexIgnore
        public ab[] newArray(int i) {
            return new ab[i];
        }
    }

    @DexIgnore
    public ab() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeTypedArray(this.e, i2);
        parcel.writeIntArray(this.f);
        parcel.writeTypedArray(this.g, i2);
        parcel.writeInt(this.h);
        parcel.writeInt(this.i);
    }

    @DexIgnore
    public ab(Parcel parcel) {
        this.e = (bb[]) parcel.createTypedArray(bb.CREATOR);
        this.f = parcel.createIntArray();
        this.g = (xa[]) parcel.createTypedArray(xa.CREATOR);
        this.h = parcel.readInt();
        this.i = parcel.readInt();
    }
}
