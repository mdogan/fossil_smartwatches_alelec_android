package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class cf extends RecyclerView.o {
    @DexIgnore
    public RecyclerView a;
    @DexIgnore
    public Scroller b;
    @DexIgnore
    public /* final */ RecyclerView.q c; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.q {
        @DexIgnore
        public boolean a; // = false;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            super.onScrollStateChanged(recyclerView, i);
            if (i == 0 && this.a) {
                this.a = false;
                cf.this.c();
            }
        }

        @DexIgnore
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            if (i != 0 || i2 != 0) {
                this.a = true;
            }
        }
    }

    @DexIgnore
    public abstract int a(RecyclerView.m mVar, int i, int i2);

    @DexIgnore
    public boolean a(int i, int i2) {
        RecyclerView.m layoutManager = this.a.getLayoutManager();
        if (layoutManager == null || this.a.getAdapter() == null) {
            return false;
        }
        int minFlingVelocity = this.a.getMinFlingVelocity();
        if ((Math.abs(i2) > minFlingVelocity || Math.abs(i) > minFlingVelocity) && b(layoutManager, i, i2)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public abstract int[] a(RecyclerView.m mVar, View view);

    @DexIgnore
    public final void b() throws IllegalStateException {
        if (this.a.getOnFlingListener() == null) {
            this.a.a(this.c);
            this.a.setOnFlingListener(this);
            return;
        }
        throw new IllegalStateException("An instance of OnFlingListener already set.");
    }

    @DexIgnore
    public abstract View c(RecyclerView.m mVar);

    @DexIgnore
    public void c() {
        RecyclerView recyclerView = this.a;
        if (recyclerView != null) {
            RecyclerView.m layoutManager = recyclerView.getLayoutManager();
            if (layoutManager != null) {
                View c2 = c(layoutManager);
                if (c2 != null) {
                    int[] a2 = a(layoutManager, c2);
                    if (a2[0] != 0 || a2[1] != 0) {
                        this.a.j(a2[0], a2[1]);
                    }
                }
            }
        }
    }

    @DexIgnore
    public int[] b(int i, int i2) {
        this.b.fling(0, 0, i, i2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
        return new int[]{this.b.getFinalX(), this.b.getFinalY()};
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends te {
        @DexIgnore
        public b(Context context) {
            super(context);
        }

        @DexIgnore
        public void a(View view, RecyclerView.State state, RecyclerView.v.a aVar) {
            cf cfVar = cf.this;
            RecyclerView recyclerView = cfVar.a;
            if (recyclerView != null) {
                int[] a = cfVar.a(recyclerView.getLayoutManager(), view);
                int i = a[0];
                int i2 = a[1];
                int d = d(Math.max(Math.abs(i), Math.abs(i2)));
                if (d > 0) {
                    aVar.a(i, i2, d, this.j);
                }
            }
        }

        @DexIgnore
        public float a(DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }
    }

    @DexIgnore
    public void a(RecyclerView recyclerView) throws IllegalStateException {
        RecyclerView recyclerView2 = this.a;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                a();
            }
            this.a = recyclerView;
            if (this.a != null) {
                b();
                this.b = new Scroller(this.a.getContext(), new DecelerateInterpolator());
                c();
            }
        }
    }

    @DexIgnore
    public final boolean b(RecyclerView.m mVar, int i, int i2) {
        if (!(mVar instanceof RecyclerView.v.b)) {
            return false;
        }
        RecyclerView.v a2 = a(mVar);
        if (a2 == null) {
            return false;
        }
        int a3 = a(mVar, i, i2);
        if (a3 == -1) {
            return false;
        }
        a2.c(a3);
        mVar.b(a2);
        return true;
    }

    @DexIgnore
    public final void a() {
        this.a.b(this.c);
        this.a.setOnFlingListener((RecyclerView.o) null);
    }

    @DexIgnore
    @Deprecated
    public te b(RecyclerView.m mVar) {
        if (!(mVar instanceof RecyclerView.v.b)) {
            return null;
        }
        return new b(this.a.getContext());
    }

    @DexIgnore
    public RecyclerView.v a(RecyclerView.m mVar) {
        return b(mVar);
    }
}
