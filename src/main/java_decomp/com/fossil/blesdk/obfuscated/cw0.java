package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cw0 {
    @DexIgnore
    public static /* final */ aw0 a; // = c();
    @DexIgnore
    public static /* final */ aw0 b; // = new bw0();

    @DexIgnore
    public static aw0 a() {
        return a;
    }

    @DexIgnore
    public static aw0 b() {
        return b;
    }

    @DexIgnore
    public static aw0 c() {
        try {
            return (aw0) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
