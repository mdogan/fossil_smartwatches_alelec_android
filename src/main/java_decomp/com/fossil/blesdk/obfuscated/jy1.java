package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class jy1 implements Runnable {
    @DexIgnore
    public /* final */ iy1 e;
    @DexIgnore
    public /* final */ Intent f;

    @DexIgnore
    public jy1(iy1 iy1, Intent intent) {
        this.e = iy1;
        this.f = intent;
    }

    @DexIgnore
    public final void run() {
        iy1 iy1 = this.e;
        String action = this.f.getAction();
        StringBuilder sb = new StringBuilder(String.valueOf(action).length() + 61);
        sb.append("Service took too long to process intent: ");
        sb.append(action);
        sb.append(" App may get closed.");
        Log.w("EnhancedIntentService", sb.toString());
        iy1.a();
    }
}
