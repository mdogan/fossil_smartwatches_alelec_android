package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.phase.TransmitDataPhase;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h60 extends TransmitDataPhase {
    @DexIgnore
    public /* final */ byte[] P;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ h60(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, boolean z, short s, byte[] bArr, float f, String str, int i, rd4 rd4) {
        this(peripheral, aVar, phaseId, z, s, bArr, r9, r10);
        String str2;
        int i2 = i;
        float f2 = (i2 & 64) != 0 ? 0.001f : f;
        if ((i2 & 128) != 0) {
            String uuid = UUID.randomUUID().toString();
            wd4.a((Object) uuid, "UUID.randomUUID().toString()");
            str2 = uuid;
        } else {
            str2 = str;
        }
    }

    @DexIgnore
    public byte[] F() {
        return this.P;
    }

    @DexIgnore
    public final byte[] N() {
        return this.P;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public h60(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, boolean z, short s, byte[] bArr, float f, String str) {
        super(peripheral, aVar, phaseId, z, s, f, r9);
        byte[] bArr2 = bArr;
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(phaseId, "id");
        wd4.b(bArr2, "fileData");
        String str2 = str;
        wd4.b(str2, "phaseUuid");
        this.P = bArr2;
    }
}
