package com.fossil.blesdk.obfuscated;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gm2 {
    @DexIgnore
    public Vector<km2> a;
    @DexIgnore
    public Map<String, km2> b;
    @DexIgnore
    public Map<String, km2> c;
    @DexIgnore
    public Map<String, km2> d;
    @DexIgnore
    public Map<String, km2> e;

    @DexIgnore
    public gm2() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.a = new Vector<>();
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = new HashMap();
    }

    @DexIgnore
    public boolean a(km2 km2) {
        if (km2 != null) {
            b(km2);
            return this.a.add(km2);
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void b(km2 km2) {
        byte[] a2 = km2.a();
        if (a2 != null) {
            this.b.put(new String(a2), km2);
        }
        byte[] b2 = km2.b();
        if (b2 != null) {
            this.c.put(new String(b2), km2);
        }
        byte[] f = km2.f();
        if (f != null) {
            this.d.put(new String(f), km2);
        }
        byte[] e2 = km2.e();
        if (e2 != null) {
            this.e.put(new String(e2), km2);
        }
    }

    @DexIgnore
    public void a(int i, km2 km2) {
        if (km2 != null) {
            b(km2);
            this.a.add(i, km2);
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public void a() {
        this.a.clear();
    }

    @DexIgnore
    public km2 a(int i) {
        return this.a.get(i);
    }
}
