package com.fossil.blesdk.obfuscated;

import android.os.Looper;
import com.fossil.blesdk.obfuscated.jj0;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vf0 implements jj0.c {
    @DexIgnore
    public /* final */ WeakReference<tf0> a;
    @DexIgnore
    public /* final */ ee0<?> b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public vf0(tf0 tf0, ee0<?> ee0, boolean z) {
        this.a = new WeakReference<>(tf0);
        this.b = ee0;
        this.c = z;
    }

    @DexIgnore
    public final void a(vd0 vd0) {
        tf0 tf0 = (tf0) this.a.get();
        if (tf0 != null) {
            ck0.b(Looper.myLooper() == tf0.a.r.f(), "onReportServiceBinding must be called on the GoogleApiClient handler thread");
            tf0.b.lock();
            try {
                if (tf0.a(0)) {
                    if (!vd0.L()) {
                        tf0.b(vd0, this.b, this.c);
                    }
                    if (tf0.d()) {
                        tf0.e();
                    }
                    tf0.b.unlock();
                }
            } finally {
                tf0.b.unlock();
            }
        }
    }
}
