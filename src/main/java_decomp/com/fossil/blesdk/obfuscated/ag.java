package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.vd;
import java.util.Collections;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ag<T> extends vd<T> {
    @DexIgnore
    public /* final */ String mCountQuery;
    @DexIgnore
    public /* final */ RoomDatabase mDb;
    @DexIgnore
    public /* final */ boolean mInTransaction;
    @DexIgnore
    public /* final */ String mLimitOffsetQuery;
    @DexIgnore
    public /* final */ qf.c mObserver;
    @DexIgnore
    public /* final */ vf mSourceQuery;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends qf.c {
        @DexIgnore
        public a(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            ag.this.invalidate();
        }
    }

    @DexIgnore
    public ag(RoomDatabase roomDatabase, kg kgVar, boolean z, String... strArr) {
        this(roomDatabase, vf.a(kgVar), z, strArr);
    }

    @DexIgnore
    private vf getSQLiteQuery(int i, int i2) {
        vf b = vf.b(this.mLimitOffsetQuery, this.mSourceQuery.a() + 2);
        b.a(this.mSourceQuery);
        b.b(b.a() - 1, (long) i2);
        b.b(b.a(), (long) i);
        return b;
    }

    @DexIgnore
    public abstract List<T> convertRows(Cursor cursor);

    @DexIgnore
    public int countItems() {
        vf b = vf.b(this.mCountQuery, this.mSourceQuery.a());
        b.a(this.mSourceQuery);
        Cursor query = this.mDb.query(b);
        try {
            if (query.moveToFirst()) {
                return query.getInt(0);
            }
            query.close();
            b.c();
            return 0;
        } finally {
            query.close();
            b.c();
        }
    }

    @DexIgnore
    public boolean isInvalid() {
        this.mDb.getInvalidationTracker().c();
        return super.isInvalid();
    }

    @DexIgnore
    public void loadInitial(vd.d dVar, vd.b<T> bVar) {
        vf vfVar;
        List list;
        int i;
        List emptyList = Collections.emptyList();
        this.mDb.beginTransaction();
        Cursor cursor = null;
        try {
            int countItems = countItems();
            if (countItems != 0) {
                i = vd.computeInitialLoadPosition(dVar, countItems);
                vfVar = getSQLiteQuery(i, vd.computeInitialLoadSize(dVar, i, countItems));
                try {
                    cursor = this.mDb.query(vfVar);
                    list = convertRows(cursor);
                    this.mDb.setTransactionSuccessful();
                } catch (Throwable th) {
                    th = th;
                }
            } else {
                list = emptyList;
                vfVar = null;
                i = 0;
            }
            if (cursor != null) {
                cursor.close();
            }
            this.mDb.endTransaction();
            if (vfVar != null) {
                vfVar.c();
            }
            bVar.a(list, i, countItems);
        } catch (Throwable th2) {
            th = th2;
            vfVar = null;
            if (cursor != null) {
                cursor.close();
            }
            this.mDb.endTransaction();
            if (vfVar != null) {
                vfVar.c();
            }
            throw th;
        }
    }

    @DexIgnore
    public void loadRange(vd.g gVar, vd.e<T> eVar) {
        eVar.a(loadRange(gVar.a, gVar.b));
    }

    @DexIgnore
    public ag(RoomDatabase roomDatabase, vf vfVar, boolean z, String... strArr) {
        this.mDb = roomDatabase;
        this.mSourceQuery = vfVar;
        this.mInTransaction = z;
        this.mCountQuery = "SELECT COUNT(*) FROM ( " + this.mSourceQuery.b() + " )";
        this.mLimitOffsetQuery = "SELECT * FROM ( " + this.mSourceQuery.b() + " ) LIMIT ? OFFSET ?";
        this.mObserver = new a(strArr);
        roomDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    public List<T> loadRange(int i, int i2) {
        vf sQLiteQuery = getSQLiteQuery(i, i2);
        if (this.mInTransaction) {
            this.mDb.beginTransaction();
            Cursor cursor = null;
            try {
                cursor = this.mDb.query(sQLiteQuery);
                List<T> convertRows = convertRows(cursor);
                this.mDb.setTransactionSuccessful();
                return convertRows;
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
                this.mDb.endTransaction();
                sQLiteQuery.c();
            }
        } else {
            Cursor query = this.mDb.query(sQLiteQuery);
            try {
                return convertRows(query);
            } finally {
                query.close();
                sQLiteQuery.c();
            }
        }
    }
}
