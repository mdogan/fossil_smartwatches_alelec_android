package com.fossil.blesdk.obfuscated;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dc1 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public dc1(int i, byte[] bArr) {
        this.a = i;
        this.b = bArr;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof dc1)) {
            return false;
        }
        dc1 dc1 = (dc1) obj;
        return this.a == dc1.a && Arrays.equals(this.b, dc1.b);
    }

    @DexIgnore
    public final int hashCode() {
        return ((this.a + 527) * 31) + Arrays.hashCode(this.b);
    }
}
