package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.obfuscated.ew3;
import com.fossil.blesdk.obfuscated.zv3;
import com.squareup.okhttp.internal.framed.ErrorCode;
import com.squareup.okhttp.internal.framed.HeadersMode;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import okio.ByteString;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fw3 implements nw3 {
    @DexIgnore
    public static /* final */ Logger a; // = Logger.getLogger(b.class.getName());
    @DexIgnore
    public static /* final */ ByteString b; // = ByteString.encodeUtf8("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");

    @DexIgnore
    public static IllegalArgumentException c(String str, Object... objArr) {
        throw new IllegalArgumentException(String.format(str, objArr));
    }

    @DexIgnore
    public static IOException d(String str, Object... objArr) throws IOException {
        throw new IOException(String.format(str, objArr));
    }

    @DexIgnore
    public static /* synthetic */ IOException a(String str, Object[] objArr) throws IOException {
        d(str, objArr);
        throw null;
    }

    @DexIgnore
    public static /* synthetic */ IllegalArgumentException b(String str, Object[] objArr) {
        c(str, objArr);
        throw null;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ String[] a; // = {"DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION"};
        @DexIgnore
        public static /* final */ String[] b; // = new String[64];
        @DexIgnore
        public static /* final */ String[] c; // = new String[256];

        /*
        static {
            int i = 0;
            int i2 = 0;
            while (true) {
                String[] strArr = c;
                if (i2 >= strArr.length) {
                    break;
                }
                strArr[i2] = String.format("%8s", new Object[]{Integer.toBinaryString(i2)}).replace(' ', '0');
                i2++;
            }
            String[] strArr2 = b;
            strArr2[0] = "";
            strArr2[1] = "END_STREAM";
            int[] iArr = {1};
            strArr2[8] = "PADDED";
            for (int i3 : iArr) {
                b[i3 | 8] = b[i3] + "|PADDED";
            }
            String[] strArr3 = b;
            strArr3[4] = "END_HEADERS";
            strArr3[32] = "PRIORITY";
            strArr3[36] = "END_HEADERS|PRIORITY";
            for (int i4 : new int[]{4, 32, 36}) {
                for (int i5 : iArr) {
                    int i6 = i5 | i4;
                    b[i6] = b[i5] + '|' + b[i4];
                    b[i6 | 8] = b[i5] + '|' + b[i4] + "|PADDED";
                }
            }
            while (true) {
                String[] strArr4 = b;
                if (i < strArr4.length) {
                    if (strArr4[i] == null) {
                        strArr4[i] = c[i];
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
        */

        @DexIgnore
        public static String a(boolean z, int i, int i2, byte b2, byte b3) {
            String[] strArr = a;
            String format = b2 < strArr.length ? strArr[b2] : String.format("0x%02x", new Object[]{Byte.valueOf(b2)});
            String a2 = a(b2, b3);
            Object[] objArr = new Object[5];
            objArr[0] = z ? "<<" : ">>";
            objArr[1] = Integer.valueOf(i);
            objArr[2] = Integer.valueOf(i2);
            objArr[3] = format;
            objArr[4] = a2;
            return String.format("%s 0x%08x %5d %-13s %s", objArr);
        }

        @DexIgnore
        public static String a(byte b2, byte b3) {
            if (b3 == 0) {
                return "";
            }
            if (!(b2 == 2 || b2 == 3)) {
                if (b2 == 4 || b2 == 6) {
                    if (b3 == 1) {
                        return "ACK";
                    }
                    return c[b3];
                } else if (!(b2 == 7 || b2 == 8)) {
                    String[] strArr = b;
                    String str = b3 < strArr.length ? strArr[b3] : c[b3];
                    if (b2 != 5 || (b3 & 4) == 0) {
                        return (b2 != 0 || (b3 & 32) == 0) ? str : str.replace("PRIORITY", "COMPRESSED");
                    }
                    return str.replace("HEADERS", "PUSH_PROMISE");
                }
            }
            return c[b3];
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements aw3 {
        @DexIgnore
        public /* final */ wo4 e;
        @DexIgnore
        public /* final */ boolean f;
        @DexIgnore
        public /* final */ vo4 g; // = new vo4();
        @DexIgnore
        public /* final */ ew3.b h; // = new ew3.b(this.g);
        @DexIgnore
        public int i; // = RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE;
        @DexIgnore
        public boolean j;

        @DexIgnore
        public d(wo4 wo4, boolean z) {
            this.e = wo4;
            this.f = z;
        }

        @DexIgnore
        public synchronized void a(lw3 lw3) throws IOException {
            if (!this.j) {
                this.i = lw3.d(this.i);
                a(0, 0, (byte) 4, (byte) 1);
                this.e.flush();
            } else {
                throw new IOException("closed");
            }
        }

        @DexIgnore
        public final void b(int i2, long j2) throws IOException {
            while (j2 > 0) {
                int min = (int) Math.min((long) this.i, j2);
                long j3 = (long) min;
                j2 -= j3;
                a(i2, min, (byte) 9, j2 == 0 ? (byte) 4 : 0);
                this.e.a(this.g, j3);
            }
        }

        @DexIgnore
        public synchronized void close() throws IOException {
            this.j = true;
            this.e.close();
        }

        @DexIgnore
        public synchronized void flush() throws IOException {
            if (!this.j) {
                this.e.flush();
            } else {
                throw new IOException("closed");
            }
        }

        @DexIgnore
        public synchronized void p() throws IOException {
            if (this.j) {
                throw new IOException("closed");
            } else if (this.f) {
                if (fw3.a.isLoggable(Level.FINE)) {
                    fw3.a.fine(String.format(">> CONNECTION %s", new Object[]{fw3.b.hex()}));
                }
                this.e.write(fw3.b.toByteArray());
                this.e.flush();
            }
        }

        @DexIgnore
        public int r() {
            return this.i;
        }

        @DexIgnore
        public synchronized void b(lw3 lw3) throws IOException {
            if (!this.j) {
                int i2 = 0;
                a(0, lw3.c() * 6, (byte) 4, (byte) 0);
                while (i2 < 10) {
                    if (lw3.f(i2)) {
                        this.e.writeShort(i2 == 4 ? 3 : i2 == 7 ? 4 : i2);
                        this.e.writeInt(lw3.b(i2));
                    }
                    i2++;
                }
                this.e.flush();
            } else {
                throw new IOException("closed");
            }
        }

        @DexIgnore
        public synchronized void a(boolean z, boolean z2, int i2, int i3, List<dw3> list) throws IOException {
            if (!z2) {
                try {
                    if (!this.j) {
                        a(z, i2, list);
                    } else {
                        throw new IOException("closed");
                    }
                } catch (Throwable th) {
                    throw th;
                }
            } else {
                throw new UnsupportedOperationException();
            }
        }

        @DexIgnore
        public synchronized void a(int i2, int i3, List<dw3> list) throws IOException {
            if (!this.j) {
                this.h.a(list);
                long B = this.g.B();
                int min = (int) Math.min((long) (this.i - 4), B);
                long j2 = (long) min;
                int i4 = (B > j2 ? 1 : (B == j2 ? 0 : -1));
                a(i2, min + 4, (byte) 5, i4 == 0 ? (byte) 4 : 0);
                this.e.writeInt(i3 & Integer.MAX_VALUE);
                this.e.a(this.g, j2);
                if (i4 > 0) {
                    b(i2, B - j2);
                }
            } else {
                throw new IOException("closed");
            }
        }

        @DexIgnore
        public void a(boolean z, int i2, List<dw3> list) throws IOException {
            if (!this.j) {
                this.h.a(list);
                long B = this.g.B();
                int min = (int) Math.min((long) this.i, B);
                long j2 = (long) min;
                int i3 = (B > j2 ? 1 : (B == j2 ? 0 : -1));
                byte b = i3 == 0 ? (byte) 4 : 0;
                if (z) {
                    b = (byte) (b | 1);
                }
                a(i2, min, (byte) 1, b);
                this.e.a(this.g, j2);
                if (i3 > 0) {
                    b(i2, B - j2);
                    return;
                }
                return;
            }
            throw new IOException("closed");
        }

        @DexIgnore
        public synchronized void a(int i2, ErrorCode errorCode) throws IOException {
            if (this.j) {
                throw new IOException("closed");
            } else if (errorCode.httpCode != -1) {
                a(i2, 4, (byte) 3, (byte) 0);
                this.e.writeInt(errorCode.httpCode);
                this.e.flush();
            } else {
                throw new IllegalArgumentException();
            }
        }

        @DexIgnore
        public synchronized void a(boolean z, int i2, vo4 vo4, int i3) throws IOException {
            if (!this.j) {
                byte b = 0;
                if (z) {
                    b = (byte) 1;
                }
                a(i2, b, vo4, i3);
            } else {
                throw new IOException("closed");
            }
        }

        @DexIgnore
        public void a(int i2, byte b, vo4 vo4, int i3) throws IOException {
            a(i2, i3, (byte) 0, b);
            if (i3 > 0) {
                this.e.a(vo4, (long) i3);
            }
        }

        @DexIgnore
        public synchronized void a(boolean z, int i2, int i3) throws IOException {
            if (!this.j) {
                a(0, 8, (byte) 6, z ? (byte) 1 : 0);
                this.e.writeInt(i2);
                this.e.writeInt(i3);
                this.e.flush();
            } else {
                throw new IOException("closed");
            }
        }

        @DexIgnore
        public synchronized void a(int i2, ErrorCode errorCode, byte[] bArr) throws IOException {
            if (this.j) {
                throw new IOException("closed");
            } else if (errorCode.httpCode != -1) {
                a(0, bArr.length + 8, (byte) 7, (byte) 0);
                this.e.writeInt(i2);
                this.e.writeInt(errorCode.httpCode);
                if (bArr.length > 0) {
                    this.e.write(bArr);
                }
                this.e.flush();
            } else {
                fw3.b("errorCode.httpCode == -1", new Object[0]);
                throw null;
            }
        }

        @DexIgnore
        public synchronized void a(int i2, long j2) throws IOException {
            if (this.j) {
                throw new IOException("closed");
            } else if (j2 == 0 || j2 > 2147483647L) {
                fw3.b("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", new Object[]{Long.valueOf(j2)});
                throw null;
            } else {
                a(i2, 4, (byte) 8, (byte) 0);
                this.e.writeInt((int) j2);
                this.e.flush();
            }
        }

        @DexIgnore
        public void a(int i2, int i3, byte b, byte b2) throws IOException {
            if (fw3.a.isLoggable(Level.FINE)) {
                fw3.a.fine(b.a(false, i2, i3, b, b2));
            }
            int i4 = this.i;
            if (i3 > i4) {
                fw3.b("FRAME_SIZE_ERROR length > %d: %d", new Object[]{Integer.valueOf(i4), Integer.valueOf(i3)});
                throw null;
            } else if ((Integer.MIN_VALUE & i2) == 0) {
                fw3.b(this.e, i3);
                this.e.writeByte(b & FileType.MASKED_INDEX);
                this.e.writeByte(b2 & FileType.MASKED_INDEX);
                this.e.writeInt(i2 & Integer.MAX_VALUE);
            } else {
                fw3.b("reserved bit set: %s", new Object[]{Integer.valueOf(i2)});
                throw null;
            }
        }
    }

    @DexIgnore
    public static int b(int i, byte b2, short s) throws IOException {
        if ((b2 & 8) != 0) {
            i--;
        }
        if (s <= i) {
            return (short) (i - s);
        }
        d("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i));
        throw null;
    }

    @DexIgnore
    public static int b(xo4 xo4) throws IOException {
        return (xo4.readByte() & FileType.MASKED_INDEX) | ((xo4.readByte() & FileType.MASKED_INDEX) << DateTimeFieldType.CLOCKHOUR_OF_DAY) | ((xo4.readByte() & FileType.MASKED_INDEX) << 8);
    }

    @DexIgnore
    public zv3 a(xo4 xo4, boolean z) {
        return new c(xo4, 4096, z);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements kp4 {
        @DexIgnore
        public /* final */ xo4 e;
        @DexIgnore
        public int f;
        @DexIgnore
        public byte g;
        @DexIgnore
        public int h;
        @DexIgnore
        public int i;
        @DexIgnore
        public short j;

        @DexIgnore
        public a(xo4 xo4) {
            this.e = xo4;
        }

        @DexIgnore
        public long b(vo4 vo4, long j2) throws IOException {
            while (true) {
                int i2 = this.i;
                if (i2 == 0) {
                    this.e.skip((long) this.j);
                    this.j = 0;
                    if ((this.g & 4) != 0) {
                        return -1;
                    }
                    c();
                } else {
                    long b = this.e.b(vo4, Math.min(j2, (long) i2));
                    if (b == -1) {
                        return -1;
                    }
                    this.i = (int) (((long) this.i) - b);
                    return b;
                }
            }
        }

        @DexIgnore
        public final void c() throws IOException {
            int i2 = this.h;
            int a = fw3.b(this.e);
            this.i = a;
            this.f = a;
            byte readByte = (byte) (this.e.readByte() & FileType.MASKED_INDEX);
            this.g = (byte) (this.e.readByte() & FileType.MASKED_INDEX);
            if (fw3.a.isLoggable(Level.FINE)) {
                fw3.a.fine(b.a(true, this.h, this.f, readByte, this.g));
            }
            this.h = this.e.readInt() & Integer.MAX_VALUE;
            if (readByte != 9) {
                fw3.a("%s != TYPE_CONTINUATION", new Object[]{Byte.valueOf(readByte)});
                throw null;
            } else if (this.h != i2) {
                fw3.a("TYPE_CONTINUATION streamId changed", new Object[0]);
                throw null;
            }
        }

        @DexIgnore
        public void close() throws IOException {
        }

        @DexIgnore
        public lp4 b() {
            return this.e.b();
        }
    }

    @DexIgnore
    public static void b(wo4 wo4, int i) throws IOException {
        wo4.writeByte((i >>> 16) & 255);
        wo4.writeByte((i >>> 8) & 255);
        wo4.writeByte(i & 255);
    }

    @DexIgnore
    public aw3 a(wo4 wo4, boolean z) {
        return new d(wo4, z);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements zv3 {
        @DexIgnore
        public /* final */ xo4 e;
        @DexIgnore
        public /* final */ a f; // = new a(this.e);
        @DexIgnore
        public /* final */ boolean g;
        @DexIgnore
        public /* final */ ew3.a h;

        @DexIgnore
        public c(xo4 xo4, int i, boolean z) {
            this.e = xo4;
            this.g = z;
            this.h = new ew3.a(i, this.f);
        }

        @DexIgnore
        public boolean a(zv3.a aVar) throws IOException {
            try {
                this.e.g(9);
                int a = fw3.b(this.e);
                if (a < 0 || a > 16384) {
                    fw3.a("FRAME_SIZE_ERROR: %s", new Object[]{Integer.valueOf(a)});
                    throw null;
                }
                byte readByte = (byte) (this.e.readByte() & FileType.MASKED_INDEX);
                byte readByte2 = (byte) (this.e.readByte() & FileType.MASKED_INDEX);
                int readInt = this.e.readInt() & Integer.MAX_VALUE;
                if (fw3.a.isLoggable(Level.FINE)) {
                    fw3.a.fine(b.a(true, readInt, a, readByte, readByte2));
                }
                switch (readByte) {
                    case 0:
                        a(aVar, a, readByte2, readInt);
                        break;
                    case 1:
                        c(aVar, a, readByte2, readInt);
                        break;
                    case 2:
                        e(aVar, a, readByte2, readInt);
                        break;
                    case 3:
                        g(aVar, a, readByte2, readInt);
                        break;
                    case 4:
                        h(aVar, a, readByte2, readInt);
                        break;
                    case 5:
                        f(aVar, a, readByte2, readInt);
                        break;
                    case 6:
                        d(aVar, a, readByte2, readInt);
                        break;
                    case 7:
                        b(aVar, a, readByte2, readInt);
                        break;
                    case 8:
                        i(aVar, a, readByte2, readInt);
                        break;
                    default:
                        this.e.skip((long) a);
                        break;
                }
                return true;
            } catch (IOException unused) {
                return false;
            }
        }

        @DexIgnore
        public final void b(zv3.a aVar, int i, byte b, int i2) throws IOException {
            if (i < 8) {
                fw3.a("TYPE_GOAWAY length < 8: %s", new Object[]{Integer.valueOf(i)});
                throw null;
            } else if (i2 == 0) {
                int readInt = this.e.readInt();
                int readInt2 = this.e.readInt();
                int i3 = i - 8;
                ErrorCode fromHttp2 = ErrorCode.fromHttp2(readInt2);
                if (fromHttp2 != null) {
                    ByteString byteString = ByteString.EMPTY;
                    if (i3 > 0) {
                        byteString = this.e.d((long) i3);
                    }
                    aVar.a(readInt, fromHttp2, byteString);
                    return;
                }
                fw3.a("TYPE_GOAWAY unexpected error code: %d", new Object[]{Integer.valueOf(readInt2)});
                throw null;
            } else {
                fw3.a("TYPE_GOAWAY streamId != 0", new Object[0]);
                throw null;
            }
        }

        @DexIgnore
        public final void c(zv3.a aVar, int i, byte b, int i2) throws IOException {
            short s = 0;
            if (i2 != 0) {
                boolean z = (b & 1) != 0;
                if ((b & 8) != 0) {
                    s = (short) (this.e.readByte() & FileType.MASKED_INDEX);
                }
                if ((b & 32) != 0) {
                    a(aVar, i2);
                    i -= 5;
                }
                aVar.a(false, z, i2, -1, a(fw3.b(i, b, s), s, b, i2), HeadersMode.HTTP_20_HEADERS);
                return;
            }
            fw3.a("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
            throw null;
        }

        @DexIgnore
        public void close() throws IOException {
            this.e.close();
        }

        @DexIgnore
        public final void d(zv3.a aVar, int i, byte b, int i2) throws IOException {
            boolean z = false;
            if (i != 8) {
                fw3.a("TYPE_PING length != 8: %s", new Object[]{Integer.valueOf(i)});
                throw null;
            } else if (i2 == 0) {
                int readInt = this.e.readInt();
                int readInt2 = this.e.readInt();
                if ((b & 1) != 0) {
                    z = true;
                }
                aVar.a(z, readInt, readInt2);
            } else {
                fw3.a("TYPE_PING streamId != 0", new Object[0]);
                throw null;
            }
        }

        @DexIgnore
        public final void e(zv3.a aVar, int i, byte b, int i2) throws IOException {
            if (i != 5) {
                fw3.a("TYPE_PRIORITY length: %d != 5", new Object[]{Integer.valueOf(i)});
                throw null;
            } else if (i2 != 0) {
                a(aVar, i2);
            } else {
                fw3.a("TYPE_PRIORITY streamId == 0", new Object[0]);
                throw null;
            }
        }

        @DexIgnore
        public final void f(zv3.a aVar, int i, byte b, int i2) throws IOException {
            short s = 0;
            if (i2 != 0) {
                if ((b & 8) != 0) {
                    s = (short) (this.e.readByte() & FileType.MASKED_INDEX);
                }
                aVar.a(i2, this.e.readInt() & Integer.MAX_VALUE, a(fw3.b(i - 4, b, s), s, b, i2));
                return;
            }
            fw3.a("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
            throw null;
        }

        @DexIgnore
        public final void g(zv3.a aVar, int i, byte b, int i2) throws IOException {
            if (i != 4) {
                fw3.a("TYPE_RST_STREAM length: %d != 4", new Object[]{Integer.valueOf(i)});
                throw null;
            } else if (i2 != 0) {
                int readInt = this.e.readInt();
                ErrorCode fromHttp2 = ErrorCode.fromHttp2(readInt);
                if (fromHttp2 != null) {
                    aVar.a(i2, fromHttp2);
                    return;
                }
                fw3.a("TYPE_RST_STREAM unexpected error code: %d", new Object[]{Integer.valueOf(readInt)});
                throw null;
            } else {
                fw3.a("TYPE_RST_STREAM streamId == 0", new Object[0]);
                throw null;
            }
        }

        @DexIgnore
        public final void h(zv3.a aVar, int i, byte b, int i2) throws IOException {
            if (i2 != 0) {
                fw3.a("TYPE_SETTINGS streamId != 0", new Object[0]);
                throw null;
            } else if ((b & 1) != 0) {
                if (i == 0) {
                    aVar.a();
                } else {
                    fw3.a("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
                    throw null;
                }
            } else if (i % 6 == 0) {
                lw3 lw3 = new lw3();
                for (int i3 = 0; i3 < i; i3 += 6) {
                    short readShort = this.e.readShort();
                    int readInt = this.e.readInt();
                    switch (readShort) {
                        case 1:
                        case 6:
                            break;
                        case 2:
                            if (!(readInt == 0 || readInt == 1)) {
                                fw3.a("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                                throw null;
                            }
                        case 3:
                            readShort = 4;
                            break;
                        case 4:
                            readShort = 7;
                            if (readInt >= 0) {
                                break;
                            } else {
                                fw3.a("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                                throw null;
                            }
                        case 5:
                            if (readInt >= 16384 && readInt <= 16777215) {
                                break;
                            } else {
                                fw3.a("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", new Object[]{Integer.valueOf(readInt)});
                                throw null;
                            }
                            break;
                        default:
                            fw3.a("PROTOCOL_ERROR invalid settings id: %s", new Object[]{Short.valueOf(readShort)});
                            throw null;
                    }
                    lw3.a(readShort, 0, readInt);
                }
                aVar.a(false, lw3);
                if (lw3.b() >= 0) {
                    this.h.d(lw3.b());
                }
            } else {
                fw3.a("TYPE_SETTINGS length %% 6 != 0: %s", new Object[]{Integer.valueOf(i)});
                throw null;
            }
        }

        @DexIgnore
        public final void i(zv3.a aVar, int i, byte b, int i2) throws IOException {
            if (i == 4) {
                long readInt = ((long) this.e.readInt()) & 2147483647L;
                if (readInt != 0) {
                    aVar.a(i2, readInt);
                    return;
                }
                fw3.a("windowSizeIncrement was 0", new Object[]{Long.valueOf(readInt)});
                throw null;
            }
            fw3.a("TYPE_WINDOW_UPDATE length !=4: %s", new Object[]{Integer.valueOf(i)});
            throw null;
        }

        @DexIgnore
        public void q() throws IOException {
            if (!this.g) {
                ByteString d = this.e.d((long) fw3.b.size());
                if (fw3.a.isLoggable(Level.FINE)) {
                    fw3.a.fine(String.format("<< CONNECTION %s", new Object[]{d.hex()}));
                }
                if (!fw3.b.equals(d)) {
                    fw3.a("Expected a connection header but was %s", new Object[]{d.utf8()});
                    throw null;
                }
            }
        }

        @DexIgnore
        public final List<dw3> a(int i, short s, byte b, int i2) throws IOException {
            a aVar = this.f;
            aVar.i = i;
            aVar.f = i;
            aVar.j = s;
            aVar.g = b;
            aVar.h = i2;
            this.h.f();
            return this.h.c();
        }

        @DexIgnore
        public final void a(zv3.a aVar, int i, byte b, int i2) throws IOException {
            boolean z = true;
            short s = 0;
            boolean z2 = (b & 1) != 0;
            if ((b & 32) == 0) {
                z = false;
            }
            if (!z) {
                if ((b & 8) != 0) {
                    s = (short) (this.e.readByte() & FileType.MASKED_INDEX);
                }
                aVar.a(z2, i2, this.e, fw3.b(i, b, s));
                this.e.skip((long) s);
                return;
            }
            fw3.a("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
            throw null;
        }

        @DexIgnore
        public final void a(zv3.a aVar, int i) throws IOException {
            int readInt = this.e.readInt();
            aVar.a(i, readInt & Integer.MAX_VALUE, (this.e.readByte() & FileType.MASKED_INDEX) + 1, (Integer.MIN_VALUE & readInt) != 0);
        }
    }
}
