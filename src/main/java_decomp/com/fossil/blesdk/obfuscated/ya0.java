package com.fossil.blesdk.obfuscated;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.fossil.blesdk.hid.HIDProfile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ya0 {
    @DexIgnore
    public static /* final */ String a; // = a;
    @DexIgnore
    public static boolean b; // = true;
    @DexIgnore
    public static /* final */ ya0 c; // = new ya0();

    @DexIgnore
    public final void a(String str) {
        wd4.b(str, "value");
        wa0.f.a(str);
        da0.e.a().a(str);
    }

    @DexIgnore
    public final String b() {
        return null;
    }

    @DexIgnore
    public final String c() {
        return wa0.f.e();
    }

    @DexIgnore
    public final boolean d() {
        return wa0.f.a() != null;
    }

    @DexIgnore
    public final boolean a() {
        return b;
    }

    @DexIgnore
    public final void a(Context context) throws IllegalArgumentException, IllegalStateException {
        wd4.b(context, "applicationContext");
        if (BluetoothAdapter.getDefaultAdapter() == null) {
            throw new IllegalStateException("Bluetooth is not supported on this hardware platform.");
        } else if (context == context.getApplicationContext()) {
            wa0.f.a(context);
            u90.c.a(a, "init: sdkVersion=5.8.5-production-release");
            HIDProfile.e.a(context);
            BluetoothLeAdapter.l.c(context);
            System.loadLibrary("FitnessAlgorithm");
            System.loadLibrary("EllipticCurveCrypto");
        } else {
            throw new IllegalArgumentException("Invalid application context.");
        }
    }

    @DexIgnore
    public final void a(oa0 oa0) {
        wd4.b(oa0, "sdkLogEndpoint");
        wa0.f.a(oa0);
    }
}
