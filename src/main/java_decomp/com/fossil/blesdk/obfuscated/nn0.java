package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nn0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<nn0> CREATOR; // = new on0();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ hn0 f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;

    @DexIgnore
    public nn0(String str, IBinder iBinder, boolean z, boolean z2) {
        this.e = str;
        this.f = a(iBinder);
        this.g = z;
        this.h = z2;
    }

    @DexIgnore
    public static hn0 a(IBinder iBinder) {
        byte[] bArr;
        if (iBinder == null) {
            return null;
        }
        try {
            tn0 zzb = wl0.a(iBinder).zzb();
            if (zzb == null) {
                bArr = null;
            } else {
                bArr = (byte[]) vn0.d(zzb);
            }
            if (bArr != null) {
                return new in0(bArr);
            }
            Log.e("GoogleCertificatesQuery", "Could not unwrap certificate");
            return null;
        } catch (RemoteException e2) {
            Log.e("GoogleCertificatesQuery", "Could not unwrap certificate", e2);
            return null;
        }
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e, false);
        hn0 hn0 = this.f;
        if (hn0 == null) {
            Log.w("GoogleCertificatesQuery", "certificate binder is null");
            hn0 = null;
        } else {
            hn0.asBinder();
        }
        lk0.a(parcel, 2, (IBinder) hn0, false);
        lk0.a(parcel, 3, this.g);
        lk0.a(parcel, 4, this.h);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public nn0(String str, hn0 hn0, boolean z, boolean z2) {
        this.e = str;
        this.f = hn0;
        this.g = z;
        this.h = z2;
    }
}
