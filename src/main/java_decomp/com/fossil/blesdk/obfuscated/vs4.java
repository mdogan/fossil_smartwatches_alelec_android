package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vs4 implements sr4<qm4, Float> {
    @DexIgnore
    public static /* final */ vs4 a; // = new vs4();

    @DexIgnore
    public Float a(qm4 qm4) throws IOException {
        return Float.valueOf(qm4.F());
    }
}
