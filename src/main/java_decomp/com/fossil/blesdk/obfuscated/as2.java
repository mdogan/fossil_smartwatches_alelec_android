package com.fossil.blesdk.obfuscated;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.br4;
import com.fossil.blesdk.obfuscated.vl2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class as2 extends Fragment implements br4.a, View.OnKeyListener, vl2.b {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public AnalyticsHelper e;
    @DexIgnore
    public /* final */ qa f; // = new w62(this);
    @DexIgnore
    public wl2 g;
    @DexIgnore
    public HashMap h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = as2.class.getSimpleName();
        wd4.a((Object) simpleName, "BaseFragment::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public void L0() {
        FLogger.INSTANCE.getLocal().d(i, "Tracer started");
        PortfolioApp.W.c().a(this.g);
    }

    @DexIgnore
    public void M0() {
        FLogger.INSTANCE.getLocal().d(i, "Tracer ended");
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final qa O0() {
        return this.f;
    }

    @DexIgnore
    public final AnalyticsHelper P0() {
        AnalyticsHelper analyticsHelper = this.e;
        if (analyticsHelper != null) {
            return analyticsHelper;
        }
        wd4.d("mAnalyticHelper");
        throw null;
    }

    @DexIgnore
    public final wl2 Q0() {
        return this.g;
    }

    @DexIgnore
    public final void R(String str) {
        wd4.b(str, "view");
        wl2 b = AnalyticsHelper.f.b();
        b.b(str);
        this.g = b;
        wl2 wl2 = this.g;
        if (wl2 != null) {
            wl2.a((vl2.b) this);
        }
    }

    @DexIgnore
    public String R0() {
        return i;
    }

    @DexIgnore
    public final void S(String str) {
        wd4.b(str, "title");
        FLogger.INSTANCE.getLocal().d(i, "showProgressDialog");
        if (isActive() && getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).a(str);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public boolean S0() {
        if (!isActive() || getParentFragment() == null || !(getParentFragment() instanceof as2)) {
            return false;
        }
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            return ((as2) parentFragment).S0();
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    public final void T(String str) {
        wd4.b(str, "content");
        LayoutInflater layoutInflater = getLayoutInflater();
        FragmentActivity activity = getActivity();
        View inflate = layoutInflater.inflate(R.layout.toast_created_new_preset_successfully, activity != null ? (ViewGroup) activity.findViewById(R.id.cv_root) : null);
        View findViewById = inflate.findViewById(R.id.ftv_content);
        wd4.a((Object) findViewById, "view.findViewById<Flexib\u2026xtView>(R.id.ftv_content)");
        ((FlexibleTextView) findViewById).setText(str);
        Toast toast = new Toast(getContext());
        toast.setGravity(80, 0, 200);
        toast.setDuration(1);
        toast.setView(inflate);
        toast.show();
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().d(i, "hideProgressDialog");
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).h();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public final void b() {
        FLogger.INSTANCE.getLocal().d(i, "showProgressDialog");
        if (isActive() && getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).p();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public final boolean isActive() {
        return isAdded();
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.e = AnalyticsHelper.f.c();
    }

    @DexIgnore
    public void onDestroyView() {
        wl2 wl2 = this.g;
        if (wl2 != null) {
            wl2.c();
        }
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        wd4.b(view, "view");
        wd4.b(keyEvent, "keyEvent");
        if (keyEvent.getAction() != 1 || i2 != 4) {
            return false;
        }
        FLogger.INSTANCE.getLocal().d(i, "onKey KEYCODE_BACK");
        return S0();
    }

    @DexIgnore
    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        wd4.b(strArr, "permissions");
        wd4.b(iArr, "grantResults");
        super.onRequestPermissionsResult(i2, strArr, iArr);
        br4.a(i2, strArr, iArr, this);
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        view.setOnKeyListener(this);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
    }

    @DexIgnore
    public final void a(Fragment fragment, String str, int i2) {
        wd4.b(fragment, "fragment");
        wd4.b(str, "tag");
        FragmentManager childFragmentManager = getChildFragmentManager();
        wd4.a((Object) childFragmentManager, "childFragmentManager");
        cb a2 = childFragmentManager.a();
        wd4.a((Object) a2, "fragmentManager.beginTransaction()");
        Fragment a3 = childFragmentManager.a(i2);
        if (a3 != null) {
            a2.d(a3);
        }
        a2.a((String) null);
        a2.b(i2, fragment, str);
        a2.b();
    }

    @DexIgnore
    public void b(int i2, List<String> list) {
        wd4.b(list, "perms");
        for (String str : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local.d(str2, "onPermissionsGranted: perm = " + str);
        }
    }

    @DexIgnore
    public void a(int i2, List<String> list) {
        wd4.b(list, "perms");
        for (String str : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local.d(str2, "onPermissionsDenied: perm = " + str);
        }
    }

    @DexIgnore
    public final void a(Intent intent, String str) {
        wd4.b(intent, "browserIntent");
        wd4.b(str, "tag");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException unused) {
            FLogger.INSTANCE.getLocal().e(str, "Exception when start url with no browser app");
        }
    }

    @DexIgnore
    public final void a(String str, Map<String, String> map) {
        wd4.b(str, Constants.EVENT);
        wd4.b(map, "values");
        AnalyticsHelper analyticsHelper = this.e;
        if (analyticsHelper != null) {
            analyticsHelper.a(str, (Map<String, ? extends Object>) map);
        } else {
            wd4.d("mAnalyticHelper");
            throw null;
        }
    }
}
