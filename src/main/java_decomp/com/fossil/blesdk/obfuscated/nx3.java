package com.fossil.blesdk.obfuscated;

import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface nx3 {
    @DexIgnore
    public static final nx3 a = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements nx3 {
        @DexIgnore
        public Map<Class<?>, Set<lx3>> a(Object obj) {
            return ix3.b(obj);
        }

        @DexIgnore
        public Map<Class<?>, mx3> b(Object obj) {
            return ix3.a(obj);
        }
    }

    @DexIgnore
    Map<Class<?>, Set<lx3>> a(Object obj);

    @DexIgnore
    Map<Class<?>, mx3> b(Object obj);
}
