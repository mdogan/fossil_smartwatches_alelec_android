package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.watchapp.WatchAppConfig;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c60 extends j60 {
    @DexIgnore
    public /* final */ WatchAppConfig R;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ c60(Peripheral peripheral, Phase.a aVar, WatchAppConfig watchAppConfig, String str, int i, rd4 rd4) {
        this(peripheral, aVar, watchAppConfig, str);
        if ((i & 8) != 0) {
            str = UUID.randomUUID().toString();
            wd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public JSONObject u() {
        return n90.a(super.u(), this.R.toJSONObject());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c60(Peripheral peripheral, Phase.a aVar, WatchAppConfig watchAppConfig, String str) {
        super(peripheral, aVar, PhaseId.SET_WATCH_APP, watchAppConfig.getSettingAssignmentJSON$blesdk_productionRelease(), false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 48, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(watchAppConfig, "watchAppConfig");
        wd4.b(str, "phaseUuid");
        this.R = watchAppConfig;
    }
}
