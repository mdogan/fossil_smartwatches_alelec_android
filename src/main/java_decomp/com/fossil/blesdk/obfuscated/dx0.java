package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dx0 extends bx0<cx0, cx0> {
    @DexIgnore
    public static void a(Object obj, cx0 cx0) {
        ((su0) obj).zzjp = cx0;
    }

    @DexIgnore
    public final /* synthetic */ Object a() {
        return cx0.e();
    }

    @DexIgnore
    public final void a(Object obj) {
        ((su0) obj).zzjp.c();
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, int i, long j) {
        ((cx0) obj).a(i << 3, (Object) Long.valueOf(j));
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, px0 px0) throws IOException {
        ((cx0) obj).b(px0);
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, Object obj2) {
        a(obj, (cx0) obj2);
    }

    @DexIgnore
    public final /* synthetic */ int b(Object obj) {
        return ((cx0) obj).a();
    }

    @DexIgnore
    public final /* synthetic */ void b(Object obj, px0 px0) throws IOException {
        ((cx0) obj).a(px0);
    }

    @DexIgnore
    public final /* synthetic */ void b(Object obj, Object obj2) {
        a(obj, (cx0) obj2);
    }

    @DexIgnore
    public final /* synthetic */ Object c(Object obj) {
        return ((su0) obj).zzjp;
    }

    @DexIgnore
    public final /* synthetic */ Object c(Object obj, Object obj2) {
        cx0 cx0 = (cx0) obj;
        cx0 cx02 = (cx0) obj2;
        return cx02.equals(cx0.d()) ? cx0 : cx0.a(cx0, cx02);
    }

    @DexIgnore
    public final /* synthetic */ int d(Object obj) {
        return ((cx0) obj).b();
    }
}
