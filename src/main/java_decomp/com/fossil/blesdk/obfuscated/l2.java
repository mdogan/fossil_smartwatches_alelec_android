package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class l2 extends ListView {
    @DexIgnore
    public /* final */ Rect e; // = new Rect();
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public int g; // = 0;
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public int i; // = 0;
    @DexIgnore
    public int j;
    @DexIgnore
    public Field k;
    @DexIgnore
    public a l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public k9 p;
    @DexIgnore
    public z9 q;
    @DexIgnore
    public b r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends p0 {
        @DexIgnore
        public boolean f; // = true;

        @DexIgnore
        public a(Drawable drawable) {
            super(drawable);
        }

        @DexIgnore
        public void a(boolean z) {
            this.f = z;
        }

        @DexIgnore
        public void draw(Canvas canvas) {
            if (this.f) {
                super.draw(canvas);
            }
        }

        @DexIgnore
        public void setHotspot(float f2, float f3) {
            if (this.f) {
                super.setHotspot(f2, f3);
            }
        }

        @DexIgnore
        public void setHotspotBounds(int i, int i2, int i3, int i4) {
            if (this.f) {
                super.setHotspotBounds(i, i2, i3, i4);
            }
        }

        @DexIgnore
        public boolean setState(int[] iArr) {
            if (this.f) {
                return super.setState(iArr);
            }
            return false;
        }

        @DexIgnore
        public boolean setVisible(boolean z, boolean z2) {
            if (this.f) {
                return super.setVisible(z, z2);
            }
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a() {
            l2 l2Var = l2.this;
            l2Var.r = null;
            l2Var.removeCallbacks(this);
        }

        @DexIgnore
        public void b() {
            l2.this.post(this);
        }

        @DexIgnore
        public void run() {
            l2 l2Var = l2.this;
            l2Var.r = null;
            l2Var.drawableStateChanged();
        }
    }

    @DexIgnore
    public l2(Context context, boolean z) {
        super(context, (AttributeSet) null, r.dropDownListViewStyle);
        this.n = z;
        setCacheColorHint(0);
        try {
            this.k = AbsListView.class.getDeclaredField("mIsChildViewEnabled");
            this.k.setAccessible(true);
        } catch (NoSuchFieldException e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    private void setSelectorEnabled(boolean z) {
        a aVar = this.l;
        if (aVar != null) {
            aVar.a(z);
        }
    }

    @DexIgnore
    public int a(int i2, int i3, int i4, int i5, int i6) {
        int i7;
        int listPaddingTop = getListPaddingTop();
        int listPaddingBottom = getListPaddingBottom();
        getListPaddingLeft();
        getListPaddingRight();
        int dividerHeight = getDividerHeight();
        Drawable divider = getDivider();
        ListAdapter adapter = getAdapter();
        if (adapter == null) {
            return listPaddingTop + listPaddingBottom;
        }
        int i8 = listPaddingTop + listPaddingBottom;
        if (dividerHeight <= 0 || divider == null) {
            dividerHeight = 0;
        }
        int count = adapter.getCount();
        int i9 = i8;
        View view = null;
        int i10 = 0;
        int i11 = 0;
        int i12 = 0;
        while (i10 < count) {
            int itemViewType = adapter.getItemViewType(i10);
            if (itemViewType != i11) {
                view = null;
                i11 = itemViewType;
            }
            view = adapter.getView(i10, view, this);
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = generateDefaultLayoutParams();
                view.setLayoutParams(layoutParams);
            }
            int i13 = layoutParams.height;
            if (i13 > 0) {
                i7 = View.MeasureSpec.makeMeasureSpec(i13, 1073741824);
            } else {
                i7 = View.MeasureSpec.makeMeasureSpec(0, 0);
            }
            view.measure(i2, i7);
            view.forceLayout();
            if (i10 > 0) {
                i9 += dividerHeight;
            }
            i9 += view.getMeasuredHeight();
            if (i9 >= i5) {
                return (i6 < 0 || i10 <= i6 || i12 <= 0 || i9 == i5) ? i5 : i12;
            }
            if (i6 >= 0 && i10 >= i6) {
                i12 = i9;
            }
            i10++;
        }
        return i9;
    }

    @DexIgnore
    public final void b(int i2, View view) {
        Drawable selector = getSelector();
        boolean z = true;
        boolean z2 = (selector == null || i2 == -1) ? false : true;
        if (z2) {
            selector.setVisible(false, false);
        }
        a(i2, view);
        if (z2) {
            Rect rect = this.e;
            float exactCenterX = rect.exactCenterX();
            float exactCenterY = rect.exactCenterY();
            if (getVisibility() != 0) {
                z = false;
            }
            selector.setVisible(z, false);
            c7.a(selector, exactCenterX, exactCenterY);
        }
    }

    @DexIgnore
    public final void c() {
        Drawable selector = getSelector();
        if (selector != null && b() && isPressed()) {
            selector.setState(getDrawableState());
        }
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        a(canvas);
        super.dispatchDraw(canvas);
    }

    @DexIgnore
    public void drawableStateChanged() {
        if (this.r == null) {
            super.drawableStateChanged();
            setSelectorEnabled(true);
            c();
        }
    }

    @DexIgnore
    public boolean hasFocus() {
        return this.n || super.hasFocus();
    }

    @DexIgnore
    public boolean hasWindowFocus() {
        return this.n || super.hasWindowFocus();
    }

    @DexIgnore
    public boolean isFocused() {
        return this.n || super.isFocused();
    }

    @DexIgnore
    public boolean isInTouchMode() {
        return (this.n && this.m) || super.isInTouchMode();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.r = null;
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public boolean onHoverEvent(MotionEvent motionEvent) {
        if (Build.VERSION.SDK_INT < 26) {
            return super.onHoverEvent(motionEvent);
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 10 && this.r == null) {
            this.r = new b();
            this.r.b();
        }
        boolean onHoverEvent = super.onHoverEvent(motionEvent);
        if (actionMasked == 9 || actionMasked == 7) {
            int pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
            if (!(pointToPosition == -1 || pointToPosition == getSelectedItemPosition())) {
                View childAt = getChildAt(pointToPosition - getFirstVisiblePosition());
                if (childAt.isEnabled()) {
                    setSelectionFromTop(pointToPosition, childAt.getTop() - getTop());
                }
                c();
            }
        } else {
            setSelection(-1);
        }
        return onHoverEvent;
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.j = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
        }
        b bVar = this.r;
        if (bVar != null) {
            bVar.a();
        }
        return super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    public void setListSelectionHidden(boolean z) {
        this.m = z;
    }

    @DexIgnore
    public void setSelector(Drawable drawable) {
        this.l = drawable != null ? new a(drawable) : null;
        super.setSelector(this.l);
        Rect rect = new Rect();
        if (drawable != null) {
            drawable.getPadding(rect);
        }
        this.f = rect.left;
        this.g = rect.top;
        this.h = rect.right;
        this.i = rect.bottom;
    }

    @DexIgnore
    public final boolean b() {
        return this.o;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (r0 != 3) goto L_0x000e;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0065  */
    public boolean a(MotionEvent motionEvent, int i2) {
        boolean z;
        boolean z2;
        int findPointerIndex;
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 1) {
            z2 = false;
            findPointerIndex = motionEvent.findPointerIndex(i2);
            if (findPointerIndex >= 0) {
            }
            z = false;
            z2 = false;
            a();
            if (!z2) {
            }
            return z2;
        } else if (actionMasked == 2) {
            z2 = true;
            findPointerIndex = motionEvent.findPointerIndex(i2);
            if (findPointerIndex >= 0) {
                int x = (int) motionEvent.getX(findPointerIndex);
                int y = (int) motionEvent.getY(findPointerIndex);
                int pointToPosition = pointToPosition(x, y);
                if (pointToPosition == -1) {
                    z = true;
                    if (!z2 || z) {
                        a();
                    }
                    if (!z2) {
                        if (this.q == null) {
                            this.q = new z9(this);
                        }
                        this.q.a(true);
                        this.q.onTouch(this, motionEvent);
                    } else {
                        z9 z9Var = this.q;
                        if (z9Var != null) {
                            z9Var.a(false);
                        }
                    }
                    return z2;
                }
                View childAt = getChildAt(pointToPosition - getFirstVisiblePosition());
                a(childAt, pointToPosition, (float) x, (float) y);
                if (actionMasked == 1) {
                    a(childAt, pointToPosition);
                }
            }
            z = false;
            z2 = false;
            a();
            if (!z2) {
            }
            return z2;
        }
        z = false;
        z2 = true;
        a();
        if (!z2) {
        }
        return z2;
    }

    @DexIgnore
    public final void a(View view, int i2) {
        performItemClick(view, i2, getItemIdAtPosition(i2));
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        if (!this.e.isEmpty()) {
            Drawable selector = getSelector();
            if (selector != null) {
                selector.setBounds(this.e);
                selector.draw(canvas);
            }
        }
    }

    @DexIgnore
    public final void a(int i2, View view, float f2, float f3) {
        b(i2, view);
        Drawable selector = getSelector();
        if (selector != null && i2 != -1) {
            c7.a(selector, f2, f3);
        }
    }

    @DexIgnore
    public final void a(int i2, View view) {
        Rect rect = this.e;
        rect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        rect.left -= this.f;
        rect.top -= this.g;
        rect.right += this.h;
        rect.bottom += this.i;
        try {
            boolean z = this.k.getBoolean(this);
            if (view.isEnabled() != z) {
                this.k.set(this, Boolean.valueOf(!z));
                if (i2 != -1) {
                    refreshDrawableState();
                }
            }
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a() {
        this.o = false;
        setPressed(false);
        drawableStateChanged();
        View childAt = getChildAt(this.j - getFirstVisiblePosition());
        if (childAt != null) {
            childAt.setPressed(false);
        }
        k9 k9Var = this.p;
        if (k9Var != null) {
            k9Var.a();
            this.p = null;
        }
    }

    @DexIgnore
    public final void a(View view, int i2, float f2, float f3) {
        this.o = true;
        if (Build.VERSION.SDK_INT >= 21) {
            drawableHotspotChanged(f2, f3);
        }
        if (!isPressed()) {
            setPressed(true);
        }
        layoutChildren();
        int i3 = this.j;
        if (i3 != -1) {
            View childAt = getChildAt(i3 - getFirstVisiblePosition());
            if (!(childAt == null || childAt == view || !childAt.isPressed())) {
                childAt.setPressed(false);
            }
        }
        this.j = i2;
        float left = f2 - ((float) view.getLeft());
        float top = f3 - ((float) view.getTop());
        if (Build.VERSION.SDK_INT >= 21) {
            view.drawableHotspotChanged(left, top);
        }
        if (!view.isPressed()) {
            view.setPressed(true);
        }
        a(i2, view, f2, f3);
        setSelectorEnabled(false);
        refreshDrawableState();
    }
}
