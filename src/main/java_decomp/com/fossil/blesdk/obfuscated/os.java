package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.load.EncodeStrategy;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class os implements oo<Bitmap> {
    @DexIgnore
    public static /* final */ lo<Integer> b; // = lo.a("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionQuality", 90);
    @DexIgnore
    public static /* final */ lo<Bitmap.CompressFormat> c; // = lo.a("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionFormat");
    @DexIgnore
    public /* final */ hq a;

    @DexIgnore
    public os(hq hqVar) {
        this.a = hqVar;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:21|(2:38|39)|40|41) */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0066, code lost:
        if (r6 == null) goto L_0x0069;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:40:0x00bf */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0061 A[Catch:{ all -> 0x0057 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00bc A[SYNTHETIC, Splitter:B:38:0x00bc] */
    public boolean a(bq<Bitmap> bqVar, File file, mo moVar) {
        Bitmap bitmap = bqVar.get();
        Bitmap.CompressFormat a2 = a(bitmap, moVar);
        xw.a("encode: [%dx%d] %s", Integer.valueOf(bitmap.getWidth()), Integer.valueOf(bitmap.getHeight()), a2);
        try {
            long a3 = qw.a();
            int intValue = ((Integer) moVar.a(b)).intValue();
            boolean z = false;
            so soVar = null;
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                try {
                    soVar = this.a != null ? new so(fileOutputStream, this.a) : fileOutputStream;
                    bitmap.compress(a2, intValue, soVar);
                    soVar.close();
                    z = true;
                } catch (IOException e) {
                    e = e;
                    soVar = fileOutputStream;
                    try {
                        if (Log.isLoggable("BitmapEncoder", 3)) {
                        }
                    } catch (Throwable th) {
                        th = th;
                        if (soVar != null) {
                            soVar.close();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    soVar = fileOutputStream;
                    if (soVar != null) {
                    }
                    throw th;
                }
            } catch (IOException e2) {
                e = e2;
                if (Log.isLoggable("BitmapEncoder", 3)) {
                    Log.d("BitmapEncoder", "Failed to encode Bitmap", e);
                }
            }
            try {
                soVar.close();
            } catch (IOException unused) {
            }
            if (Log.isLoggable("BitmapEncoder", 2)) {
                Log.v("BitmapEncoder", "Compressed with type: " + a2 + " of size " + vw.a(bitmap) + " in " + qw.a(a3) + ", options format: " + moVar.a(c) + ", hasAlpha: " + bitmap.hasAlpha());
            }
            return z;
        } finally {
            xw.a();
        }
    }

    @DexIgnore
    public final Bitmap.CompressFormat a(Bitmap bitmap, mo moVar) {
        Bitmap.CompressFormat compressFormat = (Bitmap.CompressFormat) moVar.a(c);
        if (compressFormat != null) {
            return compressFormat;
        }
        if (bitmap.hasAlpha()) {
            return Bitmap.CompressFormat.PNG;
        }
        return Bitmap.CompressFormat.JPEG;
    }

    @DexIgnore
    public EncodeStrategy a(mo moVar) {
        return EncodeStrategy.TRANSFORMED;
    }
}
