package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.ButtonService;
import io.reactivex.exceptions.OnErrorNotImplementedException;
import java.util.Comparator;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u94 {
    @DexIgnore
    public static /* final */ m94 a; // = new a();
    @DexIgnore
    public static /* final */ p94<Object> b; // = new b();
    @DexIgnore
    public static /* final */ s94<Object> c; // = new l();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements m94 {
        @DexIgnore
        public void run() {
        }

        @DexIgnore
        public String toString() {
            return "EmptyAction";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements p94<Object> {
        @DexIgnore
        public void a(Object obj) {
        }

        @DexIgnore
        public String toString() {
            return "EmptyConsumer";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements r94 {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public void run() {
        }

        @DexIgnore
        public String toString() {
            return "EmptyRunnable";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements p94<Throwable> {
        @DexIgnore
        public void a(Throwable th) {
            ta4.b(th);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements s94<Object> {
        @DexIgnore
        public boolean a(Object obj) {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements q94<Object, Object> {
        @DexIgnore
        public Object apply(Object obj) {
            return obj;
        }

        @DexIgnore
        public String toString() {
            return "IdentityFunction";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements p94<mq4> {
        @DexIgnore
        public void a(mq4 mq4) throws Exception {
            mq4.c(ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements Comparator<Object> {
        @DexIgnore
        public int compare(Object obj, Object obj2) {
            return ((Comparable) obj).compareTo(obj2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements Callable<Object> {
        @DexIgnore
        public Object call() {
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements p94<Throwable> {
        @DexIgnore
        public void a(Throwable th) {
            ta4.b(new OnErrorNotImplementedException(th));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements s94<Object> {
        @DexIgnore
        public boolean a(Object obj) {
            return true;
        }
    }

    /*
    static {
        new g();
        new d();
        new e();
        new k();
        new c();
        new f();
        new j();
        new i();
        new h();
    }
    */

    @DexIgnore
    public static <T> s94<T> a() {
        return c;
    }

    @DexIgnore
    public static <T> p94<T> b() {
        return b;
    }
}
