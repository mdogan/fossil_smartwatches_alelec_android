package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lx implements rx {
    @DexIgnore
    public /* final */ Method a;
    @DexIgnore
    public /* final */ Object b;

    @DexIgnore
    public lx(Object obj, Method method) {
        this.b = obj;
        this.a = method;
    }

    @DexIgnore
    public static Class a(Context context) {
        try {
            return context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement");
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public static rx b(Context context) {
        Class a2 = a(context);
        if (a2 == null) {
            return null;
        }
        Object a3 = a(context, a2);
        if (a3 == null) {
            return null;
        }
        Method b2 = b(context, a2);
        if (b2 == null) {
            return null;
        }
        return new lx(a3, b2);
    }

    @DexIgnore
    public static Object a(Context context, Class cls) {
        try {
            return cls.getDeclaredMethod("getInstance", new Class[]{Context.class}).invoke(cls, new Object[]{context});
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public void a(String str, Bundle bundle) {
        a("fab", str, bundle);
    }

    @DexIgnore
    public static Method b(Context context, Class cls) {
        try {
            return cls.getDeclaredMethod("logEventInternal", new Class[]{String.class, String.class, Bundle.class});
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public void a(String str, String str2, Bundle bundle) {
        try {
            this.a.invoke(this.b, new Object[]{str, str2, bundle});
        } catch (Exception unused) {
        }
    }
}
