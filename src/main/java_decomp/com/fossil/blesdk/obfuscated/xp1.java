package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class xp1 extends mc1 implements wp1 {
    @DexIgnore
    public xp1() {
        super("com.google.android.gms.wearable.internal.IWearableListener");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                a((DataHolder) nc1.a(parcel, DataHolder.CREATOR));
                return true;
            case 2:
                a((yp1) nc1.a(parcel, yp1.CREATOR));
                return true;
            case 3:
                a((aq1) nc1.a(parcel, aq1.CREATOR));
                return true;
            case 4:
                b((aq1) nc1.a(parcel, aq1.CREATOR));
                return true;
            case 5:
                b((List<aq1>) parcel.createTypedArrayList(aq1.CREATOR));
                return true;
            case 6:
                a((fq1) nc1.a(parcel, fq1.CREATOR));
                return true;
            case 7:
                a((pp1) nc1.a(parcel, pp1.CREATOR));
                return true;
            case 8:
                a((lp1) nc1.a(parcel, lp1.CREATOR));
                return true;
            case 9:
                a((dq1) nc1.a(parcel, dq1.CREATOR));
                return true;
            default:
                return false;
        }
    }
}
