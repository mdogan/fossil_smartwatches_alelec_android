package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zm1 implements Parcelable.Creator<ym1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        int i = 0;
        Intent intent = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 == 2) {
                i2 = SafeParcelReader.q(parcel, a);
            } else if (a2 != 3) {
                SafeParcelReader.v(parcel, a);
            } else {
                intent = (Intent) SafeParcelReader.a(parcel, a, Intent.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new ym1(i, i2, intent);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ym1[i];
    }
}
