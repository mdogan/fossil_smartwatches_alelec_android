package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pl0 implements Parcelable.Creator<ol0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        Bundle bundle = null;
        xd0[] xd0Arr = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                bundle = SafeParcelReader.a(parcel, a);
            } else if (a2 != 2) {
                SafeParcelReader.v(parcel, a);
            } else {
                xd0Arr = (xd0[]) SafeParcelReader.b(parcel, a, xd0.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new ol0(bundle, xd0Arr);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ol0[i];
    }
}
