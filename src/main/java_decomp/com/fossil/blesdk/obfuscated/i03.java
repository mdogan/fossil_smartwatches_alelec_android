package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i03 extends j62<j62.b, b, j62.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ Context d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements j62.c {
        @DexIgnore
        public /* final */ List<AppWrapper> a;

        @DexIgnore
        public b(List<AppWrapper> list) {
            wd4.b(list, "apps");
            this.a = list;
        }

        @DexIgnore
        public final List<AppWrapper> a() {
            return this.a;
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = wy2.class.getSimpleName();
        wd4.a((Object) simpleName, "GetApps::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public i03(Context context) {
        wd4.b(context, "mContext");
        this.d = context;
    }

    @DexIgnore
    public void a(j62.b bVar) {
        FLogger.INSTANCE.getLocal().d(e, "executeUseCase GetHybridApps");
        List<AppFilter> allAppFilters = en2.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<AppHelper.a> it = AppHelper.f.b(this.d).iterator();
        while (it.hasNext()) {
            AppHelper.a next = it.next();
            if (TextUtils.isEmpty(next.b()) || !cg4.b(next.b(), this.d.getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), false);
                Iterator<AppFilter> it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter next2 = it2.next();
                    wd4.a((Object) next2, "appFilter");
                    if (wd4.a((Object) next2.getType(), (Object) installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(next2.getDbRowId());
                        installedApp.setCurrentHandGroup(next2.getHour());
                        break;
                    }
                }
                AppWrapper appWrapper = new AppWrapper();
                appWrapper.setInstalledApp(installedApp);
                appWrapper.setUri(next.c());
                appWrapper.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(appWrapper);
            }
        }
        sb4.c(linkedList);
        a().onSuccess(new b(linkedList));
    }
}
