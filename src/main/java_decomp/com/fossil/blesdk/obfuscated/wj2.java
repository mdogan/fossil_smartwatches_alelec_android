package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wj2 {
    @DexIgnore
    public /* final */ GetDianaDeviceSettingUseCase a;
    @DexIgnore
    public /* final */ GetHybridDeviceSettingUseCase b;
    @DexIgnore
    public /* final */ HybridSyncUseCase c;
    @DexIgnore
    public /* final */ DianaSyncUseCase d;

    @DexIgnore
    public wj2(GetDianaDeviceSettingUseCase getDianaDeviceSettingUseCase, GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, HybridSyncUseCase hybridSyncUseCase, DianaSyncUseCase dianaSyncUseCase) {
        wd4.b(getDianaDeviceSettingUseCase, "mGetDianaDeviceSettingUseCase");
        wd4.b(getHybridDeviceSettingUseCase, "mGetHybridDeviceSettingUseCase");
        wd4.b(hybridSyncUseCase, "mHybridSyncUseCase");
        wd4.b(dianaSyncUseCase, "mDianaSyncUseCase");
        this.a = getDianaDeviceSettingUseCase;
        this.b = getHybridDeviceSettingUseCase;
        this.c = hybridSyncUseCase;
        this.d = dianaSyncUseCase;
    }

    @DexIgnore
    public final CoroutineUseCase<pn3, qn3, on3> a(String str) {
        wd4.b(str, "serial");
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
        if (deviceBySerial != null) {
            int i = vj2.a[deviceBySerial.ordinal()];
            if (i == 1 || i == 2 || i == 3) {
                return this.b;
            }
            if (i == 4) {
                return this.a;
            }
        }
        return this.b;
    }

    @DexIgnore
    public final CoroutineUseCase<ar2, br2, zq2> b(String str) {
        wd4.b(str, "serial");
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
        if (deviceBySerial != null) {
            int i = vj2.b[deviceBySerial.ordinal()];
            if (i == 1 || i == 2 || i == 3) {
                return this.c;
            }
            if (i == 4) {
                return this.d;
            }
        }
        return this.d;
    }
}
