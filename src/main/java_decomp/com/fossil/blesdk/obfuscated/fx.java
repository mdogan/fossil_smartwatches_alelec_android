package com.fossil.blesdk.obfuscated;

import java.util.Locale;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fx {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public fx(int i, int i2, boolean z) {
        this.a = i;
        this.b = i2;
        this.c = z;
    }

    @DexIgnore
    public String a(String str) {
        int length = str.length();
        int i = this.b;
        if (length <= i) {
            return str;
        }
        a((RuntimeException) new IllegalArgumentException(String.format(Locale.US, "String is too long, truncating to %d characters", new Object[]{Integer.valueOf(i)})));
        return str.substring(0, this.b);
    }

    @DexIgnore
    public boolean a(Object obj, String str) {
        if (obj != null) {
            return false;
        }
        a((RuntimeException) new NullPointerException(str + " must not be null"));
        return true;
    }

    @DexIgnore
    public boolean a(Map<String, Object> map, String str) {
        if (map.size() < this.a || map.containsKey(str)) {
            return false;
        }
        a((RuntimeException) new IllegalArgumentException(String.format(Locale.US, "Limit of %d attributes reached, skipping attribute", new Object[]{Integer.valueOf(this.a)})));
        return true;
    }

    @DexIgnore
    public final void a(RuntimeException runtimeException) {
        if (!this.c) {
            r44.g().e("Answers", "Invalid user input detected", runtimeException);
            return;
        }
        throw runtimeException;
    }
}
