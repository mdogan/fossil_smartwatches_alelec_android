package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class l82 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView q;
    @DexIgnore
    public /* final */ RecyclerView r;

    @DexIgnore
    public l82(Object obj, View view, int i, ImageView imageView, RecyclerView recyclerView, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
        this.q = imageView;
        this.r = recyclerView;
    }
}
