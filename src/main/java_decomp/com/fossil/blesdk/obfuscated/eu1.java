package com.fossil.blesdk.obfuscated;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class eu1<E> implements Iterable<E> {
    @DexIgnore
    public /* final */ Optional<Iterable<E>> e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends eu1<E> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Iterable iterable, Iterable iterable2) {
            super(iterable);
            this.f = iterable2;
        }

        @DexIgnore
        public Iterator<E> iterator() {
            return this.f.iterator();
        }
    }

    @DexIgnore
    public eu1() {
        this.e = Optional.absent();
    }

    @DexIgnore
    public final Iterable<E> a() {
        return this.e.or(this);
    }

    @DexIgnore
    public final ImmutableSet<E> b() {
        return ImmutableSet.copyOf(a());
    }

    @DexIgnore
    public String toString() {
        return lu1.d(a());
    }

    @DexIgnore
    public static <E> eu1<E> a(Iterable<E> iterable) {
        return iterable instanceof eu1 ? (eu1) iterable : new a(iterable, iterable);
    }

    @DexIgnore
    public eu1(Iterable<E> iterable) {
        tt1.a(iterable);
        this.e = Optional.fromNullable(this == iterable ? null : iterable);
    }

    @DexIgnore
    public final eu1<E> a(ut1<? super E> ut1) {
        return a(lu1.b(a(), ut1));
    }
}
