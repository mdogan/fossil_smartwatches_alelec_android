package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import com.fossil.blesdk.obfuscated.n54;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cx extends w44<Boolean> {
    @DexIgnore
    public boolean k; // = false;
    @DexIgnore
    public by l;

    @DexIgnore
    public static cx w() {
        return (cx) r44.a(cx.class);
    }

    @DexIgnore
    public void a(nx nxVar) {
        if (nxVar == null) {
            throw new NullPointerException("event must not be null");
        } else if (this.k) {
            a("logCustom");
        } else {
            by byVar = this.l;
            if (byVar != null) {
                byVar.a(nxVar);
            }
        }
    }

    @DexIgnore
    public String p() {
        return "com.crashlytics.sdk.android:answers";
    }

    @DexIgnore
    public String r() {
        return "1.4.7.32";
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public boolean u() {
        long lastModified;
        try {
            Context l2 = l();
            PackageManager packageManager = l2.getPackageManager();
            String packageName = l2.getPackageName();
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
            String num = Integer.toString(packageInfo.versionCode);
            String str = packageInfo.versionName == null ? "0.0" : packageInfo.versionName;
            if (Build.VERSION.SDK_INT >= 9) {
                lastModified = packageInfo.firstInstallTime;
            } else {
                lastModified = new File(packageManager.getApplicationInfo(packageName, 0).sourceDir).lastModified();
            }
            this.l = by.a(this, l2, o(), num, str, lastModified);
            this.l.c();
            this.k = new u54().e(l2);
            return true;
        } catch (Exception e) {
            r44.g().e("Answers", "Error retrieving app properties", e);
            return false;
        }
    }

    @DexIgnore
    public String v() {
        return CommonUtils.b(l(), "com.crashlytics.ApiEndpoint");
    }

    @DexIgnore
    public Boolean k() {
        if (!p54.a(l()).a()) {
            r44.g().d("Fabric", "Analytics collection disabled, because data collection is disabled by Firebase.");
            this.l.b();
            return false;
        }
        try {
            c84 a = a84.d().a();
            if (a == null) {
                r44.g().e("Answers", "Failed to retrieve settings");
                return false;
            } else if (a.d.c) {
                r44.g().d("Answers", "Analytics collection enabled");
                this.l.a(a.e, v());
                return true;
            } else {
                r44.g().d("Answers", "Analytics collection disabled");
                this.l.b();
                return false;
            }
        } catch (Exception e) {
            r44.g().e("Answers", "Error dealing with settings", e);
            return false;
        }
    }

    @DexIgnore
    public void a(n54.a aVar) {
        by byVar = this.l;
        if (byVar != null) {
            byVar.a(aVar.b(), aVar.a());
        }
    }

    @DexIgnore
    public final void a(String str) {
        z44 g = r44.g();
        g.w("Answers", "Method " + str + " is not supported when using Crashlytics through Firebase.");
    }
}
