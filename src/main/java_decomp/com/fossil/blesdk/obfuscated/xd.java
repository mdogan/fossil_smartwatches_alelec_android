package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.rd;
import com.fossil.blesdk.obfuscated.td;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xd<T> extends rd<T> implements td.a {
    @DexIgnore
    public /* final */ vd<T> s;
    @DexIgnore
    public qd.a<T> t; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends qd.a<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(int i, qd<T> qdVar) {
            if (qdVar.a()) {
                xd.this.c();
            } else if (!xd.this.h()) {
                if (i == 0 || i == 3) {
                    List<T> list = qdVar.a;
                    if (xd.this.i.i() == 0) {
                        xd xdVar = xd.this;
                        xdVar.i.a(qdVar.b, list, qdVar.c, qdVar.d, xdVar.h.a, xdVar);
                    } else {
                        xd xdVar2 = xd.this;
                        xdVar2.i.b(qdVar.d, list, xdVar2.j, xdVar2.h.d, xdVar2.l, xdVar2);
                    }
                    xd xdVar3 = xd.this;
                    if (xdVar3.g != null) {
                        boolean z = true;
                        boolean z2 = xdVar3.i.size() == 0;
                        boolean z3 = !z2 && qdVar.b == 0 && qdVar.d == 0;
                        int size = xd.this.size();
                        if (z2 || (!(i == 0 && qdVar.c == 0) && (i != 3 || qdVar.d + xd.this.h.a < size))) {
                            z = false;
                        }
                        xd.this.a(z2, z3, z);
                        return;
                    }
                    return;
                }
                throw new IllegalArgumentException("unexpected resultType" + i);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int e;

        @DexIgnore
        public b(int i) {
            this.e = i;
        }

        @DexIgnore
        public void run() {
            if (!xd.this.h()) {
                xd xdVar = xd.this;
                int i = xdVar.h.a;
                if (xdVar.s.isInvalid()) {
                    xd.this.c();
                    return;
                }
                int i2 = this.e * i;
                int min = Math.min(i, xd.this.i.size() - i2);
                xd xdVar2 = xd.this;
                xdVar2.s.dispatchLoadRange(3, i2, min, xdVar2.e, xdVar2.t);
            }
        }
    }

    @DexIgnore
    public xd(vd<T> vdVar, Executor executor, Executor executor2, rd.c<T> cVar, rd.f fVar, int i) {
        super(new td(), executor, executor2, cVar, fVar);
        this.s = vdVar;
        int i2 = this.h.a;
        this.j = i;
        if (this.s.isInvalid()) {
            c();
            return;
        }
        int max = Math.max(this.h.e / i2, 2) * i2;
        this.s.dispatchLoadInitial(true, Math.max(0, ((i - (max / 2)) / i2) * i2), max, i2, this.e, this.t);
    }

    @DexIgnore
    public void a(rd<T> rdVar, rd.e eVar) {
        td<T> tdVar = rdVar.i;
        if (tdVar.isEmpty() || this.i.size() != tdVar.size()) {
            throw new IllegalArgumentException("Invalid snapshot provided - doesn't appear to be a snapshot of this PagedList");
        }
        int i = this.h.a;
        int e = this.i.e() / i;
        int i2 = this.i.i();
        int i3 = 0;
        while (i3 < i2) {
            int i4 = i3 + e;
            int i5 = 0;
            while (i5 < this.i.i()) {
                int i6 = i4 + i5;
                if (!this.i.b(i, i6) || tdVar.b(i, i6)) {
                    break;
                }
                i5++;
            }
            if (i5 > 0) {
                eVar.a(i4 * i, i * i5);
                i3 += i5 - 1;
            }
            i3++;
        }
    }

    @DexIgnore
    public void b(int i, int i2, int i3) {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    public void c(int i, int i2) {
        d(i, i2);
    }

    @DexIgnore
    public md<?, T> d() {
        return this.s;
    }

    @DexIgnore
    public Object e() {
        return Integer.valueOf(this.j);
    }

    @DexIgnore
    public void f(int i) {
        this.f.execute(new b(i));
    }

    @DexIgnore
    public boolean g() {
        return false;
    }

    @DexIgnore
    public void h(int i) {
        td<T> tdVar = this.i;
        rd.f fVar = this.h;
        tdVar.a(i, fVar.b, fVar.a, (td.a) this);
    }

    @DexIgnore
    public void b() {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    public void b(int i, int i2) {
        f(i, i2);
    }

    @DexIgnore
    public void a(int i) {
        e(0, i);
    }

    @DexIgnore
    public void a(int i, int i2, int i3) {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    public void a() {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    public void a(int i, int i2) {
        d(i, i2);
    }
}
