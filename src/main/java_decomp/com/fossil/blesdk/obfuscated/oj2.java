package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oj2 {
    @DexIgnore
    public static final List<Alarm> a(List<com.portfolio.platform.data.source.local.alarm.Alarm> list) {
        wd4.b(list, "$this$toButtonAlarm");
        ArrayList arrayList = new ArrayList();
        for (com.portfolio.platform.data.source.local.alarm.Alarm alarm : list) {
            Alarm alarm2 = new Alarm();
            alarm2.setAlarmMinute(alarm.getTotalMinutes());
            alarm2.setRepeat(alarm.isRepeated());
            alarm2.setDays(alarm.getDays());
            arrayList.add(alarm2);
        }
        return arrayList;
    }
}
