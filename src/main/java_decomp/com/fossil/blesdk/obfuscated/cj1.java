package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cj1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public boolean e; // = true;
    @DexIgnore
    public Boolean f;
    @DexIgnore
    public pg1 g;

    @DexIgnore
    public cj1(Context context, pg1 pg1) {
        ck0.a(context);
        Context applicationContext = context.getApplicationContext();
        ck0.a(applicationContext);
        this.a = applicationContext;
        if (pg1 != null) {
            this.g = pg1;
            this.b = pg1.f;
            this.c = pg1.e;
            this.d = pg1.d;
            this.e = pg1.c;
            Bundle bundle = pg1.g;
            if (bundle != null) {
                this.f = Boolean.valueOf(bundle.getBoolean("dataCollectionDefaultEnabled", true));
            }
        }
    }
}
