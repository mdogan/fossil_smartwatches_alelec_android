package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.internal.LifecycleCallback;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class si0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ LifecycleCallback e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ ri0 g;

    @DexIgnore
    public si0(ri0 ri0, LifecycleCallback lifecycleCallback, String str) {
        this.g = ri0;
        this.e = lifecycleCallback;
        this.f = str;
    }

    @DexIgnore
    public final void run() {
        if (this.g.f > 0) {
            this.e.a(this.g.g != null ? this.g.g.getBundle(this.f) : null);
        }
        if (this.g.f >= 2) {
            this.e.d();
        }
        if (this.g.f >= 3) {
            this.e.c();
        }
        if (this.g.f >= 4) {
            this.e.e();
        }
        if (this.g.f >= 5) {
            this.e.b();
        }
    }
}
