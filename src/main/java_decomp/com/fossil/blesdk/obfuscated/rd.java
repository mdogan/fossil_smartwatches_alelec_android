package com.fossil.blesdk.obfuscated;

import java.lang.ref.WeakReference;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class rd<T> extends AbstractList<T> {
    @DexIgnore
    public /* final */ Executor e;
    @DexIgnore
    public /* final */ Executor f;
    @DexIgnore
    public /* final */ c<T> g;
    @DexIgnore
    public /* final */ f h;
    @DexIgnore
    public /* final */ td<T> i;
    @DexIgnore
    public int j; // = 0;
    @DexIgnore
    public T k; // = null;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public boolean m; // = false;
    @DexIgnore
    public boolean n; // = false;
    @DexIgnore
    public int o; // = Integer.MAX_VALUE;
    @DexIgnore
    public int p; // = Integer.MIN_VALUE;
    @DexIgnore
    public /* final */ AtomicBoolean q; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ ArrayList<WeakReference<e>> r; // = new ArrayList<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ boolean e;
        @DexIgnore
        public /* final */ /* synthetic */ boolean f;
        @DexIgnore
        public /* final */ /* synthetic */ boolean g;

        @DexIgnore
        public a(boolean z, boolean z2, boolean z3) {
            this.e = z;
            this.f = z2;
            this.g = z3;
        }

        @DexIgnore
        public void run() {
            if (this.e) {
                rd.this.g.a();
            }
            if (this.f) {
                rd.this.m = true;
            }
            if (this.g) {
                rd.this.n = true;
            }
            rd.this.a(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ boolean e;
        @DexIgnore
        public /* final */ /* synthetic */ boolean f;

        @DexIgnore
        public b(boolean z, boolean z2) {
            this.e = z;
            this.f = z2;
        }

        @DexIgnore
        public void run() {
            rd.this.a(this.e, this.f);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<T> {
        @DexIgnore
        public abstract void a();

        @DexIgnore
        public abstract void a(T t);

        @DexIgnore
        public abstract void b(T t);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<Key, Value> {
        @DexIgnore
        public /* final */ md<Key, Value> a;
        @DexIgnore
        public /* final */ f b;
        @DexIgnore
        public Executor c;
        @DexIgnore
        public Executor d;
        @DexIgnore
        public c e;
        @DexIgnore
        public Key f;

        @DexIgnore
        public d(md<Key, Value> mdVar, f fVar) {
            if (mdVar == null) {
                throw new IllegalArgumentException("DataSource may not be null");
            } else if (fVar != null) {
                this.a = mdVar;
                this.b = fVar;
            } else {
                throw new IllegalArgumentException("Config may not be null");
            }
        }

        @DexIgnore
        public d<Key, Value> a(Executor executor) {
            this.d = executor;
            return this;
        }

        @DexIgnore
        public d<Key, Value> b(Executor executor) {
            this.c = executor;
            return this;
        }

        @DexIgnore
        public d<Key, Value> a(c cVar) {
            this.e = cVar;
            return this;
        }

        @DexIgnore
        public d<Key, Value> a(Key key) {
            this.f = key;
            return this;
        }

        @DexIgnore
        public rd<Value> a() {
            Executor executor = this.c;
            if (executor != null) {
                Executor executor2 = this.d;
                if (executor2 != null) {
                    return rd.a(this.a, executor, executor2, this.e, this.b, this.f);
                }
                throw new IllegalArgumentException("BackgroundThreadExecutor required");
            }
            throw new IllegalArgumentException("MainThreadExecutor required");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e {
        @DexIgnore
        public abstract void a(int i, int i2);

        @DexIgnore
        public abstract void b(int i, int i2);

        @DexIgnore
        public abstract void c(int i, int i2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ boolean c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ int e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public int a; // = -1;
            @DexIgnore
            public int b; // = -1;
            @DexIgnore
            public int c; // = -1;
            @DexIgnore
            public boolean d; // = true;
            @DexIgnore
            public int e; // = Integer.MAX_VALUE;

            @DexIgnore
            public a a(boolean z) {
                this.d = z;
                return this;
            }

            @DexIgnore
            public a b(int i) {
                if (i >= 1) {
                    this.a = i;
                    return this;
                }
                throw new IllegalArgumentException("Page size must be a positive number");
            }

            @DexIgnore
            public a c(int i) {
                this.b = i;
                return this;
            }

            @DexIgnore
            public a a(int i) {
                this.c = i;
                return this;
            }

            @DexIgnore
            public f a() {
                if (this.b < 0) {
                    this.b = this.a;
                }
                if (this.c < 0) {
                    this.c = this.a * 3;
                }
                if (this.d || this.b != 0) {
                    int i = this.e;
                    if (i == Integer.MAX_VALUE || i >= this.a + (this.b * 2)) {
                        return new f(this.a, this.b, this.d, this.c, this.e);
                    }
                    throw new IllegalArgumentException("Maximum size must be at least pageSize + 2*prefetchDist, pageSize=" + this.a + ", prefetchDist=" + this.b + ", maxSize=" + this.e);
                }
                throw new IllegalArgumentException("Placeholders and prefetch are the only ways to trigger loading of more data in the PagedList, so either placeholders must be enabled, or prefetch distance must be > 0.");
            }
        }

        @DexIgnore
        public f(int i, int i2, boolean z, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = z;
            this.e = i3;
            this.d = i4;
        }
    }

    @DexIgnore
    public rd(td<T> tdVar, Executor executor, Executor executor2, c<T> cVar, f fVar) {
        this.i = tdVar;
        this.e = executor;
        this.f = executor2;
        this.g = cVar;
        this.h = fVar;
        f fVar2 = this.h;
        this.l = (fVar2.b * 2) + fVar2.a;
    }

    @DexIgnore
    public static <K, T> rd<T> a(md<K, T> mdVar, Executor executor, Executor executor2, c<T> cVar, f fVar, K k2) {
        int i2;
        if (mdVar.isContiguous() || !fVar.c) {
            if (!mdVar.isContiguous()) {
                mdVar = ((vd) mdVar).wrapAsContiguousWithoutPlaceholders();
                if (k2 != null) {
                    i2 = ((Integer) k2).intValue();
                    return new ld((kd) mdVar, executor, executor2, cVar, fVar, k2, i2);
                }
            }
            i2 = -1;
            return new ld((kd) mdVar, executor, executor2, cVar, fVar, k2, i2);
        }
        return new xd((vd) mdVar, executor, executor2, cVar, fVar, k2 != null ? ((Integer) k2).intValue() : 0);
    }

    @DexIgnore
    public abstract void a(rd<T> rdVar, e eVar);

    @DexIgnore
    public void c() {
        this.q.set(true);
    }

    @DexIgnore
    public abstract md<?, T> d();

    @DexIgnore
    public void d(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.r.size() - 1; size >= 0; size--) {
                e eVar = (e) this.r.get(size).get();
                if (eVar != null) {
                    eVar.a(i2, i3);
                }
            }
        }
    }

    @DexIgnore
    public abstract Object e();

    @DexIgnore
    public void e(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.r.size() - 1; size >= 0; size--) {
                e eVar = (e) this.r.get(size).get();
                if (eVar != null) {
                    eVar.b(i2, i3);
                }
            }
        }
    }

    @DexIgnore
    public int f() {
        return this.i.j();
    }

    @DexIgnore
    public void g(int i2) {
        if (i2 < 0 || i2 >= size()) {
            throw new IndexOutOfBoundsException("Index: " + i2 + ", Size: " + size());
        }
        this.j = f() + i2;
        h(i2);
        this.o = Math.min(this.o, i2);
        this.p = Math.max(this.p, i2);
        a(true);
    }

    @DexIgnore
    public abstract boolean g();

    @DexIgnore
    public T get(int i2) {
        T t = this.i.get(i2);
        if (t != null) {
            this.k = t;
        }
        return t;
    }

    @DexIgnore
    public abstract void h(int i2);

    @DexIgnore
    public boolean h() {
        return this.q.get();
    }

    @DexIgnore
    public void i(int i2) {
        this.j += i2;
        this.o += i2;
        this.p += i2;
    }

    @DexIgnore
    public List<T> j() {
        if (i()) {
            return this;
        }
        return new wd(this);
    }

    @DexIgnore
    public int size() {
        return this.i.size();
    }

    @DexIgnore
    public void f(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.r.size() - 1; size >= 0; size--) {
                e eVar = (e) this.r.get(size).get();
                if (eVar != null) {
                    eVar.c(i2, i3);
                }
            }
        }
    }

    @DexIgnore
    public boolean i() {
        return h();
    }

    @DexIgnore
    public void a(boolean z, boolean z2, boolean z3) {
        if (this.g != null) {
            if (this.o == Integer.MAX_VALUE) {
                this.o = this.i.size();
            }
            if (this.p == Integer.MIN_VALUE) {
                this.p = 0;
            }
            if (z || z2 || z3) {
                this.e.execute(new a(z, z2, z3));
                return;
            }
            return;
        }
        throw new IllegalStateException("Can't defer BoundaryCallback, no instance");
    }

    @DexIgnore
    public void a(boolean z) {
        boolean z2 = true;
        boolean z3 = this.m && this.o <= this.h.b;
        if (!this.n || this.p < (size() - 1) - this.h.b) {
            z2 = false;
        }
        if (z3 || z2) {
            if (z3) {
                this.m = false;
            }
            if (z2) {
                this.n = false;
            }
            if (z) {
                this.e.execute(new b(z3, z2));
            } else {
                a(z3, z2);
            }
        }
    }

    @DexIgnore
    public void a(boolean z, boolean z2) {
        if (z) {
            this.g.b(this.i.c());
        }
        if (z2) {
            this.g.a(this.i.d());
        }
    }

    @DexIgnore
    public void a(List<T> list, e eVar) {
        if (!(list == null || list == this)) {
            if (!list.isEmpty()) {
                a((rd) list, eVar);
            } else if (!this.i.isEmpty()) {
                eVar.b(0, this.i.size());
            }
        }
        for (int size = this.r.size() - 1; size >= 0; size--) {
            if (((e) this.r.get(size).get()) == null) {
                this.r.remove(size);
            }
        }
        this.r.add(new WeakReference(eVar));
    }

    @DexIgnore
    public void a(e eVar) {
        for (int size = this.r.size() - 1; size >= 0; size--) {
            e eVar2 = (e) this.r.get(size).get();
            if (eVar2 == null || eVar2 == eVar) {
                this.r.remove(size);
            }
        }
    }
}
