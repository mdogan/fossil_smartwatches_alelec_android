package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.j62;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sx2 extends j62<b, c, j62.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements j62.b {
        @DexIgnore
        public /* final */ ContactGroup a;

        @DexIgnore
        public b(ContactGroup contactGroup) {
            wd4.b(contactGroup, "contactGroup");
            this.a = contactGroup;
        }

        @DexIgnore
        public final ContactGroup a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements j62.c {
        @DexIgnore
        public c(boolean z) {
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = sx2.class.getSimpleName();
        wd4.a((Object) simpleName, "RemoveContactGroup::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public sx2(NotificationsRepository notificationsRepository) {
        wd4.b(notificationsRepository, "notificationsRepository");
        tt1.a(notificationsRepository, "notificationsRepository cannot be null!", new Object[0]);
        wd4.a((Object) notificationsRepository, "Preconditions.checkNotNu\u2026ository cannot be null!\")");
        this.d = notificationsRepository;
    }

    @DexIgnore
    public void a(b bVar) {
        wd4.b(bVar, "requestValues");
        a(bVar.a());
        FLogger.INSTANCE.getLocal().d(e, "Inside .RemoveContactGroup done");
        a().onSuccess(new c(true));
    }

    @DexIgnore
    public final void a(ContactGroup contactGroup) {
        Contact contact = contactGroup.getContacts().get(0);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        StringBuilder sb = new StringBuilder();
        sb.append("Removed contact = ");
        wd4.a((Object) contact, "contact");
        sb.append(contact.getFirstName());
        sb.append(" row id = ");
        sb.append(contact.getDbRowId());
        local.d(str, sb.toString());
        ArrayList arrayList = new ArrayList();
        for (PhoneNumber next : contact.getPhoneNumbers()) {
            wd4.a((Object) next, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            arrayList.add(new PhoneFavoritesContact(next.getNumber()));
        }
        this.d.removeContact(contact);
        this.d.removeContactGroup(contactGroup);
        a((List<? extends PhoneFavoritesContact>) arrayList);
    }

    @DexIgnore
    public final void a(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact removePhoneFavoritesContact : list) {
            this.d.removePhoneFavoritesContact(removePhoneFavoritesContact);
        }
    }
}
