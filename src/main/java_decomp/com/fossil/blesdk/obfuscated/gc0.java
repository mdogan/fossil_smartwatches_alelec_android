package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.google.android.gms.common.api.Status;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gc0 implements Runnable {
    @DexIgnore
    public static /* final */ dm0 g; // = new dm0("RevokeAccessOperation", new String[0]);
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ ff0 f; // = new ff0((he0) null);

    @DexIgnore
    public gc0(String str) {
        ck0.b(str);
        this.e = str;
    }

    @DexIgnore
    public static ie0<Status> a(String str) {
        if (str == null) {
            return je0.a(new Status(4), (he0) null);
        }
        gc0 gc0 = new gc0(str);
        new Thread(gc0).start();
        return gc0.f;
    }

    @DexIgnore
    public final void run() {
        Status status = Status.k;
        try {
            String valueOf = String.valueOf(this.e);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(valueOf.length() != 0 ? "https://accounts.google.com/o/oauth2/revoke?token=".concat(valueOf) : new String("https://accounts.google.com/o/oauth2/revoke?token=")).openConnection();
            httpURLConnection.setRequestProperty(GraphRequest.CONTENT_TYPE_HEADER, "application/x-www-form-urlencoded");
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                status = Status.i;
            } else {
                g.b("Unable to revoke access!", new Object[0]);
            }
            dm0 dm0 = g;
            StringBuilder sb = new StringBuilder(26);
            sb.append("Response Code: ");
            sb.append(responseCode);
            dm0.a(sb.toString(), new Object[0]);
        } catch (IOException e2) {
            dm0 dm02 = g;
            String valueOf2 = String.valueOf(e2.toString());
            dm02.b(valueOf2.length() != 0 ? "IOException when revoking access: ".concat(valueOf2) : new String("IOException when revoking access: "), new Object[0]);
        } catch (Exception e3) {
            dm0 dm03 = g;
            String valueOf3 = String.valueOf(e3.toString());
            dm03.b(valueOf3.length() != 0 ? "Exception when revoking access: ".concat(valueOf3) : new String("Exception when revoking access: "), new Object[0]);
        }
        this.f.a(status);
    }
}
