package com.fossil.blesdk.obfuscated;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uw1 implements bx1, cx1 {
    @DexIgnore
    public /* final */ Map<Class<?>, ConcurrentHashMap<ax1<Object>, Executor>> a; // = new HashMap();
    @DexIgnore
    public Queue<zw1<?>> b; // = new ArrayDeque();
    @DexIgnore
    public /* final */ Executor c;

    @DexIgnore
    public uw1(Executor executor) {
        this.c = executor;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r0.hasNext() == false) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        r1 = r0.next();
        ((java.util.concurrent.Executor) r1.getValue()).execute(com.fossil.blesdk.obfuscated.vw1.a(r1, r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        r0 = b(r4).iterator();
     */
    @DexIgnore
    public void a(zw1<?> zw1) {
        ck0.a(zw1);
        synchronized (this) {
            if (this.b != null) {
                this.b.add(zw1);
            }
        }
    }

    @DexIgnore
    public final synchronized Set<Map.Entry<ax1<Object>, Executor>> b(zw1<?> zw1) {
        Map map = this.a.get(zw1.b());
        if (map == null) {
            return Collections.emptySet();
        }
        return map.entrySet();
    }

    @DexIgnore
    public synchronized <T> void a(Class<T> cls, Executor executor, ax1<? super T> ax1) {
        ck0.a(cls);
        ck0.a(ax1);
        ck0.a(executor);
        if (!this.a.containsKey(cls)) {
            this.a.put(cls, new ConcurrentHashMap());
        }
        this.a.get(cls).put(ax1, executor);
    }

    @DexIgnore
    public <T> void a(Class<T> cls, ax1<? super T> ax1) {
        a(cls, this.c, ax1);
    }

    @DexIgnore
    public final void a() {
        Queue<zw1<?>> queue;
        synchronized (this) {
            if (this.b != null) {
                queue = this.b;
                this.b = null;
            } else {
                queue = null;
            }
        }
        if (queue != null) {
            for (zw1 a2 : queue) {
                a(a2);
            }
        }
    }
}
