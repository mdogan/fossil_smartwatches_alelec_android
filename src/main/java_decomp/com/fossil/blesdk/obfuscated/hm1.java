package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hm1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ wi1 e;
    @DexIgnore
    public /* final */ /* synthetic */ gm1 f;

    @DexIgnore
    public hm1(gm1 gm1, wi1 wi1) {
        this.f = gm1;
        this.e = wi1;
    }

    @DexIgnore
    public final void run() {
        this.e.b();
        if (vl1.a()) {
            this.e.a().a((Runnable) this);
            return;
        }
        boolean d = this.f.d();
        long unused = this.f.c = 0;
        if (d) {
            this.f.c();
        }
    }
}
