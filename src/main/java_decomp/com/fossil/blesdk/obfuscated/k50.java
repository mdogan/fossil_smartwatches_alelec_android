package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.GetFilePhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class k50 extends GetFilePhase {
    @DexIgnore
    public /* final */ boolean R;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ k50(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, HashMap hashMap, String str, int i, rd4 rd4) {
        this(peripheral, aVar, phaseId, r4, str);
        HashMap hashMap2 = (i & 8) != 0 ? new HashMap() : hashMap;
        if ((i & 16) != 0) {
            str = UUID.randomUUID().toString();
            wd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public final byte[][] O() {
        ArrayList<DeviceFile> J = J();
        ArrayList arrayList = new ArrayList(pb4.a(J, 10));
        for (DeviceFile rawData : J) {
            arrayList.add(rawData.getRawData());
        }
        Object[] array = arrayList.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public boolean c() {
        return this.R;
    }

    @DexIgnore
    public byte[][] i() {
        return O();
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public k50(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, HashMap<GetFilePhase.GetFileOption, Object> hashMap, String str) {
        super(peripheral, aVar, phaseId, a50.b.a(peripheral.k(), FileType.HARDWARE_LOG), hashMap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, r8, 32, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(phaseId, "phaseId");
        wd4.b(hashMap, "options");
        String str2 = str;
        wd4.b(str2, "phaseUuid");
        this.R = true;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ k50(Peripheral peripheral, Phase.a aVar, HashMap hashMap, String str, int i, rd4 rd4) {
        this(peripheral, aVar, hashMap, str);
        hashMap = (i & 4) != 0 ? new HashMap() : hashMap;
        if ((i & 8) != 0) {
            str = UUID.randomUUID().toString();
            wd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public k50(Peripheral peripheral, Phase.a aVar, HashMap<GetFilePhase.GetFileOption, Object> hashMap, String str) {
        this(peripheral, aVar, PhaseId.GET_HARDWARE_LOG, hashMap, str);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(hashMap, "options");
        wd4.b(str, "phaseUuid");
    }
}
