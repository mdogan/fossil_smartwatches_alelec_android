package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mc0 extends fc0 {
    @DexIgnore
    public /* final */ /* synthetic */ lc0 e;

    @DexIgnore
    public mc0(lc0 lc0) {
        this.e = lc0;
    }

    @DexIgnore
    public final void a(Status status) throws RemoteException {
        this.e.a(status);
    }
}
