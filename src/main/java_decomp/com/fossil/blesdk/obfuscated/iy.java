package com.fossil.blesdk.obfuscated;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class iy {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Map<String, Object> b; // = new HashMap();

    @DexIgnore
    public iy(String str) {
        this.a = str;
    }

    @DexIgnore
    public iy a(String str, String str2) {
        this.b.put(str, str2);
        return this;
    }

    @DexIgnore
    public nx a() {
        nx nxVar = new nx(this.a);
        for (String next : this.b.keySet()) {
            Object obj = this.b.get(next);
            if (obj instanceof String) {
                nxVar.a(next, (String) obj);
            } else if (obj instanceof Number) {
                nxVar.a(next, (Number) obj);
            }
        }
        return nxVar;
    }
}
