package com.fossil.blesdk.obfuscated;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.we0;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xh0<ResultT> extends gh0 {
    @DexIgnore
    public /* final */ gf0<ee0.b, ResultT> a;
    @DexIgnore
    public /* final */ yn1<ResultT> b;
    @DexIgnore
    public /* final */ ef0 c;

    @DexIgnore
    public xh0(int i, gf0<ee0.b, ResultT> gf0, yn1<ResultT> yn1, ef0 ef0) {
        super(i);
        this.b = yn1;
        this.a = gf0;
        this.c = ef0;
    }

    @DexIgnore
    public final void a(we0.a<?> aVar) throws DeadObjectException {
        try {
            this.a.a(aVar.f(), this.b);
        } catch (DeadObjectException e) {
            throw e;
        } catch (RemoteException e2) {
            a(jg0.a(e2));
        } catch (RuntimeException e3) {
            a(e3);
        }
    }

    @DexIgnore
    public final xd0[] b(we0.a<?> aVar) {
        return this.a.b();
    }

    @DexIgnore
    public final boolean c(we0.a<?> aVar) {
        return this.a.a();
    }

    @DexIgnore
    public final void a(Status status) {
        this.b.b(this.c.a(status));
    }

    @DexIgnore
    public final void a(RuntimeException runtimeException) {
        this.b.b((Exception) runtimeException);
    }

    @DexIgnore
    public final void a(kf0 kf0, boolean z) {
        kf0.a(this.b, z);
    }
}
