package com.fossil.blesdk.obfuscated;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class md4 {
    @DexIgnore
    public static final <T> Iterator<T> a(T[] tArr) {
        wd4.b(tArr, "array");
        return new ld4(tArr);
    }
}
