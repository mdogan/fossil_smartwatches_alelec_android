package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import android.util.Log;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cy1 {
    @DexIgnore
    public static /* final */ long d; // = TimeUnit.DAYS.toMillis(7);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore
    public cy1(String str, String str2, long j) {
        this.a = str;
        this.b = str2;
        this.c = j;
    }

    @DexIgnore
    public static String a(String str, String str2, long j) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("token", str);
            jSONObject.put("appVersion", str2);
            jSONObject.put("timestamp", j);
            return jSONObject.toString();
        } catch (JSONException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 24);
            sb.append("Failed to encode token: ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public static cy1 b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (!str.startsWith("{")) {
            return new cy1(str, (String) null, 0);
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            return new cy1(jSONObject.getString("token"), jSONObject.getString("appVersion"), jSONObject.getLong("timestamp"));
        } catch (JSONException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
            sb.append("Failed to parse token: ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public static String a(cy1 cy1) {
        if (cy1 == null) {
            return null;
        }
        return cy1.a;
    }

    @DexIgnore
    public final boolean a(String str) {
        return System.currentTimeMillis() > this.c + d || !str.equals(this.b);
    }
}
