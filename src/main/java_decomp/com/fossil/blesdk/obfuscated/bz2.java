package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bz2 implements Factory<HomeAlertsHybridPresenter> {
    @DexIgnore
    public static HomeAlertsHybridPresenter a(zy2 zy2, AlarmHelper alarmHelper, SetAlarms setAlarms, AlarmsRepository alarmsRepository, fn2 fn2) {
        return new HomeAlertsHybridPresenter(zy2, alarmHelper, setAlarms, alarmsRepository, fn2);
    }
}
