package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.fitness.data.DataType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rp0 implements Parcelable.Creator<ap0> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        DataType dataType = null;
        String str = null;
        bp0 bp0 = null;
        kp0 kp0 = null;
        String str2 = null;
        int[] iArr = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 1:
                    dataType = SafeParcelReader.a(parcel, a, DataType.CREATOR);
                    break;
                case 2:
                    str = SafeParcelReader.f(parcel, a);
                    break;
                case 3:
                    i = SafeParcelReader.q(parcel, a);
                    break;
                case 4:
                    bp0 = SafeParcelReader.a(parcel, a, bp0.CREATOR);
                    break;
                case 5:
                    kp0 = SafeParcelReader.a(parcel, a, kp0.CREATOR);
                    break;
                case 6:
                    str2 = SafeParcelReader.f(parcel, a);
                    break;
                case 8:
                    iArr = SafeParcelReader.e(parcel, a);
                    break;
                default:
                    SafeParcelReader.v(parcel, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel, b);
        return new ap0(dataType, str, i, bp0, kp0, str2, iArr);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ap0[i];
    }
}
