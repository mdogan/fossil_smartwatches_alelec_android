package com.fossil.blesdk.obfuscated;

import kotlin.Result;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wk4 {
    @DexIgnore
    public static final <T> void a(jd4<? super kc4<? super T>, ? extends Object> jd4, kc4<? super T> kc4) {
        wd4.b(jd4, "$this$startCoroutineCancellable");
        wd4.b(kc4, "completion");
        try {
            xh4.a(IntrinsicsKt__IntrinsicsJvmKt.a(IntrinsicsKt__IntrinsicsJvmKt.a(jd4, kc4)), cb4.a);
        } catch (Throwable th) {
            Result.a aVar = Result.Companion;
            kc4.resumeWith(Result.m3constructorimpl(za4.a(th)));
        }
    }

    @DexIgnore
    public static final <R, T> void a(kd4<? super R, ? super kc4<? super T>, ? extends Object> kd4, R r, kc4<? super T> kc4) {
        wd4.b(kd4, "$this$startCoroutineCancellable");
        wd4.b(kc4, "completion");
        try {
            xh4.a(IntrinsicsKt__IntrinsicsJvmKt.a(IntrinsicsKt__IntrinsicsJvmKt.a(kd4, r, kc4)), cb4.a);
        } catch (Throwable th) {
            Result.a aVar = Result.Companion;
            kc4.resumeWith(Result.m3constructorimpl(za4.a(th)));
        }
    }
}
