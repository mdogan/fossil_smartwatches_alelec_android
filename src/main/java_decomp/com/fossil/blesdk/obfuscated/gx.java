package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.crashlytics.android.answers.SessionEvent;
import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gx implements s64 {
    @DexIgnore
    public /* final */ w44 a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ hx c;
    @DexIgnore
    public /* final */ fy d;
    @DexIgnore
    public /* final */ a74 e;
    @DexIgnore
    public /* final */ sx f;
    @DexIgnore
    public /* final */ ScheduledExecutorService g;
    @DexIgnore
    public cy h; // = new ox();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ k74 e;
        @DexIgnore
        public /* final */ /* synthetic */ String f;

        @DexIgnore
        public a(k74 k74, String str) {
            this.e = k74;
            this.f = str;
        }

        @DexIgnore
        public void run() {
            try {
                gx.this.h.a(this.e, this.f);
            } catch (Exception e2) {
                r44.g().e("Answers", "Failed to set analytics settings data", e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            try {
                cy cyVar = gx.this.h;
                gx.this.h = new ox();
                cyVar.d();
            } catch (Exception e2) {
                r44.g().e("Answers", "Failed to disable events", e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            try {
                gx.this.h.a();
            } catch (Exception e2) {
                r44.g().e("Answers", "Failed to send events files", e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            try {
                dy a = gx.this.d.a();
                zx a2 = gx.this.c.a();
                a2.a((s64) gx.this);
                gx.this.h = new px(gx.this.a, gx.this.b, gx.this.g, a2, gx.this.e, a, gx.this.f);
            } catch (Exception e2) {
                r44.g().e("Answers", "Failed to enable events", e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void run() {
            try {
                gx.this.h.b();
            } catch (Exception e2) {
                r44.g().e("Answers", "Failed to flush events", e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ SessionEvent.b e;
        @DexIgnore
        public /* final */ /* synthetic */ boolean f;

        @DexIgnore
        public f(SessionEvent.b bVar, boolean z) {
            this.e = bVar;
            this.f = z;
        }

        @DexIgnore
        public void run() {
            try {
                gx.this.h.a(this.e);
                if (this.f) {
                    gx.this.h.b();
                }
            } catch (Exception e2) {
                r44.g().e("Answers", "Failed to process event", e2);
            }
        }
    }

    @DexIgnore
    public gx(w44 w44, Context context, hx hxVar, fy fyVar, a74 a74, ScheduledExecutorService scheduledExecutorService, sx sxVar) {
        this.a = w44;
        this.b = context;
        this.c = hxVar;
        this.d = fyVar;
        this.e = a74;
        this.g = scheduledExecutorService;
        this.f = sxVar;
    }

    @DexIgnore
    public void a(SessionEvent.b bVar) {
        a(bVar, false, false);
    }

    @DexIgnore
    public void b(SessionEvent.b bVar) {
        a(bVar, false, true);
    }

    @DexIgnore
    public void c(SessionEvent.b bVar) {
        a(bVar, true, false);
    }

    @DexIgnore
    public void a(k74 k74, String str) {
        a((Runnable) new a(k74, str));
    }

    @DexIgnore
    public void b() {
        a((Runnable) new d());
    }

    @DexIgnore
    public void c() {
        a((Runnable) new e());
    }

    @DexIgnore
    public void a() {
        a((Runnable) new b());
    }

    @DexIgnore
    public final void b(Runnable runnable) {
        try {
            this.g.submit(runnable).get();
        } catch (Exception e2) {
            r44.g().e("Answers", "Failed to run events task", e2);
        }
    }

    @DexIgnore
    public void a(String str) {
        a((Runnable) new c());
    }

    @DexIgnore
    public void a(SessionEvent.b bVar, boolean z, boolean z2) {
        f fVar = new f(bVar, z2);
        if (z) {
            b((Runnable) fVar);
        } else {
            a((Runnable) fVar);
        }
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        try {
            this.g.submit(runnable);
        } catch (Exception e2) {
            r44.g().e("Answers", "Failed to submit events task", e2);
        }
    }
}
