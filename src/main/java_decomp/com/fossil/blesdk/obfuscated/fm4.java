package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.om4;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fm4 {
    @DexIgnore
    public int a; // = 64;
    @DexIgnore
    public int b; // = 5;
    @DexIgnore
    public Runnable c;
    @DexIgnore
    public ExecutorService d;
    @DexIgnore
    public /* final */ Deque<om4.b> e; // = new ArrayDeque();
    @DexIgnore
    public /* final */ Deque<om4.b> f; // = new ArrayDeque();
    @DexIgnore
    public /* final */ Deque<om4> g; // = new ArrayDeque();

    @DexIgnore
    public fm4(ExecutorService executorService) {
        this.d = executorService;
    }

    @DexIgnore
    public synchronized ExecutorService a() {
        if (this.d == null) {
            this.d = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), vm4.a("OkHttp Dispatcher", false));
        }
        return this.d;
    }

    @DexIgnore
    public void b(int i) {
        if (i >= 1) {
            synchronized (this) {
                this.b = i;
            }
            b();
            return;
        }
        throw new IllegalArgumentException("max < 1: " + i);
    }

    @DexIgnore
    public final int c(om4.b bVar) {
        int i = 0;
        for (om4.b next : this.f) {
            if (!next.c().j && next.d().equals(bVar.d())) {
                i++;
            }
        }
        return i;
    }

    @DexIgnore
    public synchronized int c() {
        return this.f.size() + this.g.size();
    }

    @DexIgnore
    public void a(int i) {
        if (i >= 1) {
            synchronized (this) {
                this.a = i;
            }
            b();
            return;
        }
        throw new IllegalArgumentException("max < 1: " + i);
    }

    @DexIgnore
    public final boolean b() {
        int i;
        boolean z;
        ArrayList arrayList = new ArrayList();
        synchronized (this) {
            Iterator<om4.b> it = this.e.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                om4.b next = it.next();
                if (this.f.size() >= this.a) {
                    break;
                } else if (c(next) < this.b) {
                    it.remove();
                    arrayList.add(next);
                    this.f.add(next);
                }
            }
            z = c() > 0;
        }
        int size = arrayList.size();
        for (i = 0; i < size; i++) {
            ((om4.b) arrayList.get(i)).a(a());
        }
        return z;
    }

    @DexIgnore
    public fm4() {
    }

    @DexIgnore
    public void a(om4.b bVar) {
        synchronized (this) {
            this.e.add(bVar);
        }
        b();
    }

    @DexIgnore
    public synchronized void a(om4 om4) {
        this.g.add(om4);
    }

    @DexIgnore
    public final <T> void a(Deque<T> deque, T t) {
        Runnable runnable;
        synchronized (this) {
            if (deque.remove(t)) {
                runnable = this.c;
            } else {
                throw new AssertionError("Call wasn't in-flight!");
            }
        }
        if (!b() && runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void b(om4.b bVar) {
        a(this.f, bVar);
    }

    @DexIgnore
    public void b(om4 om4) {
        a(this.g, om4);
    }
}
