package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ed2 extends dd2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z; // = new SparseIntArray();
    @DexIgnore
    public /* final */ RelativeLayout w;
    @DexIgnore
    public long x;

    /*
    static {
        z.put(R.id.rv_preset, 1);
        z.put(R.id.tv_tap_icon_to_customize, 2);
        z.put(R.id.cpi_preset, 3);
        z.put(R.id.cl_updating_fw, 4);
        z.put(R.id.tv_title, 5);
        z.put(R.id.tv_desc, 6);
        z.put(R.id.pb_ota, 7);
        z.put(R.id.cl_no_device, 8);
        z.put(R.id.ftv_customize, 9);
        z.put(R.id.iv_avatar, 10);
        z.put(R.id.ftv_pair_watch, 11);
        z.put(R.id.ftv_description, 12);
    }
    */

    @DexIgnore
    public ed2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 13, y, z));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    public ed2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[8], objArr[4], objArr[3], objArr[9], objArr[12], objArr[11], objArr[10], objArr[7], objArr[1], objArr[6], objArr[2], objArr[5]);
        this.x = -1;
        this.w = objArr[0];
        this.w.setTag((Object) null);
        a(view);
        f();
    }
}
