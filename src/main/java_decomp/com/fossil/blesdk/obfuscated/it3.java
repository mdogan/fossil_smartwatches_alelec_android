package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;
import com.fossil.blesdk.obfuscated.mt3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class it3 {
    @DexIgnore
    public static /* final */ String a; // = "it3";

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ Bitmap b;
        @DexIgnore
        public /* final */ lt3 c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ jt3 e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.it3$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.it3$a$a  reason: collision with other inner class name */
        public class C0086a implements mt3.b {
            @DexIgnore
            public /* final */ /* synthetic */ ImageView a;

            @DexIgnore
            public C0086a(ImageView imageView) {
                this.a = imageView;
            }

            @DexIgnore
            public void a(BitmapDrawable bitmapDrawable) {
                jt3 jt3 = a.this.e;
                if (jt3 == null) {
                    this.a.setImageDrawable(bitmapDrawable);
                } else {
                    jt3.a(bitmapDrawable);
                }
            }
        }

        @DexIgnore
        public a(Context context, Bitmap bitmap, lt3 lt3, boolean z, jt3 jt3) {
            this.a = context;
            this.b = bitmap;
            this.c = lt3;
            this.d = z;
            this.e = jt3;
        }

        @DexIgnore
        public void a(ImageView imageView) {
            this.c.a = this.b.getWidth();
            this.c.b = this.b.getHeight();
            if (this.d) {
                new mt3(imageView.getContext(), this.b, this.c, new C0086a(imageView)).a();
            } else {
                imageView.setImageDrawable(new BitmapDrawable(this.a.getResources(), ht3.a(imageView.getContext(), this.b, this.c)));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ View a;
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ lt3 c; // = new lt3();
        @DexIgnore
        public boolean d;
        @DexIgnore
        public jt3 e;

        @DexIgnore
        public b(Context context) {
            this.b = context;
            this.a = new View(context);
            this.a.setTag(it3.a);
        }

        @DexIgnore
        public b a(int i) {
            this.c.c = i;
            return this;
        }

        @DexIgnore
        public b b(int i) {
            this.c.d = i;
            return this;
        }

        @DexIgnore
        public a a(Bitmap bitmap) {
            return new a(this.b, bitmap, this.c, this.d, this.e);
        }
    }

    @DexIgnore
    public static b a(Context context) {
        return new b(context);
    }
}
