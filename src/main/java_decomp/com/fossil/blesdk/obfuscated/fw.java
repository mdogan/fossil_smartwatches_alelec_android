package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface fw<R> {

    @DexIgnore
    public interface a {
    }

    @DexIgnore
    boolean a(R r, a aVar);
}
