package com.fossil.blesdk.obfuscated;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.view.DragEvent;
import android.view.View;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o13 implements View.OnDragListener {
    @DexIgnore
    public Handler a;
    @DexIgnore
    public b b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(String str);

        @DexIgnore
        boolean a(View view, String str);

        @DexIgnore
        void b(String str);

        @DexIgnore
        boolean c(String str);
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public o13(b bVar) {
        this.b = bVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0030, code lost:
        r0 = r1.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0034, code lost:
        r0.b(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0040, code lost:
        r0 = r1.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0042, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0044, code lost:
        r0.b(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    @DexIgnore
    public final void a(DragEvent dragEvent) {
        ClipDescription clipDescription = dragEvent.getClipDescription();
        if (clipDescription != null) {
            String obj = clipDescription.getLabel().toString();
            switch (obj.hashCode()) {
                case -1787753871:
                    if (!obj.equals("WATCH_APP")) {
                        return;
                    }
                    break;
                case 137912893:
                    if (!obj.equals("SWAP_PRESET_WATCH_APP")) {
                        return;
                    }
                    break;
                case 333273330:
                    if (!obj.equals("SWAP_PRESET_COMPLICATION")) {
                        return;
                    }
                    break;
                case 672879678:
                    if (!obj.equals("COMPLICATION")) {
                        return;
                    }
                    break;
                default:
                    return;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0030, code lost:
        r0 = r2.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        if (r0 == null) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0034, code lost:
        r0.removeCallbacksAndMessages((java.lang.Object) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0038, code lost:
        r0 = r2.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003a, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003c, code lost:
        r0.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0048, code lost:
        r0 = r2.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004a, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004c, code lost:
        r0.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    @DexIgnore
    public final void b(DragEvent dragEvent) {
        ClipDescription clipDescription = dragEvent.getClipDescription();
        if (clipDescription != null) {
            String obj = clipDescription.getLabel().toString();
            switch (obj.hashCode()) {
                case -1787753871:
                    if (!obj.equals("WATCH_APP")) {
                        return;
                    }
                    break;
                case 137912893:
                    if (!obj.equals("SWAP_PRESET_WATCH_APP")) {
                        return;
                    }
                    break;
                case 333273330:
                    if (!obj.equals("SWAP_PRESET_COMPLICATION")) {
                        return;
                    }
                    break;
                case 672879678:
                    if (!obj.equals("COMPLICATION")) {
                        return;
                    }
                    break;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public boolean onDrag(View view, DragEvent dragEvent) {
        DragEvent dragEvent2 = dragEvent;
        if (dragEvent2 != null) {
            int action = dragEvent.getAction();
            ClipData clipData = dragEvent.getClipData();
            ClipDescription clipDescription = dragEvent.getClipDescription();
            Integer num = null;
            CharSequence label = clipDescription != null ? clipDescription.getLabel() : null;
            if (clipData != null) {
                int i = 0;
                int itemCount = clipData.getItemCount() - 1;
                if (itemCount >= 0) {
                    while (true) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        StringBuilder sb = new StringBuilder();
                        sb.append("onDrag - v=");
                        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
                        sb.append(", action=");
                        sb.append(action);
                        sb.append(", labelDesc=");
                        sb.append(label);
                        sb.append(", item=");
                        sb.append(clipData.getItemAt(i));
                        local.d("CustomizeDragListener", sb.toString());
                        if (i == itemCount) {
                            break;
                        }
                        i++;
                    }
                }
            } else {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("onDrag - v=");
                if (view != null) {
                    num = Integer.valueOf(view.getId());
                }
                sb2.append(num);
                sb2.append(", action=");
                sb2.append(action);
                sb2.append(", labelDesc=");
                sb2.append(label);
                local2.d("CustomizeDragListener", sb2.toString());
            }
            if (action != 1) {
                if (action != 3) {
                    if (action != 4) {
                        if (action == 5) {
                            a(dragEvent2);
                        } else if (action == 6) {
                            b(dragEvent2);
                        }
                    } else if (!dragEvent.getResult()) {
                        b bVar = this.b;
                        if (bVar != null) {
                            bVar.a();
                        }
                    }
                } else if (view != null) {
                    a(view, dragEvent);
                }
            } else if (this.a == null) {
                this.a = new Handler(Looper.getMainLooper());
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0033, code lost:
        if (r1.equals("COMPLICATION") != false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003c, code lost:
        if (r1.equals("SWAP_PRESET_COMPLICATION") != false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0045, code lost:
        if (r1.equals("SWAP_PRESET_WATCH_APP") != false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r4 = r5.getItemAt(0);
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r4, "clipData.getItemAt(0)");
        r4 = r4.getIntent();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0054, code lost:
        if (r4 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0056, code lost:
        r5 = r3.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0058, code lost:
        if (r5 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005a, code lost:
        r4 = r4.getStringExtra("KEY_POSITION");
        com.fossil.blesdk.obfuscated.wd4.a((java.lang.Object) r4, "it.getStringExtra(KEY_POSITION)");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006a, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006b, code lost:
        r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r5.e("CustomizeDragListener", "processDrop - e=" + r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0087, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008e, code lost:
        if (r1.equals("WATCH_APP") != false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0094, code lost:
        return a(r4, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return r5.c(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return false;
     */
    @DexIgnore
    public final boolean a(View view, DragEvent dragEvent) {
        Handler handler = this.a;
        if (handler != null) {
            handler.removeCallbacksAndMessages((Object) null);
        }
        ClipData clipData = dragEvent.getClipData();
        if (!(clipData == null || clipData.getItemCount() == 0)) {
            ClipDescription description = clipData.getDescription();
            if (description != null) {
                String obj = description.getLabel().toString();
                switch (obj.hashCode()) {
                    case -1787753871:
                        break;
                    case 137912893:
                        break;
                    case 333273330:
                        break;
                    case 672879678:
                        break;
                }
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r1 != null) goto L_0x0018;
     */
    @DexIgnore
    public final boolean a(View view, ClipData clipData) {
        String str;
        ClipData.Item itemAt = clipData.getItemAt(0);
        if (itemAt != null) {
            Intent intent = itemAt.getIntent();
            if (intent != null) {
                str = intent.getStringExtra("KEY_ID");
            }
        }
        str = "";
        if (itemAt == null) {
            return false;
        }
        b bVar = this.b;
        if (bVar != null ? bVar.a(view, str) : false) {
            return true;
        }
        return false;
    }
}
