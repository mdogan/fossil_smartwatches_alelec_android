package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rf3 implements Factory<of3> {
    @DexIgnore
    public static of3 a(qf3 qf3) {
        of3 a = qf3.a();
        o44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
