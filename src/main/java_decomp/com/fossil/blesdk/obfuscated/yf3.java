package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.dg3;
import com.fossil.blesdk.obfuscated.eg3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.indicator.CustomPageIndicator;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yf3 extends as2 implements xf3, View.OnClickListener {
    @DexIgnore
    public static /* final */ a q; // = new a((rd4) null);
    @DexIgnore
    public /* final */ Calendar j; // = Calendar.getInstance();
    @DexIgnore
    public ur3<rf2> k;
    @DexIgnore
    public wf3 l;
    @DexIgnore
    public eg3 m;
    @DexIgnore
    public dg3 n;
    @DexIgnore
    public int o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final yf3 a(Date date) {
            wd4.b(date, "date");
            yf3 yf3 = new yf3();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            yf3.setArguments(bundle);
            return yf3;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.q {
        @DexIgnore
        public /* final */ /* synthetic */ yf3 a;
        @DexIgnore
        public /* final */ /* synthetic */ rf2 b;

        @DexIgnore
        public b(yf3 yf3, rf2 rf2) {
            this.a = yf3;
            this.b = rf2;
        }

        @DexIgnore
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            wd4.b(recyclerView, "recyclerView");
            super.onScrolled(recyclerView, i, i2);
            View childAt = recyclerView.getChildAt(0);
            wd4.a((Object) childAt, "it.getChildAt(0)");
            int measuredWidth = childAt.getMeasuredWidth();
            int computeHorizontalScrollOffset = recyclerView.computeHorizontalScrollOffset();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepDetailFragment", "initUI - offset=" + computeHorizontalScrollOffset);
            if (computeHorizontalScrollOffset % measuredWidth == 0) {
                int i3 = computeHorizontalScrollOffset / measuredWidth;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("SleepDetailFragment", "initUI - position=" + i3);
                if (i3 != -1) {
                    this.a.o = i3;
                    this.b.G.j(i3);
                }
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "SleepDetailFragment";
    }

    @DexIgnore
    public void W() {
        ur3<rf2> ur3 = this.k;
        if (ur3 != null) {
            rf2 a2 = ur3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                if (constraintLayout != null) {
                    constraintLayout.setVisibility(8);
                }
            }
        }
    }

    @DexIgnore
    public void c(ArrayList<dg3.a> arrayList) {
        wd4.b(arrayList, "sleepHeartRateDatumData");
        ur3<rf2> ur3 = this.k;
        if (ur3 != null) {
            rf2 a2 = ur3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                if (constraintLayout != null) {
                    constraintLayout.setVisibility(0);
                }
            }
        }
        dg3 dg3 = this.n;
        if (dg3 != null) {
            dg3.a(arrayList);
        }
    }

    @DexIgnore
    public void onClick(View view) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("SleepDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case R.id.iv_back /*2131362398*/:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case R.id.iv_back_date /*2131362399*/:
                    wf3 wf3 = this.l;
                    if (wf3 != null) {
                        wf3.i();
                        return;
                    }
                    return;
                case R.id.iv_next_date /*2131362447*/:
                    wf3 wf32 = this.l;
                    if (wf32 != null) {
                        wf32.h();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        rf2 rf2 = (rf2) ra.a(layoutInflater, R.layout.fragment_sleep_detail, viewGroup, false, O0());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            wd4.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        Date date = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            date = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        Calendar calendar = this.j;
        wd4.a((Object) calendar, "mCalendar");
        calendar.setTime(date);
        wd4.a((Object) rf2, "binding");
        a(rf2);
        this.k = new ur3<>(this, rf2);
        ur3<rf2> ur3 = this.k;
        if (ur3 != null) {
            rf2 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        wf3 wf3 = this.l;
        if (wf3 != null) {
            wf3.g();
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        wf3 wf3 = this.l;
        if (wf3 != null) {
            Calendar calendar = this.j;
            wd4.a((Object) calendar, "mCalendar");
            Date time = calendar.getTime();
            wd4.a((Object) time, "mCalendar.time");
            wf3.a(time);
        }
        wf3 wf32 = this.l;
        if (wf32 != null) {
            wf32.f();
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wd4.b(bundle, "outState");
        wf3 wf3 = this.l;
        if (wf3 != null) {
            wf3.a(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public void p(List<eg3.b> list) {
        wd4.b(list, "listOfSleepSessionUIData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailFragment", "showDayDetailChart - baseModel=" + list);
        if (list.size() > 1) {
            ur3<rf2> ur3 = this.k;
            if (ur3 != null) {
                rf2 a2 = ur3.a();
                if (a2 != null) {
                    CustomPageIndicator customPageIndicator = a2.s;
                    if (customPageIndicator != null) {
                        customPageIndicator.setVisibility(0);
                    }
                }
            }
        } else {
            ur3<rf2> ur32 = this.k;
            if (ur32 != null) {
                rf2 a3 = ur32.a();
                if (a3 != null) {
                    CustomPageIndicator customPageIndicator2 = a3.s;
                    if (customPageIndicator2 != null) {
                        customPageIndicator2.setVisibility(8);
                    }
                }
            }
        }
        eg3 eg3 = this.m;
        if (eg3 != null) {
            eg3.a(list);
        }
    }

    @DexIgnore
    public final void a(rf2 rf2) {
        rf2.B.setOnClickListener(this);
        rf2.C.setOnClickListener(this);
        rf2.D.setOnClickListener(this);
        this.m = new eg3(new ArrayList());
        RecyclerViewPager recyclerViewPager = rf2.H;
        wd4.a((Object) recyclerViewPager, "rvSleeps");
        recyclerViewPager.setLayoutManager(new LinearLayoutManager(getContext(), 0, false));
        recyclerViewPager.setAdapter(this.m);
        recyclerViewPager.a((RecyclerView.q) new b(this, rf2));
        recyclerViewPager.i(this.o);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new CustomPageIndicator.a(1, 0, 2, (rd4) null));
        rf2.s.a((RecyclerView) recyclerViewPager, this.o, (List<CustomPageIndicator.a>) arrayList);
        this.n = new dg3(new ArrayList());
        RecyclerViewPager recyclerViewPager2 = rf2.G;
        wd4.a((Object) recyclerViewPager2, "rvHeartRateSleep");
        recyclerViewPager2.setLayoutManager(new LinearLayoutManager(getContext(), 0, false));
        recyclerViewPager2.setAdapter(this.n);
        recyclerViewPager2.i(this.o);
        recyclerViewPager2.d(true);
    }

    @DexIgnore
    public void a(wf3 wf3) {
        wd4.b(wf3, "presenter");
        this.l = wf3;
    }

    @DexIgnore
    public void a(Date date, boolean z, boolean z2, boolean z3) {
        wd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailFragment", "showDay - date=" + date + ", isCreateAt=" + z + ", isToday=" + z2 + ", isDateAfter=" + z3);
        Calendar calendar = this.j;
        wd4.a((Object) calendar, "mCalendar");
        calendar.setTime(date);
        int i = this.j.get(7);
        ur3<rf2> ur3 = this.k;
        if (ur3 != null) {
            rf2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.x;
                wd4.a((Object) flexibleTextView, "binding.ftvDayOfMonth");
                flexibleTextView.setText(String.valueOf(this.j.get(5)));
                if (z) {
                    ImageView imageView = a2.C;
                    wd4.a((Object) imageView, "binding.ivBackDate");
                    imageView.setVisibility(4);
                } else {
                    ImageView imageView2 = a2.C;
                    wd4.a((Object) imageView2, "binding.ivBackDate");
                    imageView2.setVisibility(0);
                }
                if (z2 || z3) {
                    ImageView imageView3 = a2.D;
                    wd4.a((Object) imageView3, "binding.ivNextDate");
                    imageView3.setVisibility(8);
                    if (z2) {
                        FlexibleTextView flexibleTextView2 = a2.y;
                        wd4.a((Object) flexibleTextView2, "binding.ftvDayOfWeek");
                        flexibleTextView2.setText(tm2.a(getContext(), (int) R.string.DashboardDiana_Main_Steps7days_CTA__Today));
                        return;
                    }
                    FlexibleTextView flexibleTextView3 = a2.y;
                    wd4.a((Object) flexibleTextView3, "binding.ftvDayOfWeek");
                    flexibleTextView3.setText(ml2.b.b(i));
                    return;
                }
                ImageView imageView4 = a2.D;
                wd4.a((Object) imageView4, "binding.ivNextDate");
                imageView4.setVisibility(0);
                FlexibleTextView flexibleTextView4 = a2.y;
                wd4.a((Object) flexibleTextView4, "binding.ftvDayOfWeek");
                flexibleTextView4.setText(ml2.b.b(i));
            }
        }
    }

    @DexIgnore
    public void a(MFSleepDay mFSleepDay) {
        int i;
        int i2;
        int i3;
        MFSleepDay mFSleepDay2 = mFSleepDay;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailFragment", "showDayDetail - sleepDay=" + mFSleepDay2);
        ur3<rf2> ur3 = this.k;
        if (ur3 != null) {
            rf2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "binding");
                View d = a2.d();
                wd4.a((Object) d, "binding.root");
                Context context = d.getContext();
                if (mFSleepDay2 != null) {
                    i = mFSleepDay.getSleepMinutes();
                    i2 = mFSleepDay.getGoalMinutes();
                } else {
                    i2 = 0;
                    i = 0;
                }
                if (i > 0) {
                    int i4 = i / 60;
                    int i5 = i - (i4 * 60);
                    FlexibleTextView flexibleTextView = a2.v;
                    wd4.a((Object) flexibleTextView, "binding.ftvDailyValue");
                    flexibleTextView.setText(String.valueOf(i4));
                    FlexibleTextView flexibleTextView2 = a2.t;
                    wd4.a((Object) flexibleTextView2, "binding.ftvDailyUnit");
                    String a3 = tm2.a(context, (int) R.string.General_Time_Abbreviations_Hours__Hrs);
                    wd4.a((Object) a3, "LanguageHelper.getString\u2026Abbreviations_Hours__Hrs)");
                    if (a3 != null) {
                        String lowerCase = a3.toLowerCase();
                        wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                        flexibleTextView2.setText(lowerCase);
                        if (i5 > 0) {
                            FlexibleTextView flexibleTextView3 = a2.w;
                            wd4.a((Object) flexibleTextView3, "binding.ftvDailyValue2");
                            be4 be4 = be4.a;
                            Locale locale = Locale.US;
                            wd4.a((Object) locale, "Locale.US");
                            Object[] objArr = {Integer.valueOf(i5)};
                            String format = String.format(locale, "%02d", Arrays.copyOf(objArr, objArr.length));
                            wd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
                            flexibleTextView3.setText(format);
                            FlexibleTextView flexibleTextView4 = a2.u;
                            wd4.a((Object) flexibleTextView4, "binding.ftvDailyUnit2");
                            String a4 = tm2.a(context, (int) R.string.General_Time_Abbreviations_Minutes__Mins);
                            wd4.a((Object) a4, "LanguageHelper.getString\u2026reviations_Minutes__Mins)");
                            if (a4 != null) {
                                String lowerCase2 = a4.toLowerCase();
                                wd4.a((Object) lowerCase2, "(this as java.lang.String).toLowerCase()");
                                flexibleTextView4.setText(lowerCase2);
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                            }
                        } else {
                            FlexibleTextView flexibleTextView5 = a2.w;
                            wd4.a((Object) flexibleTextView5, "binding.ftvDailyValue2");
                            flexibleTextView5.setText("");
                            FlexibleTextView flexibleTextView6 = a2.u;
                            wd4.a((Object) flexibleTextView6, "binding.ftvDailyUnit2");
                            flexibleTextView6.setText("");
                        }
                        RecyclerViewPager recyclerViewPager = a2.H;
                        wd4.a((Object) recyclerViewPager, "binding.rvSleeps");
                        recyclerViewPager.setVisibility(0);
                        a2.A.setTextColor(k6.a(context, (int) R.color.primaryColor));
                        a2.z.setTextColor(k6.a(context, (int) R.color.primaryColor));
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    FlexibleTextView flexibleTextView7 = a2.v;
                    wd4.a((Object) flexibleTextView7, "binding.ftvDailyValue");
                    flexibleTextView7.setText("");
                    FlexibleTextView flexibleTextView8 = a2.t;
                    wd4.a((Object) flexibleTextView8, "binding.ftvDailyUnit");
                    String a5 = tm2.a(context, (int) R.string.DashboardDiana_Steps_DetailPageNoRecord_Text__NoRecord);
                    wd4.a((Object) a5, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                    if (a5 != null) {
                        String upperCase = a5.toUpperCase();
                        wd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                        flexibleTextView8.setText(upperCase);
                        FlexibleTextView flexibleTextView9 = a2.w;
                        wd4.a((Object) flexibleTextView9, "binding.ftvDailyValue2");
                        flexibleTextView9.setText("");
                        FlexibleTextView flexibleTextView10 = a2.u;
                        wd4.a((Object) flexibleTextView10, "binding.ftvDailyUnit2");
                        flexibleTextView10.setText("");
                        RecyclerViewPager recyclerViewPager2 = a2.H;
                        wd4.a((Object) recyclerViewPager2, "binding.rvSleeps");
                        recyclerViewPager2.setVisibility(8);
                        CustomPageIndicator customPageIndicator = a2.s;
                        wd4.a((Object) customPageIndicator, "binding.cpiSleep");
                        customPageIndicator.setVisibility(8);
                        a2.A.setTextColor(k6.a(context, (int) R.color.disabledCalendarDay));
                        a2.z.setTextColor(k6.a(context, (int) R.color.disabledCalendarDay));
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                }
                int i6 = i2 > 0 ? (i * 100) / i2 : -1;
                if (i6 >= 100) {
                    ImageView imageView = a2.D;
                    wd4.a((Object) imageView, "binding.ivNextDate");
                    imageView.setSelected(true);
                    ImageView imageView2 = a2.C;
                    wd4.a((Object) imageView2, "binding.ivBackDate");
                    imageView2.setSelected(true);
                    ConstraintLayout constraintLayout = a2.r;
                    wd4.a((Object) constraintLayout, "binding.clOverviewDay");
                    constraintLayout.setSelected(true);
                    FlexibleTextView flexibleTextView11 = a2.y;
                    wd4.a((Object) flexibleTextView11, "binding.ftvDayOfWeek");
                    flexibleTextView11.setSelected(true);
                    FlexibleTextView flexibleTextView12 = a2.x;
                    wd4.a((Object) flexibleTextView12, "binding.ftvDayOfMonth");
                    flexibleTextView12.setSelected(true);
                    View view = a2.E;
                    wd4.a((Object) view, "binding.line");
                    view.setSelected(true);
                    FlexibleTextView flexibleTextView13 = a2.v;
                    wd4.a((Object) flexibleTextView13, "binding.ftvDailyValue");
                    flexibleTextView13.setSelected(true);
                    FlexibleTextView flexibleTextView14 = a2.t;
                    wd4.a((Object) flexibleTextView14, "binding.ftvDailyUnit");
                    flexibleTextView14.setSelected(true);
                    FlexibleTextView flexibleTextView15 = a2.w;
                    wd4.a((Object) flexibleTextView15, "binding.ftvDailyValue2");
                    flexibleTextView15.setSelected(true);
                    FlexibleTextView flexibleTextView16 = a2.u;
                    wd4.a((Object) flexibleTextView16, "binding.ftvDailyUnit2");
                    flexibleTextView16.setSelected(true);
                    i3 = 0;
                } else {
                    ImageView imageView3 = a2.D;
                    wd4.a((Object) imageView3, "binding.ivNextDate");
                    i3 = 0;
                    imageView3.setSelected(false);
                    ImageView imageView4 = a2.C;
                    wd4.a((Object) imageView4, "binding.ivBackDate");
                    imageView4.setSelected(false);
                    ConstraintLayout constraintLayout2 = a2.r;
                    wd4.a((Object) constraintLayout2, "binding.clOverviewDay");
                    constraintLayout2.setSelected(false);
                    FlexibleTextView flexibleTextView17 = a2.y;
                    wd4.a((Object) flexibleTextView17, "binding.ftvDayOfWeek");
                    flexibleTextView17.setSelected(false);
                    FlexibleTextView flexibleTextView18 = a2.x;
                    wd4.a((Object) flexibleTextView18, "binding.ftvDayOfMonth");
                    flexibleTextView18.setSelected(false);
                    View view2 = a2.E;
                    wd4.a((Object) view2, "binding.line");
                    view2.setSelected(false);
                    FlexibleTextView flexibleTextView19 = a2.v;
                    wd4.a((Object) flexibleTextView19, "binding.ftvDailyValue");
                    flexibleTextView19.setSelected(false);
                    FlexibleTextView flexibleTextView20 = a2.t;
                    wd4.a((Object) flexibleTextView20, "binding.ftvDailyUnit");
                    flexibleTextView20.setSelected(false);
                    FlexibleTextView flexibleTextView21 = a2.w;
                    wd4.a((Object) flexibleTextView21, "binding.ftvDailyValue2");
                    flexibleTextView21.setSelected(false);
                    FlexibleTextView flexibleTextView22 = a2.u;
                    wd4.a((Object) flexibleTextView22, "binding.ftvDailyUnit2");
                    flexibleTextView22.setSelected(false);
                }
                if (i6 == -1) {
                    ProgressBar progressBar = a2.F;
                    wd4.a((Object) progressBar, "binding.pbGoal");
                    progressBar.setProgress(i3);
                    FlexibleTextView flexibleTextView23 = a2.A;
                    wd4.a((Object) flexibleTextView23, "binding.ftvProgressValue");
                    flexibleTextView23.setText(tm2.a(context, (int) R.string.character_dash_double));
                } else {
                    ProgressBar progressBar2 = a2.F;
                    wd4.a((Object) progressBar2, "binding.pbGoal");
                    progressBar2.setProgress(i6);
                    FlexibleTextView flexibleTextView24 = a2.A;
                    wd4.a((Object) flexibleTextView24, "binding.ftvProgressValue");
                    flexibleTextView24.setText(i6 + "%");
                }
                if (i2 < 60) {
                    FlexibleTextView flexibleTextView25 = a2.z;
                    wd4.a((Object) flexibleTextView25, "binding.ftvGoalValue");
                    be4 be42 = be4.a;
                    String a6 = tm2.a(context, (int) R.string.DashboardDiana_ActiveMinutes_DetailPage_Title__OfNumberMins);
                    wd4.a((Object) a6, "LanguageHelper.getString\u2026Page_Title__OfNumberMins)");
                    Object[] objArr2 = {Integer.valueOf(i2)};
                    String format2 = String.format(a6, Arrays.copyOf(objArr2, objArr2.length));
                    wd4.a((Object) format2, "java.lang.String.format(format, *args)");
                    flexibleTextView25.setText(format2);
                    return;
                }
                FlexibleTextView flexibleTextView26 = a2.z;
                wd4.a((Object) flexibleTextView26, "binding.ftvGoalValue");
                be4 be43 = be4.a;
                String a7 = tm2.a(context, (int) R.string.DashboardHybrid_Sleep_DetailPageNoRecord_Title__OfNumberHrs);
                wd4.a((Object) a7, "LanguageHelper.getString\u2026ecord_Title__OfNumberHrs)");
                Object[] objArr3 = {jl2.a(((float) i2) / 60.0f, 1)};
                String format3 = String.format(a7, Arrays.copyOf(objArr3, objArr3.length));
                wd4.a((Object) format3, "java.lang.String.format(format, *args)");
                flexibleTextView26.setText(format3);
            }
        }
    }
}
