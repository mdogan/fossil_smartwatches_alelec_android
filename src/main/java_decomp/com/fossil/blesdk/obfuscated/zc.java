package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.media.browse.MediaBrowser;
import android.os.Parcel;
import android.service.media.MediaBrowserService;
import com.fossil.blesdk.obfuscated.yc;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zc {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends yc.b {
        @DexIgnore
        public a(Context context, b bVar) {
            super(context, bVar);
        }

        @DexIgnore
        public void onLoadItem(String str, MediaBrowserService.Result<MediaBrowser.MediaItem> result) {
            ((b) this.e).a(str, new yc.c(result));
        }
    }

    @DexIgnore
    public interface b extends yc.d {
        @DexIgnore
        void a(String str, yc.c<Parcel> cVar);
    }

    @DexIgnore
    public static Object a(Context context, b bVar) {
        return new a(context, bVar);
    }
}
