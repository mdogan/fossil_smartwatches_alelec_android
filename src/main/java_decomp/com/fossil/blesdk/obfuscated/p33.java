package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p33 implements Factory<SearchRingPhonePresenter> {
    @DexIgnore
    public static SearchRingPhonePresenter a(l33 l33) {
        return new SearchRingPhonePresenter(l33);
    }
}
