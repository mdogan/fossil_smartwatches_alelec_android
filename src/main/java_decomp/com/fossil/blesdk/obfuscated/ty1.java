package com.fossil.blesdk.obfuscated;

import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class ty1 implements xx1 {
    @DexIgnore
    public /* final */ FirebaseInstanceId a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public ty1(FirebaseInstanceId firebaseInstanceId, String str, String str2, String str3, String str4) {
        this.a = firebaseInstanceId;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
    }

    @DexIgnore
    public final xn1 a() {
        return this.a.a(this.b, this.c, this.d, this.e);
    }
}
