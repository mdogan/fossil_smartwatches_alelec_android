package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sp0 implements Parcelable.Creator<DataType> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        ArrayList<cp0> arrayList = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                str = SafeParcelReader.f(parcel, a);
            } else if (a2 == 2) {
                arrayList = SafeParcelReader.c(parcel, a, cp0.CREATOR);
            } else if (a2 == 3) {
                str2 = SafeParcelReader.f(parcel, a);
            } else if (a2 != 4) {
                SafeParcelReader.v(parcel, a);
            } else {
                str3 = SafeParcelReader.f(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new DataType(str, (List<cp0>) arrayList, str2, str3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new DataType[i];
    }
}
