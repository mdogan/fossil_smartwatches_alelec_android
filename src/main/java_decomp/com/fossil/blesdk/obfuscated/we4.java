package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface we4<R> extends se4<R>, va4<R> {
    @DexIgnore
    boolean isExternal();

    @DexIgnore
    boolean isInfix();

    @DexIgnore
    boolean isInline();

    @DexIgnore
    boolean isOperator();

    @DexIgnore
    boolean isSuspend();
}
