package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.setting.SharedPreferenceFileName;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ea0 extends ba0 {
    @DexIgnore
    public static long k; // = 60000;
    @DexIgnore
    public static /* final */ ea0 l; // = new ea0();

    @DexIgnore
    public ea0() {
        super("sdk_log", 204800, "sdk_log", "sdklog", new oa0("", "", ""), 1800, new ca0(), SharedPreferenceFileName.SDK_LOG_PREFERENCE, true);
    }

    @DexIgnore
    public final void a(Exception exc) {
        wd4.b(exc, "e");
        JSONObject jSONObject = new JSONObject();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        exc.printStackTrace(new PrintWriter(byteArrayOutputStream, true));
        JSONKey jSONKey = JSONKey.STACK_TRACE;
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        wd4.a((Object) byteArray, "byteArrayOutputStream.toByteArray()");
        xa0.a(jSONObject, jSONKey, new String(byteArray, va0.y.f()));
        String localizedMessage = exc.getLocalizedMessage();
        wd4.a((Object) localizedMessage, "e.localizedMessage");
        EventType eventType = EventType.EXCEPTION;
        String canonicalName = exc.getClass().getCanonicalName();
        if (canonicalName == null) {
            canonicalName = "";
        }
        String uuid = UUID.randomUUID().toString();
        wd4.a((Object) uuid, "UUID.randomUUID().toString()");
        b(new SdkLogEntry(localizedMessage, eventType, "", canonicalName, uuid, false, (String) null, (DeviceInformation) null, (fa0) null, jSONObject, 448, (rd4) null));
    }

    @DexIgnore
    public long b() {
        return k;
    }
}
