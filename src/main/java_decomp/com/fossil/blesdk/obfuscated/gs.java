package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.tr;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gs implements tr<Uri, InputStream> {
    @DexIgnore
    public /* final */ Context a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ur<Uri, InputStream> {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public a(Context context) {
            this.a = context;
        }

        @DexIgnore
        public tr<Uri, InputStream> a(xr xrVar) {
            return new gs(this.a);
        }
    }

    @DexIgnore
    public gs(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public tr.a<InputStream> a(Uri uri, int i, int i2, mo moVar) {
        if (fp.a(i, i2)) {
            return new tr.a<>(new kw(uri), gp.a(this.a, uri));
        }
        return null;
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return fp.a(uri);
    }
}
