package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class he2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ ImageView u;
    @DexIgnore
    public /* final */ ImageView v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ ImageView x;

    @DexIgnore
    public he2(Object obj, View view, int i, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = flexibleTextView2;
        this.s = flexibleTextView3;
        this.t = flexibleTextView4;
        this.u = imageView;
        this.v = imageView2;
        this.w = imageView3;
        this.x = imageView4;
    }
}
