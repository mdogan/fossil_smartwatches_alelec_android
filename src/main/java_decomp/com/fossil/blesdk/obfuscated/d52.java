package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d52 implements Factory<bk3> {
    @DexIgnore
    public /* final */ o42 a;
    @DexIgnore
    public /* final */ Provider<InAppNotificationRepository> b;

    @DexIgnore
    public d52(o42 o42, Provider<InAppNotificationRepository> provider) {
        this.a = o42;
        this.b = provider;
    }

    @DexIgnore
    public static d52 a(o42 o42, Provider<InAppNotificationRepository> provider) {
        return new d52(o42, provider);
    }

    @DexIgnore
    public static bk3 b(o42 o42, Provider<InAppNotificationRepository> provider) {
        return a(o42, provider.get());
    }

    @DexIgnore
    public static bk3 a(o42 o42, InAppNotificationRepository inAppNotificationRepository) {
        bk3 a2 = o42.a(inAppNotificationRepository);
        o44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public bk3 get() {
        return b(this.a, this.b);
    }
}
