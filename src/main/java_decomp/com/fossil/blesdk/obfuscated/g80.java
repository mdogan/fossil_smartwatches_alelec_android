package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.FileControlOperationCode;
import com.fossil.blesdk.device.logic.request.file.FileDataRequest;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g80 extends FileDataRequest {
    @DexIgnore
    public byte[] X;
    @DexIgnore
    public long Y;
    @DexIgnore
    public long Z;
    @DexIgnore
    public /* final */ long a0;
    @DexIgnore
    public /* final */ long b0;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ g80(short s, long j, long j2, Peripheral peripheral, int i, int i2, rd4 rd4) {
        this(s, j, j2, peripheral, (i2 & 16) != 0 ? 3 : i);
    }

    @DexIgnore
    public byte[] C() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.a0).putInt((int) this.b0).array();
        wd4.a((Object) array, "ByteBuffer.allocate(8).o\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public long J() {
        return this.Y;
    }

    @DexIgnore
    public long K() {
        return this.Z;
    }

    @DexIgnore
    public byte[] L() {
        return this.X;
    }

    @DexIgnore
    public void b(long j) {
        this.Y = j;
    }

    @DexIgnore
    public void c(long j) {
        this.Z = j;
    }

    @DexIgnore
    public void f(byte[] bArr) {
        wd4.b(bArr, "<set-?>");
        this.X = bArr;
    }

    @DexIgnore
    public JSONObject t() {
        return xa0.a(xa0.a(super.t(), JSONKey.OFFSET, Long.valueOf(this.a0)), JSONKey.LENGTH, Long.valueOf(this.b0));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g80(short s, long j, long j2, Peripheral peripheral, int i) {
        super(FileControlOperationCode.GET_FILE, s, RequestId.GET_FILE, peripheral, i);
        wd4.b(peripheral, "peripheral");
        this.a0 = j;
        this.b0 = j2;
        this.X = new byte[0];
    }
}
