package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.os.RemoteException;
import com.google.android.gms.maps.model.RuntimeRemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jf1 {
    @DexIgnore
    public static e51 a;

    @DexIgnore
    public static e51 a() {
        e51 e51 = a;
        ck0.a(e51, (Object) "IBitmapDescriptorFactory is not initialized");
        return e51;
    }

    @DexIgnore
    public static void a(e51 e51) {
        if (a == null) {
            ck0.a(e51);
            a = e51;
        }
    }

    @DexIgnore
    public static if1 a(Bitmap bitmap) {
        try {
            return new if1(a().zza(bitmap));
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
