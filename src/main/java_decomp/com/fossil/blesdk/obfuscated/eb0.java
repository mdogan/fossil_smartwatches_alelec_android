package com.fossil.blesdk.obfuscated;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eb0 {
    @DexIgnore
    public static /* final */ a a; // = new a();
    @DexIgnore
    public static /* final */ LinkedBlockingQueue<Runnable> b; // = new LinkedBlockingQueue<>(128);
    @DexIgnore
    public static /* final */ ThreadPoolExecutor c; // = new ThreadPoolExecutor(1, 1, 1, TimeUnit.SECONDS, b, a);
    @DexIgnore
    public static /* final */ eb0 d; // = new eb0();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ThreadFactory {
        @DexIgnore
        public /* final */ AtomicInteger a; // = new AtomicInteger(1);

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            wd4.b(runnable, "r");
            return new Thread(runnable, eb0.class.getSimpleName() + " #" + this.a.getAndIncrement());
        }
    }

    @DexIgnore
    public final boolean a(id4<cb4> id4) {
        wd4.b(id4, "action");
        c.execute(new db0(id4));
        return true;
    }
}
