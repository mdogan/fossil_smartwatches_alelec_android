package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.we0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wh0 extends uh0<Void> {
    @DexIgnore
    public /* final */ cf0<ee0.b, ?> b;
    @DexIgnore
    public /* final */ if0<ee0.b, ?> c;

    @DexIgnore
    public wh0(fh0 fh0, yn1<Void> yn1) {
        super(3, yn1);
        this.b = fh0.a;
        this.c = fh0.b;
    }

    @DexIgnore
    public final /* bridge */ /* synthetic */ void a(kf0 kf0, boolean z) {
    }

    @DexIgnore
    public final xd0[] b(we0.a<?> aVar) {
        return this.b.c();
    }

    @DexIgnore
    public final boolean c(we0.a<?> aVar) {
        return this.b.d();
    }

    @DexIgnore
    public final void d(we0.a<?> aVar) throws RemoteException {
        this.b.a(aVar.f(), this.a);
        if (this.b.b() != null) {
            aVar.l().put(this.b.b(), new fh0(this.b, this.c));
        }
    }
}
