package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p61 implements u61 {
    @DexIgnore
    public static /* final */ Map<Uri, p61> f; // = new g4();
    @DexIgnore
    public static /* final */ String[] g; // = {"key", "value"};
    @DexIgnore
    public /* final */ ContentResolver a;
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Map<String, String> d;
    @DexIgnore
    public /* final */ List<t61> e; // = new ArrayList();

    @DexIgnore
    public p61(ContentResolver contentResolver, Uri uri) {
        this.a = contentResolver;
        this.b = uri;
        this.a.registerContentObserver(uri, false, new r61(this, (Handler) null));
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(5:2|3|(5:5|6|7|8|9)|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0018 */
    public static p61 a(ContentResolver contentResolver, Uri uri) {
        p61 p61;
        synchronized (p61.class) {
            p61 = f.get(uri);
            if (p61 == null) {
                p61 p612 = new p61(contentResolver, uri);
                try {
                    f.put(uri, p612);
                } catch (SecurityException unused) {
                }
                p61 = p612;
            }
        }
        return p61;
    }

    @DexIgnore
    public final void b() {
        synchronized (this.c) {
            this.d = null;
            b71.f();
        }
        synchronized (this) {
            for (t61 a2 : this.e) {
                a2.a();
            }
        }
    }

    @DexIgnore
    public final Map<String, String> c() {
        try {
            return (Map) v61.a(new q61(this));
        } catch (SQLiteException | SecurityException unused) {
            Log.e("ConfigurationContentLoader", "PhenotypeFlag unable to load ContentProvider, using default values");
            return null;
        }
    }

    @DexIgnore
    public final /* synthetic */ Map d() {
        Map map;
        Cursor query = this.a.query(this.b, g, (String) null, (String[]) null, (String) null);
        if (query == null) {
            return Collections.emptyMap();
        }
        try {
            int count = query.getCount();
            if (count == 0) {
                return Collections.emptyMap();
            }
            if (count <= 256) {
                map = new g4(count);
            } else {
                map = new HashMap(count, 1.0f);
            }
            while (query.moveToNext()) {
                map.put(query.getString(0), query.getString(1));
            }
            query.close();
            return map;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    public final Map<String, String> a() {
        Map<String, String> map = this.d;
        if (map == null) {
            synchronized (this.c) {
                map = this.d;
                if (map == null) {
                    map = c();
                    this.d = map;
                }
            }
        }
        if (map != null) {
            return map;
        }
        return Collections.emptyMap();
    }

    @DexIgnore
    public final /* synthetic */ Object a(String str) {
        return a().get(str);
    }
}
