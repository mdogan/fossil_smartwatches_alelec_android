package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ht1 {
    @DexIgnore
    public static /* final */ dt1 i; // = new dt1();
    @DexIgnore
    public static /* final */ et1 j; // = new et1();
    @DexIgnore
    public dt1 a;
    @DexIgnore
    public dt1 b;
    @DexIgnore
    public dt1 c;
    @DexIgnore
    public dt1 d;
    @DexIgnore
    public et1 e;
    @DexIgnore
    public et1 f;
    @DexIgnore
    public et1 g;
    @DexIgnore
    public et1 h;

    @DexIgnore
    public ht1() {
        dt1 dt1 = i;
        this.a = dt1;
        this.b = dt1;
        this.c = dt1;
        this.d = dt1;
        et1 et1 = j;
        this.e = et1;
        this.f = et1;
        this.g = et1;
        this.h = et1;
    }

    @DexIgnore
    public void a(et1 et1) {
        this.e = et1;
    }

    @DexIgnore
    public dt1 b() {
        return this.d;
    }

    @DexIgnore
    public dt1 c() {
        return this.c;
    }

    @DexIgnore
    public et1 d() {
        return this.h;
    }

    @DexIgnore
    public et1 e() {
        return this.f;
    }

    @DexIgnore
    public et1 f() {
        return this.e;
    }

    @DexIgnore
    public dt1 g() {
        return this.a;
    }

    @DexIgnore
    public dt1 h() {
        return this.b;
    }

    @DexIgnore
    public et1 a() {
        return this.g;
    }
}
