package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.network.HttpMethod;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h84 extends j74 {
    @DexIgnore
    public h84(w44 w44, String str, String str2, a74 a74) {
        super(w44, str, str2, a74, HttpMethod.PUT);
    }
}
