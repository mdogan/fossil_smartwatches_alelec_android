package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zzuv;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c81 implements na1 {
    @DexIgnore
    public /* final */ z71 a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d; // = 0;

    @DexIgnore
    public c81(z71 z71) {
        w81.a(z71, "input");
        this.a = z71;
        this.a.c = this;
    }

    @DexIgnore
    public static c81 a(z71 z71) {
        c81 c81 = z71.c;
        if (c81 != null) {
            return c81;
        }
        return new c81(z71);
    }

    @DexIgnore
    public final <T> T b(oa1<T> oa1, j81 j81) throws IOException {
        a(2);
        return c(oa1, j81);
    }

    @DexIgnore
    public final long c() throws IOException {
        a(0);
        return this.a.e();
    }

    @DexIgnore
    public final <T> T d(oa1<T> oa1, j81 j81) throws IOException {
        int i = this.c;
        this.c = ((this.b >>> 3) << 3) | 4;
        try {
            T a2 = oa1.a();
            oa1.a(a2, this, j81);
            oa1.d(a2);
            if (this.b == this.c) {
                return a2;
            }
            throw zzuv.zzww();
        } finally {
            this.c = i;
        }
    }

    @DexIgnore
    public final String e() throws IOException {
        a(2);
        return this.a.k();
    }

    @DexIgnore
    public final boolean f() throws IOException {
        a(0);
        return this.a.j();
    }

    @DexIgnore
    public final int g() throws IOException {
        a(0);
        return this.a.q();
    }

    @DexIgnore
    public final int h() throws IOException {
        a(5);
        return this.a.i();
    }

    @DexIgnore
    public final long i() throws IOException {
        a(0);
        return this.a.r();
    }

    @DexIgnore
    public final int j() throws IOException {
        int i = this.d;
        if (i != 0) {
            this.b = i;
            this.d = 0;
        } else {
            this.b = this.a.d();
        }
        int i2 = this.b;
        if (i2 == 0 || i2 == this.c) {
            return Integer.MAX_VALUE;
        }
        return i2 >>> 3;
    }

    @DexIgnore
    public final long k() throws IOException {
        a(1);
        return this.a.p();
    }

    @DexIgnore
    public final String l() throws IOException {
        a(2);
        return this.a.c();
    }

    @DexIgnore
    public final long m() throws IOException {
        a(1);
        return this.a.h();
    }

    @DexIgnore
    public final boolean n() throws IOException {
        if (this.a.s()) {
            return false;
        }
        int i = this.b;
        if (i == this.c) {
            return false;
        }
        return this.a.b(i);
    }

    @DexIgnore
    public final int o() throws IOException {
        a(5);
        return this.a.o();
    }

    @DexIgnore
    public final int p() throws IOException {
        a(0);
        return this.a.g();
    }

    @DexIgnore
    public final int q() throws IOException {
        a(0);
        return this.a.n();
    }

    @DexIgnore
    public final long r() throws IOException {
        a(0);
        return this.a.f();
    }

    @DexIgnore
    public final double readDouble() throws IOException {
        a(1);
        return this.a.a();
    }

    @DexIgnore
    public final float readFloat() throws IOException {
        a(5);
        return this.a.b();
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final int b() throws IOException {
        a(0);
        return this.a.m();
    }

    @DexIgnore
    public final <T> T c(oa1<T> oa1, j81 j81) throws IOException {
        int m = this.a.m();
        z71 z71 = this.a;
        if (z71.a < z71.b) {
            int d2 = z71.d(m);
            T a2 = oa1.a();
            this.a.a++;
            oa1.a(a2, this, j81);
            oa1.d(a2);
            this.a.a(0);
            z71 z712 = this.a;
            z712.a--;
            z712.e(d2);
            return a2;
        }
        throw zzuv.zzwv();
    }

    @DexIgnore
    public final void e(List<Float> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof r81) {
            r81 r81 = (r81) list;
            int i = this.b & 7;
            if (i == 2) {
                int m = this.a.m();
                d(m);
                int t = this.a.t() + m;
                do {
                    r81.a(this.a.b());
                } while (this.a.t() < t);
            } else if (i == 5) {
                do {
                    r81.a(this.a.b());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 2) {
                int m2 = this.a.m();
                d(m2);
                int t2 = this.a.t() + m2;
                do {
                    list.add(Float.valueOf(this.a.b()));
                } while (this.a.t() < t2);
            } else if (i2 == 5) {
                do {
                    list.add(Float.valueOf(this.a.b()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final void f(List<zzte> list) throws IOException {
        int d2;
        if ((this.b & 7) == 2) {
            do {
                list.add(d());
                if (!this.a.s()) {
                    d2 = this.a.d();
                } else {
                    return;
                }
            } while (d2 == this.b);
            this.d = d2;
            return;
        }
        throw zzuv.zzwu();
    }

    @DexIgnore
    public final void g(List<Double> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof f81) {
            f81 f81 = (f81) list;
            int i = this.b & 7;
            if (i == 1) {
                do {
                    f81.a(this.a.a());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else if (i == 2) {
                int m = this.a.m();
                c(m);
                int t = this.a.t() + m;
                do {
                    f81.a(this.a.a());
                } while (this.a.t() < t);
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 1) {
                do {
                    list.add(Double.valueOf(this.a.a()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else if (i2 == 2) {
                int m2 = this.a.m();
                c(m2);
                int t2 = this.a.t() + m2;
                do {
                    list.add(Double.valueOf(this.a.a()));
                } while (this.a.t() < t2);
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final void h(List<String> list) throws IOException {
        a(list, true);
    }

    @DexIgnore
    public final void i(List<Long> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof n91) {
            n91 n91 = (n91) list;
            int i = this.b & 7;
            if (i == 0) {
                do {
                    n91.a(this.a.f());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else if (i == 2) {
                int t = this.a.t() + this.a.m();
                do {
                    n91.a(this.a.f());
                } while (this.a.t() < t);
                b(t);
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.a.f()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else if (i2 == 2) {
                int t2 = this.a.t() + this.a.m();
                do {
                    list.add(Long.valueOf(this.a.f()));
                } while (this.a.t() < t2);
                b(t2);
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final void k(List<Long> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof n91) {
            n91 n91 = (n91) list;
            int i = this.b & 7;
            if (i == 0) {
                do {
                    n91.a(this.a.e());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else if (i == 2) {
                int t = this.a.t() + this.a.m();
                do {
                    n91.a(this.a.e());
                } while (this.a.t() < t);
                b(t);
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.a.e()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else if (i2 == 2) {
                int t2 = this.a.t() + this.a.m();
                do {
                    list.add(Long.valueOf(this.a.e()));
                } while (this.a.t() < t2);
                b(t2);
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final void l(List<Integer> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof v81) {
            v81 v81 = (v81) list;
            int i = this.b & 7;
            if (i == 0) {
                do {
                    v81.h(this.a.m());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else if (i == 2) {
                int t = this.a.t() + this.a.m();
                do {
                    v81.h(this.a.m());
                } while (this.a.t() < t);
                b(t);
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.a.m()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else if (i2 == 2) {
                int t2 = this.a.t() + this.a.m();
                do {
                    list.add(Integer.valueOf(this.a.m()));
                } while (this.a.t() < t2);
                b(t2);
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final void m(List<String> list) throws IOException {
        a(list, false);
    }

    @DexIgnore
    public final void n(List<Long> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof n91) {
            n91 n91 = (n91) list;
            int i = this.b & 7;
            if (i == 1) {
                do {
                    n91.a(this.a.h());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else if (i == 2) {
                int m = this.a.m();
                c(m);
                int t = this.a.t() + m;
                do {
                    n91.a(this.a.h());
                } while (this.a.t() < t);
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 1) {
                do {
                    list.add(Long.valueOf(this.a.h()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else if (i2 == 2) {
                int m2 = this.a.m();
                c(m2);
                int t2 = this.a.t() + m2;
                do {
                    list.add(Long.valueOf(this.a.h()));
                } while (this.a.t() < t2);
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final void o(List<Long> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof n91) {
            n91 n91 = (n91) list;
            int i = this.b & 7;
            if (i == 1) {
                do {
                    n91.a(this.a.p());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else if (i == 2) {
                int m = this.a.m();
                c(m);
                int t = this.a.t() + m;
                do {
                    n91.a(this.a.p());
                } while (this.a.t() < t);
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 1) {
                do {
                    list.add(Long.valueOf(this.a.p()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else if (i2 == 2) {
                int m2 = this.a.m();
                c(m2);
                int t2 = this.a.t() + m2;
                do {
                    list.add(Long.valueOf(this.a.p()));
                } while (this.a.t() < t2);
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final void p(List<Integer> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof v81) {
            v81 v81 = (v81) list;
            int i = this.b & 7;
            if (i == 0) {
                do {
                    v81.h(this.a.g());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else if (i == 2) {
                int t = this.a.t() + this.a.m();
                do {
                    v81.h(this.a.g());
                } while (this.a.t() < t);
                b(t);
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.a.g()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else if (i2 == 2) {
                int t2 = this.a.t() + this.a.m();
                do {
                    list.add(Integer.valueOf(this.a.g()));
                } while (this.a.t() < t2);
                b(t2);
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final void q(List<Integer> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof v81) {
            v81 v81 = (v81) list;
            int i = this.b & 7;
            if (i == 2) {
                int m = this.a.m();
                d(m);
                int t = this.a.t() + m;
                do {
                    v81.h(this.a.o());
                } while (this.a.t() < t);
            } else if (i == 5) {
                do {
                    v81.h(this.a.o());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 2) {
                int m2 = this.a.m();
                d(m2);
                int t2 = this.a.t() + m2;
                do {
                    list.add(Integer.valueOf(this.a.o()));
                } while (this.a.t() < t2);
            } else if (i2 == 5) {
                do {
                    list.add(Integer.valueOf(this.a.o()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final void a(int i) throws IOException {
        if ((this.b & 7) != i) {
            throw zzuv.zzwu();
        }
    }

    @DexIgnore
    public final void b(List<Integer> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof v81) {
            v81 v81 = (v81) list;
            int i = this.b & 7;
            if (i == 2) {
                int m = this.a.m();
                d(m);
                int t = this.a.t() + m;
                do {
                    v81.h(this.a.i());
                } while (this.a.t() < t);
            } else if (i == 5) {
                do {
                    v81.h(this.a.i());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 2) {
                int m2 = this.a.m();
                d(m2);
                int t2 = this.a.t() + m2;
                do {
                    list.add(Integer.valueOf(this.a.i()));
                } while (this.a.t() < t2);
            } else if (i2 == 5) {
                do {
                    list.add(Integer.valueOf(this.a.i()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final <T> T a(oa1<T> oa1, j81 j81) throws IOException {
        a(3);
        return d(oa1, j81);
    }

    @DexIgnore
    public final void j(List<Integer> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof v81) {
            v81 v81 = (v81) list;
            int i = this.b & 7;
            if (i == 0) {
                do {
                    v81.h(this.a.n());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else if (i == 2) {
                int t = this.a.t() + this.a.m();
                do {
                    v81.h(this.a.n());
                } while (this.a.t() < t);
                b(t);
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.a.n()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else if (i2 == 2) {
                int t2 = this.a.t() + this.a.m();
                do {
                    list.add(Integer.valueOf(this.a.n()));
                } while (this.a.t() < t2);
                b(t2);
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final void a(List<Boolean> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof p71) {
            p71 p71 = (p71) list;
            int i = this.b & 7;
            if (i == 0) {
                do {
                    p71.a(this.a.j());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else if (i == 2) {
                int t = this.a.t() + this.a.m();
                do {
                    p71.a(this.a.j());
                } while (this.a.t() < t);
                b(t);
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 0) {
                do {
                    list.add(Boolean.valueOf(this.a.j()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else if (i2 == 2) {
                int t2 = this.a.t() + this.a.m();
                do {
                    list.add(Boolean.valueOf(this.a.j()));
                } while (this.a.t() < t2);
                b(t2);
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final zzte d() throws IOException {
        a(2);
        return this.a.l();
    }

    @DexIgnore
    public final void d(List<Integer> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof v81) {
            v81 v81 = (v81) list;
            int i = this.b & 7;
            if (i == 0) {
                do {
                    v81.h(this.a.q());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else if (i == 2) {
                int t = this.a.t() + this.a.m();
                do {
                    v81.h(this.a.q());
                } while (this.a.t() < t);
                b(t);
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.a.q()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else if (i2 == 2) {
                int t2 = this.a.t() + this.a.m();
                do {
                    list.add(Integer.valueOf(this.a.q()));
                } while (this.a.t() < t2);
                b(t2);
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final void c(List<Long> list) throws IOException {
        int d2;
        int d3;
        if (list instanceof n91) {
            n91 n91 = (n91) list;
            int i = this.b & 7;
            if (i == 0) {
                do {
                    n91.a(this.a.r());
                    if (!this.a.s()) {
                        d3 = this.a.d();
                    } else {
                        return;
                    }
                } while (d3 == this.b);
                this.d = d3;
            } else if (i == 2) {
                int t = this.a.t() + this.a.m();
                do {
                    n91.a(this.a.r());
                } while (this.a.t() < t);
                b(t);
            } else {
                throw zzuv.zzwu();
            }
        } else {
            int i2 = this.b & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.a.r()));
                    if (!this.a.s()) {
                        d2 = this.a.d();
                    } else {
                        return;
                    }
                } while (d2 == this.b);
                this.d = d2;
            } else if (i2 == 2) {
                int t2 = this.a.t() + this.a.m();
                do {
                    list.add(Long.valueOf(this.a.r()));
                } while (this.a.t() < t2);
                b(t2);
            } else {
                throw zzuv.zzwu();
            }
        }
    }

    @DexIgnore
    public final <T> void b(List<T> list, oa1<T> oa1, j81 j81) throws IOException {
        int d2;
        int i = this.b;
        if ((i & 7) == 2) {
            do {
                list.add(c(oa1, j81));
                if (!this.a.s() && this.d == 0) {
                    d2 = this.a.d();
                } else {
                    return;
                }
            } while (d2 == i);
            this.d = d2;
            return;
        }
        throw zzuv.zzwu();
    }

    @DexIgnore
    public final void a(List<String> list, boolean z) throws IOException {
        int d2;
        int d3;
        if ((this.b & 7) != 2) {
            throw zzuv.zzwu();
        } else if (!(list instanceof i91) || z) {
            do {
                list.add(z ? e() : l());
                if (!this.a.s()) {
                    d2 = this.a.d();
                } else {
                    return;
                }
            } while (d2 == this.b);
            this.d = d2;
        } else {
            i91 i91 = (i91) list;
            do {
                i91.a(d());
                if (!this.a.s()) {
                    d3 = this.a.d();
                } else {
                    return;
                }
            } while (d3 == this.b);
            this.d = d3;
        }
    }

    @DexIgnore
    public final void b(int i) throws IOException {
        if (this.a.t() != i) {
            throw zzuv.zzwq();
        }
    }

    @DexIgnore
    public static void d(int i) throws IOException {
        if ((i & 3) != 0) {
            throw zzuv.zzww();
        }
    }

    @DexIgnore
    public static void c(int i) throws IOException {
        if ((i & 7) != 0) {
            throw zzuv.zzww();
        }
    }

    @DexIgnore
    public final <T> void a(List<T> list, oa1<T> oa1, j81 j81) throws IOException {
        int d2;
        int i = this.b;
        if ((i & 7) == 3) {
            do {
                list.add(d(oa1, j81));
                if (!this.a.s() && this.d == 0) {
                    d2 = this.a.d();
                } else {
                    return;
                }
            } while (d2 == i);
            this.d = d2;
            return;
        }
        throw zzuv.zzwu();
    }
}
