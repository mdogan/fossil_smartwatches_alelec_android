package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import kotlin.Result;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x52 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.e<P, E> {
        @DexIgnore
        public /* final */ /* synthetic */ pg4 a;
        @DexIgnore
        public /* final */ /* synthetic */ CoroutineUseCase b;

        @DexIgnore
        public a(pg4 pg4, CoroutineUseCase coroutineUseCase, CoroutineUseCase.b bVar) {
            this.a = pg4;
            this.b = coroutineUseCase;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(P p) {
            wd4.b(p, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String c = this.b.c();
            local.d(c, "success continuation " + this.a.hashCode());
            if (this.a.isActive()) {
                pg4 pg4 = this.a;
                Result.a aVar = Result.Companion;
                pg4.resumeWith(Result.m3constructorimpl(p));
            }
        }

        @DexIgnore
        public void a(E e) {
            wd4.b(e, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String c = this.b.c();
            local.d(c, "fail continuation " + this.a.hashCode());
            if (this.a.isActive()) {
                pg4 pg4 = this.a;
                Result.a aVar = Result.Companion;
                pg4.resumeWith(Result.m3constructorimpl(e));
            }
        }
    }

    @DexIgnore
    public static final <R extends CoroutineUseCase.b, P extends CoroutineUseCase.d, E extends CoroutineUseCase.a> Object a(CoroutineUseCase<? super R, P, E> coroutineUseCase, R r, kc4<? super CoroutineUseCase.c> kc4) {
        qg4 qg4 = new qg4(IntrinsicsKt__IntrinsicsJvmKt.a(kc4), 1);
        coroutineUseCase.a(r, (CoroutineUseCase.e<? super P, ? super E>) new a(qg4, coroutineUseCase, r));
        Object e = qg4.e();
        if (e == oc4.a()) {
            uc4.c(kc4);
        }
        return e;
    }
}
