package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class n14 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ u04 a;

    @DexIgnore
    public n14(u04 u04) {
        this.a = u04;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        if (this.a.e != null) {
            this.a.e.a(new s14(this));
        }
    }
}
