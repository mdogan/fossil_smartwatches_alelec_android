package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vj1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ rj1 e;
    @DexIgnore
    public /* final */ /* synthetic */ sj1 f;

    @DexIgnore
    public vj1(sj1 sj1, rj1 rj1) {
        this.f = sj1;
        this.e = rj1;
    }

    @DexIgnore
    public final void run() {
        this.f.a(this.e, false);
        sj1 sj1 = this.f;
        sj1.c = null;
        sj1.q().a((rj1) null);
    }
}
