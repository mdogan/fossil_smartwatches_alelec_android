package com.fossil.blesdk.obfuscated;

import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wy {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ f74 b;

    @DexIgnore
    public wy(String str, f74 f74) {
        this.a = str;
        this.b = f74;
    }

    @DexIgnore
    public boolean a() {
        try {
            return b().createNewFile();
        } catch (IOException e) {
            z44 g = r44.g();
            g.e("CrashlyticsCore", "Error creating marker: " + this.a, e);
            return false;
        }
    }

    @DexIgnore
    public final File b() {
        return new File(this.b.a(), this.a);
    }

    @DexIgnore
    public boolean c() {
        return b().exists();
    }

    @DexIgnore
    public boolean d() {
        return b().delete();
    }
}
