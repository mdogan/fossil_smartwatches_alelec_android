package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.notification.AppNotification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i22 implements n22 {
    @DexIgnore
    public int a() {
        return 5;
    }

    @DexIgnore
    public void a(o22 o22) {
        StringBuilder sb = new StringBuilder();
        sb.append(0);
        while (true) {
            if (!o22.i()) {
                break;
            }
            sb.append(o22.c());
            o22.f++;
            int a = q22.a(o22.d(), o22.f, a());
            if (a != a()) {
                o22.b(a);
                break;
            }
        }
        int length = sb.length() - 1;
        int a2 = o22.a() + length + 1;
        o22.c(a2);
        boolean z = o22.g().a() - a2 > 0;
        if (o22.i() || z) {
            if (length <= 249) {
                sb.setCharAt(0, (char) length);
            } else if (length <= 1555) {
                sb.setCharAt(0, (char) ((length / 250) + AppNotification.MAX_MESSAGE_LENGTH_IN_BYTE));
                sb.insert(1, (char) (length % 250));
            } else {
                throw new IllegalStateException("Message length not in valid ranges: " + length);
            }
        }
        int length2 = sb.length();
        for (int i = 0; i < length2; i++) {
            o22.a(a(sb.charAt(i), o22.a() + 1));
        }
    }

    @DexIgnore
    public static char a(char c, int i) {
        int i2 = c + ((i * 149) % 255) + 1;
        return i2 <= 255 ? (char) i2 : (char) (i2 - 256);
    }
}
