package com.fossil.blesdk.obfuscated;

import android.view.View;
import com.fossil.blesdk.obfuscated.l84;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n84 implements l84 {
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002f, code lost:
        if (r2 != null) goto L_0x0036;
     */
    @DexIgnore
    public k84 intercept(l84.a aVar) {
        String str;
        wd4.b(aVar, "chain");
        j84 n = aVar.n();
        View onCreateView = n.c().onCreateView(n.e(), n.d(), n.b(), n.a());
        if (onCreateView != null) {
            Class<?> cls = onCreateView.getClass();
            if (cls != null) {
                str = cls.getName();
            }
        }
        str = n.d();
        return new k84(onCreateView, str, n.b(), n.a());
    }
}
