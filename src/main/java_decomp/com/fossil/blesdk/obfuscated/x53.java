package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x53 implements Factory<WatchAppSearchPresenter> {
    @DexIgnore
    public static WatchAppSearchPresenter a(t53 t53, WatchAppRepository watchAppRepository, fn2 fn2) {
        return new WatchAppSearchPresenter(t53, watchAppRepository, fn2);
    }
}
