package com.fossil.blesdk.obfuscated;

import com.google.gson.JsonElement;
import com.google.gson.internal.LazilyParsedNumber;
import java.math.BigInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a02 extends JsonElement {
    @DexIgnore
    public static /* final */ Class<?>[] b; // = {Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class};
    @DexIgnore
    public Object a;

    @DexIgnore
    public a02(Boolean bool) {
        a((Object) bool);
    }

    @DexIgnore
    public void a(Object obj) {
        if (obj instanceof Character) {
            this.a = String.valueOf(((Character) obj).charValue());
            return;
        }
        j02.a((obj instanceof Number) || b(obj));
        this.a = obj;
    }

    @DexIgnore
    public int b() {
        return p() ? n().intValue() : Integer.parseInt(f());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || a02.class != obj.getClass()) {
            return false;
        }
        a02 a02 = (a02) obj;
        if (this.a == null) {
            if (a02.a == null) {
                return true;
            }
            return false;
        } else if (!a(this) || !a(a02)) {
            if (!(this.a instanceof Number) || !(a02.a instanceof Number)) {
                return this.a.equals(a02.a);
            }
            double doubleValue = n().doubleValue();
            double doubleValue2 = a02.n().doubleValue();
            if (doubleValue == doubleValue2) {
                return true;
            }
            if (!Double.isNaN(doubleValue) || !Double.isNaN(doubleValue2)) {
                return false;
            }
            return true;
        } else if (n().longValue() == a02.n().longValue()) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public String f() {
        if (p()) {
            return n().toString();
        }
        if (o()) {
            return k().toString();
        }
        return (String) this.a;
    }

    @DexIgnore
    public int hashCode() {
        long doubleToLongBits;
        if (this.a == null) {
            return 31;
        }
        if (a(this)) {
            doubleToLongBits = n().longValue();
        } else {
            Object obj = this.a;
            if (!(obj instanceof Number)) {
                return obj.hashCode();
            }
            doubleToLongBits = Double.doubleToLongBits(n().doubleValue());
        }
        return (int) ((doubleToLongBits >>> 32) ^ doubleToLongBits);
    }

    @DexIgnore
    public Boolean k() {
        return (Boolean) this.a;
    }

    @DexIgnore
    public double l() {
        return p() ? n().doubleValue() : Double.parseDouble(f());
    }

    @DexIgnore
    public long m() {
        return p() ? n().longValue() : Long.parseLong(f());
    }

    @DexIgnore
    public Number n() {
        Object obj = this.a;
        return obj instanceof String ? new LazilyParsedNumber((String) obj) : (Number) obj;
    }

    @DexIgnore
    public boolean o() {
        return this.a instanceof Boolean;
    }

    @DexIgnore
    public boolean p() {
        return this.a instanceof Number;
    }

    @DexIgnore
    public boolean q() {
        return this.a instanceof String;
    }

    @DexIgnore
    public static boolean b(Object obj) {
        if (obj instanceof String) {
            return true;
        }
        Class<?> cls = obj.getClass();
        for (Class<?> isAssignableFrom : b) {
            if (isAssignableFrom.isAssignableFrom(cls)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public a02(Number number) {
        a((Object) number);
    }

    @DexIgnore
    public a02(String str) {
        a((Object) str);
    }

    @DexIgnore
    public a02(Object obj) {
        a(obj);
    }

    @DexIgnore
    public boolean a() {
        if (o()) {
            return k().booleanValue();
        }
        return Boolean.parseBoolean(f());
    }

    @DexIgnore
    public static boolean a(a02 a02) {
        Object obj = a02.a;
        if (!(obj instanceof Number)) {
            return false;
        }
        Number number = (Number) obj;
        if ((number instanceof BigInteger) || (number instanceof Long) || (number instanceof Integer) || (number instanceof Short) || (number instanceof Byte)) {
            return true;
        }
        return false;
    }
}
