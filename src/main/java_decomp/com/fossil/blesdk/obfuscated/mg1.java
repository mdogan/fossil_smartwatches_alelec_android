package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class mg1 extends l61 implements lg1 {
    @DexIgnore
    public mg1() {
        super("com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                a((ig1) s61.a(parcel, ig1.CREATOR), (sl1) s61.a(parcel, sl1.CREATOR));
                parcel2.writeNoException();
                return true;
            case 2:
                a((ll1) s61.a(parcel, ll1.CREATOR), (sl1) s61.a(parcel, sl1.CREATOR));
                parcel2.writeNoException();
                return true;
            case 4:
                a((sl1) s61.a(parcel, sl1.CREATOR));
                parcel2.writeNoException();
                return true;
            case 5:
                a((ig1) s61.a(parcel, ig1.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 6:
                c((sl1) s61.a(parcel, sl1.CREATOR));
                parcel2.writeNoException();
                return true;
            case 7:
                List<ll1> a = a((sl1) s61.a(parcel, sl1.CREATOR), s61.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a);
                return true;
            case 9:
                byte[] a2 = a((ig1) s61.a(parcel, ig1.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(a2);
                return true;
            case 10:
                a(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 11:
                String d = d((sl1) s61.a(parcel, sl1.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(d);
                return true;
            case 12:
                a((wl1) s61.a(parcel, wl1.CREATOR), (sl1) s61.a(parcel, sl1.CREATOR));
                parcel2.writeNoException();
                return true;
            case 13:
                a((wl1) s61.a(parcel, wl1.CREATOR));
                parcel2.writeNoException();
                return true;
            case 14:
                List<ll1> a3 = a(parcel.readString(), parcel.readString(), s61.a(parcel), (sl1) s61.a(parcel, sl1.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a3);
                return true;
            case 15:
                List<ll1> a4 = a(parcel.readString(), parcel.readString(), parcel.readString(), s61.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a4);
                return true;
            case 16:
                List<wl1> a5 = a(parcel.readString(), parcel.readString(), (sl1) s61.a(parcel, sl1.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a5);
                return true;
            case 17:
                List<wl1> a6 = a(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeTypedList(a6);
                return true;
            case 18:
                b((sl1) s61.a(parcel, sl1.CREATOR));
                parcel2.writeNoException();
                return true;
            default:
                return false;
        }
    }
}
