package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.me;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mt2 extends me.d<DailyHeartRateSummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        wd4.b(dailyHeartRateSummary, "oldItem");
        wd4.b(dailyHeartRateSummary2, "newItem");
        return wd4.a((Object) dailyHeartRateSummary, (Object) dailyHeartRateSummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        wd4.b(dailyHeartRateSummary, "oldItem");
        wd4.b(dailyHeartRateSummary2, "newItem");
        return sk2.d(dailyHeartRateSummary.getDate(), dailyHeartRateSummary2.getDate());
    }
}
