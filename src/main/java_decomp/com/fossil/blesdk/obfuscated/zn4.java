package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ao4;
import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import okhttp3.internal.http2.ConnectionShutdownException;
import okhttp3.internal.http2.ErrorCode;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zn4 implements Closeable {
    @DexIgnore
    public static /* final */ ExecutorService y; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), vm4.a("OkHttp Http2Connection", true));
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ h f;
    @DexIgnore
    public /* final */ Map<Integer, bo4> g; // = new LinkedHashMap();
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public /* final */ ScheduledExecutorService l;
    @DexIgnore
    public /* final */ ExecutorService m;
    @DexIgnore
    public /* final */ eo4 n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public long p; // = 0;
    @DexIgnore
    public long q;
    @DexIgnore
    public fo4 r; // = new fo4();
    @DexIgnore
    public /* final */ fo4 s; // = new fo4();
    @DexIgnore
    public boolean t; // = false;
    @DexIgnore
    public /* final */ Socket u;
    @DexIgnore
    public /* final */ co4 v;
    @DexIgnore
    public /* final */ j w;
    @DexIgnore
    public /* final */ Set<Integer> x; // = new LinkedHashSet();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends um4 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ ErrorCode g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str, Object[] objArr, int i, ErrorCode errorCode) {
            super(str, objArr);
            this.f = i;
            this.g = errorCode;
        }

        @DexIgnore
        public void b() {
            try {
                zn4.this.b(this.f, this.g);
            } catch (IOException unused) {
                zn4.this.y();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends um4 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ long g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, Object[] objArr, int i, long j) {
            super(str, objArr);
            this.f = i;
            this.g = j;
        }

        @DexIgnore
        public void b() {
            try {
                zn4.this.v.a(this.f, this.g);
            } catch (IOException unused) {
                zn4.this.y();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends um4 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ List g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(String str, Object[] objArr, int i, List list) {
            super(str, objArr);
            this.f = i;
            this.g = list;
        }

        @DexIgnore
        public void b() {
            if (zn4.this.n.a(this.f, (List<vn4>) this.g)) {
                try {
                    zn4.this.v.a(this.f, ErrorCode.CANCEL);
                    synchronized (zn4.this) {
                        zn4.this.x.remove(Integer.valueOf(this.f));
                    }
                } catch (IOException unused) {
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends um4 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ List g;
        @DexIgnore
        public /* final */ /* synthetic */ boolean h;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(String str, Object[] objArr, int i2, List list, boolean z) {
            super(str, objArr);
            this.f = i2;
            this.g = list;
            this.h = z;
        }

        @DexIgnore
        public void b() {
            boolean a = zn4.this.n.a(this.f, this.g, this.h);
            if (a) {
                try {
                    zn4.this.v.a(this.f, ErrorCode.CANCEL);
                } catch (IOException unused) {
                    return;
                }
            }
            if (a || this.h) {
                synchronized (zn4.this) {
                    zn4.this.x.remove(Integer.valueOf(this.f));
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends um4 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ vo4 g;
        @DexIgnore
        public /* final */ /* synthetic */ int h;
        @DexIgnore
        public /* final */ /* synthetic */ boolean i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, Object[] objArr, int i2, vo4 vo4, int i3, boolean z) {
            super(str, objArr);
            this.f = i2;
            this.g = vo4;
            this.h = i3;
            this.i = z;
        }

        @DexIgnore
        public void b() {
            try {
                boolean a = zn4.this.n.a(this.f, this.g, this.h, this.i);
                if (a) {
                    zn4.this.v.a(this.f, ErrorCode.CANCEL);
                }
                if (a || this.i) {
                    synchronized (zn4.this) {
                        zn4.this.x.remove(Integer.valueOf(this.f));
                    }
                }
            } catch (IOException unused) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends um4 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ ErrorCode g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(String str, Object[] objArr, int i, ErrorCode errorCode) {
            super(str, objArr);
            this.f = i;
            this.g = errorCode;
        }

        @DexIgnore
        public void b() {
            zn4.this.n.a(this.f, this.g);
            synchronized (zn4.this) {
                zn4.this.x.remove(Integer.valueOf(this.f));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class h {
        @DexIgnore
        public static /* final */ h a; // = new a();

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends h {
            @DexIgnore
            public void a(bo4 bo4) throws IOException {
                bo4.a(ErrorCode.REFUSED_STREAM);
            }
        }

        @DexIgnore
        public abstract void a(bo4 bo4) throws IOException;

        @DexIgnore
        public void a(zn4 zn4) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i extends um4 {
        @DexIgnore
        public /* final */ boolean f;
        @DexIgnore
        public /* final */ int g;
        @DexIgnore
        public /* final */ int h;

        @DexIgnore
        public i(boolean z, int i2, int i3) {
            super("OkHttp %s ping %08x%08x", zn4.this.h, Integer.valueOf(i2), Integer.valueOf(i3));
            this.f = z;
            this.g = i2;
            this.h = i3;
        }

        @DexIgnore
        public void b() {
            zn4.this.b(this.f, this.g, this.h);
        }
    }

    /*
    static {
        Class<zn4> cls = zn4.class;
    }
    */

    @DexIgnore
    public zn4(g gVar) {
        g gVar2 = gVar;
        this.n = gVar2.f;
        boolean z = gVar2.g;
        this.e = z;
        this.f = gVar2.e;
        this.j = z ? 1 : 2;
        if (gVar2.g) {
            this.j += 2;
        }
        if (gVar2.g) {
            this.r.a(7, 16777216);
        }
        this.h = gVar2.b;
        this.l = new ScheduledThreadPoolExecutor(1, vm4.a(vm4.a("OkHttp %s Writer", this.h), false));
        if (gVar2.h != 0) {
            ScheduledExecutorService scheduledExecutorService = this.l;
            i iVar = new i(false, 0, 0);
            int i2 = gVar2.h;
            scheduledExecutorService.scheduleAtFixedRate(iVar, (long) i2, (long) i2, TimeUnit.MILLISECONDS);
        }
        this.m = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), vm4.a(vm4.a("OkHttp %s Push Observer", this.h), true));
        this.s.a(7, 65535);
        this.s.a(5, RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE);
        this.q = (long) this.s.c();
        this.u = gVar2.a;
        this.v = new co4(gVar2.d, this.e);
        this.w = new j(new ao4(gVar2.c, this.e));
    }

    @DexIgnore
    public synchronized int A() {
        return this.s.b(Integer.MAX_VALUE);
    }

    @DexIgnore
    public void B() throws IOException {
        a(true);
    }

    @DexIgnore
    public void c(int i2, ErrorCode errorCode) {
        try {
            this.l.execute(new a("OkHttp %s stream %d", new Object[]{this.h, Integer.valueOf(i2)}, i2, errorCode));
        } catch (RejectedExecutionException unused) {
        }
    }

    @DexIgnore
    public boolean c(int i2) {
        return i2 != 0 && (i2 & 1) == 0;
    }

    @DexIgnore
    public void close() throws IOException {
        a(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
    }

    @DexIgnore
    public synchronized bo4 d(int i2) {
        bo4 remove;
        remove = this.g.remove(Integer.valueOf(i2));
        notifyAll();
        return remove;
    }

    @DexIgnore
    public void flush() throws IOException {
        this.v.flush();
    }

    @DexIgnore
    public synchronized void h(long j2) {
        this.p += j2;
        if (this.p >= ((long) (this.r.c() / 2))) {
            c(0, this.p);
            this.p = 0;
        }
    }

    @DexIgnore
    public final void y() {
        try {
            a(ErrorCode.PROTOCOL_ERROR, ErrorCode.PROTOCOL_ERROR);
        } catch (IOException unused) {
        }
    }

    @DexIgnore
    public synchronized boolean z() {
        return this.k;
    }

    @DexIgnore
    public synchronized bo4 b(int i2) {
        return this.g.get(Integer.valueOf(i2));
    }

    @DexIgnore
    public void c(int i2, long j2) {
        try {
            this.l.execute(new b("OkHttp Window Update %s stream %d", new Object[]{this.h, Integer.valueOf(i2)}, i2, j2));
        } catch (RejectedExecutionException unused) {
        }
    }

    @DexIgnore
    public bo4 a(List<vn4> list, boolean z) throws IOException {
        return a(0, list, z);
    }

    @DexIgnore
    public void b(int i2, ErrorCode errorCode) throws IOException {
        this.v.a(i2, errorCode);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g {
        @DexIgnore
        public Socket a;
        @DexIgnore
        public String b;
        @DexIgnore
        public xo4 c;
        @DexIgnore
        public wo4 d;
        @DexIgnore
        public h e; // = h.a;
        @DexIgnore
        public eo4 f; // = eo4.a;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public int h;

        @DexIgnore
        public g(boolean z) {
            this.g = z;
        }

        @DexIgnore
        public g a(Socket socket, String str, xo4 xo4, wo4 wo4) {
            this.a = socket;
            this.b = str;
            this.c = xo4;
            this.d = wo4;
            return this;
        }

        @DexIgnore
        public g a(h hVar) {
            this.e = hVar;
            return this;
        }

        @DexIgnore
        public g a(int i) {
            this.h = i;
            return this;
        }

        @DexIgnore
        public zn4 a() {
            return new zn4(this);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0043  */
    public final bo4 a(int i2, List<vn4> list, boolean z) throws IOException {
        int i3;
        bo4 bo4;
        boolean z2;
        boolean z3 = !z;
        synchronized (this.v) {
            synchronized (this) {
                if (this.j > 1073741823) {
                    a(ErrorCode.REFUSED_STREAM);
                }
                if (!this.k) {
                    i3 = this.j;
                    this.j += 2;
                    bo4 = new bo4(i3, this, z3, false, (km4) null);
                    if (z && this.q != 0) {
                        if (bo4.b != 0) {
                            z2 = false;
                            if (bo4.g()) {
                                this.g.put(Integer.valueOf(i3), bo4);
                            }
                        }
                    }
                    z2 = true;
                    if (bo4.g()) {
                    }
                } else {
                    throw new ConnectionShutdownException();
                }
            }
            if (i2 == 0) {
                this.v.a(z3, i3, i2, list);
            } else if (!this.e) {
                this.v.a(i2, i3, list);
            } else {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            }
        }
        if (z2) {
            this.v.flush();
        }
        return bo4;
    }

    @DexIgnore
    public void b(boolean z, int i2, int i3) {
        boolean z2;
        if (!z) {
            synchronized (this) {
                z2 = this.o;
                this.o = true;
            }
            if (z2) {
                y();
                return;
            }
        }
        try {
            this.v.a(z, i2, i3);
        } catch (IOException unused) {
            y();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class j extends um4 implements ao4.b {
        @DexIgnore
        public /* final */ ao4 f;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends um4 {
            @DexIgnore
            public /* final */ /* synthetic */ bo4 f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, Object[] objArr, bo4 bo4) {
                super(str, objArr);
                this.f = bo4;
            }

            @DexIgnore
            public void b() {
                try {
                    zn4.this.f.a(this.f);
                } catch (IOException e) {
                    mo4 d = mo4.d();
                    d.a(4, "Http2Connection.Listener failure for " + zn4.this.h, (Throwable) e);
                    try {
                        this.f.a(ErrorCode.PROTOCOL_ERROR);
                    } catch (IOException unused) {
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class b extends um4 {
            @DexIgnore
            public b(String str, Object... objArr) {
                super(str, objArr);
            }

            @DexIgnore
            public void b() {
                zn4 zn4 = zn4.this;
                zn4.f.a(zn4);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class c extends um4 {
            @DexIgnore
            public /* final */ /* synthetic */ fo4 f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(String str, Object[] objArr, fo4 fo4) {
                super(str, objArr);
                this.f = fo4;
            }

            @DexIgnore
            public void b() {
                try {
                    zn4.this.v.a(this.f);
                } catch (IOException unused) {
                    zn4.this.y();
                }
            }
        }

        @DexIgnore
        public j(ao4 ao4) {
            super("OkHttp %s", zn4.this.h);
            this.f = ao4;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(int i, int i2, int i3, boolean z) {
        }

        @DexIgnore
        public void a(boolean z, int i, xo4 xo4, int i2) throws IOException {
            if (zn4.this.c(i)) {
                zn4.this.a(i, xo4, i2, z);
                return;
            }
            bo4 b2 = zn4.this.b(i);
            if (b2 == null) {
                zn4.this.c(i, ErrorCode.PROTOCOL_ERROR);
                long j = (long) i2;
                zn4.this.h(j);
                xo4.skip(j);
                return;
            }
            b2.a(xo4, i2);
            if (z) {
                b2.i();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
            r1 = okhttp3.internal.http2.ErrorCode.PROTOCOL_ERROR;
            r0 = okhttp3.internal.http2.ErrorCode.PROTOCOL_ERROR;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
            r2 = r4.g;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x002b, code lost:
            r2 = th;
         */
        @DexIgnore
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x001c */
        public void b() {
            ErrorCode errorCode;
            zn4 zn4;
            ErrorCode errorCode2 = ErrorCode.INTERNAL_ERROR;
            try {
                this.f.a((ao4.b) this);
                while (this.f.a(false, (ao4.b) this)) {
                }
                errorCode = ErrorCode.NO_ERROR;
                errorCode2 = ErrorCode.CANCEL;
                try {
                    zn4 = zn4.this;
                } catch (IOException unused) {
                }
            } catch (IOException unused2) {
                errorCode = errorCode2;
            } catch (Throwable th) {
                th = th;
                errorCode = errorCode2;
                try {
                    zn4.this.a(errorCode, errorCode2);
                } catch (IOException unused3) {
                }
                vm4.a((Closeable) this.f);
                throw th;
            }
            zn4.a(errorCode, errorCode2);
            vm4.a((Closeable) this.f);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0074, code lost:
            r0.a(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0077, code lost:
            if (r10 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0079, code lost:
            r0.i();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
            return;
         */
        @DexIgnore
        public void a(boolean z, int i, int i2, List<vn4> list) {
            if (zn4.this.c(i)) {
                zn4.this.b(i, list, z);
                return;
            }
            synchronized (zn4.this) {
                bo4 b2 = zn4.this.b(i);
                if (b2 == null) {
                    if (!zn4.this.k) {
                        if (i > zn4.this.i) {
                            if (i % 2 != zn4.this.j % 2) {
                                int i3 = i;
                                bo4 bo4 = new bo4(i3, zn4.this, false, z, vm4.b(list));
                                zn4.this.i = i;
                                zn4.this.g.put(Integer.valueOf(i), bo4);
                                zn4.y.execute(new a("OkHttp %s stream %d", new Object[]{zn4.this.h, Integer.valueOf(i)}, bo4));
                            }
                        }
                    }
                }
            }
        }

        @DexIgnore
        public void a(int i, ErrorCode errorCode) {
            if (zn4.this.c(i)) {
                zn4.this.a(i, errorCode);
                return;
            }
            bo4 d = zn4.this.d(i);
            if (d != null) {
                d.d(errorCode);
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v13, types: [java.lang.Object[]] */
        /* JADX WARNING: Multi-variable type inference failed */
        public void a(boolean z, fo4 fo4) {
            bo4[] bo4Arr;
            long j;
            int i;
            synchronized (zn4.this) {
                int c2 = zn4.this.s.c();
                if (z) {
                    zn4.this.s.a();
                }
                zn4.this.s.a(fo4);
                a(fo4);
                int c3 = zn4.this.s.c();
                bo4Arr = null;
                if (c3 == -1 || c3 == c2) {
                    j = 0;
                } else {
                    j = (long) (c3 - c2);
                    if (!zn4.this.t) {
                        zn4.this.t = true;
                    }
                    if (!zn4.this.g.isEmpty()) {
                        bo4Arr = zn4.this.g.values().toArray(new bo4[zn4.this.g.size()]);
                    }
                }
                zn4.y.execute(new b("OkHttp %s settings", zn4.this.h));
            }
            if (bo4Arr != null && j != 0) {
                for (bo4 bo4 : bo4Arr) {
                    synchronized (bo4) {
                        bo4.a(j);
                    }
                }
            }
        }

        @DexIgnore
        public final void a(fo4 fo4) {
            try {
                zn4.this.l.execute(new c("OkHttp %s ACK Settings", new Object[]{zn4.this.h}, fo4));
            } catch (RejectedExecutionException unused) {
            }
        }

        @DexIgnore
        public void a(boolean z, int i, int i2) {
            if (z) {
                synchronized (zn4.this) {
                    boolean unused = zn4.this.o = false;
                    zn4.this.notifyAll();
                }
                return;
            }
            try {
                zn4.this.l.execute(new i(true, i, i2));
            } catch (RejectedExecutionException unused2) {
            }
        }

        @DexIgnore
        public void a(int i, ErrorCode errorCode, ByteString byteString) {
            bo4[] bo4Arr;
            byteString.size();
            synchronized (zn4.this) {
                bo4Arr = (bo4[]) zn4.this.g.values().toArray(new bo4[zn4.this.g.size()]);
                zn4.this.k = true;
            }
            for (bo4 bo4 : bo4Arr) {
                if (bo4.c() > i && bo4.f()) {
                    bo4.d(ErrorCode.REFUSED_STREAM);
                    zn4.this.d(bo4.c());
                }
            }
        }

        @DexIgnore
        public void a(int i, long j) {
            if (i == 0) {
                synchronized (zn4.this) {
                    zn4.this.q += j;
                    zn4.this.notifyAll();
                }
                return;
            }
            bo4 b2 = zn4.this.b(i);
            if (b2 != null) {
                synchronized (b2) {
                    b2.a(j);
                }
            }
        }

        @DexIgnore
        public void a(int i, int i2, List<vn4> list) {
            zn4.this.a(i2, list);
        }
    }

    @DexIgnore
    public void b(int i2, List<vn4> list, boolean z) {
        try {
            a((um4) new d("OkHttp %s Push Headers[%s]", new Object[]{this.h, Integer.valueOf(i2)}, i2, list, z));
        } catch (RejectedExecutionException unused) {
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:26|27|28) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r3 = java.lang.Math.min((int) java.lang.Math.min(r12, r8.q), r8.v.r());
        r6 = (long) r3;
        r8.q -= r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0066, code lost:
        throw new java.io.InterruptedIOException();
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x005a */
    public void a(int i2, boolean z, vo4 vo4, long j2) throws IOException {
        int min;
        long j3;
        if (j2 == 0) {
            this.v.a(z, i2, vo4, 0);
            return;
        }
        while (j2 > 0) {
            synchronized (this) {
                while (true) {
                    if (this.q > 0) {
                        break;
                    } else if (this.g.containsKey(Integer.valueOf(i2))) {
                        wait();
                    } else {
                        throw new IOException("stream closed");
                    }
                }
            }
            j2 -= j3;
            this.v.a(z && j2 == 0, i2, vo4, min);
        }
    }

    @DexIgnore
    public void a(ErrorCode errorCode) throws IOException {
        synchronized (this.v) {
            synchronized (this) {
                if (!this.k) {
                    this.k = true;
                    int i2 = this.i;
                    this.v.a(i2, errorCode, vm4.a);
                }
            }
        }
    }

    @DexIgnore
    public void a(ErrorCode errorCode, ErrorCode errorCode2) throws IOException {
        bo4[] bo4Arr = null;
        try {
            a(errorCode);
            e = null;
        } catch (IOException e2) {
            e = e2;
        }
        synchronized (this) {
            if (!this.g.isEmpty()) {
                bo4Arr = (bo4[]) this.g.values().toArray(new bo4[this.g.size()]);
                this.g.clear();
            }
        }
        if (bo4Arr != null) {
            for (bo4 a2 : bo4Arr) {
                try {
                    a2.a(errorCode2);
                } catch (IOException e3) {
                    if (e != null) {
                        e = e3;
                    }
                }
            }
        }
        try {
            this.v.close();
        } catch (IOException e4) {
            if (e == null) {
                e = e4;
            }
        }
        try {
            this.u.close();
        } catch (IOException e5) {
            e = e5;
        }
        this.l.shutdown();
        this.m.shutdown();
        if (e != null) {
            throw e;
        }
    }

    @DexIgnore
    public void a(boolean z) throws IOException {
        if (z) {
            this.v.p();
            this.v.b(this.r);
            int c2 = this.r.c();
            if (c2 != 65535) {
                this.v.a(0, (long) (c2 - 65535));
            }
        }
        new Thread(this.w).start();
    }

    @DexIgnore
    public void a(int i2, List<vn4> list) {
        synchronized (this) {
            if (this.x.contains(Integer.valueOf(i2))) {
                c(i2, ErrorCode.PROTOCOL_ERROR);
                return;
            }
            this.x.add(Integer.valueOf(i2));
            try {
                a((um4) new c("OkHttp %s Push Request[%s]", new Object[]{this.h, Integer.valueOf(i2)}, i2, list));
            } catch (RejectedExecutionException unused) {
            }
        }
    }

    @DexIgnore
    public void a(int i2, xo4 xo4, int i3, boolean z) throws IOException {
        vo4 vo4 = new vo4();
        long j2 = (long) i3;
        xo4.g(j2);
        xo4.b(vo4, j2);
        if (vo4.B() == j2) {
            a((um4) new e("OkHttp %s Push Data[%s]", new Object[]{this.h, Integer.valueOf(i2)}, i2, vo4, i3, z));
            return;
        }
        throw new IOException(vo4.B() + " != " + i3);
    }

    @DexIgnore
    public void a(int i2, ErrorCode errorCode) {
        a((um4) new f("OkHttp %s Push Reset[%s]", new Object[]{this.h, Integer.valueOf(i2)}, i2, errorCode));
    }

    @DexIgnore
    public final synchronized void a(um4 um4) {
        if (!z()) {
            this.m.execute(um4);
        }
    }
}
