package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ko1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ xn1 e;
    @DexIgnore
    public /* final */ /* synthetic */ jo1 f;

    @DexIgnore
    public ko1(jo1 jo1, xn1 xn1) {
        this.f = jo1;
        this.e = xn1;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.f.b) {
            if (this.f.c != null) {
                this.f.c.onComplete(this.e);
            }
        }
    }
}
