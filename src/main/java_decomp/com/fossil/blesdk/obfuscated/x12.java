package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x12 {
    @DexIgnore
    public static /* final */ String[] b; // = {"UPPER", "LOWER", "DIGIT", "MIXED", "PUNCT"};
    @DexIgnore
    public static /* final */ int[][] c; // = {new int[]{0, 327708, 327710, 327709, 656318}, new int[]{590318, 0, 327710, 327709, 656318}, new int[]{262158, 590300, 0, 590301, 932798}, new int[]{327709, 327708, 656318, 0, 327710}, new int[]{327711, 656380, 656382, 656381, 0}};
    @DexIgnore
    public static /* final */ int[][] d;
    @DexIgnore
    public static /* final */ int[][] e;
    @DexIgnore
    public /* final */ byte[] a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Comparator<z12> {
        @DexIgnore
        public a(x12 x12) {
        }

        @DexIgnore
        /* renamed from: a */
        public int compare(z12 z12, z12 z122) {
            return z12.b() - z122.b();
        }
    }

    /*
    static {
        Class<int> cls = int.class;
        int[][] iArr = (int[][]) Array.newInstance(cls, new int[]{5, 256});
        d = iArr;
        iArr[0][32] = 1;
        for (int i = 65; i <= 90; i++) {
            d[0][i] = (i - 65) + 2;
        }
        d[1][32] = 1;
        for (int i2 = 97; i2 <= 122; i2++) {
            d[1][i2] = (i2 - 97) + 2;
        }
        d[2][32] = 1;
        for (int i3 = 48; i3 <= 57; i3++) {
            d[2][i3] = (i3 - 48) + 2;
        }
        int[][] iArr2 = d;
        iArr2[2][44] = 12;
        iArr2[2][46] = 13;
        int[] iArr3 = {0, 32, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 27, 28, 29, 30, 31, 64, 92, 94, 95, 96, 124, 126, 127};
        for (int i4 = 0; i4 < 28; i4++) {
            d[3][iArr3[i4]] = i4;
        }
        int[] iArr4 = {0, 13, 0, 0, 0, 0, 33, 39, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 58, 59, 60, 61, 62, 63, 91, 93, 123, 125};
        for (int i5 = 0; i5 < 31; i5++) {
            if (iArr4[i5] > 0) {
                d[4][iArr4[i5]] = i5;
            }
        }
        int[][] iArr5 = (int[][]) Array.newInstance(cls, new int[]{6, 6});
        e = iArr5;
        for (int[] fill : iArr5) {
            Arrays.fill(fill, -1);
        }
        int[][] iArr6 = e;
        iArr6[0][4] = 0;
        iArr6[1][4] = 0;
        iArr6[1][0] = 28;
        iArr6[3][4] = 0;
        iArr6[2][4] = 0;
        iArr6[2][0] = 15;
    }
    */

    @DexIgnore
    public x12(byte[] bArr) {
        this.a = bArr;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0049  */
    public b22 a() {
        int i;
        Collection singletonList = Collections.singletonList(z12.e);
        int i2 = 0;
        while (true) {
            byte[] bArr = this.a;
            if (i2 >= bArr.length) {
                return ((z12) Collections.min(singletonList, new a(this))).a(this.a);
            }
            int i3 = i2 + 1;
            byte b2 = i3 < bArr.length ? bArr[i3] : 0;
            byte b3 = this.a[i2];
            if (b3 != 13) {
                if (b3 != 44) {
                    if (b3 != 46) {
                        if (b3 == 58 && b2 == 32) {
                            i = 5;
                            if (i > 0) {
                                singletonList = a((Iterable<z12>) singletonList, i2, i);
                                i2 = i3;
                            } else {
                                singletonList = a(singletonList, i2);
                            }
                            i2++;
                        }
                    } else if (b2 == 32) {
                        i = 3;
                        if (i > 0) {
                        }
                        i2++;
                    }
                } else if (b2 == 32) {
                    i = 4;
                    if (i > 0) {
                    }
                    i2++;
                }
            } else if (b2 == 10) {
                i = 2;
                if (i > 0) {
                }
                i2++;
            }
            i = 0;
            if (i > 0) {
            }
            i2++;
        }
    }

    @DexIgnore
    public final Collection<z12> a(Iterable<z12> iterable, int i) {
        LinkedList linkedList = new LinkedList();
        for (z12 a2 : iterable) {
            a(a2, i, (Collection<z12>) linkedList);
        }
        return a(linkedList);
    }

    @DexIgnore
    public final void a(z12 z12, int i, Collection<z12> collection) {
        char c2 = (char) (this.a[i] & FileType.MASKED_INDEX);
        boolean z = d[z12.c()][c2] > 0;
        z12 z122 = null;
        for (int i2 = 0; i2 <= 4; i2++) {
            int i3 = d[i2][c2];
            if (i3 > 0) {
                if (z122 == null) {
                    z122 = z12.b(i);
                }
                if (!z || i2 == z12.c() || i2 == 2) {
                    collection.add(z122.a(i2, i3));
                }
                if (!z && e[z12.c()][i2] >= 0) {
                    collection.add(z122.b(i2, i3));
                }
            }
        }
        if (z12.a() > 0 || d[z12.c()][c2] == 0) {
            collection.add(z12.a(i));
        }
    }

    @DexIgnore
    public static Collection<z12> a(Iterable<z12> iterable, int i, int i2) {
        LinkedList linkedList = new LinkedList();
        for (z12 a2 : iterable) {
            a(a2, i, i2, linkedList);
        }
        return a(linkedList);
    }

    @DexIgnore
    public static void a(z12 z12, int i, int i2, Collection<z12> collection) {
        z12 b2 = z12.b(i);
        collection.add(b2.a(4, i2));
        if (z12.c() != 4) {
            collection.add(b2.b(4, i2));
        }
        if (i2 == 3 || i2 == 4) {
            collection.add(b2.a(2, 16 - i2).a(2, 1));
        }
        if (z12.a() > 0) {
            collection.add(z12.a(i).a(i + 1));
        }
    }

    @DexIgnore
    public static Collection<z12> a(Iterable<z12> iterable) {
        LinkedList linkedList = new LinkedList();
        for (z12 next : iterable) {
            boolean z = true;
            Iterator it = linkedList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                z12 z12 = (z12) it.next();
                if (z12.a(next)) {
                    z = false;
                    break;
                } else if (next.a(z12)) {
                    it.remove();
                }
            }
            if (z) {
                linkedList.add(next);
            }
        }
        return linkedList;
    }
}
