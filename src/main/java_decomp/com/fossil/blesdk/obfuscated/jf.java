package com.fossil.blesdk.obfuscated;

import android.os.Build;
import androidx.renderscript.Allocation;
import androidx.renderscript.Element;
import androidx.renderscript.RSIllegalArgumentException;
import androidx.renderscript.RenderScript;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jf extends Cif {
    @DexIgnore
    public jf(long j, RenderScript renderScript) {
        super(j, renderScript);
    }

    @DexIgnore
    public static jf a(RenderScript renderScript, Element element) {
        if (element.a(Element.h(renderScript)) || element.a(Element.g(renderScript))) {
            boolean z = renderScript.d() && Build.VERSION.SDK_INT < 19;
            jf jfVar = new jf(renderScript.a(5, element.a(renderScript), z), renderScript);
            jfVar.a(z);
            jfVar.a(5.0f);
            return jfVar;
        }
        throw new RSIllegalArgumentException("Unsupported element type.");
    }

    @DexIgnore
    public void b(Allocation allocation) {
        if (allocation.e().i() != 0) {
            a(0, (Allocation) null, allocation, (gf) null);
            return;
        }
        throw new RSIllegalArgumentException("Output is a 1D Allocation");
    }

    @DexIgnore
    public void c(Allocation allocation) {
        if (allocation.e().i() != 0) {
            a(1, (ff) allocation);
            return;
        }
        throw new RSIllegalArgumentException("Input set to a 1D Allocation");
    }

    @DexIgnore
    public void a(float f) {
        if (f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f > 25.0f) {
            throw new RSIllegalArgumentException("Radius out of range (0 < r <= 25).");
        }
        a(0, f);
    }
}
