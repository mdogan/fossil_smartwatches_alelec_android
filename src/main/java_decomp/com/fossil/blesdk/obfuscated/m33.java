package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.s62;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.Ringtone;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m33 extends as2 implements l33, s62.b {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((rd4) null);
    @DexIgnore
    public ur3<l82> j;
    @DexIgnore
    public s62 k;
    @DexIgnore
    public k33 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return m33.n;
        }

        @DexIgnore
        public final m33 b() {
            return new m33();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ m33 e;

        @DexIgnore
        public b(m33 m33) {
            this.e = m33;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    /*
    static {
        String simpleName = m33.class.getSimpleName();
        wd4.a((Object) simpleName, "SearchRingPhoneFragment::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return n;
    }

    @DexIgnore
    public boolean S0() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return false;
        }
        k33 k33 = this.l;
        if (k33 != null) {
            k33.i();
            Intent intent = new Intent();
            k33 k332 = this.l;
            if (k332 != null) {
                intent.putExtra("KEY_SELECTED_RINGPHONE", k332.h());
                activity.setResult(-1, intent);
                activity.finish();
                return false;
            }
            wd4.d("mPresenter");
            throw null;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new ur3<>(this, (l82) ra.a(layoutInflater, R.layout.activity_search_ringphone, viewGroup, false, O0()));
        ur3<l82> ur3 = this.j;
        if (ur3 != null) {
            l82 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        k33 k33 = this.l;
        if (k33 != null) {
            k33.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        k33 k33 = this.l;
        if (k33 != null) {
            k33.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<l82> ur3 = this.j;
        if (ur3 != null) {
            l82 a2 = ur3.a();
            if (a2 != null) {
                this.k = new s62(this);
                RecyclerView recyclerView = a2.r;
                wd4.a((Object) recyclerView, "it.rvRingphones");
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView2 = a2.r;
                wd4.a((Object) recyclerView2, "it.rvRingphones");
                s62 s62 = this.k;
                if (s62 != null) {
                    recyclerView2.setAdapter(s62);
                    a2.q.setOnClickListener(new b(this));
                    return;
                }
                wd4.d("mSearchRingPhoneAdapter");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void r(List<Ringtone> list) {
        wd4.b(list, "data");
        s62 s62 = this.k;
        if (s62 != null) {
            k33 k33 = this.l;
            if (k33 != null) {
                s62.a(list, k33.h());
            } else {
                wd4.d("mPresenter");
                throw null;
            }
        } else {
            wd4.d("mSearchRingPhoneAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(Ringtone ringtone) {
        wd4.b(ringtone, Constants.RINGTONE);
        k33 k33 = this.l;
        if (k33 != null) {
            k33.a(ringtone);
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(k33 k33) {
        wd4.b(k33, "presenter");
        this.l = k33;
    }
}
