package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kg0 implements oe0<Status> {
    @DexIgnore
    public /* final */ /* synthetic */ ff0 a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ he0 c;
    @DexIgnore
    public /* final */ /* synthetic */ fg0 d;

    @DexIgnore
    public kg0(fg0 fg0, ff0 ff0, boolean z, he0 he0) {
        this.d = fg0;
        this.a = ff0;
        this.b = z;
        this.c = he0;
    }

    @DexIgnore
    public final /* synthetic */ void onResult(ne0 ne0) {
        Status status = (Status) ne0;
        dc0.a(this.d.g).e();
        if (status.L() && this.d.g()) {
            this.d.k();
        }
        this.a.a(status);
        if (this.b) {
            this.c.d();
        }
    }
}
