package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class am0 extends zy0 implements yl0 {
    @DexIgnore
    public am0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
    }

    @DexIgnore
    public final boolean a(nn0 nn0, tn0 tn0) throws RemoteException {
        Parcel o = o();
        bz0.a(o, (Parcelable) nn0);
        bz0.a(o, (IInterface) tn0);
        Parcel a = a(5, o);
        boolean a2 = bz0.a(a);
        a.recycle();
        return a2;
    }
}
