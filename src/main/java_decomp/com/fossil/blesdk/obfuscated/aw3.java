package com.fossil.blesdk.obfuscated;

import com.squareup.okhttp.internal.framed.ErrorCode;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface aw3 extends Closeable {
    @DexIgnore
    void a(int i, int i2, List<dw3> list) throws IOException;

    @DexIgnore
    void a(int i, long j) throws IOException;

    @DexIgnore
    void a(int i, ErrorCode errorCode) throws IOException;

    @DexIgnore
    void a(int i, ErrorCode errorCode, byte[] bArr) throws IOException;

    @DexIgnore
    void a(lw3 lw3) throws IOException;

    @DexIgnore
    void a(boolean z, int i, int i2) throws IOException;

    @DexIgnore
    void a(boolean z, int i, vo4 vo4, int i2) throws IOException;

    @DexIgnore
    void a(boolean z, boolean z2, int i, int i2, List<dw3> list) throws IOException;

    @DexIgnore
    void b(lw3 lw3) throws IOException;

    @DexIgnore
    void flush() throws IOException;

    @DexIgnore
    void p() throws IOException;

    @DexIgnore
    int r();
}
