package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gc extends Fragment {
    @DexIgnore
    public a e;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();

        @DexIgnore
        void d();

        @DexIgnore
        void e();
    }

    @DexIgnore
    public static gc a(Activity activity) {
        return (gc) activity.getFragmentManager().findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag");
    }

    @DexIgnore
    public static void b(Activity activity) {
        FragmentManager fragmentManager = activity.getFragmentManager();
        if (fragmentManager.findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag") == null) {
            fragmentManager.beginTransaction().add(new gc(), "androidx.lifecycle.LifecycleDispatcher.report_fragment_tag").commit();
            fragmentManager.executePendingTransactions();
        }
    }

    @DexIgnore
    public final void c(a aVar) {
        if (aVar != null) {
            aVar.a();
        }
    }

    @DexIgnore
    public void d(a aVar) {
        this.e = aVar;
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        a(this.e);
        a(Lifecycle.Event.ON_CREATE);
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        a(Lifecycle.Event.ON_DESTROY);
        this.e = null;
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        a(Lifecycle.Event.ON_PAUSE);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        b(this.e);
        a(Lifecycle.Event.ON_RESUME);
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        c(this.e);
        a(Lifecycle.Event.ON_START);
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        a(Lifecycle.Event.ON_STOP);
    }

    @DexIgnore
    public final void a(a aVar) {
        if (aVar != null) {
            aVar.e();
        }
    }

    @DexIgnore
    public final void a(Lifecycle.Event event) {
        Activity activity = getActivity();
        if (activity instanceof yb) {
            ((yb) activity).getLifecycle().a(event);
        } else if (activity instanceof LifecycleOwner) {
            Lifecycle lifecycle = ((LifecycleOwner) activity).getLifecycle();
            if (lifecycle instanceof LifecycleRegistry) {
                ((LifecycleRegistry) lifecycle).a(event);
            }
        }
    }

    @DexIgnore
    public final void b(a aVar) {
        if (aVar != null) {
            aVar.d();
        }
    }
}
