package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class af1 extends b51 implements pe1 {
    @DexIgnore
    public af1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IUiSettingsDelegate");
    }

    @DexIgnore
    public final void d(boolean z) throws RemoteException {
        Parcel o = o();
        d51.a(o, z);
        b(18, o);
    }
}
