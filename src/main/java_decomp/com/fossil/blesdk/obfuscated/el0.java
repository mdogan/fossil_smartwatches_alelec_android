package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.he0;
import com.fossil.blesdk.obfuscated.jj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class el0 implements jj0.a {
    @DexIgnore
    public /* final */ /* synthetic */ he0.b a;

    @DexIgnore
    public el0(he0.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        this.a.e(bundle);
    }

    @DexIgnore
    public final void f(int i) {
        this.a.f(i);
    }
}
