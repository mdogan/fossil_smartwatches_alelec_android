package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppPresenter;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g03 implements Factory<NotificationHybridAppPresenter> {
    @DexIgnore
    public static NotificationHybridAppPresenter a(b03 b03, int i, ArrayList<String> arrayList, k62 k62, i03 i03) {
        return new NotificationHybridAppPresenter(b03, i, arrayList, k62, i03);
    }
}
