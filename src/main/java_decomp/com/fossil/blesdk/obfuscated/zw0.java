package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzbb;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zw0 implements ax0 {
    @DexIgnore
    public /* final */ /* synthetic */ zzbb a;

    @DexIgnore
    public zw0(zzbb zzbb) {
        this.a = zzbb;
    }

    @DexIgnore
    public final byte a(int i) {
        return this.a.zzj(i);
    }

    @DexIgnore
    public final int size() {
        return this.a.size();
    }
}
