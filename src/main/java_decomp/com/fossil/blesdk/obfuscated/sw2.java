package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sw2 implements Factory<NotificationCallsAndMessagesPresenter> {
    @DexIgnore
    public static NotificationCallsAndMessagesPresenter a(nw2 nw2, k62 k62, qx2 qx2, sx2 sx2, tx2 tx2, wy2 wy2, NotificationSettingsDao notificationSettingsDao, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new NotificationCallsAndMessagesPresenter(nw2, k62, qx2, sx2, tx2, wy2, notificationSettingsDao, notificationSettingsDatabase);
    }
}
