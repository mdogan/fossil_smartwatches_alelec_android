package com.fossil.blesdk.obfuscated;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.blesdk.obfuscated.th4;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class fi4 extends gi4 implements th4 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater h;
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater i;
    @DexIgnore
    public volatile Object _delayed; // = null;
    @DexIgnore
    public volatile Object _queue; // = null;
    @DexIgnore
    public volatile boolean isCompleted;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends c {
        @DexIgnore
        public /* final */ pg4<cb4> h;
        @DexIgnore
        public /* final */ /* synthetic */ fi4 i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(fi4 fi4, long j, pg4<? super cb4> pg4) {
            super(j);
            wd4.b(pg4, "cont");
            this.i = fi4;
            this.h = pg4;
        }

        @DexIgnore
        public void run() {
            this.h.a((gh4) this.i, cb4.a);
        }

        @DexIgnore
        public String toString() {
            return super.toString() + this.h.toString();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends c {
        @DexIgnore
        public /* final */ Runnable h;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(long j, Runnable runnable) {
            super(j);
            wd4.b(runnable, "block");
            this.h = runnable;
        }

        @DexIgnore
        public void run() {
            this.h.run();
        }

        @DexIgnore
        public String toString() {
            return super.toString() + this.h.toString();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends tk4<c> {
        @DexIgnore
        public long b;

        @DexIgnore
        public d(long j) {
            this.b = j;
        }
    }

    /*
    static {
        Class<Object> cls = Object.class;
        Class<fi4> cls2 = fi4.class;
        h = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_queue");
        i = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_delayed");
    }
    */

    @DexIgnore
    public long C() {
        if (super.C() == 0) {
            return 0;
        }
        Object obj = this._queue;
        if (obj != null) {
            if (obj instanceof gk4) {
                if (!((gk4) obj).c()) {
                    return 0;
                }
            } else if (obj == ii4.b) {
                return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
            } else {
                return 0;
            }
        }
        d dVar = (d) this._delayed;
        if (dVar != null) {
            c cVar = (c) dVar.d();
            if (cVar != null) {
                long j = cVar.g;
                oj4 a2 = pj4.a();
                return qe4.a(j - (a2 != null ? a2.a() : System.nanoTime()), 0);
            }
        }
        return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0055  */
    public long F() {
        Runnable L;
        Object obj;
        if (G()) {
            return C();
        }
        d dVar = (d) this._delayed;
        if (dVar == null || dVar.c()) {
            L = L();
            if (L != null) {
                L.run();
            }
            return C();
        }
        oj4 a2 = pj4.a();
        long a3 = a2 != null ? a2.a() : System.nanoTime();
        do {
            synchronized (dVar) {
                uk4 a4 = dVar.a();
                obj = null;
                if (a4 != null) {
                    c cVar = (c) a4;
                    if (cVar.a(a3) ? b(cVar) : false) {
                        obj = dVar.a(0);
                    }
                }
            }
        } while (((c) obj) != null);
        L = L();
        if (L != null) {
        }
        return C();
    }

    @DexIgnore
    public final void K() {
        if (!oh4.a() || this.isCompleted) {
            while (true) {
                Object obj = this._queue;
                if (obj == null) {
                    if (h.compareAndSet(this, (Object) null, ii4.b)) {
                        return;
                    }
                } else if (obj instanceof gk4) {
                    ((gk4) obj).a();
                    return;
                } else if (obj != ii4.b) {
                    gk4 gk4 = new gk4(8, true);
                    if (obj != null) {
                        gk4.a((Runnable) obj);
                        if (h.compareAndSet(this, obj, gk4)) {
                            return;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                    }
                } else {
                    return;
                }
            }
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public final Runnable L() {
        while (true) {
            Object obj = this._queue;
            if (obj == null) {
                return null;
            }
            if (obj instanceof gk4) {
                if (obj != null) {
                    gk4 gk4 = (gk4) obj;
                    Object f = gk4.f();
                    if (f != gk4.g) {
                        return (Runnable) f;
                    }
                    h.compareAndSet(this, obj, gk4.e());
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
            } else if (obj == ii4.b) {
                return null;
            } else {
                if (h.compareAndSet(this, obj, (Object) null)) {
                    if (obj != null) {
                        return (Runnable) obj;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
            }
        }
    }

    @DexIgnore
    public boolean M() {
        if (!E()) {
            return false;
        }
        d dVar = (d) this._delayed;
        if (dVar != null && !dVar.c()) {
            return false;
        }
        Object obj = this._queue;
        if (obj != null) {
            if (obj instanceof gk4) {
                return ((gk4) obj).c();
            }
            if (obj == ii4.b) {
                return true;
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void N() {
        oj4 a2 = pj4.a();
        long a3 = a2 != null ? a2.a() : System.nanoTime();
        while (true) {
            d dVar = (d) this._delayed;
            if (dVar != null) {
                c cVar = (c) dVar.f();
                if (cVar != null) {
                    a(a3, cVar);
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final void O() {
        this._queue = null;
        this._delayed = null;
    }

    @DexIgnore
    public final ai4 b(long j, Runnable runnable) {
        wd4.b(runnable, "block");
        long a2 = ii4.a(j);
        if (a2 >= 4611686018427387903L) {
            return dj4.e;
        }
        oj4 a3 = pj4.a();
        long a4 = a3 != null ? a3.a() : System.nanoTime();
        b bVar = new b(a2 + a4, runnable);
        b(a4, (c) bVar);
        return bVar;
    }

    @DexIgnore
    public final int c(long j, c cVar) {
        if (this.isCompleted) {
            return 1;
        }
        d dVar = (d) this._delayed;
        if (dVar == null) {
            i.compareAndSet(this, (Object) null, new d(j));
            Object obj = this._delayed;
            if (obj != null) {
                dVar = (d) obj;
            } else {
                wd4.a();
                throw null;
            }
        }
        return cVar.a(j, dVar, this);
    }

    @DexIgnore
    public void shutdown() {
        nj4.b.c();
        this.isCompleted = true;
        K();
        do {
        } while (F() <= 0);
        N();
    }

    @DexIgnore
    public ai4 a(long j, Runnable runnable) {
        wd4.b(runnable, "block");
        return th4.a.a(this, j, runnable);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c implements Runnable, Comparable<c>, ai4, uk4 {
        @DexIgnore
        public Object e;
        @DexIgnore
        public int f; // = -1;
        @DexIgnore
        public long g;

        @DexIgnore
        public c(long j) {
            this.g = j;
        }

        @DexIgnore
        public void a(tk4<?> tk4) {
            if (this.e != ii4.a) {
                this.e = tk4;
                return;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }

        @DexIgnore
        public final synchronized void dispose() {
            Object obj = this.e;
            if (obj != ii4.a) {
                if (!(obj instanceof d)) {
                    obj = null;
                }
                d dVar = (d) obj;
                if (dVar != null) {
                    dVar.b(this);
                }
                this.e = ii4.a;
            }
        }

        @DexIgnore
        public tk4<?> i() {
            Object obj = this.e;
            if (!(obj instanceof tk4)) {
                obj = null;
            }
            return (tk4) obj;
        }

        @DexIgnore
        public int j() {
            return this.f;
        }

        @DexIgnore
        public String toString() {
            return "Delayed[nanos=" + this.g + ']';
        }

        @DexIgnore
        public void a(int i) {
            this.f = i;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(c cVar) {
            wd4.b(cVar, FacebookRequestErrorClassification.KEY_OTHER);
            int i = ((this.g - cVar.g) > 0 ? 1 : ((this.g - cVar.g) == 0 ? 0 : -1));
            if (i > 0) {
                return 1;
            }
            return i < 0 ? -1 : 0;
        }

        @DexIgnore
        public final boolean a(long j) {
            return j - this.g >= 0;
        }

        @DexIgnore
        public final synchronized int a(long j, d dVar, fi4 fi4) {
            wd4.b(dVar, "delayed");
            wd4.b(fi4, "eventLoop");
            if (this.e == ii4.a) {
                return 2;
            }
            synchronized (dVar) {
                c cVar = (c) dVar.a();
                if (fi4.isCompleted) {
                    return 1;
                }
                if (cVar == null) {
                    dVar.b = j;
                } else {
                    long j2 = cVar.g;
                    if (j2 - j < 0) {
                        j = j2;
                    }
                    if (j - dVar.b > 0) {
                        dVar.b = j;
                    }
                }
                if (this.g - dVar.b < 0) {
                    this.g = dVar.b;
                }
                dVar.a(this);
                return 0;
            }
        }
    }

    @DexIgnore
    public void a(long j, pg4<? super cb4> pg4) {
        wd4.b(pg4, "continuation");
        long a2 = ii4.a(j);
        if (a2 < 4611686018427387903L) {
            oj4 a3 = pj4.a();
            long a4 = a3 != null ? a3.a() : System.nanoTime();
            a aVar = new a(this, a2 + a4, pg4);
            rg4.a((pg4<?>) pg4, (ai4) aVar);
            b(a4, (c) aVar);
        }
    }

    @DexIgnore
    public final void b(long j, c cVar) {
        wd4.b(cVar, "delayedTask");
        int c2 = c(j, cVar);
        if (c2 != 0) {
            if (c2 == 1) {
                a(j, cVar);
            } else if (c2 != 2) {
                throw new IllegalStateException("unexpected result".toString());
            }
        } else if (a(cVar)) {
            J();
        }
    }

    @DexIgnore
    public final void a(CoroutineContext coroutineContext, Runnable runnable) {
        wd4.b(coroutineContext, "context");
        wd4.b(runnable, "block");
        a(runnable);
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        wd4.b(runnable, "task");
        if (b(runnable)) {
            J();
        } else {
            qh4.k.a(runnable);
        }
    }

    @DexIgnore
    public final boolean b(Runnable runnable) {
        while (true) {
            Object obj = this._queue;
            if (this.isCompleted) {
                return false;
            }
            if (obj == null) {
                if (h.compareAndSet(this, (Object) null, runnable)) {
                    return true;
                }
            } else if (obj instanceof gk4) {
                if (obj != null) {
                    gk4 gk4 = (gk4) obj;
                    int a2 = gk4.a(runnable);
                    if (a2 == 0) {
                        return true;
                    }
                    if (a2 == 1) {
                        h.compareAndSet(this, obj, gk4.e());
                    } else if (a2 == 2) {
                        return false;
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
            } else if (obj == ii4.b) {
                return false;
            } else {
                gk4 gk42 = new gk4(8, true);
                if (obj != null) {
                    gk42.a((Runnable) obj);
                    gk42.a(runnable);
                    if (h.compareAndSet(this, obj, gk42)) {
                        return true;
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
            }
        }
    }

    @DexIgnore
    public final boolean a(c cVar) {
        d dVar = (d) this._delayed;
        return (dVar != null ? (c) dVar.d() : null) == cVar;
    }
}
