package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import okhttp3.TlsVersion;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jm4 {
    @DexIgnore
    public /* final */ TlsVersion a;
    @DexIgnore
    public /* final */ yl4 b;
    @DexIgnore
    public /* final */ List<Certificate> c;
    @DexIgnore
    public /* final */ List<Certificate> d;

    @DexIgnore
    public jm4(TlsVersion tlsVersion, yl4 yl4, List<Certificate> list, List<Certificate> list2) {
        this.a = tlsVersion;
        this.b = yl4;
        this.c = list;
        this.d = list2;
    }

    @DexIgnore
    public static jm4 a(SSLSession sSLSession) throws IOException {
        Certificate[] certificateArr;
        List list;
        List list2;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite == null) {
            throw new IllegalStateException("cipherSuite == null");
        } else if (!"SSL_NULL_WITH_NULL_NULL".equals(cipherSuite)) {
            yl4 a2 = yl4.a(cipherSuite);
            String protocol = sSLSession.getProtocol();
            if (protocol == null) {
                throw new IllegalStateException("tlsVersion == null");
            } else if (!"NONE".equals(protocol)) {
                TlsVersion forJavaName = TlsVersion.forJavaName(protocol);
                try {
                    certificateArr = sSLSession.getPeerCertificates();
                } catch (SSLPeerUnverifiedException unused) {
                    certificateArr = null;
                }
                if (certificateArr != null) {
                    list = vm4.a((T[]) certificateArr);
                } else {
                    list = Collections.emptyList();
                }
                Certificate[] localCertificates = sSLSession.getLocalCertificates();
                if (localCertificates != null) {
                    list2 = vm4.a((T[]) localCertificates);
                } else {
                    list2 = Collections.emptyList();
                }
                return new jm4(forJavaName, a2, list, list2);
            } else {
                throw new IOException("tlsVersion == NONE");
            }
        } else {
            throw new IOException("cipherSuite == SSL_NULL_WITH_NULL_NULL");
        }
    }

    @DexIgnore
    public List<Certificate> b() {
        return this.d;
    }

    @DexIgnore
    public List<Certificate> c() {
        return this.c;
    }

    @DexIgnore
    public TlsVersion d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof jm4)) {
            return false;
        }
        jm4 jm4 = (jm4) obj;
        if (!this.a.equals(jm4.a) || !this.b.equals(jm4.b) || !this.c.equals(jm4.c) || !this.d.equals(jm4.d)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((527 + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    public static jm4 a(TlsVersion tlsVersion, yl4 yl4, List<Certificate> list, List<Certificate> list2) {
        if (tlsVersion == null) {
            throw new NullPointerException("tlsVersion == null");
        } else if (yl4 != null) {
            return new jm4(tlsVersion, yl4, vm4.a(list), vm4.a(list2));
        } else {
            throw new NullPointerException("cipherSuite == null");
        }
    }

    @DexIgnore
    public yl4 a() {
        return this.b;
    }
}
