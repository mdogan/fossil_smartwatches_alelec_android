package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o52 implements Factory<k62> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public o52(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static o52 a(o42 o42) {
        return new o52(o42);
    }

    @DexIgnore
    public static k62 b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static k62 c(o42 o42) {
        k62 m = o42.m();
        o44.a(m, "Cannot return null from a non-@Nullable @Provides method");
        return m;
    }

    @DexIgnore
    public k62 get() {
        return b(this.a);
    }
}
