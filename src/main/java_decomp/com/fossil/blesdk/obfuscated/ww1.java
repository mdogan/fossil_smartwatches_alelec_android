package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ww1<T> implements fz1<T> {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Object a; // = c;
    @DexIgnore
    public volatile fz1<T> b;

    @DexIgnore
    public ww1(lw1<T> lw1, kw1 kw1) {
        this.b = xw1.a(lw1, kw1);
    }

    @DexIgnore
    public final T get() {
        T t = this.a;
        if (t == c) {
            synchronized (this) {
                t = this.a;
                if (t == c) {
                    t = this.b.get();
                    this.a = t;
                    this.b = null;
                }
            }
        }
        return t;
    }
}
