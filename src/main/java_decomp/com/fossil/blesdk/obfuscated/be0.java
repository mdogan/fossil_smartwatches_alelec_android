package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class be0 {
    @DexIgnore
    public static be0 b;
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public be0(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public static be0 a(Context context) {
        ck0.a(context);
        synchronized (be0.class) {
            if (b == null) {
                fn0.a(context);
                b = new be0(context);
            }
        }
        return b;
    }

    @DexIgnore
    public boolean a(int i) {
        pn0 pn0;
        String[] a2 = cn0.b(this.a).a(i);
        if (a2 != null && a2.length != 0) {
            pn0 = null;
            for (String a3 : a2) {
                pn0 = a(a3, i);
                if (pn0.a) {
                    break;
                }
            }
        } else {
            pn0 = pn0.a("no pkgs");
        }
        pn0.b();
        return pn0.a;
    }

    @DexIgnore
    public static boolean a(PackageInfo packageInfo, boolean z) {
        hn0 hn0;
        if (!(packageInfo == null || packageInfo.signatures == null)) {
            if (z) {
                hn0 = a(packageInfo, kn0.a);
            } else {
                hn0 = a(packageInfo, kn0.a[0]);
            }
            if (hn0 != null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (a(packageInfo, false)) {
            return true;
        }
        if (a(packageInfo, true)) {
            if (ae0.honorsDebugCertificates(this.a)) {
                return true;
            }
            Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
        }
        return false;
    }

    @DexIgnore
    public final pn0 a(String str, int i) {
        try {
            PackageInfo a2 = cn0.b(this.a).a(str, 64, i);
            boolean honorsDebugCertificates = ae0.honorsDebugCertificates(this.a);
            if (a2 == null) {
                return pn0.a("null pkg");
            }
            if (a2.signatures.length != 1) {
                return pn0.a("single cert required");
            }
            in0 in0 = new in0(a2.signatures[0].toByteArray());
            String str2 = a2.packageName;
            pn0 a3 = fn0.a(str2, in0, honorsDebugCertificates, false);
            return (!a3.a || a2.applicationInfo == null || (a2.applicationInfo.flags & 2) == 0 || !fn0.a(str2, in0, false, true).a) ? a3 : pn0.a("debuggable release cert app rejected");
        } catch (PackageManager.NameNotFoundException unused) {
            String valueOf = String.valueOf(str);
            return pn0.a(valueOf.length() != 0 ? "no pkg ".concat(valueOf) : new String("no pkg "));
        }
    }

    @DexIgnore
    public static hn0 a(PackageInfo packageInfo, hn0... hn0Arr) {
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr == null) {
            return null;
        }
        if (signatureArr.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        in0 in0 = new in0(signatureArr[0].toByteArray());
        for (int i = 0; i < hn0Arr.length; i++) {
            if (hn0Arr[i].equals(in0)) {
                return hn0Arr[i];
            }
        }
        return null;
    }
}
