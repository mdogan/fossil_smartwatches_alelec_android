package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dy {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ Boolean d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ String j;
    @DexIgnore
    public String k;

    @DexIgnore
    public dy(String str, String str2, String str3, Boolean bool, String str4, String str5, String str6, String str7, String str8, String str9) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = bool;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = str8;
        this.j = str9;
    }

    @DexIgnore
    public String toString() {
        if (this.k == null) {
            this.k = "appBundleId=" + this.a + ", executionId=" + this.b + ", installationId=" + this.c + ", limitAdTrackingEnabled=" + this.d + ", betaDeviceToken=" + this.e + ", buildId=" + this.f + ", osVersion=" + this.g + ", deviceModel=" + this.h + ", appVersionCode=" + this.i + ", appVersionName=" + this.j;
        }
        return this.k;
    }
}
