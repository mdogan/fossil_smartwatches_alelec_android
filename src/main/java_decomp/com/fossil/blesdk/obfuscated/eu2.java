package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eu2 implements Factory<zt2> {
    @DexIgnore
    public static zt2 a(bu2 bu2) {
        zt2 d = bu2.d();
        o44.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
