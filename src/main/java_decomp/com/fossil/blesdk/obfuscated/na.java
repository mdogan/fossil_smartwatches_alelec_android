package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class na<C, T, A> implements Cloneable {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<C, T, A> {
    }

    @DexIgnore
    public synchronized void a(T t, int i, A a2) {
        throw null;
    }
}
