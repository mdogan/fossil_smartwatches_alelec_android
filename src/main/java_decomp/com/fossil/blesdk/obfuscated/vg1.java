package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vg1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ int e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ Object g;
    @DexIgnore
    public /* final */ /* synthetic */ Object h;
    @DexIgnore
    public /* final */ /* synthetic */ Object i;
    @DexIgnore
    public /* final */ /* synthetic */ ug1 j;

    @DexIgnore
    public vg1(ug1 ug1, int i2, String str, Object obj, Object obj2, Object obj3) {
        this.j = ug1;
        this.e = i2;
        this.f = str;
        this.g = obj;
        this.h = obj2;
        this.i = obj3;
    }

    @DexIgnore
    public final void run() {
        gh1 t = this.j.a.t();
        if (!t.m()) {
            this.j.a(6, "Persisted config not initialized. Not logging error/warn");
            return;
        }
        if (this.j.c == 0) {
            if (this.j.l().m()) {
                ug1 ug1 = this.j;
                ug1.b();
                char unused = ug1.c = 'C';
            } else {
                ug1 ug12 = this.j;
                ug12.b();
                char unused2 = ug12.c = 'c';
            }
        }
        if (this.j.d < 0) {
            ug1 ug13 = this.j;
            long unused3 = ug13.d = ug13.l().n();
        }
        char charAt = "01VDIWEA?".charAt(this.e);
        char a = this.j.c;
        long b = this.j.d;
        String a2 = ug1.a(true, this.f, this.g, this.h, this.i);
        StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 24);
        sb.append("2");
        sb.append(charAt);
        sb.append(a);
        sb.append(b);
        sb.append(":");
        sb.append(a2);
        String sb2 = sb.toString();
        if (sb2.length() > 1024) {
            sb2 = this.f.substring(0, 1024);
        }
        t.d.a(sb2, 1);
    }
}
