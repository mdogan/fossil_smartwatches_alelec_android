package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.su1;
import com.google.common.collect.Multisets;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class au1<E> extends AbstractCollection<E> implements su1<E> {
    @DexIgnore
    public transient Set<E> e;
    @DexIgnore
    public transient Set<su1.a<E>> f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends Multisets.c<E> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public su1<E> a() {
            return au1.this;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends Multisets.d<E> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public su1<E> a() {
            return au1.this;
        }

        @DexIgnore
        public Iterator<su1.a<E>> iterator() {
            return au1.this.entryIterator();
        }

        @DexIgnore
        public int size() {
            return au1.this.distinctElements();
        }
    }

    @DexIgnore
    public boolean add(E e2) {
        add(e2, 1);
        return true;
    }

    @DexIgnore
    public boolean addAll(Collection<? extends E> collection) {
        return Multisets.a(this, collection);
    }

    @DexIgnore
    public abstract void clear();

    @DexIgnore
    public boolean contains(Object obj) {
        return count(obj) > 0;
    }

    @DexIgnore
    public abstract int count(Object obj);

    @DexIgnore
    public Set<E> createElementSet() {
        return new a();
    }

    @DexIgnore
    public Set<su1.a<E>> createEntrySet() {
        return new b();
    }

    @DexIgnore
    public abstract int distinctElements();

    @DexIgnore
    public Set<E> elementSet() {
        Set<E> set = this.e;
        if (set != null) {
            return set;
        }
        Set<E> createElementSet = createElementSet();
        this.e = createElementSet;
        return createElementSet;
    }

    @DexIgnore
    public abstract Iterator<su1.a<E>> entryIterator();

    @DexIgnore
    public Set<su1.a<E>> entrySet() {
        Set<su1.a<E>> set = this.f;
        if (set != null) {
            return set;
        }
        Set<su1.a<E>> createEntrySet = createEntrySet();
        this.f = createEntrySet;
        return createEntrySet;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return Multisets.a((su1<?>) this, obj);
    }

    @DexIgnore
    public int hashCode() {
        return entrySet().hashCode();
    }

    @DexIgnore
    public boolean isEmpty() {
        return entrySet().isEmpty();
    }

    @DexIgnore
    public abstract Iterator<E> iterator();

    @DexIgnore
    public abstract int remove(Object obj, int i);

    @DexIgnore
    public boolean remove(Object obj) {
        return remove(obj, 1) > 0;
    }

    @DexIgnore
    public boolean removeAll(Collection<?> collection) {
        return Multisets.b(this, collection);
    }

    @DexIgnore
    public boolean retainAll(Collection<?> collection) {
        return Multisets.c(this, collection);
    }

    @DexIgnore
    public int setCount(E e2, int i) {
        return Multisets.a(this, e2, i);
    }

    @DexIgnore
    public int size() {
        return Multisets.a((su1<?>) this);
    }

    @DexIgnore
    public String toString() {
        return entrySet().toString();
    }

    @DexIgnore
    public int add(E e2, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean setCount(E e2, int i, int i2) {
        return Multisets.a(this, e2, i, i2);
    }
}
