package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g11 extends oz0 implements f11 {
    @DexIgnore
    public g11(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitSessionsApi");
    }

    @DexIgnore
    public final void a(bq0 bq0) throws RemoteException {
        Parcel o = o();
        z01.a(o, (Parcelable) bq0);
        a(3, o);
    }
}
