package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface uj0 extends IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends az0 implements uj0 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uj0$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.uj0$a$a  reason: collision with other inner class name */
        public static class C0035a extends zy0 implements uj0 {
            @DexIgnore
            public C0035a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.IAccountAccessor");
            }

            @DexIgnore
            public final Account h() throws RemoteException {
                Parcel a = a(2, o());
                Account account = (Account) bz0.a(a, Account.CREATOR);
                a.recycle();
                return account;
            }
        }

        @DexIgnore
        public static uj0 a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
            if (queryLocalInterface instanceof uj0) {
                return (uj0) queryLocalInterface;
            }
            return new C0035a(iBinder);
        }
    }

    @DexIgnore
    Account h() throws RemoteException;
}
