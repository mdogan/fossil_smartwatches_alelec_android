package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qs2;
import com.fossil.blesdk.obfuscated.ss2;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k23 extends as2 implements j23, xs3.g {
    @DexIgnore
    public ur3<na2> j;
    @DexIgnore
    public i23 k;
    @DexIgnore
    public qs2 l;
    @DexIgnore
    public ss2 m;
    @DexIgnore
    public k42 n;
    @DexIgnore
    public DianaCustomizeViewModel o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ss2.c {
        @DexIgnore
        public /* final */ /* synthetic */ k23 a;

        @DexIgnore
        public b(k23 k23) {
            this.a = k23;
        }

        @DexIgnore
        public void a(Complication complication) {
            wd4.b(complication, "complication");
            k23.a(this.a).b(complication.getComplicationId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements qs2.c {
        @DexIgnore
        public /* final */ /* synthetic */ k23 a;

        @DexIgnore
        public c(k23 k23) {
            this.a = k23;
        }

        @DexIgnore
        public void a() {
            k23.a(this.a).h();
        }

        @DexIgnore
        public void a(Category category) {
            wd4.b(category, "category");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ComplicationsFragment", "onItemClicked category=" + category);
            k23.a(this.a).a(category.getId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ k23 e;

        @DexIgnore
        public d(k23 k23) {
            this.e = k23;
        }

        @DexIgnore
        public final void onClick(View view) {
            k23.a(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ k23 e;

        @DexIgnore
        public e(k23 k23) {
            this.e = k23;
        }

        @DexIgnore
        public final void onClick(View view) {
            k23.a(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ na2 e;
        @DexIgnore
        public /* final */ /* synthetic */ k23 f;

        @DexIgnore
        public f(na2 na2, k23 k23) {
            this.e = na2;
            this.f = k23;
        }

        @DexIgnore
        public final void onClick(View view) {
            i23 a = k23.a(this.f);
            Switch switchR = this.e.u;
            wd4.a((Object) switchR, "binding.switchRing");
            a.a(switchR.isChecked());
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ i23 a(k23 k23) {
        i23 i23 = k23.k;
        if (i23 != null) {
            return i23;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void A(String str) {
        wd4.b(str, "permission");
        if (isActive()) {
            int hashCode = str.hashCode();
            if (hashCode != 385352715) {
                if (hashCode != 564039755) {
                    if (hashCode == 766697727 && str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        es3 es3 = es3.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        wd4.a((Object) childFragmentManager, "childFragmentManager");
                        es3.B(childFragmentManager);
                    }
                } else if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                    es3 es32 = es3.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    wd4.a((Object) childFragmentManager2, "childFragmentManager");
                    es32.A(childFragmentManager2);
                }
            } else if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                es3 es33 = es3.c;
                FragmentManager childFragmentManager3 = getChildFragmentManager();
                wd4.a((Object) childFragmentManager3, "childFragmentManager");
                es33.u(childFragmentManager3);
            }
        }
    }

    @DexIgnore
    public void C(String str) {
        wd4.b(str, "content");
        ur3<na2> ur3 = this.j;
        if (ur3 != null) {
            na2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                wd4.a((Object) flexibleTextView, "it.tvComplicationDetail");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ComplicationsFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final void T0() {
        if (isActive()) {
            ss2 ss2 = this.m;
            if (ss2 != null) {
                ss2.b();
            } else {
                wd4.d("mComplicationAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void b(Complication complication) {
        if (complication != null) {
            ss2 ss2 = this.m;
            if (ss2 != null) {
                ss2.b(complication.getComplicationId());
                c(complication);
                return;
            }
            wd4.d("mComplicationAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void c(Complication complication) {
        ur3<na2> ur3 = this.j;
        if (ur3 != null) {
            na2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.z;
                wd4.a((Object) flexibleTextView, "binding.tvSelectedComplication");
                flexibleTextView.setText(tm2.a(PortfolioApp.W.c(), complication.getNameKey(), complication.getName()));
                FlexibleTextView flexibleTextView2 = a2.v;
                wd4.a((Object) flexibleTextView2, "binding.tvComplicationDetail");
                flexibleTextView2.setText(tm2.a(PortfolioApp.W.c(), complication.getDescriptionKey(), complication.getDescription()));
                ss2 ss2 = this.m;
                if (ss2 != null) {
                    int a3 = ss2.a(complication.getComplicationId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("ComplicationsFragment", "updateDetailComplication complicationId=" + complication.getComplicationId() + " scrollTo " + a3);
                    if (a3 >= 0) {
                        a2.t.i(a3);
                        return;
                    }
                    return;
                }
                wd4.d("mComplicationAdapter");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void e(String str) {
        wd4.b(str, "category");
        ur3<na2> ur3 = this.j;
        if (ur3 != null) {
            na2 a2 = ur3.a();
            if (a2 != null) {
                qs2 qs2 = this.l;
                if (qs2 != null) {
                    int a3 = qs2.a(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("ComplicationsFragment", "scrollToCategory category=" + str + " scrollTo " + a3);
                    if (a3 >= 0) {
                        qs2 qs22 = this.l;
                        if (qs22 != null) {
                            qs22.a(a3);
                            a2.s.j(a3);
                            return;
                        }
                        wd4.d("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                wd4.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(String str) {
        wd4.b(str, "permission");
        if (isActive()) {
            int hashCode = str.hashCode();
            if (hashCode != 385352715) {
                if (hashCode != 564039755) {
                    if (hashCode == 766697727 && str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        os3.a.a((Fragment) this, 100, "android.permission.ACCESS_FINE_LOCATION");
                    }
                } else if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                    os3.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                }
            } else if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                os3.a aVar = os3.a;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    wd4.a((Object) activity, "activity!!");
                    aVar.a(activity);
                    return;
                }
                wd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void g(String str) {
        wd4.b(str, MicroAppSetting.SETTING);
        SearchSecondTimezoneActivity.C.a(this, str);
    }

    @DexIgnore
    public void m(List<Complication> list) {
        wd4.b(list, "complications");
        ss2 ss2 = this.m;
        if (ss2 != null) {
            ss2.a(list);
        } else {
            wd4.d("mComplicationAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = (DianaCustomizeEditActivity) activity;
            k42 k42 = this.n;
            if (k42 != null) {
                jc a2 = mc.a((FragmentActivity) dianaCustomizeEditActivity, (lc.b) k42).a(DianaCustomizeViewModel.class);
                wd4.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.o = (DianaCustomizeViewModel) a2;
                i23 i23 = this.k;
                if (i23 != null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.o;
                    if (dianaCustomizeViewModel != null) {
                        i23.a(dianaCustomizeViewModel);
                    } else {
                        wd4.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            } else {
                wd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationsFragment", "onActivityResult requestCode " + i);
        if (i != 100) {
            if (i != 102) {
                if (i == 106 && i2 == -1 && intent != null) {
                    CommuteTimeSetting commuteTimeSetting = (CommuteTimeSetting) intent.getParcelableExtra("COMMUTE_TIME_SETTING");
                    if (commuteTimeSetting != null) {
                        i23 i23 = this.k;
                        if (i23 != null) {
                            i23.a("commute-time", commuteTimeSetting);
                        } else {
                            wd4.d("mPresenter");
                            throw null;
                        }
                    }
                }
            } else if (intent != null) {
                String stringExtra = intent.getStringExtra("SEARCH_COMPLICATION_RESULT_ID");
                if (!TextUtils.isEmpty(stringExtra)) {
                    i23 i232 = this.k;
                    if (i232 != null) {
                        wd4.a((Object) stringExtra, "selectedComplicationId");
                        i232.b(stringExtra);
                        return;
                    }
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        } else if (i2 == -1 && intent != null) {
            SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) intent.getParcelableExtra("SECOND_TIMEZONE");
            if (secondTimezoneSetting != null) {
                i23 i233 = this.k;
                if (i233 != null) {
                    i233.a("second-timezone", secondTimezoneSetting);
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        na2 na2 = (na2) ra.a(layoutInflater, R.layout.fragment_complications, viewGroup, false, O0());
        PortfolioApp.W.c().g().a(new m23(this)).a(this);
        this.j = new ur3<>(this, na2);
        wd4.a((Object) na2, "binding");
        return na2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        i23 i23 = this.k;
        if (i23 != null) {
            i23.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        i23 i23 = this.k;
        if (i23 != null) {
            i23.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        ss2 ss2 = new ss2((ArrayList) null, (ss2.c) null, 3, (rd4) null);
        ss2.a((ss2.c) new b(this));
        this.m = ss2;
        qs2 qs2 = new qs2((ArrayList) null, (qs2.c) null, 3, (rd4) null);
        qs2.a((qs2.c) new c(this));
        this.l = qs2;
        ur3<na2> ur3 = this.j;
        if (ur3 != null) {
            na2 a2 = ur3.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.s;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                qs2 qs22 = this.l;
                if (qs22 != null) {
                    recyclerView.setAdapter(qs22);
                    RecyclerView recyclerView2 = a2.t;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    ss2 ss22 = this.m;
                    if (ss22 != null) {
                        recyclerView2.setAdapter(ss22);
                        a2.w.setOnClickListener(new d(this));
                        a2.x.setOnClickListener(new e(this));
                        a2.u.setOnClickListener(new f(a2, this));
                        return;
                    }
                    wd4.d("mComplicationAdapter");
                    throw null;
                }
                wd4.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(i23 i23) {
        wd4.b(i23, "presenter");
        this.k = i23;
    }

    @DexIgnore
    public void a(List<Category> list) {
        wd4.b(list, "categories");
        qs2 qs2 = this.l;
        if (qs2 != null) {
            qs2.a(list);
        } else {
            wd4.d("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void b(int i, List<String> list) {
        wd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationsFragment", "onPermissionsGranted:" + i + ':' + list.size());
    }

    @DexIgnore
    public void a(boolean z, String str, String str2, Parcelable parcelable) {
        wd4.b(str, "complicationId");
        wd4.b(str2, "emptySettingRequestContent");
        ur3<na2> ur3 = this.j;
        if (ur3 != null) {
            na2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.y;
                wd4.a((Object) flexibleTextView, "it.tvPermissionOrder");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.w;
                wd4.a((Object) flexibleTextView2, "it.tvComplicationPermission");
                flexibleTextView2.setVisibility(8);
                if (z) {
                    ConstraintLayout constraintLayout = a2.q;
                    wd4.a((Object) constraintLayout, "it.clComplicationSetting");
                    constraintLayout.setVisibility(0);
                    a2.x.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    FlexibleTextView flexibleTextView3 = a2.x;
                    wd4.a((Object) flexibleTextView3, "it.tvComplicationSetting");
                    FragmentActivity activity = getActivity();
                    Resources resources = activity != null ? activity.getResources() : null;
                    if (resources != null) {
                        flexibleTextView3.setCompoundDrawablePadding((int) resources.getDimension(R.dimen.dp5));
                        int hashCode = str.hashCode();
                        if (hashCode != -829740640) {
                            if (hashCode == 134170930 && str.equals("second-timezone")) {
                                Switch switchR = a2.u;
                                wd4.a((Object) switchR, "it.switchRing");
                                switchR.setVisibility(8);
                                ImageView imageView = a2.r;
                                wd4.a((Object) imageView, "it.ivComplicationSetting");
                                imageView.setVisibility(8);
                                a2.x.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_caret_right, 0);
                                if (parcelable != null) {
                                    FlexibleTextView flexibleTextView4 = a2.x;
                                    wd4.a((Object) flexibleTextView4, "it.tvComplicationSetting");
                                    flexibleTextView4.setText(((SecondTimezoneSetting) parcelable).getTimeZoneName());
                                    return;
                                }
                                FlexibleTextView flexibleTextView5 = a2.x;
                                wd4.a((Object) flexibleTextView5, "it.tvComplicationSetting");
                                flexibleTextView5.setText(str2);
                            }
                        } else if (str.equals("commute-time")) {
                            Switch switchR2 = a2.u;
                            wd4.a((Object) switchR2, "it.switchRing");
                            switchR2.setVisibility(8);
                            ImageView imageView2 = a2.r;
                            wd4.a((Object) imageView2, "it.ivComplicationSetting");
                            imageView2.setVisibility(8);
                            a2.x.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_caret_right, 0);
                            if (parcelable != null) {
                                FlexibleTextView flexibleTextView6 = a2.x;
                                wd4.a((Object) flexibleTextView6, "it.tvComplicationSetting");
                                flexibleTextView6.setText(((CommuteTimeSetting) parcelable).getAddress());
                                return;
                            }
                            FlexibleTextView flexibleTextView7 = a2.x;
                            wd4.a((Object) flexibleTextView7, "it.tvComplicationSetting");
                            flexibleTextView7.setText(str2);
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    ConstraintLayout constraintLayout2 = a2.q;
                    wd4.a((Object) constraintLayout2, "it.clComplicationSetting");
                    constraintLayout2.setVisibility(8);
                    Switch switchR3 = a2.u;
                    wd4.a((Object) switchR3, "it.switchRing");
                    switchR3.setVisibility(8);
                }
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void b(String str) {
        wd4.b(str, MicroAppSetting.SETTING);
        CommuteTimeSettingsActivity.C.a(this, str);
    }

    @DexIgnore
    public void a(int i, int i2, String str, String str2) {
        wd4.b(str, "title");
        wd4.b(str2, "content");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ComplicationsFragment", "showPermissionRequired current " + i + " total " + i2 + " title " + str + " content " + str2);
            ur3<na2> ur3 = this.j;
            if (ur3 != null) {
                na2 a2 = ur3.a();
                if (a2 != null) {
                    Switch switchR = a2.u;
                    wd4.a((Object) switchR, "it.switchRing");
                    switchR.setVisibility(8);
                    ConstraintLayout constraintLayout = a2.q;
                    wd4.a((Object) constraintLayout, "it.clComplicationSetting");
                    constraintLayout.setVisibility(8);
                    if (i2 == 0 || i == i2) {
                        FlexibleTextView flexibleTextView = a2.w;
                        wd4.a((Object) flexibleTextView, "it.tvComplicationPermission");
                        flexibleTextView.setVisibility(8);
                        FlexibleTextView flexibleTextView2 = a2.y;
                        wd4.a((Object) flexibleTextView2, "it.tvPermissionOrder");
                        flexibleTextView2.setVisibility(8);
                        return;
                    }
                    FlexibleTextView flexibleTextView3 = a2.w;
                    wd4.a((Object) flexibleTextView3, "it.tvComplicationPermission");
                    flexibleTextView3.setVisibility(0);
                    FlexibleTextView flexibleTextView4 = a2.w;
                    wd4.a((Object) flexibleTextView4, "it.tvComplicationPermission");
                    flexibleTextView4.setText(str);
                    if (str2.length() > 0) {
                        FlexibleTextView flexibleTextView5 = a2.v;
                        wd4.a((Object) flexibleTextView5, "it.tvComplicationDetail");
                        flexibleTextView5.setText(str2);
                    }
                    if (i2 > 1) {
                        FlexibleTextView flexibleTextView6 = a2.y;
                        wd4.a((Object) flexibleTextView6, "it.tvPermissionOrder");
                        flexibleTextView6.setVisibility(0);
                        FlexibleTextView flexibleTextView7 = a2.y;
                        wd4.a((Object) flexibleTextView7, "it.tvPermissionOrder");
                        be4 be4 = be4.a;
                        String a3 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.permission_of);
                        wd4.a((Object) a3, "LanguageHelper.getString\u2026  R.string.permission_of)");
                        Object[] objArr = {Integer.valueOf(i), Integer.valueOf(i2)};
                        String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                        wd4.a((Object) format, "java.lang.String.format(format, *args)");
                        flexibleTextView7.setText(format);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(int i, List<String> list) {
        wd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationsFragment", "onPermissionsDenied:" + i + ':' + list.size());
        if (br4.a((Fragment) this, list)) {
            for (String next : list) {
                int hashCode = next.hashCode();
                if (hashCode != -1888586689) {
                    if (hashCode == 2024715147 && next.equals("android.permission.ACCESS_BACKGROUND_LOCATION")) {
                        A(InAppPermission.ACCESS_BACKGROUND_LOCATION);
                    }
                } else if (next.equals("android.permission.ACCESS_FINE_LOCATION")) {
                    A(InAppPermission.ACCESS_FINE_LOCATION);
                }
            }
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        if (wd4.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i == R.id.tv_ok) {
                FragmentActivity activity = getActivity();
                if (activity == null) {
                    return;
                }
                if (activity != null) {
                    ((BaseActivity) activity).l();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (wd4.a((Object) str, (Object) es3.c.a()) && i == R.id.tv_ok) {
            FragmentActivity activity2 = getActivity();
            if (activity2 == null) {
                return;
            }
            if (!br4.a((Fragment) this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                os3.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
            } else if (activity2 != null) {
                ((BaseActivity) activity2).l();
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        }
    }

    @DexIgnore
    public void a(String str, String str2, String str3, String str4) {
        wd4.b(str, "topComplication");
        wd4.b(str2, "bottomComplication");
        wd4.b(str3, "rightComlication");
        wd4.b(str4, "leftComplication");
        ComplicationSearchActivity.C.a(this, str, str2, str4, str3);
    }
}
