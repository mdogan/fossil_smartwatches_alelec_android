package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.su0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hw0 implements rv0 {
    @DexIgnore
    public /* final */ tv0 a;
    @DexIgnore
    public /* final */ iw0 b;

    @DexIgnore
    public hw0(tv0 tv0, String str, Object[] objArr) {
        this.a = tv0;
        this.b = new iw0(tv0.getClass(), str, objArr);
    }

    @DexIgnore
    public final int a() {
        return (this.b.d & 1) == 1 ? su0.e.i : su0.e.j;
    }

    @DexIgnore
    public final boolean b() {
        return (this.b.d & 2) == 2;
    }

    @DexIgnore
    public final tv0 c() {
        return this.a;
    }

    @DexIgnore
    public final int d() {
        return this.b.e;
    }

    @DexIgnore
    public final iw0 e() {
        return this.b;
    }

    @DexIgnore
    public final int f() {
        return this.b.h;
    }

    @DexIgnore
    public final int g() {
        return this.b.i;
    }

    @DexIgnore
    public final int h() {
        return this.b.j;
    }

    @DexIgnore
    public final int i() {
        return this.b.m;
    }

    @DexIgnore
    public final int[] j() {
        return this.b.n;
    }

    @DexIgnore
    public final int k() {
        return this.b.l;
    }

    @DexIgnore
    public final int l() {
        return this.b.k;
    }
}
