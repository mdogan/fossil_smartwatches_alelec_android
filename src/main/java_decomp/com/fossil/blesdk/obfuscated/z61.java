package com.fossil.blesdk.obfuscated;

import android.database.ContentObserver;
import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z61 extends ContentObserver {
    @DexIgnore
    public z61(x61 x61, Handler handler) {
        super((Handler) null);
    }

    @DexIgnore
    public final void onChange(boolean z) {
        b71.f();
    }
}
