package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ne0;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class qe0<R extends ne0, S extends ne0> {
    @DexIgnore
    public abstract ie0<S> a(R r);

    @DexIgnore
    public abstract Status a(Status status);
}
