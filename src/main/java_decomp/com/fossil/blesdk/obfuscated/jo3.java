package com.fossil.blesdk.obfuscated;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.login.AppleAuthorizationActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jo3 extends as2 implements io3 {
    @DexIgnore
    public static /* final */ a m; // = new a((rd4) null);
    @DexIgnore
    public ho3 j;
    @DexIgnore
    public ur3<pf2> k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final jo3 a() {
            return new jo3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ jo3 f;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ pf2 e;

            @DexIgnore
            public a(pf2 pf2) {
                this.e = pf2;
            }

            @DexIgnore
            public final void run() {
                FlexibleTextView flexibleTextView = this.e.F;
                wd4.a((Object) flexibleTextView, "it.tvHaveAccount");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = this.e.G;
                wd4.a((Object) flexibleTextView2, "it.tvLogin");
                flexibleTextView2.setVisibility(0);
            }
        }

        @DexIgnore
        public b(View view, jo3 jo3) {
            this.e = view;
            this.f = jo3;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.e.getWindowVisibleDisplayFrame(rect);
            int height = this.e.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                pf2 pf2 = (pf2) jo3.a(this.f).a();
                if (pf2 != null) {
                    try {
                        FlexibleTextView flexibleTextView = pf2.F;
                        wd4.a((Object) flexibleTextView, "it.tvHaveAccount");
                        flexibleTextView.setVisibility(8);
                        FlexibleTextView flexibleTextView2 = pf2.G;
                        wd4.a((Object) flexibleTextView2, "it.tvLogin");
                        flexibleTextView2.setVisibility(8);
                        cb4 cb4 = cb4.a;
                    } catch (Exception e2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("SignUpFragment", "onCreateView - e=" + e2);
                        cb4 cb42 = cb4.a;
                    }
                }
            } else {
                pf2 pf22 = (pf2) jo3.a(this.f).a();
                if (pf22 != null) {
                    try {
                        wd4.a((Object) pf22, "it");
                        Boolean.valueOf(pf22.d().postDelayed(new a(pf22), 100));
                    } catch (Exception e3) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("SignUpFragment", "onCreateView - e=" + e3);
                        cb4 cb43 = cb4.a;
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo3 e;

        @DexIgnore
        public c(jo3 jo3) {
            this.e = jo3;
        }

        @DexIgnore
        public final void onClick(View view) {
            jo3.b(this.e).k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo3 e;

        @DexIgnore
        public d(jo3 jo3) {
            this.e = jo3;
        }

        @DexIgnore
        public final void onClick(View view) {
            jo3.b(this.e).l();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo3 e;

        @DexIgnore
        public e(jo3 jo3) {
            this.e = jo3;
        }

        @DexIgnore
        public final void onClick(View view) {
            LoginActivity.a aVar = LoginActivity.G;
            wd4.a((Object) view, "it");
            Context context = view.getContext();
            wd4.a((Object) context, "it.context");
            aVar.a(context);
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pf2 e;
        @DexIgnore
        public /* final */ /* synthetic */ jo3 f;

        @DexIgnore
        public f(pf2 pf2, jo3 jo3) {
            this.e = pf2;
            this.f = jo3;
        }

        @DexIgnore
        public final void onClick(View view) {
            TextInputLayout textInputLayout = this.e.u;
            wd4.a((Object) textInputLayout, "binding.inputPassword");
            textInputLayout.setErrorEnabled(false);
            TextInputLayout textInputLayout2 = this.e.t;
            wd4.a((Object) textInputLayout2, "binding.inputEmail");
            textInputLayout2.setErrorEnabled(false);
            this.f.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ jo3 e;

        @DexIgnore
        public g(jo3 jo3) {
            this.e = jo3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            jo3.b(this.e).a(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo3 e;

        @DexIgnore
        public h(jo3 jo3) {
            this.e = jo3;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            jo3.b(this.e).a(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ jo3 e;

        @DexIgnore
        public i(jo3 jo3) {
            this.e = jo3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            jo3.b(this.e).b(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements TextView.OnEditorActionListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo3 a;

        @DexIgnore
        public j(jo3 jo3) {
            this.a = jo3;
        }

        @DexIgnore
        public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((i & 255) != 6) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d("SignUpFragment", "Password DONE key, trigger sign up flow");
            this.a.T0();
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo3 e;

        @DexIgnore
        public k(jo3 jo3) {
            this.e = jo3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo3 e;

        @DexIgnore
        public l(jo3 jo3) {
            this.e = jo3;
        }

        @DexIgnore
        public final void onClick(View view) {
            jo3.b(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo3 e;

        @DexIgnore
        public m(jo3 jo3) {
            this.e = jo3;
        }

        @DexIgnore
        public final void onClick(View view) {
            jo3.b(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo3 e;

        @DexIgnore
        public n(jo3 jo3) {
            this.e = jo3;
        }

        @DexIgnore
        public final void onClick(View view) {
            jo3.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ pf2 e;

        @DexIgnore
        public o(pf2 pf2) {
            this.e = pf2;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                FlexibleTextView flexibleTextView = this.e.D;
                wd4.a((Object) flexibleTextView, "binding.tvErrorCheckCharacter");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = this.e.E;
                wd4.a((Object) flexibleTextView2, "binding.tvErrorCheckCombine");
                flexibleTextView2.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView3 = this.e.D;
            wd4.a((Object) flexibleTextView3, "binding.tvErrorCheckCharacter");
            flexibleTextView3.setVisibility(8);
            FlexibleTextView flexibleTextView4 = this.e.E;
            wd4.a((Object) flexibleTextView4, "binding.tvErrorCheckCombine");
            flexibleTextView4.setVisibility(8);
        }
    }

    @DexIgnore
    public static final /* synthetic */ ur3 a(jo3 jo3) {
        ur3<pf2> ur3 = jo3.k;
        if (ur3 != null) {
            return ur3;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ho3 b(jo3 jo3) {
        ho3 ho3 = jo3.j;
        if (ho3 != null) {
            return ho3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void B0() {
        if (isActive()) {
            ur3<pf2> ur3 = this.k;
            if (ur3 != null) {
                pf2 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.q;
                    wd4.a((Object) flexibleButton, "it.btContinue");
                    flexibleButton.setEnabled(false);
                    FlexibleButton flexibleButton2 = a2.q;
                    wd4.a((Object) flexibleButton2, "it.btContinue");
                    flexibleButton2.setClickable(false);
                    FlexibleButton flexibleButton3 = a2.q;
                    wd4.a((Object) flexibleButton3, "it.btContinue");
                    flexibleButton3.setFocusable(false);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void F() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            AppleAuthorizationActivity.a aVar = AppleAuthorizationActivity.F;
            wd4.a((Object) activity, "it");
            aVar.a(activity, "file:///android_asset/signinwithapple.html");
        }
    }

    @DexIgnore
    public void N(String str) {
        wd4.b(str, "errorMessage");
        ur3<pf2> ur3 = this.k;
        if (ur3 != null) {
            pf2 a2 = ur3.a();
            if (a2 != null) {
                TextInputLayout textInputLayout = a2.t;
                wd4.a((Object) textInputLayout, "it.inputEmail");
                textInputLayout.setErrorEnabled(true);
                TextInputLayout textInputLayout2 = a2.t;
                wd4.a((Object) textInputLayout2, "it.inputEmail");
                textInputLayout2.setError(str);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "SignUpFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final void T0() {
        ur3<pf2> ur3 = this.k;
        if (ur3 != null) {
            pf2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.q;
                wd4.a((Object) flexibleButton, "this.btContinue");
                if (flexibleButton.isEnabled()) {
                    ho3 ho3 = this.j;
                    if (ho3 != null) {
                        TextInputEditText textInputEditText = a2.r;
                        wd4.a((Object) textInputEditText, "etEmail");
                        String valueOf = String.valueOf(textInputEditText.getText());
                        TextInputEditText textInputEditText2 = a2.s;
                        wd4.a((Object) textInputEditText2, "etPassword");
                        ho3.a(valueOf, String.valueOf(textInputEditText2.getText()));
                        return;
                    }
                    wd4.d("mPresenter");
                    throw null;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(int i2, String str) {
        wd4.b(str, "errorMessage");
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(childFragmentManager, i2, str);
        }
    }

    @DexIgnore
    public void g() {
        ur3<pf2> ur3 = this.k;
        if (ur3 != null) {
            pf2 a2 = ur3.a();
            if (a2 != null) {
                ObjectAnimator ofInt = ObjectAnimator.ofInt(a2.C, "progress", new int[]{0, 10});
                wd4.a((Object) ofInt, "progressAnimator");
                ofInt.setDuration(500);
                ofInt.start();
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void h() {
        if (getActivity() != null) {
            HomeActivity.a aVar = HomeActivity.C;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wd4.a((Object) activity, "activity!!");
                aVar.a(activity);
                return;
            }
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        a();
    }

    @DexIgnore
    public void k() {
        String string = getString(R.string.Onboarding_SignUp_CreatingAccount_Text__PleaseWait);
        wd4.a((Object) string, "getString(R.string.Onboa\u2026Account_Text__PleaseWait)");
        S(string);
    }

    @DexIgnore
    public void k0() {
        if (isActive()) {
            ur3<pf2> ur3 = this.k;
            if (ur3 != null) {
                pf2 a2 = ur3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.q;
                    wd4.a((Object) flexibleButton, "it.btContinue");
                    flexibleButton.setEnabled(true);
                    FlexibleButton flexibleButton2 = a2.q;
                    wd4.a((Object) flexibleButton2, "it.btContinue");
                    flexibleButton2.setClickable(true);
                    FlexibleButton flexibleButton3 = a2.q;
                    wd4.a((Object) flexibleButton3, "it.btContinue");
                    flexibleButton3.setFocusable(true);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 3535) {
            return;
        }
        if (i3 != -1) {
            FLogger.INSTANCE.getLocal().e("SignUpFragment", "Something went wrong with Apple login");
        } else if (intent != null) {
            SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) intent.getParcelableExtra("USER_INFO_EXTRA");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SignUpFragment", "Apple Auth Info = " + signUpSocialAuth);
            ho3 ho3 = this.j;
            if (ho3 != null) {
                wd4.a((Object) signUpSocialAuth, "authCode");
                ho3.a(signUpSocialAuth);
                return;
            }
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        pf2 pf2 = (pf2) ra.a(layoutInflater, R.layout.fragment_signup, viewGroup, false, O0());
        wd4.a((Object) pf2, "bindingLocal");
        View d2 = pf2.d();
        wd4.a((Object) d2, "bindingLocal.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, this));
        this.k = new ur3<>(this, pf2);
        ur3<pf2> ur3 = this.k;
        if (ur3 != null) {
            pf2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        ho3 ho3 = this.j;
        if (ho3 != null) {
            ho3.g();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ho3 ho3 = this.j;
        if (ho3 != null) {
            ho3.f();
            wl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<pf2> ur3 = this.k;
        if (ur3 != null) {
            pf2 a2 = ur3.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new f(a2, this));
                a2.r.addTextChangedListener(new g(this));
                a2.r.setOnFocusChangeListener(new h(this));
                a2.s.addTextChangedListener(new i(this));
                a2.s.setOnFocusChangeListener(new o(a2));
                a2.s.setOnEditorActionListener(new j(this));
                a2.x.setOnClickListener(new k(this));
                a2.y.setOnClickListener(new l(this));
                a2.z.setOnClickListener(new m(this));
                a2.v.setOnClickListener(new n(this));
                a2.A.setOnClickListener(new c(this));
                a2.B.setOnClickListener(new d(this));
                a2.G.setOnClickListener(new e(this));
            }
            R("sign_up_view");
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void z(boolean z) {
        if (isActive()) {
            ur3<pf2> ur3 = this.k;
            if (ur3 != null) {
                pf2 a2 = ur3.a();
                if (a2 == null) {
                    return;
                }
                if (z) {
                    FloatingActionButton floatingActionButton = a2.B;
                    wd4.a((Object) floatingActionButton, "it.ivWeibo");
                    floatingActionButton.setVisibility(0);
                    FloatingActionButton floatingActionButton2 = a2.A;
                    wd4.a((Object) floatingActionButton2, "it.ivWechat");
                    floatingActionButton2.setVisibility(0);
                    ImageView imageView = a2.z;
                    wd4.a((Object) imageView, "it.ivGoogle");
                    imageView.setVisibility(8);
                    ImageView imageView2 = a2.y;
                    wd4.a((Object) imageView2, "it.ivFacebook");
                    imageView2.setVisibility(8);
                    return;
                }
                FloatingActionButton floatingActionButton3 = a2.B;
                wd4.a((Object) floatingActionButton3, "it.ivWeibo");
                floatingActionButton3.setVisibility(8);
                FloatingActionButton floatingActionButton4 = a2.A;
                wd4.a((Object) floatingActionButton4, "it.ivWechat");
                floatingActionButton4.setVisibility(8);
                ImageView imageView3 = a2.z;
                wd4.a((Object) imageView3, "it.ivGoogle");
                imageView3.setVisibility(0);
                ImageView imageView4 = a2.y;
                wd4.a((Object) imageView4, "it.ivFacebook");
                imageView4.setVisibility(0);
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void b(int i2, String str) {
        wd4.b(str, "errorMessage");
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void c(boolean z, boolean z2) {
        if (isActive()) {
            ur3<pf2> ur3 = this.k;
            if (ur3 != null) {
                pf2 a2 = ur3.a();
                if (a2 != null) {
                    if (z) {
                        a2.D.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.ic_ok_button), (Drawable) null, (Drawable) null, (Drawable) null);
                    } else {
                        a2.D.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.gray_dot), (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                    if (z2) {
                        a2.E.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.ic_ok_button), (Drawable) null, (Drawable) null, (Drawable) null);
                    } else {
                        a2.E.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.gray_dot), (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
            } else {
                wd4.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(ho3 ho3) {
        wd4.b(ho3, "presenter");
        this.j = ho3;
    }

    @DexIgnore
    public void a(boolean z, boolean z2, String str) {
        wd4.b(str, "errorMessage");
        if (isActive()) {
            ur3<pf2> ur3 = this.k;
            if (ur3 != null) {
                pf2 a2 = ur3.a();
                if (a2 != null) {
                    TextInputLayout textInputLayout = a2.t;
                    wd4.a((Object) textInputLayout, "it.inputEmail");
                    textInputLayout.setErrorEnabled(z2);
                    TextInputLayout textInputLayout2 = a2.t;
                    wd4.a((Object) textInputLayout2, "it.inputEmail");
                    textInputLayout2.setError(str);
                }
                ur3<pf2> ur32 = this.k;
                if (ur32 != null) {
                    pf2 a3 = ur32.a();
                    if (a3 != null) {
                        ImageView imageView = a3.w;
                        if (imageView != null) {
                            wd4.a((Object) imageView, "it");
                            imageView.setVisibility(z ? 0 : 8);
                            return;
                        }
                        return;
                    }
                    return;
                }
                wd4.d("mBinding");
                throw null;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void b(SignUpEmailAuth signUpEmailAuth) {
        wd4.b(signUpEmailAuth, "emailAuth");
        if (isActive()) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                EmailOtpVerificationActivity.a aVar = EmailOtpVerificationActivity.C;
                wd4.a((Object) activity, "it");
                aVar.a(activity, signUpEmailAuth);
            }
        }
    }

    @DexIgnore
    public void b(SignUpSocialAuth signUpSocialAuth) {
        wd4.b(signUpSocialAuth, "socialAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.C;
            wd4.a((Object) activity, "it");
            aVar.a((Context) activity, signUpSocialAuth);
            activity.finish();
        }
    }
}
