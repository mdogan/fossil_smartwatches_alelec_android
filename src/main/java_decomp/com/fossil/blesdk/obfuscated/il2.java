package com.fossil.blesdk.obfuscated;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.widget.RemoteViews;
import com.fossil.blesdk.obfuscated.d6;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class il2 {
    @DexIgnore
    public static /* final */ a a; // = new a((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, int i, String str, String str2, PendingIntent pendingIntent, List<? extends d6.a> list) {
            wd4.b(context, "context");
            wd4.b(str, "title");
            wd4.b(str2, "text");
            wd4.b(pendingIntent, "pendingIntent");
            d6.b bVar = new d6.b();
            bVar.a((CharSequence) str2);
            Uri defaultUri = RingtoneManager.getDefaultUri(2);
            BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
            d6.c cVar = new d6.c(context);
            cVar.b((CharSequence) str);
            cVar.a((CharSequence) str2);
            cVar.c((int) R.mipmap.ic_launcher);
            cVar.a((d6.d) bVar);
            cVar.a(new long[]{1000, 1000, 1000});
            cVar.a(defaultUri);
            cVar.a(pendingIntent);
            Notification a = cVar.a();
            a.flags |= 16;
            if (Build.VERSION.SDK_INT >= 21) {
                Resources resources = PortfolioApp.W.c().getResources();
                Package packageR = android.R.class.getPackage();
                if (packageR != null) {
                    wd4.a((Object) packageR, "android.R::class.java.`package`!!");
                    int identifier = resources.getIdentifier("right_icon", "id", packageR.getName());
                    if (identifier != 0) {
                        RemoteViews remoteViews = a.contentView;
                        if (remoteViews != null) {
                            remoteViews.setViewVisibility(identifier, 4);
                        }
                        RemoteViews remoteViews2 = a.headsUpContentView;
                        if (remoteViews2 != null) {
                            remoteViews2.setViewVisibility(identifier, 4);
                        }
                        RemoteViews remoteViews3 = a.bigContentView;
                        if (remoteViews3 != null) {
                            remoteViews3.setViewVisibility(identifier, 4);
                        }
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            }
            if (list != null) {
                for (d6.a a2 : list) {
                    cVar.a(a2);
                }
            }
            Object systemService = context.getSystemService("notification");
            if (systemService != null) {
                ((NotificationManager) systemService).notify(i, a);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.app.NotificationManager");
        }

        @DexIgnore
        public final void b(Context context, int i, String str, String str2, PendingIntent pendingIntent, List<? extends d6.a> list) {
            wd4.b(context, "context");
            wd4.b(str, "title");
            wd4.b(str2, "text");
            wd4.b(pendingIntent, "pendingIntent");
            d6.b bVar = new d6.b();
            bVar.a((CharSequence) str2);
            Uri defaultUri = RingtoneManager.getDefaultUri(2);
            Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
            d6.c cVar = new d6.c(context);
            cVar.b((CharSequence) str);
            cVar.a((CharSequence) str2);
            cVar.c((int) R.mipmap.ic_launcher);
            cVar.b(decodeResource);
            cVar.a((d6.d) bVar);
            cVar.a(new long[]{1000, 1000, 1000});
            cVar.a(defaultUri);
            cVar.a(pendingIntent);
            Notification a = cVar.a();
            a.flags |= 16;
            if (Build.VERSION.SDK_INT >= 21) {
                Resources resources = PortfolioApp.W.c().getResources();
                Package packageR = android.R.class.getPackage();
                if (packageR != null) {
                    wd4.a((Object) packageR, "android.R::class.java.`package`!!");
                    int identifier = resources.getIdentifier("right_icon", "id", packageR.getName());
                    if (identifier != 0) {
                        RemoteViews remoteViews = a.contentView;
                        if (remoteViews != null) {
                            remoteViews.setViewVisibility(identifier, 4);
                        }
                        RemoteViews remoteViews2 = a.headsUpContentView;
                        if (remoteViews2 != null) {
                            remoteViews2.setViewVisibility(identifier, 4);
                        }
                        RemoteViews remoteViews3 = a.bigContentView;
                        if (remoteViews3 != null) {
                            remoteViews3.setViewVisibility(identifier, 4);
                        }
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            }
            if (list != null) {
                for (d6.a a2 : list) {
                    cVar.a(a2);
                }
            }
            Object systemService = context.getSystemService("notification");
            if (systemService != null) {
                ((NotificationManager) systemService).notify(i, a);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.app.NotificationManager");
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }
}
