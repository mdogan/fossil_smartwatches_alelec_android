package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sb3 implements Factory<GoalTrackingOverviewMonthPresenter> {
    @DexIgnore
    public static GoalTrackingOverviewMonthPresenter a(qb3 qb3, UserRepository userRepository, fn2 fn2, GoalTrackingRepository goalTrackingRepository) {
        return new GoalTrackingOverviewMonthPresenter(qb3, userRepository, fn2, goalTrackingRepository);
    }
}
