package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z13 implements Factory<DianaCustomizeEditPresenter> {
    @DexIgnore
    public static DianaCustomizeEditPresenter a(u13 u13, UserRepository userRepository, int i, SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase, xm2 xm2, fn2 fn2) {
        return new DianaCustomizeEditPresenter(u13, userRepository, i, setDianaPresetToWatchUseCase, xm2, fn2);
    }
}
