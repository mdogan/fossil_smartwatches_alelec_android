package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.sr4;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xr4 extends sr4.a {
    @DexIgnore
    public static /* final */ sr4.a a; // = new xr4();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements sr4<qm4, Optional<T>> {
        @DexIgnore
        public /* final */ sr4<qm4, T> a;

        @DexIgnore
        public a(sr4<qm4, T> sr4) {
            this.a = sr4;
        }

        @DexIgnore
        public Optional<T> a(qm4 qm4) throws IOException {
            return Optional.ofNullable(this.a.a(qm4));
        }
    }

    @DexIgnore
    public sr4<qm4, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (sr4.a.a(type) != Optional.class) {
            return null;
        }
        return new a(retrofit3.b(sr4.a.a(0, (ParameterizedType) type), annotationArr));
    }
}
