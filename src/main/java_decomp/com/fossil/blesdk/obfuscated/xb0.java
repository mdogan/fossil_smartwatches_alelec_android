package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface xb0 {
    @DexIgnore
    ac0 a(Intent intent);

    @DexIgnore
    ie0<Status> a(he0 he0);
}
