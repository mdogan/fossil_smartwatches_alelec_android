package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class vy3 {
    @DexIgnore
    public Context a; // = null;

    @DexIgnore
    public vy3(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void a(sy3 sy3) {
        if (sy3 != null) {
            String sy32 = sy3.toString();
            if (a()) {
                a(xy3.d(sy32));
            }
        }
    }

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public final sy3 c() {
        String c = a() ? xy3.c(b()) : null;
        if (c != null) {
            return sy3.a(c);
        }
        return null;
    }
}
