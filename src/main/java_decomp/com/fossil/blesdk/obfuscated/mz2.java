package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mz2 implements Factory<NotificationContactsAndAppsAssignedPresenter> {
    @DexIgnore
    public static NotificationContactsAndAppsAssignedPresenter a(LoaderManager loaderManager, hz2 hz2, int i, k62 k62, u03 u03, i03 i03, f13 f13, wq2 wq2) {
        return new NotificationContactsAndAppsAssignedPresenter(loaderManager, hz2, i, k62, u03, i03, f13, wq2);
    }
}
