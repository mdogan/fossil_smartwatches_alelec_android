package com.fossil.blesdk.obfuscated;

import io.reactivex.disposables.RunnableDisposable;
import io.reactivex.internal.disposables.EmptyDisposable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k94 {
    @DexIgnore
    public static j94 a(Runnable runnable) {
        v94.a(runnable, "run is null");
        return new RunnableDisposable(runnable);
    }

    @DexIgnore
    public static j94 a() {
        return EmptyDisposable.INSTANCE;
    }
}
