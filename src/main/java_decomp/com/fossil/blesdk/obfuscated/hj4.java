package com.fossil.blesdk.obfuscated;

import kotlin.Result;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hj4<T> extends wi4<xi4> {
    @DexIgnore
    public /* final */ qg4<T> i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hj4(xi4 xi4, qg4<? super T> qg4) {
        super(xi4);
        wd4.b(xi4, "job");
        wd4.b(qg4, "continuation");
        this.i = qg4;
    }

    @DexIgnore
    public void b(Throwable th) {
        Object d = ((xi4) this.h).d();
        if (oh4.a() && !(!(d instanceof mi4))) {
            throw new AssertionError();
        } else if (d instanceof zg4) {
            this.i.a(((zg4) d).a, 0);
        } else {
            qg4<T> qg4 = this.i;
            Object b = yi4.b(d);
            Result.a aVar = Result.Companion;
            qg4.resumeWith(Result.m3constructorimpl(b));
        }
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return cb4.a;
    }

    @DexIgnore
    public String toString() {
        return "ResumeAwaitOnCompletion[" + this.i + ']';
    }
}
