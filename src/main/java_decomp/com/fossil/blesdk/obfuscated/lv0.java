package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lv0 implements sv0 {
    @DexIgnore
    public final boolean zza(Class<?> cls) {
        return false;
    }

    @DexIgnore
    public final rv0 zzb(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }
}
