package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.RuntimeRemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class be1 {
    @DexIgnore
    public static je1 a;

    @DexIgnore
    public static je1 a() {
        je1 je1 = a;
        ck0.a(je1, (Object) "CameraUpdateFactory is not initialized");
        return je1;
    }

    @DexIgnore
    public static void a(je1 je1) {
        ck0.a(je1);
        a = je1;
    }

    @DexIgnore
    public static ae1 a(float f) {
        try {
            return new ae1(a().a(f));
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    @DexIgnore
    public static ae1 a(LatLng latLng, float f) {
        try {
            return new ae1(a().a(latLng, f));
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
