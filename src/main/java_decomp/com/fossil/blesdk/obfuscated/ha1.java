package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ha1 {
    @DexIgnore
    public static /* final */ fa1 a; // = c();
    @DexIgnore
    public static /* final */ fa1 b; // = new ga1();

    @DexIgnore
    public static fa1 a() {
        return a;
    }

    @DexIgnore
    public static fa1 b() {
        return b;
    }

    @DexIgnore
    public static fa1 c() {
        try {
            return (fa1) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
