package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e61 extends wb1<e61> {
    @DexIgnore
    public static volatile e61[] h;
    @DexIgnore
    public String c; // = null;
    @DexIgnore
    public String d; // = null;
    @DexIgnore
    public Long e; // = null;
    @DexIgnore
    public Float f; // = null;
    @DexIgnore
    public Double g; // = null;

    @DexIgnore
    public e61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static e61[] e() {
        if (h == null) {
            synchronized (ac1.b) {
                if (h == null) {
                    h = new e61[0];
                }
            }
        }
        return h;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        String str = this.c;
        if (str != null) {
            vb1.a(1, str);
        }
        String str2 = this.d;
        if (str2 != null) {
            vb1.a(2, str2);
        }
        Long l = this.e;
        if (l != null) {
            vb1.b(3, l.longValue());
        }
        Float f2 = this.f;
        if (f2 != null) {
            vb1.a(4, f2.floatValue());
        }
        Double d2 = this.g;
        if (d2 != null) {
            vb1.a(5, d2.doubleValue());
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof e61)) {
            return false;
        }
        e61 e61 = (e61) obj;
        String str = this.c;
        if (str == null) {
            if (e61.c != null) {
                return false;
            }
        } else if (!str.equals(e61.c)) {
            return false;
        }
        String str2 = this.d;
        if (str2 == null) {
            if (e61.d != null) {
                return false;
            }
        } else if (!str2.equals(e61.d)) {
            return false;
        }
        Long l = this.e;
        if (l == null) {
            if (e61.e != null) {
                return false;
            }
        } else if (!l.equals(e61.e)) {
            return false;
        }
        Float f2 = this.f;
        if (f2 == null) {
            if (e61.f != null) {
                return false;
            }
        } else if (!f2.equals(e61.f)) {
            return false;
        }
        Double d2 = this.g;
        if (d2 == null) {
            if (e61.g != null) {
                return false;
            }
        } else if (!d2.equals(e61.g)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(e61.b);
        }
        yb1 yb12 = e61.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (e61.class.getName().hashCode() + 527) * 31;
        String str = this.c;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.d;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        Long l = this.e;
        int hashCode4 = (hashCode3 + (l == null ? 0 : l.hashCode())) * 31;
        Float f2 = this.f;
        int hashCode5 = (hashCode4 + (f2 == null ? 0 : f2.hashCode())) * 31;
        Double d2 = this.g;
        int hashCode6 = (hashCode5 + (d2 == null ? 0 : d2.hashCode())) * 31;
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode6 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        String str = this.c;
        if (str != null) {
            a += vb1.b(1, str);
        }
        String str2 = this.d;
        if (str2 != null) {
            a += vb1.b(2, str2);
        }
        Long l = this.e;
        if (l != null) {
            a += vb1.c(3, l.longValue());
        }
        Float f2 = this.f;
        if (f2 != null) {
            f2.floatValue();
            a += vb1.c(4) + 4;
        }
        Double d2 = this.g;
        if (d2 == null) {
            return a;
        }
        d2.doubleValue();
        return a + vb1.c(5) + 8;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 10) {
                this.c = ub1.b();
            } else if (c2 == 18) {
                this.d = ub1.b();
            } else if (c2 == 24) {
                this.e = Long.valueOf(ub1.f());
            } else if (c2 == 37) {
                this.f = Float.valueOf(Float.intBitsToFloat(ub1.g()));
            } else if (c2 == 41) {
                this.g = Double.valueOf(Double.longBitsToDouble(ub1.h()));
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
