package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.gs4;
import com.fossil.blesdk.obfuscated.vl4;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.KotlinExtensions;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ur4<ResponseT, ReturnT> extends ds4<ReturnT> {
    @DexIgnore
    public /* final */ bs4 a;
    @DexIgnore
    public /* final */ vl4.a b;
    @DexIgnore
    public /* final */ sr4<qm4, ResponseT> c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<ResponseT, ReturnT> extends ur4<ResponseT, ReturnT> {
        @DexIgnore
        public /* final */ pr4<ResponseT, ReturnT> d;

        @DexIgnore
        public a(bs4 bs4, vl4.a aVar, sr4<qm4, ResponseT> sr4, pr4<ResponseT, ReturnT> pr4) {
            super(bs4, aVar, sr4);
            this.d = pr4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v0, types: [retrofit2.Call, retrofit2.Call<ResponseT>] */
        /* JADX WARNING: Unknown variable types count: 1 */
        public ReturnT a(Call<ResponseT> r1, Object[] objArr) {
            return this.d.a(r1);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<ResponseT> extends ur4<ResponseT, Object> {
        @DexIgnore
        public /* final */ pr4<ResponseT, Call<ResponseT>> d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public b(bs4 bs4, vl4.a aVar, sr4<qm4, ResponseT> sr4, pr4<ResponseT, Call<ResponseT>> pr4, boolean z) {
            super(bs4, aVar, sr4);
            this.d = pr4;
            this.e = z;
        }

        @DexIgnore
        public Object a(Call<ResponseT> call, Object[] objArr) {
            Call call2 = (Call) this.d.a(call);
            kc4 kc4 = objArr[objArr.length - 1];
            try {
                if (this.e) {
                    return KotlinExtensions.b(call2, kc4);
                }
                return KotlinExtensions.a(call2, kc4);
            } catch (Exception e2) {
                return KotlinExtensions.a(e2, (kc4<?>) kc4);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<ResponseT> extends ur4<ResponseT, Object> {
        @DexIgnore
        public /* final */ pr4<ResponseT, Call<ResponseT>> d;

        @DexIgnore
        public c(bs4 bs4, vl4.a aVar, sr4<qm4, ResponseT> sr4, pr4<ResponseT, Call<ResponseT>> pr4) {
            super(bs4, aVar, sr4);
            this.d = pr4;
        }

        @DexIgnore
        public Object a(Call<ResponseT> call, Object[] objArr) {
            Call call2 = (Call) this.d.a(call);
            kc4 kc4 = objArr[objArr.length - 1];
            try {
                return KotlinExtensions.c(call2, kc4);
            } catch (Exception e) {
                return KotlinExtensions.a(e, (kc4<?>) kc4);
            }
        }
    }

    @DexIgnore
    public ur4(bs4 bs4, vl4.a aVar, sr4<qm4, ResponseT> sr4) {
        this.a = bs4;
        this.b = aVar;
        this.c = sr4;
    }

    @DexIgnore
    public static <ResponseT, ReturnT> ur4<ResponseT, ReturnT> a(Retrofit retrofit3, Method method, bs4 bs4) {
        Type type;
        boolean z;
        Class<cs4> cls = cs4.class;
        boolean z2 = bs4.k;
        Annotation[] annotations = method.getAnnotations();
        if (z2) {
            Type[] genericParameterTypes = method.getGenericParameterTypes();
            Type a2 = gs4.a(0, (ParameterizedType) genericParameterTypes[genericParameterTypes.length - 1]);
            if (gs4.b(a2) != cls || !(a2 instanceof ParameterizedType)) {
                z = false;
            } else {
                a2 = gs4.b(0, (ParameterizedType) a2);
                z = true;
            }
            type = new gs4.b((Type) null, Call.class, a2);
            annotations = fs4.a(annotations);
        } else {
            type = method.getGenericReturnType();
            z = false;
        }
        pr4 a3 = a(retrofit3, method, type, annotations);
        Type a4 = a3.a();
        if (a4 == Response.class) {
            throw gs4.a(method, "'" + gs4.b(a4).getName() + "' is not a valid response body type. Did you mean ResponseBody?", new Object[0]);
        } else if (a4 == cls) {
            throw gs4.a(method, "Response must include generic type (e.g., Response<String>)", new Object[0]);
        } else if (!bs4.c.equals("HEAD") || Void.class.equals(a4)) {
            sr4 a5 = a(retrofit3, method, a4);
            vl4.a aVar = retrofit3.b;
            if (!z2) {
                return new a(bs4, aVar, a5, a3);
            }
            if (z) {
                return new c(bs4, aVar, a5, a3);
            }
            return new b(bs4, aVar, a5, a3, false);
        } else {
            throw gs4.a(method, "HEAD method must use Void as response type.", new Object[0]);
        }
    }

    @DexIgnore
    public abstract ReturnT a(Call<ResponseT> call, Object[] objArr);

    @DexIgnore
    public static <ResponseT, ReturnT> pr4<ResponseT, ReturnT> a(Retrofit retrofit3, Method method, Type type, Annotation[] annotationArr) {
        try {
            return retrofit3.a(type, annotationArr);
        } catch (RuntimeException e) {
            throw gs4.a(method, (Throwable) e, "Unable to create call adapter for %s", type);
        }
    }

    @DexIgnore
    public static <ResponseT> sr4<qm4, ResponseT> a(Retrofit retrofit3, Method method, Type type) {
        try {
            return retrofit3.b(type, method.getAnnotations());
        } catch (RuntimeException e) {
            throw gs4.a(method, (Throwable) e, "Unable to create converter for %s", type);
        }
    }

    @DexIgnore
    public final ReturnT a(Object[] objArr) {
        return a(new wr4(this.a, objArr, this.b, this.c), objArr);
    }
}
