package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b61 extends wb1<b61> {
    @DexIgnore
    public static volatile b61[] g;
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public h61 d; // = null;
    @DexIgnore
    public h61 e; // = null;
    @DexIgnore
    public Boolean f; // = null;

    @DexIgnore
    public b61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static b61[] e() {
        if (g == null) {
            synchronized (ac1.b) {
                if (g == null) {
                    g = new b61[0];
                }
            }
        }
        return g;
    }

    @DexIgnore
    public final void a(vb1 vb1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            vb1.b(1, num.intValue());
        }
        h61 h61 = this.d;
        if (h61 != null) {
            vb1.a(2, (bc1) h61);
        }
        h61 h612 = this.e;
        if (h612 != null) {
            vb1.a(3, (bc1) h612);
        }
        Boolean bool = this.f;
        if (bool != null) {
            vb1.a(4, bool.booleanValue());
        }
        super.a(vb1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof b61)) {
            return false;
        }
        b61 b61 = (b61) obj;
        Integer num = this.c;
        if (num == null) {
            if (b61.c != null) {
                return false;
            }
        } else if (!num.equals(b61.c)) {
            return false;
        }
        h61 h61 = this.d;
        if (h61 == null) {
            if (b61.d != null) {
                return false;
            }
        } else if (!h61.equals(b61.d)) {
            return false;
        }
        h61 h612 = this.e;
        if (h612 == null) {
            if (b61.e != null) {
                return false;
            }
        } else if (!h612.equals(b61.e)) {
            return false;
        }
        Boolean bool = this.f;
        if (bool == null) {
            if (b61.f != null) {
                return false;
            }
        } else if (!bool.equals(b61.f)) {
            return false;
        }
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            return this.b.equals(b61.b);
        }
        yb1 yb12 = b61.b;
        return yb12 == null || yb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int i;
        int i2;
        int hashCode = (b61.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i3 = 0;
        int hashCode2 = hashCode + (num == null ? 0 : num.hashCode());
        h61 h61 = this.d;
        int i4 = hashCode2 * 31;
        if (h61 == null) {
            i = 0;
        } else {
            i = h61.hashCode();
        }
        int i5 = i4 + i;
        h61 h612 = this.e;
        int i6 = i5 * 31;
        if (h612 == null) {
            i2 = 0;
        } else {
            i2 = h612.hashCode();
        }
        int i7 = (i6 + i2) * 31;
        Boolean bool = this.f;
        int hashCode3 = (i7 + (bool == null ? 0 : bool.hashCode())) * 31;
        yb1 yb1 = this.b;
        if (yb1 != null && !yb1.a()) {
            i3 = this.b.hashCode();
        }
        return hashCode3 + i3;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += vb1.c(1, num.intValue());
        }
        h61 h61 = this.d;
        if (h61 != null) {
            a += vb1.b(2, (bc1) h61);
        }
        h61 h612 = this.e;
        if (h612 != null) {
            a += vb1.b(3, (bc1) h612);
        }
        Boolean bool = this.f;
        if (bool == null) {
            return a;
        }
        bool.booleanValue();
        return a + vb1.c(4) + 1;
    }

    @DexIgnore
    public final /* synthetic */ bc1 a(ub1 ub1) throws IOException {
        while (true) {
            int c2 = ub1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Integer.valueOf(ub1.e());
            } else if (c2 == 18) {
                if (this.d == null) {
                    this.d = new h61();
                }
                ub1.a((bc1) this.d);
            } else if (c2 == 26) {
                if (this.e == null) {
                    this.e = new h61();
                }
                ub1.a((bc1) this.e);
            } else if (c2 == 32) {
                this.f = Boolean.valueOf(ub1.d());
            } else if (!super.a(ub1, c2)) {
                return this;
            }
        }
    }
}
