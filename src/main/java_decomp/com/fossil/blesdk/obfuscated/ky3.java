package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.fossil.blesdk.obfuscated.jy3;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ky3 extends jy3 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public ky3(Context context) {
        this.a = context;
    }

    @DexIgnore
    public boolean a(hy3 hy3) {
        if (hy3.e != 0) {
            return true;
        }
        return "android.resource".equals(hy3.d.getScheme());
    }

    @DexIgnore
    public jy3.a a(hy3 hy3, int i) throws IOException {
        Resources a2 = py3.a(this.a, hy3);
        return new jy3.a(a(a2, py3.a(a2, hy3), hy3), Picasso.LoadedFrom.DISK);
    }

    @DexIgnore
    public static Bitmap a(Resources resources, int i, hy3 hy3) {
        BitmapFactory.Options b = jy3.b(hy3);
        if (jy3.a(b)) {
            BitmapFactory.decodeResource(resources, i, b);
            jy3.a(hy3.h, hy3.i, b, hy3);
        }
        return BitmapFactory.decodeResource(resources, i, b);
    }
}
