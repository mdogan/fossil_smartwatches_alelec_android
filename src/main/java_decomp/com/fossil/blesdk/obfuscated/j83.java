package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j83 implements MembersInjector<ActiveTimeOverviewDayPresenter> {
    @DexIgnore
    public static void a(ActiveTimeOverviewDayPresenter activeTimeOverviewDayPresenter) {
        activeTimeOverviewDayPresenter.i();
    }
}
