package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hw implements ko {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ko c;

    @DexIgnore
    public hw(int i, ko koVar) {
        this.b = i;
        this.c = koVar;
    }

    @DexIgnore
    public static ko a(Context context) {
        return new hw(context.getResources().getConfiguration().uiMode & 48, iw.b(context));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof hw)) {
            return false;
        }
        hw hwVar = (hw) obj;
        if (this.b != hwVar.b || !this.c.equals(hwVar.c)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return vw.a((Object) this.c, this.b);
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        this.c.a(messageDigest);
        messageDigest.update(ByteBuffer.allocate(4).putInt(this.b).array());
    }
}
