package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vu implements Handler.Callback {
    @DexIgnore
    public static /* final */ b i; // = new a();
    @DexIgnore
    public volatile yn a;
    @DexIgnore
    public /* final */ Map<FragmentManager, uu> b; // = new HashMap();
    @DexIgnore
    public /* final */ Map<androidx.fragment.app.FragmentManager, yu> c; // = new HashMap();
    @DexIgnore
    public /* final */ Handler d;
    @DexIgnore
    public /* final */ b e;
    @DexIgnore
    public /* final */ g4<View, Fragment> f; // = new g4<>();
    @DexIgnore
    public /* final */ g4<View, android.app.Fragment> g; // = new g4<>();
    @DexIgnore
    public /* final */ Bundle h; // = new Bundle();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements b {
        @DexIgnore
        public yn a(sn snVar, ru ruVar, wu wuVar, Context context) {
            return new yn(snVar, ruVar, wuVar, context);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        yn a(sn snVar, ru ruVar, wu wuVar, Context context);
    }

    @DexIgnore
    public vu(b bVar) {
        this.e = bVar == null ? i : bVar;
        this.d = new Handler(Looper.getMainLooper(), this);
    }

    @DexIgnore
    public static Activity c(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        if (context instanceof ContextWrapper) {
            return c(((ContextWrapper) context).getBaseContext());
        }
        return null;
    }

    @DexIgnore
    public static boolean d(Context context) {
        Activity c2 = c(context);
        return c2 == null || !c2.isFinishing();
    }

    @DexIgnore
    public yn a(Context context) {
        if (context != null) {
            if (vw.d() && !(context instanceof Application)) {
                if (context instanceof FragmentActivity) {
                    return a((FragmentActivity) context);
                }
                if (context instanceof Activity) {
                    return a((Activity) context);
                }
                if (context instanceof ContextWrapper) {
                    ContextWrapper contextWrapper = (ContextWrapper) context;
                    if (contextWrapper.getBaseContext().getApplicationContext() != null) {
                        return a(contextWrapper.getBaseContext());
                    }
                }
            }
            return b(context);
        }
        throw new IllegalArgumentException("You cannot start a load on a null Context");
    }

    @DexIgnore
    public final yn b(Context context) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    this.a = this.e.a(sn.a(context.getApplicationContext()), new lu(), new qu(), context.getApplicationContext());
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public boolean handleMessage(Message message) {
        Object obj;
        int i2 = message.what;
        Object obj2 = null;
        boolean z = true;
        if (i2 == 1) {
            obj2 = (FragmentManager) message.obj;
            obj = this.b.remove(obj2);
        } else if (i2 != 2) {
            z = false;
            obj = null;
        } else {
            obj2 = (androidx.fragment.app.FragmentManager) message.obj;
            obj = this.c.remove(obj2);
        }
        if (z && obj == null && Log.isLoggable("RMRetriever", 5)) {
            Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + obj2);
        }
        return z;
    }

    @DexIgnore
    @TargetApi(17)
    public static void c(Activity activity) {
        if (Build.VERSION.SDK_INT >= 17 && activity.isDestroyed()) {
            throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
        }
    }

    @DexIgnore
    @Deprecated
    public final void b(FragmentManager fragmentManager, g4<View, android.app.Fragment> g4Var) {
        int i2 = 0;
        while (true) {
            int i3 = i2 + 1;
            this.h.putInt("key", i2);
            android.app.Fragment fragment = null;
            try {
                fragment = fragmentManager.getFragment(this.h, "key");
            } catch (Exception unused) {
            }
            if (fragment != null) {
                if (fragment.getView() != null) {
                    g4Var.put(fragment.getView(), fragment);
                    if (Build.VERSION.SDK_INT >= 17) {
                        a(fragment.getChildFragmentManager(), g4Var);
                    }
                }
                i2 = i3;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public yn a(FragmentActivity fragmentActivity) {
        if (vw.c()) {
            return a(fragmentActivity.getApplicationContext());
        }
        c((Activity) fragmentActivity);
        return a((Context) fragmentActivity, fragmentActivity.getSupportFragmentManager(), (Fragment) null, d(fragmentActivity));
    }

    @DexIgnore
    public yn a(Fragment fragment) {
        uw.a(fragment.getContext(), "You cannot start a load on a fragment before it is attached or after it is destroyed");
        if (vw.c()) {
            return a(fragment.getContext().getApplicationContext());
        }
        return a(fragment.getContext(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
    }

    @DexIgnore
    @Deprecated
    public uu b(Activity activity) {
        return a(activity.getFragmentManager(), (android.app.Fragment) null, d(activity));
    }

    @DexIgnore
    public yn a(Activity activity) {
        if (vw.c()) {
            return a(activity.getApplicationContext());
        }
        c(activity);
        return a((Context) activity, activity.getFragmentManager(), (android.app.Fragment) null, d(activity));
    }

    @DexIgnore
    public yn a(View view) {
        if (vw.c()) {
            return a(view.getContext().getApplicationContext());
        }
        uw.a(view);
        uw.a(view.getContext(), "Unable to obtain a request manager for a view without a Context");
        Activity c2 = c(view.getContext());
        if (c2 == null) {
            return a(view.getContext().getApplicationContext());
        }
        if (c2 instanceof FragmentActivity) {
            FragmentActivity fragmentActivity = (FragmentActivity) c2;
            Fragment a2 = a(view, fragmentActivity);
            return a2 != null ? a(a2) : a(fragmentActivity);
        }
        android.app.Fragment a3 = a(view, c2);
        if (a3 == null) {
            return a(c2);
        }
        return a(a3);
    }

    @DexIgnore
    public static void a(Collection<Fragment> collection, Map<View, Fragment> map) {
        if (collection != null) {
            for (Fragment next : collection) {
                if (!(next == null || next.getView() == null)) {
                    map.put(next.getView(), next);
                    a((Collection<Fragment>) next.getChildFragmentManager().d(), map);
                }
            }
        }
    }

    @DexIgnore
    public final Fragment a(View view, FragmentActivity fragmentActivity) {
        this.f.clear();
        a((Collection<Fragment>) fragmentActivity.getSupportFragmentManager().d(), (Map<View, Fragment>) this.f);
        View findViewById = fragmentActivity.findViewById(16908290);
        Fragment fragment = null;
        while (!view.equals(findViewById)) {
            fragment = this.f.get(view);
            if (fragment != null || !(view.getParent() instanceof View)) {
                break;
            }
            view = (View) view.getParent();
        }
        this.f.clear();
        return fragment;
    }

    @DexIgnore
    @Deprecated
    public final android.app.Fragment a(View view, Activity activity) {
        this.g.clear();
        a(activity.getFragmentManager(), this.g);
        View findViewById = activity.findViewById(16908290);
        android.app.Fragment fragment = null;
        while (!view.equals(findViewById)) {
            fragment = this.g.get(view);
            if (fragment != null || !(view.getParent() instanceof View)) {
                break;
            }
            view = (View) view.getParent();
        }
        this.g.clear();
        return fragment;
    }

    @DexIgnore
    @TargetApi(26)
    @Deprecated
    public final void a(FragmentManager fragmentManager, g4<View, android.app.Fragment> g4Var) {
        if (Build.VERSION.SDK_INT >= 26) {
            for (android.app.Fragment next : fragmentManager.getFragments()) {
                if (next.getView() != null) {
                    g4Var.put(next.getView(), next);
                    a(next.getChildFragmentManager(), g4Var);
                }
            }
            return;
        }
        b(fragmentManager, g4Var);
    }

    @DexIgnore
    @TargetApi(17)
    @Deprecated
    public yn a(android.app.Fragment fragment) {
        if (fragment.getActivity() == null) {
            throw new IllegalArgumentException("You cannot start a load on a fragment before it is attached");
        } else if (vw.c() || Build.VERSION.SDK_INT < 17) {
            return a(fragment.getActivity().getApplicationContext());
        } else {
            return a((Context) fragment.getActivity(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
        }
    }

    @DexIgnore
    public final uu a(FragmentManager fragmentManager, android.app.Fragment fragment, boolean z) {
        uu uuVar = (uu) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (uuVar == null) {
            uuVar = this.b.get(fragmentManager);
            if (uuVar == null) {
                uuVar = new uu();
                uuVar.b(fragment);
                if (z) {
                    uuVar.b().b();
                }
                this.b.put(fragmentManager, uuVar);
                fragmentManager.beginTransaction().add(uuVar, "com.bumptech.glide.manager").commitAllowingStateLoss();
                this.d.obtainMessage(1, fragmentManager).sendToTarget();
            }
        }
        return uuVar;
    }

    @DexIgnore
    @Deprecated
    public final yn a(Context context, FragmentManager fragmentManager, android.app.Fragment fragment, boolean z) {
        uu a2 = a(fragmentManager, fragment, z);
        yn d2 = a2.d();
        if (d2 != null) {
            return d2;
        }
        yn a3 = this.e.a(sn.a(context), a2.b(), a2.e(), context);
        a2.a(a3);
        return a3;
    }

    @DexIgnore
    public yu a(Context context, androidx.fragment.app.FragmentManager fragmentManager) {
        return a(fragmentManager, (Fragment) null, d(context));
    }

    @DexIgnore
    public final yu a(androidx.fragment.app.FragmentManager fragmentManager, Fragment fragment, boolean z) {
        yu yuVar = (yu) fragmentManager.a("com.bumptech.glide.manager");
        if (yuVar == null) {
            yuVar = this.c.get(fragmentManager);
            if (yuVar == null) {
                yuVar = new yu();
                yuVar.b(fragment);
                if (z) {
                    yuVar.O0().b();
                }
                this.c.put(fragmentManager, yuVar);
                cb a2 = fragmentManager.a();
                a2.a((Fragment) yuVar, "com.bumptech.glide.manager");
                a2.b();
                this.d.obtainMessage(2, fragmentManager).sendToTarget();
            }
        }
        return yuVar;
    }

    @DexIgnore
    public final yn a(Context context, androidx.fragment.app.FragmentManager fragmentManager, Fragment fragment, boolean z) {
        yu a2 = a(fragmentManager, fragment, z);
        yn Q0 = a2.Q0();
        if (Q0 != null) {
            return Q0;
        }
        yn a3 = this.e.a(sn.a(context), a2.O0(), a2.R0(), context);
        a2.a(a3);
        return a3;
    }
}
