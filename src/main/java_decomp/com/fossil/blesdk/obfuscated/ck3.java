package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ck3 implements MembersInjector<LoginActivity> {
    @DexIgnore
    public static void a(LoginActivity loginActivity, jn2 jn2) {
        loginActivity.B = jn2;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, kn2 kn2) {
        loginActivity.C = kn2;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, ln2 ln2) {
        loginActivity.D = ln2;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, MFLoginWechatManager mFLoginWechatManager) {
        loginActivity.E = mFLoginWechatManager;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, LoginPresenter loginPresenter) {
        loginActivity.F = loginPresenter;
    }
}
