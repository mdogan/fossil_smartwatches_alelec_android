package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ld0 {
    @DexIgnore
    public static /* final */ int common_google_play_services_enable_button; // = 2131821488;
    @DexIgnore
    public static /* final */ int common_google_play_services_enable_text; // = 2131821489;
    @DexIgnore
    public static /* final */ int common_google_play_services_enable_title; // = 2131821490;
    @DexIgnore
    public static /* final */ int common_google_play_services_install_button; // = 2131821491;
    @DexIgnore
    public static /* final */ int common_google_play_services_install_text; // = 2131821492;
    @DexIgnore
    public static /* final */ int common_google_play_services_install_title; // = 2131821493;
    @DexIgnore
    public static /* final */ int common_google_play_services_notification_channel_name; // = 2131821494;
    @DexIgnore
    public static /* final */ int common_google_play_services_notification_ticker; // = 2131821495;
    @DexIgnore
    public static /* final */ int common_google_play_services_unsupported_text; // = 2131821497;
    @DexIgnore
    public static /* final */ int common_google_play_services_update_button; // = 2131821498;
    @DexIgnore
    public static /* final */ int common_google_play_services_update_text; // = 2131821499;
    @DexIgnore
    public static /* final */ int common_google_play_services_update_title; // = 2131821500;
    @DexIgnore
    public static /* final */ int common_google_play_services_updating_text; // = 2131821501;
    @DexIgnore
    public static /* final */ int common_google_play_services_wear_update_text; // = 2131821502;
    @DexIgnore
    public static /* final */ int common_open_on_phone; // = 2131821503;
    @DexIgnore
    public static /* final */ int common_signin_button_text; // = 2131821504;
    @DexIgnore
    public static /* final */ int common_signin_button_text_long; // = 2131821505;
}
