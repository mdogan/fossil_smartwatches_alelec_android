package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.AnimationImageView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o62 extends RecyclerView.g<b> {
    @DexIgnore
    public static /* final */ String c; // = c;
    @DexIgnore
    public ArrayList<Explore> a; // = new ArrayList<>();
    @DexIgnore
    public b b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ AnimationImageView c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(View view) {
            super(view);
            wd4.b(view, "itemView");
            View findViewById = view.findViewById(R.id.ftv_title);
            wd4.a((Object) findViewById, "itemView.findViewById(R.id.ftv_title)");
            this.a = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(R.id.ftv_desc);
            wd4.a((Object) findViewById2, "itemView.findViewById(R.id.ftv_desc)");
            this.b = (FlexibleTextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.iv_device);
            wd4.a((Object) findViewById3, "itemView.findViewById(R.id.iv_device)");
            this.c = (AnimationImageView) findViewById3;
        }

        @DexIgnore
        public final AnimationImageView a() {
            return this.c;
        }

        @DexIgnore
        public final void a(Explore explore) {
            if (explore != null) {
                this.a.setText(explore.getTitle());
                this.b.setText(explore.getDescription());
                Explore.ExploreType exploreType = explore.getExploreType();
                if (exploreType != null) {
                    int i = p62.a[exploreType.ordinal()];
                    if (i == 1) {
                        AnimationImageView animationImageView = this.c;
                        String string = PortfolioApp.W.c().getString(R.string.animation_wrist_flick);
                        wd4.a((Object) string, "PortfolioApp.instance.ge\u2026ng.animation_wrist_flick)");
                        animationImageView.a(string, 1, 80, 30, MFNetworkReturnCode.BAD_REQUEST, FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION);
                    } else if (i == 2) {
                        AnimationImageView animationImageView2 = this.c;
                        String string2 = PortfolioApp.W.c().getString(R.string.animation_double_tap);
                        wd4.a((Object) string2, "PortfolioApp.instance.ge\u2026ing.animation_double_tap)");
                        animationImageView2.a(string2, 1, 50, 30, 600, 2400);
                    } else if (i == 3) {
                        AnimationImageView animationImageView3 = this.c;
                        String string3 = PortfolioApp.W.c().getString(R.string.animation_press_and_hold);
                        wd4.a((Object) string3, "PortfolioApp.instance.ge\u2026animation_press_and_hold)");
                        animationImageView3.a(string3, 1, 50, 30, 1000, 3000);
                    }
                }
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public final void a(List<? extends Explore> list) {
        wd4.b(list, "data");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: b */
    public void onViewDetachedFromWindow(b bVar) {
        wd4.b(bVar, "holder");
        FLogger.INSTANCE.getLocal().d(c, "onViewDetachedFromWindow");
        bVar.a().d();
        super.onViewDetachedFromWindow(bVar);
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "viewGroup");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_watch_explore, viewGroup, false);
        wd4.a((Object) inflate, "view");
        return new b(inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        wd4.b(bVar, "welcomeViewHolder");
        bVar.a(this.a.get(i));
    }

    @DexIgnore
    public final void b() {
        FLogger.INSTANCE.getLocal().d(c, "onStopAnimation");
        b bVar = this.b;
        if (bVar != null) {
            AnimationImageView a2 = bVar.a();
            if (a2 != null) {
                a2.d();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void onViewAttachedToWindow(b bVar) {
        wd4.b(bVar, "holder");
        FLogger.INSTANCE.getLocal().d(c, "onViewAttachedToWindow");
        this.b = bVar;
        bVar.a().a();
        super.onViewAttachedToWindow(bVar);
    }
}
