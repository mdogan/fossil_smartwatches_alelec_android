package com.fossil.blesdk.obfuscated;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class jv3 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends jv3 {
        @DexIgnore
        public /* final */ /* synthetic */ gv3 a;
        @DexIgnore
        public /* final */ /* synthetic */ ByteString b;

        @DexIgnore
        public a(gv3 gv3, ByteString byteString) {
            this.a = gv3;
            this.b = byteString;
        }

        @DexIgnore
        public long contentLength() throws IOException {
            return (long) this.b.size();
        }

        @DexIgnore
        public gv3 contentType() {
            return this.a;
        }

        @DexIgnore
        public void writeTo(wo4 wo4) throws IOException {
            wo4.a(this.b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends jv3 {
        @DexIgnore
        public /* final */ /* synthetic */ gv3 a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ byte[] c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;

        @DexIgnore
        public b(gv3 gv3, int i, byte[] bArr, int i2) {
            this.a = gv3;
            this.b = i;
            this.c = bArr;
            this.d = i2;
        }

        @DexIgnore
        public long contentLength() {
            return (long) this.b;
        }

        @DexIgnore
        public gv3 contentType() {
            return this.a;
        }

        @DexIgnore
        public void writeTo(wo4 wo4) throws IOException {
            wo4.write(this.c, this.d, this.b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends jv3 {
        @DexIgnore
        public /* final */ /* synthetic */ gv3 a;
        @DexIgnore
        public /* final */ /* synthetic */ File b;

        @DexIgnore
        public c(gv3 gv3, File file) {
            this.a = gv3;
            this.b = file;
        }

        @DexIgnore
        public long contentLength() {
            return this.b.length();
        }

        @DexIgnore
        public gv3 contentType() {
            return this.a;
        }

        @DexIgnore
        public void writeTo(wo4 wo4) throws IOException {
            kp4 kp4 = null;
            try {
                kp4 = ep4.c(this.b);
                wo4.a(kp4);
            } finally {
                xv3.a((Closeable) kp4);
            }
        }
    }

    @DexIgnore
    public static jv3 create(gv3 gv3, String str) {
        Charset charset = xv3.c;
        if (gv3 != null) {
            charset = gv3.a();
            if (charset == null) {
                charset = xv3.c;
                gv3 = gv3.a(gv3 + "; charset=utf-8");
            }
        }
        return create(gv3, str.getBytes(charset));
    }

    @DexIgnore
    public abstract long contentLength() throws IOException;

    @DexIgnore
    public abstract gv3 contentType();

    @DexIgnore
    public abstract void writeTo(wo4 wo4) throws IOException;

    @DexIgnore
    public static jv3 create(gv3 gv3, ByteString byteString) {
        return new a(gv3, byteString);
    }

    @DexIgnore
    public static jv3 create(gv3 gv3, byte[] bArr) {
        return create(gv3, bArr, 0, bArr.length);
    }

    @DexIgnore
    public static jv3 create(gv3 gv3, byte[] bArr, int i, int i2) {
        if (bArr != null) {
            xv3.a((long) bArr.length, (long) i, (long) i2);
            return new b(gv3, i2, bArr, i);
        }
        throw new NullPointerException("content == null");
    }

    @DexIgnore
    public static jv3 create(gv3 gv3, File file) {
        if (file != null) {
            return new c(gv3, file);
        }
        throw new NullPointerException("content == null");
    }
}
