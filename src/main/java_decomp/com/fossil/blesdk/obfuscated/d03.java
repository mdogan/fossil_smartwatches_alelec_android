package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d03 {
    @DexIgnore
    public /* final */ b03 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<String> c;

    @DexIgnore
    public d03(b03 b03, int i, ArrayList<String> arrayList) {
        wd4.b(b03, "mView");
        this.a = b03;
        this.b = i;
        this.c = arrayList;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<String> b() {
        ArrayList<String> arrayList = this.c;
        return arrayList == null ? new ArrayList<>() : arrayList;
    }

    @DexIgnore
    public final b03 c() {
        return this.a;
    }
}
