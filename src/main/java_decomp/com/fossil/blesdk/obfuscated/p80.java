package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.LegacyFileControlStatusCode;
import com.fossil.blesdk.device.logic.request.legacy.LegacyFileControlOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p80 extends g70 {
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId G;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId H;
    @DexIgnore
    public /* final */ byte[] I;
    @DexIgnore
    public byte[] J;
    @DexIgnore
    public /* final */ short K;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ p80(short s, Peripheral peripheral, int i, int i2, rd4 rd4) {
        this(s, peripheral, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    public GattCharacteristic.CharacteristicId B() {
        return this.H;
    }

    @DexIgnore
    public byte[] D() {
        return this.I;
    }

    @DexIgnore
    public GattCharacteristic.CharacteristicId E() {
        return this.G;
    }

    @DexIgnore
    public byte[] G() {
        return this.J;
    }

    @DexIgnore
    public p70 b(byte b) {
        return LegacyFileControlStatusCode.Companion.a(b);
    }

    @DexIgnore
    public JSONObject t() {
        return xa0.a(super.t(), JSONKey.FILE_HANDLE, o90.a(this.K));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p80(short s, Peripheral peripheral, int i) {
        super(RequestId.LEGACY_ERASE_ACTIVITY_FILE, peripheral, i);
        wd4.b(peripheral, "peripheral");
        this.K = s;
        GattCharacteristic.CharacteristicId characteristicId = GattCharacteristic.CharacteristicId.FTC;
        this.G = characteristicId;
        this.H = characteristicId;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(LegacyFileControlOperationCode.LEGACY_ERASE_FILE.getCode()).putShort(this.K).array();
        wd4.a((Object) array, "ByteBuffer.allocate(3)\n \u2026dle)\n            .array()");
        this.I = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(LegacyFileControlOperationCode.LEGACY_ERASE_FILE.responseCode()).putShort(this.K).array();
        wd4.a((Object) array2, "ByteBuffer.allocate(3)\n \u2026dle)\n            .array()");
        this.J = array2;
    }
}
