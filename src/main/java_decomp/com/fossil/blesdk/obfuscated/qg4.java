package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ri4;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CompletionHandlerException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qg4<T> extends yh4<T> implements pg4<T>, rc4 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater j; // = AtomicIntegerFieldUpdater.newUpdater(qg4.class, "_decision");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater k; // = AtomicReferenceFieldUpdater.newUpdater(qg4.class, Object.class, "_state");
    @DexIgnore
    public volatile int _decision; // = 0;
    @DexIgnore
    public volatile Object _state; // = hg4.e;
    @DexIgnore
    public /* final */ CoroutineContext h; // = this.i.getContext();
    @DexIgnore
    public /* final */ kc4<T> i;
    @DexIgnore
    public volatile ai4 parentHandle;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qg4(kc4<? super T> kc4, int i2) {
        super(i2);
        wd4.b(kc4, "delegate");
        this.i = kc4;
    }

    @DexIgnore
    public void a(Object obj, Throwable th) {
        wd4.b(th, "cause");
        if (obj instanceof ch4) {
            try {
                ((ch4) obj).b.invoke(th);
            } catch (Throwable th2) {
                CoroutineContext context = getContext();
                ih4.a(context, (Throwable) new CompletionHandlerException("Exception in cancellation handler for " + this, th2));
            }
        }
    }

    @DexIgnore
    public final kc4<T> b() {
        return this.i;
    }

    @DexIgnore
    public Object c() {
        return f();
    }

    @DexIgnore
    public final void d(Object obj) {
        throw new IllegalStateException(("Already resumed, but proposed with update " + obj).toString());
    }

    @DexIgnore
    public final Object e() {
        g();
        if (k()) {
            return oc4.a();
        }
        Object f = f();
        if (!(f instanceof zg4)) {
            if (this.g == 1) {
                ri4 ri4 = (ri4) getContext().get(ri4.d);
                if (ri4 != null && !ri4.isActive()) {
                    CancellationException y = ri4.y();
                    a(f, (Throwable) y);
                    throw ok4.a(y, (kc4<?>) this);
                }
            }
            return c(f);
        }
        throw ok4.a(((zg4) f).a, (kc4<?>) this);
    }

    @DexIgnore
    public final Object f() {
        return this._state;
    }

    @DexIgnore
    public final void g() {
        if (!h()) {
            ri4 ri4 = (ri4) this.i.getContext().get(ri4.d);
            if (ri4 != null) {
                ri4.start();
                ai4 a = ri4.a.a(ri4, true, false, new tg4(ri4, this), 2, (Object) null);
                this.parentHandle = a;
                if (h()) {
                    a.dispose();
                    this.parentHandle = dj4.e;
                }
            }
        }
    }

    @DexIgnore
    public rc4 getCallerFrame() {
        kc4<T> kc4 = this.i;
        if (!(kc4 instanceof rc4)) {
            kc4 = null;
        }
        return (rc4) kc4;
    }

    @DexIgnore
    public CoroutineContext getContext() {
        return this.h;
    }

    @DexIgnore
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public boolean h() {
        return !(f() instanceof ej4);
    }

    @DexIgnore
    public String i() {
        return "CancellableContinuation";
    }

    @DexIgnore
    public boolean isActive() {
        return f() instanceof ej4;
    }

    @DexIgnore
    public final boolean j() {
        do {
            int i2 = this._decision;
            if (i2 != 0) {
                if (i2 == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!j.compareAndSet(this, 0, 2));
        return true;
    }

    @DexIgnore
    public final boolean k() {
        do {
            int i2 = this._decision;
            if (i2 != 0) {
                if (i2 == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!j.compareAndSet(this, 0, 1));
        return true;
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        a(ah4.a(obj), this.g);
    }

    @DexIgnore
    public String toString() {
        return i() + '(' + ph4.a((kc4<?>) this.i) + "){" + f() + "}@" + ph4.b(this);
    }

    @DexIgnore
    public void b(Object obj) {
        wd4.b(obj, "token");
        a(this.g);
    }

    @DexIgnore
    public <T> T c(Object obj) {
        if (obj instanceof bh4) {
            return ((bh4) obj).b;
        }
        return obj instanceof ch4 ? ((ch4) obj).a : obj;
    }

    @DexIgnore
    public final void d() {
        ai4 ai4 = this.parentHandle;
        if (ai4 != null) {
            ai4.dispose();
            this.parentHandle = dj4.e;
        }
    }

    @DexIgnore
    public void b(jd4<? super Throwable, cb4> jd4) {
        Object obj;
        wd4.b(jd4, "handler");
        Throwable th = null;
        ng4 ng4 = null;
        do {
            obj = this._state;
            if (obj instanceof hg4) {
                if (ng4 == null) {
                    ng4 = a(jd4);
                }
            } else if (obj instanceof ng4) {
                a(jd4, obj);
                throw null;
            } else if (!(obj instanceof sg4)) {
                return;
            } else {
                if (((sg4) obj).b()) {
                    try {
                        if (!(obj instanceof zg4)) {
                            obj = null;
                        }
                        zg4 zg4 = (zg4) obj;
                        if (zg4 != null) {
                            th = zg4.a;
                        }
                        jd4.invoke(th);
                        return;
                    } catch (Throwable th2) {
                        ih4.a(getContext(), (Throwable) new CompletionHandlerException("Exception in cancellation handler for " + this, th2));
                        return;
                    }
                } else {
                    a(jd4, obj);
                    throw null;
                }
            }
        } while (!k.compareAndSet(this, obj, ng4));
    }

    @DexIgnore
    public Throwable a(ri4 ri4) {
        wd4.b(ri4, "parent");
        return ri4.y();
    }

    @DexIgnore
    public final sg4 a(Throwable th, int i2) {
        wd4.b(th, "exception");
        return a((Object) new zg4(th, false, 2, (rd4) null), i2);
    }

    @DexIgnore
    public final void a(jd4<? super Throwable, cb4> jd4, Object obj) {
        throw new IllegalStateException(("It's prohibited to register multiple handlers, tried to register " + jd4 + ", already has " + obj).toString());
    }

    @DexIgnore
    public final ng4 a(jd4<? super Throwable, cb4> jd4) {
        return jd4 instanceof ng4 ? (ng4) jd4 : new oi4(jd4);
    }

    @DexIgnore
    public final void a(int i2) {
        if (!j()) {
            xh4.a(this, i2);
        }
    }

    @DexIgnore
    public void a(gh4 gh4, T t) {
        wd4.b(gh4, "$this$resumeUndispatched");
        kc4<T> kc4 = this.i;
        gh4 gh42 = null;
        if (!(kc4 instanceof vh4)) {
            kc4 = null;
        }
        vh4 vh4 = (vh4) kc4;
        if (vh4 != null) {
            gh42 = vh4.k;
        }
        a((Object) t, gh42 == gh4 ? 3 : this.g);
    }

    @DexIgnore
    public boolean a(Throwable th) {
        Object obj;
        boolean z;
        do {
            obj = this._state;
            if (!(obj instanceof ej4)) {
                return false;
            }
            z = obj instanceof ng4;
        } while (!k.compareAndSet(this, obj, new sg4(this, th, z)));
        if (z) {
            try {
                ((ng4) obj).a(th);
            } catch (Throwable th2) {
                CoroutineContext context = getContext();
                ih4.a(context, (Throwable) new CompletionHandlerException("Exception in cancellation handler for " + this, th2));
            }
        }
        d();
        a(0);
        return true;
    }

    @DexIgnore
    public final sg4 a(Object obj, int i2) {
        Object obj2;
        do {
            obj2 = this._state;
            if (!(obj2 instanceof ej4)) {
                if (obj2 instanceof sg4) {
                    sg4 sg4 = (sg4) obj2;
                    if (sg4.c()) {
                        return sg4;
                    }
                }
                d(obj);
                throw null;
            }
        } while (!k.compareAndSet(this, obj2, obj));
        d();
        a(i2);
        return null;
    }

    @DexIgnore
    public Object a(T t, Object obj) {
        Object obj2;
        T t2;
        do {
            obj2 = this._state;
            if (obj2 instanceof ej4) {
                if (obj == null) {
                    t2 = t;
                } else {
                    t2 = new bh4(obj, t, (ej4) obj2);
                }
            } else if (!(obj2 instanceof bh4)) {
                return null;
            } else {
                bh4 bh4 = (bh4) obj2;
                if (bh4.a != obj) {
                    return null;
                }
                if (oh4.a()) {
                    if (!(bh4.b == t)) {
                        throw new AssertionError();
                    }
                }
                return bh4.c;
            }
        } while (!k.compareAndSet(this, obj2, t2));
        d();
        return obj2;
    }
}
