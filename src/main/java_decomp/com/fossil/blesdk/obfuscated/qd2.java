package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qd2 extends pd2 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(R.id.ftv_title, 1);
        A.put(R.id.v_line0, 2);
        A.put(R.id.ftv_calls_and_messages, 3);
        A.put(R.id.iv_calls_message_check, 4);
        A.put(R.id.v_line1, 5);
        A.put(R.id.ftv_calls, 6);
        A.put(R.id.iv_calls_check, 7);
        A.put(R.id.v_line2, 8);
        A.put(R.id.ftv_messages, 9);
        A.put(R.id.iv_messages_check, 10);
        A.put(R.id.v_line3, 11);
        A.put(R.id.iv_x, 12);
    }
    */

    @DexIgnore
    public qd2(qa qaVar, View view) {
        this(qaVar, view, ViewDataBinding.a(qaVar, view, 13, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    public qd2(qa qaVar, View view, Object[] objArr) {
        super(qaVar, view, 0, objArr[6], objArr[3], objArr[9], objArr[1], objArr[7], objArr[4], objArr[10], objArr[12], objArr[2], objArr[5], objArr[8], objArr[11]);
        this.y = -1;
        this.x = objArr[0];
        this.x.setTag((Object) null);
        a(view);
        f();
    }
}
