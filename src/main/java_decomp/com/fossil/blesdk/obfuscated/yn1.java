package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yn1<TResult> {
    @DexIgnore
    public /* final */ vo1<TResult> a; // = new vo1<>();

    @DexIgnore
    public yn1() {
    }

    @DexIgnore
    public void a(TResult tresult) {
        this.a.a(tresult);
    }

    @DexIgnore
    public boolean b(TResult tresult) {
        return this.a.b(tresult);
    }

    @DexIgnore
    public void a(Exception exc) {
        this.a.a(exc);
    }

    @DexIgnore
    public boolean b(Exception exc) {
        return this.a.b(exc);
    }

    @DexIgnore
    public yn1(on1 on1) {
        on1.a(new to1(this));
    }

    @DexIgnore
    public xn1<TResult> a() {
        return this.a;
    }
}
