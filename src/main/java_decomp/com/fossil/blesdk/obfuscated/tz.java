package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@SuppressLint({"CommitPrefEdits"})
public class tz {
    @DexIgnore
    public /* final */ h74 a;
    @DexIgnore
    public /* final */ vy b;

    @DexIgnore
    public tz(h74 h74, vy vyVar) {
        this.a = h74;
        this.b = vyVar;
    }

    @DexIgnore
    public static tz a(h74 h74, vy vyVar) {
        return new tz(h74, vyVar);
    }

    @DexIgnore
    public void a(boolean z) {
        h74 h74 = this.a;
        h74.a(h74.edit().putBoolean("always_send_reports_opt_in", z));
    }

    @DexIgnore
    public boolean a() {
        if (!this.a.get().contains("preferences_migration_complete")) {
            i74 i74 = new i74(this.b);
            if (!this.a.get().contains("always_send_reports_opt_in") && i74.get().contains("always_send_reports_opt_in")) {
                boolean z = i74.get().getBoolean("always_send_reports_opt_in", false);
                h74 h74 = this.a;
                h74.a(h74.edit().putBoolean("always_send_reports_opt_in", z));
            }
            h74 h742 = this.a;
            h742.a(h742.edit().putBoolean("preferences_migration_complete", true));
        }
        return this.a.get().getBoolean("always_send_reports_opt_in", false);
    }
}
