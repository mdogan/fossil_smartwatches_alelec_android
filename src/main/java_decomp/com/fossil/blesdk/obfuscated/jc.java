package com.fossil.blesdk.obfuscated;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class jc {
    @DexIgnore
    public /* final */ Map<String, Object> a; // = new HashMap();
    @DexIgnore
    public volatile boolean b; // = false;

    @DexIgnore
    public final void a() {
        this.b = true;
        Map<String, Object> map = this.a;
        if (map != null) {
            synchronized (map) {
                for (Object a2 : this.a.values()) {
                    a(a2);
                }
            }
        }
        b();
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public <T> T a(String str, T t) {
        T t2;
        synchronized (this.a) {
            t2 = this.a.get(str);
            if (t2 == null) {
                this.a.put(str, t);
            }
        }
        if (t2 != null) {
            t = t2;
        }
        if (this.b) {
            a((Object) t);
        }
        return t;
    }

    @DexIgnore
    public <T> T a(String str) {
        T t;
        synchronized (this.a) {
            t = this.a.get(str);
        }
        return t;
    }

    @DexIgnore
    public static void a(Object obj) {
        if (obj instanceof Closeable) {
            try {
                ((Closeable) obj).close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
