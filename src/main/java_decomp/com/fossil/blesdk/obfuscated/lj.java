package com.fossil.blesdk.obfuscated;

import androidx.work.ListenableWorker;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class lj {
    @DexIgnore
    public UUID a;
    @DexIgnore
    public il b;
    @DexIgnore
    public Set<String> c;

    @DexIgnore
    public lj(UUID uuid, il ilVar, Set<String> set) {
        this.a = uuid;
        this.b = ilVar;
        this.c = set;
    }

    @DexIgnore
    public UUID a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return this.a.toString();
    }

    @DexIgnore
    public Set<String> c() {
        return this.c;
    }

    @DexIgnore
    public il d() {
        return this.b;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<B extends a, W extends lj> {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public UUID b; // = UUID.randomUUID();
        @DexIgnore
        public il c;
        @DexIgnore
        public Set<String> d; // = new HashSet();

        @DexIgnore
        public a(Class<? extends ListenableWorker> cls) {
            this.c = new il(this.b.toString(), cls.getName());
            a(cls.getName());
        }

        @DexIgnore
        public final B a(zi ziVar) {
            this.c.j = ziVar;
            c();
            return this;
        }

        @DexIgnore
        public abstract W b();

        @DexIgnore
        public abstract B c();

        @DexIgnore
        public final B a(String str) {
            this.d.add(str);
            c();
            return this;
        }

        @DexIgnore
        public final W a() {
            W b2 = b();
            this.b = UUID.randomUUID();
            this.c = new il(this.c);
            this.c.a = this.b.toString();
            return b2;
        }
    }
}
