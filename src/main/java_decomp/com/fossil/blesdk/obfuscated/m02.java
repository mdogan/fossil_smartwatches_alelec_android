package com.fossil.blesdk.obfuscated;

import com.google.gson.stream.JsonReader;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class m02 {
    @DexIgnore
    public static m02 a;

    @DexIgnore
    public abstract void a(JsonReader jsonReader) throws IOException;
}
