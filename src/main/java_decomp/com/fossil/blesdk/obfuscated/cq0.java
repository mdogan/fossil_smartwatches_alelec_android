package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cq0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<cq0> CREATOR; // = new dq0();
    @DexIgnore
    public /* final */ h11 e;

    @DexIgnore
    public cq0(IBinder iBinder) {
        this.e = i11.a(iBinder);
    }

    @DexIgnore
    public final String toString() {
        return String.format("DisableFitRequest", new Object[0]);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e.asBinder(), false);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public cq0(h11 h11) {
        this.e = h11;
    }
}
