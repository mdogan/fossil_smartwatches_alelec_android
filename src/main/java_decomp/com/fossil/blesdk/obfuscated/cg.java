package com.fossil.blesdk.obfuscated;

import android.database.AbstractWindowedCursor;
import android.database.Cursor;
import android.os.Build;
import androidx.room.RoomDatabase;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cg {
    @DexIgnore
    public static Cursor a(RoomDatabase roomDatabase, kg kgVar, boolean z) {
        Cursor query = roomDatabase.query(kgVar);
        if (!z || !(query instanceof AbstractWindowedCursor)) {
            return query;
        }
        AbstractWindowedCursor abstractWindowedCursor = (AbstractWindowedCursor) query;
        int count = abstractWindowedCursor.getCount();
        return (Build.VERSION.SDK_INT < 23 || (abstractWindowedCursor.hasWindow() ? abstractWindowedCursor.getWindow().getNumRows() : count) < count) ? bg.a(abstractWindowedCursor) : query;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static void a(hg hgVar) {
        ArrayList<String> arrayList = new ArrayList<>();
        Cursor d = hgVar.d("SELECT name FROM sqlite_master WHERE type = 'trigger'");
        while (d.moveToNext()) {
            try {
                arrayList.add(d.getString(0));
            } catch (Throwable th) {
                d.close();
                throw th;
            }
        }
        d.close();
        for (String str : arrayList) {
            if (str.startsWith("room_fts_content_sync_")) {
                hgVar.b("DROP TRIGGER IF EXISTS " + str);
            }
        }
    }
}
