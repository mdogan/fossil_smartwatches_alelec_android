package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class j91 {
    @DexIgnore
    public static /* final */ j91 a; // = new l91();
    @DexIgnore
    public static /* final */ j91 b; // = new m91();

    @DexIgnore
    public j91() {
    }

    @DexIgnore
    public static j91 a() {
        return a;
    }

    @DexIgnore
    public static j91 b() {
        return b;
    }

    @DexIgnore
    public abstract <L> List<L> a(Object obj, long j);

    @DexIgnore
    public abstract <L> void a(Object obj, Object obj2, long j);

    @DexIgnore
    public abstract void b(Object obj, long j);
}
