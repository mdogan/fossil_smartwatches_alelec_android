package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bz1 implements az1 {
    @DexIgnore
    public /* final */ IBinder e;

    @DexIgnore
    public bz1(IBinder iBinder) {
        this.e = iBinder;
    }

    @DexIgnore
    public final void a(Message message) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken("com.google.android.gms.iid.IMessengerCompat");
        obtain.writeInt(1);
        message.writeToParcel(obtain, 0);
        try {
            this.e.transact(1, obtain, (Parcel) null, 1);
        } finally {
            obtain.recycle();
        }
    }

    @DexIgnore
    public final IBinder asBinder() {
        return this.e;
    }
}
