package com.fossil.blesdk.obfuscated;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ab1 extends AbstractSet<Map.Entry<K, V>> {
    @DexIgnore
    public /* final */ /* synthetic */ ra1 e;

    @DexIgnore
    public ab1(ra1 ra1) {
        this.e = ra1;
    }

    @DexIgnore
    public /* synthetic */ boolean add(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (contains(entry)) {
            return false;
        }
        this.e.put((Comparable) entry.getKey(), entry.getValue());
        return true;
    }

    @DexIgnore
    public void clear() {
        this.e.clear();
    }

    @DexIgnore
    public boolean contains(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.e.get(entry.getKey());
        Object value = entry.getValue();
        if (obj2 != value) {
            return obj2 != null && obj2.equals(value);
        }
        return true;
    }

    @DexIgnore
    public Iterator<Map.Entry<K, V>> iterator() {
        return new za1(this.e, (sa1) null);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (!contains(entry)) {
            return false;
        }
        this.e.remove(entry.getKey());
        return true;
    }

    @DexIgnore
    public int size() {
        return this.e.size();
    }

    @DexIgnore
    public /* synthetic */ ab1(ra1 ra1, sa1 sa1) {
        this(ra1);
    }
}
