package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class te3 implements MembersInjector<CaloriesDetailActivity> {
    @DexIgnore
    public static void a(CaloriesDetailActivity caloriesDetailActivity, CaloriesDetailPresenter caloriesDetailPresenter) {
        caloriesDetailActivity.B = caloriesDetailPresenter;
    }
}
