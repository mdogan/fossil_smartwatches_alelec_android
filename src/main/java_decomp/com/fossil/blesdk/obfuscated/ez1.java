package com.fossil.blesdk.obfuscated;

import android.util.Base64;
import java.security.KeyPair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ez1 {
    @DexIgnore
    public /* final */ KeyPair a;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore
    public ez1(KeyPair keyPair, long j) {
        this.a = keyPair;
        this.b = j;
    }

    @DexIgnore
    public final KeyPair a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return Base64.encodeToString(this.a.getPublic().getEncoded(), 11);
    }

    @DexIgnore
    public final String c() {
        return Base64.encodeToString(this.a.getPrivate().getEncoded(), 11);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof ez1)) {
            return false;
        }
        ez1 ez1 = (ez1) obj;
        if (this.b != ez1.b || !this.a.getPublic().equals(ez1.a.getPublic()) || !this.a.getPrivate().equals(ez1.a.getPrivate())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int hashCode() {
        return ak0.a(this.a.getPublic(), this.a.getPrivate(), Long.valueOf(this.b));
    }
}
