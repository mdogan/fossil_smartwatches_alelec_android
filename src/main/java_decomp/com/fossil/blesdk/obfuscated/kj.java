package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.work.ExistingWorkPolicy;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class kj {
    @DexIgnore
    @Deprecated
    public static kj a() {
        uj a = uj.a();
        if (a != null) {
            return a;
        }
        throw new IllegalStateException("WorkManager is not initialized properly.  The most likely cause is that you disabled WorkManagerInitializer in your manifest but forgot to call WorkManager#initialize in your Application#onCreate or a ContentProvider.");
    }

    @DexIgnore
    public abstract gj a(String str, ExistingWorkPolicy existingWorkPolicy, List<fj> list);

    @DexIgnore
    public static void a(Context context, yi yiVar) {
        uj.a(context, yiVar);
    }

    @DexIgnore
    public gj a(String str, ExistingWorkPolicy existingWorkPolicy, fj fjVar) {
        return a(str, existingWorkPolicy, (List<fj>) Collections.singletonList(fjVar));
    }
}
