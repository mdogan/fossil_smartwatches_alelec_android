package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.BaseActivity;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wg3 extends as2 implements vg3, xs3.g {
    @DexIgnore
    public static /* final */ a m; // = new a((rd4) null);
    @DexIgnore
    public ur3<p82> j;
    @DexIgnore
    public ug3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final wg3 a() {
            return new wg3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wg3 e;

        @DexIgnore
        public b(wg3 wg3) {
            this.e = wg3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wg3 e;

        @DexIgnore
        public c(wg3 wg3) {
            this.e = wg3;
        }

        @DexIgnore
        public final void onClick(View view) {
            be4 be4 = be4.a;
            Locale a = tm2.a();
            wd4.a((Object) a, "LanguageHelper.getLocale()");
            Object[] objArr = {a.getLanguage()};
            String format = String.format("https://help.wearablessupport.com/hc/%s/articles/210312606", Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Privacy Policy URL = " + format);
            this.e.U(format);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wg3 e;

        @DexIgnore
        public d(wg3 wg3) {
            this.e = wg3;
        }

        @DexIgnore
        public final void onClick(View view) {
            be4 be4 = be4.a;
            Locale a = tm2.a();
            wd4.a((Object) a, "LanguageHelper.getLocale()");
            Object[] objArr = {a.getLanguage()};
            String format = String.format("https://help.wearablessupport.com/hc/%s/articles/210312186", Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Term Of Use URL = " + format);
            this.e.U(format);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wg3 e;

        @DexIgnore
        public e(wg3 wg3) {
            this.e = wg3;
        }

        @DexIgnore
        public final void onClick(View view) {
            be4 be4 = be4.a;
            Locale a = tm2.a();
            wd4.a((Object) a, "LanguageHelper.getLocale()");
            Object[] objArr = {a.getLanguage()};
            String format = String.format("https://c.fossil.com/web/open-source-licenses", Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Open Source Licenses URL = " + format);
            this.e.U(format);
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void U(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), ji3.n.a());
    }

    @DexIgnore
    public void n() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        p82 p82 = (p82) ra.a(LayoutInflater.from(getContext()), R.layout.fragment_about, (ViewGroup) null, false, O0());
        this.j = new ur3<>(this, p82);
        wd4.a((Object) p82, "binding");
        return p82.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        ug3 ug3 = this.k;
        if (ug3 != null) {
            ug3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ug3 ug3 = this.k;
        if (ug3 != null) {
            ug3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<p82> ur3 = this.j;
        if (ur3 != null) {
            p82 a2 = ur3.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                a2.s.setOnClickListener(new c(this));
                a2.t.setOnClickListener(new d(this));
                a2.r.setOnClickListener(new e(this));
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(ug3 ug3) {
        wd4.b(ug3, "presenter");
        tt1.a(ug3);
        wd4.a((Object) ug3, "Preconditions.checkNotNull(presenter)");
        this.k = ug3;
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wd4.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AboutFragment", "Inside .onDialogFragmentResult with TAG=" + str);
        FragmentActivity activity = getActivity();
        if (!(activity instanceof BaseActivity)) {
            activity = null;
        }
        BaseActivity baseActivity = (BaseActivity) activity;
        if (baseActivity != null) {
            baseActivity.a(str, i, intent);
        }
    }
}
