package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.helper.AnalyticsHelper;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q42 implements Factory<AnalyticsHelper> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public q42(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static q42 a(o42 o42) {
        return new q42(o42);
    }

    @DexIgnore
    public static AnalyticsHelper b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static AnalyticsHelper c(o42 o42) {
        AnalyticsHelper a2 = o42.a();
        o44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public AnalyticsHelper get() {
        return b(this.a);
    }
}
