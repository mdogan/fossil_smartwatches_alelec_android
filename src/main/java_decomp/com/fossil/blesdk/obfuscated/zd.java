package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.md;
import com.fossil.blesdk.obfuscated.pd;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zd<K, A, B> extends pd<K, B> {
    @DexIgnore
    public /* final */ pd<K, A> a;
    @DexIgnore
    public /* final */ m3<List<A>, List<B>> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends pd.c<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ pd.c a;

        @DexIgnore
        public a(pd.c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public void a(List<A> list, K k, K k2) {
            this.a.a(md.convert(zd.this.b, list), k, k2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends pd.a<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ pd.a a;

        @DexIgnore
        public b(pd.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public void a(List<A> list, K k) {
            this.a.a(md.convert(zd.this.b, list), k);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends pd.a<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ pd.a a;

        @DexIgnore
        public c(pd.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public void a(List<A> list, K k) {
            this.a.a(md.convert(zd.this.b, list), k);
        }
    }

    @DexIgnore
    public zd(pd<K, A> pdVar, m3<List<A>, List<B>> m3Var) {
        this.a = pdVar;
        this.b = m3Var;
    }

    @DexIgnore
    public void addInvalidatedCallback(md.c cVar) {
        this.a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    public void loadAfter(pd.f<K> fVar, pd.a<K, B> aVar) {
        this.a.loadAfter(fVar, new c(aVar));
    }

    @DexIgnore
    public void loadBefore(pd.f<K> fVar, pd.a<K, B> aVar) {
        this.a.loadBefore(fVar, new b(aVar));
    }

    @DexIgnore
    public void loadInitial(pd.e<K> eVar, pd.c<K, B> cVar) {
        this.a.loadInitial(eVar, new a(cVar));
    }

    @DexIgnore
    public void removeInvalidatedCallback(md.c cVar) {
        this.a.removeInvalidatedCallback(cVar);
    }
}
