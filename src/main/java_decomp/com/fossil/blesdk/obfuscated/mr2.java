package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.google.maps.DirectionsApi;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixElement;
import com.google.maps.model.DistanceMatrixElementStatus;
import com.google.maps.model.Duration;
import com.google.maps.model.LatLng;
import com.google.maps.model.TrafficModel;
import com.google.maps.model.TravelMode;
import com.google.maps.model.Unit;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.manager.SoLibraryLoader;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mr2 {
    @DexIgnore
    public static /* final */ String a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
        String simpleName = mr2.class.getSimpleName();
        wd4.a((Object) simpleName, "DurationUtils::class.java.simpleName");
        a = simpleName;
    }
    */

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0027, code lost:
        if (r0 != null) goto L_0x002c;
     */
    @DexIgnore
    public final Object a(String str, TravelMode travelMode, boolean z, double d, double d2, kc4<? super Long> kc4) {
        String str2;
        FLogger.INSTANCE.getLocal().d(a, "executeUseCase");
        GeoApiContext.Builder builder = new GeoApiContext.Builder();
        Access a2 = new SoLibraryLoader().a((Context) PortfolioApp.W.c());
        if (a2 != null) {
            str2 = a2.getN();
        }
        str2 = "";
        DistanceMatrixApiRequest departureTime = DistanceMatrixApi.newRequest(builder.apiKey(str2).build()).origins(new LatLng(d, d2)).destinations(str).mode(travelMode).units(Unit.IMPERIAL).trafficModel(TrafficModel.BEST_GUESS).departureTime(new DateTime(System.currentTimeMillis()));
        if (z) {
            departureTime.avoid(DirectionsApi.RouteRestriction.TOLLS);
        }
        try {
            DistanceMatrix distanceMatrix = (DistanceMatrix) departureTime.await();
            DistanceMatrixElement distanceMatrixElement = distanceMatrix.rows[0].elements[0];
            Duration duration = null;
            DistanceMatrixElementStatus distanceMatrixElementStatus = distanceMatrixElement != null ? distanceMatrixElement.status : null;
            if (distanceMatrixElementStatus == null || distanceMatrixElementStatus != DistanceMatrixElementStatus.OK) {
                return pc4.a(-1);
            }
            DistanceMatrixElement distanceMatrixElement2 = distanceMatrix.rows[0].elements[0];
            Duration duration2 = distanceMatrixElement2 != null ? distanceMatrixElement2.durationInTraffic : null;
            if (duration2 != null) {
                return pc4.a(duration2.inSeconds);
            }
            DistanceMatrixElement distanceMatrixElement3 = distanceMatrix.rows[0].elements[0];
            if (distanceMatrixElement3 != null) {
                duration = distanceMatrixElement3.duration;
            }
            if (duration != null) {
                return pc4.a(duration.inSeconds);
            }
            return pc4.a(-1);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a;
            local.d(str3, "Exception ex=" + e);
            e.printStackTrace();
        }
    }
}
