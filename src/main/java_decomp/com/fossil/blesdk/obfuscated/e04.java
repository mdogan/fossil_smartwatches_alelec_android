package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.fossil.blesdk.obfuscated.c04;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class e04 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ c04.b e;

    @DexIgnore
    public e04(c04.b bVar) {
        this.e = bVar;
    }

    @DexIgnore
    public void run() {
        if (c04.e != null && !this.e.e) {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", "WXStat trigger onForeground");
            j04.a(this.e.g, "onForeground_WX", (Properties) null);
            boolean unused = this.e.e = true;
        }
    }
}
