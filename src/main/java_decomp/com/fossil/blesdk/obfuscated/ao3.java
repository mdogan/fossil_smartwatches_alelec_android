package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ao3 implements Factory<ArrayList<Integer>> {
    @DexIgnore
    public static ArrayList<Integer> a(zn3 zn3) {
        ArrayList<Integer> a = zn3.a();
        o44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
