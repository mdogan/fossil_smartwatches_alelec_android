package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gj4 extends ng4 {
    @DexIgnore
    public /* final */ ek4 e;

    @DexIgnore
    public gj4(ek4 ek4) {
        wd4.b(ek4, "node");
        this.e = ek4;
    }

    @DexIgnore
    public void a(Throwable th) {
        this.e.j();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        a((Throwable) obj);
        return cb4.a;
    }

    @DexIgnore
    public String toString() {
        return "RemoveOnCancel[" + this.e + ']';
    }
}
