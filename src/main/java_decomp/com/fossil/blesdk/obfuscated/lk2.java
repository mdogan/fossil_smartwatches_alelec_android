package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AppHelper;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lk2 implements MembersInjector<AppHelper> {
    @DexIgnore
    public static void a(AppHelper appHelper, fn2 fn2) {
        appHelper.a = fn2;
    }

    @DexIgnore
    public static void a(AppHelper appHelper, DeviceRepository deviceRepository) {
        appHelper.b = deviceRepository;
    }

    @DexIgnore
    public static void a(AppHelper appHelper, UserRepository userRepository) {
        appHelper.c = userRepository;
    }
}
