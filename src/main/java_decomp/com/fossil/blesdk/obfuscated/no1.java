package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class no1<TResult> implements ro1<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public un1<? super TResult> c;

    @DexIgnore
    public no1(Executor executor, un1<? super TResult> un1) {
        this.a = executor;
        this.c = un1;
    }

    @DexIgnore
    public final void onComplete(xn1<TResult> xn1) {
        if (xn1.e()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new oo1(this, xn1));
                }
            }
        }
    }
}
