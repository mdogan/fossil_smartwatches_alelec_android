package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.zv3;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.framed.ErrorCode;
import com.squareup.okhttp.internal.framed.HeadersMode;
import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bw3 implements Closeable {
    @DexIgnore
    public static /* final */ ExecutorService A; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), xv3.a("OkHttp FramedConnection", true));
    @DexIgnore
    public /* final */ Protocol e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ hw3 g;
    @DexIgnore
    public /* final */ Map<Integer, cw3> h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public long m;
    @DexIgnore
    public /* final */ ExecutorService n;
    @DexIgnore
    public Map<Integer, jw3> o;
    @DexIgnore
    public /* final */ kw3 p;
    @DexIgnore
    public long q;
    @DexIgnore
    public long r;
    @DexIgnore
    public /* final */ lw3 s;
    @DexIgnore
    public /* final */ lw3 t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public /* final */ nw3 v;
    @DexIgnore
    public /* final */ Socket w;
    @DexIgnore
    public /* final */ aw3 x;
    @DexIgnore
    public /* final */ i y;
    @DexIgnore
    public /* final */ Set<Integer> z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends sv3 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ ErrorCode g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str, Object[] objArr, int i, ErrorCode errorCode) {
            super(str, objArr);
            this.f = i;
            this.g = errorCode;
        }

        @DexIgnore
        public void b() {
            try {
                bw3.this.c(this.f, this.g);
            } catch (IOException unused) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends sv3 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ long g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, Object[] objArr, int i, long j) {
            super(str, objArr);
            this.f = i;
            this.g = j;
        }

        @DexIgnore
        public void b() {
            try {
                bw3.this.x.a(this.f, this.g);
            } catch (IOException unused) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends sv3 {
        @DexIgnore
        public /* final */ /* synthetic */ boolean f;
        @DexIgnore
        public /* final */ /* synthetic */ int g;
        @DexIgnore
        public /* final */ /* synthetic */ int h;
        @DexIgnore
        public /* final */ /* synthetic */ jw3 i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(String str, Object[] objArr, boolean z, int i2, int i3, jw3 jw3) {
            super(str, objArr);
            this.f = z;
            this.g = i2;
            this.h = i3;
            this.i = jw3;
        }

        @DexIgnore
        public void b() {
            try {
                bw3.this.a(this.f, this.g, this.h, this.i);
            } catch (IOException unused) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends sv3 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ List g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(String str, Object[] objArr, int i, List list) {
            super(str, objArr);
            this.f = i;
            this.g = list;
        }

        @DexIgnore
        public void b() {
            if (bw3.this.p.a(this.f, (List<dw3>) this.g)) {
                try {
                    bw3.this.x.a(this.f, ErrorCode.CANCEL);
                    synchronized (bw3.this) {
                        bw3.this.z.remove(Integer.valueOf(this.f));
                    }
                } catch (IOException unused) {
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends sv3 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ List g;
        @DexIgnore
        public /* final */ /* synthetic */ boolean h;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, Object[] objArr, int i2, List list, boolean z) {
            super(str, objArr);
            this.f = i2;
            this.g = list;
            this.h = z;
        }

        @DexIgnore
        public void b() {
            boolean a = bw3.this.p.a(this.f, this.g, this.h);
            if (a) {
                try {
                    bw3.this.x.a(this.f, ErrorCode.CANCEL);
                } catch (IOException unused) {
                    return;
                }
            }
            if (a || this.h) {
                synchronized (bw3.this) {
                    bw3.this.z.remove(Integer.valueOf(this.f));
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends sv3 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ vo4 g;
        @DexIgnore
        public /* final */ /* synthetic */ int h;
        @DexIgnore
        public /* final */ /* synthetic */ boolean i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(String str, Object[] objArr, int i2, vo4 vo4, int i3, boolean z) {
            super(str, objArr);
            this.f = i2;
            this.g = vo4;
            this.h = i3;
            this.i = z;
        }

        @DexIgnore
        public void b() {
            try {
                boolean a = bw3.this.p.a(this.f, this.g, this.h, this.i);
                if (a) {
                    bw3.this.x.a(this.f, ErrorCode.CANCEL);
                }
                if (a || this.i) {
                    synchronized (bw3.this) {
                        bw3.this.z.remove(Integer.valueOf(this.f));
                    }
                }
            } catch (IOException unused) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends sv3 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ ErrorCode g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(String str, Object[] objArr, int i, ErrorCode errorCode) {
            super(str, objArr);
            this.f = i;
            this.g = errorCode;
        }

        @DexIgnore
        public void b() {
            bw3.this.p.a(this.f, this.g);
            synchronized (bw3.this) {
                bw3.this.z.remove(Integer.valueOf(this.f));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h {
        @DexIgnore
        public String a;
        @DexIgnore
        public Socket b;
        @DexIgnore
        public hw3 c; // = hw3.a;
        @DexIgnore
        public Protocol d; // = Protocol.SPDY_3;
        @DexIgnore
        public kw3 e; // = kw3.a;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public h(String str, boolean z, Socket socket) throws IOException {
            this.a = str;
            this.f = z;
            this.b = socket;
        }

        @DexIgnore
        public h a(Protocol protocol) {
            this.d = protocol;
            return this;
        }

        @DexIgnore
        public bw3 a() throws IOException {
            return new bw3(this, (a) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class i extends sv3 implements zv3.a {
        @DexIgnore
        public zv3 f;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends sv3 {
            @DexIgnore
            public /* final */ /* synthetic */ cw3 f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, Object[] objArr, cw3 cw3) {
                super(str, objArr);
                this.f = cw3;
            }

            @DexIgnore
            public void b() {
                try {
                    bw3.this.g.a(this.f);
                } catch (IOException e) {
                    Logger logger = qv3.a;
                    Level level = Level.INFO;
                    logger.log(level, "StreamHandler failure for " + bw3.this.i, e);
                    try {
                        this.f.a(ErrorCode.PROTOCOL_ERROR);
                    } catch (IOException unused) {
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class b extends sv3 {
            @DexIgnore
            public /* final */ /* synthetic */ lw3 f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(String str, Object[] objArr, lw3 lw3) {
                super(str, objArr);
                this.f = lw3;
            }

            @DexIgnore
            public void b() {
                try {
                    bw3.this.x.a(this.f);
                } catch (IOException unused) {
                }
            }
        }

        @DexIgnore
        public /* synthetic */ i(bw3 bw3, a aVar) {
            this();
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(int i, int i2, int i3, boolean z) {
        }

        @DexIgnore
        public void a(boolean z, int i, xo4 xo4, int i2) throws IOException {
            if (bw3.this.c(i)) {
                bw3.this.a(i, xo4, i2, z);
                return;
            }
            cw3 b2 = bw3.this.b(i);
            if (b2 == null) {
                bw3.this.d(i, ErrorCode.INVALID_STREAM);
                xo4.skip((long) i2);
                return;
            }
            b2.a(xo4, i2);
            if (z) {
                b2.j();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
            r1 = com.squareup.okhttp.internal.framed.ErrorCode.PROTOCOL_ERROR;
            r0 = com.squareup.okhttp.internal.framed.ErrorCode.PROTOCOL_ERROR;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            r2 = r5.g;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x004a, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x004b, code lost:
            r4 = r2;
            r2 = r1;
            r1 = r4;
         */
        @DexIgnore
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x003b */
        public void b() {
            bw3 bw3;
            ErrorCode errorCode;
            ErrorCode errorCode2 = ErrorCode.INTERNAL_ERROR;
            try {
                this.f = bw3.this.v.a(ep4.a(ep4.b(bw3.this.w)), bw3.this.f);
                if (!bw3.this.f) {
                    this.f.q();
                }
                while (this.f.a(this)) {
                }
                errorCode = ErrorCode.NO_ERROR;
                errorCode2 = ErrorCode.CANCEL;
                try {
                    bw3 = bw3.this;
                } catch (IOException unused) {
                }
            } catch (IOException unused2) {
                errorCode = errorCode2;
            } catch (Throwable th) {
                Throwable th2 = th;
                ErrorCode errorCode3 = errorCode2;
                try {
                    bw3.this.a(errorCode3, errorCode2);
                } catch (IOException unused3) {
                }
                xv3.a((Closeable) this.f);
                throw th2;
            }
            bw3.a(errorCode, errorCode2);
            xv3.a((Closeable) this.f);
        }

        @DexIgnore
        public i() {
            super("OkHttp %s", bw3.this.i);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:31:0x008f, code lost:
            if (r14.failIfStreamPresent() == false) goto L_0x009c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0091, code lost:
            r0.c(com.squareup.okhttp.internal.framed.ErrorCode.PROTOCOL_ERROR);
            r8.g.e(r11);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x009b, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x009c, code lost:
            r0.a(r13, r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x009f, code lost:
            if (r10 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a1, code lost:
            r0.j();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
            return;
         */
        @DexIgnore
        public void a(boolean z, boolean z2, int i, int i2, List<dw3> list, HeadersMode headersMode) {
            if (bw3.this.c(i)) {
                bw3.this.a(i, list, z2);
                return;
            }
            synchronized (bw3.this) {
                if (!bw3.this.l) {
                    cw3 b2 = bw3.this.b(i);
                    if (b2 == null) {
                        if (headersMode.failIfStreamAbsent()) {
                            bw3.this.d(i, ErrorCode.INVALID_STREAM);
                        } else if (i > bw3.this.j) {
                            if (i % 2 != bw3.this.k % 2) {
                                cw3 cw3 = new cw3(i, bw3.this, z, z2, list);
                                int unused = bw3.this.j = i;
                                bw3.this.h.put(Integer.valueOf(i), cw3);
                                bw3.A.execute(new a("OkHttp %s stream %d", new Object[]{bw3.this.i, Integer.valueOf(i)}, cw3));
                            }
                        }
                    }
                }
            }
        }

        @DexIgnore
        public void a(int i, ErrorCode errorCode) {
            if (bw3.this.c(i)) {
                bw3.this.b(i, errorCode);
                return;
            }
            cw3 e = bw3.this.e(i);
            if (e != null) {
                e.d(errorCode);
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v14, types: [java.lang.Object[]] */
        /* JADX WARNING: Multi-variable type inference failed */
        public void a(boolean z, lw3 lw3) {
            cw3[] cw3Arr;
            long j;
            synchronized (bw3.this) {
                int c = bw3.this.t.c(65536);
                if (z) {
                    bw3.this.t.a();
                }
                bw3.this.t.a(lw3);
                if (bw3.this.z() == Protocol.HTTP_2) {
                    a(lw3);
                }
                int c2 = bw3.this.t.c(65536);
                cw3Arr = null;
                if (c2 == -1 || c2 == c) {
                    j = 0;
                } else {
                    j = (long) (c2 - c);
                    if (!bw3.this.u) {
                        bw3.this.h(j);
                        boolean unused = bw3.this.u = true;
                    }
                    if (!bw3.this.h.isEmpty()) {
                        cw3Arr = bw3.this.h.values().toArray(new cw3[bw3.this.h.size()]);
                    }
                }
            }
            if (cw3Arr != null && j != 0) {
                for (cw3 cw3 : cw3Arr) {
                    synchronized (cw3) {
                        cw3.a(j);
                    }
                }
            }
        }

        @DexIgnore
        public final void a(lw3 lw3) {
            bw3.A.execute(new b("OkHttp %s ACK Settings", new Object[]{bw3.this.i}, lw3));
        }

        @DexIgnore
        public void a(boolean z, int i, int i2) {
            if (z) {
                jw3 c = bw3.this.d(i);
                if (c != null) {
                    c.b();
                    return;
                }
                return;
            }
            bw3.this.b(true, i, i2, (jw3) null);
        }

        @DexIgnore
        public void a(int i, ErrorCode errorCode, ByteString byteString) {
            cw3[] cw3Arr;
            byteString.size();
            synchronized (bw3.this) {
                cw3Arr = (cw3[]) bw3.this.h.values().toArray(new cw3[bw3.this.h.size()]);
                boolean unused = bw3.this.l = true;
            }
            for (cw3 cw3 : cw3Arr) {
                if (cw3.c() > i && cw3.g()) {
                    cw3.d(ErrorCode.REFUSED_STREAM);
                    bw3.this.e(cw3.c());
                }
            }
        }

        @DexIgnore
        public void a(int i, long j) {
            if (i == 0) {
                synchronized (bw3.this) {
                    bw3.this.r += j;
                    bw3.this.notifyAll();
                }
                return;
            }
            cw3 b2 = bw3.this.b(i);
            if (b2 != null) {
                synchronized (b2) {
                    b2.a(j);
                }
            }
        }

        @DexIgnore
        public void a(int i, int i2, List<dw3> list) {
            bw3.this.a(i2, list);
        }
    }

    /*
    static {
        Class<bw3> cls = bw3.class;
    }
    */

    @DexIgnore
    public /* synthetic */ bw3(h hVar, a aVar) throws IOException {
        this(hVar);
    }

    @DexIgnore
    public synchronized boolean A() {
        return this.m != ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
    }

    @DexIgnore
    public void B() throws IOException {
        this.x.p();
        this.x.b(this.s);
        int c2 = this.s.c(65536);
        if (c2 != 65536) {
            this.x.a(0, (long) (c2 - 65536));
        }
    }

    @DexIgnore
    public void close() throws IOException {
        a(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
    }

    @DexIgnore
    public void flush() throws IOException {
        this.x.flush();
    }

    @DexIgnore
    public synchronized long y() {
        return this.m;
    }

    @DexIgnore
    public Protocol z() {
        return this.e;
    }

    @DexIgnore
    public bw3(h hVar) throws IOException {
        this.h = new HashMap();
        this.m = System.nanoTime();
        this.q = 0;
        this.s = new lw3();
        this.t = new lw3();
        this.u = false;
        this.z = new LinkedHashSet();
        this.e = hVar.d;
        this.p = hVar.e;
        this.f = hVar.f;
        this.g = hVar.c;
        this.k = hVar.f ? 1 : 2;
        if (hVar.f && this.e == Protocol.HTTP_2) {
            this.k += 2;
        }
        boolean c2 = hVar.f;
        if (hVar.f) {
            this.s.a(7, 0, 16777216);
        }
        this.i = hVar.a;
        Protocol protocol = this.e;
        if (protocol == Protocol.HTTP_2) {
            this.v = new fw3();
            this.n = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), xv3.a(String.format("OkHttp %s Push Observer", new Object[]{this.i}), true));
            this.t.a(7, 0, 65535);
            this.t.a(5, 0, RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE);
        } else if (protocol == Protocol.SPDY_3) {
            this.v = new mw3();
            this.n = null;
        } else {
            throw new AssertionError(protocol);
        }
        this.r = (long) this.t.c(65536);
        this.w = hVar.b;
        this.x = this.v.a(ep4.a(ep4.a(hVar.b)), this.f);
        this.y = new i(this, (a) null);
        new Thread(this.y).start();
    }

    @DexIgnore
    public void d(int i2, ErrorCode errorCode) {
        A.submit(new a("OkHttp %s stream %d", new Object[]{this.i, Integer.valueOf(i2)}, i2, errorCode));
    }

    @DexIgnore
    public synchronized cw3 e(int i2) {
        cw3 remove;
        remove = this.h.remove(Integer.valueOf(i2));
        if (remove != null && this.h.isEmpty()) {
            a(true);
        }
        notifyAll();
        return remove;
    }

    @DexIgnore
    public void h(long j2) {
        this.r += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    @DexIgnore
    public void c(int i2, ErrorCode errorCode) throws IOException {
        this.x.a(i2, errorCode);
    }

    @DexIgnore
    public final synchronized jw3 d(int i2) {
        return this.o != null ? this.o.remove(Integer.valueOf(i2)) : null;
    }

    @DexIgnore
    public void c(int i2, long j2) {
        A.execute(new b("OkHttp Window Update %s stream %d", new Object[]{this.i, Integer.valueOf(i2)}, i2, j2));
    }

    @DexIgnore
    public synchronized cw3 b(int i2) {
        return this.h.get(Integer.valueOf(i2));
    }

    @DexIgnore
    public final boolean c(int i2) {
        return this.e == Protocol.HTTP_2 && i2 != 0 && (i2 & 1) == 0;
    }

    @DexIgnore
    public final void b(boolean z2, int i2, int i3, jw3 jw3) {
        A.execute(new c("OkHttp %s ping %08x%08x", new Object[]{this.i, Integer.valueOf(i2), Integer.valueOf(i3)}, z2, i2, i3, jw3));
    }

    @DexIgnore
    public final void b(int i2, ErrorCode errorCode) {
        this.n.execute(new g("OkHttp %s Push Reset[%s]", new Object[]{this.i, Integer.valueOf(i2)}, i2, errorCode));
    }

    @DexIgnore
    public final synchronized void a(boolean z2) {
        long j2;
        if (z2) {
            try {
                j2 = System.nanoTime();
            } catch (Throwable th) {
                throw th;
            }
        } else {
            j2 = ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        this.m = j2;
    }

    @DexIgnore
    public cw3 a(List<dw3> list, boolean z2, boolean z3) throws IOException {
        return a(0, list, z2, z3);
    }

    @DexIgnore
    public final cw3 a(int i2, List<dw3> list, boolean z2, boolean z3) throws IOException {
        int i3;
        cw3 cw3;
        boolean z4 = !z2;
        boolean z5 = !z3;
        synchronized (this.x) {
            synchronized (this) {
                if (!this.l) {
                    i3 = this.k;
                    this.k += 2;
                    cw3 = new cw3(i3, this, z4, z5, list);
                    if (cw3.h()) {
                        this.h.put(Integer.valueOf(i3), cw3);
                        a(false);
                    }
                } else {
                    throw new IOException("shutdown");
                }
            }
            if (i2 == 0) {
                this.x.a(z4, z5, i3, i2, list);
            } else if (!this.f) {
                this.x.a(i2, i3, list);
            } else {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            }
        }
        if (!z2) {
            this.x.flush();
        }
        return cw3;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:26|27|28) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r3 = java.lang.Math.min((int) java.lang.Math.min(r12, r8.r), r8.x.r());
        r6 = (long) r3;
        r8.r -= r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005f, code lost:
        throw new java.io.InterruptedIOException();
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x005a */
    public void a(int i2, boolean z2, vo4 vo4, long j2) throws IOException {
        int min;
        long j3;
        if (j2 == 0) {
            this.x.a(z2, i2, vo4, 0);
            return;
        }
        while (j2 > 0) {
            synchronized (this) {
                while (true) {
                    if (this.r > 0) {
                        break;
                    } else if (this.h.containsKey(Integer.valueOf(i2))) {
                        wait();
                    } else {
                        throw new IOException("stream closed");
                    }
                }
            }
            j2 -= j3;
            this.x.a(z2 && j2 == 0, i2, vo4, min);
        }
    }

    @DexIgnore
    public final void a(boolean z2, int i2, int i3, jw3 jw3) throws IOException {
        synchronized (this.x) {
            if (jw3 != null) {
                jw3.c();
            }
            this.x.a(z2, i2, i3);
        }
    }

    @DexIgnore
    public void a(ErrorCode errorCode) throws IOException {
        synchronized (this.x) {
            synchronized (this) {
                if (!this.l) {
                    this.l = true;
                    int i2 = this.j;
                    this.x.a(i2, errorCode, xv3.a);
                }
            }
        }
    }

    @DexIgnore
    public final void a(ErrorCode errorCode, ErrorCode errorCode2) throws IOException {
        int i2;
        cw3[] cw3Arr;
        jw3[] jw3Arr = null;
        try {
            a(errorCode);
            e = null;
        } catch (IOException e2) {
            e = e2;
        }
        synchronized (this) {
            if (!this.h.isEmpty()) {
                cw3Arr = (cw3[]) this.h.values().toArray(new cw3[this.h.size()]);
                this.h.clear();
                a(false);
            } else {
                cw3Arr = null;
            }
            if (this.o != null) {
                this.o = null;
                jw3Arr = (jw3[]) this.o.values().toArray(new jw3[this.o.size()]);
            }
        }
        if (cw3Arr != null) {
            IOException iOException = e;
            for (cw3 a2 : cw3Arr) {
                try {
                    a2.a(errorCode2);
                } catch (IOException e3) {
                    if (iOException != null) {
                        iOException = e3;
                    }
                }
            }
            e = iOException;
        }
        if (jw3Arr != null) {
            for (jw3 a3 : jw3Arr) {
                a3.a();
            }
        }
        try {
            this.x.close();
        } catch (IOException e4) {
            if (e == null) {
                e = e4;
            }
        }
        try {
            this.w.close();
        } catch (IOException e5) {
            e = e5;
        }
        if (e != null) {
            throw e;
        }
    }

    @DexIgnore
    public final void a(int i2, List<dw3> list) {
        synchronized (this) {
            if (this.z.contains(Integer.valueOf(i2))) {
                d(i2, ErrorCode.PROTOCOL_ERROR);
                return;
            }
            this.z.add(Integer.valueOf(i2));
            this.n.execute(new d("OkHttp %s Push Request[%s]", new Object[]{this.i, Integer.valueOf(i2)}, i2, list));
        }
    }

    @DexIgnore
    public final void a(int i2, List<dw3> list, boolean z2) {
        this.n.execute(new e("OkHttp %s Push Headers[%s]", new Object[]{this.i, Integer.valueOf(i2)}, i2, list, z2));
    }

    @DexIgnore
    public final void a(int i2, xo4 xo4, int i3, boolean z2) throws IOException {
        vo4 vo4 = new vo4();
        long j2 = (long) i3;
        xo4.g(j2);
        xo4.b(vo4, j2);
        if (vo4.B() == j2) {
            this.n.execute(new f("OkHttp %s Push Data[%s]", new Object[]{this.i, Integer.valueOf(i2)}, i2, vo4, i3, z2));
            return;
        }
        throw new IOException(vo4.B() + " != " + i3);
    }
}
