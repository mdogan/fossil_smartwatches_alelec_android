package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.Log;
import com.google.android.gms.dynamite.DynamiteModule;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fn0 {
    @DexIgnore
    public static volatile yl0 a;
    @DexIgnore
    public static /* final */ Object b; // = new Object();
    @DexIgnore
    public static Context c;

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0019, code lost:
        return;
     */
    @DexIgnore
    public static synchronized void a(Context context) {
        synchronized (fn0.class) {
            if (c != null) {
                Log.w("GoogleCertificates", "GoogleCertificates has been initialized already");
            } else if (context != null) {
                c = context.getApplicationContext();
            }
        }
    }

    @DexIgnore
    public static pn0 b(String str, hn0 hn0, boolean z, boolean z2) {
        try {
            if (a == null) {
                ck0.a(c);
                synchronized (b) {
                    if (a == null) {
                        a = zl0.a(DynamiteModule.a(c, DynamiteModule.j, "com.google.android.gms.googlecertificates").a("com.google.android.gms.common.GoogleCertificatesImpl"));
                    }
                }
            }
            ck0.a(c);
            try {
                if (a.a(new nn0(str, hn0, z, z2), vn0.a(c.getPackageManager()))) {
                    return pn0.c();
                }
                return pn0.a((Callable<String>) new gn0(z, str, hn0));
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
                return pn0.a("module call", e);
            }
        } catch (DynamiteModule.LoadingException e2) {
            Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e2);
            String valueOf = String.valueOf(e2.getMessage());
            return pn0.a(valueOf.length() != 0 ? "module init: ".concat(valueOf) : new String("module init: "), e2);
        }
    }

    @DexIgnore
    public static pn0 a(String str, hn0 hn0, boolean z, boolean z2) {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return b(str, hn0, z, z2);
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    @DexIgnore
    public static final /* synthetic */ String a(boolean z, String str, hn0 hn0) throws Exception {
        boolean z2 = true;
        if (z || !b(str, hn0, true, false).a) {
            z2 = false;
        }
        return pn0.a(str, hn0, z, z2);
    }
}
