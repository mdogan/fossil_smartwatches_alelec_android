package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hb1 extends fb1<gb1, gb1> {
    @DexIgnore
    public static void a(Object obj, gb1 gb1) {
        ((u81) obj).zzbyf = gb1;
    }

    @DexIgnore
    public final boolean a(na1 na1) {
        return false;
    }

    @DexIgnore
    public final /* synthetic */ int b(Object obj) {
        return ((gb1) obj).b();
    }

    @DexIgnore
    public final /* synthetic */ Object c(Object obj, Object obj2) {
        gb1 gb1 = (gb1) obj;
        gb1 gb12 = (gb1) obj2;
        if (gb12.equals(gb1.d())) {
            return gb1;
        }
        return gb1.a(gb1, gb12);
    }

    @DexIgnore
    public final /* synthetic */ Object d(Object obj) {
        gb1 gb1 = ((u81) obj).zzbyf;
        if (gb1 != gb1.d()) {
            return gb1;
        }
        gb1 e = gb1.e();
        a(obj, e);
        return e;
    }

    @DexIgnore
    public final /* synthetic */ int e(Object obj) {
        return ((gb1) obj).c();
    }

    @DexIgnore
    public final void f(Object obj) {
        ((u81) obj).zzbyf.a();
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, tb1 tb1) throws IOException {
        ((gb1) obj).b(tb1);
    }

    @DexIgnore
    public final /* synthetic */ void b(Object obj, tb1 tb1) throws IOException {
        ((gb1) obj).a(tb1);
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, Object obj2) {
        a(obj, (gb1) obj2);
    }

    @DexIgnore
    public final /* synthetic */ Object c(Object obj) {
        return ((u81) obj).zzbyf;
    }

    @DexIgnore
    public final /* synthetic */ Object a(Object obj) {
        gb1 gb1 = (gb1) obj;
        gb1.a();
        return gb1;
    }

    @DexIgnore
    public final /* synthetic */ void b(Object obj, Object obj2) {
        a(obj, (gb1) obj2);
    }

    @DexIgnore
    public final /* synthetic */ Object a() {
        return gb1.e();
    }

    @DexIgnore
    public final /* synthetic */ void b(Object obj, int i, long j) {
        ((gb1) obj).a((i << 3) | 1, (Object) Long.valueOf(j));
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, int i, Object obj2) {
        ((gb1) obj).a((i << 3) | 3, (Object) (gb1) obj2);
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, int i, zzte zzte) {
        ((gb1) obj).a((i << 3) | 2, (Object) zzte);
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, int i, int i2) {
        ((gb1) obj).a((i << 3) | 5, (Object) Integer.valueOf(i2));
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, int i, long j) {
        ((gb1) obj).a(i << 3, (Object) Long.valueOf(j));
    }
}
