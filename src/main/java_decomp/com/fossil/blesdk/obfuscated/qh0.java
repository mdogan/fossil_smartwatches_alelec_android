package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.ee0;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qh0 {
    @DexIgnore
    public static /* final */ Status d; // = new Status(8, "The connection to Google Play services was lost");
    @DexIgnore
    public static /* final */ BasePendingResult<?>[] e; // = new BasePendingResult[0];
    @DexIgnore
    public /* final */ Set<BasePendingResult<?>> a; // = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
    @DexIgnore
    public /* final */ th0 b; // = new rh0(this);
    @DexIgnore
    public /* final */ Map<ee0.c<?>, ee0.f> c;

    @DexIgnore
    public qh0(Map<ee0.c<?>, ee0.f> map) {
        this.c = map;
    }

    @DexIgnore
    public final void a(BasePendingResult<? extends ne0> basePendingResult) {
        this.a.add(basePendingResult);
        basePendingResult.a(this.b);
    }

    @DexIgnore
    public final void b() {
        for (BasePendingResult b2 : (BasePendingResult[]) this.a.toArray(e)) {
            b2.b(d);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.fossil.blesdk.obfuscated.oe0, com.fossil.blesdk.obfuscated.rh0, com.fossil.blesdk.obfuscated.wi0, com.fossil.blesdk.obfuscated.th0] */
    public final void a() {
        for (BasePendingResult basePendingResult : (BasePendingResult[]) this.a.toArray(e)) {
            Object r5 = 0;
            basePendingResult.a((th0) r5);
            if (basePendingResult.e() != null) {
                basePendingResult.a(r5);
                IBinder m = this.c.get(((ue0) basePendingResult).i()).m();
                if (basePendingResult.d()) {
                    basePendingResult.a((th0) new sh0(basePendingResult, r5, m, r5));
                } else if (m == null || !m.isBinderAlive()) {
                    basePendingResult.a((th0) r5);
                    basePendingResult.a();
                    r5.a(basePendingResult.e().intValue());
                } else {
                    sh0 sh0 = new sh0(basePendingResult, r5, m, r5);
                    basePendingResult.a((th0) sh0);
                    try {
                        m.linkToDeath(sh0, 0);
                    } catch (RemoteException unused) {
                        basePendingResult.a();
                        r5.a(basePendingResult.e().intValue());
                    }
                }
                this.a.remove(basePendingResult);
            } else if (basePendingResult.f()) {
                this.a.remove(basePendingResult);
            }
        }
    }
}
