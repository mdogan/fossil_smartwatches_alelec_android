package com.fossil.blesdk.obfuscated;

import android.os.Binder;
import android.os.Process;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ky1 extends Binder {
    @DexIgnore
    public /* final */ fy1 e;

    @DexIgnore
    public ky1(fy1 fy1) {
        this.e = fy1;
    }

    @DexIgnore
    public final void a(iy1 iy1) {
        if (Binder.getCallingUid() == Process.myUid()) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "service received new intent via bind strategy");
            }
            if (this.e.c(iy1.a)) {
                iy1.a();
                return;
            }
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "intent being queued for bg execution");
            }
            this.e.e.execute(new ly1(this, iy1));
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
}
