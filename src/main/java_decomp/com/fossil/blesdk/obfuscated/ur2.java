package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.ui.BaseActivity;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ur2 extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((rd4) null);
    @DexIgnore
    public /* final */ MFLoginWechatManager d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ur2.e;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ WeakReference<BaseActivity> a;

        @DexIgnore
        public b(WeakReference<BaseActivity> weakReference) {
            wd4.b(weakReference, "activityContext");
            this.a = weakReference;
        }

        @DexIgnore
        public final WeakReference<BaseActivity> a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, vd0 vd0) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ SignUpSocialAuth a;

        @DexIgnore
        public d(SignUpSocialAuth signUpSocialAuth) {
            wd4.b(signUpSocialAuth, "auth");
            this.a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = ur2.class.getSimpleName();
        wd4.a((Object) simpleName, "LoginWechatUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public ur2(MFLoginWechatManager mFLoginWechatManager) {
        wd4.b(mFLoginWechatManager, "mLoginWechatManager");
        this.d = mFLoginWechatManager;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements mn2 {
        @DexIgnore
        public /* final */ /* synthetic */ ur2 a;

        @DexIgnore
        public e(ur2 ur2) {
            this.a = ur2;
        }

        @DexIgnore
        public void a(SignUpSocialAuth signUpSocialAuth) {
            wd4.b(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ur2.f.a();
            local.d(a2, "Inside .onLoginSuccess with result=" + signUpSocialAuth);
            this.a.a(new d(signUpSocialAuth));
        }

        @DexIgnore
        public void a(int i, vd0 vd0, String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ur2.f.a();
            local.d(a2, "Inside .onLoginFailed with errorCode=" + i + ", connectionResult=" + vd0);
            this.a.a(new c(i, vd0));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0031 A[Catch:{ Exception -> 0x0059 }] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036 A[Catch:{ Exception -> 0x0059 }] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0039 A[Catch:{ Exception -> 0x0059 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0055 A[SYNTHETIC, Splitter:B:18:0x0055] */
    public Object a(b bVar, kc4<Object> kc4) {
        String str;
        WeakReference<BaseActivity> a2;
        try {
            FLogger.INSTANCE.getLocal().d(e, "running UseCase");
            MFLoginWechatManager mFLoginWechatManager = this.d;
            Access a3 = new SoLibraryLoader().a((Context) PortfolioApp.W.c());
            if (a3 != null) {
                str = a3.getD();
                if (str != null) {
                    mFLoginWechatManager.b(str);
                    MFLoginWechatManager mFLoginWechatManager2 = this.d;
                    a2 = bVar == null ? bVar.a() : null;
                    if (a2 == null) {
                        Object obj = a2.get();
                        if (obj != null) {
                            wd4.a(obj, "requestValues?.activityContext!!.get()!!");
                            mFLoginWechatManager2.a((Activity) obj, new e(this));
                            return cb4.a;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.a();
                    throw null;
                }
            }
            str = "";
            mFLoginWechatManager.b(str);
            MFLoginWechatManager mFLoginWechatManager22 = this.d;
            if (bVar == null) {
            }
            if (a2 == null) {
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = e;
            local.d(str2, "Inside .run failed with exception=" + e2);
            return new c(600, (vd0) null);
        }
    }
}
