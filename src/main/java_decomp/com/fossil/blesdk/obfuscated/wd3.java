package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.blesdk.obfuscated.xr2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepWeekChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wd3 extends as2 implements vd3 {
    @DexIgnore
    public ur3<zf2> j;
    @DexIgnore
    public ud3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "SleepOverviewWeekFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onCreateView");
        this.j = new ur3<>(this, (zf2) ra.a(layoutInflater, R.layout.fragment_sleep_overview_week, viewGroup, false, O0()));
        ur3<zf2> ur3 = this.j;
        if (ur3 != null) {
            zf2 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onResume");
        ud3 ud3 = this.k;
        if (ud3 != null) {
            ud3.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onStop");
        ud3 ud3 = this.k;
        if (ud3 != null) {
            ud3.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(xr2 xr2) {
        wd4.b(xr2, "baseModel");
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "showWeekDetails");
        ur3<zf2> ur3 = this.j;
        if (ur3 != null) {
            zf2 a2 = ur3.a();
            if (a2 != null) {
                OverviewSleepWeekChart overviewSleepWeekChart = a2.q;
                if (overviewSleepWeekChart != null) {
                    new ArrayList();
                    BarChart.c cVar = (BarChart.c) xr2;
                    cVar.b(xr2.a.a(cVar.c()));
                    xr2.a aVar = xr2.a;
                    wd4.a((Object) overviewSleepWeekChart, "it");
                    Context context = overviewSleepWeekChart.getContext();
                    wd4.a((Object) context, "it.context");
                    BarChart.a((BarChart) overviewSleepWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
                    overviewSleepWeekChart.a(xr2);
                }
            }
        }
    }

    @DexIgnore
    public void a(ud3 ud3) {
        wd4.b(ud3, "presenter");
        this.k = ud3;
    }
}
