package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.os.UserManager;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class o61 {
    @DexIgnore
    public static volatile UserManager a;
    @DexIgnore
    public static volatile boolean b; // = (!a());

    @DexIgnore
    public static boolean a() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @DexIgnore
    @TargetApi(24)
    public static boolean b(Context context) {
        boolean z = b;
        if (!z) {
            boolean z2 = z;
            int i = 1;
            while (true) {
                if (i > 2) {
                    break;
                }
                UserManager c = c(context);
                if (c == null) {
                    b = true;
                    return true;
                }
                try {
                    if (!c.isUserUnlocked()) {
                        if (c.isUserRunning(Process.myUserHandle())) {
                            z2 = false;
                            b = z2;
                        }
                    }
                    z2 = true;
                    b = z2;
                } catch (NullPointerException e) {
                    Log.w("DirectBootUtils", "Failed to check if user is unlocked", e);
                    a = null;
                    i++;
                }
            }
            z = z2;
            if (z) {
                a = null;
            }
        }
        return z;
    }

    @DexIgnore
    @TargetApi(24)
    public static UserManager c(Context context) {
        UserManager userManager = a;
        if (userManager == null) {
            synchronized (o61.class) {
                userManager = a;
                if (userManager == null) {
                    UserManager userManager2 = (UserManager) context.getSystemService(UserManager.class);
                    a = userManager2;
                    userManager = userManager2;
                }
            }
        }
        return userManager;
    }

    @DexIgnore
    public static boolean a(Context context) {
        return !a() || b(context);
    }
}
