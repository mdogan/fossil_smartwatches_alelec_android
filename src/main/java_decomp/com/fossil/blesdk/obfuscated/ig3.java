package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ig3 extends as2 implements hg3 {
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public gg3 j;
    @DexIgnore
    public t62 k;
    @DexIgnore
    public ur3<hd2> l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ig3 a() {
            return new ig3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewPager.i {
        @DexIgnore
        public void a(int i) {
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
        }

        @DexIgnore
        public void b(int i) {
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void g(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i);
        if (isActive()) {
            ur3<hd2> ur3 = this.l;
            if (ur3 != null) {
                hd2 a2 = ur3.a();
                if (a2 != null) {
                    ProgressBar progressBar = a2.t;
                    if (progressBar != null) {
                        progressBar.setProgress(i);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void i(List<? extends Explore> list) {
        wd4.b(list, "data");
        ur3<hd2> ur3 = this.l;
        if (ur3 != null) {
            hd2 a2 = ur3.a();
            if (a2 != null) {
                if (FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.W.c().e())) {
                    FlexibleTextView flexibleTextView = a2.q;
                    wd4.a((Object) flexibleTextView, "it.ftvUpdate");
                    flexibleTextView.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Title__ExploreYourWatch));
                } else {
                    FlexibleTextView flexibleTextView2 = a2.q;
                    wd4.a((Object) flexibleTextView2, "it.ftvUpdate");
                    flexibleTextView2.setText(tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Title__ExploreYourWatch));
                }
            }
            t62 t62 = this.k;
            if (t62 != null) {
                t62.a(list);
            } else {
                wd4.d("mAdapterUpdateFirmware");
                throw null;
            }
        } else {
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        hd2 hd2 = (hd2) ra.a(layoutInflater, R.layout.fragment_home_update_firmware, viewGroup, false, O0());
        this.l = new ur3<>(this, hd2);
        wd4.a((Object) hd2, "binding");
        return hd2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        gg3 gg3 = this.j;
        if (gg3 == null) {
            return;
        }
        if (gg3 != null) {
            gg3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        gg3 gg3 = this.j;
        if (gg3 == null) {
            return;
        }
        if (gg3 != null) {
            gg3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        this.k = new t62(new ArrayList());
        ur3<hd2> ur3 = this.l;
        if (ur3 != null) {
            hd2 a2 = ur3.a();
            if (a2 != null) {
                ProgressBar progressBar = a2.t;
                wd4.a((Object) progressBar, "binding.progressUpdate");
                progressBar.setMax(1000);
                FlexibleTextView flexibleTextView = a2.r;
                wd4.a((Object) flexibleTextView, "binding.ftvUpdateWarning");
                flexibleTextView.setVisibility(0);
                RecyclerViewPager recyclerViewPager = a2.u;
                wd4.a((Object) recyclerViewPager, "binding.rvpTutorial");
                recyclerViewPager.setLayoutManager(new LinearLayoutManager(getActivity(), 0, false));
                RecyclerViewPager recyclerViewPager2 = a2.u;
                wd4.a((Object) recyclerViewPager2, "binding.rvpTutorial");
                t62 t62 = this.k;
                if (t62 != null) {
                    recyclerViewPager2.setAdapter(t62);
                    a2.s.a((RecyclerView) a2.u, 0);
                    a2.s.setOnPageChangeListener(new b());
                    return;
                }
                wd4.d("mAdapterUpdateFirmware");
                throw null;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(gg3 gg3) {
        wd4.b(gg3, "presenter");
        this.j = gg3;
    }
}
