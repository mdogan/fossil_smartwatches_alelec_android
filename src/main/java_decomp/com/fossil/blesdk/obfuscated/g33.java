package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g33 implements Factory<CommuteTimeSettingsPresenter> {
    @DexIgnore
    public static CommuteTimeSettingsPresenter a(v23 v23, fn2 fn2, UserRepository userRepository) {
        return new CommuteTimeSettingsPresenter(v23, fn2, userRepository);
    }
}
