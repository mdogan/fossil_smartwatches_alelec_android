package com.fossil.blesdk.obfuscated;

import com.google.gson.JsonIOException;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k02 {
    @DexIgnore
    public /* final */ Map<Type, tz1<?>> a;
    @DexIgnore
    public /* final */ x02 b; // = x02.a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements n02<T> {
        @DexIgnore
        public a(k02 k02) {
        }

        @DexIgnore
        public T a() {
            return new ConcurrentHashMap();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements n02<T> {
        @DexIgnore
        public b(k02 k02) {
        }

        @DexIgnore
        public T a() {
            return new TreeMap();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements n02<T> {
        @DexIgnore
        public c(k02 k02) {
        }

        @DexIgnore
        public T a() {
            return new LinkedHashMap();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements n02<T> {
        @DexIgnore
        public d(k02 k02) {
        }

        @DexIgnore
        public T a() {
            return new LinkedTreeMap();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements n02<T> {
        @DexIgnore
        public /* final */ r02 a; // = r02.a();
        @DexIgnore
        public /* final */ /* synthetic */ Class b;
        @DexIgnore
        public /* final */ /* synthetic */ Type c;

        @DexIgnore
        public e(k02 k02, Class cls, Type type) {
            this.b = cls;
            this.c = type;
        }

        @DexIgnore
        public T a() {
            try {
                return this.a.a(this.b);
            } catch (Exception e) {
                throw new RuntimeException("Unable to invoke no-args constructor for " + this.c + ". Registering an InstanceCreator with Gson for this type may fix this problem.", e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements n02<T> {
        @DexIgnore
        public /* final */ /* synthetic */ tz1 a;
        @DexIgnore
        public /* final */ /* synthetic */ Type b;

        @DexIgnore
        public f(k02 k02, tz1 tz1, Type type) {
            this.a = tz1;
            this.b = type;
        }

        @DexIgnore
        public T a() {
            return this.a.createInstance(this.b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements n02<T> {
        @DexIgnore
        public /* final */ /* synthetic */ tz1 a;
        @DexIgnore
        public /* final */ /* synthetic */ Type b;

        @DexIgnore
        public g(k02 k02, tz1 tz1, Type type) {
            this.a = tz1;
            this.b = type;
        }

        @DexIgnore
        public T a() {
            return this.a.createInstance(this.b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements n02<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor a;

        @DexIgnore
        public h(k02 k02, Constructor constructor) {
            this.a = constructor;
        }

        @DexIgnore
        public T a() {
            try {
                return this.a.newInstance((Object[]) null);
            } catch (InstantiationException e) {
                throw new RuntimeException("Failed to invoke " + this.a + " with no args", e);
            } catch (InvocationTargetException e2) {
                throw new RuntimeException("Failed to invoke " + this.a + " with no args", e2.getTargetException());
            } catch (IllegalAccessException e3) {
                throw new AssertionError(e3);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements n02<T> {
        @DexIgnore
        public i(k02 k02) {
        }

        @DexIgnore
        public T a() {
            return new TreeSet();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements n02<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Type a;

        @DexIgnore
        public j(k02 k02, Type type) {
            this.a = type;
        }

        @DexIgnore
        public T a() {
            Type type = this.a;
            if (type instanceof ParameterizedType) {
                Type type2 = ((ParameterizedType) type).getActualTypeArguments()[0];
                if (type2 instanceof Class) {
                    return EnumSet.noneOf((Class) type2);
                }
                throw new JsonIOException("Invalid EnumSet type: " + this.a.toString());
            }
            throw new JsonIOException("Invalid EnumSet type: " + this.a.toString());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class k implements n02<T> {
        @DexIgnore
        public k(k02 k02) {
        }

        @DexIgnore
        public T a() {
            return new LinkedHashSet();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class l implements n02<T> {
        @DexIgnore
        public l(k02 k02) {
        }

        @DexIgnore
        public T a() {
            return new ArrayDeque();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class m implements n02<T> {
        @DexIgnore
        public m(k02 k02) {
        }

        @DexIgnore
        public T a() {
            return new ArrayList();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class n implements n02<T> {
        @DexIgnore
        public n(k02 k02) {
        }

        @DexIgnore
        public T a() {
            return new ConcurrentSkipListMap();
        }
    }

    @DexIgnore
    public k02(Map<Type, tz1<?>> map) {
        this.a = map;
    }

    @DexIgnore
    public <T> n02<T> a(TypeToken<T> typeToken) {
        Type type = typeToken.getType();
        Class<? super T> rawType = typeToken.getRawType();
        tz1 tz1 = this.a.get(type);
        if (tz1 != null) {
            return new f(this, tz1, type);
        }
        tz1 tz12 = this.a.get(rawType);
        if (tz12 != null) {
            return new g(this, tz12, type);
        }
        n02<T> a2 = a(rawType);
        if (a2 != null) {
            return a2;
        }
        n02<T> a3 = a(type, rawType);
        if (a3 != null) {
            return a3;
        }
        return b(type, rawType);
    }

    @DexIgnore
    public final <T> n02<T> b(Type type, Class<? super T> cls) {
        return new e(this, cls, type);
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public final <T> n02<T> a(Class<? super T> cls) {
        try {
            Constructor<? super T> declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                this.b.a(declaredConstructor);
            }
            return new h(this, declaredConstructor);
        } catch (NoSuchMethodException unused) {
            return null;
        }
    }

    @DexIgnore
    public final <T> n02<T> a(Type type, Class<? super T> cls) {
        if (Collection.class.isAssignableFrom(cls)) {
            if (SortedSet.class.isAssignableFrom(cls)) {
                return new i(this);
            }
            if (EnumSet.class.isAssignableFrom(cls)) {
                return new j(this, type);
            }
            if (Set.class.isAssignableFrom(cls)) {
                return new k(this);
            }
            if (Queue.class.isAssignableFrom(cls)) {
                return new l(this);
            }
            return new m(this);
        } else if (!Map.class.isAssignableFrom(cls)) {
            return null;
        } else {
            if (ConcurrentNavigableMap.class.isAssignableFrom(cls)) {
                return new n(this);
            }
            if (ConcurrentMap.class.isAssignableFrom(cls)) {
                return new a(this);
            }
            if (SortedMap.class.isAssignableFrom(cls)) {
                return new b(this);
            }
            if (!(type instanceof ParameterizedType) || String.class.isAssignableFrom(TypeToken.get(((ParameterizedType) type).getActualTypeArguments()[0]).getRawType())) {
                return new d(this);
            }
            return new c(this);
        }
    }
}
