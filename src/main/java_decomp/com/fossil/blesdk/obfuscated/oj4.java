package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface oj4 {
    @DexIgnore
    long a();

    @DexIgnore
    Runnable a(Runnable runnable);

    @DexIgnore
    void a(Object obj, long j);

    @DexIgnore
    void a(Thread thread);

    @DexIgnore
    void b();

    @DexIgnore
    void c();

    @DexIgnore
    void d();

    @DexIgnore
    void e();
}
