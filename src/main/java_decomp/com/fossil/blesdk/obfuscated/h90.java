package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.FeatureError;
import com.fossil.blesdk.device.FeatureErrorCode;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.error.Error;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h90<V> extends w00<V, Error> {
    @DexIgnore
    public h90<V> d(jd4<? super Float, cb4> jd4) {
        wd4.b(jd4, "actionOnProgressChange");
        super.d(jd4);
        return this;
    }

    @DexIgnore
    public h90<V> f(jd4<? super cb4, cb4> jd4) {
        wd4.b(jd4, "actionOnFinal");
        super.a(jd4);
        return this;
    }

    @DexIgnore
    /* renamed from: g */
    public h90<V> c(jd4<? super Error, cb4> jd4) {
        wd4.b(jd4, "actionOnError");
        super.c(jd4);
        return this;
    }

    @DexIgnore
    /* renamed from: h */
    public h90<V> e(jd4<? super V, cb4> jd4) {
        wd4.b(jd4, "actionOnSuccess");
        super.e(jd4);
        return this;
    }

    @DexIgnore
    public h90<V> b(jd4<? super Error, cb4> jd4) {
        wd4.b(jd4, "actionOnCancel");
        super.b(jd4);
        return this;
    }

    @DexIgnore
    public final void e() {
        super.a(new FeatureError(FeatureErrorCode.INTERRUPTED, (Phase.Result) null, 2, (rd4) null));
    }
}
