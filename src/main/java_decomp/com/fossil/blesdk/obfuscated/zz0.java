package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class zz0 extends xz0<Status> {
    @DexIgnore
    public zz0(he0 he0) {
        super(he0);
    }

    @DexIgnore
    public /* synthetic */ ne0 a(Status status) {
        ck0.a(!status.L());
        return status;
    }
}
