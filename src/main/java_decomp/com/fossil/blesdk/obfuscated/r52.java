package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.WatchParamHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r52 implements Factory<WatchParamHelper> {
    @DexIgnore
    public /* final */ o42 a;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> b;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> c;

    @DexIgnore
    public r52(o42 o42, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        this.a = o42;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static r52 a(o42 o42, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        return new r52(o42, provider, provider2);
    }

    @DexIgnore
    public static WatchParamHelper b(o42 o42, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        return a(o42, provider.get(), provider2.get());
    }

    @DexIgnore
    public static WatchParamHelper a(o42 o42, DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        WatchParamHelper a2 = o42.a(deviceRepository, portfolioApp);
        o44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public WatchParamHelper get() {
        return b(this.a, this.b, this.c);
    }
}
