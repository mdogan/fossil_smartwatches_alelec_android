package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mx2 implements Factory<ix2> {
    @DexIgnore
    public static ix2 a(lx2 lx2) {
        ix2 a = lx2.a();
        o44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
