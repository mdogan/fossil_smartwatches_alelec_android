package com.fossil.blesdk.obfuscated;

import android.os.Looper;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y41 implements oc1 {
    @DexIgnore
    public final ie0<Status> a(he0 he0, sc1 sc1) {
        return he0.b(new a51(this, he0, sc1));
    }

    @DexIgnore
    public final ie0<Status> a(he0 he0, LocationRequest locationRequest, sc1 sc1) {
        ck0.a(Looper.myLooper(), (Object) "Calling thread must be a prepared Looper thread.");
        return he0.b(new z41(this, he0, locationRequest, sc1));
    }
}
