package com.fossil.blesdk.obfuscated;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ha0 {
    @DexIgnore
    public static /* final */ ha0 a; // = new ha0();

    @DexIgnore
    public final byte[] a(Integer num, JSONObject jSONObject) {
        wd4.b(jSONObject, "responseSettingJSONData");
        JSONObject jSONObject2 = new JSONObject();
        String str = num == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", num);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        String jSONObject4 = jSONObject2.toString();
        wd4.a((Object) jSONObject4, "deviceResponseJSONObject.toString()");
        Charset f = va0.y.f();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(f);
            wd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
}
