package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.google.android.gms.common.api.AvailabilityException;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qi0 implements sn1<Map<zh0<?>, String>> {
    @DexIgnore
    public /* final */ /* synthetic */ oi0 a;

    @DexIgnore
    public qi0(oi0 oi0) {
        this.a = oi0;
    }

    @DexIgnore
    public final void onComplete(xn1<Map<zh0<?>, String>> xn1) {
        this.a.j.lock();
        try {
            if (this.a.r) {
                if (xn1.e()) {
                    Map unused = this.a.s = new g4(this.a.e.size());
                    for (ni0 h : this.a.e.values()) {
                        this.a.s.put(h.h(), vd0.i);
                    }
                } else if (xn1.a() instanceof AvailabilityException) {
                    AvailabilityException availabilityException = (AvailabilityException) xn1.a();
                    if (this.a.p) {
                        Map unused2 = this.a.s = new g4(this.a.e.size());
                        for (ni0 ni0 : this.a.e.values()) {
                            zh0 h2 = ni0.h();
                            vd0 connectionResult = availabilityException.getConnectionResult(ni0);
                            if (this.a.a((ni0<?>) ni0, connectionResult)) {
                                this.a.s.put(h2, new vd0(16));
                            } else {
                                this.a.s.put(h2, connectionResult);
                            }
                        }
                    } else {
                        Map unused3 = this.a.s = availabilityException.zaj();
                    }
                    vd0 unused4 = this.a.v = this.a.k();
                } else {
                    Log.e("ConnectionlessGAC", "Unexpected availability exception", xn1.a());
                    Map unused5 = this.a.s = Collections.emptyMap();
                    vd0 unused6 = this.a.v = new vd0(8);
                }
                if (this.a.t != null) {
                    this.a.s.putAll(this.a.t);
                    vd0 unused7 = this.a.v = this.a.k();
                }
                if (this.a.v == null) {
                    this.a.i();
                    this.a.j();
                } else {
                    boolean unused8 = this.a.r = false;
                    this.a.i.a(this.a.v);
                }
                this.a.m.signalAll();
                this.a.j.unlock();
            }
        } finally {
            this.a.j.unlock();
        }
    }
}
