package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import com.portfolio.platform.service.notification.HybridMessageNotificationComponent;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fp2 implements MembersInjector<FossilNotificationListenerService> {
    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, DianaNotificationComponent dianaNotificationComponent) {
        fossilNotificationListenerService.l = dianaNotificationComponent;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, HybridMessageNotificationComponent hybridMessageNotificationComponent) {
        fossilNotificationListenerService.m = hybridMessageNotificationComponent;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, i42 i42) {
        fossilNotificationListenerService.n = i42;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, fn2 fn2) {
        fossilNotificationListenerService.o = fn2;
    }
}
