package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.k71;
import com.fossil.blesdk.obfuscated.l71;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class l71<MessageType extends k71<MessageType, BuilderType>, BuilderType extends l71<MessageType, BuilderType>> implements y91 {
    @DexIgnore
    public abstract BuilderType a(MessageType messagetype);

    @DexIgnore
    public final /* synthetic */ y91 a(x91 x91) {
        if (b().getClass().isInstance(x91)) {
            a((k71) x91);
            return this;
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }
}
