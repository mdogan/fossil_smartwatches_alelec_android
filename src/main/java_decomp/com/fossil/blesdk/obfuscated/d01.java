package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ee0;
import com.fossil.blesdk.obfuscated.he0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d01 extends ee0.a<a01, to0> {
    @DexIgnore
    public d01() {
    }

    @DexIgnore
    public final /* synthetic */ ee0.f a(Context context, Looper looper, lj0 lj0, Object obj, he0.b bVar, he0.c cVar) {
        return new a01(context, looper, lj0, bVar, cVar);
    }
}
