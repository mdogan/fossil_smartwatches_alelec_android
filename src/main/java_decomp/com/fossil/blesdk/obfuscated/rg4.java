package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rg4 {
    @DexIgnore
    public static final void a(pg4<?> pg4, ek4 ek4) {
        wd4.b(pg4, "$this$removeOnCancellation");
        wd4.b(ek4, "node");
        pg4.b((jd4<? super Throwable, cb4>) new gj4(ek4));
    }

    @DexIgnore
    public static final void a(pg4<?> pg4, ai4 ai4) {
        wd4.b(pg4, "$this$disposeOnCancellation");
        wd4.b(ai4, "handle");
        pg4.b((jd4<? super Throwable, cb4>) new bi4(ai4));
    }
}
