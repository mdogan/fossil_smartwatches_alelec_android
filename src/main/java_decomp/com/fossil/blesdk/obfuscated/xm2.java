package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.enums.Unit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xm2 {
    @DexIgnore
    public /* final */ Gson a; // = new Gson();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public final String a(MFUser mFUser, List<CustomizeRealData> list, String str, DianaPreset dianaPreset) {
        T t;
        wd4.b(list, "realDataList");
        wd4.b(str, "complicationId");
        wd4.b(dianaPreset, "preset");
        if (str.hashCode() == 134170930 && str.equals("second-timezone")) {
            Iterator<T> it = dianaPreset.getComplications().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (wd4.a((Object) ((DianaPresetComplicationSetting) t).getId(), (Object) "second-timezone")) {
                    break;
                }
            }
            DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
            if (dianaPresetComplicationSetting == null || pj2.a(dianaPresetComplicationSetting.getSettings())) {
                return "- -";
            }
            try {
                SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) this.a.a(dianaPresetComplicationSetting.getSettings(), SecondTimezoneSetting.class);
                if (secondTimezoneSetting == null) {
                    return "- -";
                }
                double timezoneRawOffsetById = (((double) ConversionUtils.getTimezoneRawOffsetById(secondTimezoneSetting.getTimeZoneId())) / 60.0d) - ((double) (((float) nl2.a()) / ((float) 3600)));
                if (timezoneRawOffsetById > ((double) 0)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append('+');
                    sb.append(timezoneRawOffsetById);
                    sb.append('h');
                    return sb.toString();
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append(timezoneRawOffsetById);
                sb2.append('h');
                return sb2.toString();
            } catch (Exception unused) {
                FLogger.INSTANCE.getLocal().d("CustomizeRealDataManager", "exception when parse 2nd tz setting");
                return "- -";
            }
        } else {
            ArrayList arrayList = new ArrayList(pb4.a(list, 10));
            for (CustomizeRealData copy$default : list) {
                arrayList.add(CustomizeRealData.copy$default(copy$default, (String) null, (String) null, 3, (Object) null));
            }
            return a(new ArrayList(arrayList), str, mFUser);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0047, code lost:
        if (r5 != null) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0112, code lost:
        if (r5 != null) goto L_0x008e;
     */
    @DexIgnore
    public final String a(List<CustomizeRealData> list, String str, MFUser mFUser) {
        T t;
        T t2;
        String valueOf;
        T t3;
        Unit unit;
        switch (str.hashCode()) {
            case -829740640:
                if (str.equals("commute-time")) {
                    return "34m";
                }
                return "- -";
            case -85386984:
                if (str.equals("active-minutes")) {
                    return "12m";
                }
                return "- -";
            case -48173007:
                if (!str.equals("chance-of-rain")) {
                    return "- -";
                }
                Iterator<T> it = list.iterator();
                while (true) {
                    if (it.hasNext()) {
                        t = it.next();
                        if (wd4.a((Object) "chance_of_rain", (Object) ((CustomizeRealData) t).getId())) {
                        }
                    } else {
                        t = null;
                    }
                }
                CustomizeRealData customizeRealData = (CustomizeRealData) t;
                if (customizeRealData == null) {
                    return "- -";
                }
                return customizeRealData.getValue() + '%';
            case 3076014:
                if (str.equals("date")) {
                    Iterator<T> it2 = list.iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            t2 = it2.next();
                            if (wd4.a((Object) "day", (Object) ((CustomizeRealData) t2).getId())) {
                            }
                        } else {
                            t2 = null;
                        }
                    }
                    CustomizeRealData customizeRealData2 = (CustomizeRealData) t2;
                    if (customizeRealData2 != null) {
                        valueOf = customizeRealData2.getValue();
                        break;
                    }
                    valueOf = String.valueOf(Calendar.getInstance().get(5));
                    break;
                } else {
                    return "- -";
                }
            case 96634189:
                if (!str.equals("empty")) {
                    return "- -";
                }
                String a2 = tm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Rename_PresetSwiped_CTA__Set);
                wd4.a((Object) a2, "LanguageHelper.getString\u2026me_PresetSwiped_CTA__Set)");
                return a2;
            case 109761319:
                if (str.equals("steps")) {
                    return "2385";
                }
                return "- -";
            case 1223440372:
                if (!str.equals("weather")) {
                    return "- -";
                }
                Iterator<T> it3 = list.iterator();
                while (true) {
                    if (it3.hasNext()) {
                        t3 = it3.next();
                        if (wd4.a((Object) "temperature", (Object) ((CustomizeRealData) t3).getId())) {
                        }
                    } else {
                        t3 = null;
                    }
                }
                CustomizeRealData customizeRealData3 = (CustomizeRealData) t3;
                if (mFUser != null) {
                    unit = mFUser.getTemperatureUnit();
                    break;
                }
                unit = Unit.METRIC;
                int i = ym2.a[unit.ordinal()];
                float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                if (i != 1) {
                    if (i == 2 && customizeRealData3 != null) {
                        try {
                            f = Float.parseFloat(customizeRealData3.getValue());
                        } catch (Exception unused) {
                            FLogger.INSTANCE.getLocal().d("CustomizeRealDataManager", "exception when parse celsius temperature");
                        }
                        StringBuilder sb = new StringBuilder();
                        sb.append(fe4.a(qk2.a(f)));
                        sb.append(8457);
                        valueOf = sb.toString();
                        break;
                    } else {
                        return "- -";
                    }
                } else if (customizeRealData3 != null) {
                    try {
                        f = Float.parseFloat(customizeRealData3.getValue());
                    } catch (Exception unused2) {
                        FLogger.INSTANCE.getLocal().d("CustomizeRealDataManager", "exception when parse celsius temperature");
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(fe4.a(f));
                    sb2.append(8451);
                    valueOf = sb2.toString();
                    break;
                } else {
                    return "- -";
                }
                break;
            case 1884273159:
                return str.equals("heart-rate") ? "68" : "- -";
            default:
                return "- -";
        }
        return valueOf;
    }
}
