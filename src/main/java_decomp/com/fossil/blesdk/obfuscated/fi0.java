package com.fossil.blesdk.obfuscated;

import android.app.Dialog;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fi0 extends bh0 {
    @DexIgnore
    public /* final */ /* synthetic */ Dialog a;
    @DexIgnore
    public /* final */ /* synthetic */ ei0 b;

    @DexIgnore
    public fi0(ei0 ei0, Dialog dialog) {
        this.b = ei0;
        this.a = dialog;
    }

    @DexIgnore
    public final void a() {
        this.b.f.g();
        if (this.a.isShowing()) {
            this.a.dismiss();
        }
    }
}
