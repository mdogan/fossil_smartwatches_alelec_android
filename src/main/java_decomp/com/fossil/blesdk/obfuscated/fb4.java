package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.collections.State;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class fb4<T> implements Iterator<T>, de4 {
    @DexIgnore
    public State e; // = State.NotReady;
    @DexIgnore
    public T f;

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void b(T t) {
        this.f = t;
        this.e = State.Ready;
    }

    @DexIgnore
    public final boolean c() {
        this.e = State.Failed;
        a();
        return this.e == State.Ready;
    }

    @DexIgnore
    public boolean hasNext() {
        if (this.e != State.Failed) {
            int i = eb4.a[this.e.ordinal()];
            if (i == 1) {
                return false;
            }
            if (i != 2) {
                return c();
            }
            return true;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    @DexIgnore
    public T next() {
        if (hasNext()) {
            this.e = State.NotReady;
            return this.f;
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final void b() {
        this.e = State.Done;
    }
}
