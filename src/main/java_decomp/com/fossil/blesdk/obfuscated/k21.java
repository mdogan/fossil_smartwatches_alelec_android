package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k21 {
    @DexIgnore
    public /* final */ double a;
    @DexIgnore
    public /* final */ double b;

    @DexIgnore
    public k21(double d, double d2) {
        this.a = d;
        this.b = d2;
    }

    @DexIgnore
    public final boolean a(double d) {
        return d >= this.a && d <= this.b;
    }
}
