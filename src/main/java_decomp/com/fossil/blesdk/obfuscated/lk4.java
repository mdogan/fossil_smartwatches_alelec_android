package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lk4 {
    @DexIgnore
    public /* final */ ek4 a;

    @DexIgnore
    public lk4(ek4 ek4) {
        wd4.b(ek4, "ref");
        this.a = ek4;
    }

    @DexIgnore
    public String toString() {
        return "Removed[" + this.a + ']';
    }
}
