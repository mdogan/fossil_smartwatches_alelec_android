package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.blesdk.device.data.background.BackgroundImageConfig;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.helper.mms.InvalidHeaderValueException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jm2 {
    @DexIgnore
    public static byte[] d;
    @DexIgnore
    public static byte[] e;
    @DexIgnore
    public ByteArrayInputStream a; // = null;
    @DexIgnore
    public im2 b; // = null;
    @DexIgnore
    public gm2 c; // = null;

    @DexIgnore
    public jm2(byte[] bArr) {
        this.a = new ByteArrayInputStream(bArr);
    }

    @DexIgnore
    public static void a(String str) {
    }

    @DexIgnore
    public static boolean a(int i) {
        return (i >= 32 && i <= 126) || (i >= 128 && i <= 255) || i == 9 || i == 10 || i == 13;
    }

    @DexIgnore
    public static boolean b(int i) {
        if (!(i < 33 || i > 126 || i == 34 || i == 44 || i == 47 || i == 123 || i == 125 || i == 40 || i == 41)) {
            switch (i) {
                case 58:
                case 59:
                case 60:
                case 61:
                case 62:
                case 63:
                case 64:
                    break;
                default:
                    switch (i) {
                        case 91:
                        case 92:
                        case 93:
                            break;
                        default:
                            return true;
                    }
            }
        }
        return false;
    }

    @DexIgnore
    public static byte[] b(ByteArrayInputStream byteArrayInputStream, int i) {
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read();
        if (1 == i && 34 == read) {
            byteArrayInputStream.mark(1);
        } else if (i == 0 && 127 == read) {
            byteArrayInputStream.mark(1);
        } else {
            byteArrayInputStream.reset();
        }
        return a(byteArrayInputStream, i);
    }

    @DexIgnore
    public static bm2 c(ByteArrayInputStream byteArrayInputStream) {
        int i;
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read() & 255;
        if (read == 0) {
            return new bm2("");
        }
        byteArrayInputStream.reset();
        if (read < 32) {
            i(byteArrayInputStream);
            i = g(byteArrayInputStream);
        } else {
            i = 0;
        }
        byte[] b2 = b(byteArrayInputStream, 0);
        if (i == 0) {
            return new bm2(b2);
        }
        try {
            return new bm2(i, b2);
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public static long d(ByteArrayInputStream byteArrayInputStream) {
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read();
        byteArrayInputStream.reset();
        if (read > 127) {
            return (long) g(byteArrayInputStream);
        }
        return e(byteArrayInputStream);
    }

    @DexIgnore
    public static long e(ByteArrayInputStream byteArrayInputStream) {
        int read = byteArrayInputStream.read() & 255;
        if (read <= 8) {
            long j = 0;
            for (int i = 0; i < read; i++) {
                j = (j << 8) + ((long) (byteArrayInputStream.read() & 255));
            }
            return j;
        }
        throw new RuntimeException("Octet count greater than 8 and I can't represent that!");
    }

    @DexIgnore
    public static gm2 f(ByteArrayInputStream byteArrayInputStream) {
        if (byteArrayInputStream == null) {
            return null;
        }
        int h = h(byteArrayInputStream);
        gm2 gm2 = new gm2();
        for (int i = 0; i < h; i++) {
            int h2 = h(byteArrayInputStream);
            int h3 = h(byteArrayInputStream);
            km2 km2 = new km2();
            int available = byteArrayInputStream.available();
            if (available <= 0) {
                return null;
            }
            HashMap hashMap = new HashMap();
            byte[] a2 = a(byteArrayInputStream, (HashMap<Integer, Object>) hashMap);
            if (a2 != null) {
                km2.e(a2);
            } else {
                km2.e(hm2.a[0].getBytes());
            }
            byte[] bArr = (byte[]) hashMap.get(151);
            if (bArr != null) {
                km2.h(bArr);
            }
            Integer num = (Integer) hashMap.get(129);
            if (num != null) {
                km2.a(num.intValue());
            }
            int available2 = h2 - (available - byteArrayInputStream.available());
            if (available2 > 0) {
                if (!a(byteArrayInputStream, km2, available2)) {
                    return null;
                }
            } else if (available2 < 0) {
                return null;
            }
            if (km2.b() == null && km2.f() == null && km2.e() == null && km2.a() == null) {
                km2.c(Long.toOctalString(System.currentTimeMillis()).getBytes());
            }
            if (h3 > 0) {
                byte[] bArr2 = new byte[h3];
                String str = new String(km2.d());
                byteArrayInputStream.read(bArr2, 0, h3);
                if (str.equalsIgnoreCase("application/vnd.wap.multipart.alternative")) {
                    km2 = f(new ByteArrayInputStream(bArr2)).a(0);
                } else {
                    byte[] c2 = km2.c();
                    if (c2 != null) {
                        String str2 = new String(c2);
                        if (str2.equalsIgnoreCase("base64")) {
                            bArr2 = yl2.a(bArr2);
                        } else if (str2.equalsIgnoreCase("quoted-printable")) {
                            bArr2 = lm2.a(bArr2);
                        }
                    }
                    if (bArr2 == null) {
                        a("Decode part data error!");
                        return null;
                    }
                    km2.f(bArr2);
                }
            }
            if (a(km2) == 0) {
                gm2.a(0, km2);
            } else {
                gm2.a(km2);
            }
        }
        return gm2;
    }

    @DexIgnore
    public static int g(ByteArrayInputStream byteArrayInputStream) {
        return byteArrayInputStream.read() & 127;
    }

    @DexIgnore
    public static int h(ByteArrayInputStream byteArrayInputStream) {
        int i = 0;
        int read = byteArrayInputStream.read();
        if (read == -1) {
            return read;
        }
        while ((read & 128) != 0) {
            i = (i << 7) | (read & 127);
            read = byteArrayInputStream.read();
            if (read == -1) {
                return read;
            }
        }
        return (i << 7) | (read & 127);
    }

    @DexIgnore
    public static int i(ByteArrayInputStream byteArrayInputStream) {
        int read = byteArrayInputStream.read() & 255;
        if (read <= 30) {
            return read;
        }
        if (read == 31) {
            return h(byteArrayInputStream);
        }
        throw new RuntimeException("Value length > LENGTH_QUOTE!");
    }

    @DexIgnore
    public cm2 a() {
        ByteArrayInputStream byteArrayInputStream = this.a;
        if (byteArrayInputStream == null) {
            return null;
        }
        this.b = a(byteArrayInputStream);
        im2 im2 = this.b;
        if (im2 == null) {
            return null;
        }
        int d2 = im2.d(ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
        if (!a(this.b)) {
            a("check mandatory headers failed!");
            return null;
        }
        if (128 == d2 || 132 == d2) {
            this.c = f(this.a);
            if (this.c == null) {
                return null;
            }
        }
        switch (d2) {
            case 128:
                return new qm2(this.b, this.c);
            case 129:
                return new pm2(this.b);
            case 130:
                return new em2(this.b);
            case 131:
                return new fm2(this.b);
            case 132:
                om2 om2 = new om2(this.b, this.c);
                byte[] b2 = om2.b();
                if (b2 == null) {
                    return null;
                }
                String str = new String(b2);
                if (str.equals("application/vnd.wap.multipart.mixed") || str.equals("application/vnd.wap.multipart.related") || str.equals("application/vnd.wap.multipart.alternative")) {
                    return om2;
                }
                if (!str.equals("application/vnd.wap.multipart.alternative")) {
                    return null;
                }
                km2 a2 = this.c.a(0);
                this.c.a();
                this.c.a(0, a2);
                return om2;
            case 133:
                return new xl2(this.b);
            case 134:
                return new am2(this.b);
            case 135:
                return new nm2(this.b);
            case 136:
                return new mm2(this.b);
            default:
                a("Parser doesn't support this message type in this version!");
                return null;
        }
    }

    @DexIgnore
    public static int b(ByteArrayInputStream byteArrayInputStream) {
        return byteArrayInputStream.read() & 255;
    }

    @DexIgnore
    public static int c(ByteArrayInputStream byteArrayInputStream, int i) {
        int read = byteArrayInputStream.read(new byte[i], 0, i);
        if (read < i) {
            return -1;
        }
        return read;
    }

    @DexIgnore
    public im2 a(ByteArrayInputStream byteArrayInputStream) {
        bm2 bm2;
        ByteArrayInputStream byteArrayInputStream2 = byteArrayInputStream;
        if (byteArrayInputStream2 == null) {
            return null;
        }
        im2 im2 = new im2();
        boolean z = true;
        while (z && byteArrayInputStream.available() > 0) {
            byteArrayInputStream2.mark(1);
            int b2 = b(byteArrayInputStream);
            if (b2 < 32 || b2 > 127) {
                switch (b2) {
                    case 129:
                    case 130:
                    case 151:
                        bm2 c2 = c(byteArrayInputStream);
                        if (c2 == null) {
                            break;
                        } else {
                            byte[] b3 = c2.b();
                            if (b3 != null) {
                                String str = new String(b3);
                                int indexOf = str.indexOf(47);
                                if (indexOf > 0) {
                                    str = str.substring(0, indexOf);
                                }
                                try {
                                    c2.a(str.getBytes());
                                } catch (NullPointerException unused) {
                                    a("null pointer error!");
                                    return null;
                                }
                            }
                            try {
                                im2.a(c2, b2);
                                break;
                            } catch (NullPointerException unused2) {
                                a("null pointer error!");
                                break;
                            } catch (RuntimeException unused3) {
                                a(b2 + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        }
                    case 131:
                    case 139:
                    case 152:
                    case 158:
                    case 183:
                    case 184:
                    case 185:
                    case 189:
                    case FacebookRequestErrorClassification.EC_INVALID_TOKEN:
                        byte[] b4 = b(byteArrayInputStream2, 0);
                        if (b4 == null) {
                            break;
                        } else {
                            try {
                                im2.a(b4, b2);
                                break;
                            } catch (NullPointerException unused4) {
                                a("null pointer error!");
                                break;
                            } catch (RuntimeException unused5) {
                                a(b2 + "is not Text-String header field!");
                                return null;
                            }
                        }
                    case 132:
                        HashMap hashMap = new HashMap();
                        byte[] a2 = a(byteArrayInputStream2, (HashMap<Integer, Object>) hashMap);
                        if (a2 != null) {
                            try {
                                im2.a(a2, 132);
                            } catch (NullPointerException unused6) {
                                a("null pointer error!");
                            } catch (RuntimeException unused7) {
                                a(b2 + "is not Text-String header field!");
                                return null;
                            }
                        }
                        e = (byte[]) hashMap.get(153);
                        d = (byte[]) hashMap.get(131);
                        z = false;
                        break;
                    case 133:
                    case 142:
                    case 159:
                        try {
                            im2.a(e(byteArrayInputStream), b2);
                            break;
                        } catch (RuntimeException unused8) {
                            a(b2 + "is not Long-Integer header field!");
                            return null;
                        }
                    case 134:
                    case 143:
                    case 144:
                    case 145:
                    case 146:
                    case 148:
                    case 149:
                    case 153:
                    case 155:
                    case 156:
                    case 162:
                    case 163:
                    case 165:
                    case 167:
                    case 169:
                    case 171:
                    case 177:
                    case BackgroundImageConfig.BOTTOM_BACKGROUND_ANGLE:
                    case 186:
                    case 187:
                    case 188:
                    case 191:
                        int b5 = b(byteArrayInputStream);
                        try {
                            im2.a(b5, b2);
                            break;
                        } catch (InvalidHeaderValueException unused9) {
                            a("Set invalid Octet value: " + b5 + " into the header filed: " + b2);
                            return null;
                        } catch (RuntimeException unused10) {
                            a(b2 + "is not Octet header field!");
                            return null;
                        }
                    case 135:
                    case 136:
                    case 157:
                        i(byteArrayInputStream);
                        int b6 = b(byteArrayInputStream);
                        try {
                            long e2 = e(byteArrayInputStream);
                            if (129 == b6) {
                                e2 += System.currentTimeMillis() / 1000;
                            }
                            try {
                                im2.a(e2, b2);
                                break;
                            } catch (RuntimeException unused11) {
                                a(b2 + "is not Long-Integer header field!");
                                return null;
                            }
                        } catch (RuntimeException unused12) {
                            a(b2 + "is not Long-Integer header field!");
                            return null;
                        }
                    case 137:
                        i(byteArrayInputStream);
                        if (128 == b(byteArrayInputStream)) {
                            bm2 = c(byteArrayInputStream);
                            if (bm2 != null) {
                                byte[] b7 = bm2.b();
                                if (b7 != null) {
                                    String str2 = new String(b7);
                                    int indexOf2 = str2.indexOf(47);
                                    if (indexOf2 > 0) {
                                        str2 = str2.substring(0, indexOf2);
                                    }
                                    try {
                                        bm2.a(str2.getBytes());
                                    } catch (NullPointerException unused13) {
                                        a("null pointer error!");
                                        return null;
                                    }
                                }
                            }
                        } else {
                            try {
                                bm2 = new bm2("insert-address-token".getBytes());
                            } catch (NullPointerException unused14) {
                                a(b2 + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        }
                        try {
                            im2.b(bm2, 137);
                            break;
                        } catch (NullPointerException unused15) {
                            a("null pointer error!");
                            break;
                        } catch (RuntimeException unused16) {
                            a(b2 + "is not Encoded-String-Value header field!");
                            return null;
                        }
                    case 138:
                        byteArrayInputStream2.mark(1);
                        int b8 = b(byteArrayInputStream);
                        if (b8 >= 128) {
                            if (128 != b8) {
                                if (129 != b8) {
                                    if (130 != b8) {
                                        if (131 != b8) {
                                            break;
                                        } else {
                                            im2.a("auto".getBytes(), 138);
                                            break;
                                        }
                                    } else {
                                        im2.a("informational".getBytes(), 138);
                                        break;
                                    }
                                } else {
                                    im2.a("advertisement".getBytes(), 138);
                                    break;
                                }
                            } else {
                                try {
                                    im2.a("personal".getBytes(), 138);
                                    break;
                                } catch (NullPointerException unused17) {
                                    a("null pointer error!");
                                    break;
                                } catch (RuntimeException unused18) {
                                    a(b2 + "is not Text-String header field!");
                                    return null;
                                }
                            }
                        } else {
                            byteArrayInputStream.reset();
                            byte[] b9 = b(byteArrayInputStream2, 0);
                            if (b9 == null) {
                                break;
                            } else {
                                try {
                                    im2.a(b9, 138);
                                    break;
                                } catch (NullPointerException unused19) {
                                    a("null pointer error!");
                                    break;
                                } catch (RuntimeException unused20) {
                                    a(b2 + "is not Text-String header field!");
                                    return null;
                                }
                            }
                        }
                    case ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL:
                        int b10 = b(byteArrayInputStream);
                        switch (b10) {
                            case 137:
                            case 138:
                            case 139:
                            case ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL:
                            case 141:
                            case 142:
                            case 143:
                            case 144:
                            case 145:
                            case 146:
                            case 147:
                            case 148:
                            case 149:
                            case 150:
                            case 151:
                                return null;
                            default:
                                try {
                                    im2.a(b10, b2);
                                    break;
                                } catch (InvalidHeaderValueException unused21) {
                                    a("Set invalid Octet value: " + b10 + " into the header filed: " + b2);
                                    return null;
                                } catch (RuntimeException unused22) {
                                    a(b2 + "is not Octet header field!");
                                    return null;
                                }
                        }
                    case 141:
                        int g = g(byteArrayInputStream);
                        try {
                            im2.a(g, 141);
                            break;
                        } catch (InvalidHeaderValueException unused23) {
                            a("Set invalid Octet value: " + g + " into the header filed: " + b2);
                            return null;
                        } catch (RuntimeException unused24) {
                            a(b2 + "is not Octet header field!");
                            return null;
                        }
                    case 147:
                    case 150:
                    case 154:
                    case 166:
                    case 181:
                    case 182:
                        bm2 c3 = c(byteArrayInputStream);
                        if (c3 == null) {
                            break;
                        } else {
                            try {
                                im2.b(c3, b2);
                                break;
                            } catch (NullPointerException unused25) {
                                a("null pointer error!");
                                break;
                            } catch (RuntimeException unused26) {
                                a(b2 + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        }
                    case 160:
                        i(byteArrayInputStream);
                        try {
                            d(byteArrayInputStream);
                            bm2 c4 = c(byteArrayInputStream);
                            if (c4 == null) {
                                break;
                            } else {
                                try {
                                    im2.b(c4, 160);
                                    break;
                                } catch (NullPointerException unused27) {
                                    a("null pointer error!");
                                    break;
                                } catch (RuntimeException unused28) {
                                    a(b2 + "is not Encoded-String-Value header field!");
                                    return null;
                                }
                            }
                        } catch (RuntimeException unused29) {
                            a(b2 + " is not Integer-Value");
                            return null;
                        }
                    case 161:
                        i(byteArrayInputStream);
                        try {
                            d(byteArrayInputStream);
                            try {
                                im2.a(e(byteArrayInputStream), 161);
                                break;
                            } catch (RuntimeException unused30) {
                                a(b2 + "is not Long-Integer header field!");
                                return null;
                            }
                        } catch (RuntimeException unused31) {
                            a(b2 + " is not Integer-Value");
                            return null;
                        }
                    case 164:
                        i(byteArrayInputStream);
                        b(byteArrayInputStream);
                        c(byteArrayInputStream);
                        break;
                    case 170:
                    case 172:
                        i(byteArrayInputStream);
                        b(byteArrayInputStream);
                        try {
                            d(byteArrayInputStream);
                            break;
                        } catch (RuntimeException unused32) {
                            a(b2 + " is not Integer-Value");
                            return null;
                        }
                    case 173:
                    case 175:
                    case 179:
                        try {
                            im2.a(d(byteArrayInputStream), b2);
                            break;
                        } catch (RuntimeException unused33) {
                            a(b2 + "is not Long-Integer header field!");
                            return null;
                        }
                    case 178:
                        a(byteArrayInputStream2, (HashMap<Integer, Object>) null);
                        break;
                    default:
                        a("Unknown header");
                        break;
                }
            } else {
                byteArrayInputStream.reset();
                b(byteArrayInputStream2, 0);
            }
        }
        return im2;
    }

    @DexIgnore
    public static byte[] a(ByteArrayInputStream byteArrayInputStream, int i) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int read = byteArrayInputStream.read();
        while (-1 != read && read != 0) {
            if (i == 2) {
                if (b(read)) {
                    byteArrayOutputStream.write(read);
                }
            } else if (a(read)) {
                byteArrayOutputStream.write(read);
            }
            read = byteArrayInputStream.read();
        }
        if (byteArrayOutputStream.size() > 0) {
            return byteArrayOutputStream.toByteArray();
        }
        return null;
    }

    @DexIgnore
    public static void a(ByteArrayInputStream byteArrayInputStream, HashMap<Integer, Object> hashMap, Integer num) {
        int available;
        int intValue;
        int available2 = byteArrayInputStream.available();
        int intValue2 = num.intValue();
        while (intValue2 > 0) {
            int read = byteArrayInputStream.read();
            intValue2--;
            if (read != 129) {
                if (read != 131) {
                    if (read == 133 || read == 151) {
                        byte[] b2 = b(byteArrayInputStream, 0);
                        if (!(b2 == null || hashMap == null)) {
                            hashMap.put(151, b2);
                        }
                        available = byteArrayInputStream.available();
                        intValue = num.intValue();
                    } else {
                        if (read != 153) {
                            if (read != 137) {
                                if (read != 138) {
                                    if (-1 == c(byteArrayInputStream, intValue2)) {
                                        FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Content-Type");
                                    } else {
                                        intValue2 = 0;
                                    }
                                }
                            }
                        }
                        byte[] b3 = b(byteArrayInputStream, 0);
                        if (!(b3 == null || hashMap == null)) {
                            hashMap.put(153, b3);
                        }
                        available = byteArrayInputStream.available();
                        intValue = num.intValue();
                    }
                }
                byteArrayInputStream.mark(1);
                int b4 = b(byteArrayInputStream);
                byteArrayInputStream.reset();
                if (b4 > 127) {
                    int g = g(byteArrayInputStream);
                    String[] strArr = hm2.a;
                    if (g < strArr.length) {
                        hashMap.put(131, strArr[g].getBytes());
                    }
                } else {
                    byte[] b5 = b(byteArrayInputStream, 0);
                    if (!(b5 == null || hashMap == null)) {
                        hashMap.put(131, b5);
                    }
                }
                available = byteArrayInputStream.available();
                intValue = num.intValue();
            } else {
                byteArrayInputStream.mark(1);
                int b6 = b(byteArrayInputStream);
                byteArrayInputStream.reset();
                if ((b6 <= 32 || b6 >= 127) && b6 != 0) {
                    int d2 = (int) d(byteArrayInputStream);
                    if (hashMap != null) {
                        hashMap.put(129, Integer.valueOf(d2));
                    }
                } else {
                    try {
                        hashMap.put(129, Integer.valueOf(zl2.a(new String(b(byteArrayInputStream, 0)))));
                    } catch (UnsupportedEncodingException unused) {
                        hashMap.put(129, 0);
                    }
                }
                available = byteArrayInputStream.available();
                intValue = num.intValue();
            }
            intValue2 = intValue - (available2 - available);
        }
        if (intValue2 != 0) {
            FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Content-Type");
        }
    }

    @DexIgnore
    public static byte[] a(ByteArrayInputStream byteArrayInputStream, HashMap<Integer, Object> hashMap) {
        byte[] bArr;
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read();
        byteArrayInputStream.reset();
        int i = read & 255;
        if (i < 32) {
            int i2 = i(byteArrayInputStream);
            int available = byteArrayInputStream.available();
            byteArrayInputStream.mark(1);
            int read2 = byteArrayInputStream.read();
            byteArrayInputStream.reset();
            int i3 = read2 & 255;
            if (i3 >= 32 && i3 <= 127) {
                bArr = b(byteArrayInputStream, 0);
            } else if (i3 > 127) {
                int g = g(byteArrayInputStream);
                String[] strArr = hm2.a;
                if (g < strArr.length) {
                    bArr = strArr[g].getBytes();
                } else {
                    byteArrayInputStream.reset();
                    bArr = b(byteArrayInputStream, 0);
                }
            } else {
                FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt content-type");
                return hm2.a[0].getBytes();
            }
            int available2 = i2 - (available - byteArrayInputStream.available());
            if (available2 > 0) {
                a(byteArrayInputStream, hashMap, Integer.valueOf(available2));
            }
            if (available2 >= 0) {
                return bArr;
            }
            FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt MMS message");
            return hm2.a[0].getBytes();
        } else if (i <= 127) {
            return b(byteArrayInputStream, 0);
        } else {
            return hm2.a[g(byteArrayInputStream)].getBytes();
        }
    }

    @DexIgnore
    public static boolean a(ByteArrayInputStream byteArrayInputStream, km2 km2, int i) {
        int available;
        int available2 = byteArrayInputStream.available();
        int i2 = i;
        while (i2 > 0) {
            int read = byteArrayInputStream.read();
            i2--;
            if (read > 127) {
                if (read != 142) {
                    if (read != 174) {
                        if (read == 192) {
                            byte[] b2 = b(byteArrayInputStream, 1);
                            if (b2 != null) {
                                km2.b(b2);
                            }
                            available = byteArrayInputStream.available();
                        } else if (read != 197) {
                            if (-1 == c(byteArrayInputStream, i2)) {
                                FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Part headers");
                                return false;
                            }
                            i2 = 0;
                        }
                    }
                    if (Resources.getSystem().getBoolean(Resources.getSystem().getIdentifier("config_mms_content_disposition_support", "id", "android"))) {
                        int i3 = i(byteArrayInputStream);
                        byteArrayInputStream.mark(1);
                        int available3 = byteArrayInputStream.available();
                        int read2 = byteArrayInputStream.read();
                        if (read2 == 128) {
                            km2.a(km2.c);
                        } else if (read2 == 129) {
                            km2.a(km2.d);
                        } else if (read2 == 130) {
                            km2.a(km2.e);
                        } else {
                            byteArrayInputStream.reset();
                            km2.a(b(byteArrayInputStream, 0));
                        }
                        if (available3 - byteArrayInputStream.available() < i3) {
                            if (byteArrayInputStream.read() == 152) {
                                km2.g(b(byteArrayInputStream, 0));
                            }
                            int available4 = available3 - byteArrayInputStream.available();
                            if (available4 < i3) {
                                int i4 = i3 - available4;
                                byteArrayInputStream.read(new byte[i4], 0, i4);
                            }
                        }
                        available = byteArrayInputStream.available();
                    }
                } else {
                    byte[] b3 = b(byteArrayInputStream, 0);
                    if (b3 != null) {
                        km2.c(b3);
                    }
                    available = byteArrayInputStream.available();
                }
            } else if (read < 32 || read > 127) {
                if (-1 == c(byteArrayInputStream, i2)) {
                    FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Part headers");
                    return false;
                }
                i2 = 0;
            } else {
                byte[] b4 = b(byteArrayInputStream, 0);
                byte[] b5 = b(byteArrayInputStream, 0);
                if (true == "Content-Transfer-Encoding".equalsIgnoreCase(new String(b4))) {
                    km2.d(b5);
                }
                available = byteArrayInputStream.available();
            }
            i2 = i - (available2 - available);
        }
        if (i2 == 0) {
            return true;
        }
        FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Part headers");
        return false;
    }

    @DexIgnore
    public static int a(km2 km2) {
        if (d == null && e == null) {
            return 1;
        }
        if (e != null) {
            byte[] a2 = km2.a();
            if (a2 != null && true == Arrays.equals(e, a2)) {
                return 0;
            }
        }
        if (d != null) {
            byte[] d2 = km2.d();
            if (d2 == null || true != Arrays.equals(d, d2)) {
                return 1;
            }
            return 0;
        }
        return 1;
    }

    @DexIgnore
    public static boolean a(im2 im2) {
        if (im2 == null) {
            return false;
        }
        int d2 = im2.d(ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
        if (im2.d(141) == 0) {
            return false;
        }
        switch (d2) {
            case 128:
                if (im2.e(132) == null || im2.a(137) == null || im2.e(152) == null) {
                    return false;
                }
                return true;
            case 129:
                if (im2.d(146) == 0 || im2.e(152) == null) {
                    return false;
                }
                return true;
            case 130:
                if (im2.e(131) == null || -1 == im2.c(136) || im2.e(138) == null || -1 == im2.c(142) || im2.e(152) == null) {
                    return false;
                }
                return true;
            case 131:
                if (im2.d(149) == 0 || im2.e(152) == null) {
                    return false;
                }
                return true;
            case 132:
                if (im2.e(132) == null || -1 == im2.c(133)) {
                    return false;
                }
                return true;
            case 133:
                if (im2.e(152) == null) {
                    return false;
                }
                return true;
            case 134:
                if (-1 == im2.c(133) || im2.e(139) == null || im2.d(149) == 0 || im2.b(151) == null) {
                    return false;
                }
                return true;
            case 135:
                if (im2.a(137) == null || im2.e(139) == null || im2.d(155) == 0 || im2.b(151) == null) {
                    return false;
                }
                return true;
            case 136:
                if (-1 == im2.c(133) || im2.a(137) == null || im2.e(139) == null || im2.d(155) == 0 || im2.b(151) == null) {
                    return false;
                }
                return true;
            default:
                return false;
        }
    }
}
