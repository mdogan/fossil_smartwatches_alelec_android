package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.ui.BaseActivity;
import java.lang.ref.WeakReference;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kn2 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public GoogleSignInOptions a;
    @DexIgnore
    public yb0 b;
    @DexIgnore
    public /* final */ String c; // = "23412525";
    @DexIgnore
    public WeakReference<BaseActivity> d;
    @DexIgnore
    public mn2 e;
    @DexIgnore
    public int f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<TResult> implements sn1<Void> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void onComplete(xn1<Void> xn1) {
            wd4.b(xn1, "it");
            FLogger.INSTANCE.getLocal().e(kn2.g, "Log out google account completely");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements tn1 {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public final void onFailure(Exception exc) {
            wd4.b(exc, "it");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = kn2.g;
            local.e(d, "Could not log out google account, error = " + exc.getLocalizedMessage());
            exc.printStackTrace();
        }
    }

    /*
    static {
        new a((rd4) null);
        String canonicalName = kn2.class.getCanonicalName();
        if (canonicalName != null) {
            wd4.a((Object) canonicalName, "MFLoginGoogleManager::class.java.canonicalName!!");
            g = canonicalName;
            return;
        }
        wd4.a();
        throw null;
    }
    */

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001f, code lost:
        if (r0 != null) goto L_0x0024;
     */
    @DexIgnore
    public final void a(WeakReference<BaseActivity> weakReference, mn2 mn2) {
        String str;
        wd4.b(weakReference, Constants.ACTIVITY);
        wd4.b(mn2, Constants.CALLBACK);
        Access a2 = new SoLibraryLoader().a((Context) PortfolioApp.W.c());
        if (a2 != null) {
            str = a2.getA();
        }
        str = this.c;
        if (str != null) {
            String obj = StringsKt__StringsKt.d(str).toString();
            GoogleSignInOptions.a aVar = new GoogleSignInOptions.a(GoogleSignInOptions.s);
            aVar.a(obj);
            aVar.b(obj);
            aVar.b();
            aVar.d();
            GoogleSignInOptions a3 = aVar.a();
            wd4.a((Object) a3, "GoogleSignInOptions.Buil\u2026\n                .build()");
            this.a = a3;
            Context applicationContext = PortfolioApp.W.c().getApplicationContext();
            GoogleSignInOptions googleSignInOptions = this.a;
            if (googleSignInOptions != null) {
                this.b = wb0.a(applicationContext, googleSignInOptions);
                this.e = mn2;
                this.d = weakReference;
                b();
                return;
            }
            wd4.d("gso");
            throw null;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @DexIgnore
    public final void b() {
        a((WeakReference<Activity>) this.d);
        a();
    }

    @DexIgnore
    public final void c() {
        a((WeakReference<Activity>) this.d);
    }

    @DexIgnore
    public final boolean a(int i, int i2, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "Inside .onActivityResult requestCode=" + i + ", resultCode=" + i2);
        if (i != 922) {
            return true;
        }
        if (intent != null) {
            ac0 a2 = rb0.f.a(intent);
            a(a2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = g;
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .onActivityResult googleSignInResult=");
            wd4.a((Object) a2, Constants.RESULT);
            sb.append(a2.G());
            local2.d(str2, sb.toString());
            return true;
        }
        a((ac0) null);
        return true;
    }

    @DexIgnore
    public final void a(ac0 ac0) {
        int i = 600;
        if (ac0 == null) {
            c();
            mn2 mn2 = this.e;
            if (mn2 != null) {
                mn2.a(600, (vd0) null, "");
            }
        } else if (ac0.b()) {
            GoogleSignInAccount a2 = ac0.a();
            if (a2 == null) {
                mn2 mn22 = this.e;
                if (mn22 != null) {
                    mn22.a(600, (vd0) null, "");
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                FLogger.INSTANCE.getLocal().d(g, "Step 1: Login using google success");
                SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                String J = a2.J();
                if (J == null) {
                    J = "";
                }
                signUpSocialAuth.setEmail(J);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = g;
                local.d(str, "Google user email is " + a2.J());
                String L = a2.L();
                if (L == null) {
                    L = "";
                }
                signUpSocialAuth.setFirstName(L);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = g;
                local2.d(str2, "Google user first name is " + a2.L());
                String K = a2.K();
                if (K == null) {
                    K = "";
                }
                signUpSocialAuth.setLastName(K);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = g;
                local3.d(str3, "Google user last name is " + a2.K());
                String O = a2.O();
                if (O == null) {
                    O = "";
                }
                signUpSocialAuth.setToken(O);
                signUpSocialAuth.setClientId(AppHelper.f.a(""));
                signUpSocialAuth.setService("google");
                mn2 mn23 = this.e;
                if (mn23 != null) {
                    mn23.a(signUpSocialAuth);
                }
                this.f = 0;
                c();
            }
        } else {
            Status G = ac0.G();
            wd4.a((Object) G, "result.status");
            int I = G.I();
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = g;
            local4.e(str4, "login result code from google: " + I);
            if (I == 12502) {
                int i2 = this.f;
                if (i2 < 2) {
                    this.f = i2 + 1;
                    a();
                    return;
                }
                this.f = 0;
                FLogger.INSTANCE.getLocal().e(g, "why the login session always end up here, bug from Google!!");
                c();
                mn2 mn24 = this.e;
                if (mn24 != null) {
                    mn24.a(600, (vd0) null, "");
                    return;
                }
                return;
            }
            if (I == 12501) {
                i = 2;
            }
            c();
            mn2 mn25 = this.e;
            if (mn25 != null) {
                mn25.a(i, (vd0) null, "");
            }
        }
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().e(g, "connectNewAccount");
        WeakReference<BaseActivity> weakReference = this.d;
        if (weakReference != null) {
            if (weakReference == null) {
                wd4.a();
                throw null;
            } else if (weakReference.get() != null) {
                yb0 yb0 = this.b;
                if (yb0 != null) {
                    if (yb0 != null) {
                        Intent i = yb0.i();
                        wd4.a((Object) i, "googleSignInClient!!.signInIntent");
                        WeakReference<BaseActivity> weakReference2 = this.d;
                        if (weakReference2 != null) {
                            BaseActivity baseActivity = (BaseActivity) weakReference2.get();
                            if (baseActivity != null) {
                                baseActivity.startActivityForResult(i, 922);
                                return;
                            }
                            return;
                        }
                        wd4.a();
                        throw null;
                    }
                    wd4.a();
                    throw null;
                }
            }
        }
        mn2 mn2 = this.e;
        if (mn2 != null) {
            mn2.a(600, (vd0) null, "");
        }
    }

    @DexIgnore
    public final void a(WeakReference<Activity> weakReference) {
        if ((weakReference != null ? (Activity) weakReference.get() : null) != null && wb0.a((Context) weakReference.get()) != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.d(str, "inside .logOut(), googleSignInClient=" + this.b);
            yb0 yb0 = this.b;
            if (yb0 == null) {
                return;
            }
            if (yb0 != null) {
                xn1<Void> j = yb0.j();
                if (j != null) {
                    j.a((sn1<Void>) b.a);
                }
                if (j != null) {
                    j.a((tn1) c.a);
                    return;
                }
                return;
            }
            wd4.a();
            throw null;
        }
    }
}
