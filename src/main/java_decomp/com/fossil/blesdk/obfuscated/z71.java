package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zzuv;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class z71 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public c81 c;

    @DexIgnore
    public z71() {
        this.b = 100;
    }

    @DexIgnore
    public static z71 a(byte[] bArr, int i, int i2) {
        return a(bArr, i, i2, false);
    }

    @DexIgnore
    public abstract double a() throws IOException;

    @DexIgnore
    public abstract <T extends x91> T a(ia1<T> ia1, j81 j81) throws IOException;

    @DexIgnore
    public abstract void a(int i) throws zzuv;

    @DexIgnore
    public abstract float b() throws IOException;

    @DexIgnore
    public abstract boolean b(int i) throws IOException;

    @DexIgnore
    public final int c(int i) {
        if (i >= 0) {
            int i2 = this.b;
            this.b = i;
            return i2;
        }
        StringBuilder sb = new StringBuilder(47);
        sb.append("Recursion limit cannot be negative: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    public abstract String c() throws IOException;

    @DexIgnore
    public abstract int d() throws IOException;

    @DexIgnore
    public abstract int d(int i) throws zzuv;

    @DexIgnore
    public abstract long e() throws IOException;

    @DexIgnore
    public abstract void e(int i);

    @DexIgnore
    public abstract long f() throws IOException;

    @DexIgnore
    public abstract void f(int i) throws IOException;

    @DexIgnore
    public abstract int g() throws IOException;

    @DexIgnore
    public abstract long h() throws IOException;

    @DexIgnore
    public abstract int i() throws IOException;

    @DexIgnore
    public abstract boolean j() throws IOException;

    @DexIgnore
    public abstract String k() throws IOException;

    @DexIgnore
    public abstract zzte l() throws IOException;

    @DexIgnore
    public abstract int m() throws IOException;

    @DexIgnore
    public abstract int n() throws IOException;

    @DexIgnore
    public abstract int o() throws IOException;

    @DexIgnore
    public abstract long p() throws IOException;

    @DexIgnore
    public abstract int q() throws IOException;

    @DexIgnore
    public abstract long r() throws IOException;

    @DexIgnore
    public abstract boolean s() throws IOException;

    @DexIgnore
    public abstract int t();

    @DexIgnore
    public static z71 a(byte[] bArr, int i, int i2, boolean z) {
        b81 b81 = new b81(bArr, i, i2, false);
        try {
            b81.d(i2);
            return b81;
        } catch (zzuv e) {
            throw new IllegalArgumentException(e);
        }
    }
}
