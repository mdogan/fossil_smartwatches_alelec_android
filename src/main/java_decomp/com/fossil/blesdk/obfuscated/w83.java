package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.blesdk.obfuscated.xr2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w83 extends as2 implements v83 {
    @DexIgnore
    public ur3<z82> j;
    @DexIgnore
    public u83 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ActiveTimeOverviewWeekFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onCreateView");
        this.j = new ur3<>(this, (z82) ra.a(layoutInflater, R.layout.fragment_active_time_overview_week, viewGroup, false, O0()));
        ur3<z82> ur3 = this.j;
        if (ur3 != null) {
            z82 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onResume");
        u83 u83 = this.k;
        if (u83 != null) {
            u83.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onStop");
        u83 u83 = this.k;
        if (u83 != null) {
            u83.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(xr2 xr2) {
        wd4.b(xr2, "baseModel");
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "showWeekDetails");
        ur3<z82> ur3 = this.j;
        if (ur3 != null) {
            z82 a2 = ur3.a();
            if (a2 != null) {
                OverviewWeekChart overviewWeekChart = a2.q;
                if (overviewWeekChart != null) {
                    new ArrayList();
                    BarChart.c cVar = (BarChart.c) xr2;
                    cVar.b(xr2.a.a(cVar.c()));
                    xr2.a aVar = xr2.a;
                    wd4.a((Object) overviewWeekChart, "it");
                    Context context = overviewWeekChart.getContext();
                    wd4.a((Object) context, "it.context");
                    BarChart.a((BarChart) overviewWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
                    overviewWeekChart.a(xr2);
                }
            }
        }
    }

    @DexIgnore
    public void a(u83 u83) {
        wd4.b(u83, "presenter");
        this.k = u83;
    }
}
