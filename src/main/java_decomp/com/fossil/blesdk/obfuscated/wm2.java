package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wm2 {
    @DexIgnore
    public static vm2 a; // = new vm2();

    @DexIgnore
    public static void a(String str, boolean z) {
        a.a(str, z);
    }

    @DexIgnore
    public static String b(String str) {
        return a.a(str);
    }

    @DexIgnore
    public static void a() {
        a.a();
        sm2.b().a();
    }

    @DexIgnore
    public static void b() {
        vm2 vm2 = a;
        if (vm2 != null && vm2.b() != null) {
            sm2 b = sm2.b();
            for (Map.Entry next : a.b().entrySet()) {
                if (b.a((String) next.getKey())) {
                    b.c((String) next.getKey());
                }
                b.a((String) next.getKey(), (String) next.getValue());
            }
        }
    }

    @DexIgnore
    public static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        return str.substring(str.lastIndexOf(File.separator) + 1);
    }
}
