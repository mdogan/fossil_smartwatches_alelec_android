package com.fossil.blesdk.obfuscated;

import com.android.volley.Request;
import com.fossil.blesdk.obfuscated.vm;
import java.io.UnsupportedEncodingException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class mn<T> extends Request<T> {
    @DexIgnore
    public static /* final */ String PROTOCOL_CHARSET; // = "utf-8";
    @DexIgnore
    public static /* final */ String PROTOCOL_CONTENT_TYPE; // = String.format("application/json; charset=%s", new Object[]{PROTOCOL_CHARSET});
    @DexIgnore
    public vm.b<T> mListener;
    @DexIgnore
    public /* final */ Object mLock;
    @DexIgnore
    public /* final */ String mRequestBody;

    @DexIgnore
    @Deprecated
    public mn(String str, String str2, vm.b<T> bVar, vm.a aVar) {
        this(-1, str, str2, bVar, aVar);
    }

    @DexIgnore
    public void cancel() {
        super.cancel();
        synchronized (this.mLock) {
            this.mListener = null;
        }
    }

    @DexIgnore
    public void deliverResponse(T t) {
        vm.b<T> bVar;
        synchronized (this.mLock) {
            bVar = this.mListener;
        }
        if (bVar != null) {
            bVar.onResponse(t);
        }
    }

    @DexIgnore
    public byte[] getBody() {
        try {
            if (this.mRequestBody == null) {
                return null;
            }
            return this.mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException unused) {
            ym.e("Unsupported Encoding while trying to get the bytes of %s using %s", this.mRequestBody, PROTOCOL_CHARSET);
            return null;
        }
    }

    @DexIgnore
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @DexIgnore
    @Deprecated
    public byte[] getPostBody() {
        return getBody();
    }

    @DexIgnore
    @Deprecated
    public String getPostBodyContentType() {
        return getBodyContentType();
    }

    @DexIgnore
    public abstract vm<T> parseNetworkResponse(tm tmVar);

    @DexIgnore
    public mn(int i, String str, String str2, vm.b<T> bVar, vm.a aVar) {
        super(i, str, aVar);
        this.mLock = new Object();
        this.mListener = bVar;
        this.mRequestBody = str2;
    }
}
