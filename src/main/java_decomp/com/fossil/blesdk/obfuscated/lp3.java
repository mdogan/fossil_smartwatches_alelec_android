package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lp3 extends as2 implements kp3 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public ur3<bg2> j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return lp3.m;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final lp3 a(boolean z) {
            lp3 lp3 = new lp3();
            lp3.k = z;
            return lp3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lp3 e;

        @DexIgnore
        public b(lp3 lp3) {
            this.e = lp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lp3 e;

        @DexIgnore
        public c(lp3 lp3) {
            this.e = lp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lp3 e;

        @DexIgnore
        public d(lp3 lp3) {
            this.e = lp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            HelpActivity.a aVar = HelpActivity.C;
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                wd4.a((Object) activity, "activity!!");
                aVar.a(activity);
                FragmentActivity activity2 = this.e.getActivity();
                if (activity2 != null) {
                    activity2.finish();
                } else {
                    wd4.a();
                    throw null;
                }
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = lp3.class.getSimpleName();
        wd4.a((Object) simpleName, "TroubleshootingFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return m;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void a(jp3 jp3) {
        wd4.b(jp3, "presenter");
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new ur3<>(this, (bg2) ra.a(layoutInflater, R.layout.fragment_troubleshooting, viewGroup, false, O0()));
        ur3<bg2> ur3 = this.j;
        if (ur3 != null) {
            bg2 a2 = ur3.a();
            if (a2 != null) {
                wd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<bg2> ur3 = this.j;
        if (ur3 != null) {
            bg2 a2 = ur3.a();
            if (a2 != null) {
                View findViewById = a2.s.findViewById(R.id.ll_battery_reinstall);
                if (this.k) {
                    wd4.a((Object) findViewById, "batteryText");
                    findViewById.setVisibility(8);
                }
                a2.q.setOnClickListener(new b(this));
                a2.t.setOnClickListener(new c(this));
                a2.r.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }
}
