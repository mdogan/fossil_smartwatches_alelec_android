package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOverlay;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ai implements bi {
    @DexIgnore
    public /* final */ ViewOverlay a;

    @DexIgnore
    public ai(View view) {
        this.a = view.getOverlay();
    }

    @DexIgnore
    public void a(Drawable drawable) {
        this.a.add(drawable);
    }

    @DexIgnore
    public void b(Drawable drawable) {
        this.a.remove(drawable);
    }
}
