package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.to;
import com.fossil.blesdk.obfuscated.tr;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bk2 implements tr<ck2, InputStream> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ur<ck2, InputStream> {
        @DexIgnore
        public bk2 a(xr xrVar) {
            wd4.b(xrVar, "multiFactory");
            return new bk2();
        }
    }

    @DexIgnore
    public boolean a(ck2 ck2) {
        wd4.b(ck2, "avatarModel");
        return true;
    }

    @DexIgnore
    public tr.a<InputStream> a(ck2 ck2, int i, int i2, mo moVar) {
        wd4.b(ck2, "avatarModel");
        wd4.b(moVar, "options");
        return new tr.a<>(ck2, new a(this, ck2));
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements to<InputStream> {
        @DexIgnore
        public to<InputStream> e;
        @DexIgnore
        public volatile boolean f;
        @DexIgnore
        public /* final */ ck2 g;

        @DexIgnore
        public a(bk2 bk2, ck2 ck2) {
            wd4.b(ck2, "mAvatarModel");
            this.g = ck2;
        }

        @DexIgnore
        public void a(Priority priority, to.a<? super InputStream> aVar) {
            wd4.b(priority, "priority");
            wd4.b(aVar, Constants.CALLBACK);
            try {
                if (!TextUtils.isEmpty(this.g.c())) {
                    this.e = new zo(new mr(this.g.c()), 100);
                } else if (this.g.b() != null) {
                    this.e = new dp(PortfolioApp.W.c().getContentResolver(), this.g.b());
                }
                to<InputStream> toVar = this.e;
                if (toVar != null) {
                    toVar.a(priority, aVar);
                }
            } catch (Exception unused) {
                to<InputStream> toVar2 = this.e;
                if (toVar2 != null) {
                    toVar2.a();
                }
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            String a = this.g.a();
            if (a == null) {
                a = "";
            }
            Bitmap b = wr3.b(a);
            if (b != null) {
                b.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            }
            aVar.a(this.f ? null : new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
        }

        @DexIgnore
        public DataSource b() {
            return DataSource.REMOTE;
        }

        @DexIgnore
        public void cancel() {
            this.f = true;
        }

        @DexIgnore
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }

        @DexIgnore
        public void a() {
            to<InputStream> toVar = this.e;
            if (toVar != null) {
                toVar.a();
            }
        }
    }
}
