package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gk2 extends yn {
    @DexIgnore
    public gk2(sn snVar, ru ruVar, wu wuVar, Context context) {
        super(snVar, ruVar, wuVar, context);
    }

    @DexIgnore
    public fk2<Bitmap> e() {
        return (fk2) super.e();
    }

    @DexIgnore
    public fk2<Drawable> f() {
        return (fk2) super.f();
    }

    @DexIgnore
    public <ResourceType> fk2<ResourceType> a(Class<ResourceType> cls) {
        return new fk2<>(this.e, this, cls, this.f);
    }

    @DexIgnore
    public fk2<Drawable> a(String str) {
        return (fk2) super.a(str);
    }

    @DexIgnore
    public fk2<Drawable> a(Uri uri) {
        return (fk2) super.a(uri);
    }

    @DexIgnore
    public fk2<Drawable> a(Integer num) {
        return (fk2) super.a(num);
    }

    @DexIgnore
    public fk2<Drawable> a(byte[] bArr) {
        return (fk2) super.a(bArr);
    }

    @DexIgnore
    public fk2<Drawable> a(Object obj) {
        return (fk2) super.a(obj);
    }

    @DexIgnore
    public void a(sv svVar) {
        if (svVar instanceof ek2) {
            super.a(svVar);
        } else {
            super.a((sv) new ek2().a((mv<?>) svVar));
        }
    }
}
