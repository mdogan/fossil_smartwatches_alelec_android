package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.asyncevent.AsyncEvent;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i70 extends Request {
    @DexIgnore
    public jd4<? super AsyncEvent, cb4> A;
    @DexIgnore
    public long B;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i70(Peripheral peripheral) {
        super(RequestId.STREAMING, peripheral, 0, 4, (rd4) null);
        wd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public void a(long j) {
        this.B = j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0058, code lost:
        if (r4 != null) goto L_0x005d;
     */
    @DexIgnore
    public void b(d20 d20) {
        Object obj;
        wd4.b(d20, "characteristicChangedNotification");
        if (d20.a() == GattCharacteristic.CharacteristicId.ASYNC) {
            AsyncEvent a = e30.b.a(i().k(), d20.b());
            ea0 ea0 = ea0.l;
            EventType eventType = EventType.RESPONSE;
            String k = i().k();
            String j = j();
            String k2 = k();
            JSONObject a2 = xa0.a(new JSONObject(), JSONKey.RAW_DATA, l90.a(d20.b(), (String) null, 1, (Object) null));
            JSONKey jSONKey = JSONKey.DEVICE_EVENT;
            if (a != null) {
                obj = a.toJSONObject();
            }
            obj = JSONObject.NULL;
            AsyncEvent asyncEvent = a;
            SdkLogEntry sdkLogEntry = r3;
            SdkLogEntry sdkLogEntry2 = new SdkLogEntry("streaming", eventType, k, j, k2, true, (String) null, (DeviceInformation) null, (fa0) null, xa0.a(a2, jSONKey, obj), 448, (rd4) null);
            ea0.b(sdkLogEntry);
            if (asyncEvent != null) {
                jd4<? super AsyncEvent, cb4> jd4 = this.A;
                if (jd4 != null) {
                    cb4 invoke = jd4.invoke(asyncEvent);
                    return;
                }
                return;
            }
        }
    }

    @DexIgnore
    public final i70 d(jd4<? super AsyncEvent, cb4> jd4) {
        wd4.b(jd4, "actionOnEventReceived");
        this.A = jd4;
        return this;
    }

    @DexIgnore
    public BluetoothCommand h() {
        return null;
    }

    @DexIgnore
    public long m() {
        return this.B;
    }

    @DexIgnore
    public void s() {
    }

    @DexIgnore
    public void a(e20 e20) {
        wd4.b(e20, "connectionStateChangedNotification");
        if (e20.a() == Peripheral.State.DISCONNECTED) {
            e30.b.a(i().k());
        }
    }
}
