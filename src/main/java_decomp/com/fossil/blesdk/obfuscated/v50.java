package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.data.notification.AppNotification;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.phase.TransmitDataPhase;
import com.fossil.blesdk.setting.JSONKey;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v50 extends TransmitDataPhase {
    @DexIgnore
    public /* final */ AppNotification P;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ v50(Peripheral peripheral, Phase.a aVar, AppNotification appNotification, short s, String str, int i, rd4 rd4) {
        this(peripheral, aVar, appNotification, r4, str);
        short b = (i & 8) != 0 ? a50.b.b(peripheral.k(), FileType.NOTIFICATION) : s;
        if ((i & 16) != 0) {
            str = UUID.randomUUID().toString();
            wd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public byte[] F() {
        o20 o20 = o20.c;
        short A = A();
        Version version = e().getDeviceInformation().getSupportedFilesVersion$blesdk_productionRelease().get(Short.valueOf(FileType.NOTIFICATION.getFileHandleMask$blesdk_productionRelease()));
        if (version == null) {
            version = va0.y.g();
        }
        return o20.a(A, version, this.P);
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.NOTIFICATION, this.P.toJSONObject());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public v50(Peripheral peripheral, Phase.a aVar, AppNotification appNotification, short s, String str) {
        super(peripheral, aVar, PhaseId.SEND_APP_NOTIFICATION, true, s, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, r9, 32, (rd4) null);
        wd4.b(peripheral, "peripheral");
        wd4.b(aVar, "delegate");
        wd4.b(appNotification, "appNotification");
        String str2 = str;
        wd4.b(str2, "phaseUuid");
        this.P = appNotification;
    }
}
