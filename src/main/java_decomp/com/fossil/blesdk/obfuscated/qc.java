package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import androidx.core.os.OperationCanceledException;
import com.fossil.blesdk.obfuscated.rc;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qc extends pc<Cursor> {
    @DexIgnore
    public /* final */ rc<Cursor>.a a; // = new rc.a();
    @DexIgnore
    public Uri b;
    @DexIgnore
    public String[] c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String[] e;
    @DexIgnore
    public String f;
    @DexIgnore
    public Cursor g;
    @DexIgnore
    public n7 h;

    @DexIgnore
    public qc(Context context, Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        super(context);
        this.b = uri;
        this.c = strArr;
        this.d = str;
        this.e = strArr2;
        this.f = str2;
    }

    @DexIgnore
    public void cancelLoadInBackground() {
        super.cancelLoadInBackground();
        synchronized (this) {
            if (this.h != null) {
                this.h.a();
            }
        }
    }

    @DexIgnore
    @Deprecated
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.print("mUri=");
        printWriter.println(this.b);
        printWriter.print(str);
        printWriter.print("mProjection=");
        printWriter.println(Arrays.toString(this.c));
        printWriter.print(str);
        printWriter.print("mSelection=");
        printWriter.println(this.d);
        printWriter.print(str);
        printWriter.print("mSelectionArgs=");
        printWriter.println(Arrays.toString(this.e));
        printWriter.print(str);
        printWriter.print("mSortOrder=");
        printWriter.println(this.f);
        printWriter.print(str);
        printWriter.print("mCursor=");
        printWriter.println(this.g);
        printWriter.print(str);
        printWriter.print("mContentChanged=");
        printWriter.println(this.mContentChanged);
    }

    @DexIgnore
    public void onReset() {
        super.onReset();
        onStopLoading();
        Cursor cursor = this.g;
        if (cursor != null && !cursor.isClosed()) {
            this.g.close();
        }
        this.g = null;
    }

    @DexIgnore
    public void onStartLoading() {
        Cursor cursor = this.g;
        if (cursor != null) {
            deliverResult(cursor);
        }
        if (takeContentChanged() || this.g == null) {
            forceLoad();
        }
    }

    @DexIgnore
    public void onStopLoading() {
        cancelLoad();
    }

    @DexIgnore
    public void deliverResult(Cursor cursor) {
        if (!isReset()) {
            Cursor cursor2 = this.g;
            this.g = cursor;
            if (isStarted()) {
                super.deliverResult(cursor);
            }
            if (cursor2 != null && cursor2 != cursor && !cursor2.isClosed()) {
                cursor2.close();
            }
        } else if (cursor != null) {
            cursor.close();
        }
    }

    @DexIgnore
    public Cursor loadInBackground() {
        Cursor a2;
        synchronized (this) {
            if (!isLoadInBackgroundCanceled()) {
                this.h = new n7();
            } else {
                throw new OperationCanceledException();
            }
        }
        try {
            a2 = j6.a(getContext().getContentResolver(), this.b, this.c, this.d, this.e, this.f, this.h);
            if (a2 != null) {
                a2.getCount();
                a2.registerContentObserver(this.a);
            }
            synchronized (this) {
                this.h = null;
            }
            return a2;
        } catch (RuntimeException e2) {
            a2.close();
            throw e2;
        } catch (Throwable th) {
            synchronized (this) {
                this.h = null;
                throw th;
            }
        }
    }

    @DexIgnore
    public void onCanceled(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }
}
