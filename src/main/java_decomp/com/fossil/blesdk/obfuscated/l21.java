package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.he0;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class l21<T extends IInterface> extends pj0<T> {
    @DexIgnore
    public l21(Context context, Looper looper, int i, he0.b bVar, he0.c cVar, lj0 lj0) {
        super(context, looper, i, lj0, bVar, cVar);
    }

    @DexIgnore
    public boolean B() {
        return true;
    }

    @DexIgnore
    public Set<Scope> a(Set<Scope> set) {
        return pq0.a(set);
    }

    @DexIgnore
    public boolean l() {
        return !mm0.b(t());
    }
}
