package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nz0 extends jz0 {
    @DexIgnore
    public final void a(Throwable th, Throwable th2) {
        th.addSuppressed(th2);
    }
}
