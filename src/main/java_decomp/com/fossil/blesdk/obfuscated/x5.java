package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.ActivityOptions;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class x5 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends x5 {
        @DexIgnore
        public /* final */ ActivityOptions a;

        @DexIgnore
        public a(ActivityOptions activityOptions) {
            this.a = activityOptions;
        }

        @DexIgnore
        public Bundle a() {
            return this.a.toBundle();
        }
    }

    @DexIgnore
    public static x5 a(Activity activity, g8<View, String>... g8VarArr) {
        if (Build.VERSION.SDK_INT < 21) {
            return new x5();
        }
        Pair[] pairArr = null;
        if (g8VarArr != null) {
            pairArr = new Pair[g8VarArr.length];
            for (int i = 0; i < g8VarArr.length; i++) {
                pairArr[i] = Pair.create(g8VarArr[i].a, g8VarArr[i].b);
            }
        }
        return new a(ActivityOptions.makeSceneTransitionAnimation(activity, pairArr));
    }

    @DexIgnore
    public Bundle a() {
        return null;
    }
}
