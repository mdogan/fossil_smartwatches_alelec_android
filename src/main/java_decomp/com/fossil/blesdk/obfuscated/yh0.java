package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.af0;
import com.fossil.blesdk.obfuscated.we0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yh0 extends uh0<Boolean> {
    @DexIgnore
    public /* final */ af0.a<?> b;

    @DexIgnore
    public yh0(af0.a<?> aVar, yn1<Boolean> yn1) {
        super(4, yn1);
        this.b = aVar;
    }

    @DexIgnore
    public final /* bridge */ /* synthetic */ void a(kf0 kf0, boolean z) {
    }

    @DexIgnore
    public final xd0[] b(we0.a<?> aVar) {
        fh0 fh0 = aVar.l().get(this.b);
        if (fh0 == null) {
            return null;
        }
        return fh0.a.c();
    }

    @DexIgnore
    public final boolean c(we0.a<?> aVar) {
        fh0 fh0 = aVar.l().get(this.b);
        return fh0 != null && fh0.a.d();
    }

    @DexIgnore
    public final void d(we0.a<?> aVar) throws RemoteException {
        fh0 remove = aVar.l().remove(this.b);
        if (remove != null) {
            remove.b.a(aVar.f(), this.a);
            remove.a.a();
            return;
        }
        this.a.b(false);
    }
}
