package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import com.zendesk.sdk.attachment.AttachmentHelper;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.Closeable;
import java.io.InputStream;
import java.util.Collection;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class j74 extends f54 implements o74 {
    @DexIgnore
    public j74(w44 w44, String str, String str2, a74 a74, HttpMethod httpMethod) {
        super(w44, str, str2, a74, httpMethod);
    }

    @DexIgnore
    public boolean a(m74 m74) {
        HttpRequest a = a();
        a(a, m74);
        b(a, m74);
        z44 g = r44.g();
        g.d("Fabric", "Sending app info to " + b());
        if (m74.j != null) {
            z44 g2 = r44.g();
            g2.d("Fabric", "App icon hash is " + m74.j.a);
            z44 g3 = r44.g();
            g3.d("Fabric", "App icon size is " + m74.j.c + "x" + m74.j.d);
        }
        int g4 = a.g();
        String str = "POST".equals(a.m()) ? "Create" : "Update";
        z44 g5 = r44.g();
        g5.d("Fabric", str + " app request ID: " + a.c("X-REQUEST-ID"));
        z44 g6 = r44.g();
        g6.d("Fabric", "Result was " + g4);
        return x54.a(g4) == 0;
    }

    @DexIgnore
    public final HttpRequest b(HttpRequest httpRequest, m74 m74) {
        httpRequest.e("app[identifier]", m74.b);
        httpRequest.e("app[name]", m74.f);
        httpRequest.e("app[display_version]", m74.c);
        httpRequest.e("app[build_version]", m74.d);
        httpRequest.a("app[source]", (Number) Integer.valueOf(m74.g));
        httpRequest.e("app[minimum_sdk_version]", m74.h);
        httpRequest.e("app[built_sdk_version]", m74.i);
        if (!CommonUtils.b(m74.e)) {
            httpRequest.e("app[instance_identifier]", m74.e);
        }
        if (m74.j != null) {
            InputStream inputStream = null;
            try {
                inputStream = this.e.l().getResources().openRawResource(m74.j.b);
                httpRequest.e("app[icon][hash]", m74.j.a);
                httpRequest.a("app[icon][data]", "icon.png", AttachmentHelper.DEFAULT_MIMETYPE, inputStream);
                httpRequest.a("app[icon][width]", (Number) Integer.valueOf(m74.j.c));
                httpRequest.a("app[icon][height]", (Number) Integer.valueOf(m74.j.d));
            } catch (Resources.NotFoundException e) {
                z44 g = r44.g();
                g.e("Fabric", "Failed to find app icon with resource ID: " + m74.j.b, e);
            } catch (Throwable th) {
                CommonUtils.a((Closeable) inputStream, "Failed to close app icon InputStream.");
                throw th;
            }
            CommonUtils.a((Closeable) inputStream, "Failed to close app icon InputStream.");
        }
        Collection<y44> collection = m74.k;
        if (collection != null) {
            for (y44 next : collection) {
                httpRequest.e(b(next), next.c());
                httpRequest.e(a(next), next.a());
            }
        }
        return httpRequest;
    }

    @DexIgnore
    public final HttpRequest a(HttpRequest httpRequest, m74 m74) {
        httpRequest.c("X-CRASHLYTICS-API-KEY", m74.a);
        httpRequest.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        httpRequest.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.e.r());
        return httpRequest;
    }

    @DexIgnore
    public String a(y44 y44) {
        return String.format(Locale.US, "app[build][libraries][%s][type]", new Object[]{y44.b()});
    }

    @DexIgnore
    public String b(y44 y44) {
        return String.format(Locale.US, "app[build][libraries][%s][version]", new Object[]{y44.b()});
    }
}
