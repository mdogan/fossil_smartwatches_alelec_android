package com.fossil.blesdk.obfuscated;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class pk1 implements Runnable {
    @DexIgnore
    public /* final */ ok1 e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ ug1 g;
    @DexIgnore
    public /* final */ Intent h;

    @DexIgnore
    public pk1(ok1 ok1, int i, ug1 ug1, Intent intent) {
        this.e = ok1;
        this.f = i;
        this.g = ug1;
        this.h = intent;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f, this.g, this.h);
    }
}
