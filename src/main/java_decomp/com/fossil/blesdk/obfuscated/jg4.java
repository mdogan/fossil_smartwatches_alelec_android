package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jg4 extends fi4 {
    @DexIgnore
    public /* final */ Thread j;

    @DexIgnore
    public jg4(Thread thread) {
        wd4.b(thread, "thread");
        this.j = thread;
    }

    @DexIgnore
    public Thread I() {
        return this.j;
    }
}
