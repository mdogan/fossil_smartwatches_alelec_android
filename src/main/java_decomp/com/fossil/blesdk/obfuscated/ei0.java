package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.content.DialogInterface;
import com.google.android.gms.common.api.GoogleApiActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ei0 implements Runnable {
    @DexIgnore
    public /* final */ di0 e;
    @DexIgnore
    public /* final */ /* synthetic */ ci0 f;

    @DexIgnore
    public ei0(ci0 ci0, di0 di0) {
        this.f = ci0;
        this.e = di0;
    }

    @DexIgnore
    public final void run() {
        if (this.f.f) {
            vd0 a = this.e.a();
            if (a.K()) {
                ci0 ci0 = this.f;
                ci0.e.startActivityForResult(GoogleApiActivity.a(ci0.a(), a.J(), this.e.b(), false), 1);
            } else if (this.f.i.c(a.H())) {
                ci0 ci02 = this.f;
                ci02.i.a(ci02.a(), this.f.e, a.H(), 2, this.f);
            } else if (a.H() == 18) {
                Dialog a2 = yd0.a(this.f.a(), (DialogInterface.OnCancelListener) this.f);
                ci0 ci03 = this.f;
                ci03.i.a(ci03.a().getApplicationContext(), (bh0) new fi0(this, a2));
            } else {
                this.f.a(a, this.e.b());
            }
        }
    }
}
