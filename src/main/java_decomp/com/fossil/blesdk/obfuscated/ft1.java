package com.fossil.blesdk.obfuscated;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ft1 extends Drawable implements d7 {
    @DexIgnore
    public Paint.Style A;
    @DexIgnore
    public PorterDuffColorFilter B;
    @DexIgnore
    public PorterDuff.Mode C;
    @DexIgnore
    public ColorStateList D;
    @DexIgnore
    public /* final */ Paint e; // = new Paint();
    @DexIgnore
    public /* final */ Matrix[] f; // = new Matrix[4];
    @DexIgnore
    public /* final */ Matrix[] g; // = new Matrix[4];
    @DexIgnore
    public /* final */ gt1[] h; // = new gt1[4];
    @DexIgnore
    public /* final */ Matrix i; // = new Matrix();
    @DexIgnore
    public /* final */ Path j; // = new Path();
    @DexIgnore
    public /* final */ PointF k; // = new PointF();
    @DexIgnore
    public /* final */ gt1 l; // = new gt1();
    @DexIgnore
    public /* final */ Region m; // = new Region();
    @DexIgnore
    public /* final */ Region n; // = new Region();
    @DexIgnore
    public /* final */ float[] o; // = new float[2];
    @DexIgnore
    public /* final */ float[] p; // = new float[2];
    @DexIgnore
    public ht1 q; // = null;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public float t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public float y;
    @DexIgnore
    public float z;

    @DexIgnore
    public ft1(ht1 ht1) {
        this.r = false;
        this.s = false;
        this.t = 1.0f;
        this.u = -16777216;
        this.v = 5;
        this.w = 10;
        this.x = 255;
        this.y = 1.0f;
        this.z = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.A = Paint.Style.FILL_AND_STROKE;
        this.C = PorterDuff.Mode.SRC_IN;
        this.D = null;
        this.q = ht1;
        for (int i2 = 0; i2 < 4; i2++) {
            this.f[i2] = new Matrix();
            this.g[i2] = new Matrix();
            this.h[i2] = new gt1();
        }
    }

    @DexIgnore
    public static int a(int i2, int i3) {
        return (i2 * (i3 + (i3 >>> 7))) >>> 8;
    }

    @DexIgnore
    public void a(boolean z2) {
        this.r = z2;
        invalidateSelf();
    }

    @DexIgnore
    public ColorStateList b() {
        return this.D;
    }

    @DexIgnore
    public final void c(int i2, int i3, int i4) {
        a(i2, i3, i4, this.k);
        a(i2).a(a(i2, i3, i4), this.t, this.h[i2]);
        this.f[i2].reset();
        Matrix matrix = this.f[i2];
        PointF pointF = this.k;
        matrix.setTranslate(pointF.x, pointF.y);
        this.f[i2].preRotate((float) Math.toDegrees((double) (b(((i2 - 1) + 4) % 4, i3, i4) + 1.5707964f)));
    }

    @DexIgnore
    public final void d(int i2, int i3, int i4) {
        float[] fArr = this.o;
        gt1[] gt1Arr = this.h;
        fArr[0] = gt1Arr[i2].c;
        fArr[1] = gt1Arr[i2].d;
        this.f[i2].mapPoints(fArr);
        float b = b(i2, i3, i4);
        this.g[i2].reset();
        Matrix matrix = this.g[i2];
        float[] fArr2 = this.o;
        matrix.setTranslate(fArr2[0], fArr2[1]);
        this.g[i2].preRotate((float) Math.toDegrees((double) b));
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        this.e.setColorFilter(this.B);
        int alpha = this.e.getAlpha();
        this.e.setAlpha(a(alpha, this.x));
        this.e.setStrokeWidth(this.z);
        this.e.setStyle(this.A);
        int i2 = this.v;
        if (i2 > 0 && this.r) {
            this.e.setShadowLayer((float) this.w, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i2, this.u);
        }
        if (this.q != null) {
            a(canvas.getWidth(), canvas.getHeight(), this.j);
            canvas.drawPath(this.j, this.e);
        } else {
            canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) canvas.getWidth(), (float) canvas.getHeight(), this.e);
        }
        this.e.setAlpha(alpha);
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public Region getTransparentRegion() {
        Rect bounds = getBounds();
        this.m.set(bounds);
        a(bounds.width(), bounds.height(), this.j);
        this.n.setPath(this.j, this.m);
        this.m.op(this.n, Region.Op.DIFFERENCE);
        return this.m;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        this.x = i2;
        invalidateSelf();
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.e.setColorFilter(colorFilter);
        invalidateSelf();
    }

    @DexIgnore
    public void setTint(int i2) {
        setTintList(ColorStateList.valueOf(i2));
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        this.D = colorStateList;
        c();
        invalidateSelf();
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        this.C = mode;
        c();
        invalidateSelf();
    }

    @DexIgnore
    public void b(int i2, int i3, Path path) {
        path.rewind();
        if (this.q != null) {
            for (int i4 = 0; i4 < 4; i4++) {
                c(i4, i2, i3);
                d(i4, i2, i3);
            }
            for (int i5 = 0; i5 < 4; i5++) {
                a(i5, path);
                b(i5, path);
            }
            path.close();
        }
    }

    @DexIgnore
    public float a() {
        return this.t;
    }

    @DexIgnore
    public void a(float f2) {
        this.t = f2;
        invalidateSelf();
    }

    @DexIgnore
    public void a(Paint.Style style) {
        this.A = style;
        invalidateSelf();
    }

    @DexIgnore
    public final void a(int i2, Path path) {
        float[] fArr = this.o;
        gt1[] gt1Arr = this.h;
        fArr[0] = gt1Arr[i2].a;
        fArr[1] = gt1Arr[i2].b;
        this.f[i2].mapPoints(fArr);
        if (i2 == 0) {
            float[] fArr2 = this.o;
            path.moveTo(fArr2[0], fArr2[1]);
        } else {
            float[] fArr3 = this.o;
            path.lineTo(fArr3[0], fArr3[1]);
        }
        this.h[i2].a(this.f[i2], path);
    }

    @DexIgnore
    public final void c() {
        ColorStateList colorStateList = this.D;
        if (colorStateList == null || this.C == null) {
            this.B = null;
            return;
        }
        int colorForState = colorStateList.getColorForState(getState(), 0);
        this.B = new PorterDuffColorFilter(colorForState, this.C);
        if (this.s) {
            this.u = colorForState;
        }
    }

    @DexIgnore
    public final void b(int i2, Path path) {
        int i3 = (i2 + 1) % 4;
        float[] fArr = this.o;
        gt1[] gt1Arr = this.h;
        fArr[0] = gt1Arr[i2].c;
        fArr[1] = gt1Arr[i2].d;
        this.f[i2].mapPoints(fArr);
        float[] fArr2 = this.p;
        gt1[] gt1Arr2 = this.h;
        fArr2[0] = gt1Arr2[i3].a;
        fArr2[1] = gt1Arr2[i3].b;
        this.f[i3].mapPoints(fArr2);
        float[] fArr3 = this.o;
        float f2 = fArr3[0];
        float[] fArr4 = this.p;
        this.l.b(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        b(i2).a((float) Math.hypot((double) (f2 - fArr4[0]), (double) (fArr3[1] - fArr4[1])), this.t, this.l);
        this.l.a(this.g[i2], path);
    }

    @DexIgnore
    public final dt1 a(int i2) {
        if (i2 == 1) {
            return this.q.h();
        }
        if (i2 == 2) {
            return this.q.c();
        }
        if (i2 != 3) {
            return this.q.g();
        }
        return this.q.b();
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, PointF pointF) {
        if (i2 == 1) {
            pointF.set((float) i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        } else if (i2 == 2) {
            pointF.set((float) i3, (float) i4);
        } else if (i2 != 3) {
            pointF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        } else {
            pointF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i4);
        }
    }

    @DexIgnore
    public final et1 b(int i2) {
        if (i2 == 1) {
            return this.q.e();
        }
        if (i2 == 2) {
            return this.q.a();
        }
        if (i2 != 3) {
            return this.q.f();
        }
        return this.q.d();
    }

    @DexIgnore
    public final float a(int i2, int i3, int i4) {
        a(((i2 - 1) + 4) % 4, i3, i4, this.k);
        PointF pointF = this.k;
        float f2 = pointF.x;
        float f3 = pointF.y;
        a((i2 + 1) % 4, i3, i4, pointF);
        PointF pointF2 = this.k;
        float f4 = pointF2.x;
        float f5 = pointF2.y;
        a(i2, i3, i4, pointF2);
        PointF pointF3 = this.k;
        float f6 = pointF3.x;
        float f7 = pointF3.y;
        float atan2 = ((float) Math.atan2((double) (f3 - f7), (double) (f2 - f6))) - ((float) Math.atan2((double) (f5 - f7), (double) (f4 - f6)));
        return atan2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? (float) (((double) atan2) + 6.283185307179586d) : atan2;
    }

    @DexIgnore
    public final float b(int i2, int i3, int i4) {
        a(i2, i3, i4, this.k);
        PointF pointF = this.k;
        float f2 = pointF.x;
        float f3 = pointF.y;
        a((i2 + 1) % 4, i3, i4, pointF);
        PointF pointF2 = this.k;
        return (float) Math.atan2((double) (pointF2.y - f3), (double) (pointF2.x - f2));
    }

    @DexIgnore
    public final void a(int i2, int i3, Path path) {
        b(i2, i3, path);
        if (this.y != 1.0f) {
            this.i.reset();
            Matrix matrix = this.i;
            float f2 = this.y;
            matrix.setScale(f2, f2, (float) (i2 / 2), (float) (i3 / 2));
            path.transform(this.i);
        }
    }
}
