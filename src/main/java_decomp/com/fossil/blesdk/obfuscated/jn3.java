package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jn3 implements Factory<hn3> {
    @DexIgnore
    public static hn3 a(in3 in3) {
        hn3 a = in3.a();
        o44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
