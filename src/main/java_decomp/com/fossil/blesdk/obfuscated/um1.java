package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;
import com.google.android.gms.dynamic.RemoteCreator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class um1 extends RemoteCreator<sm1> {
    @DexIgnore
    public static /* final */ um1 c; // = new um1();

    @DexIgnore
    public um1() {
        super("com.google.android.gms.plus.plusone.PlusOneButtonCreatorImpl");
    }

    @DexIgnore
    public static View a(Context context, int i, int i2, String str, int i3) {
        if (str != null) {
            try {
                return (View) vn0.d(((sm1) c.a(context)).a(vn0.a(context), i, i2, str, i3));
            } catch (Exception unused) {
                return new rm1(context, i);
            }
        } else {
            throw new NullPointerException();
        }
    }

    @DexIgnore
    public final /* synthetic */ Object a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.plus.internal.IPlusOneButtonCreator");
        return queryLocalInterface instanceof sm1 ? (sm1) queryLocalInterface : new tm1(iBinder);
    }
}
