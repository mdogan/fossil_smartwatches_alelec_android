package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dl2 {
    @DexIgnore
    public static /* final */ dl2 a; // = new dl2();

    @DexIgnore
    public final void a(View view, Context context) {
        wd4.b(view, "view");
        wd4.b(context, "context");
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @DexIgnore
    public final void b(View view, Context context) {
        wd4.b(view, "view");
        wd4.b(context, "context");
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(view, 2);
        }
    }
}
