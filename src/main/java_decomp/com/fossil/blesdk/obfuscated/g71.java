package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g71 extends b71<String> {
    @DexIgnore
    public g71(h71 h71, String str, String str2) {
        super(h71, str, str2, (c71) null);
    }

    @DexIgnore
    public final /* synthetic */ Object a(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        return null;
    }
}
