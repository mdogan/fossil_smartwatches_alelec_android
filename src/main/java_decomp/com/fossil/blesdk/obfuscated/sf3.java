package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sf3 implements Factory<HeartRateDetailPresenter> {
    @DexIgnore
    public static HeartRateDetailPresenter a(of3 of3, HeartRateSummaryRepository heartRateSummaryRepository, HeartRateSampleRepository heartRateSampleRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FitnessDataDao fitnessDataDao, WorkoutDao workoutDao, FitnessDatabase fitnessDatabase, i42 i42) {
        return new HeartRateDetailPresenter(of3, heartRateSummaryRepository, heartRateSampleRepository, userRepository, workoutSessionRepository, fitnessDataDao, workoutDao, fitnessDatabase, i42);
    }
}
