package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.TextPaint;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.r6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ys1 {
    @DexIgnore
    public /* final */ float a;
    @DexIgnore
    public /* final */ ColorStateList b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ ColorStateList f;
    @DexIgnore
    public /* final */ float g;
    @DexIgnore
    public /* final */ float h;
    @DexIgnore
    public /* final */ float i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public boolean k; // = false;
    @DexIgnore
    public Typeface l;

    @DexIgnore
    public ys1(Context context, int i2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i2, dr1.TextAppearance);
        this.a = obtainStyledAttributes.getDimension(dr1.TextAppearance_android_textSize, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.b = xs1.a(context, obtainStyledAttributes, dr1.TextAppearance_android_textColor);
        xs1.a(context, obtainStyledAttributes, dr1.TextAppearance_android_textColorHint);
        xs1.a(context, obtainStyledAttributes, dr1.TextAppearance_android_textColorLink);
        this.c = obtainStyledAttributes.getInt(dr1.TextAppearance_android_textStyle, 0);
        this.d = obtainStyledAttributes.getInt(dr1.TextAppearance_android_typeface, 1);
        int a2 = xs1.a(obtainStyledAttributes, dr1.TextAppearance_fontFamily, dr1.TextAppearance_android_fontFamily);
        this.j = obtainStyledAttributes.getResourceId(a2, 0);
        this.e = obtainStyledAttributes.getString(a2);
        obtainStyledAttributes.getBoolean(dr1.TextAppearance_textAllCaps, false);
        this.f = xs1.a(context, obtainStyledAttributes, dr1.TextAppearance_android_shadowColor);
        this.g = obtainStyledAttributes.getFloat(dr1.TextAppearance_android_shadowDx, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.h = obtainStyledAttributes.getFloat(dr1.TextAppearance_android_shadowDy, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.i = obtainStyledAttributes.getFloat(dr1.TextAppearance_android_shadowRadius, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public void b(Context context, TextPaint textPaint, r6.a aVar) {
        c(context, textPaint, aVar);
        ColorStateList colorStateList = this.b;
        textPaint.setColor(colorStateList != null ? colorStateList.getColorForState(textPaint.drawableState, colorStateList.getDefaultColor()) : -16777216);
        float f2 = this.i;
        float f3 = this.g;
        float f4 = this.h;
        ColorStateList colorStateList2 = this.f;
        textPaint.setShadowLayer(f2, f3, f4, colorStateList2 != null ? colorStateList2.getColorForState(textPaint.drawableState, colorStateList2.getDefaultColor()) : 0);
    }

    @DexIgnore
    public void c(Context context, TextPaint textPaint, r6.a aVar) {
        if (zs1.a()) {
            a(textPaint, a(context));
            return;
        }
        a(context, textPaint, aVar);
        if (!this.k) {
            a(textPaint, this.l);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends r6.a {
        @DexIgnore
        public /* final */ /* synthetic */ TextPaint a;
        @DexIgnore
        public /* final */ /* synthetic */ r6.a b;

        @DexIgnore
        public a(TextPaint textPaint, r6.a aVar) {
            this.a = textPaint;
            this.b = aVar;
        }

        @DexIgnore
        public void a(Typeface typeface) {
            ys1 ys1 = ys1.this;
            Typeface unused = ys1.l = Typeface.create(typeface, ys1.c);
            ys1.this.a(this.a, typeface);
            boolean unused2 = ys1.this.k = true;
            this.b.a(typeface);
        }

        @DexIgnore
        public void a(int i) {
            ys1.this.a();
            boolean unused = ys1.this.k = true;
            this.b.a(i);
        }
    }

    @DexIgnore
    public Typeface a(Context context) {
        if (this.k) {
            return this.l;
        }
        if (!context.isRestricted()) {
            try {
                this.l = r6.a(context, this.j);
                if (this.l != null) {
                    this.l = Typeface.create(this.l, this.c);
                }
            } catch (Resources.NotFoundException | UnsupportedOperationException unused) {
            } catch (Exception e2) {
                Log.d("TextAppearance", "Error loading font " + this.e, e2);
            }
        }
        a();
        this.k = true;
        return this.l;
    }

    @DexIgnore
    public void a(Context context, TextPaint textPaint, r6.a aVar) {
        if (this.k) {
            a(textPaint, this.l);
            return;
        }
        a();
        if (context.isRestricted()) {
            this.k = true;
            a(textPaint, this.l);
            return;
        }
        try {
            r6.a(context, this.j, new a(textPaint, aVar), (Handler) null);
        } catch (Resources.NotFoundException | UnsupportedOperationException unused) {
        } catch (Exception e2) {
            Log.d("TextAppearance", "Error loading font " + this.e, e2);
        }
    }

    @DexIgnore
    public final void a() {
        if (this.l == null) {
            this.l = Typeface.create(this.e, this.c);
        }
        if (this.l == null) {
            int i2 = this.d;
            if (i2 == 1) {
                this.l = Typeface.SANS_SERIF;
            } else if (i2 == 2) {
                this.l = Typeface.SERIF;
            } else if (i2 != 3) {
                this.l = Typeface.DEFAULT;
            } else {
                this.l = Typeface.MONOSPACE;
            }
            Typeface typeface = this.l;
            if (typeface != null) {
                this.l = Typeface.create(typeface, this.c);
            }
        }
    }

    @DexIgnore
    public void a(TextPaint textPaint, Typeface typeface) {
        textPaint.setTypeface(typeface);
        int i2 = (~typeface.getStyle()) & this.c;
        textPaint.setFakeBoldText((i2 & 1) != 0);
        textPaint.setTextSkewX((i2 & 2) != 0 ? -0.25f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        textPaint.setTextSize(this.a);
    }
}
