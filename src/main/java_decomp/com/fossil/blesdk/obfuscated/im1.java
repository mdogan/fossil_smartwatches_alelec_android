package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class im1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<im1> CREATOR; // = new om1();
    @DexIgnore
    public static /* final */ byte[][] m; // = new byte[0][];
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ byte[] f;
    @DexIgnore
    public /* final */ byte[][] g;
    @DexIgnore
    public /* final */ byte[][] h;
    @DexIgnore
    public /* final */ byte[][] i;
    @DexIgnore
    public /* final */ byte[][] j;
    @DexIgnore
    public /* final */ int[] k;
    @DexIgnore
    public /* final */ byte[][] l;

    @DexIgnore
    public interface a {
    }

    /*
    static {
        byte[][] bArr = m;
        new im1("", (byte[]) null, bArr, bArr, bArr, bArr, (int[]) null, (byte[][]) null);
        new km1();
        new lm1();
        new mm1();
        new nm1();
    }
    */

    @DexIgnore
    public im1(String str, byte[] bArr, byte[][] bArr2, byte[][] bArr3, byte[][] bArr4, byte[][] bArr5, int[] iArr, byte[][] bArr6) {
        this.e = str;
        this.f = bArr;
        this.g = bArr2;
        this.h = bArr3;
        this.i = bArr4;
        this.j = bArr5;
        this.k = iArr;
        this.l = bArr6;
    }

    @DexIgnore
    public static List<Integer> a(int[] iArr) {
        if (iArr == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int valueOf : iArr) {
            arrayList.add(Integer.valueOf(valueOf));
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    @DexIgnore
    public static List<String> a(byte[][] bArr) {
        if (bArr == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(bArr.length);
        for (byte[] encodeToString : bArr) {
            arrayList.add(Base64.encodeToString(encodeToString, 3));
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    @DexIgnore
    public static void a(StringBuilder sb, String str, byte[][] bArr) {
        String str2;
        sb.append(str);
        sb.append(SimpleComparison.EQUAL_TO_OPERATION);
        if (bArr == null) {
            str2 = "null";
        } else {
            sb.append("(");
            int length = bArr.length;
            int i2 = 0;
            boolean z = true;
            while (i2 < length) {
                byte[] bArr2 = bArr[i2];
                if (!z) {
                    sb.append(", ");
                }
                sb.append("'");
                sb.append(Base64.encodeToString(bArr2, 3));
                sb.append("'");
                i2++;
                z = false;
            }
            str2 = ")";
        }
        sb.append(str2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof im1) {
            im1 im1 = (im1) obj;
            return qm1.a(this.e, im1.e) && Arrays.equals(this.f, im1.f) && qm1.a(a(this.g), a(im1.g)) && qm1.a(a(this.h), a(im1.h)) && qm1.a(a(this.i), a(im1.i)) && qm1.a(a(this.j), a(im1.j)) && qm1.a(a(this.k), a(im1.k)) && qm1.a(a(this.l), a(im1.l));
        }
    }

    @DexIgnore
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("ExperimentTokens");
        sb.append("(");
        String str2 = this.e;
        if (str2 == null) {
            str = "null";
        } else {
            StringBuilder sb2 = new StringBuilder(String.valueOf(str2).length() + 2);
            sb2.append("'");
            sb2.append(str2);
            sb2.append("'");
            str = sb2.toString();
        }
        sb.append(str);
        sb.append(", ");
        byte[] bArr = this.f;
        sb.append("direct");
        sb.append(SimpleComparison.EQUAL_TO_OPERATION);
        if (bArr == null) {
            sb.append("null");
        } else {
            sb.append("'");
            sb.append(Base64.encodeToString(bArr, 3));
            sb.append("'");
        }
        sb.append(", ");
        a(sb, "GAIA", this.g);
        sb.append(", ");
        a(sb, "PSEUDO", this.h);
        sb.append(", ");
        a(sb, "ALWAYS", this.i);
        sb.append(", ");
        a(sb, "OTHER", this.j);
        sb.append(", ");
        int[] iArr = this.k;
        sb.append("weak");
        sb.append(SimpleComparison.EQUAL_TO_OPERATION);
        if (iArr == null) {
            sb.append("null");
        } else {
            sb.append("(");
            int length = iArr.length;
            int i2 = 0;
            boolean z = true;
            while (i2 < length) {
                int i3 = iArr[i2];
                if (!z) {
                    sb.append(", ");
                }
                sb.append(i3);
                i2++;
                z = false;
            }
            sb.append(")");
        }
        sb.append(", ");
        a(sb, "directs", this.l);
        sb.append(")");
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = lk0.a(parcel);
        lk0.a(parcel, 2, this.e, false);
        lk0.a(parcel, 3, this.f, false);
        lk0.a(parcel, 4, this.g, false);
        lk0.a(parcel, 5, this.h, false);
        lk0.a(parcel, 6, this.i, false);
        lk0.a(parcel, 7, this.j, false);
        lk0.a(parcel, 8, this.k, false);
        lk0.a(parcel, 9, this.l, false);
        lk0.a(parcel, a2);
    }
}
