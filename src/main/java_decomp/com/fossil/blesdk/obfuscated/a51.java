package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.ee0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a51 extends n31 {
    @DexIgnore
    public /* final */ /* synthetic */ sc1 s;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a51(y41 y41, he0 he0, sc1 sc1) {
        super(he0);
        this.s = sc1;
    }

    @DexIgnore
    public final /* synthetic */ void a(ee0.b bVar) throws RemoteException {
        ((g41) bVar).a(bf0.a(this.s, sc1.class.getSimpleName()), new o31(this));
    }
}
