package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ta1 implements Iterator<Map.Entry<K, V>> {
    @DexIgnore
    public int e;
    @DexIgnore
    public Iterator<Map.Entry<K, V>> f;
    @DexIgnore
    public /* final */ /* synthetic */ ra1 g;

    @DexIgnore
    public ta1(ra1 ra1) {
        this.g = ra1;
        this.e = this.g.f.size();
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> a() {
        if (this.f == null) {
            this.f = this.g.j.entrySet().iterator();
        }
        return this.f;
    }

    @DexIgnore
    public final boolean hasNext() {
        int i = this.e;
        return (i > 0 && i <= this.g.f.size()) || a().hasNext();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        if (a().hasNext()) {
            return (Map.Entry) a().next();
        }
        List b = this.g.f;
        int i = this.e - 1;
        this.e = i;
        return (Map.Entry) b.get(i);
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public /* synthetic */ ta1(ra1 ra1, sa1 sa1) {
        this(ra1);
    }
}
