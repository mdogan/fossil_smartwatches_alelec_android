package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class w00<V, E> {
    @DexIgnore
    public V a;
    @DexIgnore
    public E b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public CopyOnWriteArrayList<jd4<V, cb4>> d; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<jd4<E, cb4>> e; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<jd4<cb4, cb4>> f; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<jd4<Float, cb4>> g; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<jd4<E, cb4>> h; // = new CopyOnWriteArrayList<>();

    @DexIgnore
    public final boolean a() {
        return c() || b();
    }

    @DexIgnore
    public final boolean b() {
        return this.b != null;
    }

    @DexIgnore
    public final boolean c() {
        return this.a != null;
    }

    @DexIgnore
    public w00<V, E> d(jd4<? super Float, cb4> jd4) {
        wd4.b(jd4, "actionOnProgressChange");
        if (!a()) {
            this.g.add(jd4);
        }
        return this;
    }

    @DexIgnore
    public w00<V, E> e(jd4<? super V, cb4> jd4) {
        wd4.b(jd4, "actionOnSuccess");
        if (!a()) {
            this.d.add(jd4);
        } else if (c()) {
            try {
                V v = this.a;
                if (v != null) {
                    jd4.invoke(v);
                } else {
                    wd4.a();
                    throw null;
                }
            } catch (Exception e2) {
                ea0.l.a(e2);
            }
        }
        return this;
    }

    @DexIgnore
    public w00<V, E> a(jd4<? super cb4, cb4> jd4) {
        wd4.b(jd4, "actionOnFinal");
        if (a()) {
            try {
                jd4.invoke(cb4.a);
            } catch (Exception e2) {
                ea0.l.a(e2);
            }
        } else {
            this.f.add(jd4);
        }
        return this;
    }

    @DexIgnore
    public w00<V, E> b(jd4<? super E, cb4> jd4) {
        wd4.b(jd4, "actionOnCancel");
        if (!a()) {
            this.h.add(jd4);
        }
        return this;
    }

    @DexIgnore
    public w00<V, E> c(jd4<? super E, cb4> jd4) {
        wd4.b(jd4, "actionOnError");
        if (!a()) {
            this.e.add(jd4);
        } else if (b()) {
            try {
                E e2 = this.b;
                if (e2 != null) {
                    jd4.invoke(e2);
                } else {
                    wd4.a();
                    throw null;
                }
            } catch (Exception e3) {
                ea0.l.a(e3);
            }
        }
        return this;
    }

    @DexIgnore
    public final synchronized void d() {
        for (jd4 invoke : this.f) {
            try {
                invoke.invoke(cb4.a);
            } catch (Exception e2) {
                ea0.l.a(e2);
            }
        }
        this.d.clear();
        this.e.clear();
        this.f.clear();
        this.g.clear();
        this.h.clear();
    }

    @DexIgnore
    public final synchronized void b(E e2) {
        wd4.b(e2, "error");
        if (!a()) {
            this.b = e2;
            for (jd4 jd4 : this.e) {
                try {
                    E e3 = this.b;
                    if (e3 != null) {
                        jd4.invoke(e3);
                    } else {
                        wd4.a();
                        throw null;
                    }
                } catch (Exception e4) {
                    ea0.l.a(e4);
                }
            }
            d();
        }
    }

    @DexIgnore
    public final synchronized void a(float f2) {
        for (jd4 invoke : this.g) {
            try {
                invoke.invoke(Float.valueOf(f2));
            } catch (Exception e2) {
                ea0.l.a(e2);
            }
        }
    }

    @DexIgnore
    public final synchronized void c(V v) {
        wd4.b(v, Constants.RESULT);
        if (!a()) {
            this.a = v;
            for (jd4 jd4 : this.d) {
                try {
                    V v2 = this.a;
                    if (v2 != null) {
                        jd4.invoke(v2);
                    } else {
                        wd4.a();
                        throw null;
                    }
                } catch (Exception e2) {
                    ea0.l.a(e2);
                }
            }
            d();
        }
    }

    @DexIgnore
    public final synchronized void a(E e2) {
        wd4.b(e2, "error");
        if (!a() && !this.c) {
            this.c = true;
            if (true ^ this.h.isEmpty()) {
                for (jd4 invoke : this.h) {
                    try {
                        invoke.invoke(e2);
                    } catch (Exception e3) {
                        ea0.l.a(e3);
                    }
                }
                this.c = false;
            } else {
                b(e2);
            }
        }
    }
}
