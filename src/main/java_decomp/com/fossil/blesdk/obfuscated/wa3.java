package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.blesdk.obfuscated.xr2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wa3 extends as2 implements va3 {
    @DexIgnore
    public ur3<ba2> j;
    @DexIgnore
    public ua3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "CaloriesOverviewWeekFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onCreateView");
        this.j = new ur3<>(this, (ba2) ra.a(layoutInflater, R.layout.fragment_calories_overview_week, viewGroup, false, O0()));
        ur3<ba2> ur3 = this.j;
        if (ur3 != null) {
            ba2 a2 = ur3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onResume");
        ua3 ua3 = this.k;
        if (ua3 != null) {
            ua3.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onStop");
        ua3 ua3 = this.k;
        if (ua3 != null) {
            ua3.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(xr2 xr2) {
        wd4.b(xr2, "baseModel");
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "showWeekDetails");
        ur3<ba2> ur3 = this.j;
        if (ur3 != null) {
            ba2 a2 = ur3.a();
            if (a2 != null) {
                OverviewWeekChart overviewWeekChart = a2.q;
                if (overviewWeekChart != null) {
                    new ArrayList();
                    BarChart.c cVar = (BarChart.c) xr2;
                    cVar.b(xr2.a.a(cVar.c()));
                    xr2.a aVar = xr2.a;
                    wd4.a((Object) overviewWeekChart, "it");
                    Context context = overviewWeekChart.getContext();
                    wd4.a((Object) context, "it.context");
                    BarChart.a((BarChart) overviewWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
                    overviewWeekChart.a(xr2);
                }
            }
        }
    }

    @DexIgnore
    public void a(ua3 ua3) {
        wd4.b(ua3, "presenter");
        this.k = ua3;
    }
}
