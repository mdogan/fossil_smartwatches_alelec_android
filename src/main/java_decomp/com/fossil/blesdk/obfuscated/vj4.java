package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import java.lang.reflect.Constructor;
import kotlin.Result;
import kotlin.TypeCastException;
import kotlinx.coroutines.android.HandlerContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vj4 {
    /*
    static {
        Object obj;
        try {
            Result.a aVar = Result.Companion;
            Looper mainLooper = Looper.getMainLooper();
            wd4.a((Object) mainLooper, "Looper.getMainLooper()");
            obj = Result.m3constructorimpl(new HandlerContext(a(mainLooper, true), "Main"));
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            obj = Result.m3constructorimpl(za4.a(th));
        }
        if (Result.m8isFailureimpl(obj)) {
            obj = null;
        }
        uj4 uj4 = (uj4) obj;
    }
    */

    @DexIgnore
    public static final Handler a(Looper looper, boolean z) {
        wd4.b(looper, "$this$asHandler");
        if (z) {
            int i = Build.VERSION.SDK_INT;
            if (i >= 16) {
                if (i >= 28) {
                    Object invoke = Handler.class.getDeclaredMethod("createAsync", new Class[]{Looper.class}).invoke((Object) null, new Object[]{looper});
                    if (invoke != null) {
                        return (Handler) invoke;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type android.os.Handler");
                }
                Class<Handler> cls = Handler.class;
                try {
                    Constructor<Handler> declaredConstructor = cls.getDeclaredConstructor(new Class[]{Looper.class, Handler.Callback.class, Boolean.TYPE});
                    wd4.a((Object) declaredConstructor, "Handler::class.java.getD\u2026:class.javaPrimitiveType)");
                    Handler newInstance = declaredConstructor.newInstance(new Object[]{looper, null, true});
                    wd4.a((Object) newInstance, "constructor.newInstance(this, null, true)");
                    return newInstance;
                } catch (NoSuchMethodException unused) {
                    return new Handler(looper);
                }
            }
        }
        return new Handler(looper);
    }
}
