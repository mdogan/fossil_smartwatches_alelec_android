package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.su0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ay0 extends su0<ay0, a> implements vv0 {
    @DexIgnore
    public static volatile dw0<ay0> zzbg;
    @DexIgnore
    public static /* final */ ay0 zztx; // = new ay0();
    @DexIgnore
    public int zzbb;
    @DexIgnore
    public int zztu;
    @DexIgnore
    public String zztv; // = "";
    @DexIgnore
    public String zztw; // = "";

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends su0.a<ay0, a> implements vv0 {
        @DexIgnore
        public a() {
            super(ay0.zztx);
        }

        @DexIgnore
        public /* synthetic */ a(by0 by0) {
            this();
        }
    }

    /*
    static {
        su0.a(ay0.class, zztx);
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v14, types: [com.fossil.blesdk.obfuscated.dw0<com.fossil.blesdk.obfuscated.ay0>, com.fossil.blesdk.obfuscated.su0$b] */
    public final Object a(int i, Object obj, Object obj2) {
        dw0<ay0> dw0;
        switch (by0.a[i - 1]) {
            case 1:
                return new ay0();
            case 2:
                return new a((by0) null);
            case 3:
                return su0.a((tv0) zztx, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0004\u0000\u0000\u0000\u0001\u0004\u0000\u0002\b\u0001\u0003\b\u0002", new Object[]{"zzbb", "zztu", "zztv", "zztw"});
            case 4:
                return zztx;
            case 5:
                dw0<ay0> dw02 = zzbg;
                dw0<ay0> dw03 = dw02;
                if (dw02 == null) {
                    synchronized (ay0.class) {
                        dw0<ay0> dw04 = zzbg;
                        dw0 = dw04;
                        if (dw04 == null) {
                            Object bVar = new su0.b(zztx);
                            zzbg = bVar;
                            dw0 = bVar;
                        }
                    }
                    dw03 = dw0;
                }
                return dw03;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
