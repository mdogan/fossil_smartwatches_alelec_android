package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jk4 extends bj4 implements th4 {
    @DexIgnore
    public /* final */ Throwable e;
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ jk4(Throwable th, String str, int i, rd4 rd4) {
        this(th, (i & 2) != 0 ? null : str);
    }

    @DexIgnore
    public bj4 C() {
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0023, code lost:
        if (r1 != null) goto L_0x0028;
     */
    @DexIgnore
    public final Void D() {
        String str;
        if (this.e != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Module with the Main dispatcher had failed to initialize");
            String str2 = this.f;
            if (str2 != null) {
                str = ". " + str2;
            }
            str = "";
            sb.append(str);
            throw new IllegalStateException(sb.toString(), this.e);
        }
        throw new IllegalStateException("Module with the Main dispatcher is missing. Add dependency providing the Main dispatcher, e.g. 'kotlinx-coroutines-android'");
    }

    @DexIgnore
    public boolean b(CoroutineContext coroutineContext) {
        wd4.b(coroutineContext, "context");
        D();
        throw null;
    }

    @DexIgnore
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append("Main[missing");
        if (this.e != null) {
            str = ", cause=" + this.e;
        } else {
            str = "";
        }
        sb.append(str);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    public jk4(Throwable th, String str) {
        this.e = th;
        this.f = str;
    }

    @DexIgnore
    public ai4 a(long j, Runnable runnable) {
        wd4.b(runnable, "block");
        D();
        throw null;
    }

    @DexIgnore
    public Void a(CoroutineContext coroutineContext, Runnable runnable) {
        wd4.b(coroutineContext, "context");
        wd4.b(runnable, "block");
        D();
        throw null;
    }

    @DexIgnore
    public Void a(long j, pg4<? super cb4> pg4) {
        wd4.b(pg4, "continuation");
        D();
        throw null;
    }
}
