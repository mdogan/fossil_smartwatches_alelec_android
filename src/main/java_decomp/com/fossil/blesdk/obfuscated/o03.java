package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o03 {
    @DexIgnore
    public /* final */ m03 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<ContactWrapper> c;
    @DexIgnore
    public /* final */ LoaderManager d;

    @DexIgnore
    public o03(m03 m03, int i, ArrayList<ContactWrapper> arrayList, LoaderManager loaderManager) {
        wd4.b(m03, "mView");
        wd4.b(loaderManager, "mLoaderManager");
        this.a = m03;
        this.b = i;
        this.c = arrayList;
        this.d = loaderManager;
    }

    @DexIgnore
    public final ArrayList<ContactWrapper> a() {
        ArrayList<ContactWrapper> arrayList = this.c;
        if (arrayList != null) {
            return arrayList;
        }
        return new ArrayList<>();
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final LoaderManager c() {
        return this.d;
    }

    @DexIgnore
    public final m03 d() {
        return this.a;
    }
}
