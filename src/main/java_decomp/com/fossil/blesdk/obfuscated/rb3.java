package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rb3 extends as2 implements qb3, RecyclerViewCalendar.d {
    @DexIgnore
    public ju3 j;
    @DexIgnore
    public ur3<dc2> k;
    @DexIgnore
    public pb3 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rb3 e;

        @DexIgnore
        public b(rb3 rb3, dc2 dc2) {
            this.e = rb3;
        }

        @DexIgnore
        public final void onClick(View view) {
            rb3.a(this.e).c().a(1);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewCalendar.e {
        @DexIgnore
        public /* final */ /* synthetic */ rb3 a;

        @DexIgnore
        public c(rb3 rb3) {
            this.a = rb3;
        }

        @DexIgnore
        public final void a(Calendar calendar) {
            pb3 b = this.a.l;
            if (b != null) {
                wd4.a((Object) calendar, "calendar");
                Date time = calendar.getTime();
                wd4.a((Object) time, "calendar.time");
                b.a(time);
            }
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ ju3 a(rb3 rb3) {
        ju3 ju3 = rb3.j;
        if (ju3 != null) {
            return ju3;
        }
        wd4.d("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "GoalTrackingOverviewMonthFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public void c(boolean z) {
        ur3<dc2> ur3 = this.k;
        if (ur3 != null) {
            dc2 a2 = ur3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                RecyclerViewCalendar recyclerViewCalendar = a2.q;
                wd4.a((Object) recyclerViewCalendar, "binding.calendarMonth");
                recyclerViewCalendar.setVisibility(4);
                ConstraintLayout constraintLayout = a2.r;
                wd4.a((Object) constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            RecyclerViewCalendar recyclerViewCalendar2 = a2.q;
            wd4.a((Object) recyclerViewCalendar2, "binding.calendarMonth");
            recyclerViewCalendar2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.r;
            wd4.a((Object) constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onCreateView");
        dc2 dc2 = (dc2) ra.a(layoutInflater, R.layout.fragment_goal_tracking_overview_month, viewGroup, false, O0());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            jc a2 = mc.a(activity).a(ju3.class);
            wd4.a((Object) a2, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.j = (ju3) a2;
            dc2.s.setOnClickListener(new b(this, dc2));
        }
        dc2.q.setEndDate(Calendar.getInstance());
        dc2.q.setOnCalendarMonthChanged(new c(this));
        dc2.q.setOnCalendarItemClickListener(this);
        this.k = new ur3<>(this, dc2);
        ur3<dc2> ur3 = this.k;
        if (ur3 != null) {
            dc2 a3 = ur3.a();
            if (a3 != null) {
                return a3.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onResume");
        pb3 pb3 = this.l;
        if (pb3 != null) {
            pb3.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onStop");
        pb3 pb3 = this.l;
        if (pb3 != null) {
            pb3.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    public View p(int i) {
        if (this.m == null) {
            this.m = new HashMap();
        }
        View view = (View) this.m.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.m.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    @DexIgnore
    public void a(TreeMap<Long, Float> treeMap) {
        wd4.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        ur3<dc2> ur3 = this.k;
        if (ur3 != null) {
            dc2 a2 = ur3.a();
            if (a2 != null) {
                RecyclerViewCalendar recyclerViewCalendar = a2.q;
                if (recyclerViewCalendar != null) {
                    recyclerViewCalendar.setTintColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.hybridGoalTrackingTab));
                }
            }
        }
        ur3<dc2> ur32 = this.k;
        if (ur32 != null) {
            dc2 a3 = ur32.a();
            if (a3 != null) {
                RecyclerViewCalendar recyclerViewCalendar2 = a3.q;
                if (recyclerViewCalendar2 != null) {
                    recyclerViewCalendar2.setData(treeMap);
                }
            }
        }
        ((RecyclerViewCalendar) p(h62.calendarMonth)).setEnableButtonNextAndPrevMonth(true);
    }

    @DexIgnore
    public void a(Date date, Date date2) {
        wd4.b(date, "selectDate");
        wd4.b(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        ur3<dc2> ur3 = this.k;
        if (ur3 != null) {
            dc2 a2 = ur3.a();
            if (a2 != null) {
                Calendar instance = Calendar.getInstance();
                Calendar instance2 = Calendar.getInstance();
                Calendar instance3 = Calendar.getInstance();
                wd4.a((Object) instance, "selectCalendar");
                instance.setTime(date);
                wd4.a((Object) instance2, "startCalendar");
                instance2.setTime(sk2.n(date2));
                wd4.a((Object) instance3, "endCalendar");
                instance3.setTime(sk2.i(instance3.getTime()));
                a2.q.a(instance, instance2, instance3);
            }
        }
    }

    @DexIgnore
    public void a(int i, Calendar calendar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
        FragmentActivity activity = getActivity();
        if (activity != null && calendar != null) {
            GoalTrackingDetailActivity.a aVar = GoalTrackingDetailActivity.D;
            Date time = calendar.getTime();
            wd4.a((Object) time, "it.time");
            wd4.a((Object) activity, Constants.ACTIVITY);
            aVar.a(time, activity);
        }
    }

    @DexIgnore
    public void a(pb3 pb3) {
        wd4.b(pb3, "presenter");
        this.l = pb3;
    }
}
