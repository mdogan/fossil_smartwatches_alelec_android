package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sc3 implements Factory<HeartRateOverviewMonthPresenter> {
    @DexIgnore
    public static HeartRateOverviewMonthPresenter a(qc3 qc3, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        return new HeartRateOverviewMonthPresenter(qc3, userRepository, heartRateSummaryRepository);
    }
}
