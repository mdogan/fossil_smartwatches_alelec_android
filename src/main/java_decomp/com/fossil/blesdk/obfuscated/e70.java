package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.asyncevent.AsyncEvent;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e70 extends h70 {
    @DexIgnore
    public /* final */ byte[] A; // = this.B.getAckResponseData$blesdk_productionRelease();
    @DexIgnore
    public /* final */ AsyncEvent B;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e70(Peripheral peripheral, AsyncEvent asyncEvent) {
        super(RequestId.SEND_ASYNC_EVENT_ACK, peripheral);
        wd4.b(peripheral, "peripheral");
        wd4.b(asyncEvent, "asyncEvent");
        this.B = asyncEvent;
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new o10(GattCharacteristic.CharacteristicId.ASYNC, this.A, i().h());
    }

    @DexIgnore
    public JSONObject t() {
        return xa0.a(super.t(), JSONKey.DEVICE_EVENT, this.B.toJSONObject());
    }
}
