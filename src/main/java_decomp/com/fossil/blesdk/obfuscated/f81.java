package com.fossil.blesdk.obfuscated;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f81 extends n71<Double> implements a91<Double>, ja1, RandomAccess {
    @DexIgnore
    public double[] f;
    @DexIgnore
    public int g;

    /*
    static {
        new f81().B();
    }
    */

    @DexIgnore
    public f81() {
        this(new double[10], 0);
    }

    @DexIgnore
    public final void a(double d) {
        a(this.g, d);
    }

    @DexIgnore
    public final /* synthetic */ void add(int i, Object obj) {
        a(i, ((Double) obj).doubleValue());
    }

    @DexIgnore
    public final boolean addAll(Collection<? extends Double> collection) {
        a();
        w81.a(collection);
        if (!(collection instanceof f81)) {
            return super.addAll(collection);
        }
        f81 f81 = (f81) collection;
        int i = f81.g;
        if (i == 0) {
            return false;
        }
        int i2 = this.g;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            double[] dArr = this.f;
            if (i3 > dArr.length) {
                this.f = Arrays.copyOf(dArr, i3);
            }
            System.arraycopy(f81.f, 0, this.f, this.g, f81.g);
            this.g = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @DexIgnore
    public final /* synthetic */ a91 b(int i) {
        if (i >= this.g) {
            return new f81(Arrays.copyOf(this.f, i), this.g);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f81)) {
            return super.equals(obj);
        }
        f81 f81 = (f81) obj;
        if (this.g != f81.g) {
            return false;
        }
        double[] dArr = f81.f;
        for (int i = 0; i < this.g; i++) {
            if (Double.doubleToLongBits(this.f[i]) != Double.doubleToLongBits(dArr[i])) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final String f(int i) {
        int i2 = this.g;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        a(i);
        return Double.valueOf(this.f[i]);
    }

    @DexIgnore
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g; i2++) {
            i = (i * 31) + w81.a(Double.doubleToLongBits(this.f[i2]));
        }
        return i;
    }

    @DexIgnore
    public final boolean remove(Object obj) {
        a();
        for (int i = 0; i < this.g; i++) {
            if (obj.equals(Double.valueOf(this.f[i]))) {
                double[] dArr = this.f;
                System.arraycopy(dArr, i + 1, dArr, i, (this.g - i) - 1);
                this.g--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void removeRange(int i, int i2) {
        a();
        if (i2 >= i) {
            double[] dArr = this.f;
            System.arraycopy(dArr, i2, dArr, i, this.g - i2);
            this.g -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @DexIgnore
    public final /* synthetic */ Object set(int i, Object obj) {
        double doubleValue = ((Double) obj).doubleValue();
        a();
        a(i);
        double[] dArr = this.f;
        double d = dArr[i];
        dArr[i] = doubleValue;
        return Double.valueOf(d);
    }

    @DexIgnore
    public final int size() {
        return this.g;
    }

    @DexIgnore
    public f81(double[] dArr, int i) {
        this.f = dArr;
        this.g = i;
    }

    @DexIgnore
    public final void a(int i, double d) {
        a();
        if (i >= 0) {
            int i2 = this.g;
            if (i <= i2) {
                double[] dArr = this.f;
                if (i2 < dArr.length) {
                    System.arraycopy(dArr, i, dArr, i + 1, i2 - i);
                } else {
                    double[] dArr2 = new double[(((i2 * 3) / 2) + 1)];
                    System.arraycopy(dArr, 0, dArr2, 0, i);
                    System.arraycopy(this.f, i, dArr2, i + 1, this.g - i);
                    this.f = dArr2;
                }
                this.f[i] = d;
                this.g++;
                this.modCount++;
                return;
            }
        }
        throw new IndexOutOfBoundsException(f(i));
    }

    @DexIgnore
    public final /* synthetic */ Object remove(int i) {
        a();
        a(i);
        double[] dArr = this.f;
        double d = dArr[i];
        int i2 = this.g;
        if (i < i2 - 1) {
            System.arraycopy(dArr, i + 1, dArr, i, (i2 - i) - 1);
        }
        this.g--;
        this.modCount++;
        return Double.valueOf(d);
    }

    @DexIgnore
    public final void a(int i) {
        if (i < 0 || i >= this.g) {
            throw new IndexOutOfBoundsException(f(i));
        }
    }
}
