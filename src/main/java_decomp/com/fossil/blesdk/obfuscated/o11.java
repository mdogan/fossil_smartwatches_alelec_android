package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o11 implements vo0 {
    @DexIgnore
    public final ie0<Status> a(he0 he0, DataSet dataSet) {
        ck0.a(dataSet, (Object) "Must set the data set");
        ck0.b(!dataSet.I().isEmpty(), "Cannot use an empty data set");
        ck0.a(dataSet.J().Q(), (Object) "Must set the app package name for the data source");
        return he0.a(new p11(this, he0, dataSet, false));
    }
}
