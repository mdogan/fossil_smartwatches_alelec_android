package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface h51 extends IInterface {
    @DexIgnore
    boolean a(h51 h51) throws RemoteException;

    @DexIgnore
    int k() throws RemoteException;
}
