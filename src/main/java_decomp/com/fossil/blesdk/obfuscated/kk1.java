package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kk1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ComponentName e;
    @DexIgnore
    public /* final */ /* synthetic */ ik1 f;

    @DexIgnore
    public kk1(ik1 ik1, ComponentName componentName) {
        this.f = ik1;
        this.e = componentName;
    }

    @DexIgnore
    public final void run() {
        this.f.c.a(this.e);
    }
}
