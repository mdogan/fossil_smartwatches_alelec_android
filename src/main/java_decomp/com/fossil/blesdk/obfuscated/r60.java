package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r60 extends h70 {
    @DexIgnore
    public Peripheral.State A; // = Peripheral.State.DISCONNECTED;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r60(Peripheral peripheral) {
        super(RequestId.DISCONNECT, peripheral);
        wd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new g10(i().h());
    }

    @DexIgnore
    public void a(BluetoothCommand bluetoothCommand) {
        wd4.b(bluetoothCommand, Constants.COMMAND);
        this.A = ((g10) bluetoothCommand).i();
        a(new Request.ResponseInfo(0, (GattCharacteristic.CharacteristicId) null, (byte[]) null, xa0.a(new JSONObject(), JSONKey.NEW_STATE, this.A.getLogName$blesdk_productionRelease()), 7, (rd4) null));
    }

    @DexIgnore
    public void a(e20 e20) {
        wd4.b(e20, "connectionStateChangedNotification");
    }

    @DexIgnore
    public JSONObject t() {
        return xa0.a(xa0.a(super.t(), JSONKey.PERIPHERAL_CURRENT_STATE, i().getState().getLogName$blesdk_productionRelease()), JSONKey.MAC_ADDRESS, i().k());
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.NEW_STATE, this.A.getLogName$blesdk_productionRelease());
    }
}
