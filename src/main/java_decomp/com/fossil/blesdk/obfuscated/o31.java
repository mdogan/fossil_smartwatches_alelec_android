package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o31 extends t31 {
    @DexIgnore
    public /* final */ ve0<Status> e;

    @DexIgnore
    public o31(ve0<Status> ve0) {
        this.e = ve0;
    }

    @DexIgnore
    public final void a(p31 p31) {
        this.e.a(p31.G());
    }
}
