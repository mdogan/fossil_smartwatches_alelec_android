package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zu2 implements MembersInjector<xu2> {
    @DexIgnore
    public static void a(xu2 xu2, HomeDashboardPresenter homeDashboardPresenter) {
        xu2.k = homeDashboardPresenter;
    }

    @DexIgnore
    public static void a(xu2 xu2, HomeDianaCustomizePresenter homeDianaCustomizePresenter) {
        xu2.l = homeDianaCustomizePresenter;
    }

    @DexIgnore
    public static void a(xu2 xu2, HomeHybridCustomizePresenter homeHybridCustomizePresenter) {
        xu2.m = homeHybridCustomizePresenter;
    }

    @DexIgnore
    public static void a(xu2 xu2, HomeProfilePresenter homeProfilePresenter) {
        xu2.n = homeProfilePresenter;
    }

    @DexIgnore
    public static void a(xu2 xu2, HomeAlertsPresenter homeAlertsPresenter) {
        xu2.o = homeAlertsPresenter;
    }

    @DexIgnore
    public static void a(xu2 xu2, HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
        xu2.p = homeAlertsHybridPresenter;
    }

    @DexIgnore
    public static void a(xu2 xu2, jg3 jg3) {
        xu2.q = jg3;
    }
}
