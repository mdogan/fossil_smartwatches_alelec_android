package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jx {
    @DexIgnore
    public /* final */ h74 a;

    @DexIgnore
    public jx(h74 h74) {
        this.a = h74;
    }

    @DexIgnore
    public static jx a(Context context) {
        return new jx(new i74(context, Constants.USER_SETTING));
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public void b() {
        h74 h74 = this.a;
        h74.a(h74.edit().putBoolean("analytics_launched", true));
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public boolean a() {
        return this.a.get().getBoolean("analytics_launched", false);
    }
}
