package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c83 implements Factory<DashboardActiveTimePresenter> {
    @DexIgnore
    public static DashboardActiveTimePresenter a(a83 a83, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, i42 i42, yk2 yk2) {
        return new DashboardActiveTimePresenter(a83, summariesRepository, fitnessDataRepository, activitySummaryDao, fitnessDatabase, userRepository, i42, yk2);
    }
}
