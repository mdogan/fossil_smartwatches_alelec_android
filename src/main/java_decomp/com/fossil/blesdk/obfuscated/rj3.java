package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.xs3;
import com.fossil.wearables.fossil.R;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.enums.Unit;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rj3 extends as2 implements zj3, View.OnClickListener, xs3.g {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((rd4) null);
    @DexIgnore
    public ur3<xe2> j;
    @DexIgnore
    public yj3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return rj3.m;
        }

        @DexIgnore
        public final rj3 b() {
            return new rj3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rj3 e;

        @DexIgnore
        public b(rj3 rj3) {
            this.e = rj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rj3 e;

        @DexIgnore
        public c(rj3 rj3) {
            this.e = rj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            rj3.a(this.e).b(Unit.METRIC);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rj3 e;

        @DexIgnore
        public d(rj3 rj3) {
            this.e = rj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            rj3.a(this.e).b(Unit.IMPERIAL);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rj3 e;

        @DexIgnore
        public e(rj3 rj3) {
            this.e = rj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            rj3.a(this.e).d(Unit.METRIC);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rj3 e;

        @DexIgnore
        public f(rj3 rj3) {
            this.e = rj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            rj3.a(this.e).d(Unit.IMPERIAL);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rj3 e;

        @DexIgnore
        public g(rj3 rj3) {
            this.e = rj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            rj3.a(this.e).a(Unit.METRIC);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rj3 e;

        @DexIgnore
        public h(rj3 rj3) {
            this.e = rj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            rj3.a(this.e).a(Unit.IMPERIAL);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rj3 e;

        @DexIgnore
        public i(rj3 rj3) {
            this.e = rj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            rj3.a(this.e).c(Unit.METRIC);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rj3 e;

        @DexIgnore
        public j(rj3 rj3) {
            this.e = rj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            rj3.a(this.e).c(Unit.IMPERIAL);
        }
    }

    /*
    static {
        String simpleName = rj3.class.getSimpleName();
        if (simpleName != null) {
            wd4.a((Object) simpleName, "PreferredUnitFragment::class.java.simpleName!!");
            m = simpleName;
            return;
        }
        wd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ yj3 a(rj3 rj3) {
        yj3 yj3 = rj3.k;
        if (yj3 != null) {
            return yj3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void c(Unit unit) {
        ur3<xe2> ur3 = this.j;
        if (ur3 != null) {
            xe2 a2 = ur3.a();
            if (a2 != null) {
                if (unit != null) {
                    int i2 = sj3.a[unit.ordinal()];
                    if (i2 == 1) {
                        TabLayout.g c2 = a2.s.c(1);
                        if (c2 != null) {
                            c2.g();
                            return;
                        }
                        return;
                    } else if (i2 == 2) {
                        TabLayout.g c3 = a2.s.c(0);
                        if (c3 != null) {
                            c3.g();
                            return;
                        }
                        return;
                    }
                }
                TabLayout.g c4 = a2.s.c(0);
                if (c4 != null) {
                    c4.g();
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d(Unit unit) {
        ur3<xe2> ur3 = this.j;
        if (ur3 != null) {
            xe2 a2 = ur3.a();
            if (a2 != null) {
                if (unit != null) {
                    int i2 = sj3.b[unit.ordinal()];
                    if (i2 == 1) {
                        TabLayout.g c2 = a2.u.c(1);
                        if (c2 != null) {
                            c2.g();
                            return;
                        }
                        return;
                    } else if (i2 == 2) {
                        TabLayout.g c3 = a2.u.c(0);
                        if (c3 != null) {
                            c3.g();
                            return;
                        }
                        return;
                    }
                }
                TabLayout.g c4 = a2.u.c(0);
                if (c4 != null) {
                    c4.g();
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void e(Unit unit) {
        ur3<xe2> ur3 = this.j;
        if (ur3 != null) {
            xe2 a2 = ur3.a();
            if (a2 != null) {
                if (unit != null) {
                    int i2 = sj3.c[unit.ordinal()];
                    if (i2 == 1) {
                        TabLayout.g c2 = a2.r.c(1);
                        if (c2 != null) {
                            c2.g();
                            return;
                        }
                        return;
                    } else if (i2 == 2) {
                        TabLayout.g c3 = a2.r.c(0);
                        if (c3 != null) {
                            c3.g();
                            return;
                        }
                        return;
                    }
                }
                TabLayout.g c4 = a2.r.c(0);
                if (c4 != null) {
                    c4.g();
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void n() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void onClick(View view) {
        wd4.b(view, "v");
        if (view.getId() == R.id.aciv_back) {
            n();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        Context context = getContext();
        if (context != null) {
            Integer.valueOf(k6.a(context, (int) R.color.white));
            Context context2 = getContext();
            if (context2 != null) {
                Integer.valueOf(k6.a(context2, (int) R.color.fossilOrange));
                xe2 xe2 = (xe2) ra.a(LayoutInflater.from(getContext()), R.layout.fragment_preferred_unit, (ViewGroup) null, false, O0());
                xe2.q.setOnClickListener(new b(this));
                View childAt = xe2.s.getChildAt(0);
                if (childAt != null) {
                    View childAt2 = ((LinearLayout) childAt).getChildAt(0);
                    if (childAt2 != null) {
                        childAt2.setOnClickListener(new c(this));
                    }
                    View childAt3 = xe2.s.getChildAt(0);
                    if (childAt3 != null) {
                        View childAt4 = ((LinearLayout) childAt3).getChildAt(1);
                        if (childAt4 != null) {
                            childAt4.setOnClickListener(new d(this));
                        }
                        View childAt5 = xe2.u.getChildAt(0);
                        if (childAt5 != null) {
                            View childAt6 = ((LinearLayout) childAt5).getChildAt(0);
                            if (childAt6 != null) {
                                childAt6.setOnClickListener(new e(this));
                            }
                            View childAt7 = xe2.u.getChildAt(0);
                            if (childAt7 != null) {
                                View childAt8 = ((LinearLayout) childAt7).getChildAt(1);
                                if (childAt8 != null) {
                                    childAt8.setOnClickListener(new f(this));
                                }
                                View childAt9 = xe2.r.getChildAt(0);
                                if (childAt9 != null) {
                                    View childAt10 = ((LinearLayout) childAt9).getChildAt(0);
                                    if (childAt10 != null) {
                                        childAt10.setOnClickListener(new g(this));
                                    }
                                    View childAt11 = xe2.r.getChildAt(0);
                                    if (childAt11 != null) {
                                        View childAt12 = ((LinearLayout) childAt11).getChildAt(1);
                                        if (childAt12 != null) {
                                            childAt12.setOnClickListener(new h(this));
                                        }
                                        View childAt13 = xe2.t.getChildAt(0);
                                        if (childAt13 != null) {
                                            View childAt14 = ((LinearLayout) childAt13).getChildAt(0);
                                            if (childAt14 != null) {
                                                childAt14.setOnClickListener(new i(this));
                                            }
                                            View childAt15 = xe2.t.getChildAt(0);
                                            if (childAt15 != null) {
                                                View childAt16 = ((LinearLayout) childAt15).getChildAt(1);
                                                if (childAt16 != null) {
                                                    childAt16.setOnClickListener(new j(this));
                                                }
                                                this.j = new ur3<>(this, xe2);
                                                wd4.a((Object) xe2, "binding");
                                                return xe2.d();
                                            }
                                            throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                                        }
                                        throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                                    }
                                    throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                                }
                                throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                            }
                            throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                        }
                        throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                }
                throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        yj3 yj3 = this.k;
        if (yj3 != null) {
            yj3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        yj3 yj3 = this.k;
        if (yj3 != null) {
            yj3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(yj3 yj3) {
        wd4.b(yj3, "presenter");
        tt1.a(yj3);
        wd4.a((Object) yj3, "checkNotNull(presenter)");
        this.k = yj3;
    }

    @DexIgnore
    public void a(Unit unit) {
        ur3<xe2> ur3 = this.j;
        if (ur3 != null) {
            xe2 a2 = ur3.a();
            if (a2 != null) {
                if (unit != null) {
                    int i2 = sj3.d[unit.ordinal()];
                    if (i2 == 1) {
                        TabLayout.g c2 = a2.t.c(1);
                        if (c2 != null) {
                            c2.g();
                            return;
                        }
                        return;
                    } else if (i2 == 2) {
                        TabLayout.g c3 = a2.t.c(0);
                        if (c3 != null) {
                            c3.g();
                            return;
                        }
                        return;
                    }
                }
                TabLayout.g c4 = a2.t.c(0);
                if (c4 != null) {
                    c4.g();
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(int i2, String str) {
        if (isActive()) {
            es3 es3 = es3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wd4.a((Object) childFragmentManager, "childFragmentManager");
            es3.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wd4.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, "Inside .onDialogFragmentResult with TAG=" + str);
    }
}
