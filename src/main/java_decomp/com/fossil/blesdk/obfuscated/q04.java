package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.a.f;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class q04 extends p04 {
    @DexIgnore
    public static /* final */ l04 m;

    /*
    static {
        l04 l04 = new l04();
        m = l04;
        l04.a("A9VH9B8L4GX4");
    }
    */

    @DexIgnore
    public q04(Context context) {
        super(context, 0, m);
    }

    @DexIgnore
    public f a() {
        return f.NETWORK_DETECTOR;
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        k24.a(jSONObject, "actky", i04.b(this.j));
        return true;
    }
}
