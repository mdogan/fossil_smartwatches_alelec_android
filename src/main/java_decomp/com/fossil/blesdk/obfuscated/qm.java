package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qm {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public qm(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || qm.class != obj.getClass()) {
            return false;
        }
        qm qmVar = (qm) obj;
        if (!TextUtils.equals(this.a, qmVar.a) || !TextUtils.equals(this.b, qmVar.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Header[name=" + this.a + ",value=" + this.b + "]";
    }
}
