package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d70 extends h70 {
    @DexIgnore
    public int A; // = 23;
    @DexIgnore
    public /* final */ int B;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d70(int i, Peripheral peripheral) {
        super(RequestId.REQUEST_MTU, peripheral);
        wd4.b(peripheral, "peripheral");
        this.B = i;
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new m10(this.B, i().h());
    }

    @DexIgnore
    public void a(BluetoothCommand bluetoothCommand) {
        wd4.b(bluetoothCommand, Constants.COMMAND);
        this.A = ((m10) bluetoothCommand).i();
        a(new Request.ResponseInfo(0, (GattCharacteristic.CharacteristicId) null, (byte[]) null, xa0.a(new JSONObject(), JSONKey.EXCHANGED_MTU, Integer.valueOf(this.A)), 7, (rd4) null));
    }

    @DexIgnore
    public JSONObject t() {
        return xa0.a(super.t(), JSONKey.REQUESTED_MTU, Integer.valueOf(this.B));
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.EXCHANGED_MTU, Integer.valueOf(this.A));
    }
}
