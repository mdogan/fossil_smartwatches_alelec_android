package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yb1 implements Cloneable {
    @DexIgnore
    public static /* final */ zb1 h; // = new zb1();
    @DexIgnore
    public int[] e;
    @DexIgnore
    public zb1[] f;
    @DexIgnore
    public int g;

    @DexIgnore
    public yb1() {
        this(10);
    }

    @DexIgnore
    public static int d(int i) {
        int i2 = i << 2;
        int i3 = 4;
        while (true) {
            if (i3 >= 32) {
                break;
            }
            int i4 = (1 << i3) - 12;
            if (i2 <= i4) {
                i2 = i4;
                break;
            }
            i3++;
        }
        return i2 / 4;
    }

    @DexIgnore
    public final zb1 a(int i) {
        int c = c(i);
        if (c < 0) {
            return null;
        }
        zb1[] zb1Arr = this.f;
        if (zb1Arr[c] == h) {
            return null;
        }
        return zb1Arr[c];
    }

    @DexIgnore
    public final int b() {
        return this.g;
    }

    @DexIgnore
    public final int c(int i) {
        int i2 = this.g - 1;
        int i3 = 0;
        while (i3 <= i2) {
            int i4 = (i3 + i2) >>> 1;
            int i5 = this.e[i4];
            if (i5 < i) {
                i3 = i4 + 1;
            } else if (i5 <= i) {
                return i4;
            } else {
                i2 = i4 - 1;
            }
        }
        return ~i3;
    }

    @DexIgnore
    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        int i = this.g;
        yb1 yb1 = new yb1(i);
        System.arraycopy(this.e, 0, yb1.e, 0, i);
        for (int i2 = 0; i2 < i; i2++) {
            zb1[] zb1Arr = this.f;
            if (zb1Arr[i2] != null) {
                yb1.f[i2] = (zb1) zb1Arr[i2].clone();
            }
        }
        yb1.g = i;
        return yb1;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof yb1)) {
            return false;
        }
        yb1 yb1 = (yb1) obj;
        int i = this.g;
        if (i != yb1.g) {
            return false;
        }
        int[] iArr = this.e;
        int[] iArr2 = yb1.e;
        int i2 = 0;
        while (true) {
            if (i2 >= i) {
                z = true;
                break;
            } else if (iArr[i2] != iArr2[i2]) {
                z = false;
                break;
            } else {
                i2++;
            }
        }
        if (z) {
            zb1[] zb1Arr = this.f;
            zb1[] zb1Arr2 = yb1.f;
            int i3 = this.g;
            int i4 = 0;
            while (true) {
                if (i4 >= i3) {
                    z2 = true;
                    break;
                } else if (!zb1Arr[i4].equals(zb1Arr2[i4])) {
                    z2 = false;
                    break;
                } else {
                    i4++;
                }
            }
            if (z2) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        int i = 17;
        for (int i2 = 0; i2 < this.g; i2++) {
            i = (((i * 31) + this.e[i2]) * 31) + this.f[i2].hashCode();
        }
        return i;
    }

    @DexIgnore
    public yb1(int i) {
        int d = d(i);
        this.e = new int[d];
        this.f = new zb1[d];
        this.g = 0;
    }

    @DexIgnore
    public final zb1 b(int i) {
        return this.f[i];
    }

    @DexIgnore
    public final void a(int i, zb1 zb1) {
        int c = c(i);
        if (c >= 0) {
            this.f[c] = zb1;
            return;
        }
        int i2 = ~c;
        if (i2 < this.g) {
            zb1[] zb1Arr = this.f;
            if (zb1Arr[i2] == h) {
                this.e[i2] = i;
                zb1Arr[i2] = zb1;
                return;
            }
        }
        int i3 = this.g;
        if (i3 >= this.e.length) {
            int d = d(i3 + 1);
            int[] iArr = new int[d];
            zb1[] zb1Arr2 = new zb1[d];
            int[] iArr2 = this.e;
            System.arraycopy(iArr2, 0, iArr, 0, iArr2.length);
            zb1[] zb1Arr3 = this.f;
            System.arraycopy(zb1Arr3, 0, zb1Arr2, 0, zb1Arr3.length);
            this.e = iArr;
            this.f = zb1Arr2;
        }
        int i4 = this.g;
        if (i4 - i2 != 0) {
            int[] iArr3 = this.e;
            int i5 = i2 + 1;
            System.arraycopy(iArr3, i2, iArr3, i5, i4 - i2);
            zb1[] zb1Arr4 = this.f;
            System.arraycopy(zb1Arr4, i2, zb1Arr4, i5, this.g - i2);
        }
        this.e[i2] = i;
        this.f[i2] = zb1;
        this.g++;
    }

    @DexIgnore
    public final boolean a() {
        return this.g == 0;
    }
}
