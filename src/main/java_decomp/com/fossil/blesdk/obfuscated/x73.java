package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x73 implements Factory<HomeDashboardPresenter> {
    @DexIgnore
    public static HomeDashboardPresenter a(w73 w73, PortfolioApp portfolioApp, DeviceRepository deviceRepository, wj2 wj2, SummariesRepository summariesRepository, GoalTrackingRepository goalTrackingRepository, SleepSummariesRepository sleepSummariesRepository, HeartRateSampleRepository heartRateSampleRepository) {
        return new HomeDashboardPresenter(w73, portfolioApp, deviceRepository, wj2, summariesRepository, goalTrackingRepository, sleepSummariesRepository, heartRateSampleRepository);
    }
}
