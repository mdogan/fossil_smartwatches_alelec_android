package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import com.android.volley.Request;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class um {
    @DexIgnore
    public /* final */ AtomicInteger a;
    @DexIgnore
    public /* final */ Set<Request<?>> b;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<Request<?>> c;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<Request<?>> d;
    @DexIgnore
    public /* final */ mm e;
    @DexIgnore
    public /* final */ rm f;
    @DexIgnore
    public /* final */ wm g;
    @DexIgnore
    public /* final */ sm[] h;
    @DexIgnore
    public nm i;
    @DexIgnore
    public /* final */ List<a> j;

    @DexIgnore
    public interface a<T> {
        @DexIgnore
        void a(Request<T> request);
    }

    @DexIgnore
    public um(mm mmVar, rm rmVar, int i2, wm wmVar) {
        this.a = new AtomicInteger();
        this.b = new HashSet();
        this.c = new PriorityBlockingQueue<>();
        this.d = new PriorityBlockingQueue<>();
        this.j = new ArrayList();
        this.e = mmVar;
        this.f = rmVar;
        this.h = new sm[i2];
        this.g = wmVar;
    }

    @DexIgnore
    public int a() {
        return this.a.incrementAndGet();
    }

    @DexIgnore
    public void b() {
        c();
        this.i = new nm(this.c, this.d, this.e, this.g);
        this.i.start();
        for (int i2 = 0; i2 < this.h.length; i2++) {
            sm smVar = new sm(this.d, this.f, this.e, this.g);
            this.h[i2] = smVar;
            smVar.start();
        }
    }

    @DexIgnore
    public void c() {
        nm nmVar = this.i;
        if (nmVar != null) {
            nmVar.b();
        }
        for (sm smVar : this.h) {
            if (smVar != null) {
                smVar.b();
            }
        }
    }

    @DexIgnore
    public <T> Request<T> a(Request<T> request) {
        request.setRequestQueue(this);
        synchronized (this.b) {
            this.b.add(request);
        }
        request.setSequence(a());
        request.addMarker("add-to-queue");
        if (!request.shouldCache()) {
            this.d.add(request);
            return request;
        }
        this.c.add(request);
        return request;
    }

    @DexIgnore
    public <T> void b(Request<T> request) {
        synchronized (this.b) {
            this.b.remove(request);
        }
        synchronized (this.j) {
            for (a a2 : this.j) {
                a2.a(request);
            }
        }
    }

    @DexIgnore
    public um(mm mmVar, rm rmVar, int i2) {
        this(mmVar, rmVar, i2, new pm(new Handler(Looper.getMainLooper())));
    }

    @DexIgnore
    public um(mm mmVar, rm rmVar) {
        this(mmVar, rmVar, 4);
    }
}
