package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x60 extends h70 {
    @DexIgnore
    public int A;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x60(Peripheral peripheral) {
        super(RequestId.READ_RSSI, peripheral);
        wd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new k10(i().h());
    }

    @DexIgnore
    public final int B() {
        return this.A;
    }

    @DexIgnore
    public void a(BluetoothCommand bluetoothCommand) {
        wd4.b(bluetoothCommand, Constants.COMMAND);
        this.A = ((k10) bluetoothCommand).i();
        a(new Request.ResponseInfo(0, (GattCharacteristic.CharacteristicId) null, (byte[]) null, xa0.a(new JSONObject(), JSONKey.RSSI, Integer.valueOf(this.A)), 7, (rd4) null));
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.RSSI, Integer.valueOf(this.A));
    }
}
