package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x13 {
    @DexIgnore
    public /* final */ u13 a;
    @DexIgnore
    public /* final */ int b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public x13(u13 u13, String str, int i, String str2, String str3) {
        wd4.b(u13, "mView");
        wd4.b(str, "mPresetId");
        this.a = u13;
        this.b = i;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final u13 b() {
        return this.a;
    }
}
