package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface pg3 extends cs2<og3> {
    @DexIgnore
    void I();

    @DexIgnore
    void L();

    @DexIgnore
    void a(int i, String str);

    @DexIgnore
    void a(ng3 ng3);

    @DexIgnore
    void a(ActivityStatistic activityStatistic);

    @DexIgnore
    void a(SleepStatistic sleepStatistic);

    @DexIgnore
    void a(MFUser mFUser);

    @DexIgnore
    void a(boolean z, boolean z2);

    @DexIgnore
    void b(ArrayList<HomeProfilePresenter.b> arrayList);

    @DexIgnore
    void d();

    @DexIgnore
    void e();

    @DexIgnore
    void o();
}
