package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dk4 {
    @DexIgnore
    public static /* final */ Object a; // = new pk4("CONDITION_FALSE");

    /*
    static {
        new pk4("ALREADY_REMOVED");
        new pk4("LIST_EMPTY");
        new pk4("REMOVE_PREPARED");
    }
    */

    @DexIgnore
    public static final Object a() {
        return a;
    }

    @DexIgnore
    public static final ek4 a(Object obj) {
        wd4.b(obj, "$this$unwrap");
        lk4 lk4 = (lk4) (!(obj instanceof lk4) ? null : obj);
        if (lk4 != null) {
            ek4 ek4 = lk4.a;
            if (ek4 != null) {
                return ek4;
            }
        }
        return (ek4) obj;
    }
}
