package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ts4 implements sr4<qm4, Character> {
    @DexIgnore
    public static /* final */ ts4 a; // = new ts4();

    @DexIgnore
    public Character a(qm4 qm4) throws IOException {
        String F = qm4.F();
        if (F.length() == 1) {
            return Character.valueOf(F.charAt(0));
        }
        throw new IOException("Expected body of length 1 for Character conversion but was " + F.length());
    }
}
