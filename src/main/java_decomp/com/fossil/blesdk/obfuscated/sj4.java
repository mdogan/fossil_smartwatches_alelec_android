package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sj4<T> extends mk4<T> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sj4(CoroutineContext coroutineContext, kc4<? super T> kc4) {
        super(coroutineContext, kc4);
        wd4.b(coroutineContext, "context");
        wd4.b(kc4, "uCont");
    }

    @DexIgnore
    public int j() {
        return 3;
    }
}
