package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface po<T> extends ko {
    @DexIgnore
    bq<T> a(Context context, bq<T> bqVar, int i, int i2);
}
