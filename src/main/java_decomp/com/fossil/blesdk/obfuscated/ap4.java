package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ap4 implements kp4 {
    @DexIgnore
    public /* final */ kp4 e;

    @DexIgnore
    public ap4(kp4 kp4) {
        if (kp4 != null) {
            this.e = kp4;
            return;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    @DexIgnore
    public long b(vo4 vo4, long j) throws IOException {
        return this.e.b(vo4, j);
    }

    @DexIgnore
    public final kp4 c() {
        return this.e;
    }

    @DexIgnore
    public void close() throws IOException {
        this.e.close();
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + "(" + this.e.toString() + ")";
    }

    @DexIgnore
    public lp4 b() {
        return this.e.b();
    }
}
