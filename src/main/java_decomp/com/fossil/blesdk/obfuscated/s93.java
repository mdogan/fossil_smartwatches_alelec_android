package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s93 implements Factory<ActivityOverviewMonthPresenter> {
    @DexIgnore
    public static ActivityOverviewMonthPresenter a(q93 q93, UserRepository userRepository, SummariesRepository summariesRepository) {
        return new ActivityOverviewMonthPresenter(q93, userRepository, summariesRepository);
    }
}
