package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.fossil.blesdk.obfuscated.ee0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tc1 {
    @DexIgnore
    public static /* final */ ee0.g<g41> a; // = new ee0.g<>();
    @DexIgnore
    public static /* final */ ee0.a<g41, Object> b; // = new dd1();
    @DexIgnore
    public static /* final */ ee0<Object> c; // = new ee0<>("LocationServices.API", b, a);
    @DexIgnore
    @Deprecated
    public static /* final */ oc1 d; // = new y41();
    @DexIgnore
    @Deprecated
    public static /* final */ yc1 e; // = new o41();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<R extends ne0> extends ue0<R, g41> {
        @DexIgnore
        public a(he0 he0) {
            super(tc1.c, he0);
        }
    }

    /*
    static {
        new r31();
    }
    */

    @DexIgnore
    public static pc1 a(Context context) {
        return new pc1(context);
    }

    @DexIgnore
    public static zc1 b(Context context) {
        return new zc1(context);
    }
}
