package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import com.fossil.blesdk.obfuscated.gc;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fc implements LifecycleOwner {
    @DexIgnore
    public static /* final */ fc m; // = new fc();
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public boolean g; // = true;
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public Handler i;
    @DexIgnore
    public /* final */ LifecycleRegistry j; // = new LifecycleRegistry(this);
    @DexIgnore
    public Runnable k; // = new a();
    @DexIgnore
    public gc.a l; // = new b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            fc.this.e();
            fc.this.f();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements gc.a {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a() {
            fc.this.c();
        }

        @DexIgnore
        public void d() {
            fc.this.b();
        }

        @DexIgnore
        public void e() {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends rb {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
            gc.a(activity).d(fc.this.l);
        }

        @DexIgnore
        public void onActivityPaused(Activity activity) {
            fc.this.a();
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
            fc.this.d();
        }
    }

    @DexIgnore
    public static void b(Context context) {
        m.a(context);
    }

    @DexIgnore
    public static LifecycleOwner g() {
        return m;
    }

    @DexIgnore
    public void a() {
        this.f--;
        if (this.f == 0) {
            this.i.postDelayed(this.k, 700);
        }
    }

    @DexIgnore
    public void c() {
        this.e++;
        if (this.e == 1 && this.h) {
            this.j.a(Lifecycle.Event.ON_START);
            this.h = false;
        }
    }

    @DexIgnore
    public void d() {
        this.e--;
        f();
    }

    @DexIgnore
    public void e() {
        if (this.f == 0) {
            this.g = true;
            this.j.a(Lifecycle.Event.ON_PAUSE);
        }
    }

    @DexIgnore
    public void f() {
        if (this.e == 0 && this.g) {
            this.j.a(Lifecycle.Event.ON_STOP);
            this.h = true;
        }
    }

    @DexIgnore
    public Lifecycle getLifecycle() {
        return this.j;
    }

    @DexIgnore
    public void b() {
        this.f++;
        if (this.f != 1) {
            return;
        }
        if (this.g) {
            this.j.a(Lifecycle.Event.ON_RESUME);
            this.g = false;
            return;
        }
        this.i.removeCallbacks(this.k);
    }

    @DexIgnore
    public void a(Context context) {
        this.i = new Handler();
        this.j.a(Lifecycle.Event.ON_CREATE);
        ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new c());
    }
}
