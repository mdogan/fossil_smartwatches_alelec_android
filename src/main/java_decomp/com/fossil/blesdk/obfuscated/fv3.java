package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface fv3 {

    @DexIgnore
    public interface a {
        @DexIgnore
        kv3 a(iv3 iv3) throws IOException;
    }

    @DexIgnore
    kv3 a(a aVar) throws IOException;
}
