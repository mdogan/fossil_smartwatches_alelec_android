package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ln3 implements Factory<PairingPresenter> {
    @DexIgnore
    public static PairingPresenter a(hn3 hn3, LinkDeviceUseCase linkDeviceUseCase, DeviceRepository deviceRepository, NotificationsRepository notificationsRepository, wy2 wy2, qx2 qx2, wj2 wj2, NotificationSettingsDatabase notificationSettingsDatabase, SetNotificationUseCase setNotificationUseCase, fn2 fn2) {
        return new PairingPresenter(hn3, linkDeviceUseCase, deviceRepository, notificationsRepository, wy2, qx2, wj2, notificationSettingsDatabase, setNotificationUseCase, fn2);
    }
}
