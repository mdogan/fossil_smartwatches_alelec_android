package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import androidx.room.MultiInstanceInvalidationService;
import com.fossil.blesdk.obfuscated.nf;
import com.fossil.blesdk.obfuscated.of;
import com.fossil.blesdk.obfuscated.qf;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rf {
    @DexIgnore
    public Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ qf d;
    @DexIgnore
    public /* final */ qf.c e;
    @DexIgnore
    public of f;
    @DexIgnore
    public /* final */ Executor g;
    @DexIgnore
    public /* final */ nf h; // = new a();
    @DexIgnore
    public /* final */ AtomicBoolean i; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ ServiceConnection j; // = new b();
    @DexIgnore
    public /* final */ Runnable k; // = new c();
    @DexIgnore
    public /* final */ Runnable l; // = new d();
    @DexIgnore
    public /* final */ Runnable m; // = new e();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends nf.a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rf$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.rf$a$a  reason: collision with other inner class name */
        public class C0031a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ String[] e;

            @DexIgnore
            public C0031a(String[] strArr) {
                this.e = strArr;
            }

            @DexIgnore
            public void run() {
                rf.this.d.a(this.e);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(String[] strArr) {
            rf.this.g.execute(new C0031a(strArr));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ServiceConnection {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            rf.this.f = of.a.a(iBinder);
            rf rfVar = rf.this;
            rfVar.g.execute(rfVar.k);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            rf rfVar = rf.this;
            rfVar.g.execute(rfVar.l);
            rf rfVar2 = rf.this;
            rfVar2.f = null;
            rfVar2.a = null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            try {
                of ofVar = rf.this.f;
                if (ofVar != null) {
                    rf.this.c = ofVar.a(rf.this.h, rf.this.b);
                    rf.this.d.a(rf.this.e);
                }
            } catch (RemoteException e2) {
                Log.w("ROOM", "Cannot register multi-instance invalidation callback", e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            rf rfVar = rf.this;
            rfVar.d.c(rfVar.e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void run() {
            rf rfVar = rf.this;
            rfVar.d.c(rfVar.e);
            try {
                of ofVar = rf.this.f;
                if (ofVar != null) {
                    ofVar.a(rf.this.h, rf.this.c);
                }
            } catch (RemoteException e2) {
                Log.w("ROOM", "Cannot unregister multi-instance invalidation callback", e2);
            }
            rf rfVar2 = rf.this;
            Context context = rfVar2.a;
            if (context != null) {
                context.unbindService(rfVar2.j);
                rf.this.a = null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends qf.c {
        @DexIgnore
        public f(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        public boolean isRemote() {
            return true;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            if (!rf.this.i.get()) {
                try {
                    rf.this.f.a(rf.this.c, (String[]) set.toArray(new String[0]));
                } catch (RemoteException e) {
                    Log.w("ROOM", "Cannot broadcast invalidation", e);
                }
            }
        }
    }

    @DexIgnore
    public rf(Context context, String str, qf qfVar, Executor executor) {
        this.a = context.getApplicationContext();
        this.b = str;
        this.d = qfVar;
        this.g = executor;
        this.e = new f(qfVar.b);
        this.a.bindService(new Intent(this.a, MultiInstanceInvalidationService.class), this.j, 1);
    }

    @DexIgnore
    public void a() {
        if (this.i.compareAndSet(false, true)) {
            this.g.execute(this.m);
        }
    }
}
