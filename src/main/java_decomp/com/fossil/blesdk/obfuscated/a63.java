package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface a63 extends cs2<z53> {
    @DexIgnore
    void a(boolean z);

    @DexIgnore
    void c(int i);

    @DexIgnore
    void c(List<h13> list);

    @DexIgnore
    void d(int i);

    @DexIgnore
    void d(boolean z);

    @DexIgnore
    void e(int i);

    @DexIgnore
    int getItemCount();

    @DexIgnore
    void j();

    @DexIgnore
    void l();

    @DexIgnore
    void m();

    @DexIgnore
    void q(String str);

    @DexIgnore
    void v();
}
