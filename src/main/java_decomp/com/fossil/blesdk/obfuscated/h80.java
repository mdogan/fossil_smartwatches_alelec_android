package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.FileControlOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h80 extends f80 {
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ h80(short s, Peripheral peripheral, int i, int i2, rd4 rd4) {
        this(s, peripheral, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    public final long J() {
        return this.M;
    }

    @DexIgnore
    public final long K() {
        return this.N;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        wd4.b(bArr, "responseData");
        JSONObject a = super.a(bArr);
        if (bArr.length >= 8) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            this.M = o90.b(order.getInt(0));
            this.N = o90.b(order.getInt(4));
            xa0.a(xa0.a(a, JSONKey.WRITTEN_SIZE, Long.valueOf(this.M)), JSONKey.WRITTEN_DATA_CRC, Long.valueOf(this.N));
        }
        return a;
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(xa0.a(super.u(), JSONKey.WRITTEN_SIZE, Long.valueOf(this.M)), JSONKey.WRITTEN_DATA_CRC, Long.valueOf(this.N));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h80(short s, Peripheral peripheral, int i) {
        super(FileControlOperationCode.GET_SIZE_WRITTEN, s, RequestId.GET_FILE_SIZE_WRITTEN, peripheral, i);
        wd4.b(peripheral, "peripheral");
    }
}
