package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fn1 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<fn1> CREATOR; // = new gn1();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ dk0 f;

    @DexIgnore
    public fn1(int i, dk0 dk0) {
        this.e = i;
        this.f = dk0;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, (Parcelable) this.f, i, false);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public fn1(dk0 dk0) {
        this(1, dk0);
    }
}
