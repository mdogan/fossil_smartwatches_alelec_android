package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nx extends ex<nx> {
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public nx(String str) {
        if (str != null) {
            this.c = this.a.a(str);
            return;
        }
        throw new NullPointerException("eventName must not be null");
    }

    @DexIgnore
    public String b() {
        return this.c;
    }

    @DexIgnore
    public String toString() {
        return "{eventName:\"" + this.c + '\"' + ", customAttributes:" + this.b + "}";
    }
}
