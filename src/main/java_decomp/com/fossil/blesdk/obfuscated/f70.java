package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class f70 extends Request {
    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ f70(RequestId requestId, Peripheral peripheral, int i, int i2, rd4 rd4) {
        this(requestId, peripheral, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    public abstract BluetoothCommand A();

    @DexIgnore
    public final BluetoothCommand h() {
        return A();
    }

    @DexIgnore
    public final void s() {
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f70(RequestId requestId, Peripheral peripheral, int i) {
        super(requestId, peripheral, i);
        wd4.b(requestId, "id");
        wd4.b(peripheral, "peripheral");
    }
}
