package com.fossil.blesdk.obfuscated;

import com.android.volley.VolleyError;
import com.fossil.blesdk.obfuscated.mm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vm<T> {
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ mm.a b;
    @DexIgnore
    public /* final */ VolleyError c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void onErrorResponse(VolleyError volleyError);
    }

    @DexIgnore
    public interface b<T> {
        @DexIgnore
        void onResponse(T t);
    }

    @DexIgnore
    public vm(T t, mm.a aVar) {
        this.d = false;
        this.a = t;
        this.b = aVar;
        this.c = null;
    }

    @DexIgnore
    public static <T> vm<T> a(T t, mm.a aVar) {
        return new vm<>(t, aVar);
    }

    @DexIgnore
    public static <T> vm<T> a(VolleyError volleyError) {
        return new vm<>(volleyError);
    }

    @DexIgnore
    public boolean a() {
        return this.c == null;
    }

    @DexIgnore
    public vm(VolleyError volleyError) {
        this.d = false;
        this.a = null;
        this.b = null;
        this.c = volleyError;
    }
}
