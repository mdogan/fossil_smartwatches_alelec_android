package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.file.FileFormatException;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.GetFilePhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.UUID;
import kotlin.Pair;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t50 extends GetFilePhase {
    @DexIgnore
    public DeviceInformation R;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ t50(Peripheral peripheral, Phase.a aVar, short s, String str, int i, rd4 rd4) {
        this(peripheral, aVar, s, str);
        s = (i & 4) != 0 ? a50.b.a(peripheral.k(), FileType.DEVICE_INFO) : s;
        if ((i & 8) != 0) {
            str = UUID.randomUUID().toString();
            wd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public final DeviceInformation O() {
        return this.R;
    }

    @DexIgnore
    public void a(ArrayList<DeviceFile> arrayList) {
        wd4.b(arrayList, "filesData");
        a(k());
    }

    @DexIgnore
    public void c(DeviceFile deviceFile) {
        Phase.Result.ResultCode resultCode;
        wd4.b(deviceFile, "deviceFile");
        super.c(deviceFile);
        try {
            this.R = DeviceInformation.copy$default((DeviceInformation) q20.c.a(deviceFile.getRawData()), this.R.getName(), this.R.getMacAddress(), (String) null, (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (DeviceInformation.BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131068, (Object) null);
            resultCode = Phase.Result.ResultCode.SUCCESS;
        } catch (FileFormatException e) {
            ea0.l.a(e);
            resultCode = Phase.Result.ResultCode.INCORRECT_FILE_DATA;
        }
        b(Phase.Result.copy$default(k(), (PhaseId) null, resultCode, (Request.Result) null, 5, (Object) null));
    }

    @DexIgnore
    public JSONObject x() {
        return xa0.a(super.x(), JSONKey.DEVICE_INFO, this.R.toJSONObject());
    }

    @DexIgnore
    public DeviceInformation i() {
        return this.R;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public t50(Peripheral peripheral, Phase.a aVar, short s, String str) {
        super(r2, r3, PhaseId.READ_DEVICE_INFO_FILE, r5, dc4.a((Pair<? extends K, ? extends V>[]) new Pair[]{ab4.a(GetFilePhase.GetFileOption.SKIP_ERASE, true), ab4.a(GetFilePhase.GetFileOption.NUMBER_OF_FILE_REQUIRED, 1), ab4.a(GetFilePhase.GetFileOption.ERASE_CACHE_FILE_BEFORE_GET, true)}), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, r8, 32, (rd4) null);
        wd4.b(peripheral, "peripheral");
        Phase.a aVar2 = aVar;
        wd4.b(aVar2, "delegate");
        String str2 = str;
        wd4.b(str2, "phaseUuid");
        Peripheral peripheral2 = peripheral;
        short s2 = s;
        this.R = new DeviceInformation(peripheral.i(), peripheral.k(), "", "", "", (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (DeviceInformation.BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131040, (rd4) null);
    }
}
