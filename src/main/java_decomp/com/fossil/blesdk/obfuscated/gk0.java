package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.dynamic.RemoteCreator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gk0 extends RemoteCreator<zj0> {
    @DexIgnore
    public static /* final */ gk0 c; // = new gk0();

    @DexIgnore
    public gk0() {
        super("com.google.android.gms.common.ui.SignInButtonCreatorImpl");
    }

    @DexIgnore
    public static View b(Context context, int i, int i2) throws RemoteCreator.RemoteCreatorException {
        return c.a(context, i, i2);
    }

    @DexIgnore
    public final View a(Context context, int i, int i2) throws RemoteCreator.RemoteCreatorException {
        try {
            fk0 fk0 = new fk0(i, i2, (Scope[]) null);
            return (View) vn0.d(((zj0) a(context)).a(vn0.a(context), fk0));
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder(64);
            sb.append("Could not get button with size ");
            sb.append(i);
            sb.append(" and color ");
            sb.append(i2);
            throw new RemoteCreator.RemoteCreatorException(sb.toString(), e);
        }
    }

    @DexIgnore
    public final zj0 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.ISignInButtonCreator");
        if (queryLocalInterface instanceof zj0) {
            return (zj0) queryLocalInterface;
        }
        return new gl0(iBinder);
    }
}
