package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kj0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<kj0> CREATOR; // = new al0();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    public kj0(int i, String str) {
        this.e = i;
        this.f = str;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof kj0)) {
            kj0 kj0 = (kj0) obj;
            return kj0.e == this.e && ak0.a(kj0.f, this.f);
        }
    }

    @DexIgnore
    public int hashCode() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        int i = this.e;
        String str = this.f;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 12);
        sb.append(i);
        sb.append(":");
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, this.f, false);
        lk0.a(parcel, a);
    }
}
