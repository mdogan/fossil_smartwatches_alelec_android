package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.data.DataHolder;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class zi0<T> implements aj0<T> {
    @DexIgnore
    public /* final */ DataHolder e;

    @DexIgnore
    public zi0(DataHolder dataHolder) {
        this.e = dataHolder;
    }

    @DexIgnore
    public void a() {
        DataHolder dataHolder = this.e;
        if (dataHolder != null) {
            dataHolder.close();
        }
    }

    @DexIgnore
    public Iterator<T> iterator() {
        return new bj0(this);
    }
}
