package com.fossil.blesdk.obfuscated;

import android.graphics.PointF;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ue extends cf {
    @DexIgnore
    public xe d;
    @DexIgnore
    public xe e;

    @DexIgnore
    public int[] a(RecyclerView.m mVar, View view) {
        int[] iArr = new int[2];
        if (mVar.a()) {
            iArr[0] = a(mVar, view, d(mVar));
        } else {
            iArr[0] = 0;
        }
        if (mVar.b()) {
            iArr[1] = a(mVar, view, e(mVar));
        } else {
            iArr[1] = 0;
        }
        return iArr;
    }

    @DexIgnore
    public final View b(RecyclerView.m mVar, xe xeVar) {
        int i;
        int e2 = mVar.e();
        View view = null;
        if (e2 == 0) {
            return null;
        }
        if (mVar.f()) {
            i = xeVar.f() + (xeVar.g() / 2);
        } else {
            i = xeVar.a() / 2;
        }
        int i2 = Integer.MAX_VALUE;
        for (int i3 = 0; i3 < e2; i3++) {
            View d2 = mVar.d(i3);
            int abs = Math.abs((xeVar.d(d2) + (xeVar.b(d2) / 2)) - i);
            if (abs < i2) {
                view = d2;
                i2 = abs;
            }
        }
        return view;
    }

    @DexIgnore
    public View c(RecyclerView.m mVar) {
        if (mVar.b()) {
            return b(mVar, e(mVar));
        }
        if (mVar.a()) {
            return b(mVar, d(mVar));
        }
        return null;
    }

    @DexIgnore
    public final xe d(RecyclerView.m mVar) {
        xe xeVar = this.e;
        if (xeVar == null || xeVar.a != mVar) {
            this.e = xe.a(mVar);
        }
        return this.e;
    }

    @DexIgnore
    public final xe e(RecyclerView.m mVar) {
        xe xeVar = this.d;
        if (xeVar == null || xeVar.a != mVar) {
            this.d = xe.b(mVar);
        }
        return this.d;
    }

    @DexIgnore
    public int a(RecyclerView.m mVar, int i, int i2) {
        int i3;
        int i4;
        if (!(mVar instanceof RecyclerView.v.b)) {
            return -1;
        }
        int j = mVar.j();
        if (j == 0) {
            return -1;
        }
        View c = c(mVar);
        if (c == null) {
            return -1;
        }
        int l = mVar.l(c);
        if (l == -1) {
            return -1;
        }
        int i5 = j - 1;
        PointF a = ((RecyclerView.v.b) mVar).a(i5);
        if (a == null) {
            return -1;
        }
        if (mVar.a()) {
            i3 = a(mVar, d(mVar), i, 0);
            if (a.x < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i3 = -i3;
            }
        } else {
            i3 = 0;
        }
        if (mVar.b()) {
            i4 = a(mVar, e(mVar), 0, i2);
            if (a.y < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i4 = -i4;
            }
        } else {
            i4 = 0;
        }
        if (mVar.b()) {
            i3 = i4;
        }
        if (i3 == 0) {
            return -1;
        }
        int i6 = l + i3;
        if (i6 < 0) {
            i6 = 0;
        }
        return i6 >= j ? i5 : i6;
    }

    @DexIgnore
    public final int a(RecyclerView.m mVar, View view, xe xeVar) {
        int i;
        int d2 = xeVar.d(view) + (xeVar.b(view) / 2);
        if (mVar.f()) {
            i = xeVar.f() + (xeVar.g() / 2);
        } else {
            i = xeVar.a() / 2;
        }
        return d2 - i;
    }

    @DexIgnore
    public final int a(RecyclerView.m mVar, xe xeVar, int i, int i2) {
        int[] b = b(i, i2);
        float a = a(mVar, xeVar);
        if (a <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return 0;
        }
        return Math.round(((float) (Math.abs(b[0]) > Math.abs(b[1]) ? b[0] : b[1])) / a);
    }

    @DexIgnore
    public final float a(RecyclerView.m mVar, xe xeVar) {
        int e2 = mVar.e();
        if (e2 == 0) {
            return 1.0f;
        }
        View view = null;
        View view2 = null;
        int i = Integer.MAX_VALUE;
        int i2 = Integer.MIN_VALUE;
        for (int i3 = 0; i3 < e2; i3++) {
            View d2 = mVar.d(i3);
            int l = mVar.l(d2);
            if (l != -1) {
                if (l < i) {
                    view = d2;
                    i = l;
                }
                if (l > i2) {
                    view2 = d2;
                    i2 = l;
                }
            }
        }
        if (view == null || view2 == null) {
            return 1.0f;
        }
        int max = Math.max(xeVar.a(view), xeVar.a(view2)) - Math.min(xeVar.d(view), xeVar.d(view2));
        if (max == 0) {
            return 1.0f;
        }
        return (((float) max) * 1.0f) / ((float) ((i2 - i) + 1));
    }
}
