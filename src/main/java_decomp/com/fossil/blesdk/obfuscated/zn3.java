package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zn3 {
    @DexIgnore
    public /* final */ xn3 a;
    @DexIgnore
    public /* final */ ArrayList<Integer> b;

    @DexIgnore
    public zn3(xn3 xn3, ArrayList<Integer> arrayList) {
        wd4.b(xn3, "mView");
        this.a = xn3;
        this.b = arrayList;
    }

    @DexIgnore
    public final ArrayList<Integer> a() {
        ArrayList<Integer> arrayList = this.b;
        if (arrayList != null) {
            return arrayList;
        }
        return new ArrayList<>();
    }

    @DexIgnore
    public final xn3 b() {
        return this.a;
    }
}
