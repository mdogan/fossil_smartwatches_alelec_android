package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zk0 extends ls0 implements yk0 {
    @DexIgnore
    public zk0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.service.ICommonService");
    }

    @DexIgnore
    public final void a(wk0 wk0) throws RemoteException {
        Parcel o = o();
        ns0.a(o, (IInterface) wk0);
        c(1, o);
    }
}
