package com.fossil.blesdk.obfuscated;

import android.location.Location;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w31 extends m31 implements v31 {
    @DexIgnore
    public w31(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IGoogleLocationManagerService");
    }

    @DexIgnore
    public final void a(l41 l41) throws RemoteException {
        Parcel o = o();
        r41.a(o, (Parcelable) l41);
        b(59, o);
    }

    @DexIgnore
    public final void a(uc1 uc1, x31 x31, String str) throws RemoteException {
        Parcel o = o();
        r41.a(o, (Parcelable) uc1);
        r41.a(o, (IInterface) x31);
        o.writeString(str);
        b(63, o);
    }

    @DexIgnore
    public final void a(w41 w41) throws RemoteException {
        Parcel o = o();
        r41.a(o, (Parcelable) w41);
        b(75, o);
    }

    @DexIgnore
    public final Location b(String str) throws RemoteException {
        Parcel o = o();
        o.writeString(str);
        Parcel a = a(21, o);
        Location location = (Location) r41.a(a, Location.CREATOR);
        a.recycle();
        return location;
    }

    @DexIgnore
    public final void e(boolean z) throws RemoteException {
        Parcel o = o();
        r41.a(o, z);
        b(12, o);
    }
}
