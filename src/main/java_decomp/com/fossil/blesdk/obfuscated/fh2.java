package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class fh2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ SwitchCompat u;

    @DexIgnore
    public fh2(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, ImageView imageView, ConstraintLayout constraintLayout2, SwitchCompat switchCompat) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = imageView;
        this.u = switchCompat;
    }

    @DexIgnore
    public static fh2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, ra.a());
    }

    @DexIgnore
    @Deprecated
    public static fh2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (fh2) ViewDataBinding.a(layoutInflater, (int) R.layout.item_app_hybrid_notification, viewGroup, z, obj);
    }
}
