package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e52 implements Factory<c62> {
    @DexIgnore
    public /* final */ o42 a;
    @DexIgnore
    public /* final */ Provider<fn2> b;
    @DexIgnore
    public /* final */ Provider<UserRepository> c;
    @DexIgnore
    public /* final */ Provider<HybridPresetDao> d;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> e;
    @DexIgnore
    public /* final */ Provider<DeviceDao> f;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> g;

    @DexIgnore
    public e52(o42 o42, Provider<fn2> provider, Provider<UserRepository> provider2, Provider<HybridPresetDao> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceDao> provider5, Provider<PortfolioApp> provider6) {
        this.a = o42;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
    }

    @DexIgnore
    public static e52 a(o42 o42, Provider<fn2> provider, Provider<UserRepository> provider2, Provider<HybridPresetDao> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceDao> provider5, Provider<PortfolioApp> provider6) {
        return new e52(o42, provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static c62 b(o42 o42, Provider<fn2> provider, Provider<UserRepository> provider2, Provider<HybridPresetDao> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceDao> provider5, Provider<PortfolioApp> provider6) {
        return a(o42, provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get());
    }

    @DexIgnore
    public static c62 a(o42 o42, fn2 fn2, UserRepository userRepository, HybridPresetDao hybridPresetDao, NotificationsRepository notificationsRepository, DeviceDao deviceDao, PortfolioApp portfolioApp) {
        c62 a2 = o42.a(fn2, userRepository, hybridPresetDao, notificationsRepository, deviceDao, portfolioApp);
        o44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public c62 get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g);
    }
}
