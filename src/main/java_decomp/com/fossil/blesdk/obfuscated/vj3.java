package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.or2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vj3 extends yj3 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a((rd4) null);
    @DexIgnore
    public MFUser f;
    @DexIgnore
    public /* final */ zj3 g;
    @DexIgnore
    public /* final */ UpdateUser h;
    @DexIgnore
    public /* final */ or2 i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return vj3.j;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<UpdateUser.d, UpdateUser.c> {
        @DexIgnore
        public /* final */ /* synthetic */ vj3 a;
        @DexIgnore
        public /* final */ /* synthetic */ Unit b;

        @DexIgnore
        public b(vj3 vj3, Unit unit) {
            this.a = vj3;
            this.b = unit;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            wd4.b(dVar, "responseValue");
            this.a.g.a();
            this.a.g.e(this.b);
            MFUser h = this.a.h();
            if (h != null) {
                UserDisplayUnit a2 = rj2.a(h);
                if (a2 != null) {
                    PortfolioApp.W.c().a(a2, PortfolioApp.W.c().e());
                }
            }
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wd4.b(cVar, "errorValue");
            this.a.g.a();
            this.a.g.a(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.e<UpdateUser.d, UpdateUser.c> {
        @DexIgnore
        public /* final */ /* synthetic */ vj3 a;
        @DexIgnore
        public /* final */ /* synthetic */ Unit b;

        @DexIgnore
        public c(vj3 vj3, Unit unit) {
            this.a = vj3;
            this.b = unit;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            wd4.b(dVar, "responseValue");
            this.a.g.a();
            this.a.g.c(this.b);
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wd4.b(cVar, "errorValue");
            this.a.g.a();
            this.a.g.a(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.e<UpdateUser.d, UpdateUser.c> {
        @DexIgnore
        public /* final */ /* synthetic */ vj3 a;
        @DexIgnore
        public /* final */ /* synthetic */ Unit b;

        @DexIgnore
        public d(vj3 vj3, Unit unit) {
            this.a = vj3;
            this.b = unit;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            wd4.b(dVar, "responseValue");
            this.a.g.a();
            this.a.g.a(this.b);
            MFUser h = this.a.h();
            if (h != null) {
                UserDisplayUnit a2 = rj2.a(h);
                if (a2 != null) {
                    PortfolioApp.W.c().a(a2, PortfolioApp.W.c().e());
                }
            }
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wd4.b(cVar, "errorValue");
            this.a.g.a();
            this.a.g.a(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CoroutineUseCase.e<UpdateUser.d, UpdateUser.c> {
        @DexIgnore
        public /* final */ /* synthetic */ vj3 a;
        @DexIgnore
        public /* final */ /* synthetic */ Unit b;

        @DexIgnore
        public e(vj3 vj3, Unit unit) {
            this.a = vj3;
            this.b = unit;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            wd4.b(dVar, "responseValue");
            this.a.g.a();
            this.a.g.d(this.b);
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wd4.b(cVar, "errorValue");
            this.a.g.a();
            this.a.g.a(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CoroutineUseCase.e<or2.a, CoroutineUseCase.a> {
        @DexIgnore
        public /* final */ /* synthetic */ vj3 a;

        @DexIgnore
        public f(vj3 vj3) {
            this.a = vj3;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(or2.a aVar) {
            wd4.b(aVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(vj3.k.a(), "GetUser information onSuccess");
            this.a.g.a();
            this.a.a(aVar.a());
            if (this.a.g.isActive() && this.a.h() != null) {
                zj3 a2 = this.a.g;
                MFUser h = this.a.h();
                if (h != null) {
                    a2.c(h.getHeightUnit());
                    zj3 a3 = this.a.g;
                    MFUser h2 = this.a.h();
                    if (h2 != null) {
                        a3.d(h2.getWeightUnit());
                        zj3 a4 = this.a.g;
                        MFUser h3 = this.a.h();
                        if (h3 != null) {
                            a4.e(h3.getDistanceUnit());
                            zj3 a5 = this.a.g;
                            MFUser h4 = this.a.h();
                            if (h4 != null) {
                                a5.a(h4.getTemperatureUnit());
                            } else {
                                wd4.a();
                                throw null;
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public void a(CoroutineUseCase.a aVar) {
            wd4.b(aVar, "errorValue");
            this.a.g.a();
            FLogger.INSTANCE.getLocal().d(vj3.k.a(), "GetUser information onError");
        }
    }

    /*
    static {
        String simpleName = vj3.class.getSimpleName();
        wd4.a((Object) simpleName, "PreferredUnitPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public vj3(zj3 zj3, UpdateUser updateUser, or2 or2) {
        wd4.b(zj3, "mView");
        wd4.b(updateUser, "mUpdateUser");
        wd4.b(or2, "mGetUser");
        this.g = zj3;
        this.h = updateUser;
        this.i = or2;
    }

    @DexIgnore
    public void b(Unit unit) {
        wd4.b(unit, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "setHeightUnit() called with: unit = [" + unit + ']');
        MFUser mFUser = this.f;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(j, "Can't save height with null user");
        } else if (mFUser != null) {
            mFUser.setHeightUnit(unit.getValue());
            this.g.b();
            UpdateUser updateUser = this.h;
            MFUser mFUser2 = this.f;
            if (mFUser2 != null) {
                updateUser.a(new UpdateUser.b(mFUser2), new c(this, unit));
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void c(Unit unit) {
        wd4.b(unit, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "setTemperatureUnit() called with: unit = [" + unit + ']');
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = j;
        local2.d(str2, "setTemperatureUnit: unit = " + unit);
        MFUser mFUser = this.f;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(j, "Can't save temperature unit with null user");
        } else if (mFUser != null) {
            mFUser.setTemperatureUnit(unit.getValue());
            this.g.b();
            UpdateUser updateUser = this.h;
            MFUser mFUser2 = this.f;
            if (mFUser2 != null) {
                updateUser.a(new UpdateUser.b(mFUser2), new d(this, unit));
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void d(Unit unit) {
        wd4.b(unit, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "setWeightUnit() called with: unit = [" + unit + ']');
        MFUser mFUser = this.f;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(j, "Can't save weight with null user");
        } else if (mFUser != null) {
            mFUser.setWeightUnit(unit.getValue());
            this.g.b();
            UpdateUser updateUser = this.h;
            MFUser mFUser2 = this.f;
            if (mFUser2 != null) {
                updateUser.a(new UpdateUser.b(mFUser2), new e(this, unit));
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(j, "presenter starts: Get user information");
        this.g.b();
        this.i.a(null, new f(this));
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(j, "presenter stop");
    }

    @DexIgnore
    public final MFUser h() {
        return this.f;
    }

    @DexIgnore
    public void i() {
        this.g.a(this);
    }

    @DexIgnore
    public final void a(MFUser mFUser) {
        this.f = mFUser;
    }

    @DexIgnore
    public void a(Unit unit) {
        wd4.b(unit, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "setDistanceUnit() called with: unit = [" + unit + ']');
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = j;
        local2.d(str2, "setDistanceUnit: unit = " + unit);
        MFUser mFUser = this.f;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(j, "Can't save distance unit with null user");
        } else if (mFUser != null) {
            mFUser.setDistanceUnit(unit.getValue());
            this.g.b();
            UpdateUser updateUser = this.h;
            MFUser mFUser2 = this.f;
            if (mFUser2 != null) {
                updateUser.a(new UpdateUser.b(mFUser2), new b(this, unit));
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }
}
