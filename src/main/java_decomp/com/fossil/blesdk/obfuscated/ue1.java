package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ue1 extends c51 implements te1 {
    @DexIgnore
    public ue1() {
        super("com.google.android.gms.maps.internal.IOnMapReadyCallback");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        ke1 ke1;
        if (i != 1) {
            return false;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        if (readStrongBinder == null) {
            ke1 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IGoogleMapDelegate");
            if (queryLocalInterface instanceof ke1) {
                ke1 = queryLocalInterface;
            } else {
                ke1 = new ff1(readStrongBinder);
            }
        }
        a(ke1);
        parcel2.writeNoException();
        return true;
    }
}
