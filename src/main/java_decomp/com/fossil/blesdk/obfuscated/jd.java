package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ie;
import com.fossil.blesdk.obfuscated.me;
import com.fossil.blesdk.obfuscated.rd;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jd<T> {
    @DexIgnore
    public /* final */ ve a;
    @DexIgnore
    public /* final */ ie<T> b;
    @DexIgnore
    public Executor c; // = h3.d();
    @DexIgnore
    public /* final */ List<c<T>> d; // = new CopyOnWriteArrayList();
    @DexIgnore
    public boolean e;
    @DexIgnore
    public rd<T> f;
    @DexIgnore
    public rd<T> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public rd.e i; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends rd.e {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(int i, int i2) {
            jd.this.a.a(i, i2, (Object) null);
        }

        @DexIgnore
        public void b(int i, int i2) {
            jd.this.a.b(i, i2);
        }

        @DexIgnore
        public void c(int i, int i2) {
            jd.this.a.c(i, i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ rd e;
        @DexIgnore
        public /* final */ /* synthetic */ rd f;
        @DexIgnore
        public /* final */ /* synthetic */ int g;
        @DexIgnore
        public /* final */ /* synthetic */ rd h;
        @DexIgnore
        public /* final */ /* synthetic */ Runnable i;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ me.c e;

            @DexIgnore
            public a(me.c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public void run() {
                b bVar = b.this;
                jd jdVar = jd.this;
                if (jdVar.h == bVar.g) {
                    jdVar.a(bVar.h, bVar.f, this.e, bVar.e.j, bVar.i);
                }
            }
        }

        @DexIgnore
        public b(rd rdVar, rd rdVar2, int i2, rd rdVar3, Runnable runnable) {
            this.e = rdVar;
            this.f = rdVar2;
            this.g = i2;
            this.h = rdVar3;
            this.i = runnable;
        }

        @DexIgnore
        public void run() {
            jd.this.c.execute(new a(ud.a(this.e.i, this.f.i, jd.this.b.b())));
        }
    }

    @DexIgnore
    public interface c<T> {
        @DexIgnore
        void a(rd<T> rdVar, rd<T> rdVar2);
    }

    @DexIgnore
    public jd(RecyclerView.g gVar, me.d<T> dVar) {
        this.a = new he(gVar);
        this.b = new ie.a(dVar).a();
    }

    @DexIgnore
    public T a(int i2) {
        rd<T> rdVar = this.f;
        if (rdVar == null) {
            rd<T> rdVar2 = this.g;
            if (rdVar2 != null) {
                return rdVar2.get(i2);
            }
            throw new IndexOutOfBoundsException("Item count is zero, getItem() call is invalid");
        }
        rdVar.g(i2);
        return this.f.get(i2);
    }

    @DexIgnore
    public int a() {
        rd<T> rdVar = this.f;
        if (rdVar != null) {
            return rdVar.size();
        }
        rd<T> rdVar2 = this.g;
        if (rdVar2 == null) {
            return 0;
        }
        return rdVar2.size();
    }

    @DexIgnore
    public void a(rd<T> rdVar) {
        a(rdVar, (Runnable) null);
    }

    @DexIgnore
    public void a(rd<T> rdVar, Runnable runnable) {
        if (rdVar != null) {
            if (this.f == null && this.g == null) {
                this.e = rdVar.g();
            } else if (rdVar.g() != this.e) {
                throw new IllegalArgumentException("AsyncPagedListDiffer cannot handle both contiguous and non-contiguous lists.");
            }
        }
        int i2 = this.h + 1;
        this.h = i2;
        rd<T> rdVar2 = this.f;
        if (rdVar != rdVar2) {
            rd<T> rdVar3 = this.g;
            if (rdVar3 != null) {
                rdVar2 = rdVar3;
            }
            if (rdVar == null) {
                int a2 = a();
                rd<T> rdVar4 = this.f;
                if (rdVar4 != null) {
                    rdVar4.a(this.i);
                    this.f = null;
                } else if (this.g != null) {
                    this.g = null;
                }
                this.a.c(0, a2);
                a(rdVar2, (rd<T>) null, runnable);
            } else if (this.f == null && this.g == null) {
                this.f = rdVar;
                rdVar.a((List<T>) null, this.i);
                this.a.b(0, rdVar.size());
                a((rd) null, rdVar, runnable);
            } else {
                rd<T> rdVar5 = this.f;
                if (rdVar5 != null) {
                    rdVar5.a(this.i);
                    this.g = (rd) this.f.j();
                    this.f = null;
                }
                rd<T> rdVar6 = this.g;
                if (rdVar6 == null || this.f != null) {
                    throw new IllegalStateException("must be in snapshot state to diff");
                }
                this.b.a().execute(new b(rdVar6, (rd) rdVar.j(), i2, rdVar, runnable));
            }
        } else if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void a(rd<T> rdVar, rd<T> rdVar2, me.c cVar, int i2, Runnable runnable) {
        rd<T> rdVar3 = this.g;
        if (rdVar3 == null || this.f != null) {
            throw new IllegalStateException("must be in snapshot state to apply diff");
        }
        this.f = rdVar;
        this.g = null;
        ud.a(this.a, rdVar3.i, rdVar.i, cVar);
        rdVar.a((List<T>) rdVar2, this.i);
        if (!this.f.isEmpty()) {
            int a2 = ud.a(cVar, (td) rdVar3.i, (td) rdVar2.i, i2);
            rd<T> rdVar4 = this.f;
            rdVar4.g(Math.max(0, Math.min(rdVar4.size() - 1, a2)));
        }
        a(rdVar3, this.f, runnable);
    }

    @DexIgnore
    public final void a(rd<T> rdVar, rd<T> rdVar2, Runnable runnable) {
        for (c<T> a2 : this.d) {
            a2.a(rdVar, rdVar2);
        }
        if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void a(c<T> cVar) {
        this.d.add(cVar);
    }
}
