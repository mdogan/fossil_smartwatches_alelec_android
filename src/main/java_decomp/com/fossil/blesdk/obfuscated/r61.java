package com.fossil.blesdk.obfuscated;

import android.database.ContentObserver;
import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r61 extends ContentObserver {
    @DexIgnore
    public /* final */ /* synthetic */ p61 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r61(p61 p61, Handler handler) {
        super((Handler) null);
        this.a = p61;
    }

    @DexIgnore
    public final void onChange(boolean z) {
        this.a.b();
    }
}
