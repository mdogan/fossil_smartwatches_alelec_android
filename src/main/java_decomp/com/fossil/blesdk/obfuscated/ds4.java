package com.fossil.blesdk.obfuscated;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ds4<T> {
    @DexIgnore
    public static <T> ds4<T> a(Retrofit retrofit3, Method method) {
        bs4 a = bs4.a(retrofit3, method);
        Type genericReturnType = method.getGenericReturnType();
        if (gs4.c(genericReturnType)) {
            throw gs4.a(method, "Method return type must not include a type variable or wildcard: %s", genericReturnType);
        } else if (genericReturnType != Void.TYPE) {
            return ur4.a(retrofit3, method, a);
        } else {
            throw gs4.a(method, "Service methods cannot return void.", new Object[0]);
        }
    }

    @DexIgnore
    public abstract T a(Object[] objArr);
}
