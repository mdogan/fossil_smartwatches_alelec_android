package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dn4 implements Interceptor {
    @DexIgnore
    public /* final */ OkHttpClient a;

    @DexIgnore
    public dn4(OkHttpClient okHttpClient) {
        this.a = okHttpClient;
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        pn4 pn4 = (pn4) chain;
        pm4 n = pn4.n();
        in4 h = pn4.h();
        return pn4.a(n, h, h.a(this.a, chain, !n.e().equals("GET")), h.c());
    }
}
