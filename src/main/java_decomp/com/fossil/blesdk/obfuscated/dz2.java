package com.fossil.blesdk.obfuscated;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.fossil.wearables.fossil.R;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dz2 extends ys3 {
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ a w; // = new a((rd4) null);
    @DexIgnore
    public int m; // = -1;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public /* final */ qa r; // = new w62(this);
    @DexIgnore
    public ur3<pd2> s;
    @DexIgnore
    public b t;
    @DexIgnore
    public HashMap u;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return dz2.v;
        }

        @DexIgnore
        public final dz2 b() {
            return new dz2();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, boolean z, boolean z2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dz2 e;

        @DexIgnore
        public c(dz2 dz2) {
            this.e = dz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            dz2 dz2 = this.e;
            dz2.onDismiss(dz2.getDialog());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dz2 e;
        @DexIgnore
        public /* final */ /* synthetic */ pd2 f;

        @DexIgnore
        public d(dz2 dz2, pd2 pd2) {
            this.e = dz2;
            this.f = pd2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ImageView imageView = this.f.u;
            wd4.a((Object) imageView, "binding.ivCallsMessageCheck");
            if (imageView.getVisibility() == 4) {
                ImageView imageView2 = this.f.u;
                wd4.a((Object) imageView2, "binding.ivCallsMessageCheck");
                imageView2.setVisibility(0);
                ImageView imageView3 = this.f.t;
                wd4.a((Object) imageView3, "binding.ivCallsCheck");
                imageView3.setVisibility(4);
                ImageView imageView4 = this.f.v;
                wd4.a((Object) imageView4, "binding.ivMessagesCheck");
                imageView4.setVisibility(4);
                this.e.o = true;
                this.e.n = true;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dz2 e;
        @DexIgnore
        public /* final */ /* synthetic */ pd2 f;

        @DexIgnore
        public e(dz2 dz2, pd2 pd2) {
            this.e = dz2;
            this.f = pd2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ImageView imageView = this.f.t;
            wd4.a((Object) imageView, "binding.ivCallsCheck");
            if (imageView.getVisibility() == 4) {
                ImageView imageView2 = this.f.t;
                wd4.a((Object) imageView2, "binding.ivCallsCheck");
                imageView2.setVisibility(0);
                ImageView imageView3 = this.f.u;
                wd4.a((Object) imageView3, "binding.ivCallsMessageCheck");
                imageView3.setVisibility(4);
                ImageView imageView4 = this.f.v;
                wd4.a((Object) imageView4, "binding.ivMessagesCheck");
                imageView4.setVisibility(4);
                this.e.n = true;
                this.e.o = false;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dz2 e;
        @DexIgnore
        public /* final */ /* synthetic */ pd2 f;

        @DexIgnore
        public f(dz2 dz2, pd2 pd2) {
            this.e = dz2;
            this.f = pd2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ImageView imageView = this.f.v;
            wd4.a((Object) imageView, "binding.ivMessagesCheck");
            if (imageView.getVisibility() == 4) {
                ImageView imageView2 = this.f.v;
                wd4.a((Object) imageView2, "binding.ivMessagesCheck");
                imageView2.setVisibility(0);
                ImageView imageView3 = this.f.u;
                wd4.a((Object) imageView3, "binding.ivCallsMessageCheck");
                imageView3.setVisibility(4);
                ImageView imageView4 = this.f.t;
                wd4.a((Object) imageView4, "binding.ivCallsCheck");
                imageView4.setVisibility(4);
                this.e.o = true;
                this.e.n = false;
            }
        }
    }

    /*
    static {
        String simpleName = dz2.class.getSimpleName();
        wd4.a((Object) simpleName, "NotificationAllowCallsAn\u2026nt::class.java.simpleName");
        v = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void P0() {
        if (this.o && this.n) {
            ur3<pd2> ur3 = this.s;
            if (ur3 != null) {
                pd2 a2 = ur3.a();
                if (a2 != null) {
                    ImageView imageView = a2.u;
                    wd4.a((Object) imageView, "it.ivCallsMessageCheck");
                    imageView.setVisibility(0);
                    ImageView imageView2 = a2.t;
                    wd4.a((Object) imageView2, "it.ivCallsCheck");
                    imageView2.setVisibility(4);
                    ImageView imageView3 = a2.v;
                    wd4.a((Object) imageView3, "it.ivMessagesCheck");
                    imageView3.setVisibility(4);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        } else if (this.n) {
            ur3<pd2> ur32 = this.s;
            if (ur32 != null) {
                pd2 a3 = ur32.a();
                if (a3 != null) {
                    ImageView imageView4 = a3.u;
                    wd4.a((Object) imageView4, "it.ivCallsMessageCheck");
                    imageView4.setVisibility(4);
                    ImageView imageView5 = a3.t;
                    wd4.a((Object) imageView5, "it.ivCallsCheck");
                    imageView5.setVisibility(0);
                    ImageView imageView6 = a3.v;
                    wd4.a((Object) imageView6, "it.ivMessagesCheck");
                    imageView6.setVisibility(4);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        } else {
            ur3<pd2> ur33 = this.s;
            if (ur33 != null) {
                pd2 a4 = ur33.a();
                if (a4 != null) {
                    ImageView imageView7 = a4.u;
                    wd4.a((Object) imageView7, "it.ivCallsMessageCheck");
                    imageView7.setVisibility(4);
                    ImageView imageView8 = a4.t;
                    wd4.a((Object) imageView8, "it.ivCallsCheck");
                    imageView8.setVisibility(4);
                    ImageView imageView9 = a4.v;
                    wd4.a((Object) imageView9, "it.ivMessagesCheck");
                    imageView9.setVisibility(0);
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        pd2 pd2 = (pd2) ra.a(layoutInflater, R.layout.fragment_notification_allow_calls_and_messages, viewGroup, false, this.r);
        pd2.w.setOnClickListener(new c(this));
        this.s = new ur3<>(this, pd2);
        P0();
        pd2.r.setOnClickListener(new d(this, pd2));
        pd2.q.setOnClickListener(new e(this, pd2));
        pd2.s.setOnClickListener(new f(this, pd2));
        wd4.a((Object) pd2, "binding");
        return pd2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        if (!(this.p == this.n && this.q == this.o)) {
            b bVar = this.t;
            if (bVar != null) {
                bVar.a(this.m, this.n, this.o);
            }
        }
        super.onDismiss(dialogInterface);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        P0();
    }

    @DexIgnore
    public final void a(int i, boolean z, boolean z2) {
        this.m = i;
        this.n = z;
        this.o = z2;
        this.p = z;
        this.q = z2;
    }

    @DexIgnore
    public final void a(b bVar) {
        wd4.b(bVar, "onDismissListener");
        this.t = bVar;
    }
}
