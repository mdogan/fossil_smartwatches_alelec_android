package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dk1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean e;
    @DexIgnore
    public /* final */ /* synthetic */ boolean f;
    @DexIgnore
    public /* final */ /* synthetic */ ig1 g;
    @DexIgnore
    public /* final */ /* synthetic */ sl1 h;
    @DexIgnore
    public /* final */ /* synthetic */ String i;
    @DexIgnore
    public /* final */ /* synthetic */ wj1 j;

    @DexIgnore
    public dk1(wj1 wj1, boolean z, boolean z2, ig1 ig1, sl1 sl1, String str) {
        this.j = wj1;
        this.e = z;
        this.f = z2;
        this.g = ig1;
        this.h = sl1;
        this.i = str;
    }

    @DexIgnore
    public final void run() {
        lg1 d = this.j.d;
        if (d == null) {
            this.j.d().s().a("Discarding data. Failed to send event to service");
            return;
        }
        if (this.e) {
            this.j.a(d, this.f ? null : this.g, this.h);
        } else {
            try {
                if (TextUtils.isEmpty(this.i)) {
                    d.a(this.g, this.h);
                } else {
                    d.a(this.g, this.i, this.j.d().C());
                }
            } catch (RemoteException e2) {
                this.j.d().s().a("Failed to send event to the service", e2);
            }
        }
        this.j.C();
    }
}
