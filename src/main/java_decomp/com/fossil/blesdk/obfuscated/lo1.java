package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lo1<TResult> implements ro1<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public tn1 c;

    @DexIgnore
    public lo1(Executor executor, tn1 tn1) {
        this.a = executor;
        this.c = tn1;
    }

    @DexIgnore
    public final void onComplete(xn1<TResult> xn1) {
        if (!xn1.e() && !xn1.c()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new mo1(this, xn1));
                }
            }
        }
    }
}
