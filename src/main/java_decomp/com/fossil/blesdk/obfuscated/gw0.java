package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gw0<E> extends nt0<E> {
    @DexIgnore
    public static /* final */ gw0<Object> g;
    @DexIgnore
    public /* final */ List<E> f;

    /*
    static {
        gw0<Object> gw0 = new gw0<>();
        g = gw0;
        gw0.z();
    }
    */

    @DexIgnore
    public gw0() {
        this(new ArrayList(10));
    }

    @DexIgnore
    public gw0(List<E> list) {
        this.f = list;
    }

    @DexIgnore
    public static <E> gw0<E> b() {
        return g;
    }

    @DexIgnore
    public final void add(int i, E e) {
        a();
        this.f.add(i, e);
        this.modCount++;
    }

    @DexIgnore
    public final /* synthetic */ xu0 c(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.f);
            return new gw0(arrayList);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final E get(int i) {
        return this.f.get(i);
    }

    @DexIgnore
    public final E remove(int i) {
        a();
        E remove = this.f.remove(i);
        this.modCount++;
        return remove;
    }

    @DexIgnore
    public final E set(int i, E e) {
        a();
        E e2 = this.f.set(i, e);
        this.modCount++;
        return e2;
    }

    @DexIgnore
    public final int size() {
        return this.f.size();
    }
}
