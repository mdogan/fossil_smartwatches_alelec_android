package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.sr4;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class at4 extends sr4.a {
    @DexIgnore
    public static at4 a() {
        return new at4();
    }

    @DexIgnore
    public sr4<?, RequestBody> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit3) {
        if (type == String.class || type == Boolean.TYPE || type == Boolean.class || type == Byte.TYPE || type == Byte.class || type == Character.TYPE || type == Character.class || type == Double.TYPE || type == Double.class || type == Float.TYPE || type == Float.class || type == Integer.TYPE || type == Integer.class || type == Long.TYPE || type == Long.class || type == Short.TYPE || type == Short.class) {
            return qs4.a;
        }
        return null;
    }

    @DexIgnore
    public sr4<qm4, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (type == String.class) {
            return zs4.a;
        }
        if (type == Boolean.class || type == Boolean.TYPE) {
            return rs4.a;
        }
        if (type == Byte.class || type == Byte.TYPE) {
            return ss4.a;
        }
        if (type == Character.class || type == Character.TYPE) {
            return ts4.a;
        }
        if (type == Double.class || type == Double.TYPE) {
            return us4.a;
        }
        if (type == Float.class || type == Float.TYPE) {
            return vs4.a;
        }
        if (type == Integer.class || type == Integer.TYPE) {
            return ws4.a;
        }
        if (type == Long.class || type == Long.TYPE) {
            return xs4.a;
        }
        if (type == Short.class || type == Short.TYPE) {
            return ys4.a;
        }
        return null;
    }
}
