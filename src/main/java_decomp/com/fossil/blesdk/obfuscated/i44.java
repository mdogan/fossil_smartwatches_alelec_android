package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class i44 extends AppCompatActivity implements g44, k44 {
    @DexIgnore
    public void onCreate(Bundle bundle) {
        u34.a((Activity) this);
        super.onCreate(bundle);
    }
}
