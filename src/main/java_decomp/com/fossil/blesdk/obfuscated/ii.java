package com.fossil.blesdk.obfuscated;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ii implements ki {
    @DexIgnore
    public /* final */ IBinder a;

    @DexIgnore
    public ii(IBinder iBinder) {
        this.a = iBinder;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof ii) && ((ii) obj).a.equals(this.a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }
}
