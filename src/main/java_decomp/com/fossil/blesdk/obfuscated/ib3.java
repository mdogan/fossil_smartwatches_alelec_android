package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ib3 implements Factory<GoalTrackingOverviewDayPresenter> {
    @DexIgnore
    public static GoalTrackingOverviewDayPresenter a(gb3 gb3, fn2 fn2, GoalTrackingRepository goalTrackingRepository) {
        return new GoalTrackingOverviewDayPresenter(gb3, fn2, goalTrackingRepository);
    }
}
