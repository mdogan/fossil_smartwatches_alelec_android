package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ak0;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fp0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<fp0> CREATOR; // = new ip0();
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ kp0 k;
    @DexIgnore
    public /* final */ Long l;

    @DexIgnore
    public fp0(long j2, long j3, String str, String str2, String str3, int i2, kp0 kp0, Long l2) {
        this.e = j2;
        this.f = j3;
        this.g = str;
        this.h = str2;
        this.i = str3;
        this.j = i2;
        this.k = kp0;
        this.l = l2;
    }

    @DexIgnore
    public String H() {
        return this.i;
    }

    @DexIgnore
    public String I() {
        return this.h;
    }

    @DexIgnore
    public String J() {
        return this.g;
    }

    @DexIgnore
    public long a(TimeUnit timeUnit) {
        return timeUnit.convert(this.f, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public long b(TimeUnit timeUnit) {
        return timeUnit.convert(this.e, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fp0)) {
            return false;
        }
        fp0 fp0 = (fp0) obj;
        return this.e == fp0.e && this.f == fp0.f && ak0.a(this.g, fp0.g) && ak0.a(this.h, fp0.h) && ak0.a(this.i, fp0.i) && ak0.a(this.k, fp0.k) && this.j == fp0.j;
    }

    @DexIgnore
    public int hashCode() {
        return ak0.a(Long.valueOf(this.e), Long.valueOf(this.f), this.h);
    }

    @DexIgnore
    public String toString() {
        ak0.a a2 = ak0.a((Object) this);
        a2.a(SampleRaw.COLUMN_START_TIME, Long.valueOf(this.e));
        a2.a(SampleRaw.COLUMN_END_TIME, Long.valueOf(this.f));
        a2.a("name", this.g);
        a2.a("identifier", this.h);
        a2.a("description", this.i);
        a2.a(Constants.ACTIVITY, Integer.valueOf(this.j));
        a2.a("application", this.k);
        return a2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, this.f);
        lk0.a(parcel, 3, J(), false);
        lk0.a(parcel, 4, I(), false);
        lk0.a(parcel, 5, H(), false);
        lk0.a(parcel, 7, this.j);
        lk0.a(parcel, 8, (Parcelable) this.k, i2, false);
        lk0.a(parcel, 9, this.l, false);
        lk0.a(parcel, a2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public long a; // = 0;
        @DexIgnore
        public long b; // = 0;
        @DexIgnore
        public String c; // = null;
        @DexIgnore
        public String d; // = null;
        @DexIgnore
        public String e; // = "";
        @DexIgnore
        public int f; // = 4;
        @DexIgnore
        public Long g;

        @DexIgnore
        public a a(long j, TimeUnit timeUnit) {
            ck0.b(j >= 0, "End time should be positive.");
            this.b = timeUnit.toMillis(j);
            return this;
        }

        @DexIgnore
        public a b(long j, TimeUnit timeUnit) {
            ck0.b(j > 0, "Start time should be positive.");
            this.a = timeUnit.toMillis(j);
            return this;
        }

        @DexIgnore
        public a c(String str) {
            ck0.a(str != null && TextUtils.getTrimmedLength(str) > 0);
            this.d = str;
            return this;
        }

        @DexIgnore
        public a d(String str) {
            ck0.a(str.length() <= 100, "Session name cannot exceed %d characters", 100);
            this.c = str;
            return this;
        }

        @DexIgnore
        public a a(String str) {
            this.f = e21.a(str);
            return this;
        }

        @DexIgnore
        public a b(String str) {
            ck0.a(str.length() <= 1000, "Session description cannot exceed %d characters", 1000);
            this.e = str;
            return this;
        }

        @DexIgnore
        public fp0 a() {
            boolean z = true;
            ck0.b(this.a > 0, "Start time should be specified.");
            long j = this.b;
            if (j != 0 && j <= this.a) {
                z = false;
            }
            ck0.b(z, "End time should be later than start time.");
            if (this.d == null) {
                String str = this.c;
                if (str == null) {
                    str = "";
                }
                long j2 = this.a;
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 20);
                sb.append(str);
                sb.append(j2);
                this.d = sb.toString();
            }
            return new fp0(this);
        }
    }

    @DexIgnore
    public fp0(a aVar) {
        this(aVar.a, aVar.b, aVar.c, aVar.d, aVar.e, aVar.f, (kp0) null, aVar.g);
    }
}
