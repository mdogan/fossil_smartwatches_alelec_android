package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.fossil.blesdk.obfuscated.tk1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ok1<T extends Context & tk1> {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public ok1(T t) {
        ck0.a(t);
        this.a = t;
    }

    @DexIgnore
    public final void a() {
        yh1 a2 = yh1.a((Context) this.a, (pg1) null);
        ug1 d = a2.d();
        a2.b();
        d.A().a("Local AppMeasurementService is starting up");
    }

    @DexIgnore
    public final void b() {
        yh1 a2 = yh1.a((Context) this.a, (pg1) null);
        ug1 d = a2.d();
        a2.b();
        d.A().a("Local AppMeasurementService is shutting down");
    }

    @DexIgnore
    public final boolean c(Intent intent) {
        if (intent == null) {
            c().s().a("onUnbind called with null intent");
            return true;
        }
        c().A().a("onUnbind called for intent. action", intent.getAction());
        return true;
    }

    @DexIgnore
    public final ug1 c() {
        return yh1.a((Context) this.a, (pg1) null).d();
    }

    @DexIgnore
    public final int a(Intent intent, int i, int i2) {
        yh1 a2 = yh1.a((Context) this.a, (pg1) null);
        ug1 d = a2.d();
        if (intent == null) {
            d.v().a("AppMeasurementService started with null intent");
            return 2;
        }
        String action = intent.getAction();
        a2.b();
        d.A().a("Local AppMeasurementService called. startId, action", Integer.valueOf(i2), action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            a((Runnable) new pk1(this, i2, d, intent));
        }
        return 2;
    }

    @DexIgnore
    public final void b(Intent intent) {
        if (intent == null) {
            c().s().a("onRebind called with null intent");
            return;
        }
        c().A().a("onRebind called. action", intent.getAction());
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        el1 a2 = el1.a((Context) this.a);
        a2.a().a((Runnable) new sk1(this, a2, runnable));
    }

    @DexIgnore
    public final IBinder a(Intent intent) {
        if (intent == null) {
            c().s().a("onBind called with null intent");
            return null;
        }
        String action = intent.getAction();
        if ("com.google.android.gms.measurement.START".equals(action)) {
            return new ai1(el1.a((Context) this.a));
        }
        c().v().a("onBind received unknown action", action);
        return null;
    }

    @DexIgnore
    @TargetApi(24)
    public final boolean a(JobParameters jobParameters) {
        yh1 a2 = yh1.a((Context) this.a, (pg1) null);
        ug1 d = a2.d();
        String string = jobParameters.getExtras().getString("action");
        a2.b();
        d.A().a("Local AppMeasurementJobService called. action", string);
        if (!"com.google.android.gms.measurement.UPLOAD".equals(string)) {
            return true;
        }
        a((Runnable) new rk1(this, d, jobParameters));
        return true;
    }

    @DexIgnore
    public final /* synthetic */ void a(ug1 ug1, JobParameters jobParameters) {
        ug1.A().a("AppMeasurementJobService processed last upload request.");
        ((tk1) this.a).a(jobParameters, false);
    }

    @DexIgnore
    public final /* synthetic */ void a(int i, ug1 ug1, Intent intent) {
        if (((tk1) this.a).a(i)) {
            ug1.A().a("Local AppMeasurementService processed last upload request. StartId", Integer.valueOf(i));
            c().A().a("Completed wakeful intent.");
            ((tk1) this.a).a(intent);
        }
    }
}
