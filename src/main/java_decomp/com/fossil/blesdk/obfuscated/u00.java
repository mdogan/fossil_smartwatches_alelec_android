package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.DeviceType;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u00 {
    @DexIgnore
    public static final JSONArray a(DeviceType[] deviceTypeArr) {
        wd4.b(deviceTypeArr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (DeviceType logName$blesdk_productionRelease : deviceTypeArr) {
            jSONArray.put(logName$blesdk_productionRelease.getLogName$blesdk_productionRelease());
        }
        return jSONArray;
    }
}
