package com.fossil.blesdk.obfuscated;

import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gn4 {
    @DexIgnore
    public /* final */ Set<rm4> a; // = new LinkedHashSet();

    @DexIgnore
    public synchronized void a(rm4 rm4) {
        this.a.remove(rm4);
    }

    @DexIgnore
    public synchronized void b(rm4 rm4) {
        this.a.add(rm4);
    }

    @DexIgnore
    public synchronized boolean c(rm4 rm4) {
        return this.a.contains(rm4);
    }
}
