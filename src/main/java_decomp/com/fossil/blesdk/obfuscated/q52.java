package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q52 implements Factory<rl2> {
    @DexIgnore
    public /* final */ o42 a;

    @DexIgnore
    public q52(o42 o42) {
        this.a = o42;
    }

    @DexIgnore
    public static q52 a(o42 o42) {
        return new q52(o42);
    }

    @DexIgnore
    public static rl2 b(o42 o42) {
        return c(o42);
    }

    @DexIgnore
    public static rl2 c(o42 o42) {
        rl2 o = o42.o();
        o44.a(o, "Cannot return null from a non-@Nullable @Provides method");
        return o;
    }

    @DexIgnore
    public rl2 get() {
        return b(this.a);
    }
}
