package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Looper;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hx {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ f74 b;

    @DexIgnore
    public hx(Context context, f74 f74) {
        this.a = context;
        this.b = f74;
    }

    @DexIgnore
    public zx a() throws IOException {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            return new zx(this.a, new ey(), new y54(), new v64(this.a, this.b.a(), "session_analytics.tap", "session_analytics_to_send"));
        }
        throw new IllegalStateException("AnswersFilesManagerProvider cannot be called on the main thread");
    }
}
