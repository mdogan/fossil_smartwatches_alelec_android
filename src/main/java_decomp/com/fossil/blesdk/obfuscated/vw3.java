package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.facebook.appevents.AppEventsConstants;
import com.fossil.blesdk.obfuscated.dv3;
import com.fossil.blesdk.obfuscated.fv3;
import com.fossil.blesdk.obfuscated.iv3;
import com.fossil.blesdk.obfuscated.kv3;
import com.fossil.blesdk.obfuscated.qw3;
import com.misfit.frameworks.common.enums.Action;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.http.RequestException;
import com.squareup.okhttp.internal.http.RouteException;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.CookieHandler;
import java.net.ProtocolException;
import java.net.Proxy;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vw3 {
    @DexIgnore
    public static /* final */ lv3 u; // = new a();
    @DexIgnore
    public /* final */ hv3 a;
    @DexIgnore
    public xu3 b;
    @DexIgnore
    public qu3 c;
    @DexIgnore
    public cx3 d;
    @DexIgnore
    public mv3 e;
    @DexIgnore
    public /* final */ kv3 f;
    @DexIgnore
    public ex3 g;
    @DexIgnore
    public long h; // = -1;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ iv3 k;
    @DexIgnore
    public iv3 l;
    @DexIgnore
    public kv3 m;
    @DexIgnore
    public kv3 n;
    @DexIgnore
    public jp4 o;
    @DexIgnore
    public wo4 p;
    @DexIgnore
    public /* final */ boolean q;
    @DexIgnore
    public /* final */ boolean r;
    @DexIgnore
    public pw3 s;
    @DexIgnore
    public qw3 t;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends lv3 {
        @DexIgnore
        public gv3 A() {
            return null;
        }

        @DexIgnore
        public xo4 B() {
            return new vo4();
        }

        @DexIgnore
        public long z() {
            return 0;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements fv3.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public int b;

        @DexIgnore
        public c(int i, iv3 iv3) {
            this.a = i;
        }

        @DexIgnore
        public xu3 a() {
            return vw3.this.b;
        }

        @DexIgnore
        public kv3 a(iv3 iv3) throws IOException {
            this.b++;
            if (this.a > 0) {
                fv3 fv3 = vw3.this.a.F().get(this.a - 1);
                qu3 a2 = a().e().a();
                if (!iv3.d().f().equals(a2.j()) || iv3.d().h() != a2.k()) {
                    throw new IllegalStateException("network interceptor " + fv3 + " must retain the same host and port");
                } else if (this.b > 1) {
                    throw new IllegalStateException("network interceptor " + fv3 + " must call proceed() exactly once");
                }
            }
            if (this.a < vw3.this.a.F().size()) {
                c cVar = new c(this.a + 1, iv3);
                fv3 fv32 = vw3.this.a.F().get(this.a);
                kv3 a3 = fv32.a(cVar);
                if (cVar.b == 1) {
                    return a3;
                }
                throw new IllegalStateException("network interceptor " + fv32 + " must call proceed() exactly once");
            }
            vw3.this.g.a(iv3);
            iv3 unused = vw3.this.l = iv3;
            if (vw3.this.j() && iv3.a() != null) {
                wo4 a4 = ep4.a(vw3.this.g.a(iv3, iv3.a().contentLength()));
                iv3.a().writeTo(a4);
                a4.close();
            }
            kv3 c2 = vw3.this.k();
            int e = c2.e();
            if ((e != 204 && e != 205) || c2.a().z() <= 0) {
                return c2;
            }
            throw new ProtocolException("HTTP " + e + " had non-zero Content-Length: " + c2.a().z());
        }
    }

    @DexIgnore
    public vw3(hv3 hv3, iv3 iv3, boolean z, boolean z2, boolean z3, xu3 xu3, cx3 cx3, bx3 bx3, kv3 kv3) {
        this.a = hv3;
        this.k = iv3;
        this.j = z;
        this.q = z2;
        this.r = z3;
        this.b = xu3;
        this.d = cx3;
        this.o = bx3;
        this.f = kv3;
        if (xu3 != null) {
            qv3.b.b(xu3, this);
            this.e = xu3.e();
            return;
        }
        this.e = null;
    }

    @DexIgnore
    public iv3 d() throws IOException {
        Proxy proxy;
        if (this.n != null) {
            if (h() != null) {
                proxy = h().b();
            } else {
                proxy = this.a.w();
            }
            int e2 = this.n.e();
            if (e2 != 307 && e2 != 308) {
                if (e2 != 401) {
                    if (e2 != 407) {
                        switch (e2) {
                            case 300:
                            case Action.Presenter.NEXT /*301*/:
                            case Action.Presenter.PREVIOUS /*302*/:
                            case Action.Presenter.BLACKOUT /*303*/:
                                break;
                            default:
                                return null;
                        }
                    } else if (proxy.type() != Proxy.Type.HTTP) {
                        throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
                    }
                }
                return yw3.a(this.a.b(), this.n, proxy);
            } else if (!this.k.f().equals("GET") && !this.k.f().equals("HEAD")) {
                return null;
            }
            if (!this.a.j()) {
                return null;
            }
            String a2 = this.n.a("Location");
            if (a2 == null) {
                return null;
            }
            ev3 a3 = this.k.d().a(a2);
            if (a3 == null) {
                return null;
            }
            if (!a3.j().equals(this.k.d().j()) && !this.a.k()) {
                return null;
            }
            iv3.b g2 = this.k.g();
            if (ww3.b(this.k.f())) {
                g2.a("GET", (jv3) null);
                g2.a("Transfer-Encoding");
                g2.a("Content-Length");
                g2.a(GraphRequest.CONTENT_TYPE_HEADER);
            }
            if (!a(a3)) {
                g2.a("Authorization");
            }
            g2.a(a3);
            return g2.a();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public xu3 e() {
        return this.b;
    }

    @DexIgnore
    public iv3 f() {
        return this.k;
    }

    @DexIgnore
    public kv3 g() {
        kv3 kv3 = this.n;
        if (kv3 != null) {
            return kv3;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public mv3 h() {
        return this.e;
    }

    @DexIgnore
    public final void i() throws IOException {
        rv3 a2 = qv3.b.a(this.a);
        if (a2 != null) {
            if (qw3.a(this.n, this.l)) {
                this.s = a2.a(c(this.n));
            } else if (ww3.a(this.l.f())) {
                try {
                    a2.b(this.l);
                } catch (IOException unused) {
                }
            }
        }
    }

    @DexIgnore
    public boolean j() {
        return ww3.b(this.k.f());
    }

    @DexIgnore
    public final kv3 k() throws IOException {
        this.g.a();
        kv3.b c2 = this.g.c();
        c2.a(this.l);
        c2.a(this.b.b());
        c2.b(yw3.c, Long.toString(this.h));
        c2.b(yw3.d, Long.toString(System.currentTimeMillis()));
        kv3 a2 = c2.a();
        if (!this.r) {
            kv3.b j2 = a2.j();
            j2.a(this.g.a(a2));
            a2 = j2.a();
        }
        qv3.b.a(this.b, a2.k());
        return a2;
    }

    @DexIgnore
    public void l() throws IOException {
        kv3 kv3;
        if (this.n == null) {
            if (this.l == null && this.m == null) {
                throw new IllegalStateException("call sendRequest() first!");
            }
            iv3 iv3 = this.l;
            if (iv3 != null) {
                if (this.r) {
                    this.g.a(iv3);
                    kv3 = k();
                } else if (!this.q) {
                    kv3 = new c(0, iv3).a(this.l);
                } else {
                    wo4 wo4 = this.p;
                    if (wo4 != null && wo4.a().B() > 0) {
                        this.p.c();
                    }
                    if (this.h == -1) {
                        if (yw3.a(this.l) == -1) {
                            jp4 jp4 = this.o;
                            if (jp4 instanceof bx3) {
                                long f2 = ((bx3) jp4).f();
                                iv3.b g2 = this.l.g();
                                g2.b("Content-Length", Long.toString(f2));
                                this.l = g2.a();
                            }
                        }
                        this.g.a(this.l);
                    }
                    jp4 jp42 = this.o;
                    if (jp42 != null) {
                        wo4 wo42 = this.p;
                        if (wo42 != null) {
                            wo42.close();
                        } else {
                            jp42.close();
                        }
                        jp4 jp43 = this.o;
                        if (jp43 instanceof bx3) {
                            this.g.a((bx3) jp43);
                        }
                    }
                    kv3 = k();
                }
                a(kv3.g());
                kv3 kv32 = this.m;
                if (kv32 != null) {
                    if (a(kv32, kv3)) {
                        kv3.b j2 = this.m.j();
                        j2.a(this.k);
                        j2.d(c(this.f));
                        j2.a(a(this.m.g(), kv3.g()));
                        j2.a(c(this.m));
                        j2.c(c(kv3));
                        this.n = j2.a();
                        kv3.a().close();
                        m();
                        rv3 a2 = qv3.b.a(this.a);
                        a2.a();
                        a2.a(this.m, c(this.n));
                        this.n = a(this.n);
                        return;
                    }
                    xv3.a((Closeable) this.m.a());
                }
                kv3.b j3 = kv3.j();
                j3.a(this.k);
                j3.d(c(this.f));
                j3.a(c(this.m));
                j3.c(c(kv3));
                this.n = j3.a();
                if (b(this.n)) {
                    i();
                    this.n = a(a(this.s, this.n));
                }
            }
        }
    }

    @DexIgnore
    public void m() throws IOException {
        ex3 ex3 = this.g;
        if (!(ex3 == null || this.b == null)) {
            ex3.b();
        }
        this.b = null;
    }

    @DexIgnore
    public void n() throws RequestException, RouteException, IOException {
        if (this.t == null) {
            if (this.g == null) {
                iv3 a2 = a(this.k);
                rv3 a3 = qv3.b.a(this.a);
                kv3 a4 = a3 != null ? a3.a(a2) : null;
                this.t = new qw3.b(System.currentTimeMillis(), a2, a4).c();
                qw3 qw3 = this.t;
                this.l = qw3.a;
                this.m = qw3.b;
                if (a3 != null) {
                    a3.a(qw3);
                }
                if (a4 != null && this.m == null) {
                    xv3.a((Closeable) a4.a());
                }
                if (this.l != null) {
                    if (this.b == null) {
                        b();
                    }
                    this.g = qv3.b.a(this.b, this);
                    if (this.q && j() && this.o == null) {
                        long a5 = yw3.a(a2);
                        if (!this.j) {
                            this.g.a(this.l);
                            this.o = this.g.a(this.l, a5);
                        } else if (a5 > 2147483647L) {
                            throw new IllegalStateException("Use setFixedLengthStreamingMode() or setChunkedStreamingMode() for requests larger than 2 GiB.");
                        } else if (a5 != -1) {
                            this.g.a(this.l);
                            this.o = new bx3((int) a5);
                        } else {
                            this.o = new bx3();
                        }
                    }
                } else {
                    if (this.b != null) {
                        qv3.b.a(this.a.e(), this.b);
                        this.b = null;
                    }
                    kv3 kv3 = this.m;
                    if (kv3 != null) {
                        kv3.b j2 = kv3.j();
                        j2.a(this.k);
                        j2.d(c(this.f));
                        j2.a(c(this.m));
                        this.n = j2.a();
                    } else {
                        kv3.b bVar = new kv3.b();
                        bVar.a(this.k);
                        bVar.d(c(this.f));
                        bVar.a(Protocol.HTTP_1_1);
                        bVar.a(504);
                        bVar.a("Unsatisfiable Request (only-if-cached)");
                        bVar.a(u);
                        this.n = bVar.a();
                    }
                    this.n = a(this.n);
                }
            } else {
                throw new IllegalStateException();
            }
        }
    }

    @DexIgnore
    public void o() {
        if (this.h == -1) {
            this.h = System.currentTimeMillis();
            return;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public static kv3 c(kv3 kv3) {
        if (kv3 == null || kv3.a() == null) {
            return kv3;
        }
        kv3.b j2 = kv3.j();
        j2.a((lv3) null);
        return j2.a();
    }

    @DexIgnore
    public final void b() throws RequestException, RouteException {
        if (this.b == null) {
            if (this.d == null) {
                this.c = a(this.a, this.l);
                try {
                    this.d = cx3.a(this.c, this.l, this.a);
                } catch (IOException e2) {
                    throw new RequestException(e2);
                }
            }
            this.b = c();
            qv3.b.a(this.a, this.b, this, this.l);
            this.e = this.b.e();
            return;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public final boolean a(RouteException routeException) {
        if (!this.a.z()) {
            return false;
        }
        IOException lastConnectException = routeException.getLastConnectException();
        if ((lastConnectException instanceof ProtocolException) || (lastConnectException instanceof InterruptedIOException)) {
            return false;
        }
        if ((!(lastConnectException instanceof SSLHandshakeException) || !(lastConnectException.getCause() instanceof CertificateException)) && !(lastConnectException instanceof SSLPeerUnverifiedException)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final xu3 c() throws RouteException {
        xu3 a2;
        yu3 e2 = this.a.e();
        while (true) {
            a2 = e2.a(this.c);
            if (a2 == null) {
                try {
                    return new xu3(e2, this.d.e());
                } catch (IOException e3) {
                    throw new RouteException(e3);
                }
            } else if (this.l.f().equals("GET") || qv3.b.b(a2)) {
                return a2;
            } else {
                xv3.a(a2.f());
            }
        }
        return a2;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements kp4 {
        @DexIgnore
        public boolean e;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 f;
        @DexIgnore
        public /* final */ /* synthetic */ pw3 g;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 h;

        @DexIgnore
        public b(vw3 vw3, xo4 xo4, pw3 pw3, wo4 wo4) {
            this.f = xo4;
            this.g = pw3;
            this.h = wo4;
        }

        @DexIgnore
        public long b(vo4 vo4, long j) throws IOException {
            try {
                long b = this.f.b(vo4, j);
                if (b == -1) {
                    if (!this.e) {
                        this.e = true;
                        this.h.close();
                    }
                    return -1;
                }
                vo4.a(this.h.a(), vo4.B() - b, b);
                this.h.d();
                return b;
            } catch (IOException e2) {
                if (!this.e) {
                    this.e = true;
                    this.g.abort();
                }
                throw e2;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.e && !xv3.a((kp4) this, 100, TimeUnit.MILLISECONDS)) {
                this.e = true;
                this.g.abort();
            }
            this.f.close();
        }

        @DexIgnore
        public lp4 b() {
            return this.f.b();
        }
    }

    @DexIgnore
    public vw3 a(IOException iOException, jp4 jp4) {
        cx3 cx3 = this.d;
        if (!(cx3 == null || this.b == null)) {
            a(cx3, iOException);
        }
        boolean z = jp4 == null || (jp4 instanceof bx3);
        if (this.d == null && this.b == null) {
            return null;
        }
        cx3 cx32 = this.d;
        if ((cx32 != null && !cx32.a()) || !a(iOException) || !z) {
            return null;
        }
        return new vw3(this.a, this.k, this.j, this.q, this.r, a(), this.d, (bx3) jp4, this.f);
    }

    @DexIgnore
    public vw3 b(RouteException routeException) {
        cx3 cx3 = this.d;
        if (!(cx3 == null || this.b == null)) {
            a(cx3, routeException.getLastConnectException());
        }
        if (this.d == null && this.b == null) {
            return null;
        }
        cx3 cx32 = this.d;
        if ((cx32 != null && !cx32.a()) || !a(routeException)) {
            return null;
        }
        return new vw3(this.a, this.k, this.j, this.q, this.r, a(), this.d, (bx3) this.o, this.f);
    }

    @DexIgnore
    public static boolean b(kv3 kv3) {
        if (kv3.l().f().equals("HEAD")) {
            return false;
        }
        int e2 = kv3.e();
        if (((e2 >= 100 && e2 < 200) || e2 == 204 || e2 == 304) && yw3.a(kv3) == -1 && !"chunked".equalsIgnoreCase(kv3.a("Transfer-Encoding"))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void a(cx3 cx3, IOException iOException) {
        if (qv3.b.c(this.b) <= 0) {
            cx3.a(this.b.e(), iOException);
        }
    }

    @DexIgnore
    public final boolean a(IOException iOException) {
        if (this.a.z() && !(iOException instanceof ProtocolException) && !(iOException instanceof InterruptedIOException)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public xu3 a() {
        wo4 wo4 = this.p;
        if (wo4 != null) {
            xv3.a((Closeable) wo4);
        } else {
            jp4 jp4 = this.o;
            if (jp4 != null) {
                xv3.a((Closeable) jp4);
            }
        }
        kv3 kv3 = this.n;
        if (kv3 == null) {
            xu3 xu3 = this.b;
            if (xu3 != null) {
                xv3.a(xu3.f());
            }
            this.b = null;
            return null;
        }
        xv3.a((Closeable) kv3.a());
        ex3 ex3 = this.g;
        if (ex3 == null || this.b == null || ex3.d()) {
            xu3 xu32 = this.b;
            if (xu32 != null && !qv3.b.a(xu32)) {
                this.b = null;
            }
            xu3 xu33 = this.b;
            this.b = null;
            return xu33;
        }
        xv3.a(this.b.f());
        this.b = null;
        return null;
    }

    @DexIgnore
    public final kv3 a(kv3 kv3) throws IOException {
        if (!this.i || !"gzip".equalsIgnoreCase(this.n.a(GraphRequest.CONTENT_ENCODING_HEADER)) || kv3.a() == null) {
            return kv3;
        }
        cp4 cp4 = new cp4(kv3.a().B());
        dv3.b a2 = kv3.g().a();
        a2.b(GraphRequest.CONTENT_ENCODING_HEADER);
        a2.b("Content-Length");
        dv3 a3 = a2.a();
        kv3.b j2 = kv3.j();
        j2.a(a3);
        j2.a((lv3) new zw3(a3, ep4.a((kp4) cp4)));
        return j2.a();
    }

    @DexIgnore
    public final iv3 a(iv3 iv3) throws IOException {
        iv3.b g2 = iv3.g();
        if (iv3.a("Host") == null) {
            g2.b("Host", xv3.a(iv3.d()));
        }
        xu3 xu3 = this.b;
        if ((xu3 == null || xu3.d() != Protocol.HTTP_1_0) && iv3.a("Connection") == null) {
            g2.b("Connection", "Keep-Alive");
        }
        if (iv3.a("Accept-Encoding") == null) {
            this.i = true;
            g2.b("Accept-Encoding", "gzip");
        }
        CookieHandler g3 = this.a.g();
        if (g3 != null) {
            yw3.a(g2, g3.get(iv3.h(), yw3.b(g2.a().c(), (String) null)));
        }
        if (iv3.a("User-Agent") == null) {
            g2.b("User-Agent", yv3.a());
        }
        return g2.a();
    }

    @DexIgnore
    public final kv3 a(pw3 pw3, kv3 kv3) throws IOException {
        if (pw3 == null) {
            return kv3;
        }
        jp4 a2 = pw3.a();
        if (a2 == null) {
            return kv3;
        }
        b bVar = new b(this, kv3.a().B(), pw3, ep4.a(a2));
        kv3.b j2 = kv3.j();
        j2.a((lv3) new zw3(kv3.g(), ep4.a((kp4) bVar)));
        return j2.a();
    }

    @DexIgnore
    public static boolean a(kv3 kv3, kv3 kv32) {
        if (kv32.e() == 304) {
            return true;
        }
        Date b2 = kv3.g().b("Last-Modified");
        if (b2 == null) {
            return false;
        }
        Date b3 = kv32.g().b("Last-Modified");
        if (b3 == null || b3.getTime() >= b2.getTime()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static dv3 a(dv3 dv3, dv3 dv32) throws IOException {
        dv3.b bVar = new dv3.b();
        int b2 = dv3.b();
        for (int i2 = 0; i2 < b2; i2++) {
            String a2 = dv3.a(i2);
            String b3 = dv3.b(i2);
            if ((!"Warning".equalsIgnoreCase(a2) || !b3.startsWith(AppEventsConstants.EVENT_PARAM_VALUE_YES)) && (!yw3.a(a2) || dv32.a(a2) == null)) {
                bVar.a(a2, b3);
            }
        }
        int b4 = dv32.b();
        for (int i3 = 0; i3 < b4; i3++) {
            String a3 = dv32.a(i3);
            if (!"Content-Length".equalsIgnoreCase(a3) && yw3.a(a3)) {
                bVar.a(a3, dv32.b(i3));
            }
        }
        return bVar.a();
    }

    @DexIgnore
    public void a(dv3 dv3) throws IOException {
        CookieHandler g2 = this.a.g();
        if (g2 != null) {
            g2.put(this.k.h(), yw3.b(dv3, (String) null));
        }
    }

    @DexIgnore
    public boolean a(ev3 ev3) {
        ev3 d2 = this.k.d();
        return d2.f().equals(ev3.f()) && d2.h() == ev3.h() && d2.j().equals(ev3.j());
    }

    @DexIgnore
    public static qu3 a(hv3 hv3, iv3 iv3) {
        vu3 vu3;
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        if (iv3.e()) {
            SSLSocketFactory B = hv3.B();
            hostnameVerifier = hv3.l();
            sSLSocketFactory = B;
            vu3 = hv3.c();
        } else {
            sSLSocketFactory = null;
            hostnameVerifier = null;
            vu3 = null;
        }
        return new qu3(iv3.d().f(), iv3.d().h(), hv3.A(), sSLSocketFactory, hostnameVerifier, vu3, hv3.b(), hv3.w(), hv3.m(), hv3.f(), hv3.x());
    }
}
