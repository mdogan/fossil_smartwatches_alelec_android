package com.fossil.blesdk.obfuscated;

import bolts.ExecutorException;
import bolts.UnobservedTaskException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jm<TResult> {
    @DexIgnore
    public static /* final */ Executor i; // = gm.b();
    @DexIgnore
    public static volatile g j;
    @DexIgnore
    public static jm<?> k; // = new jm<>((Object) null);
    @DexIgnore
    public static jm<Boolean> l; // = new jm<>(true);
    @DexIgnore
    public static jm<Boolean> m; // = new jm<>(false);
    @DexIgnore
    public static jm<?> n; // = new jm<>(true);
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public TResult d;
    @DexIgnore
    public Exception e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public lm g;
    @DexIgnore
    public List<im<TResult, Void>> h; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements im<TResult, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ km a;
        @DexIgnore
        public /* final */ /* synthetic */ im b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;
        @DexIgnore
        public /* final */ /* synthetic */ hm d;

        @DexIgnore
        public a(jm jmVar, km kmVar, im imVar, Executor executor, hm hmVar) {
            this.a = kmVar;
            this.b = imVar;
            this.c = executor;
            this.d = hmVar;
        }

        @DexIgnore
        public Void then(jm<TResult> jmVar) {
            jm.d(this.a, this.b, jmVar, this.c, this.d);
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements im<TResult, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ km a;
        @DexIgnore
        public /* final */ /* synthetic */ im b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;
        @DexIgnore
        public /* final */ /* synthetic */ hm d;

        @DexIgnore
        public b(jm jmVar, km kmVar, im imVar, Executor executor, hm hmVar) {
            this.a = kmVar;
            this.b = imVar;
            this.c = executor;
            this.d = hmVar;
        }

        @DexIgnore
        public Void then(jm<TResult> jmVar) {
            jm.c(this.a, this.b, jmVar, this.c, this.d);
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements im<TResult, jm<TContinuationResult>> {
        @DexIgnore
        public /* final */ /* synthetic */ hm a;
        @DexIgnore
        public /* final */ /* synthetic */ im b;

        @DexIgnore
        public c(jm jmVar, hm hmVar, im imVar) {
            this.a = hmVar;
            this.b = imVar;
        }

        @DexIgnore
        public jm<TContinuationResult> then(jm<TResult> jmVar) {
            hm hmVar = this.a;
            if (hmVar != null) {
                hmVar.a();
                throw null;
            } else if (jmVar.e()) {
                return jm.b(jmVar.a());
            } else {
                if (jmVar.c()) {
                    return jm.h();
                }
                return jmVar.a((im<TResult, TContinuationResult>) this.b);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ hm e;
        @DexIgnore
        public /* final */ /* synthetic */ km f;
        @DexIgnore
        public /* final */ /* synthetic */ im g;
        @DexIgnore
        public /* final */ /* synthetic */ jm h;

        @DexIgnore
        public d(hm hmVar, km kmVar, im imVar, jm jmVar) {
            this.e = hmVar;
            this.f = kmVar;
            this.g = imVar;
            this.h = jmVar;
        }

        @DexIgnore
        public void run() {
            hm hmVar = this.e;
            if (hmVar == null) {
                try {
                    this.f.a(this.g.then(this.h));
                } catch (CancellationException unused) {
                    this.f.b();
                } catch (Exception e2) {
                    this.f.a(e2);
                }
            } else {
                hmVar.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ hm e;
        @DexIgnore
        public /* final */ /* synthetic */ km f;
        @DexIgnore
        public /* final */ /* synthetic */ im g;
        @DexIgnore
        public /* final */ /* synthetic */ jm h;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements im<TContinuationResult, Void> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public Void then(jm<TContinuationResult> jmVar) {
                hm hmVar = e.this.e;
                if (hmVar == null) {
                    if (jmVar.c()) {
                        e.this.f.b();
                    } else if (jmVar.e()) {
                        e.this.f.a(jmVar.a());
                    } else {
                        e.this.f.a(jmVar.b());
                    }
                    return null;
                }
                hmVar.a();
                throw null;
            }
        }

        @DexIgnore
        public e(hm hmVar, km kmVar, im imVar, jm jmVar) {
            this.e = hmVar;
            this.f = kmVar;
            this.g = imVar;
            this.h = jmVar;
        }

        @DexIgnore
        public void run() {
            hm hmVar = this.e;
            if (hmVar == null) {
                try {
                    jm jmVar = (jm) this.g.then(this.h);
                    if (jmVar == null) {
                        this.f.a(null);
                    } else {
                        jmVar.a(new a());
                    }
                } catch (CancellationException unused) {
                    this.f.b();
                } catch (Exception e2) {
                    this.f.a(e2);
                }
            } else {
                hmVar.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends km<TResult> {
        @DexIgnore
        public f(jm jmVar) {
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(jm<?> jmVar, UnobservedTaskException unobservedTaskException);
    }

    /*
    static {
        gm.a();
        cm.b();
    }
    */

    @DexIgnore
    public jm() {
    }

    @DexIgnore
    public static <TResult> jm<TResult> h() {
        return n;
    }

    @DexIgnore
    public static <TResult> jm<TResult>.f i() {
        return new f(new jm());
    }

    @DexIgnore
    public static g j() {
        return j;
    }

    @DexIgnore
    public boolean c() {
        boolean z;
        synchronized (this.a) {
            z = this.c;
        }
        return z;
    }

    @DexIgnore
    public boolean d() {
        boolean z;
        synchronized (this.a) {
            z = this.b;
        }
        return z;
    }

    @DexIgnore
    public boolean e() {
        boolean z;
        synchronized (this.a) {
            z = a() != null;
        }
        return z;
    }

    @DexIgnore
    public final void f() {
        synchronized (this.a) {
            for (im then : this.h) {
                try {
                    then.then(this);
                } catch (RuntimeException e2) {
                    throw e2;
                } catch (Exception e3) {
                    throw new RuntimeException(e3);
                }
            }
            this.h = null;
        }
    }

    @DexIgnore
    public boolean g() {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.c = true;
            this.a.notifyAll();
            f();
            return true;
        }
    }

    @DexIgnore
    public Exception a() {
        Exception exc;
        synchronized (this.a) {
            if (this.e != null) {
                this.f = true;
                if (this.g != null) {
                    this.g.a();
                    this.g = null;
                }
            }
            exc = this.e;
        }
        return exc;
    }

    @DexIgnore
    public TResult b() {
        TResult tresult;
        synchronized (this.a) {
            tresult = this.d;
        }
        return tresult;
    }

    @DexIgnore
    public jm(TResult tresult) {
        a(tresult);
    }

    @DexIgnore
    public static <TContinuationResult, TResult> void d(km<TContinuationResult> kmVar, im<TResult, TContinuationResult> imVar, jm<TResult> jmVar, Executor executor, hm hmVar) {
        try {
            executor.execute(new d(hmVar, kmVar, imVar, jmVar));
        } catch (Exception e2) {
            kmVar.a((Exception) new ExecutorException(e2));
        }
    }

    @DexIgnore
    public <TContinuationResult> jm<TContinuationResult> c(im<TResult, TContinuationResult> imVar, Executor executor, hm hmVar) {
        return a(new c(this, hmVar, imVar), executor);
    }

    @DexIgnore
    public static <TResult> jm<TResult> b(TResult tresult) {
        if (tresult == null) {
            return k;
        }
        if (tresult instanceof Boolean) {
            return ((Boolean) tresult).booleanValue() ? l : m;
        }
        km kmVar = new km();
        kmVar.a(tresult);
        return kmVar.a();
    }

    @DexIgnore
    public static <TContinuationResult, TResult> void c(km<TContinuationResult> kmVar, im<TResult, jm<TContinuationResult>> imVar, jm<TResult> jmVar, Executor executor, hm hmVar) {
        try {
            executor.execute(new e(hmVar, kmVar, imVar, jmVar));
        } catch (Exception e2) {
            kmVar.a((Exception) new ExecutorException(e2));
        }
    }

    @DexIgnore
    public jm(boolean z) {
        if (z) {
            g();
        } else {
            a((Object) null);
        }
    }

    @DexIgnore
    public <TContinuationResult> jm<TContinuationResult> a(im<TResult, TContinuationResult> imVar, Executor executor, hm hmVar) {
        boolean d2;
        km kmVar = new km();
        synchronized (this.a) {
            d2 = d();
            if (!d2) {
                this.h.add(new a(this, kmVar, imVar, executor, hmVar));
            }
        }
        if (d2) {
            d(kmVar, imVar, this, executor, hmVar);
        }
        return kmVar.a();
    }

    @DexIgnore
    public static <TResult> jm<TResult> b(Exception exc) {
        km kmVar = new km();
        kmVar.a(exc);
        return kmVar.a();
    }

    @DexIgnore
    public <TContinuationResult> jm<TContinuationResult> b(im<TResult, jm<TContinuationResult>> imVar, Executor executor, hm hmVar) {
        boolean d2;
        km kmVar = new km();
        synchronized (this.a) {
            d2 = d();
            if (!d2) {
                this.h.add(new b(this, kmVar, imVar, executor, hmVar));
            }
        }
        if (d2) {
            c(kmVar, imVar, this, executor, hmVar);
        }
        return kmVar.a();
    }

    @DexIgnore
    public <TContinuationResult> jm<TContinuationResult> a(im<TResult, TContinuationResult> imVar) {
        return a(imVar, i, (hm) null);
    }

    @DexIgnore
    public <TContinuationResult> jm<TContinuationResult> a(im<TResult, jm<TContinuationResult>> imVar, Executor executor) {
        return b(imVar, executor, (hm) null);
    }

    @DexIgnore
    public boolean a(TResult tresult) {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.d = tresult;
            this.a.notifyAll();
            f();
            return true;
        }
    }

    @DexIgnore
    public <TContinuationResult> jm<TContinuationResult> b(im<TResult, TContinuationResult> imVar) {
        return c(imVar, i, (hm) null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        return true;
     */
    @DexIgnore
    public boolean a(Exception exc) {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.e = exc;
            this.f = false;
            this.a.notifyAll();
            f();
            if (!this.f && j() != null) {
                this.g = new lm(this);
            }
        }
    }
}
