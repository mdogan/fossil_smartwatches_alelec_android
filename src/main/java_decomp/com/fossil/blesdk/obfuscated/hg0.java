package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.he0;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hg0 implements he0.b {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference e;
    @DexIgnore
    public /* final */ /* synthetic */ ff0 f;
    @DexIgnore
    public /* final */ /* synthetic */ fg0 g;

    @DexIgnore
    public hg0(fg0 fg0, AtomicReference atomicReference, ff0 ff0) {
        this.g = fg0;
        this.e = atomicReference;
        this.f = ff0;
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        this.g.a((he0) this.e.get(), this.f, true);
    }

    @DexIgnore
    public final void f(int i) {
    }
}
