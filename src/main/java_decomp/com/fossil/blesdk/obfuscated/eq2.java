package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.BaseActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eq2 implements MembersInjector<BaseActivity> {
    @DexIgnore
    public static void a(BaseActivity baseActivity, UserRepository userRepository) {
        baseActivity.p = userRepository;
    }

    @DexIgnore
    public static void a(BaseActivity baseActivity, fn2 fn2) {
        baseActivity.q = fn2;
    }

    @DexIgnore
    public static void a(BaseActivity baseActivity, DeviceRepository deviceRepository) {
        baseActivity.r = deviceRepository;
    }

    @DexIgnore
    public static void a(BaseActivity baseActivity, MigrationManager migrationManager) {
        baseActivity.s = migrationManager;
    }

    @DexIgnore
    public static void a(BaseActivity baseActivity, hr2 hr2) {
        baseActivity.t = hr2;
    }
}
