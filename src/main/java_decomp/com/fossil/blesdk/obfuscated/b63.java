package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b63 implements Factory<HomeHybridCustomizePresenter> {
    @DexIgnore
    public static HomeHybridCustomizePresenter a(PortfolioApp portfolioApp, a63 a63, MicroAppRepository microAppRepository, HybridPresetRepository hybridPresetRepository, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, fn2 fn2) {
        return new HomeHybridCustomizePresenter(portfolioApp, a63, microAppRepository, hybridPresetRepository, setHybridPresetToWatchUseCase, fn2);
    }
}
