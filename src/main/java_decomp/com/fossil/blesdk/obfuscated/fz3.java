package com.fossil.blesdk.obfuscated;

import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fz3 {
    /*
    static {
        TimeZone.getTimeZone("GMT");
        new String[]{"&lt;", "&gt;", "&quot;", "&apos;", "&amp;", "&#x0D;", "&#x0A;", "&#x20;", "&#x09;"};
    }
    */

    @DexIgnore
    public static boolean a(String str) {
        return str == null || str.length() <= 0;
    }
}
