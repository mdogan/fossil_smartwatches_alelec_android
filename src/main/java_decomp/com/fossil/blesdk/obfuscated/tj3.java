package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tj3 {
    @DexIgnore
    public /* final */ zj3 a;

    @DexIgnore
    public tj3(zj3 zj3) {
        wd4.b(zj3, "view");
        tt1.a(zj3, "view cannot be null!", new Object[0]);
        wd4.a((Object) zj3, "checkNotNull(view, \"view cannot be null!\")");
        this.a = zj3;
    }

    @DexIgnore
    public final zj3 a() {
        return this.a;
    }
}
