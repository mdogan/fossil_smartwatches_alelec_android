package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ms2;
import com.fossil.blesdk.obfuscated.r62;
import com.fossil.wearables.fossil.R;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p23 extends as2 implements z23, r62.b {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((rd4) null);
    @DexIgnore
    public ur3<fa2> j;
    @DexIgnore
    public y23 k;
    @DexIgnore
    public ms2 l;
    @DexIgnore
    public r62 m;
    @DexIgnore
    public String n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final p23 a() {
            return new p23();
        }

        @DexIgnore
        public final String b() {
            return p23.p;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ms2.b {
        @DexIgnore
        public /* final */ /* synthetic */ p23 a;

        @DexIgnore
        public b(p23 p23) {
            this.a = p23;
        }

        @DexIgnore
        public void a(String str) {
            wd4.b(str, "address");
            p23.c(this.a).a(StringsKt__StringsKt.d(str).toString());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ fa2 f;

        @DexIgnore
        public c(View view, fa2 fa2) {
            this.e = view;
            this.f = fa2;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.e.getWindowVisibleDisplayFrame(rect);
            int height = this.e.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.f.v.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                fa2 fa2 = this.f;
                wd4.a((Object) fa2, "binding");
                fa2.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = p23.q.b();
                local.d(b, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = this.f.q;
                wd4.a((Object) appCompatAutoCompleteTextView, "binding.autocompletePlaces");
                appCompatAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;

        @DexIgnore
        public d(p23 p23, fa2 fa2) {
            this.e = p23;
        }

        @DexIgnore
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            r62 a = this.e.m;
            if (a != null) {
                AutocompletePrediction item = a.getItem(i);
                if (item != null) {
                    SpannableString fullText = item.getFullText((CharacterStyle) null);
                    wd4.a((Object) fullText, "item.getFullText(null)");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = q23.q.b();
                    local.i(b, "Autocomplete item selected: " + fullText);
                    this.e.n = fullText.toString();
                    if (this.e.n != null) {
                        String b2 = this.e.n;
                        if (b2 != null) {
                            if (b2.length() > 0) {
                                y23 c = p23.c(this.e);
                                String b3 = this.e.n;
                                if (b3 == null) {
                                    wd4.a();
                                    throw null;
                                } else if (b3 != null) {
                                    c.a(StringsKt__StringsKt.d(b3).toString());
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                                }
                            }
                        } else {
                            wd4.a();
                            throw null;
                        }
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ fa2 e;

        @DexIgnore
        public e(p23 p23, fa2 fa2) {
            this.e = fa2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.e.r;
            wd4.a((Object) imageView, "binding.closeIv");
            imageView.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;

        @DexIgnore
        public f(p23 p23, fa2 fa2) {
            this.e = p23;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            wd4.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.e.S0();
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;

        @DexIgnore
        public g(p23 p23) {
            this.e = p23;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;
        @DexIgnore
        public /* final */ /* synthetic */ fa2 f;

        @DexIgnore
        public h(p23 p23, fa2 fa2) {
            this.e = p23;
            this.f = fa2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.n = "";
            this.f.q.setText("");
            this.e.T0();
        }
    }

    /*
    static {
        String simpleName = p23.class.getSimpleName();
        wd4.a((Object) simpleName, "CommuteTimeSettingsDefau\u2026nt::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ y23 c(p23 p23) {
        y23 y23 = p23.k;
        if (y23 != null) {
            return y23;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void B(String str) {
        if (str != null) {
            Intent intent = new Intent();
            intent.putExtra("KEY_DEFAULT_PLACE", str);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.setResult(-1, intent);
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void M(boolean z) {
        ur3<fa2> ur3 = this.j;
        if (ur3 != null) {
            fa2 a2 = ur3.a();
            if (a2 != null) {
                if (z) {
                    AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                    wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(appCompatAutoCompleteTextView.getText())) {
                        this.n = null;
                        U0();
                        return;
                    }
                }
                T0();
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        ur3<fa2> ur3 = this.j;
        String str = null;
        if (ur3 == null) {
            wd4.d("mBinding");
            throw null;
        } else if (ur3.a() == null) {
            return true;
        } else {
            y23 y23 = this.k;
            if (y23 != null) {
                String str2 = this.n;
                if (str2 != null) {
                    if (str2 != null) {
                        str = StringsKt__StringsKt.d(str2).toString();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
                y23.a(str);
                return true;
            }
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void T0() {
        ur3<fa2> ur3 = this.j;
        if (ur3 != null) {
            fa2 a2 = ur3.a();
            if (a2 != null) {
                LinearLayout linearLayout = a2.w;
                wd4.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(0);
                FlexibleTextView flexibleTextView = a2.s;
                wd4.a((Object) flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void U0() {
        ur3<fa2> ur3 = this.j;
        if (ur3 != null) {
            fa2 a2 = ur3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                wd4.a((Object) flexibleTextView, "it.ftvAddressError");
                PortfolioApp c2 = PortfolioApp.W.c();
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(c2.getString(R.string.Customization_Buttons_CommuteTimeDestinationSettings_Text__NothingFoundForInputaddress, new Object[]{appCompatAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.s;
                wd4.a((Object) flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                LinearLayout linearLayout = a2.w;
                wd4.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(8);
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void j(List<String> list) {
        wd4.b(list, "recentSearchedList");
        T0();
        if (!list.isEmpty()) {
            ms2 ms2 = this.l;
            if (ms2 != null) {
                ms2.a((List<String>) wb4.d(list));
            }
            ur3<fa2> ur3 = this.j;
            if (ur3 != null) {
                fa2 a2 = ur3.a();
                if (a2 != null) {
                    LinearLayout linearLayout = a2.w;
                    if (linearLayout != null) {
                        linearLayout.setVisibility(0);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
        ur3<fa2> ur32 = this.j;
        if (ur32 != null) {
            fa2 a3 = ur32.a();
            if (a3 != null) {
                LinearLayout linearLayout2 = a3.w;
                if (linearLayout2 != null) {
                    linearLayout2.setVisibility(4);
                    return;
                }
                return;
            }
            return;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        fa2 fa2 = (fa2) ra.a(layoutInflater, R.layout.fragment_commute_time_settings_default_address, viewGroup, false, O0());
        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = fa2.q;
        appCompatAutoCompleteTextView.setDropDownBackgroundDrawable(k6.c(appCompatAutoCompleteTextView.getContext(), R.drawable.autocomplete_dropdown));
        appCompatAutoCompleteTextView.setOnItemClickListener(new d(this, fa2));
        appCompatAutoCompleteTextView.addTextChangedListener(new e(this, fa2));
        appCompatAutoCompleteTextView.setOnKeyListener(new f(this, fa2));
        fa2.u.setOnClickListener(new g(this));
        fa2.r.setOnClickListener(new h(this, fa2));
        ms2 ms2 = new ms2();
        ms2.a((ms2.b) new b(this));
        this.l = ms2;
        RecyclerView recyclerView = fa2.x;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.l);
        wd4.a((Object) fa2, "binding");
        View d2 = fa2.d();
        wd4.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new c(d2, fa2));
        this.j = new ur3<>(this, fa2);
        return fa2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        y23 y23 = this.k;
        if (y23 != null) {
            y23.g();
            super.onPause();
            return;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        y23 y23 = this.k;
        if (y23 != null) {
            y23.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void setTitle(String str) {
        wd4.b(str, "title");
        ur3<fa2> ur3 = this.j;
        if (ur3 != null) {
            fa2 a2 = ur3.a();
            FlexibleTextView flexibleTextView = a2 != null ? a2.t : null;
            if (flexibleTextView != null) {
                wd4.a((Object) flexibleTextView, "mBinding.get()?.ftvTitle!!");
                flexibleTextView.setText(str);
                return;
            }
            wd4.a();
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void v(String str) {
        wd4.b(str, "address");
        if (isActive()) {
            ur3<fa2> ur3 = this.j;
            if (ur3 != null) {
                fa2 a2 = ur3.a();
                if (a2 != null) {
                    AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                    wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = appCompatAutoCompleteTextView.getText();
                    wd4.a((Object) text, "it.autocompletePlaces.text");
                    CharSequence d2 = StringsKt__StringsKt.d(text);
                    boolean z = true;
                    if (d2.length() > 0) {
                        a2.q.setText(d2);
                        return;
                    }
                    if (str.length() <= 0) {
                        z = false;
                    }
                    if (z) {
                        a2.q.setText(str);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(y23 y23) {
        wd4.b(y23, "presenter");
        this.k = y23;
    }

    @DexIgnore
    public void a(PlacesClient placesClient) {
        if (isActive()) {
            Context context = getContext();
            if (context != null) {
                wd4.a((Object) context, "context!!");
                this.m = new r62(context, placesClient);
                r62 r62 = this.m;
                if (r62 != null) {
                    r62.a((r62.b) this);
                }
                ur3<fa2> ur3 = this.j;
                if (ur3 != null) {
                    fa2 a2 = ur3.a();
                    if (a2 != null) {
                        a2.q.setAdapter(this.m);
                        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                        wd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                        Editable text = appCompatAutoCompleteTextView.getText();
                        wd4.a((Object) text, "it.autocompletePlaces.text");
                        CharSequence d2 = StringsKt__StringsKt.d(text);
                        if (d2.length() > 0) {
                            a2.q.setText(d2);
                            a2.q.setSelection(d2.length());
                            return;
                        }
                        T0();
                        return;
                    }
                    return;
                }
                wd4.d("mBinding");
                throw null;
            }
            wd4.a();
            throw null;
        }
    }
}
