package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileFormatException;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.utils.Crc32Calculator;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class u20<T> extends m20<T> {
    @DexIgnore
    public /* final */ Crc32Calculator.CrcType b;
    @DexIgnore
    public /* final */ FileType c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u20(FileType fileType, Version version) {
        super(version);
        wd4.b(fileType, "fileType");
        wd4.b(version, "baseVersion");
        this.c = fileType;
        this.b = Crc32Calculator.CrcType.CRC32;
    }

    @DexIgnore
    public T a(byte[] bArr) throws FileFormatException {
        wd4.b(bArr, "data");
        if (c(bArr)) {
            return b(bArr);
        }
        throw new FileFormatException(FileFormatException.FileFormatErrorCode.INVALID_FILE_DATA, "Invalid file.", (Throwable) null, 4, (rd4) null);
    }

    @DexIgnore
    public Crc32Calculator.CrcType b() {
        return this.b;
    }

    @DexIgnore
    public abstract T b(byte[] bArr) throws FileFormatException;

    @DexIgnore
    public boolean c(byte[] bArr) {
        wd4.b(bArr, "fileData");
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        if (this.c != FileType.Companion.a(order.getShort(0)) || new Version(bArr[2], bArr[3]).getMajor() != a().getMajor() || o90.b(order.getInt(8)) != o90.b((bArr.length - 12) - 4)) {
            return false;
        }
        if (o90.b(order.getInt(bArr.length - 4)) != Crc32Calculator.a.a(kb4.a(bArr, 12, bArr.length - 4), b())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public u20(FileType fileType) {
        this(fileType, new Version((byte) 1, (byte) 0));
        wd4.b(fileType, "fileType");
    }
}
