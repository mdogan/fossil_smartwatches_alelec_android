package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rx2 implements Factory<qx2> {
    @DexIgnore
    public static /* final */ rx2 a; // = new rx2();

    @DexIgnore
    public static rx2 a() {
        return a;
    }

    @DexIgnore
    public static qx2 b() {
        return new qx2();
    }

    @DexIgnore
    public qx2 get() {
        return b();
    }
}
