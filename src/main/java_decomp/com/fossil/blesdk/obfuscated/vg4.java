package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vg4 extends si4<xi4> implements ug4 {
    @DexIgnore
    public /* final */ wg4 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vg4(xi4 xi4, wg4 wg4) {
        super(xi4);
        wd4.b(xi4, "parent");
        wd4.b(wg4, "childJob");
        this.i = wg4;
    }

    @DexIgnore
    public boolean a(Throwable th) {
        wd4.b(th, "cause");
        return ((xi4) this.h).d(th);
    }

    @DexIgnore
    public void b(Throwable th) {
        this.i.a((fj4) this.h);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return cb4.a;
    }

    @DexIgnore
    public String toString() {
        return "ChildHandle[" + this.i + ']';
    }
}
