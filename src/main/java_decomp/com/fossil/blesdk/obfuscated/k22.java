package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k22 extends r22 {
    @DexIgnore
    public k22() {
        super(false, 1558, 620, 22, 22, 36, -1, 62);
    }

    @DexIgnore
    public int a(int i) {
        return i <= 8 ? 156 : 155;
    }

    @DexIgnore
    public int d() {
        return 10;
    }
}
