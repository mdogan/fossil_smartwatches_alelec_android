package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nj4 {
    @DexIgnore
    public static /* final */ ThreadLocal<ei4> a; // = new ThreadLocal<>();
    @DexIgnore
    public static /* final */ nj4 b; // = new nj4();

    @DexIgnore
    public final ei4 a() {
        return a.get();
    }

    @DexIgnore
    public final ei4 b() {
        ei4 ei4 = a.get();
        if (ei4 != null) {
            return ei4;
        }
        ei4 a2 = hi4.a();
        a.set(a2);
        return a2;
    }

    @DexIgnore
    public final void c() {
        a.set((Object) null);
    }

    @DexIgnore
    public final void a(ei4 ei4) {
        wd4.b(ei4, "eventLoop");
        a.set(ei4);
    }
}
