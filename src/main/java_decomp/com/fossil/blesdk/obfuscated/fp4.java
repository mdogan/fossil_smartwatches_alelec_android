package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fp4 implements wo4 {
    @DexIgnore
    public /* final */ vo4 e; // = new vo4();
    @DexIgnore
    public /* final */ jp4 f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public fp4(jp4 jp4) {
        if (jp4 != null) {
            this.f = jp4;
            return;
        }
        throw new NullPointerException("sink == null");
    }

    @DexIgnore
    public vo4 a() {
        return this.e;
    }

    @DexIgnore
    public wo4 b(long j) throws IOException {
        if (!this.g) {
            this.e.b(j);
            d();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public wo4 c() throws IOException {
        if (!this.g) {
            long B = this.e.B();
            if (B > 0) {
                this.f.a(this.e, B);
            }
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.g) {
            try {
                if (this.e.f > 0) {
                    this.f.a(this.e, this.e.f);
                }
                th = null;
            } catch (Throwable th) {
                th = th;
            }
            try {
                this.f.close();
            } catch (Throwable th2) {
                if (th == null) {
                    th = th2;
                }
            }
            this.g = true;
            if (th != null) {
                mp4.a(th);
                throw null;
            }
        }
    }

    @DexIgnore
    public wo4 d() throws IOException {
        if (!this.g) {
            long x = this.e.x();
            if (x > 0) {
                this.f.a(this.e, x);
            }
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public OutputStream e() {
        return new a();
    }

    @DexIgnore
    public void flush() throws IOException {
        if (!this.g) {
            vo4 vo4 = this.e;
            long j = vo4.f;
            if (j > 0) {
                this.f.a(vo4, j);
            }
            this.f.flush();
            return;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public boolean isOpen() {
        return !this.g;
    }

    @DexIgnore
    public String toString() {
        return "buffer(" + this.f + ")";
    }

    @DexIgnore
    public wo4 write(byte[] bArr) throws IOException {
        if (!this.g) {
            this.e.write(bArr);
            d();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public wo4 writeByte(int i) throws IOException {
        if (!this.g) {
            this.e.writeByte(i);
            return d();
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public wo4 writeInt(int i) throws IOException {
        if (!this.g) {
            this.e.writeInt(i);
            return d();
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public wo4 writeShort(int i) throws IOException {
        if (!this.g) {
            this.e.writeShort(i);
            d();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public void a(vo4 vo4, long j) throws IOException {
        if (!this.g) {
            this.e.a(vo4, j);
            d();
            return;
        }
        throw new IllegalStateException("closed");
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends OutputStream {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void close() throws IOException {
            fp4.this.close();
        }

        @DexIgnore
        public void flush() throws IOException {
            fp4 fp4 = fp4.this;
            if (!fp4.g) {
                fp4.flush();
            }
        }

        @DexIgnore
        public String toString() {
            return fp4.this + ".outputStream()";
        }

        @DexIgnore
        public void write(int i) throws IOException {
            fp4 fp4 = fp4.this;
            if (!fp4.g) {
                fp4.e.writeByte((int) (byte) i);
                fp4.this.d();
                return;
            }
            throw new IOException("closed");
        }

        @DexIgnore
        public void write(byte[] bArr, int i, int i2) throws IOException {
            fp4 fp4 = fp4.this;
            if (!fp4.g) {
                fp4.e.write(bArr, i, i2);
                fp4.this.d();
                return;
            }
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public lp4 b() {
        return this.f.b();
    }

    @DexIgnore
    public wo4 write(byte[] bArr, int i, int i2) throws IOException {
        if (!this.g) {
            this.e.write(bArr, i, i2);
            d();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public wo4 a(ByteString byteString) throws IOException {
        if (!this.g) {
            this.e.a(byteString);
            d();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public int write(ByteBuffer byteBuffer) throws IOException {
        if (!this.g) {
            int write = this.e.write(byteBuffer);
            d();
            return write;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public wo4 a(String str) throws IOException {
        if (!this.g) {
            this.e.a(str);
            return d();
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public long a(kp4 kp4) throws IOException {
        if (kp4 != null) {
            long j = 0;
            while (true) {
                long b = kp4.b(this.e, 8192);
                if (b == -1) {
                    return j;
                }
                j += b;
                d();
            }
        } else {
            throw new IllegalArgumentException("source == null");
        }
    }

    @DexIgnore
    public wo4 a(long j) throws IOException {
        if (!this.g) {
            this.e.a(j);
            return d();
        }
        throw new IllegalStateException("closed");
    }
}
