package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class xy1 implements Runnable {
    @DexIgnore
    public /* final */ wy1 e;
    @DexIgnore
    public /* final */ Bundle f;
    @DexIgnore
    public /* final */ yn1 g;

    @DexIgnore
    public xy1(wy1 wy1, Bundle bundle, yn1 yn1) {
        this.e = wy1;
        this.f = bundle;
        this.g = yn1;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f, this.g);
    }
}
