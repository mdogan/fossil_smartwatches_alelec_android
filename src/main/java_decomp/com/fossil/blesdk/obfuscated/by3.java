package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class by3 extends rx3<ImageView> {
    @DexIgnore
    public vx3 m;

    @DexIgnore
    public by3(Picasso picasso, ImageView imageView, hy3 hy3, int i, int i2, int i3, Drawable drawable, String str, Object obj, vx3 vx3, boolean z) {
        super(picasso, imageView, hy3, i, i2, i3, drawable, str, obj, z);
        this.m = vx3;
    }

    @DexIgnore
    public void a(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
        if (bitmap != null) {
            ImageView imageView = (ImageView) this.c.get();
            if (imageView != null) {
                Picasso picasso = this.a;
                Bitmap bitmap2 = bitmap;
                Picasso.LoadedFrom loadedFrom2 = loadedFrom;
                fy3.a(imageView, picasso.e, bitmap2, loadedFrom2, this.d, picasso.m);
                vx3 vx3 = this.m;
                if (vx3 != null) {
                    vx3.onSuccess();
                    return;
                }
                return;
            }
            return;
        }
        throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", new Object[]{this}));
    }

    @DexIgnore
    public void b() {
        ImageView imageView = (ImageView) this.c.get();
        if (imageView != null) {
            int i = this.g;
            if (i != 0) {
                imageView.setImageResource(i);
            } else {
                Drawable drawable = this.h;
                if (drawable != null) {
                    imageView.setImageDrawable(drawable);
                }
            }
            vx3 vx3 = this.m;
            if (vx3 != null) {
                vx3.onError();
            }
        }
    }

    @DexIgnore
    public void a() {
        super.a();
        if (this.m != null) {
            this.m = null;
        }
    }
}
