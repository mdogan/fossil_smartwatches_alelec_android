package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.fossil.blesdk.obfuscated.ee0;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.clearcut.zzge$zzv$zzb;
import java.util.ArrayList;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nd0 {
    @DexIgnore
    public static /* final */ ee0.g<ny0> m; // = new ee0.g<>();
    @DexIgnore
    public static /* final */ ee0.a<ny0, Object> n; // = new od0();
    @DexIgnore
    @Deprecated
    public static /* final */ ee0<Object> o; // = new ee0<>("ClearcutLogger.API", n, m);
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public String d;
    @DexIgnore
    public int e; // = -1;
    @DexIgnore
    public String f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public zzge$zzv$zzb h; // = zzge$zzv$zzb.zzbhk;
    @DexIgnore
    public /* final */ pd0 i;
    @DexIgnore
    public /* final */ im0 j;
    @DexIgnore
    public d k;
    @DexIgnore
    public /* final */ b l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a {
        @DexIgnore
        public int a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public zzge$zzv$zzb e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public /* final */ ky0 g;
        @DexIgnore
        public boolean h;

        @DexIgnore
        public a(nd0 nd0, byte[] bArr) {
            this(bArr, (c) null);
        }

        @DexIgnore
        public a(byte[] bArr, c cVar) {
            this.a = nd0.this.e;
            this.b = nd0.this.d;
            this.c = nd0.this.f;
            nd0 nd0 = nd0.this;
            this.d = null;
            this.e = nd0.h;
            this.f = true;
            this.g = new ky0();
            this.h = false;
            this.c = nd0.this.f;
            this.d = null;
            this.g.z = vs0.a(nd0.this.a);
            this.g.g = nd0.this.j.b();
            this.g.h = nd0.this.j.c();
            ky0 ky0 = this.g;
            d unused = nd0.this.k;
            ky0.t = (long) (TimeZone.getDefault().getOffset(this.g.g) / 1000);
            if (bArr != null) {
                this.g.o = bArr;
            }
        }

        @DexIgnore
        public /* synthetic */ a(nd0 nd0, byte[] bArr, od0 od0) {
            this(nd0, bArr);
        }

        @DexIgnore
        public void a() {
            if (!this.h) {
                this.h = true;
                sd0 sd0 = new sd0(new vy0(nd0.this.b, nd0.this.c, this.a, this.b, this.c, this.d, nd0.this.g, this.e), this.g, (c) null, (c) null, nd0.a((ArrayList<Integer>) null), (String[]) null, nd0.a((ArrayList<Integer>) null), (byte[][]) null, (im1[]) null, this.f);
                if (nd0.this.l.a(sd0)) {
                    nd0.this.i.a(sd0);
                } else {
                    je0.a(Status.i, (he0) null);
                }
            } else {
                throw new IllegalStateException("do not reuse LogEventBuilder");
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        boolean a(sd0 sd0);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        byte[] zza();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
    }

    @DexIgnore
    public nd0(Context context, int i2, String str, String str2, String str3, boolean z, pd0 pd0, im0 im0, d dVar, b bVar) {
        this.a = context;
        this.b = context.getPackageName();
        this.c = a(context);
        this.e = -1;
        this.d = str;
        this.f = str2;
        this.g = z;
        this.i = pd0;
        this.j = im0;
        this.k = new d();
        this.h = zzge$zzv$zzb.zzbhk;
        this.l = bVar;
        if (z) {
            ck0.a(str2 == null, (Object) "can't be anonymous with an upload account");
        }
    }

    @DexIgnore
    public static int a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.wtf("ClearcutLogger", "This can't happen.", e2);
            return 0;
        }
    }

    @DexIgnore
    public static nd0 a(Context context, String str) {
        return new nd0(context, -1, str, (String) null, (String) null, true, ew0.a(context), lm0.d(), (d) null, new ty0(context));
    }

    @DexIgnore
    public static int[] a(ArrayList<Integer> arrayList) {
        if (arrayList == null) {
            return null;
        }
        int[] iArr = new int[arrayList.size()];
        int size = arrayList.size();
        int i2 = 0;
        int i3 = 0;
        while (i2 < size) {
            Integer num = arrayList.get(i2);
            i2++;
            iArr[i3] = num.intValue();
            i3++;
        }
        return iArr;
    }

    @DexIgnore
    public final a a(byte[] bArr) {
        return new a(this, bArr, (od0) null);
    }
}
