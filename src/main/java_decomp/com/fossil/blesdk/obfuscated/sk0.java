package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sk0 extends nk0 {
    @DexIgnore
    public /* final */ ve0<Status> e;

    @DexIgnore
    public sk0(ve0<Status> ve0) {
        this.e = ve0;
    }

    @DexIgnore
    public final void e(int i) throws RemoteException {
        this.e.a(new Status(i));
    }
}
