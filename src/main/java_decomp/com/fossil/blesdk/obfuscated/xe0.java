package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@Deprecated
public final class xe0 {
    @DexIgnore
    public static /* final */ Object d; // = new Object();
    @DexIgnore
    public static xe0 e;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Status b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public xe0(Context context) {
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("google_app_measurement_enable", "integer", resources.getResourcePackageName(ce0.common_google_play_services_unknown_issue));
        boolean z = false;
        if (identifier != 0) {
            this.c = !(resources.getInteger(identifier) != 0 ? true : z);
        } else {
            this.c = false;
        }
        String a2 = bm0.a(context);
        a2 = a2 == null ? new ik0(context).a("google_app_id") : a2;
        if (TextUtils.isEmpty(a2)) {
            this.b = new Status(10, "Missing google app id value from from string resources with name google_app_id.");
            this.a = null;
            return;
        }
        this.a = a2;
        this.b = Status.i;
    }

    @DexIgnore
    public static Status a(Context context) {
        Status status;
        ck0.a(context, (Object) "Context must not be null.");
        synchronized (d) {
            if (e == null) {
                e = new xe0(context);
            }
            status = e.b;
        }
        return status;
    }

    @DexIgnore
    public static boolean b() {
        return a("isMeasurementExplicitlyDisabled").c;
    }

    @DexIgnore
    public static String a() {
        return a("getGoogleAppId").a;
    }

    @DexIgnore
    public static xe0 a(String str) {
        xe0 xe0;
        synchronized (d) {
            if (e != null) {
                xe0 = e;
            } else {
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 34);
                sb.append("Initialize must be called before ");
                sb.append(str);
                sb.append(CodelessMatcher.CURRENT_CLASS_NAME);
                throw new IllegalStateException(sb.toString());
            }
        }
        return xe0;
    }
}
