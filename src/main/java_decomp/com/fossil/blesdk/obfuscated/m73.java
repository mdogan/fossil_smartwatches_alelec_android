package com.fossil.blesdk.obfuscated;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m73 extends RecyclerView.l {
    @DexIgnore
    public Drawable a;
    @DexIgnore
    public /* final */ Rect b; // = new Rect();
    @DexIgnore
    public int c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public m73(int i) {
        this.c = i;
    }

    @DexIgnore
    public final void a(Drawable drawable) {
        wd4.b(drawable, ResourceManager.DRAWABLE);
        this.a = drawable;
    }

    @DexIgnore
    public final void b(Canvas canvas, RecyclerView recyclerView) {
        int i;
        int i2;
        canvas.save();
        if (recyclerView.getClipToPadding()) {
            i2 = recyclerView.getPaddingLeft();
            i = recyclerView.getWidth() - recyclerView.getPaddingRight();
            canvas.clipRect(i2, recyclerView.getPaddingTop(), i, recyclerView.getHeight() - recyclerView.getPaddingBottom());
        } else {
            i = recyclerView.getWidth();
            i2 = 0;
        }
        Drawable drawable = this.a;
        if (drawable != null) {
            int childCount = recyclerView.getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = recyclerView.getChildAt(i3);
                recyclerView.a(childAt, this.b);
                int i4 = this.b.bottom;
                wd4.a((Object) childAt, "child");
                int round = i4 + Math.round(childAt.getTranslationY());
                drawable.setBounds(i2, round - drawable.getIntrinsicHeight(), i, round);
                drawable.draw(canvas);
            }
        }
        canvas.restore();
    }

    @DexIgnore
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        wd4.b(rect, "outRect");
        wd4.b(view, "view");
        wd4.b(recyclerView, "parent");
        wd4.b(state, "state");
        Drawable drawable = this.a;
        if (drawable != null) {
            RecyclerView.g adapter = recyclerView.getAdapter();
            if (adapter != null) {
                int itemCount = adapter.getItemCount();
                int e = recyclerView.e(view);
                if (e != 0) {
                    int i = e + 1;
                    if (i < itemCount) {
                        RecyclerView.g adapter2 = recyclerView.getAdapter();
                        if (adapter2 == null) {
                            wd4.a();
                            throw null;
                        } else if (adapter2.getItemViewType(i) == 1) {
                            if (this.c == 1) {
                                rect.set(0, 0, 0, drawable.getIntrinsicHeight());
                                return;
                            } else {
                                rect.set(0, 0, drawable.getIntrinsicWidth(), 0);
                                return;
                            }
                        }
                    }
                }
                rect.set(0, 0, 0, 0);
                return;
            }
            wd4.a();
            throw null;
        }
        rect.set(0, 0, 0, 0);
    }

    @DexIgnore
    public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        wd4.b(canvas, "c");
        wd4.b(recyclerView, "parent");
        wd4.b(state, "state");
        if (recyclerView.getLayoutManager() != null && this.a != null) {
            if (this.c == 1) {
                b(canvas, recyclerView);
            } else {
                a(canvas, recyclerView);
            }
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, RecyclerView recyclerView) {
        int i;
        int i2;
        canvas.save();
        int i3 = 0;
        if (recyclerView.getClipToPadding()) {
            i2 = recyclerView.getPaddingTop();
            i = recyclerView.getHeight() - recyclerView.getPaddingBottom();
            canvas.clipRect(recyclerView.getPaddingLeft(), i2, recyclerView.getWidth() - recyclerView.getPaddingRight(), i);
        } else {
            i = recyclerView.getHeight();
            i2 = 0;
        }
        Drawable drawable = this.a;
        if (drawable != null) {
            int childCount = recyclerView.getChildCount();
            while (i3 < childCount) {
                View childAt = recyclerView.getChildAt(i3);
                RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null) {
                    layoutManager.b(childAt, this.b);
                    int i4 = this.b.right;
                    wd4.a((Object) childAt, "child");
                    int round = i4 + Math.round(childAt.getTranslationX());
                    drawable.setBounds(round - drawable.getIntrinsicWidth(), i2, round, i);
                    drawable.draw(canvas);
                    i3++;
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }
        canvas.restore();
    }
}
