package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.hs3;
import com.fossil.blesdk.obfuscated.zs2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import kotlin.Pair;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lm3 extends zm3 implements dn3, zs2.b {
    @DexIgnore
    public static /* final */ a q; // = new a((rd4) null);
    @DexIgnore
    public ur3<pe2> k;
    @DexIgnore
    public en3 l;
    @DexIgnore
    public zs2 m;
    @DexIgnore
    public ps2 n;
    @DexIgnore
    public List<Pair<ShineDevice, String>> o; // = new ArrayList();
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            String simpleName = lm3.class.getSimpleName();
            wd4.a((Object) simpleName, "PairingDeviceFoundFragment::class.java.simpleName");
            return simpleName;
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }

        @DexIgnore
        public final lm3 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            lm3 lm3 = new lm3();
            lm3.setArguments(bundle);
            return lm3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ pe2 a;
        @DexIgnore
        public /* final */ /* synthetic */ lm3 b;

        @DexIgnore
        public b(pe2 pe2, lm3 lm3, Ref$ObjectRef ref$ObjectRef) {
            this.a = pe2;
            this.b = lm3;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.b.getActivity(), R.anim.move_in);
            this.a.s.startAnimation(loadAnimation);
            this.a.v.startAnimation(loadAnimation);
            this.a.q.startAnimation(loadAnimation);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lm3 e;

        @DexIgnore
        public c(lm3 lm3) {
            this.e = lm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            lm3.b(this.e).a(2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lm3 e;

        @DexIgnore
        public d(lm3 lm3) {
            this.e = lm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            lm3.b(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lm3 e;

        @DexIgnore
        public e(lm3 lm3) {
            this.e = lm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (lm3.c(this.e).d() < this.e.o.size()) {
                lm3.b(this.e).b((ShineDevice) ((Pair) this.e.o.get(lm3.c(this.e).d())).getFirst());
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ en3 b(lm3 lm3) {
        en3 en3 = lm3.l;
        if (en3 != null) {
            return en3;
        }
        wd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ps2 c(lm3 lm3) {
        ps2 ps2 = lm3.n;
        if (ps2 != null) {
            return ps2;
        }
        wd4.d("mSnapHelper");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void h(List<Pair<ShineDevice, String>> list) {
        wd4.b(list, "shineDeviceList");
        this.o = list;
        zs2 zs2 = this.m;
        if (zs2 != null) {
            zs2.a(list);
            int size = this.o.size();
            if (size <= 1) {
                ur3<pe2> ur3 = this.k;
                if (ur3 != null) {
                    pe2 a2 = ur3.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.s;
                        if (flexibleTextView != null) {
                            be4 be4 = be4.a;
                            Locale locale = Locale.US;
                            wd4.a((Object) locale, "Locale.US");
                            String a3 = tm2.a((Context) getActivity(), (int) R.string.Onboarding_Pairing_DeviceFound_Title__NumberDevicesFound);
                            wd4.a((Object) a3, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                            Object[] objArr = {Integer.valueOf(size)};
                            String format = String.format(locale, a3, Arrays.copyOf(objArr, objArr.length));
                            wd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
                            flexibleTextView.setText(format);
                            return;
                        }
                        return;
                    }
                    return;
                }
                wd4.d("mBinding");
                throw null;
            }
            ur3<pe2> ur32 = this.k;
            if (ur32 != null) {
                pe2 a4 = ur32.a();
                if (a4 != null) {
                    FlexibleTextView flexibleTextView2 = a4.s;
                    if (flexibleTextView2 != null) {
                        be4 be42 = be4.a;
                        Locale locale2 = Locale.US;
                        wd4.a((Object) locale2, "Locale.US");
                        String a5 = tm2.a((Context) getActivity(), (int) R.string.Onboarding_Pairing_DeviceFound_Title__NumberDevicesFound);
                        wd4.a((Object) a5, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                        Object[] objArr2 = {Integer.valueOf(size)};
                        String format2 = String.format(locale2, a5, Arrays.copyOf(objArr2, objArr2.length));
                        wd4.a((Object) format2, "java.lang.String.format(locale, format, *args)");
                        flexibleTextView2.setText(format2);
                        return;
                    }
                    return;
                }
                return;
            }
            wd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public Animation onCreateAnimation(int i, boolean z, int i2) {
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = null;
        if (z) {
            ur3<pe2> ur3 = this.k;
            if (ur3 != null) {
                pe2 a2 = ur3.a();
                if (!(a2 == null || getActivity() == null)) {
                    ref$ObjectRef.element = AnimationUtils.loadAnimation(getActivity(), R.anim.fragment_enter);
                    Animation animation = (Animation) ref$ObjectRef.element;
                    if (animation != null) {
                        animation.setAnimationListener(new b(a2, this, ref$ObjectRef));
                    }
                }
            } else {
                wd4.d("mBinding");
                throw null;
            }
        }
        return (Animation) ref$ObjectRef.element;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        pe2 pe2 = (pe2) ra.a(layoutInflater, R.layout.fragment_pairing_device_found, viewGroup, false, O0());
        this.k = new ur3<>(this, pe2);
        wd4.a((Object) pe2, "binding");
        return pe2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wd4.b(view, "view");
        super.onViewCreated(view, bundle);
        ur3<pe2> ur3 = this.k;
        if (ur3 != null) {
            pe2 a2 = ur3.a();
            if (a2 != null) {
                RTLImageView rTLImageView = a2.t;
                if (rTLImageView != null) {
                    rTLImageView.setOnClickListener(new c(this));
                }
            }
            ur3<pe2> ur32 = this.k;
            if (ur32 != null) {
                pe2 a3 = ur32.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView = a3.s;
                    if (flexibleTextView != null) {
                        be4 be4 = be4.a;
                        Locale locale = Locale.US;
                        wd4.a((Object) locale, "Locale.US");
                        String a4 = tm2.a(getContext(), (int) R.string.Onboarding_Pairing_DeviceFound_Title__NumberDevicesFound);
                        wd4.a((Object) a4, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                        Object[] objArr = {Integer.valueOf(this.o.size())};
                        String format = String.format(locale, a4, Arrays.copyOf(objArr, objArr.length));
                        wd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
                        flexibleTextView.setText(format);
                    }
                }
                yn a5 = sn.a((Fragment) this);
                wd4.a((Object) a5, "Glide.with(this)");
                this.m = new zs2(a5, this);
                zs2 zs2 = this.m;
                if (zs2 != null) {
                    zs2.a(this.o);
                }
                ur3<pe2> ur33 = this.k;
                if (ur33 != null) {
                    pe2 a6 = ur33.a();
                    if (a6 != null) {
                        RecyclerView recyclerView = a6.v;
                        if (recyclerView != null) {
                            wd4.a((Object) recyclerView, "it");
                            recyclerView.setLayoutManager(new LinearLayoutManager(PortfolioApp.W.c().getApplicationContext(), 0, false));
                            recyclerView.a((RecyclerView.l) new eu3());
                            recyclerView.setAdapter(this.m);
                            this.n = new ps2();
                            ps2 ps2 = this.n;
                            if (ps2 != null) {
                                ps2.a(recyclerView);
                            } else {
                                wd4.d("mSnapHelper");
                                throw null;
                            }
                        }
                    }
                    ur3<pe2> ur34 = this.k;
                    if (ur34 != null) {
                        pe2 a7 = ur34.a();
                        if (a7 != null) {
                            FlexibleTextView flexibleTextView2 = a7.r;
                            if (flexibleTextView2 != null) {
                                flexibleTextView2.setOnClickListener(new d(this));
                            }
                        }
                        ur3<pe2> ur35 = this.k;
                        if (ur35 != null) {
                            pe2 a8 = ur35.a();
                            if (a8 != null) {
                                FlexibleButton flexibleButton = a8.q;
                                if (flexibleButton != null) {
                                    flexibleButton.setOnClickListener(new e(this));
                                }
                            }
                            Bundle arguments = getArguments();
                            if (arguments != null) {
                                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                                ur3<pe2> ur36 = this.k;
                                if (ur36 != null) {
                                    pe2 a9 = ur36.a();
                                    if (a9 != null) {
                                        DashBar dashBar = a9.u;
                                        if (dashBar != null) {
                                            hs3.a aVar = hs3.a;
                                            wd4.a((Object) dashBar, "this");
                                            aVar.b(dashBar, z, 500);
                                            return;
                                        }
                                        return;
                                    }
                                    return;
                                }
                                wd4.d("mBinding");
                                throw null;
                            }
                            return;
                        }
                        wd4.d("mBinding");
                        throw null;
                    }
                    wd4.d("mBinding");
                    throw null;
                }
                wd4.d("mBinding");
                throw null;
            }
            wd4.d("mBinding");
            throw null;
        }
        wd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(en3 en3) {
        wd4.b(en3, "presenter");
        this.l = en3;
    }

    @DexIgnore
    public void a(View view, zs2.c cVar, int i) {
        wd4.b(view, "view");
        wd4.b(cVar, "viewHolder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = q.a();
        StringBuilder sb = new StringBuilder();
        sb.append("onItemClick - position=");
        sb.append(i);
        sb.append(", mSnappedPos=");
        ps2 ps2 = this.n;
        if (ps2 != null) {
            sb.append(ps2.d());
            local.d(a2, sb.toString());
            ps2 ps22 = this.n;
            if (ps22 == null) {
                wd4.d("mSnapHelper");
                throw null;
            } else if (ps22.d() != i) {
                ur3<pe2> ur3 = this.k;
                if (ur3 != null) {
                    pe2 a3 = ur3.a();
                    if (a3 != null) {
                        RecyclerView recyclerView = a3.v;
                        if (recyclerView != null) {
                            ps2 ps23 = this.n;
                            if (ps23 != null) {
                                recyclerView.j((i - ps23.d()) * view.getWidth(), view.getHeight());
                            } else {
                                wd4.d("mSnapHelper");
                                throw null;
                            }
                        }
                    }
                } else {
                    wd4.d("mBinding");
                    throw null;
                }
            } else {
                en3 en3 = this.l;
                if (en3 != null) {
                    List<Pair<ShineDevice, String>> list = this.o;
                    ps2 ps24 = this.n;
                    if (ps24 != null) {
                        en3.b((ShineDevice) list.get(ps24.d()).getFirst());
                    } else {
                        wd4.d("mSnapHelper");
                        throw null;
                    }
                } else {
                    wd4.d("mPresenter");
                    throw null;
                }
            }
        } else {
            wd4.d("mSnapHelper");
            throw null;
        }
    }
}
