package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Paint;
import android.os.Build;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k9 {
    @DexIgnore
    public WeakReference<View> a;
    @DexIgnore
    public Runnable b; // = null;
    @DexIgnore
    public Runnable c; // = null;
    @DexIgnore
    public int d; // = -1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ l9 a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;

        @DexIgnore
        public a(k9 k9Var, l9 l9Var, View view) {
            this.a = l9Var;
            this.b = view;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.a.a(this.b);
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.a.b(this.b);
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            this.a.c(this.b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ n9 a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;

        @DexIgnore
        public b(k9 k9Var, n9 n9Var, View view) {
            this.a = n9Var;
            this.b = view;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.a.a(this.b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements l9 {
        @DexIgnore
        public k9 a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public c(k9 k9Var) {
            this.a = k9Var;
        }

        @DexIgnore
        public void a(View view) {
            Object tag = view.getTag(2113929216);
            l9 l9Var = tag instanceof l9 ? (l9) tag : null;
            if (l9Var != null) {
                l9Var.a(view);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: com.fossil.blesdk.obfuscated.l9} */
        /* JADX WARNING: Multi-variable type inference failed */
        public void b(View view) {
            int i = this.a.d;
            l9 l9Var = null;
            if (i > -1) {
                view.setLayerType(i, (Paint) null);
                this.a.d = -1;
            }
            if (Build.VERSION.SDK_INT >= 16 || !this.b) {
                k9 k9Var = this.a;
                Runnable runnable = k9Var.c;
                if (runnable != null) {
                    k9Var.c = null;
                    runnable.run();
                }
                Object tag = view.getTag(2113929216);
                if (tag instanceof l9) {
                    l9Var = tag;
                }
                if (l9Var != null) {
                    l9Var.b(view);
                }
                this.b = true;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: com.fossil.blesdk.obfuscated.l9} */
        /* JADX WARNING: Multi-variable type inference failed */
        public void c(View view) {
            this.b = false;
            l9 l9Var = null;
            if (this.a.d > -1) {
                view.setLayerType(2, (Paint) null);
            }
            k9 k9Var = this.a;
            Runnable runnable = k9Var.b;
            if (runnable != null) {
                k9Var.b = null;
                runnable.run();
            }
            Object tag = view.getTag(2113929216);
            if (tag instanceof l9) {
                l9Var = tag;
            }
            if (l9Var != null) {
                l9Var.c(view);
            }
        }
    }

    @DexIgnore
    public k9(View view) {
        this.a = new WeakReference<>(view);
    }

    @DexIgnore
    public k9 a(long j) {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().setDuration(j);
        }
        return this;
    }

    @DexIgnore
    public k9 b(float f) {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().translationY(f);
        }
        return this;
    }

    @DexIgnore
    public void c() {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().start();
        }
    }

    @DexIgnore
    public k9 a(float f) {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().alpha(f);
        }
        return this;
    }

    @DexIgnore
    public long b() {
        View view = (View) this.a.get();
        if (view != null) {
            return view.animate().getDuration();
        }
        return 0;
    }

    @DexIgnore
    public k9 a(Interpolator interpolator) {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().setInterpolator(interpolator);
        }
        return this;
    }

    @DexIgnore
    public k9 b(long j) {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().setStartDelay(j);
        }
        return this;
    }

    @DexIgnore
    public void a() {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().cancel();
        }
    }

    @DexIgnore
    public k9 a(l9 l9Var) {
        View view = (View) this.a.get();
        if (view != null) {
            if (Build.VERSION.SDK_INT >= 16) {
                a(view, l9Var);
            } else {
                view.setTag(2113929216, l9Var);
                a(view, new c(this));
            }
        }
        return this;
    }

    @DexIgnore
    public final void a(View view, l9 l9Var) {
        if (l9Var != null) {
            view.animate().setListener(new a(this, l9Var, view));
        } else {
            view.animate().setListener((Animator.AnimatorListener) null);
        }
    }

    @DexIgnore
    public k9 a(n9 n9Var) {
        View view = (View) this.a.get();
        if (view != null && Build.VERSION.SDK_INT >= 19) {
            b bVar = null;
            if (n9Var != null) {
                bVar = new b(this, n9Var, view);
            }
            view.animate().setUpdateListener(bVar);
        }
        return this;
    }
}
