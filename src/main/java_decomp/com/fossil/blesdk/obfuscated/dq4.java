package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface dq4 {
    @DexIgnore
    String a();

    @DexIgnore
    String b();

    @DexIgnore
    String c();

    @DexIgnore
    long getContentLength();
}
