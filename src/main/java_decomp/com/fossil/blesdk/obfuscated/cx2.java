package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cx2 implements Factory<NotificationContactsPresenter> {
    @DexIgnore
    public static NotificationContactsPresenter a(xw2 xw2, k62 k62, qx2 qx2, tx2 tx2, LoaderManager loaderManager) {
        return new NotificationContactsPresenter(xw2, k62, qx2, tx2, loaderManager);
    }
}
