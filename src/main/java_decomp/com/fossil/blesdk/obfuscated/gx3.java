package com.fossil.blesdk.obfuscated;

import javax.security.auth.x500.X500Principal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gx3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b; // = this.a.length();
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public char[] g;

    @DexIgnore
    public gx3(X500Principal x500Principal) {
        this.a = x500Principal.getName("RFC2253");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009b, code lost:
        r1 = r8.g;
        r2 = r8.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a7, code lost:
        return new java.lang.String(r1, r2, r8.f - r2);
     */
    @DexIgnore
    public final String a() {
        int i = this.c;
        this.d = i;
        this.e = i;
        while (true) {
            int i2 = this.c;
            if (i2 < this.b) {
                char[] cArr = this.g;
                char c2 = cArr[i2];
                if (c2 == ' ') {
                    int i3 = this.e;
                    this.f = i3;
                    this.c = i2 + 1;
                    this.e = i3 + 1;
                    cArr[i3] = ' ';
                    while (true) {
                        int i4 = this.c;
                        if (i4 >= this.b) {
                            break;
                        }
                        char[] cArr2 = this.g;
                        if (cArr2[i4] != ' ') {
                            break;
                        }
                        int i5 = this.e;
                        this.e = i5 + 1;
                        cArr2[i5] = ' ';
                        this.c = i4 + 1;
                    }
                    int i6 = this.c;
                    if (i6 == this.b) {
                        break;
                    }
                    char[] cArr3 = this.g;
                    if (cArr3[i6] == ',' || cArr3[i6] == '+' || cArr3[i6] == ';') {
                        break;
                    }
                } else if (c2 == ';') {
                    break;
                } else if (c2 != '\\') {
                    if (c2 == '+' || c2 == ',') {
                        break;
                    }
                    int i7 = this.e;
                    this.e = i7 + 1;
                    cArr[i7] = cArr[i2];
                    this.c = i2 + 1;
                } else {
                    int i8 = this.e;
                    this.e = i8 + 1;
                    cArr[i8] = b();
                    this.c++;
                }
            } else {
                char[] cArr4 = this.g;
                int i9 = this.d;
                return new String(cArr4, i9, this.e - i9);
            }
        }
        char[] cArr5 = this.g;
        int i10 = this.d;
        return new String(cArr5, i10, this.e - i10);
    }

    @DexIgnore
    public final char b() {
        this.c++;
        int i = this.c;
        if (i != this.b) {
            char c2 = this.g[i];
            if (!(c2 == ' ' || c2 == '%' || c2 == '\\' || c2 == '_' || c2 == '\"' || c2 == '#')) {
                switch (c2) {
                    case '*':
                    case '+':
                    case ',':
                        break;
                    default:
                        switch (c2) {
                            case ';':
                            case '<':
                            case '=':
                            case '>':
                                break;
                            default:
                                return c();
                        }
                }
            }
            return this.g[this.c];
        }
        throw new IllegalStateException("Unexpected end of DN: " + this.a);
    }

    @DexIgnore
    public final char c() {
        int i;
        int i2;
        int a2 = a(this.c);
        this.c++;
        if (a2 < 128) {
            return (char) a2;
        }
        if (a2 < 192 || a2 > 247) {
            return '?';
        }
        if (a2 <= 223) {
            i2 = a2 & 31;
            i = 1;
        } else if (a2 <= 239) {
            i = 2;
            i2 = a2 & 15;
        } else {
            i = 3;
            i2 = a2 & 7;
        }
        for (int i3 = 0; i3 < i; i3++) {
            this.c++;
            int i4 = this.c;
            if (i4 == this.b || this.g[i4] != '\\') {
                return '?';
            }
            this.c = i4 + 1;
            int a3 = a(this.c);
            this.c++;
            if ((a3 & 192) != 128) {
                return '?';
            }
            i2 = (i2 << 6) + (a3 & 63);
        }
        return (char) i2;
    }

    @DexIgnore
    public final String d() {
        int i = this.c;
        if (i + 4 < this.b) {
            this.d = i;
            this.c = i + 1;
            while (true) {
                int i2 = this.c;
                if (i2 == this.b) {
                    break;
                }
                char[] cArr = this.g;
                if (cArr[i2] == '+' || cArr[i2] == ',' || cArr[i2] == ';') {
                    break;
                } else if (cArr[i2] == ' ') {
                    this.e = i2;
                    this.c = i2 + 1;
                    while (true) {
                        int i3 = this.c;
                        if (i3 >= this.b || this.g[i3] != ' ') {
                            break;
                        }
                        this.c = i3 + 1;
                    }
                } else {
                    if (cArr[i2] >= 'A' && cArr[i2] <= 'F') {
                        cArr[i2] = (char) (cArr[i2] + ' ');
                    }
                    this.c++;
                }
            }
            this.e = this.c;
            int i4 = this.e;
            int i5 = this.d;
            int i6 = i4 - i5;
            if (i6 < 5 || (i6 & 1) == 0) {
                throw new IllegalStateException("Unexpected end of DN: " + this.a);
            }
            byte[] bArr = new byte[(i6 / 2)];
            int i7 = i5 + 1;
            for (int i8 = 0; i8 < bArr.length; i8++) {
                bArr[i8] = (byte) a(i7);
                i7 += 2;
            }
            return new String(this.g, this.d, i6);
        }
        throw new IllegalStateException("Unexpected end of DN: " + this.a);
    }

    @DexIgnore
    public final String e() {
        while (true) {
            int i = this.c;
            if (i >= this.b || this.g[i] != ' ') {
                int i2 = this.c;
            } else {
                this.c = i + 1;
            }
        }
        int i22 = this.c;
        if (i22 == this.b) {
            return null;
        }
        this.d = i22;
        this.c = i22 + 1;
        while (true) {
            int i3 = this.c;
            if (i3 >= this.b) {
                break;
            }
            char[] cArr = this.g;
            if (cArr[i3] == '=' || cArr[i3] == ' ') {
                break;
            }
            this.c = i3 + 1;
        }
        int i4 = this.c;
        if (i4 < this.b) {
            this.e = i4;
            if (this.g[i4] == ' ') {
                while (true) {
                    int i5 = this.c;
                    if (i5 >= this.b) {
                        break;
                    }
                    char[] cArr2 = this.g;
                    if (cArr2[i5] == '=' || cArr2[i5] != ' ') {
                        break;
                    }
                    this.c = i5 + 1;
                }
                char[] cArr3 = this.g;
                int i6 = this.c;
                if (cArr3[i6] != '=' || i6 == this.b) {
                    throw new IllegalStateException("Unexpected end of DN: " + this.a);
                }
            }
            this.c++;
            while (true) {
                int i7 = this.c;
                if (i7 >= this.b || this.g[i7] != ' ') {
                    int i8 = this.e;
                    int i9 = this.d;
                } else {
                    this.c = i7 + 1;
                }
            }
            int i82 = this.e;
            int i92 = this.d;
            if (i82 - i92 > 4) {
                char[] cArr4 = this.g;
                if (cArr4[i92 + 3] == '.' && (cArr4[i92] == 'O' || cArr4[i92] == 'o')) {
                    char[] cArr5 = this.g;
                    int i10 = this.d;
                    if (cArr5[i10 + 1] == 'I' || cArr5[i10 + 1] == 'i') {
                        char[] cArr6 = this.g;
                        int i11 = this.d;
                        if (cArr6[i11 + 2] == 'D' || cArr6[i11 + 2] == 'd') {
                            this.d += 4;
                        }
                    }
                }
            }
            char[] cArr7 = this.g;
            int i12 = this.d;
            return new String(cArr7, i12, this.e - i12);
        }
        throw new IllegalStateException("Unexpected end of DN: " + this.a);
    }

    @DexIgnore
    public final String f() {
        this.c++;
        this.d = this.c;
        this.e = this.d;
        while (true) {
            int i = this.c;
            if (i != this.b) {
                char[] cArr = this.g;
                if (cArr[i] == '\"') {
                    this.c = i + 1;
                    while (true) {
                        int i2 = this.c;
                        if (i2 >= this.b || this.g[i2] != ' ') {
                            char[] cArr2 = this.g;
                            int i3 = this.d;
                        } else {
                            this.c = i2 + 1;
                        }
                    }
                    char[] cArr22 = this.g;
                    int i32 = this.d;
                    return new String(cArr22, i32, this.e - i32);
                }
                if (cArr[i] == '\\') {
                    cArr[this.e] = b();
                } else {
                    cArr[this.e] = cArr[i];
                }
                this.c++;
                this.e++;
            } else {
                throw new IllegalStateException("Unexpected end of DN: " + this.a);
            }
        }
    }

    @DexIgnore
    public final int a(int i) {
        int i2;
        int i3;
        int i4 = i + 1;
        if (i4 < this.b) {
            char c2 = this.g[i];
            if (c2 >= '0' && c2 <= '9') {
                i2 = c2 - '0';
            } else if (c2 >= 'a' && c2 <= 'f') {
                i2 = c2 - 'W';
            } else if (c2 < 'A' || c2 > 'F') {
                throw new IllegalStateException("Malformed DN: " + this.a);
            } else {
                i2 = c2 - '7';
            }
            char c3 = this.g[i4];
            if (c3 >= '0' && c3 <= '9') {
                i3 = c3 - '0';
            } else if (c3 >= 'a' && c3 <= 'f') {
                i3 = c3 - 'W';
            } else if (c3 < 'A' || c3 > 'F') {
                throw new IllegalStateException("Malformed DN: " + this.a);
            } else {
                i3 = c3 - '7';
            }
            return (i2 << 4) + i3;
        }
        throw new IllegalStateException("Malformed DN: " + this.a);
    }

    @DexIgnore
    public String a(String str) {
        String str2;
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = this.a.toCharArray();
        String e2 = e();
        if (e2 == null) {
            return null;
        }
        do {
            int i = this.c;
            if (i == this.b) {
                return null;
            }
            char c2 = this.g[i];
            if (c2 == '\"') {
                str2 = f();
            } else if (c2 != '#') {
                str2 = (c2 == '+' || c2 == ',' || c2 == ';') ? "" : a();
            } else {
                str2 = d();
            }
            if (str.equalsIgnoreCase(e2)) {
                return str2;
            }
            int i2 = this.c;
            if (i2 >= this.b) {
                return null;
            }
            char[] cArr = this.g;
            if (cArr[i2] == ',' || cArr[i2] == ';' || cArr[i2] == '+') {
                this.c++;
                e2 = e();
            } else {
                throw new IllegalStateException("Malformed DN: " + this.a);
            }
        } while (e2 != null);
        throw new IllegalStateException("Malformed DN: " + this.a);
    }
}
