package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.sr4;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qa0 {
    @DexIgnore
    public static /* final */ qa0 a; // = new qa0();

    @DexIgnore
    public final pa0 a(String str, String str2) {
        wd4.b(str, "secretKey");
        wd4.b(str2, "accessKey");
        OkHttpClient.b bVar = new OkHttpClient.b();
        bVar.b((Interceptor) new ra0(str2, str));
        bVar.a(30000, TimeUnit.MILLISECONDS);
        bVar.b(60000, TimeUnit.MILLISECONDS);
        try {
            Retrofit.b bVar2 = new Retrofit.b();
            bVar2.a(bVar.a());
            bVar2.a((sr4.a) at4.a());
            bVar2.a("http://localhost/");
            return (pa0) bVar2.a().a(pa0.class);
        } catch (Exception e) {
            ea0.l.a(e);
            return null;
        }
    }
}
