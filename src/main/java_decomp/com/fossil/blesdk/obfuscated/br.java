package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface br {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(bq<?> bqVar);
    }

    @DexIgnore
    bq<?> a(ko koVar);

    @DexIgnore
    bq<?> a(ko koVar, bq<?> bqVar);

    @DexIgnore
    void a();

    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(a aVar);
}
