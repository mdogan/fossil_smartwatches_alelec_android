package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yw1 extends ow1 {
    @DexIgnore
    public /* final */ Set<Class<?>> a;
    @DexIgnore
    public /* final */ Set<Class<?>> b;
    @DexIgnore
    public /* final */ Set<Class<?>> c;
    @DexIgnore
    public /* final */ kw1 d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements bx1 {
        @DexIgnore
        public a(Set<Class<?>> set, bx1 bx1) {
        }
    }

    @DexIgnore
    public yw1(jw1<?> jw1, kw1 kw1) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        for (nw1 next : jw1.b()) {
            if (next.c()) {
                hashSet.add(next.a());
            } else {
                hashSet2.add(next.a());
            }
        }
        if (!jw1.d().isEmpty()) {
            hashSet.add(bx1.class);
        }
        this.a = Collections.unmodifiableSet(hashSet);
        this.b = Collections.unmodifiableSet(hashSet2);
        this.c = jw1.d();
        this.d = kw1;
    }

    @DexIgnore
    public final <T> T a(Class<T> cls) {
        if (this.a.contains(cls)) {
            T a2 = this.d.a(cls);
            if (!cls.equals(bx1.class)) {
                return a2;
            }
            return new a(this.c, (bx1) a2);
        }
        throw new IllegalArgumentException(String.format("Requesting %s is not allowed.", new Object[]{cls}));
    }

    @DexIgnore
    public final <T> fz1<T> b(Class<T> cls) {
        if (this.b.contains(cls)) {
            return this.d.b(cls);
        }
        throw new IllegalArgumentException(String.format("Requesting Provider<%s> is not allowed.", new Object[]{cls}));
    }
}
