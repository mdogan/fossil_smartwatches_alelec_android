package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fs3 {
    @DexIgnore
    public static /* final */ rs3 a; // = new rs3("GLOBAL");

    @DexIgnore
    public static void a() {
        a.b();
    }

    @DexIgnore
    public static tg b() {
        return a;
    }

    @DexIgnore
    public static void c() {
        a.c();
    }
}
