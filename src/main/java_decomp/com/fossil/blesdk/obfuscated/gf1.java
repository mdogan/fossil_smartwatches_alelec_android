package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.tn0;
import com.google.android.gms.maps.GoogleMapOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gf1 extends b51 implements le1 {
    @DexIgnore
    public gf1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IMapFragmentDelegate");
    }

    @DexIgnore
    public final void a(tn0 tn0, GoogleMapOptions googleMapOptions, Bundle bundle) throws RemoteException {
        Parcel o = o();
        d51.a(o, (IInterface) tn0);
        d51.a(o, (Parcelable) googleMapOptions);
        d51.a(o, (Parcelable) bundle);
        b(2, o);
    }

    @DexIgnore
    public final void b(Bundle bundle) throws RemoteException {
        Parcel o = o();
        d51.a(o, (Parcelable) bundle);
        b(3, o);
    }

    @DexIgnore
    public final void c() throws RemoteException {
        b(16, o());
    }

    @DexIgnore
    public final void d() throws RemoteException {
        b(5, o());
    }

    @DexIgnore
    public final void e() throws RemoteException {
        b(7, o());
    }

    @DexIgnore
    public final void onLowMemory() throws RemoteException {
        b(9, o());
    }

    @DexIgnore
    public final void onPause() throws RemoteException {
        b(6, o());
    }

    @DexIgnore
    public final void b() throws RemoteException {
        b(8, o());
    }

    @DexIgnore
    public final tn0 a(tn0 tn0, tn0 tn02, Bundle bundle) throws RemoteException {
        Parcel o = o();
        d51.a(o, (IInterface) tn0);
        d51.a(o, (IInterface) tn02);
        d51.a(o, (Parcelable) bundle);
        Parcel a = a(4, o);
        tn0 a2 = tn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final void a(Bundle bundle) throws RemoteException {
        Parcel o = o();
        d51.a(o, (Parcelable) bundle);
        Parcel a = a(10, o);
        if (a.readInt() != 0) {
            bundle.readFromParcel(a);
        }
        a.recycle();
    }

    @DexIgnore
    public final void a(te1 te1) throws RemoteException {
        Parcel o = o();
        d51.a(o, (IInterface) te1);
        b(12, o);
    }

    @DexIgnore
    public final void a() throws RemoteException {
        b(15, o());
    }
}
