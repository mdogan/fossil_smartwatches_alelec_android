package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tj {
    @DexIgnore
    public static zf a; // = new a(1, 2);
    @DexIgnore
    public static zf b; // = new b(3, 4);
    @DexIgnore
    public static zf c; // = new c(4, 5);
    @DexIgnore
    public static zf d; // = new d(6, 7);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends zf {
        @DexIgnore
        public a(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(hg hgVar) {
            hgVar.b("CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
            hgVar.b("INSERT INTO SystemIdInfo(work_spec_id, system_id) SELECT work_spec_id, alarm_id AS system_id FROM alarmInfo");
            hgVar.b("DROP TABLE IF EXISTS alarmInfo");
            hgVar.b("INSERT OR IGNORE INTO worktag(tag, work_spec_id) SELECT worker_class_name AS tag, id AS work_spec_id FROM workspec");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends zf {
        @DexIgnore
        public b(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(hg hgVar) {
            if (Build.VERSION.SDK_INT >= 23) {
                hgVar.b("UPDATE workspec SET schedule_requested_at=0 WHERE state NOT IN (2, 3, 5) AND schedule_requested_at=-1 AND interval_duration<>0");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends zf {
        @DexIgnore
        public c(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(hg hgVar) {
            hgVar.b("ALTER TABLE workspec ADD COLUMN `trigger_content_update_delay` INTEGER NOT NULL DEFAULT -1");
            hgVar.b("ALTER TABLE workspec ADD COLUMN `trigger_max_content_delay` INTEGER NOT NULL DEFAULT -1");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends zf {
        @DexIgnore
        public d(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(hg hgVar) {
            hgVar.b("CREATE TABLE IF NOT EXISTS `WorkProgress` (`work_spec_id` TEXT NOT NULL, `progress` BLOB NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends zf {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public e(Context context, int i, int i2) {
            super(i, i2);
            this.a = context;
        }

        @DexIgnore
        public void migrate(hg hgVar) {
            new tl(this.a).a(true);
        }
    }
}
