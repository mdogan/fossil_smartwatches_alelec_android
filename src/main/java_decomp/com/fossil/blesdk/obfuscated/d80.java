package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.FileControlOperationCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d80 extends f80 {
    @DexIgnore
    public long M;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ d80(short s, Peripheral peripheral, int i, int i2, rd4 rd4) {
        this(s, peripheral, (i2 & 4) != 0 ? 1 : i);
    }

    @DexIgnore
    public void a(long j) {
        this.M = j;
    }

    @DexIgnore
    public long m() {
        return this.M;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d80(short s, Peripheral peripheral, int i) {
        super(FileControlOperationCode.ABORT_FILE, s, RequestId.ABORT_FILE, peripheral, i);
        wd4.b(peripheral, "peripheral");
        this.M = 5000;
    }
}
