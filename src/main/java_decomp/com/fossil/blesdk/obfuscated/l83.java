package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l83 {
    @DexIgnore
    public /* final */ g83 a;
    @DexIgnore
    public /* final */ v83 b;
    @DexIgnore
    public /* final */ q83 c;

    @DexIgnore
    public l83(g83 g83, v83 v83, q83 q83) {
        wd4.b(g83, "mActiveTimeOverviewDayView");
        wd4.b(v83, "mActiveTimeOverviewWeekView");
        wd4.b(q83, "mActiveTimeOverviewMonthView");
        this.a = g83;
        this.b = v83;
        this.c = q83;
    }

    @DexIgnore
    public final g83 a() {
        return this.a;
    }

    @DexIgnore
    public final q83 b() {
        return this.c;
    }

    @DexIgnore
    public final v83 c() {
        return this.b;
    }
}
