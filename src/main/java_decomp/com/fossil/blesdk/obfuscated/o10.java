package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o10 extends b10 {
    @DexIgnore
    public /* final */ byte[] m;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o10(GattCharacteristic.CharacteristicId characteristicId, byte[] bArr, Peripheral.c cVar) {
        super(BluetoothCommandId.WRITE_CHARACTERISTIC, characteristicId, cVar);
        wd4.b(characteristicId, "characteristicId");
        wd4.b(bArr, "dataToWrite");
        wd4.b(cVar, "bluetoothGattOperationCallbackProvider");
        this.m = bArr;
    }

    @DexIgnore
    public void a(Peripheral peripheral) {
        wd4.b(peripheral, "peripheral");
        peripheral.b(i(), this.m);
        b(true);
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        wd4.b(gattOperationResult, "gattOperationResult");
        return (gattOperationResult instanceof b20) && ((b20) gattOperationResult).b() == i();
    }

    @DexIgnore
    public sa0<GattOperationResult> f() {
        return b().k();
    }

    @DexIgnore
    public final byte[] j() {
        return this.m;
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        JSONObject a = super.a(z);
        if (z) {
            byte[] bArr = this.m;
            if (bArr.length < 100) {
                xa0.a(a, JSONKey.RAW_DATA, l90.a(bArr, (String) null, 1, (Object) null));
                return a;
            }
        }
        xa0.a(a, JSONKey.RAW_DATA_LENGTH, Integer.valueOf(this.m.length));
        return a;
    }
}
