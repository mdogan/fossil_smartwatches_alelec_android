package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ft2 extends sd<ActivitySummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public Unit d; // = Unit.METRIC;
    @DexIgnore
    public /* final */ PortfolioApp e;
    @DexIgnore
    public /* final */ kt2 f;
    @DexIgnore
    public /* final */ FragmentManager g;
    @DexIgnore
    public /* final */ as2 h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;

        @DexIgnore
        public b(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, String str5) {
            wd4.b(str, "mDayOfWeek");
            wd4.b(str2, "mDayOfMonth");
            wd4.b(str3, "mDailyValue");
            wd4.b(str4, "mDailyUnit");
            wd4.b(str5, "mDailyEst");
            this.a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = str3;
            this.g = str4;
            this.h = str5;
        }

        @DexIgnore
        public final void a(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final String c() {
            return this.f;
        }

        @DexIgnore
        public final Date d() {
            return this.a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final boolean g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, String str5, int i, rd4 rd4) {
            this(r1, (r0 & 2) != 0 ? false : z, (r0 & 4) == 0 ? z2 : false, (r0 & 8) != 0 ? r5 : str, (r0 & 16) != 0 ? r5 : str2, (r0 & 32) != 0 ? r5 : str3, (r0 & 64) != 0 ? r5 : str4, (r0 & 128) == 0 ? str5 : r5);
            int i2 = i;
            Date date2 = (i2 & 1) != 0 ? null : date;
            String str6 = "";
        }

        @DexIgnore
        public final void a(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final String b() {
            return this.g;
        }

        @DexIgnore
        public final void c(String str) {
            wd4.b(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void d(String str) {
            wd4.b(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void e(String str) {
            wd4.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final void b(String str) {
            wd4.b(str, "<set-?>");
            this.g = str;
        }

        @DexIgnore
        public final void a(String str) {
            wd4.b(str, "<set-?>");
            this.h = str;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ vg2 b;
        @DexIgnore
        public /* final */ /* synthetic */ ft2 c;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public a(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date a = this.e.a;
                if (a != null) {
                    this.e.c.f.b(a);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ft2 ft2, vg2 vg2, View view) {
            super(view);
            wd4.b(vg2, "binding");
            wd4.b(view, "root");
            this.c = ft2;
            this.b = vg2;
            this.b.d().setOnClickListener(new a(this));
        }

        @DexIgnore
        public void a(ActivitySummary activitySummary) {
            b a2 = this.c.a(activitySummary);
            this.a = a2.d();
            FlexibleTextView flexibleTextView = this.b.u;
            wd4.a((Object) flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(a2.f());
            FlexibleTextView flexibleTextView2 = this.b.t;
            wd4.a((Object) flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(a2.e());
            FlexibleTextView flexibleTextView3 = this.b.s;
            wd4.a((Object) flexibleTextView3, "binding.ftvDailyValue");
            flexibleTextView3.setText(a2.c());
            FlexibleTextView flexibleTextView4 = this.b.r;
            wd4.a((Object) flexibleTextView4, "binding.ftvDailyUnit");
            flexibleTextView4.setText(a2.b());
            FlexibleTextView flexibleTextView5 = this.b.v;
            wd4.a((Object) flexibleTextView5, "binding.ftvEst");
            flexibleTextView5.setText(a2.a());
            if (a2.g()) {
                this.b.r.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.disabledCalendarDay));
                FlexibleTextView flexibleTextView6 = this.b.r;
                wd4.a((Object) flexibleTextView6, "binding.ftvDailyUnit");
                flexibleTextView6.setAllCaps(true);
            } else {
                this.b.r.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.nonReachGoal));
                FlexibleTextView flexibleTextView7 = this.b.r;
                wd4.a((Object) flexibleTextView7, "binding.ftvDailyUnit");
                flexibleTextView7.setAllCaps(false);
            }
            ConstraintLayout constraintLayout = this.b.q;
            wd4.a((Object) constraintLayout, "binding.container");
            constraintLayout.setSelected(!a2.g());
            FlexibleTextView flexibleTextView8 = this.b.u;
            wd4.a((Object) flexibleTextView8, "binding.ftvDayOfWeek");
            flexibleTextView8.setSelected(a2.h());
            FlexibleTextView flexibleTextView9 = this.b.t;
            wd4.a((Object) flexibleTextView9, "binding.ftvDayOfMonth");
            flexibleTextView9.setSelected(a2.h());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public d(Date date, Date date2, String str, String str2) {
            wd4.b(str, "mWeekly");
            wd4.b(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Date date, Date date2, String str, String str2, int i, rd4 rd4) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final void a(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void b(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void a(String str) {
            wd4.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void b(String str) {
            wd4.b(str, "<set-?>");
            this.d = str;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends c {
        @DexIgnore
        public Date d;
        @DexIgnore
        public Date e;
        @DexIgnore
        public /* final */ xg2 f;
        @DexIgnore
        public /* final */ /* synthetic */ ft2 g;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e e;

            @DexIgnore
            public a(e eVar) {
                this.e = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.e.d != null && this.e.e != null) {
                    kt2 c = this.e.g.f;
                    Date b = this.e.d;
                    if (b != null) {
                        Date a = this.e.e;
                        if (a != null) {
                            c.b(b, a);
                        } else {
                            wd4.a();
                            throw null;
                        }
                    } else {
                        wd4.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public e(ft2 ft2, xg2 xg2) {
            super(ft2, r0, r1);
            wd4.b(xg2, "binding");
            this.g = ft2;
            vg2 vg2 = xg2.r;
            if (vg2 != null) {
                wd4.a((Object) vg2, "binding.dailyItem!!");
                View d2 = xg2.d();
                wd4.a((Object) d2, "binding.root");
                this.f = xg2;
                this.f.q.setOnClickListener(new a(this));
                return;
            }
            wd4.a();
            throw null;
        }

        @DexIgnore
        public void a(ActivitySummary activitySummary) {
            d b = this.g.b(activitySummary);
            this.e = b.a();
            this.d = b.b();
            FlexibleTextView flexibleTextView = this.f.s;
            wd4.a((Object) flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(b.c());
            FlexibleTextView flexibleTextView2 = this.f.t;
            wd4.a((Object) flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(b.d());
            super.a(activitySummary);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ft2 e;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder f;
        @DexIgnore
        public /* final */ /* synthetic */ boolean g;

        @DexIgnore
        public f(ft2 ft2, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.e = ft2;
            this.f = viewHolder;
            this.g = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            wd4.b(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardActivitiesAdapter", "onViewAttachedToWindow - mFragment.id=" + this.e.h.getId() + ", isAdded=" + this.e.h.isAdded());
            this.f.itemView.removeOnAttachStateChangeListener(this);
            Fragment a = this.e.g.a(this.e.h.R0());
            if (a == null) {
                FLogger.INSTANCE.getLocal().d("DashboardActivitiesAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                cb a2 = this.e.g.a();
                a2.a(view.getId(), this.e.h, this.e.h.R0());
                a2.d();
            } else if (this.g) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardActivitiesAdapter", "onViewAttachedToWindow - oldFragment.id=" + a.getId() + ", isAdded=" + a.isAdded());
                cb a3 = this.e.g.a();
                a3.d(a);
                a3.d();
                cb a4 = this.e.g.a();
                a4.a(view.getId(), this.e.h, this.e.h.R0());
                a4.d();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardActivitiesAdapter", "onViewAttachedToWindow - oldFragment.id=" + a.getId() + ", isAdded=" + a.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardActivitiesAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.e.h.getId() + ", isAdded2=" + this.e.h.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            wd4.b(view, "v");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends RecyclerView.ViewHolder {
        @DexIgnore
        public g(FrameLayout frameLayout, View view) {
            super(view);
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ft2(dt2 dt2, PortfolioApp portfolioApp, kt2 kt2, FragmentManager fragmentManager, as2 as2) {
        super(dt2);
        wd4.b(dt2, "activityDifference");
        wd4.b(portfolioApp, "mApp");
        wd4.b(kt2, "mOnItemClick");
        wd4.b(fragmentManager, "mFragmentManager");
        wd4.b(as2, "mFragment");
        this.e = portfolioApp;
        this.f = kt2;
        this.g = fragmentManager;
        this.h = as2;
    }

    @DexIgnore
    public long getItemId(int i) {
        if (getItemViewType(i) != 0) {
            return super.getItemId(i);
        }
        if (this.h.getId() == 0) {
            return 1010101;
        }
        return (long) this.h.getId();
    }

    @DexIgnore
    public int getItemViewType(int i) {
        if (i == 0) {
            return 0;
        }
        ActivitySummary activitySummary = (ActivitySummary) a(i);
        if (activitySummary == null) {
            return 1;
        }
        this.c.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
        Calendar calendar = this.c;
        wd4.a((Object) calendar, "mCalendar");
        Boolean s = sk2.s(calendar.getTime());
        wd4.a((Object) s, "DateHelper.isToday(mCalendar.time)");
        if (s.booleanValue() || this.c.get(7) == 7) {
            return 2;
        }
        return 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        wd4.b(viewHolder, "holder");
        int itemViewType = getItemViewType(i);
        boolean z = true;
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            wd4.a((Object) view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            wd4.a((Object) view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local.d("DashboardActivitiesAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            wd4.a((Object) view3, "holder.itemView");
            view3.setId((int) getItemId(i));
            viewHolder.itemView.addOnAttachStateChangeListener(new f(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((c) viewHolder).a((ActivitySummary) a(i));
        } else if (itemViewType != 2) {
            ((c) viewHolder).a((ActivitySummary) a(i));
        } else {
            ((e) viewHolder).a((ActivitySummary) a(i));
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        wd4.b(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new g(frameLayout, frameLayout);
        } else if (i == 1) {
            vg2 a2 = vg2.a(from, viewGroup, false);
            wd4.a((Object) a2, "ItemActivityDayBinding.i\u2026tInflater, parent, false)");
            View d2 = a2.d();
            wd4.a((Object) d2, "itemActivityDayBinding.root");
            return new c(this, a2, d2);
        } else if (i != 2) {
            vg2 a3 = vg2.a(from, viewGroup, false);
            wd4.a((Object) a3, "ItemActivityDayBinding.i\u2026tInflater, parent, false)");
            View d3 = a3.d();
            wd4.a((Object) d3, "itemActivityDayBinding.root");
            return new c(this, a3, d3);
        } else {
            xg2 a4 = xg2.a(from, viewGroup, false);
            wd4.a((Object) a4, "ItemActivityWeekBinding.\u2026tInflater, parent, false)");
            return new e(this, a4);
        }
    }

    @DexIgnore
    public final void c(rd<ActivitySummary> rdVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList - size=");
        sb.append(rdVar != null ? Integer.valueOf(rdVar.size()) : null);
        local.d("DashboardActivitiesAdapter", sb.toString());
        super.b(rdVar);
    }

    @DexIgnore
    public final b a(ActivitySummary activitySummary) {
        String str;
        b bVar = new b((Date) null, false, false, (String) null, (String) null, (String) null, (String) null, (String) null, 255, (rd4) null);
        if (activitySummary != null) {
            Calendar instance = Calendar.getInstance();
            instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            int i = instance.get(7);
            wd4.a((Object) instance, "calendar");
            Boolean s = sk2.s(instance.getTime());
            wd4.a((Object) s, "DateHelper.isToday(calendar.time)");
            if (s.booleanValue()) {
                String a2 = tm2.a((Context) this.e, (int) R.string.DashboardDiana_Main_StepsToday_Text__Today);
                wd4.a((Object) a2, "LanguageHelper.getString\u2026n_StepsToday_Text__Today)");
                bVar.e(a2);
            } else {
                bVar.e(ml2.b.b(i));
            }
            bVar.a(instance.getTime());
            bVar.d(String.valueOf(instance.get(5)));
            boolean z = false;
            if (activitySummary.getSteps() > ((double) 0)) {
                double steps = activitySummary.getSteps();
                bVar.c(pl2.a.b(Integer.valueOf((int) steps)));
                String a3 = tm2.a((Context) this.e, (int) R.string.DashboardDiana_Main_StepsToday_Title__Steps);
                wd4.a((Object) a3, "LanguageHelper.getString\u2026_StepsToday_Title__Steps)");
                if (a3 != null) {
                    String lowerCase = a3.toLowerCase();
                    wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    bVar.b(lowerCase);
                    if (this.d == Unit.IMPERIAL) {
                        StringBuilder sb = new StringBuilder();
                        be4 be4 = be4.a;
                        String a4 = tm2.a((Context) this.e, (int) R.string.DashboardDiana_Main_StepsToday_Text__EstNumberUnit);
                        wd4.a((Object) a4, "LanguageHelper.getString\u2026oday_Text__EstNumberUnit)");
                        Object[] objArr = {pl2.a.a(Float.valueOf((float) activitySummary.getDistance()), this.d)};
                        String format = String.format(a4, Arrays.copyOf(objArr, objArr.length));
                        wd4.a((Object) format, "java.lang.String.format(format, *args)");
                        sb.append(format);
                        sb.append(" ");
                        sb.append(PortfolioApp.W.c().getString(R.string.General_Measurement_Abbreviations_Miles__Mi));
                        str = sb.toString();
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        be4 be42 = be4.a;
                        String a5 = tm2.a((Context) this.e, (int) R.string.DashboardDiana_Main_StepsToday_Text__EstNumberUnit);
                        wd4.a((Object) a5, "LanguageHelper.getString\u2026oday_Text__EstNumberUnit)");
                        Object[] objArr2 = {pl2.a.a(Float.valueOf((float) activitySummary.getDistance()), this.d)};
                        String format2 = String.format(a5, Arrays.copyOf(objArr2, objArr2.length));
                        wd4.a((Object) format2, "java.lang.String.format(format, *args)");
                        sb2.append(format2);
                        sb2.append(" ");
                        sb2.append(PortfolioApp.W.c().getString(R.string.General_Measurement_Abbreviations_Kilometers__Km));
                        str = sb2.toString();
                    }
                    bVar.a(str);
                    if (activitySummary.getStepGoal() > 0) {
                        if (steps >= ((double) activitySummary.getStepGoal())) {
                            z = true;
                        }
                        bVar.b(z);
                    } else {
                        bVar.b(false);
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                String a6 = tm2.a((Context) this.e, (int) R.string.DashboardDiana_Steps_DetailPageNoRecord_Text__NoRecord);
                wd4.a((Object) a6, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                bVar.b(a6);
                bVar.a(true);
            }
        }
        return bVar;
    }

    @DexIgnore
    public final d b(ActivitySummary activitySummary) {
        String str;
        d dVar = new d((Date) null, (Date) null, (String) null, (String) null, 15, (rd4) null);
        if (activitySummary != null) {
            Calendar instance = Calendar.getInstance();
            instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            wd4.a((Object) instance, "calendar");
            Boolean s = sk2.s(instance.getTime());
            int i = instance.get(5);
            int i2 = instance.get(2);
            String b2 = sk2.b(i2);
            int i3 = instance.get(1);
            dVar.a(instance.getTime());
            instance.add(5, -6);
            int i4 = instance.get(5);
            int i5 = instance.get(2);
            String b3 = sk2.b(i5);
            int i6 = instance.get(1);
            dVar.b(instance.getTime());
            wd4.a((Object) s, "isToday");
            if (s.booleanValue()) {
                str = tm2.a((Context) this.e, (int) R.string.DashboardDiana_Main_StepsToday_Title__ThisWeek);
                wd4.a((Object) str, "LanguageHelper.getString\u2026epsToday_Title__ThisWeek)");
            } else if (i2 == i5) {
                str = b3 + ' ' + i4 + " - " + b3 + ' ' + i;
            } else if (i6 == i3) {
                str = b3 + ' ' + i4 + " - " + b2 + ' ' + i;
            } else {
                str = b3 + ' ' + i4 + ", " + i6 + " - " + b2 + ' ' + i + ", " + i3;
            }
            dVar.a(str);
            be4 be4 = be4.a;
            String a2 = tm2.a((Context) this.e, (int) R.string.DashboardDiana_Main_StepsToday_Text__NumberSteps);
            wd4.a((Object) a2, "LanguageHelper.getString\u2026sToday_Text__NumberSteps)");
            Object[] objArr = new Object[1];
            pl2 pl2 = pl2.a;
            ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = activitySummary.getTotalValuesOfWeek();
            objArr[0] = pl2.b(totalValuesOfWeek != null ? Integer.valueOf((int) totalValuesOfWeek.getTotalStepsOfWeek()) : null);
            String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            dVar.b(format);
        }
        return dVar;
    }

    @DexIgnore
    public final void a(Unit unit, int i, int i2) {
        wd4.b(unit, MFUser.DISTANCE_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActivitiesAdapter", "updateUserUnits - old=" + this.d + ", new=" + unit);
        this.d = unit;
        if (getItemCount() > 0 && i2 >= 0 && i >= 0) {
            int max = Math.max(0, i - 5);
            notifyItemRangeChanged(max, Math.min(getItemCount(), i2 + 5) - max);
        }
    }
}
