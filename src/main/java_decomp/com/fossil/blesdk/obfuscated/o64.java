package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class o64 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ k64 b;
    @DexIgnore
    public /* final */ n64 c;

    @DexIgnore
    public o64(k64 k64, n64 n64) {
        this(0, k64, n64);
    }

    @DexIgnore
    public long a() {
        return this.b.a(this.a);
    }

    @DexIgnore
    public o64 b() {
        return new o64(this.b, this.c);
    }

    @DexIgnore
    public o64 c() {
        return new o64(this.a + 1, this.b, this.c);
    }

    @DexIgnore
    public o64(int i, k64 k64, n64 n64) {
        this.a = i;
        this.b = k64;
        this.c = n64;
    }
}
