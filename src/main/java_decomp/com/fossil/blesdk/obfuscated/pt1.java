package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface pt1<F, T> {
    @DexIgnore
    T apply(F f);

    @DexIgnore
    boolean equals(Object obj);
}
