package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.he0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cg0 implements he0.b, he0.c {
    @DexIgnore
    public /* final */ /* synthetic */ tf0 e;

    @DexIgnore
    public cg0(tf0 tf0) {
        this.e = tf0;
    }

    @DexIgnore
    public final void a(vd0 vd0) {
        this.e.b.lock();
        try {
            if (this.e.a(vd0)) {
                this.e.g();
                this.e.e();
            } else {
                this.e.b(vd0);
            }
        } finally {
            this.e.b.unlock();
        }
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        if (this.e.r.k()) {
            this.e.b.lock();
            try {
                if (this.e.k != null) {
                    this.e.k.a(new ag0(this.e));
                    this.e.b.unlock();
                }
            } finally {
                this.e.b.unlock();
            }
        } else {
            this.e.k.a(new ag0(this.e));
        }
    }

    @DexIgnore
    public final void f(int i) {
    }

    @DexIgnore
    public /* synthetic */ cg0(tf0 tf0, uf0 uf0) {
        this(tf0);
    }
}
