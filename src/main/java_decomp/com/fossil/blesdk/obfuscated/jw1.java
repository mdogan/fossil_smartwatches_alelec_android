package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.Log;
import com.google.firebase.components.DependencyCycleException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jw1<T> {
    @DexIgnore
    public /* final */ Set<Class<? super T>> a;
    @DexIgnore
    public /* final */ Set<nw1> b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ lw1<T> d;
    @DexIgnore
    public /* final */ Set<Class<?>> e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a<T> {
        @DexIgnore
        public /* final */ T a;
        @DexIgnore
        public /* final */ rw1<T> b;

        @DexIgnore
        public a(T t, rw1<T> rw1) {
            this.a = t;
            this.b = rw1;
        }

        @DexIgnore
        public static a<Context> a(Context context) {
            return new a<>(context, new qw1((byte) 0));
        }

        @DexIgnore
        public static List<mw1> b(List<String> list) {
            ArrayList arrayList = new ArrayList();
            for (String next : list) {
                try {
                    Class<?> cls = Class.forName(next);
                    if (!mw1.class.isAssignableFrom(cls)) {
                        Log.w("ComponentDiscovery", String.format("Class %s is not an instance of %s", new Object[]{next, "com.google.firebase.components.ComponentRegistrar"}));
                    } else {
                        arrayList.add((mw1) cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
                    }
                } catch (ClassNotFoundException e) {
                    Log.w("ComponentDiscovery", String.format("Class %s is not an found.", new Object[]{next}), e);
                } catch (IllegalAccessException e2) {
                    Log.w("ComponentDiscovery", String.format("Could not instantiate %s.", new Object[]{next}), e2);
                } catch (InstantiationException e3) {
                    Log.w("ComponentDiscovery", String.format("Could not instantiate %s.", new Object[]{next}), e3);
                } catch (NoSuchMethodException e4) {
                    Log.w("ComponentDiscovery", String.format("Could not instantiate %s", new Object[]{next}), e4);
                } catch (InvocationTargetException e5) {
                    Log.w("ComponentDiscovery", String.format("Could not instantiate %s", new Object[]{next}), e5);
                }
            }
            return arrayList;
        }

        @DexIgnore
        public List<mw1> a() {
            return b(this.b.zza(this.a));
        }

        @DexIgnore
        public static List<jw1<?>> a(List<jw1<?>> list) {
            HashMap hashMap = new HashMap(list.size());
            for (jw1 next : list) {
                tw1 tw1 = new tw1(next);
                Iterator it = next.a().iterator();
                while (true) {
                    if (it.hasNext()) {
                        Class cls = (Class) it.next();
                        if (hashMap.put(cls, tw1) != null) {
                            throw new IllegalArgumentException(String.format("Multiple components provide %s.", new Object[]{cls}));
                        }
                    }
                }
            }
            for (tw1 tw12 : hashMap.values()) {
                for (nw1 next2 : tw12.b().b()) {
                    if (next2.c()) {
                        tw1 tw13 = (tw1) hashMap.get(next2.a());
                        if (tw13 != null) {
                            tw12.a(tw13);
                            tw13.b(tw12);
                        }
                    }
                }
            }
            HashSet<tw1> hashSet = new HashSet<>(hashMap.values());
            Set<tw1> a2 = a((Set<tw1>) hashSet);
            ArrayList arrayList = new ArrayList();
            while (!a2.isEmpty()) {
                tw1 next3 = a2.iterator().next();
                a2.remove(next3);
                arrayList.add(next3.b());
                for (tw1 next4 : next3.a()) {
                    next4.c(next3);
                    if (next4.c()) {
                        a2.add(next4);
                    }
                }
            }
            if (arrayList.size() == list.size()) {
                Collections.reverse(arrayList);
                return arrayList;
            }
            ArrayList arrayList2 = new ArrayList();
            for (tw1 tw14 : hashSet) {
                if (!tw14.c() && !tw14.d()) {
                    arrayList2.add(tw14.b());
                }
            }
            throw new DependencyCycleException(arrayList2);
        }

        @DexIgnore
        public static Set<tw1> a(Set<tw1> set) {
            HashSet hashSet = new HashSet();
            for (tw1 next : set) {
                if (next.c()) {
                    hashSet.add(next);
                }
            }
            return hashSet;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T> {
        @DexIgnore
        public /* final */ Set<Class<? super T>> a;
        @DexIgnore
        public /* final */ Set<nw1> b;
        @DexIgnore
        public int c;
        @DexIgnore
        public lw1<T> d;
        @DexIgnore
        public Set<Class<?>> e;

        @DexIgnore
        public /* synthetic */ b(Class cls, Class[] clsArr, byte b2) {
            this(cls, clsArr);
        }

        @DexIgnore
        public b<T> a(nw1 nw1) {
            ck0.a(nw1, (Object) "Null dependency");
            ck0.a(!this.a.contains(nw1.a()), (Object) "Components are not allowed to depend on interfaces they themselves provide.");
            this.b.add(nw1);
            return this;
        }

        @DexIgnore
        public jw1<T> b() {
            ck0.b(this.d != null, "Missing required property: factory.");
            return new jw1(new HashSet(this.a), new HashSet(this.b), this.c, this.d, this.e, (byte) 0);
        }

        @DexIgnore
        public b<T> c() {
            a(2);
            return this;
        }

        @DexIgnore
        public b(Class<T> cls, Class<? super T>... clsArr) {
            this.a = new HashSet();
            this.b = new HashSet();
            this.c = 0;
            this.e = new HashSet();
            ck0.a(cls, (Object) "Null interface");
            this.a.add(cls);
            for (Class<? super T> a2 : clsArr) {
                ck0.a(a2, (Object) "Null interface");
            }
            Collections.addAll(this.a, clsArr);
        }

        @DexIgnore
        public b<T> a() {
            a(1);
            return this;
        }

        @DexIgnore
        public final b<T> a(int i) {
            ck0.b(this.c == 0, "Instantiation type has already been set.");
            this.c = i;
            return this;
        }

        @DexIgnore
        public b<T> a(lw1<T> lw1) {
            ck0.a(lw1, (Object) "Null factory");
            this.d = lw1;
            return this;
        }
    }

    @DexIgnore
    public /* synthetic */ jw1(Set set, Set set2, int i, lw1 lw1, Set set3, byte b2) {
        this(set, set2, i, lw1, set3);
    }

    @DexIgnore
    public static /* synthetic */ Object a(Object obj) {
        return obj;
    }

    @DexIgnore
    public final Set<Class<? super T>> a() {
        return this.a;
    }

    @DexIgnore
    public final Set<nw1> b() {
        return this.b;
    }

    @DexIgnore
    public final lw1<T> c() {
        return this.d;
    }

    @DexIgnore
    public final Set<Class<?>> d() {
        return this.e;
    }

    @DexIgnore
    public final boolean e() {
        return this.c == 1;
    }

    @DexIgnore
    public final boolean f() {
        return this.c == 2;
    }

    @DexIgnore
    public final String toString() {
        return "Component<" + Arrays.toString(this.a.toArray()) + ">{" + this.c + ", deps=" + Arrays.toString(this.b.toArray()) + "}";
    }

    @DexIgnore
    public jw1(Set<Class<? super T>> set, Set<nw1> set2, int i, lw1<T> lw1, Set<Class<?>> set3) {
        this.a = Collections.unmodifiableSet(set);
        this.b = Collections.unmodifiableSet(set2);
        this.c = i;
        this.d = lw1;
        this.e = Collections.unmodifiableSet(set3);
    }

    @DexIgnore
    public static <T> b<T> a(Class<T> cls) {
        return new b<>(cls, new Class[0], (byte) 0);
    }

    @DexIgnore
    public static <T> b<T> a(Class<T> cls, Class<? super T>... clsArr) {
        return new b<>(cls, clsArr, (byte) 0);
    }

    @DexIgnore
    @SafeVarargs
    public static <T> jw1<T> a(T t, Class<T> cls, Class<? super T>... clsArr) {
        b<T> a2 = a(cls, clsArr);
        a2.a((lw1<T>) pw1.a((Object) t));
        return a2.b();
    }
}
