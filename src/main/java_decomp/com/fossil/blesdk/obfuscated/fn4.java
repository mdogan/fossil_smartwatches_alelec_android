package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.pm4;
import com.fossil.blesdk.obfuscated.zn4;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownServiceException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.internal.connection.RouteException;
import okhttp3.internal.http2.ErrorCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fn4 extends zn4.h implements zl4 {
    @DexIgnore
    public /* final */ am4 b;
    @DexIgnore
    public /* final */ rm4 c;
    @DexIgnore
    public Socket d;
    @DexIgnore
    public Socket e;
    @DexIgnore
    public jm4 f;
    @DexIgnore
    public Protocol g;
    @DexIgnore
    public zn4 h;
    @DexIgnore
    public xo4 i;
    @DexIgnore
    public wo4 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m; // = 1;
    @DexIgnore
    public /* final */ List<Reference<in4>> n; // = new ArrayList();
    @DexIgnore
    public long o; // = ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;

    @DexIgnore
    public fn4(am4 am4, rm4 rm4) {
        this.b = am4;
        this.c = rm4;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x013b  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0143 A[EDGE_INSN: B:63:0x0143->B:56:0x0143 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    public void a(int i2, int i3, int i4, int i5, boolean z, vl4 vl4, hm4 hm4) {
        vl4 vl42 = vl4;
        hm4 hm42 = hm4;
        if (this.g == null) {
            List<bm4> b2 = this.c.a().b();
            en4 en4 = new en4(b2);
            if (this.c.a().j() == null) {
                if (b2.contains(bm4.h)) {
                    String g2 = this.c.a().k().g();
                    if (!mo4.d().b(g2)) {
                        throw new RouteException(new UnknownServiceException("CLEARTEXT communication to " + g2 + " not permitted by network security policy"));
                    }
                } else {
                    throw new RouteException(new UnknownServiceException("CLEARTEXT communication not enabled for client"));
                }
            } else if (this.c.a().e().contains(Protocol.H2_PRIOR_KNOWLEDGE)) {
                throw new RouteException(new UnknownServiceException("H2_PRIOR_KNOWLEDGE cannot be used with HTTPS"));
            }
            RouteException routeException = null;
            do {
                try {
                    if (this.c.c()) {
                        a(i2, i3, i4, vl4, hm4);
                        if (this.d != null) {
                            int i6 = i2;
                            int i7 = i3;
                        } else if (!this.c.c() && this.d == null) {
                            throw new RouteException(new ProtocolException("Too many tunnel connections attempted: 21"));
                        } else if (this.h == null) {
                            synchronized (this.b) {
                                this.m = this.h.A();
                            }
                            return;
                        } else {
                            return;
                        }
                    } else {
                        try {
                            a(i2, i3, vl42, hm42);
                        } catch (IOException e2) {
                            e = e2;
                            int i8 = i5;
                            vm4.a(this.e);
                            vm4.a(this.d);
                            this.e = null;
                            this.d = null;
                            this.i = null;
                            this.j = null;
                            this.f = null;
                            this.g = null;
                            this.h = null;
                            hm4.a(vl4, this.c.d(), this.c.b(), (Protocol) null, e);
                            if (routeException != null) {
                            }
                            if (z) {
                            }
                            throw routeException;
                        }
                    }
                    try {
                        a(en4, i5, vl42, hm42);
                        hm42.a(vl42, this.c.d(), this.c.b(), this.g);
                        if (!this.c.c()) {
                        }
                        if (this.h == null) {
                        }
                    } catch (IOException e3) {
                        e = e3;
                    }
                } catch (IOException e4) {
                    e = e4;
                    int i9 = i2;
                    int i10 = i3;
                    int i82 = i5;
                    vm4.a(this.e);
                    vm4.a(this.d);
                    this.e = null;
                    this.d = null;
                    this.i = null;
                    this.j = null;
                    this.f = null;
                    this.g = null;
                    this.h = null;
                    hm4.a(vl4, this.c.d(), this.c.b(), (Protocol) null, e);
                    if (routeException != null) {
                        routeException = new RouteException(e);
                    } else {
                        routeException.addConnectException(e);
                    }
                    if (z) {
                        break;
                    } else if (!en4.a(e)) {
                    }
                    throw routeException;
                }
            } while (!en4.a(e));
            throw routeException;
        }
        throw new IllegalStateException("already connected");
    }

    @DexIgnore
    public void b() {
        vm4.a(this.d);
    }

    @DexIgnore
    public final pm4 c() throws IOException {
        pm4.a aVar = new pm4.a();
        aVar.a(this.c.a().k());
        aVar.a("CONNECT", (RequestBody) null);
        aVar.b("Host", vm4.a(this.c.a().k(), true));
        aVar.b("Proxy-Connection", "Keep-Alive");
        aVar.b("User-Agent", wm4.a());
        pm4 a = aVar.a();
        Response.a aVar2 = new Response.a();
        aVar2.a(a);
        aVar2.a(Protocol.HTTP_1_1);
        aVar2.a(407);
        aVar2.a("Preemptive Authenticate");
        aVar2.a(vm4.c);
        aVar2.b(-1);
        aVar2.a(-1);
        aVar2.b("Proxy-Authenticate", "OkHttp-Preemptive");
        pm4 authenticate = this.c.a().g().authenticate(this.c, aVar2.a());
        return authenticate != null ? authenticate : a;
    }

    @DexIgnore
    public jm4 d() {
        return this.f;
    }

    @DexIgnore
    public boolean e() {
        return this.h != null;
    }

    @DexIgnore
    public rm4 f() {
        return this.c;
    }

    @DexIgnore
    public Socket g() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Connection{");
        sb.append(this.c.a().k().g());
        sb.append(":");
        sb.append(this.c.a().k().k());
        sb.append(", proxy=");
        sb.append(this.c.b());
        sb.append(" hostAddress=");
        sb.append(this.c.d());
        sb.append(" cipherSuite=");
        jm4 jm4 = this.f;
        sb.append(jm4 != null ? jm4.a() : "none");
        sb.append(" protocol=");
        sb.append(this.g);
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, vl4 vl4, hm4 hm4) throws IOException {
        pm4 c2 = c();
        lm4 g2 = c2.g();
        int i5 = 0;
        while (i5 < 21) {
            a(i2, i3, vl4, hm4);
            c2 = a(i3, i4, c2, g2);
            if (c2 != null) {
                vm4.a(this.d);
                this.d = null;
                this.j = null;
                this.i = null;
                hm4.a(vl4, this.c.d(), this.c.b(), (Protocol) null);
                i5++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final void a(int i2, int i3, vl4 vl4, hm4 hm4) throws IOException {
        Socket socket;
        Proxy b2 = this.c.b();
        sl4 a = this.c.a();
        if (b2.type() == Proxy.Type.DIRECT || b2.type() == Proxy.Type.HTTP) {
            socket = a.i().createSocket();
        } else {
            socket = new Socket(b2);
        }
        this.d = socket;
        hm4.a(vl4, this.c.d(), b2);
        this.d.setSoTimeout(i3);
        try {
            mo4.d().a(this.d, this.c.d(), i2);
            try {
                this.i = ep4.a(ep4.b(this.d));
                this.j = ep4.a(ep4.a(this.d));
            } catch (NullPointerException e2) {
                if ("throw with null exception".equals(e2.getMessage())) {
                    throw new IOException(e2);
                }
            }
        } catch (ConnectException e3) {
            ConnectException connectException = new ConnectException("Failed to connect to " + this.c.d());
            connectException.initCause(e3);
            throw connectException;
        }
    }

    @DexIgnore
    public final void a(en4 en4, int i2, vl4 vl4, hm4 hm4) throws IOException {
        if (this.c.a().j() != null) {
            hm4.g(vl4);
            a(en4);
            hm4.a(vl4, this.f);
            if (this.g == Protocol.HTTP_2) {
                a(i2);
            }
        } else if (this.c.a().e().contains(Protocol.H2_PRIOR_KNOWLEDGE)) {
            this.e = this.d;
            this.g = Protocol.H2_PRIOR_KNOWLEDGE;
            a(i2);
        } else {
            this.e = this.d;
            this.g = Protocol.HTTP_1_1;
        }
    }

    @DexIgnore
    public final void a(int i2) throws IOException {
        this.e.setSoTimeout(0);
        zn4.g gVar = new zn4.g(true);
        gVar.a(this.e, this.c.a().k().g(), this.i, this.j);
        gVar.a((zn4.h) this);
        gVar.a(i2);
        this.h = gVar.a();
        this.h.B();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: java.lang.String} */
    /* JADX WARNING: type inference failed for: r1v1, types: [java.net.Socket, javax.net.ssl.SSLSocket] */
    /* JADX WARNING: type inference failed for: r1v2 */
    /* JADX WARNING: type inference failed for: r1v5 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0111 A[Catch:{ all -> 0x0107 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0117 A[Catch:{ all -> 0x0107 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x011a  */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final void a(en4 en4) throws IOException {
        Object r1;
        Protocol protocol;
        sl4 a = this.c.a();
        String str = null;
        try {
            SSLSocket sSLSocket = (SSLSocket) a.j().createSocket(this.d, a.k().g(), a.k().k(), true);
            try {
                bm4 a2 = en4.a(sSLSocket);
                if (a2.c()) {
                    mo4.d().a(sSLSocket, a.k().g(), a.e());
                }
                sSLSocket.startHandshake();
                SSLSession session = sSLSocket.getSession();
                jm4 a3 = jm4.a(session);
                if (a.d().verify(a.k().g(), session)) {
                    a.a().a(a.k().g(), a3.c());
                    if (a2.c()) {
                        str = mo4.d().b(sSLSocket);
                    }
                    this.e = sSLSocket;
                    this.i = ep4.a(ep4.b(this.e));
                    this.j = ep4.a(ep4.a(this.e));
                    this.f = a3;
                    if (str != null) {
                        protocol = Protocol.get(str);
                    } else {
                        protocol = Protocol.HTTP_1_1;
                    }
                    this.g = protocol;
                    if (sSLSocket != 0) {
                        mo4.d().a(sSLSocket);
                        return;
                    }
                    return;
                }
                X509Certificate x509Certificate = (X509Certificate) a3.c().get(0);
                throw new SSLPeerUnverifiedException("Hostname " + a.k().g() + " not verified:\n    certificate: " + xl4.a((Certificate) x509Certificate) + "\n    DN: " + x509Certificate.getSubjectDN().getName() + "\n    subjectAltNames: " + ro4.a(x509Certificate));
            } catch (AssertionError e2) {
                e = e2;
                str = sSLSocket;
                try {
                    if (!vm4.a(e)) {
                    }
                } catch (Throwable th) {
                    th = th;
                    r1 = str;
                    if (r1 != 0) {
                        mo4.d().a((SSLSocket) r1);
                    }
                    vm4.a((Socket) r1);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                r1 = sSLSocket;
                if (r1 != 0) {
                }
                vm4.a((Socket) r1);
                throw th;
            }
        } catch (AssertionError e3) {
            e = e3;
            if (!vm4.a(e)) {
                throw new IOException(e);
            }
            throw e;
        }
    }

    @DexIgnore
    public final pm4 a(int i2, int i3, pm4 pm4, lm4 lm4) throws IOException {
        String str = "CONNECT " + vm4.a(lm4, true) + " HTTP/1.1";
        while (true) {
            un4 un4 = new un4((OkHttpClient) null, (in4) null, this.i, this.j);
            this.i.b().a((long) i2, TimeUnit.MILLISECONDS);
            this.j.b().a((long) i3, TimeUnit.MILLISECONDS);
            un4.a(pm4.c(), str);
            un4.a();
            Response.a a = un4.a(false);
            a.a(pm4);
            Response a2 = a.a();
            long a3 = nn4.a(a2);
            if (a3 == -1) {
                a3 = 0;
            }
            kp4 b2 = un4.b(a3);
            vm4.b(b2, Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
            b2.close();
            int B = a2.B();
            if (B != 200) {
                if (B == 407) {
                    pm4 authenticate = this.c.a().g().authenticate(this.c, a2);
                    if (authenticate == null) {
                        throw new IOException("Failed to authenticate with proxy");
                    } else if ("close".equalsIgnoreCase(a2.e("Connection"))) {
                        return authenticate;
                    } else {
                        pm4 = authenticate;
                    }
                } else {
                    throw new IOException("Unexpected response code for CONNECT: " + a2.B());
                }
            } else if (this.i.a().g() && this.j.a().g()) {
                return null;
            } else {
                throw new IOException("TLS tunnel buffered too many bytes!");
            }
        }
    }

    @DexIgnore
    public boolean a(sl4 sl4, rm4 rm4) {
        if (this.n.size() >= this.m || this.k || !tm4.a.a(this.c.a(), sl4)) {
            return false;
        }
        if (sl4.k().g().equals(f().a().k().g())) {
            return true;
        }
        if (this.h == null || rm4 == null || rm4.b().type() != Proxy.Type.DIRECT || this.c.b().type() != Proxy.Type.DIRECT || !this.c.d().equals(rm4.d()) || rm4.a().d() != ro4.a || !a(sl4.k())) {
            return false;
        }
        try {
            sl4.a().a(sl4.k().g(), d().c());
            return true;
        } catch (SSLPeerUnverifiedException unused) {
            return false;
        }
    }

    @DexIgnore
    public boolean a(lm4 lm4) {
        if (lm4.k() != this.c.a().k().k()) {
            return false;
        }
        if (lm4.g().equals(this.c.a().k().g())) {
            return true;
        }
        if (this.f == null || !ro4.a.a(lm4.g(), (X509Certificate) this.f.c().get(0))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public ln4 a(OkHttpClient okHttpClient, Interceptor.Chain chain, in4 in4) throws SocketException {
        zn4 zn4 = this.h;
        if (zn4 != null) {
            return new yn4(okHttpClient, chain, in4, zn4);
        }
        this.e.setSoTimeout(chain.a());
        this.i.b().a((long) chain.a(), TimeUnit.MILLISECONDS);
        this.j.b().a((long) chain.b(), TimeUnit.MILLISECONDS);
        return new un4(okHttpClient, in4, this.i, this.j);
    }

    @DexIgnore
    public boolean a(boolean z) {
        int soTimeout;
        if (this.e.isClosed() || this.e.isInputShutdown() || this.e.isOutputShutdown()) {
            return false;
        }
        zn4 zn4 = this.h;
        if (zn4 != null) {
            return !zn4.z();
        }
        if (z) {
            try {
                soTimeout = this.e.getSoTimeout();
                this.e.setSoTimeout(1);
                if (this.i.g()) {
                    this.e.setSoTimeout(soTimeout);
                    return false;
                }
                this.e.setSoTimeout(soTimeout);
                return true;
            } catch (SocketTimeoutException unused) {
            } catch (IOException unused2) {
                return false;
            } catch (Throwable th) {
                this.e.setSoTimeout(soTimeout);
                throw th;
            }
        }
        return true;
    }

    @DexIgnore
    public void a(bo4 bo4) throws IOException {
        bo4.a(ErrorCode.REFUSED_STREAM);
    }

    @DexIgnore
    public void a(zn4 zn4) {
        synchronized (this.b) {
            this.m = zn4.A();
        }
    }

    @DexIgnore
    public Protocol a() {
        return this.g;
    }
}
