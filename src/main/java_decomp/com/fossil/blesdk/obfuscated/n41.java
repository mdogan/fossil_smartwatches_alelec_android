package com.fossil.blesdk.obfuscated;

import android.os.DeadObjectException;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface n41<T extends IInterface> {
    @DexIgnore
    void a();

    @DexIgnore
    T b() throws DeadObjectException;
}
