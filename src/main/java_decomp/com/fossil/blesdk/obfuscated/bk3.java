package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.d6;
import com.fossil.blesdk.obfuscated.il2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import java.util.List;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bk3 {
    @DexIgnore
    public b a;
    @DexIgnore
    public /* final */ InAppNotificationRepository b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(InAppNotification inAppNotification);
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public bk3(InAppNotificationRepository inAppNotificationRepository) {
        wd4.b(inAppNotificationRepository, "repository");
        this.b = inAppNotificationRepository;
    }

    @DexIgnore
    public final void a(b bVar) {
        wd4.b(bVar, "listener");
        this.a = bVar;
    }

    @DexIgnore
    public final void b(InAppNotification inAppNotification) {
        FLogger.INSTANCE.getLocal().d("InAppNotificationManager", "sendNotification()");
        PortfolioApp c = PortfolioApp.W.c();
        PendingIntent activity = PendingIntent.getActivity(c, 0, new Intent(c, SplashScreenActivity.class), 134217728);
        il2.a aVar = il2.a;
        String title = inAppNotification.getTitle();
        String content = inAppNotification.getContent();
        wd4.a((Object) activity, "pendingIntent");
        aVar.a(c, 1, title, content, activity, (List<? extends d6.a>) null);
    }

    @DexIgnore
    public final void a(String str, String str2) {
        wd4.b(str, "title");
        wd4.b(str2, "messageBody");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("InAppNotificationManager", "handleNewNotification() - title = " + str + " - messageBody = " + str2);
        String uuid = UUID.randomUUID().toString();
        wd4.a((Object) uuid, "UUID.randomUUID().toString()");
        InAppNotification inAppNotification = new InAppNotification(uuid, str, str2);
        this.b.handleReceivingNotification(inAppNotification);
        if (!PortfolioApp.W.c().z()) {
            a(inAppNotification);
            b(inAppNotification);
            return;
        }
        b bVar = this.a;
        if (bVar != null) {
            bVar.a(inAppNotification);
        }
    }

    @DexIgnore
    public final void a(InAppNotification inAppNotification) {
        wd4.b(inAppNotification, "inAppNotification");
        this.b.removeNotificationAfterSending(inAppNotification);
    }

    @DexIgnore
    public final void a() {
        this.a = null;
    }
}
