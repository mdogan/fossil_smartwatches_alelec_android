package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.nm4;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Map;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class yr4<T> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends yr4<Iterable<T>> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(as4 as4, Iterable<T> iterable) throws IOException {
            if (iterable != null) {
                for (T a2 : iterable) {
                    yr4.this.a(as4, a2);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends yr4<Object> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(as4 as4, Object obj) throws IOException {
            if (obj != null) {
                int length = Array.getLength(obj);
                for (int i = 0; i < length; i++) {
                    yr4.this.a(as4, Array.get(obj, i));
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> extends yr4<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ sr4<T, RequestBody> c;

        @DexIgnore
        public c(Method method, int i, sr4<T, RequestBody> sr4) {
            this.a = method;
            this.b = i;
            this.c = sr4;
        }

        @DexIgnore
        public void a(as4 as4, T t) {
            if (t != null) {
                try {
                    as4.a(this.c.a(t));
                } catch (IOException e) {
                    Method method = this.a;
                    int i = this.b;
                    throw gs4.a(method, e, i, "Unable to convert " + t + " to RequestBody", new Object[0]);
                }
            } else {
                throw gs4.a(this.a, this.b, "Body parameter value must not be null.", new Object[0]);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> extends yr4<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ sr4<T, String> b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public d(String str, sr4<T, String> sr4, boolean z) {
            gs4.a(str, "name == null");
            this.a = str;
            this.b = sr4;
            this.c = z;
        }

        @DexIgnore
        public void a(as4 as4, T t) throws IOException {
            if (t != null) {
                String a2 = this.b.a(t);
                if (a2 != null) {
                    as4.a(this.a, a2, this.c);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> extends yr4<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ sr4<T, String> c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public e(Method method, int i, sr4<T, String> sr4, boolean z) {
            this.a = method;
            this.b = i;
            this.c = sr4;
            this.d = z;
        }

        @DexIgnore
        public void a(as4 as4, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry next : map.entrySet()) {
                    String str = (String) next.getKey();
                    if (str != null) {
                        Object value = next.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                as4.a(str, a2, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.b;
                                throw gs4.a(method, i, "Field map value '" + value + "' converted to null by " + this.c.getClass().getName() + " for key '" + str + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.a;
                            int i2 = this.b;
                            throw gs4.a(method2, i2, "Field map contained null value for key '" + str + "'.", new Object[0]);
                        }
                    } else {
                        throw gs4.a(this.a, this.b, "Field map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw gs4.a(this.a, this.b, "Field map was null.", new Object[0]);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> extends yr4<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ sr4<T, String> b;

        @DexIgnore
        public f(String str, sr4<T, String> sr4) {
            gs4.a(str, "name == null");
            this.a = str;
            this.b = sr4;
        }

        @DexIgnore
        public void a(as4 as4, T t) throws IOException {
            if (t != null) {
                String a2 = this.b.a(t);
                if (a2 != null) {
                    as4.a(this.a, a2);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> extends yr4<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ km4 c;
        @DexIgnore
        public /* final */ sr4<T, RequestBody> d;

        @DexIgnore
        public g(Method method, int i, km4 km4, sr4<T, RequestBody> sr4) {
            this.a = method;
            this.b = i;
            this.c = km4;
            this.d = sr4;
        }

        @DexIgnore
        public void a(as4 as4, T t) {
            if (t != null) {
                try {
                    as4.a(this.c, this.d.a(t));
                } catch (IOException e) {
                    Method method = this.a;
                    int i = this.b;
                    throw gs4.a(method, i, "Unable to convert " + t + " to RequestBody", e);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> extends yr4<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ sr4<T, RequestBody> c;
        @DexIgnore
        public /* final */ String d;

        @DexIgnore
        public h(Method method, int i, sr4<T, RequestBody> sr4, String str) {
            this.a = method;
            this.b = i;
            this.c = sr4;
            this.d = str;
        }

        @DexIgnore
        public void a(as4 as4, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry next : map.entrySet()) {
                    String str = (String) next.getKey();
                    if (str != null) {
                        Object value = next.getValue();
                        if (value != null) {
                            as4.a(km4.a("Content-Disposition", "form-data; name=\"" + str + "\"", "Content-Transfer-Encoding", this.d), this.c.a(value));
                        } else {
                            Method method = this.a;
                            int i = this.b;
                            throw gs4.a(method, i, "Part map contained null value for key '" + str + "'.", new Object[0]);
                        }
                    } else {
                        throw gs4.a(this.a, this.b, "Part map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw gs4.a(this.a, this.b, "Part map was null.", new Object[0]);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> extends yr4<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ sr4<T, String> d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public i(Method method, int i, String str, sr4<T, String> sr4, boolean z) {
            this.a = method;
            this.b = i;
            gs4.a(str, "name == null");
            this.c = str;
            this.d = sr4;
            this.e = z;
        }

        @DexIgnore
        public void a(as4 as4, T t) throws IOException {
            if (t != null) {
                as4.b(this.c, this.d.a(t), this.e);
                return;
            }
            Method method = this.a;
            int i = this.b;
            throw gs4.a(method, i, "Path parameter \"" + this.c + "\" value must not be null.", new Object[0]);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> extends yr4<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ sr4<T, String> b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public j(String str, sr4<T, String> sr4, boolean z) {
            gs4.a(str, "name == null");
            this.a = str;
            this.b = sr4;
            this.c = z;
        }

        @DexIgnore
        public void a(as4 as4, T t) throws IOException {
            if (t != null) {
                String a2 = this.b.a(t);
                if (a2 != null) {
                    as4.c(this.a, a2, this.c);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> extends yr4<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ sr4<T, String> c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public k(Method method, int i, sr4<T, String> sr4, boolean z) {
            this.a = method;
            this.b = i;
            this.c = sr4;
            this.d = z;
        }

        @DexIgnore
        public void a(as4 as4, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry next : map.entrySet()) {
                    String str = (String) next.getKey();
                    if (str != null) {
                        Object value = next.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                as4.c(str, a2, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.b;
                                throw gs4.a(method, i, "Query map value '" + value + "' converted to null by " + this.c.getClass().getName() + " for key '" + str + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.a;
                            int i2 = this.b;
                            throw gs4.a(method2, i2, "Query map contained null value for key '" + str + "'.", new Object[0]);
                        }
                    } else {
                        throw gs4.a(this.a, this.b, "Query map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw gs4.a(this.a, this.b, "Query map was null", new Object[0]);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> extends yr4<T> {
        @DexIgnore
        public /* final */ sr4<T, String> a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public l(sr4<T, String> sr4, boolean z) {
            this.a = sr4;
            this.b = z;
        }

        @DexIgnore
        public void a(as4 as4, T t) throws IOException {
            if (t != null) {
                as4.c(this.a.a(t), (String) null, this.b);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends yr4<nm4.b> {
        @DexIgnore
        public static /* final */ m a; // = new m();

        @DexIgnore
        public void a(as4 as4, nm4.b bVar) {
            if (bVar != null) {
                as4.a(bVar);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends yr4<Object> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public n(Method method, int i) {
            this.a = method;
            this.b = i;
        }

        @DexIgnore
        public void a(as4 as4, Object obj) {
            if (obj != null) {
                as4.a(obj);
                return;
            }
            throw gs4.a(this.a, this.b, "@Url parameter is null.", new Object[0]);
        }
    }

    @DexIgnore
    public final yr4<Object> a() {
        return new b();
    }

    @DexIgnore
    public abstract void a(as4 as4, T t) throws IOException;

    @DexIgnore
    public final yr4<Iterable<T>> b() {
        return new a();
    }
}
