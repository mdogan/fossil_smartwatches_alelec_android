package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class en1 extends ls0 implements dn1 {
    @DexIgnore
    public en1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.signin.internal.ISignInService");
    }

    @DexIgnore
    public final void a(uj0 uj0, int i, boolean z) throws RemoteException {
        Parcel o = o();
        ns0.a(o, (IInterface) uj0);
        o.writeInt(i);
        ns0.a(o, z);
        b(9, o);
    }

    @DexIgnore
    public final void c(int i) throws RemoteException {
        Parcel o = o();
        o.writeInt(i);
        b(7, o);
    }

    @DexIgnore
    public final void a(fn1 fn1, bn1 bn1) throws RemoteException {
        Parcel o = o();
        ns0.a(o, (Parcelable) fn1);
        ns0.a(o, (IInterface) bn1);
        b(12, o);
    }
}
