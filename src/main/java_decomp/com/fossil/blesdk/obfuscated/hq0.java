package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hq0 implements Parcelable.Creator<gq0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        DataSet dataSet = null;
        IBinder iBinder = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                dataSet = (DataSet) SafeParcelReader.a(parcel, a, DataSet.CREATOR);
            } else if (a2 == 2) {
                iBinder = SafeParcelReader.p(parcel, a);
            } else if (a2 != 4) {
                SafeParcelReader.v(parcel, a);
            } else {
                z = SafeParcelReader.i(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new gq0(dataSet, iBinder, z);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new gq0[i];
    }
}
