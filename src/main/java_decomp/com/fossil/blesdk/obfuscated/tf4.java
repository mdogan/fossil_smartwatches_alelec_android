package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface tf4 {
    @DexIgnore
    ke4 a();

    @DexIgnore
    String getValue();

    @DexIgnore
    tf4 next();
}
