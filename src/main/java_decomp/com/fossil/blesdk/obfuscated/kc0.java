package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.SignInConfiguration;
import com.google.android.gms.auth.api.signin.internal.SignInHubActivity;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kc0 {
    @DexIgnore
    public static dm0 a; // = new dm0("GoogleSignInCommon", new String[0]);

    @DexIgnore
    public static Intent a(Context context, GoogleSignInOptions googleSignInOptions) {
        a.a("getSignInIntent()", new Object[0]);
        SignInConfiguration signInConfiguration = new SignInConfiguration(context.getPackageName(), googleSignInOptions);
        Intent intent = new Intent("com.google.android.gms.auth.GOOGLE_SIGN_IN");
        intent.setPackage(context.getPackageName());
        intent.setClass(context, SignInHubActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("config", signInConfiguration);
        intent.putExtra("config", bundle);
        return intent;
    }

    @DexIgnore
    public static Intent b(Context context, GoogleSignInOptions googleSignInOptions) {
        a.a("getFallbackSignInIntent()", new Object[0]);
        Intent a2 = a(context, googleSignInOptions);
        a2.setAction("com.google.android.gms.auth.APPAUTH_SIGN_IN");
        return a2;
    }

    @DexIgnore
    public static Intent c(Context context, GoogleSignInOptions googleSignInOptions) {
        a.a("getNoImplementationSignInIntent()", new Object[0]);
        Intent a2 = a(context, googleSignInOptions);
        a2.setAction("com.google.android.gms.auth.NO_IMPL");
        return a2;
    }

    @DexIgnore
    public static ie0<Status> b(he0 he0, Context context, boolean z) {
        a.a("Revoking access", new Object[0]);
        String d = dc0.a(context).d();
        a(context);
        if (z) {
            return gc0.a(d);
        }
        return he0.b(new nc0(he0));
    }

    @DexIgnore
    public static ie0<Status> a(he0 he0, Context context, boolean z) {
        a.a("Signing out", new Object[0]);
        a(context);
        if (z) {
            return je0.a(Status.i, he0);
        }
        return he0.b(new lc0(he0));
    }

    @DexIgnore
    public static void a(Context context) {
        qc0.a(context).a();
        for (he0 h : he0.i()) {
            h.h();
        }
        we0.d();
    }

    @DexIgnore
    public static ac0 a(Intent intent) {
        if (intent == null) {
            return null;
        }
        if (!intent.hasExtra("googleSignInStatus") && !intent.hasExtra("googleSignInAccount")) {
            return null;
        }
        GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) intent.getParcelableExtra("googleSignInAccount");
        Status status = (Status) intent.getParcelableExtra("googleSignInStatus");
        if (googleSignInAccount != null) {
            status = Status.i;
        }
        return new ac0(googleSignInAccount, status);
    }
}
