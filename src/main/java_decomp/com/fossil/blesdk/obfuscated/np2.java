package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import com.portfolio.platform.usecase.GetWeather;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class np2 implements MembersInjector<ComplicationWeatherService> {
    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, GetWeather getWeather) {
        complicationWeatherService.l = getWeather;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, k62 k62) {
        complicationWeatherService.m = k62;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, fn2 fn2) {
        complicationWeatherService.n = fn2;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, PortfolioApp portfolioApp) {
        complicationWeatherService.o = portfolioApp;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, CustomizeRealDataRepository customizeRealDataRepository) {
        complicationWeatherService.p = customizeRealDataRepository;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, UserRepository userRepository) {
        complicationWeatherService.q = userRepository;
    }
}
