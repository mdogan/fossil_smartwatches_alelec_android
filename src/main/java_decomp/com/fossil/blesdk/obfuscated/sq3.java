package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sq3 extends as2 implements bp3 {
    @DexIgnore
    public static /* final */ a l; // = new a((rd4) null);
    @DexIgnore
    public ap3 j;
    @DexIgnore
    public HashMap k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final sq3 a() {
            return new sq3();
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void d0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.C;
            wd4.a((Object) activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    public void h() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.C;
            wd4.a((Object) activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wd4.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_splash_screen, viewGroup, false);
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        ap3 ap3 = this.j;
        if (ap3 != null) {
            ap3.g();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ap3 ap3 = this.j;
        if (ap3 != null) {
            ap3.f();
        } else {
            wd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void p0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WelcomeActivity.a aVar = WelcomeActivity.C;
            wd4.a((Object) activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    public void a(ap3 ap3) {
        wd4.b(ap3, "presenter");
        this.j = ap3;
    }
}
