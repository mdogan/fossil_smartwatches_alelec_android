package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ib0 {
    @DexIgnore
    public static /* final */ ib0 a; // = new ib0();

    @DexIgnore
    public final Handler a() {
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            return new Handler(myLooper);
        }
        wd4.a();
        throw null;
    }
}
