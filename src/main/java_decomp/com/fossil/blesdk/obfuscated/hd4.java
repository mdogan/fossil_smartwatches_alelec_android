package com.fossil.blesdk.obfuscated;

import com.facebook.LegacyTokenHelper;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hd4 {
    @DexIgnore
    public static final <T> Class<T> a(te4<T> te4) {
        wd4.b(te4, "$this$javaObjectType");
        Class a = ((od4) te4).a();
        if (a.isPrimitive()) {
            String name = a.getName();
            if (name != null) {
                switch (name.hashCode()) {
                    case -1325958191:
                        if (name.equals(LegacyTokenHelper.TYPE_DOUBLE)) {
                            a = Double.class;
                            break;
                        }
                        break;
                    case 104431:
                        if (name.equals(LegacyTokenHelper.TYPE_INTEGER)) {
                            a = Integer.class;
                            break;
                        }
                        break;
                    case 3039496:
                        if (name.equals(LegacyTokenHelper.TYPE_BYTE)) {
                            a = Byte.class;
                            break;
                        }
                        break;
                    case 3052374:
                        if (name.equals(LegacyTokenHelper.TYPE_CHAR)) {
                            a = Character.class;
                            break;
                        }
                        break;
                    case 3327612:
                        if (name.equals(LegacyTokenHelper.TYPE_LONG)) {
                            a = Long.class;
                            break;
                        }
                        break;
                    case 3625364:
                        if (name.equals("void")) {
                            a = Void.class;
                            break;
                        }
                        break;
                    case 64711720:
                        if (name.equals("boolean")) {
                            a = Boolean.class;
                            break;
                        }
                        break;
                    case 97526364:
                        if (name.equals(LegacyTokenHelper.TYPE_FLOAT)) {
                            a = Float.class;
                            break;
                        }
                        break;
                    case 109413500:
                        if (name.equals(LegacyTokenHelper.TYPE_SHORT)) {
                            a = Short.class;
                            break;
                        }
                        break;
                }
            }
            if (a != null) {
                return a;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.Class<T>");
        } else if (a != null) {
            return a;
        } else {
            throw new TypeCastException("null cannot be cast to non-null type java.lang.Class<T>");
        }
    }

    @DexIgnore
    public static final <T> te4<T> a(Class<T> cls) {
        wd4.b(cls, "$this$kotlin");
        return yd4.a((Class) cls);
    }

    @DexIgnore
    public static final <T> Class<T> a(T t) {
        wd4.b(t, "$this$javaClass");
        Class<?> cls = t.getClass();
        if (cls != null) {
            return cls;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.Class<T>");
    }
}
