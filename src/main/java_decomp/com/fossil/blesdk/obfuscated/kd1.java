package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kd1 implements Parcelable.Creator<jd1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        long j = -1;
        long j2 = -1;
        int i = 1;
        int i2 = 1;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 == 2) {
                i2 = SafeParcelReader.q(parcel, a);
            } else if (a2 == 3) {
                j = SafeParcelReader.s(parcel, a);
            } else if (a2 != 4) {
                SafeParcelReader.v(parcel, a);
            } else {
                j2 = SafeParcelReader.s(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new jd1(i, i2, j, j2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new jd1[i];
    }
}
