package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xs4 implements sr4<qm4, Long> {
    @DexIgnore
    public static /* final */ xs4 a; // = new xs4();

    @DexIgnore
    public Long a(qm4 qm4) throws IOException {
        return Long.valueOf(qm4.F());
    }
}
