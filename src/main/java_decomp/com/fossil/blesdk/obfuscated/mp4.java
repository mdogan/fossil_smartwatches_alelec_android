package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.notification.NotificationHandMovingConfig;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mp4 {
    @DexIgnore
    public static /* final */ Charset a; // = Charset.forName("UTF-8");

    @DexIgnore
    public static int a(int i) {
        return ((i & 255) << 24) | ((-16777216 & i) >>> 24) | ((16711680 & i) >>> 8) | ((65280 & i) << 8);
    }

    @DexIgnore
    public static short a(short s) {
        short s2 = s & NotificationHandMovingConfig.HAND_DEGREE_DEVICE_DEFAULT_POSITION;
        return (short) (((s2 & 255) << 8) | ((65280 & s2) >>> 8));
    }

    @DexIgnore
    public static void a(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException(String.format("size=%s offset=%s byteCount=%s", new Object[]{Long.valueOf(j), Long.valueOf(j2), Long.valueOf(j3)}));
        }
    }

    @DexIgnore
    public static <T extends Throwable> void b(Throwable th) throws Throwable {
        throw th;
    }

    @DexIgnore
    public static void a(Throwable th) {
        b(th);
        throw null;
    }

    @DexIgnore
    public static boolean a(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        for (int i4 = 0; i4 < i3; i4++) {
            if (bArr[i4 + i] != bArr2[i4 + i2]) {
                return false;
            }
        }
        return true;
    }
}
