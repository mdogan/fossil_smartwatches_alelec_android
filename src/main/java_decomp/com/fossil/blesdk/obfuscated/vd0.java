package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.ak0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vd0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<vd0> CREATOR; // = new dn0();
    @DexIgnore
    public static /* final */ vd0 i; // = new vd0(0);
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ PendingIntent g;
    @DexIgnore
    public /* final */ String h;

    @DexIgnore
    public vd0(int i2, int i3, PendingIntent pendingIntent, String str) {
        this.e = i2;
        this.f = i3;
        this.g = pendingIntent;
        this.h = str;
    }

    @DexIgnore
    public static String zza(int i2) {
        if (i2 == 99) {
            return "UNFINISHED";
        }
        if (i2 == 1500) {
            return "DRIVE_EXTERNAL_STORAGE_REQUIRED";
        }
        switch (i2) {
            case -1:
                return "UNKNOWN";
            case 0:
                return "SUCCESS";
            case 1:
                return "SERVICE_MISSING";
            case 2:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case 3:
                return "SERVICE_DISABLED";
            case 4:
                return "SIGN_IN_REQUIRED";
            case 5:
                return "INVALID_ACCOUNT";
            case 6:
                return "RESOLUTION_REQUIRED";
            case 7:
                return "NETWORK_ERROR";
            case 8:
                return "INTERNAL_ERROR";
            case 9:
                return "SERVICE_INVALID";
            case 10:
                return "DEVELOPER_ERROR";
            case 11:
                return "LICENSE_CHECK_FAILED";
            default:
                switch (i2) {
                    case 13:
                        return "CANCELED";
                    case 14:
                        return "TIMEOUT";
                    case 15:
                        return "INTERRUPTED";
                    case 16:
                        return "API_UNAVAILABLE";
                    case 17:
                        return "SIGN_IN_FAILED";
                    case 18:
                        return "SERVICE_UPDATING";
                    case 19:
                        return "SERVICE_MISSING_PERMISSION";
                    case 20:
                        return "RESTRICTED_PROFILE";
                    case 21:
                        return "API_VERSION_UPDATE_REQUIRED";
                    default:
                        StringBuilder sb = new StringBuilder(31);
                        sb.append("UNKNOWN_ERROR_CODE(");
                        sb.append(i2);
                        sb.append(")");
                        return sb.toString();
                }
        }
    }

    @DexIgnore
    public final int H() {
        return this.f;
    }

    @DexIgnore
    public final String I() {
        return this.h;
    }

    @DexIgnore
    public final PendingIntent J() {
        return this.g;
    }

    @DexIgnore
    public final boolean K() {
        return (this.f == 0 || this.g == null) ? false : true;
    }

    @DexIgnore
    public final boolean L() {
        return this.f == 0;
    }

    @DexIgnore
    public final void a(Activity activity, int i2) throws IntentSender.SendIntentException {
        if (K()) {
            activity.startIntentSenderForResult(this.g.getIntentSender(), i2, (Intent) null, 0, 0, 0);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof vd0)) {
            return false;
        }
        vd0 vd0 = (vd0) obj;
        return this.f == vd0.f && ak0.a(this.g, vd0.g) && ak0.a(this.h, vd0.h);
    }

    @DexIgnore
    public final int hashCode() {
        return ak0.a(Integer.valueOf(this.f), this.g, this.h);
    }

    @DexIgnore
    public final String toString() {
        ak0.a a = ak0.a((Object) this);
        a.a("statusCode", zza(this.f));
        a.a("resolution", this.g);
        a.a("message", this.h);
        return a.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, H());
        lk0.a(parcel, 3, (Parcelable) J(), i2, false);
        lk0.a(parcel, 4, I(), false);
        lk0.a(parcel, a);
    }

    @DexIgnore
    public vd0(int i2) {
        this(i2, (PendingIntent) null, (String) null);
    }

    @DexIgnore
    public vd0(int i2, PendingIntent pendingIntent) {
        this(i2, pendingIntent, (String) null);
    }

    @DexIgnore
    public vd0(int i2, PendingIntent pendingIntent, String str) {
        this(1, i2, pendingIntent, str);
    }
}
