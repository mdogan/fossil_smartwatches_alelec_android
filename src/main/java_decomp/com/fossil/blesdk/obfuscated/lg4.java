package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class lg4 {
    @DexIgnore
    public static /* synthetic */ Object a(CoroutineContext coroutineContext, kd4 kd4, int i, Object obj) throws InterruptedException {
        if ((i & 1) != 0) {
            coroutineContext = EmptyCoroutineContext.INSTANCE;
        }
        return kg4.a(coroutineContext, kd4);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003b, code lost:
        if (r1 != null) goto L_0x0044;
     */
    @DexIgnore
    public static final <T> T a(CoroutineContext coroutineContext, kd4<? super lh4, ? super kc4<? super T>, ? extends Object> kd4) throws InterruptedException {
        CoroutineContext coroutineContext2;
        ei4 ei4;
        wd4.b(coroutineContext, "context");
        wd4.b(kd4, "block");
        Thread currentThread = Thread.currentThread();
        lc4 lc4 = (lc4) coroutineContext.get(lc4.b);
        if (lc4 == null) {
            ei4 = nj4.b.b();
            coroutineContext2 = fh4.a(ki4.e, coroutineContext.plus(ei4));
        } else {
            if (!(lc4 instanceof ei4)) {
                lc4 = null;
            }
            ei4 = (ei4) lc4;
            if (ei4 != null) {
                if (!ei4.H()) {
                    ei4 = null;
                }
            }
            ei4 = nj4.b.a();
            coroutineContext2 = fh4.a(ki4.e, coroutineContext);
        }
        wd4.a((Object) currentThread, "currentThread");
        ig4 ig4 = new ig4(coroutineContext2, currentThread, ei4);
        ig4.a(CoroutineStart.DEFAULT, ig4, kd4);
        return ig4.m();
    }
}
