package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qd0 extends kk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<qd0> CREATOR; // = new rd0();
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ long g;

    @DexIgnore
    public qd0(boolean z, long j, long j2) {
        this.e = z;
        this.f = j;
        this.g = j2;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof qd0) {
            qd0 qd0 = (qd0) obj;
            return this.e == qd0.e && this.f == qd0.f && this.g == qd0.g;
        }
    }

    @DexIgnore
    public final int hashCode() {
        return ak0.a(Boolean.valueOf(this.e), Long.valueOf(this.f), Long.valueOf(this.g));
    }

    @DexIgnore
    public final String toString() {
        return "CollectForDebugParcelable[skipPersistentStorage: " + this.e + ",collectForDebugStartTimeMillis: " + this.f + ",collectForDebugExpiryTimeMillis: " + this.g + "]";
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = lk0.a(parcel);
        lk0.a(parcel, 1, this.e);
        lk0.a(parcel, 2, this.g);
        lk0.a(parcel, 3, this.f);
        lk0.a(parcel, a);
    }
}
