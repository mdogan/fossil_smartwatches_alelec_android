package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sk3 implements Factory<ExploreWatchPresenter> {
    @DexIgnore
    public static ExploreWatchPresenter a(pk3 pk3, wj2 wj2, DeviceRepository deviceRepository) {
        return new ExploreWatchPresenter(pk3, wj2, deviceRepository);
    }
}
