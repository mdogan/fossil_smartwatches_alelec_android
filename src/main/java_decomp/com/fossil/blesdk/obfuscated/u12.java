package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u12 {
    @DexIgnore
    public c22 a;

    @DexIgnore
    public c22 a() {
        return this.a;
    }

    @DexIgnore
    public void a(int i) {
    }

    @DexIgnore
    public void a(boolean z) {
    }

    @DexIgnore
    public void b(int i) {
    }

    @DexIgnore
    public void c(int i) {
    }

    @DexIgnore
    public void a(c22 c22) {
        this.a = c22;
    }
}
