package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q84 implements i84 {
    @DexIgnore
    public static /* final */ Class<? extends Object>[] a; // = {Context.class};
    @DexIgnore
    public static /* final */ Class<? extends Object>[] b; // = {Context.class, AttributeSet.class};

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.lang.Class<? extends java.lang.Object>[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: java.lang.Class<? extends java.lang.Object>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|9) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0030 */
    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        wd4.b(str, "name");
        wd4.b(context, "context");
        try {
            Class<? extends U> asSubclass = Class.forName(str).asSubclass(View.class);
            Class<? extends Object>[] clsArr = b;
            Constructor<? extends U> constructor = asSubclass.getConstructor((Class[]) Arrays.copyOf(clsArr, clsArr.length));
            wd4.a((Object) constructor, "clazz.getConstructor(*CONSTRUCTOR_SIGNATURE_2)");
            Object[] objArr = {context, attributeSet};
            Class<? extends Object>[] clsArr2 = a;
            constructor = asSubclass.getConstructor((Class[]) Arrays.copyOf(clsArr2, clsArr2.length));
            wd4.a((Object) constructor, "clazz.getConstructor(*CONSTRUCTOR_SIGNATURE_1)");
            objArr = new Context[]{context};
            constructor.setAccessible(true);
            return (View) constructor.newInstance(Arrays.copyOf(objArr, objArr.length));
        } catch (Exception e) {
            if (e instanceof ClassNotFoundException) {
                e.printStackTrace();
                return null;
            } else if (e instanceof NoSuchMethodException) {
                e.printStackTrace();
                return null;
            } else if (e instanceof IllegalAccessException) {
                e.printStackTrace();
                return null;
            } else if (e instanceof InstantiationException) {
                e.printStackTrace();
                return null;
            } else if (e instanceof InvocationTargetException) {
                e.printStackTrace();
                return null;
            } else {
                throw e;
            }
        }
    }
}
