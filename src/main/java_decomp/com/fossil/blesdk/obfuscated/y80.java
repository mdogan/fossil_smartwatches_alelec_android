package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.legacy.LegacyFileControlOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y80 extends r80 {
    @DexIgnore
    public GattCharacteristic.CharacteristicId L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ long N;
    @DexIgnore
    public /* final */ long O;
    @DexIgnore
    public /* final */ long P;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ y80(long j, long j2, long j3, short s, Peripheral peripheral, int i, int i2, rd4 rd4) {
        this(j, j2, j3, s, peripheral, (i2 & 32) != 0 ? 3 : i);
    }

    @DexIgnore
    public byte[] C() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.N).putInt((int) this.O).putInt((int) this.P).array();
        wd4.a((Object) array, "ByteBuffer.allocate(12)\n\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public boolean F() {
        return this.M;
    }

    @DexIgnore
    public final long J() {
        return this.O;
    }

    @DexIgnore
    public final long K() {
        return this.N;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        wd4.b(bArr, "responseData");
        JSONObject a = super.a(bArr);
        this.L = GattCharacteristic.CharacteristicId.FTD;
        return xa0.a(a, JSONKey.SOCKET_ID, this.L.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public JSONObject t() {
        return xa0.a(xa0.a(xa0.a(super.t(), JSONKey.OFFSET, Long.valueOf(this.N)), JSONKey.LENGTH, Long.valueOf(this.O)), JSONKey.TOTAL_LENGTH, Long.valueOf(this.P));
    }

    @DexIgnore
    public JSONObject u() {
        return xa0.a(super.u(), JSONKey.SOCKET_ID, this.L.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public y80(long j, long j2, long j3, short s, Peripheral peripheral, int i) {
        super(LegacyFileControlOperationCode.LEGACY_PUT_FILE, r2, RequestId.LEGACY_PUT_FILE, peripheral, i);
        wd4.b(peripheral, "peripheral");
        short s2 = s;
        this.N = j;
        this.O = j2;
        this.P = j3;
        this.L = GattCharacteristic.CharacteristicId.UNKNOWN;
        this.M = true;
    }
}
