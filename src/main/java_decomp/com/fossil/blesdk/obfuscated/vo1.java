package com.fossil.blesdk.obfuscated;

import com.google.android.gms.tasks.RuntimeExecutionException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vo1<TResult> extends xn1<TResult> {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ so1<TResult> b; // = new so1<>();
    @DexIgnore
    public boolean c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public TResult e;
    @DexIgnore
    public Exception f;

    @DexIgnore
    public final <X extends Throwable> TResult a(Class<X> cls) throws Throwable {
        TResult tresult;
        synchronized (this.a) {
            g();
            i();
            if (cls.isInstance(this.f)) {
                throw ((Throwable) cls.cast(this.f));
            } else if (this.f == null) {
                tresult = this.e;
            } else {
                throw new RuntimeExecutionException(this.f);
            }
        }
        return tresult;
    }

    @DexIgnore
    public final TResult b() {
        TResult tresult;
        synchronized (this.a) {
            g();
            i();
            if (this.f == null) {
                tresult = this.e;
            } else {
                throw new RuntimeExecutionException(this.f);
            }
        }
        return tresult;
    }

    @DexIgnore
    public final boolean c() {
        return this.d;
    }

    @DexIgnore
    public final boolean d() {
        boolean z;
        synchronized (this.a) {
            z = this.c;
        }
        return z;
    }

    @DexIgnore
    public final boolean e() {
        boolean z;
        synchronized (this.a) {
            z = this.c && !this.d && this.f == null;
        }
        return z;
    }

    @DexIgnore
    public final boolean f() {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.d = true;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final void g() {
        ck0.b(this.c, "Task is not yet complete");
    }

    @DexIgnore
    public final void h() {
        ck0.b(!this.c, "Task is already complete");
    }

    @DexIgnore
    public final void i() {
        if (this.d) {
            throw new CancellationException("Task is already canceled.");
        }
    }

    @DexIgnore
    public final void j() {
        synchronized (this.a) {
            if (this.c) {
                this.b.a(this);
            }
        }
    }

    @DexIgnore
    public final <TContinuationResult> xn1<TContinuationResult> b(qn1<TResult, xn1<TContinuationResult>> qn1) {
        return b(zn1.a, qn1);
    }

    @DexIgnore
    public final <TContinuationResult> xn1<TContinuationResult> b(Executor executor, qn1<TResult, xn1<TContinuationResult>> qn1) {
        vo1 vo1 = new vo1();
        this.b.a(new fo1(executor, qn1, vo1));
        j();
        return vo1;
    }

    @DexIgnore
    public final Exception a() {
        Exception exc;
        synchronized (this.a) {
            exc = this.f;
        }
        return exc;
    }

    @DexIgnore
    public final boolean b(TResult tresult) {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.e = tresult;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final xn1<TResult> a(un1<? super TResult> un1) {
        a(zn1.a, un1);
        return this;
    }

    @DexIgnore
    public final xn1<TResult> a(Executor executor, un1<? super TResult> un1) {
        this.b.a(new no1(executor, un1));
        j();
        return this;
    }

    @DexIgnore
    public final xn1<TResult> a(tn1 tn1) {
        a(zn1.a, tn1);
        return this;
    }

    @DexIgnore
    public final xn1<TResult> a(Executor executor, tn1 tn1) {
        this.b.a(new lo1(executor, tn1));
        j();
        return this;
    }

    @DexIgnore
    public final xn1<TResult> a(sn1<TResult> sn1) {
        a(zn1.a, sn1);
        return this;
    }

    @DexIgnore
    public final xn1<TResult> a(Executor executor, sn1<TResult> sn1) {
        this.b.a(new jo1(executor, sn1));
        j();
        return this;
    }

    @DexIgnore
    public final boolean b(Exception exc) {
        ck0.a(exc, (Object) "Exception must not be null");
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.f = exc;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final <TContinuationResult> xn1<TContinuationResult> a(qn1<TResult, TContinuationResult> qn1) {
        return a(zn1.a, qn1);
    }

    @DexIgnore
    public final <TContinuationResult> xn1<TContinuationResult> a(Executor executor, qn1<TResult, TContinuationResult> qn1) {
        vo1 vo1 = new vo1();
        this.b.a(new do1(executor, qn1, vo1));
        j();
        return vo1;
    }

    @DexIgnore
    public final xn1<TResult> a(Executor executor, rn1 rn1) {
        this.b.a(new ho1(executor, rn1));
        j();
        return this;
    }

    @DexIgnore
    public final <TContinuationResult> xn1<TContinuationResult> a(Executor executor, wn1<TResult, TContinuationResult> wn1) {
        vo1 vo1 = new vo1();
        this.b.a(new po1(executor, wn1, vo1));
        j();
        return vo1;
    }

    @DexIgnore
    public final <TContinuationResult> xn1<TContinuationResult> a(wn1<TResult, TContinuationResult> wn1) {
        return a(zn1.a, wn1);
    }

    @DexIgnore
    public final void a(TResult tresult) {
        synchronized (this.a) {
            h();
            this.c = true;
            this.e = tresult;
        }
        this.b.a(this);
    }

    @DexIgnore
    public final void a(Exception exc) {
        ck0.a(exc, (Object) "Exception must not be null");
        synchronized (this.a) {
            h();
            this.c = true;
            this.f = exc;
        }
        this.b.a(this);
    }
}
