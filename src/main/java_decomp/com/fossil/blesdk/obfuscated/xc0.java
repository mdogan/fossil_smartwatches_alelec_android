package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Binder;
import com.fossil.blesdk.obfuscated.he0;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xc0 extends sc0 {
    @DexIgnore
    public /* final */ Context e;

    @DexIgnore
    public xc0(Context context) {
        this.e = context;
    }

    @DexIgnore
    public final void k() {
        o();
        dc0 a = dc0.a(this.e);
        GoogleSignInAccount b = a.b();
        GoogleSignInOptions googleSignInOptions = GoogleSignInOptions.s;
        if (b != null) {
            googleSignInOptions = a.c();
        }
        he0.a aVar = new he0.a(this.e);
        aVar.a(rb0.e, googleSignInOptions);
        he0 a2 = aVar.a();
        try {
            if (a2.a().L()) {
                if (b != null) {
                    rb0.f.a(a2);
                } else {
                    a2.b();
                }
            }
        } finally {
            a2.d();
        }
    }

    @DexIgnore
    public final void l() {
        o();
        qc0.a(this.e).a();
    }

    @DexIgnore
    public final void o() {
        if (!ae0.isGooglePlayServicesUid(this.e, Binder.getCallingUid())) {
            int callingUid = Binder.getCallingUid();
            StringBuilder sb = new StringBuilder(52);
            sb.append("Calling UID ");
            sb.append(callingUid);
            sb.append(" is not Google Play services.");
            throw new SecurityException(sb.toString());
        }
    }
}
