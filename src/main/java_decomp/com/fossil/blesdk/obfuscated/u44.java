package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface u44<T> {
    @DexIgnore
    public static final u44 a = new b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements u44<Object> {
        @DexIgnore
        public void a(Exception exc) {
        }

        @DexIgnore
        public void a(Object obj) {
        }

        @DexIgnore
        public b() {
        }
    }

    @DexIgnore
    void a(Exception exc);

    @DexIgnore
    void a(T t);
}
