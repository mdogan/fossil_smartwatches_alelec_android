package com.fossil.blesdk.error;

import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class Error extends JSONAbleObject {
    @DexIgnore
    public /* final */ i90 errorCode;

    @DexIgnore
    public Error(i90 i90) {
        wd4.b(i90, "errorCode");
        this.errorCode = i90;
    }

    @DexIgnore
    public i90 getErrorCode() {
        return this.errorCode;
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            xa0.a(jSONObject, JSONKey.ERROR_CODE, getErrorCode().getLogName());
        } catch (JSONException e) {
            ea0.l.a(e);
        }
        return jSONObject;
    }
}
