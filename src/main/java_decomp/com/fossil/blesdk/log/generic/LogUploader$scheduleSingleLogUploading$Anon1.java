package com.fossil.blesdk.log.generic;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.id4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogUploader$scheduleSingleLogUploading$Anon1 extends Lambda implements id4<cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ LogUploader this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LogUploader$scheduleSingleLogUploading$Anon1(LogUploader logUploader) {
        super(0);
        this.this$Anon0 = logUploader;
    }

    @DexIgnore
    public final void invoke() {
        this.this$Anon0.b();
    }
}
