package com.fossil.blesdk.log.generic;

import android.os.Handler;
import com.fossil.blesdk.obfuscated.cb0;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.jb0;
import com.fossil.blesdk.obfuscated.lb4;
import com.fossil.blesdk.obfuscated.oa0;
import com.fossil.blesdk.obfuscated.pa0;
import com.fossil.blesdk.obfuscated.qa0;
import com.fossil.blesdk.obfuscated.qm4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.tb4;
import com.fossil.blesdk.obfuscated.u90;
import com.fossil.blesdk.obfuscated.w90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.x90;
import java.io.File;
import java.util.ArrayList;
import org.json.JSONObject;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogUploader {
    @DexIgnore
    public /* final */ String a; // = LogUploader.class.getSimpleName();
    @DexIgnore
    public pa0 b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ ArrayList<File> d; // = new ArrayList<>();
    @DexIgnore
    public b e;
    @DexIgnore
    public cb0 f;
    @DexIgnore
    public long g; // = 60000;
    @DexIgnore
    public /* final */ x90 h;
    @DexIgnore
    public oa0 i;
    @DexIgnore
    public /* final */ w90 j;
    @DexIgnore
    public /* final */ Handler k;
    @DexIgnore
    public boolean l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Runnable {
        @DexIgnore
        public boolean e;

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final void a() {
            this.e = true;
        }

        @DexIgnore
        public void run() {
            if (!this.e) {
                LogUploader.this.b();
                b a = LogUploader.this.e;
                if (a != null) {
                    LogUploader.this.k.postDelayed(a, LogUploader.this.a());
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements qr4<String> {
        @DexIgnore
        public /* final */ /* synthetic */ File e;
        @DexIgnore
        public /* final */ /* synthetic */ LogUploader f;

        @DexIgnore
        public c(File file, LogUploader logUploader, pa0 pa0) {
            this.e = file;
            this.f = logUploader;
        }

        @DexIgnore
        public void onFailure(Call<String> call, Throwable th) {
            wd4.b(call, "call");
            wd4.b(th, "t");
            u90 u90 = u90.c;
            u90.b("LogUploader", "Upload Log Fail: throw: " + th.getMessage());
            this.f.e();
        }

        @DexIgnore
        public void onResponse(Call<String> call, cs4<String> cs4) {
            wd4.b(call, "call");
            wd4.b(cs4, "response");
            u90 u90 = u90.c;
            StringBuilder sb = new StringBuilder();
            sb.append("Upload Log Response: message: ");
            sb.append(cs4.e());
            sb.append(", ");
            sb.append("code: ");
            sb.append(cs4.b());
            sb.append(", ");
            sb.append("body: ");
            sb.append(cs4.a());
            sb.append(", ");
            sb.append("error body: ");
            qm4 c = cs4.c();
            sb.append(c != null ? c.F() : null);
            u90.a("LogUploader", sb.toString());
            int b = cs4.b();
            if ((200 <= b && 299 >= b) || b == 400) {
                this.f.d.remove(this.e);
                this.e.delete();
                this.f.f();
                return;
            }
            this.f.e();
        }
    }

    /*
    static {
        new a((rd4) null);
    }
    */

    @DexIgnore
    public LogUploader(x90 x90, oa0 oa0, w90 w90, Handler handler, boolean z) {
        wd4.b(x90, "logIO");
        wd4.b(oa0, "endpoint");
        wd4.b(w90, "logFileParser");
        wd4.b(handler, "handler");
        this.h = x90;
        this.i = oa0;
        this.j = w90;
        this.k = handler;
        this.l = z;
        a(this.i);
    }

    @DexIgnore
    public final long a() {
        return this.g;
    }

    @DexIgnore
    public final void b(long j2) {
        if (j2 > 0) {
            this.g = j2;
        }
    }

    @DexIgnore
    public final void c(long j2) {
        c();
        b(j2);
        this.e = new b();
        b bVar = this.e;
        if (bVar != null) {
            this.k.post(bVar);
        }
    }

    @DexIgnore
    public final void d() {
        cb0 cb0 = this.f;
        if (cb0 != null) {
            cb0.a();
            this.k.removeCallbacks(cb0);
        }
    }

    @DexIgnore
    public final void e() {
        synchronized (Boolean.valueOf(this.c)) {
            this.d.clear();
            this.c = false;
            cb4 cb4 = cb4.a;
        }
    }

    @DexIgnore
    public final void f() {
        String str;
        pa0 pa0 = this.b;
        boolean z = false;
        if (pa0 == null) {
            str = "Invalid end point";
        } else if (!jb0.a(jb0.a, wa0.f.a(), false, 2, (Object) null)) {
            str = "Network is not available";
        } else if (!this.l || jb0.a.a(wa0.f.a())) {
            str = new String();
        } else {
            str = "Wifi is not available";
        }
        if (str.length() > 0) {
            z = true;
        }
        if (z || pa0 == null) {
            u90 u90 = u90.c;
            String str2 = this.a;
            wd4.a((Object) str2, "tagName");
            u90.a(str2, "Stop uploading: " + str + '.');
            e();
            return;
        }
        File file = (File) wb4.e(this.d);
        if (file != null) {
            JSONObject a2 = this.j.a(file);
            if (a2.length() > 0) {
                String c2 = this.i.c();
                String jSONObject = a2.toString();
                wd4.a((Object) jSONObject, "logFileInJSON.toString()");
                pa0.a(c2, jSONObject).a(new c(file, this, pa0));
                return;
            }
            this.d.remove(file);
            file.delete();
            f();
            return;
        }
        e();
    }

    @DexIgnore
    public final boolean a(oa0 oa0) {
        wd4.b(oa0, "endpoint");
        if (oa0.c().length() == 0) {
            return false;
        }
        pa0 a2 = qa0.a.a(oa0.b(), oa0.a());
        if (a2 == null) {
            return false;
        }
        this.i = oa0;
        this.b = a2;
        return true;
    }

    @DexIgnore
    public final void b() {
        synchronized (Boolean.valueOf(this.c)) {
            if (!this.c) {
                this.c = true;
                this.d.clear();
                tb4.a(this.d, (T[]) lb4.a((T[]) this.h.a()));
                f();
            }
        }
    }

    @DexIgnore
    public final void a(long j2) {
        d();
        this.f = new cb0(new LogUploader$scheduleSingleLogUploading$Anon1(this));
        cb0 cb0 = this.f;
        if (cb0 != null) {
            this.k.postDelayed(cb0, j2);
        }
    }

    @DexIgnore
    public final void c() {
        b bVar = this.e;
        if (bVar != null) {
            bVar.a();
            this.k.removeCallbacks(bVar);
        }
    }
}
