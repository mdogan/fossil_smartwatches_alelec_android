package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum StatusCode {
    SUCCESS,
    UNSUPPORT_FILE_FORMAT,
    UNSUPPORT_SPECIAL_ENTRY,
    INVALID_BINARY_FILE,
    INVALID_ENTRY,
    INVALID_HEADER
}
