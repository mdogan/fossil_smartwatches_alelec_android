package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TimezoneChange implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<TimezoneChange> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mTimestamp;
    @DexIgnore
    public /* final */ int mTimezoneOffsetInSecond;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<TimezoneChange> {
        @DexIgnore
        public TimezoneChange createFromParcel(Parcel parcel) {
            return new TimezoneChange(parcel);
        }

        @DexIgnore
        public TimezoneChange[] newArray(int i) {
            return new TimezoneChange[i];
        }
    }

    @DexIgnore
    public TimezoneChange(int i, int i2) {
        this.mTimestamp = i;
        this.mTimezoneOffsetInSecond = i2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof TimezoneChange)) {
            return false;
        }
        TimezoneChange timezoneChange = (TimezoneChange) obj;
        if (this.mTimestamp == timezoneChange.mTimestamp && this.mTimezoneOffsetInSecond == timezoneChange.mTimezoneOffsetInSecond) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int getTimestamp() {
        return this.mTimestamp;
    }

    @DexIgnore
    public int getTimezoneOffsetInSecond() {
        return this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public int hashCode() {
        return ((527 + this.mTimestamp) * 31) + this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public String toString() {
        return "TimezoneChange{mTimestamp=" + this.mTimestamp + ",mTimezoneOffsetInSecond=" + this.mTimezoneOffsetInSecond + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mTimestamp);
        parcel.writeInt(this.mTimezoneOffsetInSecond);
    }

    @DexIgnore
    public TimezoneChange(Parcel parcel) {
        this.mTimestamp = parcel.readInt();
        this.mTimezoneOffsetInSecond = parcel.readInt();
    }
}
