package com.fossil.wearables.fossil.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.blesdk.obfuscated.iz3;
import com.fossil.blesdk.obfuscated.jz3;
import com.fossil.blesdk.obfuscated.vs3;
import com.fossil.blesdk.obfuscated.yz3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class WXEntryActivity extends Activity implements yz3 {
    /*
    static {
        Class<WXEntryActivity> cls = WXEntryActivity.class;
    }
    */

    @DexIgnore
    public void a(iz3 iz3) {
        vs3.a().a(iz3);
        finish();
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        vs3.a().a(getIntent(), this);
    }

    @DexIgnore
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        vs3.a().a(getIntent(), this);
    }

    @DexIgnore
    public void a(jz3 jz3) {
        vs3.a().a(jz3);
        finish();
    }
}
