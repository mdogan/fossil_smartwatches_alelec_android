package com.fossil.wearables.fsl.keyvalue;

import android.content.Context;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class KeyValueProviderImpl extends BaseDbProvider implements KeyValueProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "keyvalue.db";

    @DexIgnore
    public KeyValueProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<KeyValue, Integer> getKeyValueDao() throws SQLException {
        return this.databaseHelper.getDao(KeyValue.class);
    }

    @DexIgnore
    public List<KeyValue> getAllKeyValues() {
        ArrayList arrayList = new ArrayList();
        try {
            return getKeyValueDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{KeyValue.class};
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    public KeyValue getKeyValueById(int i) {
        try {
            return getKeyValueDao().queryForId(Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public String getValueByKey(String str) {
        try {
            QueryBuilder<KeyValue, Integer> queryBuilder = getKeyValueDao().queryBuilder();
            queryBuilder.where().eq("key", str);
            List<KeyValue> query = getKeyValueDao().query(queryBuilder.prepare());
            if (query == null || query.size() <= 0) {
                return null;
            }
            KeyValue keyValue = query.get(0);
            if (keyValue != null) {
                return keyValue.getValue();
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public void removeAllKeyValues() {
        for (KeyValue removeKeyValue : getAllKeyValues()) {
            removeKeyValue(removeKeyValue);
        }
    }

    @DexIgnore
    public void removeKeyValue(KeyValue keyValue) {
        if (keyValue != null) {
            try {
                getKeyValueDao().delete(keyValue);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void saveKeyValue(KeyValue keyValue) {
        if (keyValue != null) {
            try {
                QueryBuilder<KeyValue, Integer> queryBuilder = getKeyValueDao().queryBuilder();
                queryBuilder.where().eq("key", keyValue.getKey());
                List<KeyValue> query = getKeyValueDao().query(queryBuilder.prepare());
                if (query != null && query.size() > 0) {
                    KeyValue keyValue2 = query.get(0);
                    if (keyValue2 != null) {
                        keyValue.setDbRowId(keyValue2.getDbRowId());
                    }
                }
                getKeyValueDao().createOrUpdate(keyValue);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
