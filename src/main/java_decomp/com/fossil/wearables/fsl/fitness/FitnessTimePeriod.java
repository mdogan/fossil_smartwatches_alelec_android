package com.fossil.wearables.fsl.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum FitnessTimePeriod {
    HOUR,
    DAY
}
