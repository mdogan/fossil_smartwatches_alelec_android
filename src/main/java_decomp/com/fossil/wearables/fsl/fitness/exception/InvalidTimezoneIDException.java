package com.fossil.wearables.fsl.fitness.exception;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class InvalidTimezoneIDException extends Exception {
    @DexIgnore
    public InvalidTimezoneIDException(String str) {
        super("Invalid timezone id: " + str);
    }
}
