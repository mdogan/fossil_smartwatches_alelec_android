package com.fossil.wearables.fsl.dial;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ValueItem {
    @DexIgnore
    public boolean disabled;
    @DexIgnore
    public String displayName;
    @DexIgnore
    public int displayNameResId;
    @DexIgnore
    public int imageResId;
    @DexIgnore
    public String value;

    @DexIgnore
    public ValueItem(String str, int i) {
        this.displayName = str;
        this.imageResId = i;
        this.value = str;
    }

    @DexIgnore
    public ValueItem(String str, String str2, int i) {
        this.displayName = str;
        this.imageResId = i;
        this.value = str2;
    }

    @DexIgnore
    public ValueItem(int i, String str, int i2) {
        this.displayNameResId = i;
        this.imageResId = i2;
        this.value = str;
    }
}
