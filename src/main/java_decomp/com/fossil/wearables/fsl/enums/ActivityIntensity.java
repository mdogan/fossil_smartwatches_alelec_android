package com.fossil.wearables.fsl.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ActivityIntensity {
    @DexIgnore
    public static /* final */ int INTENSITY_INTENSE; // = 2;
    @DexIgnore
    public static /* final */ int INTENSITY_LIGHT; // = 0;
    @DexIgnore
    public static /* final */ int INTENSITY_MODERATE; // = 1;
    @DexIgnore
    public static /* final */ int MAX_STEPS_PER_MINUTE_LIGHT_LEVEL; // = 70;
    @DexIgnore
    public static /* final */ int MAX_STEPS_PER_MINUTE_MODERATE_LEVEL; // = 140;
    @DexIgnore
    public static /* final */ int MAX_STEPS_TWO_MINUTES_DATA; // = 600;
}
