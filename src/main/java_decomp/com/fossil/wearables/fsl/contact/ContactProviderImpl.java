package com.fossil.wearables.fsl.contact;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ContactProviderImpl extends BaseDbProvider implements ContactProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "entourage.db";

    @DexIgnore
    public ContactProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private List<Contact> getAllContacts() {
        try {
            return getContactDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    private boolean isContactExisted(Contact contact) {
        Cursor query = this.context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, (String[]) null, "name_raw_contact_id=?", new String[]{String.valueOf(contact.getContactId())}, (String) null);
        if (query == null) {
            return false;
        }
        boolean moveToFirst = query.moveToFirst();
        query.close();
        return moveToFirst;
    }

    @DexIgnore
    private void updateContactToDatabase(Contact contact) {
        Cursor cursor = null;
        try {
            ContentResolver contentResolver = this.context.getContentResolver();
            ContentResolver contentResolver2 = contentResolver;
            cursor = contentResolver2.query(ContactsContract.Contacts.CONTENT_URI, (String[]) null, "name_raw_contact_id=?", new String[]{String.valueOf(contact.getContactId())}, (String) null);
            cursor.moveToFirst();
            int i = cursor.getInt(cursor.getColumnIndex("has_phone_number"));
            contact.setPhotoThumbUri(cursor.getString(cursor.getColumnIndex("photo_thumb_uri")));
            ContentResolver contentResolver3 = contentResolver;
            Cursor query = contentResolver3.query(ContactsContract.Data.CONTENT_URI, (String[]) null, "mimetype = ? AND raw_contact_id = ?", new String[]{"vnd.android.cursor.item/name", String.valueOf(contact.getContactId())}, "data2");
            while (query.moveToNext()) {
                contact.setFirstName(query.getString(query.getColumnIndex("data2")));
                contact.setLastName(query.getString(query.getColumnIndex("data3")));
            }
            query.close();
            if (i != 0) {
                Log.i(this.TAG, "Contact has phone numbers");
                Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                Cursor query2 = contentResolver.query(uri, (String[]) null, "name_raw_contact_id = " + contact.getContactId(), (String[]) null, (String) null);
                for (PhoneNumber removePhoneNumber : contact.getPhoneNumbers()) {
                    removePhoneNumber(removePhoneNumber);
                }
                while (query2.moveToNext()) {
                    String string = query2.getString(query2.getColumnIndex("data1"));
                    PhoneNumber phoneNumber = new PhoneNumber();
                    phoneNumber.setNumber(string);
                    phoneNumber.setContact(contact);
                    savePhoneNumber(phoneNumber);
                }
                query2.close();
            } else {
                Log.i(this.TAG, "Contact has no phone numbers");
            }
            Uri uri2 = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
            Cursor query3 = contentResolver.query(uri2, (String[]) null, "raw_contact_id=" + contact.getContactId(), (String[]) null, (String) null);
            while (query3.moveToNext()) {
                String string2 = query3.getString(query3.getColumnIndex("data1"));
                if (!TextUtils.isEmpty(string2)) {
                    EmailAddress emailAddress = new EmailAddress();
                    emailAddress.setAddress(string2);
                    emailAddress.setContact(contact);
                    saveEmailAddress(emailAddress);
                }
            }
            query3.close();
            saveContact(contact);
            if (cursor == null) {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (cursor == null) {
                return;
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        cursor.close();
    }

    @DexIgnore
    public void clearAllTables() {
        try {
            TableUtils.clearTable(getContactGroupDao().getConnectionSource(), ContactGroup.class);
            TableUtils.clearTable(getContactDao().getConnectionSource(), Contact.class);
            TableUtils.clearTable(getPhoneNumberDao().getConnectionSource(), PhoneNumber.class);
            TableUtils.clearTable(getEmailAddressDao().getConnectionSource(), EmailAddress.class);
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "clearAllTables Exception=" + e);
        }
    }

    @DexIgnore
    public List<ContactGroup> getAllContactGroups() {
        ArrayList arrayList = new ArrayList();
        try {
            return getContactGroupDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    public Contact getContact(int i) {
        try {
            return getContactDao().queryForId(Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public Dao<Contact, Integer> getContactDao() throws SQLException {
        return this.databaseHelper.getDao(Contact.class);
    }

    @DexIgnore
    public ContactGroup getContactGroup(int i) {
        try {
            return getContactGroupDao().queryForId(Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public Dao<ContactGroup, Integer> getContactGroupDao() throws SQLException {
        return this.databaseHelper.getDao(ContactGroup.class);
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingEmail(String str) {
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = getAllContactGroups();
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup next : allContactGroups) {
                if (next != null && next.isEnabled()) {
                    Contact contactWithEmail = next.getContactWithEmail(str);
                    if (contactWithEmail != null && contactWithEmail.isUseEmail()) {
                        arrayList.add(next);
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingIncomingCall(String str) {
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = getAllContactGroups();
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup next : allContactGroups) {
                if (next != null && next.isEnabled()) {
                    Contact contactWithPhoneNumber = next.getContactWithPhoneNumber(str);
                    if (contactWithPhoneNumber != null && contactWithPhoneNumber.isUseCall()) {
                        arrayList.add(next);
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingSms(String str) {
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = getAllContactGroups();
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup next : allContactGroups) {
                if (next != null && next.isEnabled()) {
                    Contact contactWithPhoneNumber = next.getContactWithPhoneNumber(str);
                    if (contactWithPhoneNumber != null && contactWithPhoneNumber.isUseSms()) {
                        arrayList.add(next);
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{Contact.class, ContactGroup.class, EmailAddress.class, PhoneNumber.class};
    }

    @DexIgnore
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    public int getDbVersion() {
        return 5;
    }

    @DexIgnore
    public Dao<EmailAddress, Integer> getEmailAddressDao() throws SQLException {
        return this.databaseHelper.getDao(EmailAddress.class);
    }

    @DexIgnore
    public Dao<PhoneNumber, Integer> getPhoneNumberDao() throws SQLException {
        return this.databaseHelper.getDao(PhoneNumber.class);
    }

    @DexIgnore
    public void removeAllContactGroups() {
        for (ContactGroup removeContactGroup : getAllContactGroups()) {
            removeContactGroup(removeContactGroup);
        }
    }

    @DexIgnore
    public void removeContact(Contact contact) {
        if (contact != null) {
            try {
                for (PhoneNumber removePhoneNumber : contact.getPhoneNumbers()) {
                    removePhoneNumber(removePhoneNumber);
                }
                for (EmailAddress removeEmailAddress : contact.getEmailAddresses()) {
                    removeEmailAddress(removeEmailAddress);
                }
                getContactDao().delete(contact);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void removeContactGroup(ContactGroup contactGroup) {
        try {
            for (Contact removeContact : contactGroup.getContacts()) {
                removeContact(removeContact);
            }
            getContactGroupDao().delete(contactGroup);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public void removeEmailAddress(EmailAddress emailAddress) {
        try {
            getEmailAddressDao().delete(emailAddress);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public void removePhoneNumber(PhoneNumber phoneNumber) {
        try {
            getPhoneNumberDao().delete(phoneNumber);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public void saveContact(Contact contact) {
        if (contact != null) {
            try {
                Contact queryForSameId = getContactDao().queryForSameId(contact);
                if (queryForSameId != null) {
                    contact.setDbRowId(queryForSameId.getDbRowId());
                }
                getContactDao().createOrUpdate(contact);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void saveContactGroup(ContactGroup contactGroup) {
        if (contactGroup != null) {
            try {
                getContactGroupDao().createOrUpdate(contactGroup);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void saveEmailAddress(EmailAddress emailAddress) {
        if (emailAddress != null && emailAddress.getContact() != null) {
            try {
                QueryBuilder<EmailAddress, Integer> queryBuilder = getEmailAddressDao().queryBuilder();
                queryBuilder.where().idEq(Integer.valueOf(emailAddress.getContact().getContactId()));
                List<EmailAddress> query = getEmailAddressDao().query(queryBuilder.prepare());
                if (query != null && query.size() > 0) {
                    EmailAddress emailAddress2 = query.get(0);
                    if (emailAddress2 != null) {
                        emailAddress.setDbRowId(emailAddress2.getDbRowId());
                    }
                }
                getEmailAddressDao().createOrUpdate(emailAddress);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void savePhoneNumber(PhoneNumber phoneNumber) {
        if (phoneNumber != null && phoneNumber.getContact() != null) {
            try {
                QueryBuilder<PhoneNumber, Integer> queryBuilder = getPhoneNumberDao().queryBuilder();
                queryBuilder.where().idEq(Integer.valueOf(phoneNumber.getContact().getContactId()));
                List<PhoneNumber> query = getPhoneNumberDao().query(queryBuilder.prepare());
                if (query != null && query.size() > 0) {
                    PhoneNumber phoneNumber2 = query.get(0);
                    if (phoneNumber2 != null) {
                        phoneNumber.setDbRowId(phoneNumber2.getDbRowId());
                    }
                }
                getPhoneNumberDao().createOrUpdate(phoneNumber);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void syncContacts() {
        List<ContactGroup> allContactGroups = getAllContactGroups();
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup contacts : allContactGroups) {
                List<Contact> contacts2 = contacts.getContacts();
                if (contacts2 != null && contacts2.size() > 0) {
                    for (Contact next : contacts2) {
                        if (isContactExisted(next)) {
                            updateContactToDatabase(next);
                        } else {
                            removeContact(next);
                        }
                    }
                }
            }
        }
        for (ContactGroup next2 : getAllContactGroups()) {
            if (next2.getContacts().size() == 0) {
                removeContactGroup(next2);
            }
        }
    }

    @DexIgnore
    public List<ContactGroup> getAllContactGroups(int i) {
        ArrayList arrayList = new ArrayList();
        try {
            return getContactGroupDao().queryForEq("deviceFamily", Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingEmail(String str, int i) {
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = getAllContactGroups(i);
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup next : allContactGroups) {
                if (next != null && next.isEnabled()) {
                    Contact contactWithEmail = next.getContactWithEmail(str);
                    if (contactWithEmail != null && contactWithEmail.isUseEmail()) {
                        arrayList.add(next);
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingIncomingCall(String str, int i) {
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = getAllContactGroups(i);
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup next : allContactGroups) {
                if (next != null && next.isEnabled()) {
                    Contact contactWithPhoneNumber = next.getContactWithPhoneNumber(str);
                    if (contactWithPhoneNumber != null && contactWithPhoneNumber.isUseCall()) {
                        arrayList.add(next);
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingSms(String str, int i) {
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = getAllContactGroups(i);
        if (allContactGroups != null && allContactGroups.size() > 0) {
            for (ContactGroup next : allContactGroups) {
                if (next != null && next.isEnabled()) {
                    Contact contactWithPhoneNumber = next.getContactWithPhoneNumber(str);
                    if (contactWithPhoneNumber != null && contactWithPhoneNumber.isUseSms()) {
                        arrayList.add(next);
                    }
                }
            }
        }
        return arrayList;
    }
}
