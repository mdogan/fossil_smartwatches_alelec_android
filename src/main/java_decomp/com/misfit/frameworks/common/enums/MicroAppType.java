package com.misfit.frameworks.common.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MicroAppType {
    @DexIgnore
    public static int ACTIVITY_THROW_DOWN; // = 12;
    @DexIgnore
    public static int COMMUTE; // = 2;
    @DexIgnore
    public static int COMMUTE_2; // = 3;
    @DexIgnore
    public static int CONTROL_MUSIC; // = 1007;
    @DexIgnore
    public static int GOAL_TRACKING; // = 1008;
    @DexIgnore
    public static int LAST_ALERT; // = 1002;
    @DexIgnore
    public static int MUSIC_MODE; // = 14;
    @DexIgnore
    public static int MUSIC_VOLUME_DOWN; // = 1005;
    @DexIgnore
    public static int MUSIC_VOLUME_UP; // = 1004;
    @DexIgnore
    public static int NOISE_MAKER; // = 13;
    @DexIgnore
    public static int PLAY_NATIVE_MUSIC; // = 7;
    @DexIgnore
    public static int PLAY_SPOTIFY_MUSIC; // = 8;
    @DexIgnore
    public static int RING_MY_PHONE; // = 1000;
    @DexIgnore
    public static int RUNNING_TETHERED_GPS; // = 4;
    @DexIgnore
    public static int SELFIE; // = 1006;
    @DexIgnore
    public static int SEVEN_MINUTE_WORKOUT; // = 9;
    @DexIgnore
    public static int SHOW_DATE; // = 1003;
    @DexIgnore
    public static int SHOW_SECOND_TIMEZONE; // = 1001;
    @DexIgnore
    public static int SPEED_DIAL; // = 5;
    @DexIgnore
    public static int SPIN_DIAL; // = 11;
    @DexIgnore
    public static int STOCKS; // = 16;
    @DexIgnore
    public static int STOP_WATCH; // = 10;
    @DexIgnore
    public static int TETHERED_GPS2; // = 15;
    @DexIgnore
    public static int TEXT_LOCATION; // = 6;
    @DexIgnore
    public static int TIMER_MODE; // = 17;
    @DexIgnore
    public static int UNKNOWN; // = 0;
    @DexIgnore
    public static int WEATHER; // = 1;
}
