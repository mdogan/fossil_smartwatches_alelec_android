package com.misfit.frameworks.buttonservice.model.calibration;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.PlaceManager;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HandCalibrationObj implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<HandCalibrationObj> CREATOR; // = new HandCalibrationObj$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public /* final */ int degree;
    @DexIgnore
    public /* final */ CalibrationEnums.Direction direction;
    @DexIgnore
    public /* final */ CalibrationEnums.HandId handId;
    @DexIgnore
    public /* final */ CalibrationEnums.MovingType movingType;
    @DexIgnore
    public /* final */ CalibrationEnums.Speed speed;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public HandCalibrationObj(CalibrationEnums.HandId handId2, CalibrationEnums.MovingType movingType2, CalibrationEnums.Direction direction2, CalibrationEnums.Speed speed2, int i) {
        wd4.b(handId2, "handId");
        wd4.b(movingType2, "movingType");
        wd4.b(direction2, "direction");
        wd4.b(speed2, PlaceManager.PARAM_SPEED);
        this.handId = handId2;
        this.movingType = movingType2;
        this.direction = direction2;
        this.speed = speed2;
        this.degree = i;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getDegree() {
        return this.degree;
    }

    @DexIgnore
    public final CalibrationEnums.Direction getDirection() {
        return this.direction;
    }

    @DexIgnore
    public final CalibrationEnums.HandId getHandId() {
        return this.handId;
    }

    @DexIgnore
    public final CalibrationEnums.MovingType getMovingType() {
        return this.movingType;
    }

    @DexIgnore
    public final CalibrationEnums.Speed getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        wd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeString(HandCalibrationObj.class.getName());
        parcel.writeInt(this.handId.getValue());
        parcel.writeInt(this.movingType.getValue());
        parcel.writeInt(this.direction.getValue());
        parcel.writeInt(this.speed.getValue());
        parcel.writeInt(this.degree);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public HandCalibrationObj(Parcel parcel) {
        this(r2, r3, r4, r5, parcel.readInt());
        wd4.b(parcel, "parcel");
        CalibrationEnums.HandId fromValue = CalibrationEnums.HandId.fromValue(parcel.readInt());
        wd4.a((Object) fromValue, "CalibrationEnums.HandId.\u2026omValue(parcel.readInt())");
        CalibrationEnums.MovingType fromValue2 = CalibrationEnums.MovingType.fromValue(parcel.readInt());
        wd4.a((Object) fromValue2, "CalibrationEnums.MovingT\u2026omValue(parcel.readInt())");
        CalibrationEnums.Direction fromValue3 = CalibrationEnums.Direction.fromValue(parcel.readInt());
        wd4.a((Object) fromValue3, "CalibrationEnums.Directi\u2026omValue(parcel.readInt())");
        CalibrationEnums.Speed fromValue4 = CalibrationEnums.Speed.fromValue(parcel.readInt());
        wd4.a((Object) fromValue4, "CalibrationEnums.Speed.fromValue(parcel.readInt())");
    }
}
