package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ShineDevice$Companion$CREATOR$Anon1 implements Parcelable.Creator<ShineDevice> {
    @DexIgnore
    public ShineDevice createFromParcel(Parcel parcel) {
        wd4.b(parcel, "parcel");
        return new ShineDevice(parcel);
    }

    @DexIgnore
    public ShineDevice[] newArray(int i) {
        return new ShineDevice[i];
    }
}
