package com.misfit.frameworks.buttonservice.model.watchapp.response;

import android.os.Parcel;
import com.fossil.blesdk.device.data.music.MusicAction;
import com.fossil.blesdk.device.data.music.MusicActionStatus;
import com.fossil.blesdk.device.data.music.MusicEvent;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotifyMusicEventResponse extends MusicResponse {
    @DexIgnore
    public /* final */ MusicMediaAction musicAction;
    @DexIgnore
    public /* final */ MusicMediaStatus status;

    @DexIgnore
    public enum MusicMediaAction {
        PLAY,
        PAUSE,
        TOGGLE_PLAY_PAUSE,
        NEXT,
        PREVIOUS,
        VOLUME_UP,
        VOLUME_DOWN,
        NONE;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = null;

                /*
                static {
                    $EnumSwitchMapping$Anon0 = new int[MusicAction.values().length];
                    $EnumSwitchMapping$Anon0[MusicAction.PLAY.ordinal()] = 1;
                    $EnumSwitchMapping$Anon0[MusicAction.PAUSE.ordinal()] = 2;
                    $EnumSwitchMapping$Anon0[MusicAction.TOGGLE_PLAY_PAUSE.ordinal()] = 3;
                    $EnumSwitchMapping$Anon0[MusicAction.NEXT.ordinal()] = 4;
                    $EnumSwitchMapping$Anon0[MusicAction.PREVIOUS.ordinal()] = 5;
                    $EnumSwitchMapping$Anon0[MusicAction.VOLUME_UP.ordinal()] = 6;
                    $EnumSwitchMapping$Anon0[MusicAction.VOLUME_DOWN.ordinal()] = 7;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final MusicMediaAction fromSDKMusicAction(MusicAction musicAction) {
                wd4.b(musicAction, "sdkMusicAction");
                switch (WhenMappings.$EnumSwitchMapping$Anon0[musicAction.ordinal()]) {
                    case 1:
                        return MusicMediaAction.PLAY;
                    case 2:
                        return MusicMediaAction.PAUSE;
                    case 3:
                        return MusicMediaAction.TOGGLE_PLAY_PAUSE;
                    case 4:
                        return MusicMediaAction.NEXT;
                    case 5:
                        return MusicMediaAction.PREVIOUS;
                    case 6:
                        return MusicMediaAction.VOLUME_UP;
                    case 7:
                        return MusicMediaAction.VOLUME_DOWN;
                    default:
                        return MusicMediaAction.NONE;
                }
            }

            @DexIgnore
            public /* synthetic */ Companion(rd4 rd4) {
                this();
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = null;

            /*
            static {
                $EnumSwitchMapping$Anon0 = new int[MusicMediaAction.values().length];
                $EnumSwitchMapping$Anon0[MusicMediaAction.PLAY.ordinal()] = 1;
                $EnumSwitchMapping$Anon0[MusicMediaAction.PAUSE.ordinal()] = 2;
                $EnumSwitchMapping$Anon0[MusicMediaAction.TOGGLE_PLAY_PAUSE.ordinal()] = 3;
                $EnumSwitchMapping$Anon0[MusicMediaAction.NEXT.ordinal()] = 4;
                $EnumSwitchMapping$Anon0[MusicMediaAction.PREVIOUS.ordinal()] = 5;
                $EnumSwitchMapping$Anon0[MusicMediaAction.VOLUME_UP.ordinal()] = 6;
                $EnumSwitchMapping$Anon0[MusicMediaAction.VOLUME_DOWN.ordinal()] = 7;
                $EnumSwitchMapping$Anon0[MusicMediaAction.NONE.ordinal()] = 8;
            }
            */
        }

        /*
        static {
            Companion = new Companion((rd4) null);
        }
        */

        @DexIgnore
        public final MusicAction toSDKMusicAction() {
            switch (WhenMappings.$EnumSwitchMapping$Anon0[ordinal()]) {
                case 1:
                    return MusicAction.PLAY;
                case 2:
                    return MusicAction.PAUSE;
                case 3:
                    return MusicAction.TOGGLE_PLAY_PAUSE;
                case 4:
                    return MusicAction.NEXT;
                case 5:
                    return MusicAction.PREVIOUS;
                case 6:
                    return MusicAction.VOLUME_UP;
                case 7:
                    return MusicAction.VOLUME_DOWN;
                case 8:
                    return null;
                default:
                    throw new NoWhenBranchMatchedException();
            }
        }
    }

    @DexIgnore
    public enum MusicMediaStatus {
        SUCCESS,
        NO_MUSIC_PLAYER,
        FAIL_TO_TRIGGER;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = null;

            /*
            static {
                $EnumSwitchMapping$Anon0 = new int[MusicMediaStatus.values().length];
                $EnumSwitchMapping$Anon0[MusicMediaStatus.SUCCESS.ordinal()] = 1;
                $EnumSwitchMapping$Anon0[MusicMediaStatus.NO_MUSIC_PLAYER.ordinal()] = 2;
                $EnumSwitchMapping$Anon0[MusicMediaStatus.FAIL_TO_TRIGGER.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public final MusicActionStatus toSDKMusicActionStatus() {
            int i = WhenMappings.$EnumSwitchMapping$Anon0[ordinal()];
            if (i == 1) {
                return MusicActionStatus.SUCCESS;
            }
            if (i == 2) {
                return MusicActionStatus.NO_MUSIC_PLAYER;
            }
            if (i == 3) {
                return MusicActionStatus.FAIL_TO_TRIGGER;
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotifyMusicEventResponse(MusicMediaAction musicMediaAction, MusicMediaStatus musicMediaStatus) {
        super("Music Event_" + musicMediaAction);
        wd4.b(musicMediaAction, "musicAction");
        wd4.b(musicMediaStatus, "status");
        this.musicAction = musicMediaAction;
        this.status = musicMediaStatus;
    }

    @DexIgnore
    public String getHash() {
        String str = this.musicAction + ":" + this.status;
        wd4.a((Object) str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public final MusicMediaAction getMusicAction() {
        return this.musicAction;
    }

    @DexIgnore
    public final MusicMediaStatus getStatus() {
        return this.status;
    }

    @DexIgnore
    public String toRemoteLogString() {
        return super.toRemoteLogString() + ", musicAction=" + this.musicAction + ", status=" + this.status;
    }

    @DexIgnore
    public final MusicEvent toSDKMusicEvent() {
        MusicAction sDKMusicAction = this.musicAction.toSDKMusicAction();
        if (sDKMusicAction != null) {
            return new MusicEvent(sDKMusicAction, this.status.toSDKMusicActionStatus());
        }
        return null;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.musicAction.ordinal());
        parcel.writeInt(this.status.ordinal());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotifyMusicEventResponse(Parcel parcel) {
        super(parcel);
        wd4.b(parcel, "parcel");
        this.musicAction = MusicMediaAction.values()[parcel.readInt()];
        this.status = MusicMediaStatus.values()[parcel.readInt()];
    }
}
