package com.misfit.frameworks.buttonservice.model.watchapp.response;

import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicResponseFactory {
    @DexIgnore
    public static /* final */ MusicResponseFactory INSTANCE; // = new MusicResponseFactory();

    @DexIgnore
    public final NotifyMusicEventResponse createMusicEventResponse(NotifyMusicEventResponse.MusicMediaAction musicMediaAction, NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus) {
        wd4.b(musicMediaAction, "musicAction");
        wd4.b(musicMediaStatus, "status");
        return new NotifyMusicEventResponse(musicMediaAction, musicMediaStatus);
    }

    @DexIgnore
    public final MusicTrackInfoResponse createMusicTrackInfoResponse(String str, byte b, String str2, String str3, String str4) {
        wd4.b(str, "appName");
        wd4.b(str2, "trackTitle");
        wd4.b(str3, "artistName");
        wd4.b(str4, "albumName");
        return new MusicTrackInfoResponse(str, b, str2, str3, str4);
    }
}
