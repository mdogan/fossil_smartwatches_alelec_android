package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class FirmwareData implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<FirmwareData> CREATOR; // = new FirmwareData$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public String checkSum;
    @DexIgnore
    public String deviceModel;
    @DexIgnore
    public String firmwareVersion;
    @DexIgnore
    public boolean isEmbedded;
    @DexIgnore
    public int rawBundleResource;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public FirmwareData() {
        this.isEmbedded = true;
        this.deviceModel = "";
        this.firmwareVersion = "";
        this.rawBundleResource = 1;
        this.checkSum = "";
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getCheckSum() {
        return this.checkSum;
    }

    @DexIgnore
    public final String getDeviceModel() {
        return this.deviceModel;
    }

    @DexIgnore
    public final String getFirmwareVersion() {
        return this.firmwareVersion;
    }

    @DexIgnore
    public final int getRawBundleResource() {
        return this.rawBundleResource;
    }

    @DexIgnore
    public final boolean isEmbedded() {
        return this.isEmbedded;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "dest");
        parcel.writeString(getClass().getName());
        parcel.writeInt(this.isEmbedded ? 1 : 0);
        parcel.writeString(this.firmwareVersion);
        parcel.writeString(this.deviceModel);
        parcel.writeInt(this.rawBundleResource);
        parcel.writeString(this.checkSum);
    }

    @DexIgnore
    public FirmwareData(String str, String str2, int i) {
        wd4.b(str, "firmwareVersion");
        wd4.b(str2, "deviceModel");
        this.isEmbedded = true;
        this.deviceModel = str2;
        this.firmwareVersion = str;
        this.rawBundleResource = i;
        this.checkSum = "";
    }

    @DexIgnore
    public FirmwareData(String str, String str2, String str3) {
        wd4.b(str, "firmwareVersion");
        wd4.b(str2, "deviceModel");
        wd4.b(str3, "checkSum");
        this.isEmbedded = false;
        this.deviceModel = str2;
        this.firmwareVersion = str;
        this.rawBundleResource = -1;
        this.checkSum = str3;
    }

    @DexIgnore
    public FirmwareData(Parcel parcel) {
        wd4.b(parcel, "parcel");
        this.isEmbedded = parcel.readInt() != 0;
        String readString = parcel.readString();
        this.firmwareVersion = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        this.deviceModel = readString2 == null ? "" : readString2;
        this.rawBundleResource = parcel.readInt();
        String readString3 = parcel.readString();
        this.checkSum = readString3 == null ? "" : readString3;
    }
}
