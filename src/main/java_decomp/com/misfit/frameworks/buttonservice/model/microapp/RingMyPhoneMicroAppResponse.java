package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.device.event.request.RingMyPhoneMicroAppRequest;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.model.devicedata.RingMyPhoneMicroAppData;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RingMyPhoneMicroAppResponse extends DeviceAppResponse {
    @DexIgnore
    public RingMyPhoneMicroAppResponse() {
        super(DeviceEventId.RING_MY_PHONE_MICRO_APP);
    }

    @DexIgnore
    public DeviceData getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    public DeviceData getSDKDeviceResponse(DeviceRequest deviceRequest, Version version) {
        wd4.b(deviceRequest, "deviceRequest");
        if (!(deviceRequest instanceof RingMyPhoneMicroAppRequest) || version == null) {
            return null;
        }
        return new RingMyPhoneMicroAppData((RingMyPhoneMicroAppRequest) deviceRequest, version);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
    }
}
