package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.enumerate.Weekday;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherDayForecast implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((rd4) null);
    @DexIgnore
    public /* final */ float highTemperature;
    @DexIgnore
    public /* final */ float lowTemperature;
    @DexIgnore
    public /* final */ WeatherComplicationAppInfo.WeatherCondition weatherCondition;
    @DexIgnore
    public /* final */ WeatherWeekDay weekDay;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherDayForecast> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(rd4 rd4) {
            this();
        }

        @DexIgnore
        public WeatherDayForecast createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new WeatherDayForecast(parcel);
        }

        @DexIgnore
        public WeatherDayForecast[] newArray(int i) {
            return new WeatherDayForecast[i];
        }
    }

    @DexIgnore
    public enum WeatherWeekDay {
        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = null;

            /*
            static {
                $EnumSwitchMapping$Anon0 = new int[WeatherWeekDay.values().length];
                $EnumSwitchMapping$Anon0[WeatherWeekDay.SUNDAY.ordinal()] = 1;
                $EnumSwitchMapping$Anon0[WeatherWeekDay.MONDAY.ordinal()] = 2;
                $EnumSwitchMapping$Anon0[WeatherWeekDay.TUESDAY.ordinal()] = 3;
                $EnumSwitchMapping$Anon0[WeatherWeekDay.WEDNESDAY.ordinal()] = 4;
                $EnumSwitchMapping$Anon0[WeatherWeekDay.THURSDAY.ordinal()] = 5;
                $EnumSwitchMapping$Anon0[WeatherWeekDay.FRIDAY.ordinal()] = 6;
                $EnumSwitchMapping$Anon0[WeatherWeekDay.SATURDAY.ordinal()] = 7;
            }
            */
        }

        @DexIgnore
        public final Weekday toSDKWeekDay() {
            switch (WhenMappings.$EnumSwitchMapping$Anon0[ordinal()]) {
                case 1:
                    return Weekday.SUNDAY;
                case 2:
                    return Weekday.MONDAY;
                case 3:
                    return Weekday.TUESDAY;
                case 4:
                    return Weekday.WEDNESDAY;
                case 5:
                    return Weekday.THURSDAY;
                case 6:
                    return Weekday.FRIDAY;
                case 7:
                    return Weekday.SATURDAY;
                default:
                    throw new NoWhenBranchMatchedException();
            }
        }
    }

    @DexIgnore
    public WeatherDayForecast(float f, float f2, WeatherComplicationAppInfo.WeatherCondition weatherCondition2, WeatherWeekDay weatherWeekDay) {
        wd4.b(weatherCondition2, "weatherCondition");
        wd4.b(weatherWeekDay, "weekDay");
        this.highTemperature = f;
        this.lowTemperature = f2;
        this.weatherCondition = weatherCondition2;
        this.weekDay = weatherWeekDay;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final com.fossil.blesdk.device.data.weather.WeatherDayForecast toSDKWeatherDayForecast() {
        return new com.fossil.blesdk.device.data.weather.WeatherDayForecast(this.weekDay.toSDKWeekDay(), this.highTemperature, this.lowTemperature, this.weatherCondition.toSdkWeatherCondition());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeFloat(this.highTemperature);
        parcel.writeFloat(this.lowTemperature);
        parcel.writeInt(this.weatherCondition.ordinal());
        parcel.writeInt(this.weekDay.ordinal());
    }

    @DexIgnore
    public WeatherDayForecast(Parcel parcel) {
        wd4.b(parcel, "parcel");
        this.highTemperature = parcel.readFloat();
        this.lowTemperature = parcel.readFloat();
        this.weatherCondition = WeatherComplicationAppInfo.WeatherCondition.values()[parcel.readInt()];
        this.weekDay = WeatherWeekDay.values()[parcel.readInt()];
    }
}
