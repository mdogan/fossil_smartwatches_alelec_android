package com.misfit.frameworks.buttonservice.model.pairing;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class PairingResponse implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((rd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<PairingResponse> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public final PairingLinkServerResponse buildPairingLinkServerResponse(boolean z, int i) {
            return new PairingLinkServerResponse(z, i);
        }

        @DexIgnore
        public final PairingUpdateFWResponse buildPairingUpdateFWResponse(FirmwareData firmwareData) {
            wd4.b(firmwareData, "fwData");
            return new PairingUpdateFWResponse(firmwareData);
        }

        @DexIgnore
        public /* synthetic */ CREATOR(rd4 rd4) {
            this();
        }

        @DexIgnore
        public PairingResponse createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            String readString = parcel.readString();
            if (readString != null) {
                try {
                    Class<?> cls = Class.forName(readString);
                    wd4.a((Object) cls, "Class.forName(dynamicClassName!!)");
                    Constructor<?> declaredConstructor = cls.getDeclaredConstructor(new Class[]{Parcel.class});
                    wd4.a((Object) declaredConstructor, "dynamicClass.getDeclared\u2026uctor(Parcel::class.java)");
                    declaredConstructor.setAccessible(true);
                    Object newInstance = declaredConstructor.newInstance(new Object[]{parcel});
                    if (newInstance != null) {
                        return (PairingResponse) newInstance;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.model.pairing.PairingResponse");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    return null;
                } catch (NoSuchMethodException e2) {
                    e2.printStackTrace();
                    return null;
                } catch (IllegalAccessException e3) {
                    e3.printStackTrace();
                    return null;
                } catch (InstantiationException e4) {
                    e4.printStackTrace();
                    return null;
                } catch (InvocationTargetException e5) {
                    e5.printStackTrace();
                    return null;
                }
            } else {
                wd4.a();
                throw null;
            }
        }

        @DexIgnore
        public PairingResponse[] newArray(int i) {
            return new PairingResponse[i];
        }
    }

    @DexIgnore
    public PairingResponse() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        wd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeString(getClass().getName());
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public PairingResponse(Parcel parcel) {
        this();
        wd4.b(parcel, "parcel");
    }
}
