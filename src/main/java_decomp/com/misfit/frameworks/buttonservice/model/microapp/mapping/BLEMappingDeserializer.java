package com.misfit.frameworks.buttonservice.model.microapp.mapping;

import com.fossil.blesdk.obfuscated.sz1;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.wz1;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomizationDeserializer;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class BLEMappingDeserializer implements wz1<BLEMapping> {
    @DexIgnore
    public BLEMapping deserialize(JsonElement jsonElement, Type type, vz1 vz1) throws JsonParseException {
        FLogger.INSTANCE.getLocal().d(BLEMappingDeserializer.class.getName(), jsonElement.toString());
        int b = jsonElement.d().a("mType").b();
        sz1 sz1 = new sz1();
        sz1.a(BLECustomization.class, new BLECustomizationDeserializer());
        if (b == 1) {
            return (BLEMapping) sz1.a().a(jsonElement, LinkMapping.class);
        }
        if (b != 2) {
            return null;
        }
        return (BLEMapping) sz1.a().a(jsonElement, MicroAppMapping.class);
    }
}
