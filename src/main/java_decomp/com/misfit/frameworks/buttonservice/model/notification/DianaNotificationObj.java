package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import com.fossil.blesdk.device.data.notification.AppNotification;
import com.fossil.blesdk.device.data.notification.NotificationFlag;
import com.fossil.blesdk.device.data.notification.NotificationType;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.sina.weibo.sdk.WeiboAppManager;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaNotificationObj extends NotificationBaseObj {
    @DexIgnore
    public AApplicationName notificationApp;

    @DexIgnore
    public enum AApplicationName {
        PHONE_INCOMING_CALL("Phone", 3076063360L, (byte) 1, "incoming call", R.drawable.icon_phone, "icIncomingCall.icon", NotificationBaseObj.ANotificationType.INCOMING_CALL),
        PHONE_MISSED_CALL("Phone", 3076063360L, (byte) 1, "miss call", R.drawable.icon_phone, "icMissedCall.icon", NotificationBaseObj.ANotificationType.MISSED_CALL),
        MESSAGES("Messages", 758293805, (byte) 2, "text message", R.drawable.icon_messages, "icMessage.icon", NotificationBaseObj.ANotificationType.TEXT),
        GOOGLE_CALENDAR("Google Calendar", 1373531549, (byte) 5, "com.google.android.calendar", R.drawable.icon_google_calendar, "icCalendar.icon", NotificationBaseObj.ANotificationType.CALENDAR),
        GMAIL("Gmail", 4233104465L, (byte) 6, "com.google.android.gm", R.drawable.icon_gmail, "icEmail.icon", NotificationBaseObj.ANotificationType.EMAIL),
        HANGOUTS("Hangouts", 3756985074L, (byte) 3, "com.google.android.talk", R.drawable.icon_hangouts, "icHangouts.icon", NotificationBaseObj.ANotificationType.NOTIFICATION),
        MESSENGER("Messenger", 1427889641, (byte) 2, "com.facebook.orca", R.drawable.icon_messenger, "icMessenger.icon", NotificationBaseObj.ANotificationType.NOTIFICATION),
        WHATSAPP("WhatsApp", 3672127513L, (byte) 2, "com.whatsapp", R.drawable.icon_whatsapp, "icWhatsapp.icon", NotificationBaseObj.ANotificationType.NOTIFICATION),
        FACEBOOK("Facebook", 2178452653L, (byte) 2, "com.facebook.katana", R.drawable.icon_facebook, "icFacebook.icon", NotificationBaseObj.ANotificationType.NOTIFICATION),
        TWITTER("Twitter", 2290640688L, (byte) 4, "com.twitter.android", R.drawable.icon_twitter, "icTwitter.icon", NotificationBaseObj.ANotificationType.NOTIFICATION),
        INSTAGRAM("Instagram", 351310857, (byte) 3, "com.instagram.android", R.drawable.icon_instagram, "icInstagram.icon", NotificationBaseObj.ANotificationType.NOTIFICATION),
        SNAPCHAT("Snapchat", 1569315745, (byte) 2, "com.snapchat.android", R.drawable.icon_snapchat, "icSnapchat.icon", NotificationBaseObj.ANotificationType.NOTIFICATION),
        WECHAT("WeChat", 2332507162L, (byte) 2, "com.tencent.mm", R.drawable.icon_wechat, "icWeChat.icon", NotificationBaseObj.ANotificationType.NOTIFICATION),
        WEIBO("Weibo", 331027221, (byte) 7, WeiboAppManager.WEIBO_PACKAGENAME, R.drawable.icon_weibo, "icWeibo.icon", NotificationBaseObj.ANotificationType.NOTIFICATION),
        LINE("Line", 3857604782L, (byte) 2, "jp.naver.line.android", R.drawable.icon_line, "icLine.icon", NotificationBaseObj.ANotificationType.NOTIFICATION),
        GOOGLE_FIT("Google Fit", 1727023459, (byte) 2, "com.google.android.apps.fitness", R.drawable.icon_google_fit, "", NotificationBaseObj.ANotificationType.NOTIFICATION),
        FOSSIL("Fossil Smartwatches", 2875037489L, (byte) 2, "com.fossil.wearables.fossil", R.drawable.icon_messenger, "", NotificationBaseObj.ANotificationType.NOTIFICATION);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ String appName;
        @DexIgnore
        public /* final */ long bundleCrc;
        @DexIgnore
        public /* final */ byte groupId;
        @DexIgnore
        public /* final */ String iconFwPath;
        @DexIgnore
        public /* final */ int iconResourceId;
        @DexIgnore
        public /* final */ NotificationBaseObj.ANotificationType notificationType;
        @DexIgnore
        public /* final */ String packageName;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final boolean isPackageNameSupportedBySDK(String str) {
                wd4.b(str, "packageName");
                for (AApplicationName packageName : AApplicationName.values()) {
                    if (wd4.a((Object) str, (Object) packageName.getPackageName())) {
                        return true;
                    }
                }
                return false;
            }

            @DexIgnore
            public /* synthetic */ Companion(rd4 rd4) {
                this();
            }
        }

        /*
        static {
            Companion = new Companion((rd4) null);
        }
        */

        @DexIgnore
        AApplicationName(String str, long j, byte b, String str2, int i, String str3, NotificationBaseObj.ANotificationType aNotificationType) {
            this.appName = str;
            this.bundleCrc = j;
            this.groupId = b;
            this.packageName = str2;
            this.iconResourceId = i;
            this.iconFwPath = str3;
            this.notificationType = aNotificationType;
        }

        @DexIgnore
        public final String getAppName() {
            return this.appName;
        }

        @DexIgnore
        public final long getBundleCrc() {
            return this.bundleCrc;
        }

        @DexIgnore
        public final byte getGroupId() {
            return this.groupId;
        }

        @DexIgnore
        public final String getIconFwPath() {
            return this.iconFwPath;
        }

        @DexIgnore
        public final int getIconResourceId() {
            return this.iconResourceId;
        }

        @DexIgnore
        public final NotificationBaseObj.ANotificationType getNotificationType() {
            return this.notificationType;
        }

        @DexIgnore
        public final String getPackageName() {
            return this.packageName;
        }
    }

    @DexIgnore
    public DianaNotificationObj(AApplicationName aApplicationName) {
        wd4.b(aApplicationName, "appName");
        this.notificationApp = aApplicationName;
    }

    @DexIgnore
    public final AApplicationName getNotificationApp() {
        return this.notificationApp;
    }

    @DexIgnore
    public final void setNotificationApp(AApplicationName aApplicationName) {
        wd4.b(aApplicationName, "<set-?>");
        this.notificationApp = aApplicationName;
    }

    @DexIgnore
    public String toRemoteLogString() {
        return "UID=" + getUid() + ", notificationApp=" + this.notificationApp + ", flag=" + getNotificationFlags();
    }

    @DexIgnore
    public AppNotification toSDKNotification() {
        NotificationType sDKNotificationType = getNotificationType().toSDKNotificationType();
        int uid = getUid();
        long bundleCrc = this.notificationApp.getBundleCrc();
        String title = getTitle();
        String sender = getSender();
        String message = getMessage();
        Object[] array = toSDKNotificationFlags(getNotificationFlags()).toArray(new NotificationFlag[0]);
        if (array != null) {
            return new AppNotification(sDKNotificationType, uid, bundleCrc, title, sender, message, (NotificationFlag[]) array);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(getUid());
        parcel.writeInt(getNotificationType().ordinal());
        parcel.writeInt(this.notificationApp.ordinal());
        parcel.writeString(getTitle());
        parcel.writeString(getSender());
        parcel.writeString(getMessage());
        ArrayList arrayList = new ArrayList();
        for (NotificationBaseObj.ANotificationFlag ordinal : getNotificationFlags()) {
            arrayList.add(Integer.valueOf(ordinal.ordinal()));
        }
        parcel.writeList(arrayList);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DianaNotificationObj(int i, NotificationBaseObj.ANotificationType aNotificationType, AApplicationName aApplicationName, String str, String str2, String str3, List<NotificationBaseObj.ANotificationFlag> list) {
        this(aApplicationName);
        wd4.b(aNotificationType, "notificationType");
        wd4.b(aApplicationName, "appName");
        wd4.b(str, "title");
        wd4.b(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
        wd4.b(str3, "message");
        wd4.b(list, "notificationFlags");
        setUid(i);
        setNotificationType(aNotificationType);
        setTitle(str);
        setSender(str2);
        setMessage(str3);
        setNotificationFlags(list);
    }

    @DexIgnore
    public DianaNotificationObj(Parcel parcel) {
        super(parcel);
        setUid(parcel.readInt());
        setNotificationType(NotificationBaseObj.ANotificationType.values()[parcel.readInt()]);
        this.notificationApp = AApplicationName.values()[parcel.readInt()];
        String readString = parcel.readString();
        setTitle(readString == null ? "" : readString);
        String readString2 = parcel.readString();
        setSender(readString2 == null ? "" : readString2);
        String readString3 = parcel.readString();
        setMessage(readString3 == null ? "" : readString3);
        setNotificationFlags(new ArrayList());
        ArrayList<Number> arrayList = new ArrayList<>();
        parcel.readList(arrayList, (ClassLoader) null);
        for (Number intValue : arrayList) {
            getNotificationFlags().add(NotificationBaseObj.ANotificationFlag.values()[intValue.intValue()]);
        }
    }
}
