package com.misfit.frameworks.buttonservice.model;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class AppInfo {
    @DexIgnore
    public static /* final */ String TAG; // = "AppInfo";
    @DexIgnore
    public String appVersion;
    @DexIgnore
    public String osVersion;

    @DexIgnore
    public AppInfo(String str, String str2) {
        this.appVersion = str;
        this.osVersion = str2;
    }

    @DexIgnore
    public static AppInfo convertJsonToObject(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "convertJsonToObject - appInfoJson=" + str);
        try {
            return (AppInfo) new Gson().a(str, AppInfo.class);
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.e(TAG, "convertJsonToObject - ex=" + e.toString());
            return null;
        }
    }

    @DexIgnore
    public static String getAppInfoJson(AppInfo appInfo) {
        if (appInfo == null) {
            return "";
        }
        try {
            return new Gson().a((Object) appInfo);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e(TAG, "getAppInfoJson - ex=" + e.toString());
            return "";
        }
    }

    @DexIgnore
    public String getAppVersion() {
        return this.appVersion;
    }

    @DexIgnore
    public String getOsVersion() {
        return this.osVersion;
    }

    @DexIgnore
    public String toString() {
        return "AppInfo{appVersion='" + this.appVersion + '\'' + ", osVersion='" + this.osVersion + '\'' + '}';
    }
}
