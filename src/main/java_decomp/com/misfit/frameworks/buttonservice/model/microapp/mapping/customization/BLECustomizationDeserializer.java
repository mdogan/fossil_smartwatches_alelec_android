package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.util.Log;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.wz1;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class BLECustomizationDeserializer implements wz1<BLECustomization> {
    @DexIgnore
    public BLECustomization deserialize(JsonElement jsonElement, Type type, vz1 vz1) throws JsonParseException {
        Log.d(BLECustomizationDeserializer.class.getName(), jsonElement.toString());
        if (jsonElement.d().a("type").b() != 1) {
            return new BLENonCustomization();
        }
        return (BLECustomization) new Gson().a(jsonElement, BLEGoalTrackingCustomization.class);
    }
}
