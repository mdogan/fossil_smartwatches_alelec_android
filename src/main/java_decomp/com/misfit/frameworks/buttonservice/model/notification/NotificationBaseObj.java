package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.notification.AppNotification;
import com.fossil.blesdk.device.data.notification.NotificationFlag;
import com.fossil.blesdk.device.data.notification.NotificationType;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.LifeCountDownObject;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class NotificationBaseObj implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<NotificationBaseObj> CREATOR; // = new NotificationBaseObj$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public /* final */ int LIFE_COUNT_DOWN; // = 7;
    @DexIgnore
    public /* final */ LifeCountDownObject lifeCountDownObject; // = new LifeCountDownObject(this.LIFE_COUNT_DOWN);
    @DexIgnore
    public String message;
    @DexIgnore
    public List<ANotificationFlag> notificationFlags;
    @DexIgnore
    public ANotificationType notificationType;
    @DexIgnore
    public String sender;
    @DexIgnore
    public String title;
    @DexIgnore
    public int uid;

    @DexIgnore
    public enum ANotificationFlag {
        SILENT,
        IMPORTANT,
        PRE_EXISTING,
        ALLOW_USER_ACTION;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = null;

            /*
            static {
                $EnumSwitchMapping$Anon0 = new int[ANotificationFlag.values().length];
                $EnumSwitchMapping$Anon0[ANotificationFlag.SILENT.ordinal()] = 1;
                $EnumSwitchMapping$Anon0[ANotificationFlag.IMPORTANT.ordinal()] = 2;
                $EnumSwitchMapping$Anon0[ANotificationFlag.PRE_EXISTING.ordinal()] = 3;
                $EnumSwitchMapping$Anon0[ANotificationFlag.ALLOW_USER_ACTION.ordinal()] = 4;
            }
            */
        }

        @DexIgnore
        public final NotificationFlag toSDKNotificationFlag() {
            int i = WhenMappings.$EnumSwitchMapping$Anon0[ordinal()];
            if (i == 1) {
                return NotificationFlag.SILENT;
            }
            if (i == 2) {
                return NotificationFlag.IMPORTANT;
            }
            if (i == 3) {
                return NotificationFlag.PRE_EXISTING;
            }
            if (i == 4) {
                return NotificationFlag.ALLOW_USER_ACTION;
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    @DexIgnore
    public enum ANotificationType {
        UNSUPPORTED,
        INCOMING_CALL,
        TEXT,
        NOTIFICATION,
        EMAIL,
        CALENDAR,
        MISSED_CALL,
        REMOVED;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = null;

            /*
            static {
                $EnumSwitchMapping$Anon0 = new int[ANotificationType.values().length];
                $EnumSwitchMapping$Anon0[ANotificationType.UNSUPPORTED.ordinal()] = 1;
                $EnumSwitchMapping$Anon0[ANotificationType.INCOMING_CALL.ordinal()] = 2;
                $EnumSwitchMapping$Anon0[ANotificationType.TEXT.ordinal()] = 3;
                $EnumSwitchMapping$Anon0[ANotificationType.NOTIFICATION.ordinal()] = 4;
                $EnumSwitchMapping$Anon0[ANotificationType.EMAIL.ordinal()] = 5;
                $EnumSwitchMapping$Anon0[ANotificationType.CALENDAR.ordinal()] = 6;
                $EnumSwitchMapping$Anon0[ANotificationType.MISSED_CALL.ordinal()] = 7;
                $EnumSwitchMapping$Anon0[ANotificationType.REMOVED.ordinal()] = 8;
            }
            */
        }

        @DexIgnore
        public final NotificationType toSDKNotificationType() {
            switch (WhenMappings.$EnumSwitchMapping$Anon0[ordinal()]) {
                case 1:
                    return NotificationType.UNSUPPORTED;
                case 2:
                    return NotificationType.INCOMING_CALL;
                case 3:
                    return NotificationType.TEXT;
                case 4:
                    return NotificationType.NOTIFICATION;
                case 5:
                    return NotificationType.EMAIL;
                case 6:
                    return NotificationType.CALENDAR;
                case 7:
                    return NotificationType.MISSED_CALL;
                case 8:
                    return NotificationType.REMOVED;
                default:
                    throw new NoWhenBranchMatchedException();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public NotificationBaseObj() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final LifeCountDownObject getLifeCountDownObject() {
        return this.lifeCountDownObject;
    }

    @DexIgnore
    public final String getMessage() {
        String str = this.message;
        if (str != null) {
            return str;
        }
        wd4.d("message");
        throw null;
    }

    @DexIgnore
    public final List<ANotificationFlag> getNotificationFlags() {
        List<ANotificationFlag> list = this.notificationFlags;
        if (list != null) {
            return list;
        }
        wd4.d("notificationFlags");
        throw null;
    }

    @DexIgnore
    public final ANotificationType getNotificationType() {
        ANotificationType aNotificationType = this.notificationType;
        if (aNotificationType != null) {
            return aNotificationType;
        }
        wd4.d("notificationType");
        throw null;
    }

    @DexIgnore
    public final String getSender() {
        String str = this.sender;
        if (str != null) {
            return str;
        }
        wd4.d(RemoteFLogger.MESSAGE_SENDER_KEY);
        throw null;
    }

    @DexIgnore
    public final String getTitle() {
        String str = this.title;
        if (str != null) {
            return str;
        }
        wd4.d("title");
        throw null;
    }

    @DexIgnore
    public final int getUid() {
        return this.uid;
    }

    @DexIgnore
    public final void setMessage(String str) {
        wd4.b(str, "<set-?>");
        this.message = str;
    }

    @DexIgnore
    public final void setNotificationFlags(List<ANotificationFlag> list) {
        wd4.b(list, "<set-?>");
        this.notificationFlags = list;
    }

    @DexIgnore
    public final void setNotificationType(ANotificationType aNotificationType) {
        wd4.b(aNotificationType, "<set-?>");
        this.notificationType = aNotificationType;
    }

    @DexIgnore
    public final void setSender(String str) {
        wd4.b(str, "<set-?>");
        this.sender = str;
    }

    @DexIgnore
    public final void setTitle(String str) {
        wd4.b(str, "<set-?>");
        this.title = str;
    }

    @DexIgnore
    public final void setUid(int i) {
        this.uid = i;
    }

    @DexIgnore
    public String toRemoteLogString() {
        String a = new Gson().a((Object) this);
        wd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public abstract AppNotification toSDKNotification();

    @DexIgnore
    public final List<NotificationFlag> toSDKNotificationFlags(List<? extends ANotificationFlag> list) {
        wd4.b(list, "$this$toSDKNotificationFlags");
        ArrayList arrayList = new ArrayList(pb4.a(list, 10));
        for (ANotificationFlag sDKNotificationFlag : list) {
            arrayList.add(sDKNotificationFlag.toSDKNotificationFlag());
        }
        return arrayList;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        wd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeString(getClass().getName());
    }

    @DexIgnore
    public NotificationBaseObj(Parcel parcel) {
        wd4.b(parcel, "parcel");
    }
}
