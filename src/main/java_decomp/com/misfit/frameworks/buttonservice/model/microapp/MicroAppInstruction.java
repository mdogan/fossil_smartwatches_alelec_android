package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppId;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppVariantId;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class MicroAppInstruction implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<MicroAppInstruction> CREATOR; // = new MicroAppInstruction$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public /* final */ String className;
    @DexIgnore
    public MicroAppId declarationID;
    @DexIgnore
    public MicroAppVariantId variantID;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Parcelable.Creator<MicroAppInstruction> getCREATOR() {
            return MicroAppInstruction.CREATOR;
        }

        @DexIgnore
        public final void setCREATOR(Parcelable.Creator<MicroAppInstruction> creator) {
            MicroAppInstruction.CREATOR = creator;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public enum MicroAppID {
        UAPP_HID_MEDIA_CONTROL_MUSIC("music-control", true, false),
        UAPP_HID_MEDIA_VOL_UP_ID("music-volumn-up", true, false),
        UAPP_HID_MEDIA_VOL_DOWN_ID("music-volumn-down", true, false),
        UAPP_ACTIVITY_TAGGING_ID(Constants.ACTIVITY, false, false),
        UAPP_GOAL_TRACKING_ID("goal-tracking", false, false),
        UAPP_DATE_ID("date", false, false),
        UAPP_TIME2_ID("second-time-zone", false, false),
        UAPP_ALERT_ID("alert", false, false),
        UAPP_ALARM_ID(Alarm.TABLE_NAME, false, false),
        UAPP_PROGRESS_ID(Constants.ACTIVITY, false, false),
        UAPP_WEATHER_STANDARD("weather", false, true),
        UAPP_COMMUTE_TIME("commute-time", false, true),
        UAPP_TOGGLE_MODE("sequence", false, false),
        UAPP_RING_PHONE("ring-my-phone", false, true),
        UAPP_SELFIE(Constants.SELFIE, true, false),
        UAPP_STOPWATCH("stopwatch", false, false),
        UAPP_UNKNOWN("unknown", false, false);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ boolean isNeedHID;
        @DexIgnore
        public /* final */ boolean isNeedStreaming;
        @DexIgnore
        public /* final */ String value;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final MicroAppID getMicroAppId(String str) {
                wd4.b(str, "value");
                MicroAppID[] values = MicroAppID.values();
                int length = values.length;
                for (int i = 0; i < length; i++) {
                    if (wd4.a((Object) values[i].getValue(), (Object) str)) {
                        return values[i];
                    }
                }
                return MicroAppID.UAPP_TOGGLE_MODE;
            }

            @DexIgnore
            public final MicroAppID getMicroAppIdFromDeviceEventId(int i) {
                if (i == DeviceEventId.RING_MY_PHONE_MICRO_APP.ordinal()) {
                    return MicroAppID.UAPP_RING_PHONE;
                }
                if (i == DeviceEventId.COMMUTE_TIME_ETA_MICRO_APP.ordinal() || i == DeviceEventId.COMMUTE_TIME_TRAVEL_MICRO_APP.ordinal()) {
                    return MicroAppID.UAPP_COMMUTE_TIME;
                }
                return MicroAppID.UAPP_UNKNOWN;
            }

            @DexIgnore
            public /* synthetic */ Companion(rd4 rd4) {
                this();
            }
        }

        /*
        static {
            Companion = new Companion((rd4) null);
        }
        */

        @DexIgnore
        MicroAppID(String str, boolean z, boolean z2) {
            this.value = str;
            this.isNeedHID = z;
            this.isNeedStreaming = z2;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }

        @DexIgnore
        public final boolean isNeedHID() {
            return this.isNeedHID;
        }

        @DexIgnore
        public final boolean isNeedStreaming() {
            return this.isNeedStreaming;
        }
    }

    @DexIgnore
    public MicroAppInstruction(MicroAppId microAppId, MicroAppVariantId microAppVariantId) {
        wd4.b(microAppId, "declarationID");
        wd4.b(microAppVariantId, "variantID");
        String name = MicroAppInstruction.class.getName();
        wd4.a((Object) name, "javaClass.name");
        this.className = name;
        this.declarationID = microAppId;
        this.variantID = microAppVariantId;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final MicroAppId getDeclarationID() {
        return this.declarationID;
    }

    @DexIgnore
    public abstract DeviceData getMicroAppData();

    @DexIgnore
    public final MicroAppVariantId getVariantID() {
        return this.variantID;
    }

    @DexIgnore
    public final void setDeclarationID(MicroAppId microAppId) {
        this.declarationID = microAppId;
    }

    @DexIgnore
    public final void setVariantID(MicroAppVariantId microAppVariantId) {
        this.variantID = microAppVariantId;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeString(this.className);
        MicroAppId microAppId = this.declarationID;
        if (microAppId != null) {
            parcel.writeInt(microAppId.ordinal());
            MicroAppVariantId microAppVariantId = this.variantID;
            if (microAppVariantId != null) {
                parcel.writeInt(microAppVariantId.ordinal());
            } else {
                wd4.a();
                throw null;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public MicroAppInstruction(Parcel parcel) {
        wd4.b(parcel, "in");
        String name = MicroAppInstruction.class.getName();
        wd4.a((Object) name, "javaClass.name");
        this.className = name;
        this.declarationID = MicroAppId.values()[parcel.readInt()];
        this.variantID = MicroAppVariantId.values()[parcel.readInt()];
    }
}
