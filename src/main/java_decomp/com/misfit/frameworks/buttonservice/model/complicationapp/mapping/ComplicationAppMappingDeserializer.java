package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import com.fossil.blesdk.obfuscated.sz1;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wz1;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationAppMappingDeserializer implements wz1<ComplicationAppMapping> {
    @DexIgnore
    public ComplicationAppMapping deserialize(JsonElement jsonElement, Type type, vz1 vz1) throws JsonParseException {
        wd4.b(jsonElement, "json");
        wd4.b(type, "typeOfT");
        wd4.b(vz1, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String name = ComplicationAppMappingDeserializer.class.getName();
        wd4.a((Object) name, "ComplicationAppMappingDe\u2026rializer::class.java.name");
        local.d(name, jsonElement.toString());
        JsonElement a = jsonElement.d().a(ComplicationAppMapping.Companion.getFIELD_TYPE());
        wd4.a((Object) a, "jsonType");
        int b = a.b();
        sz1 sz1 = new sz1();
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getWEATHER_TYPE()) {
            return (ComplicationAppMapping) sz1.a().a(jsonElement, WeatherComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getHEART_RATE_TYPE()) {
            return (ComplicationAppMapping) sz1.a().a(jsonElement, HeartRateComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getSTEPS_TYPE()) {
            return (ComplicationAppMapping) sz1.a().a(jsonElement, StepsComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getDATE_TYPE()) {
            return (ComplicationAppMapping) sz1.a().a(jsonElement, DateComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getEMPTY_TYPE()) {
            return (ComplicationAppMapping) sz1.a().a(jsonElement, NoneComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getCHANCE_OF_RAIN()) {
            return (ComplicationAppMapping) sz1.a().a(jsonElement, ChanceOfRainComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getTIMEZONE_2_TYPE()) {
            return (ComplicationAppMapping) sz1.a().a(jsonElement, SecondTimezoneComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getACTIVE_MINUTES()) {
            return (ComplicationAppMapping) sz1.a().a(jsonElement, ActiveMinutesComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getCALORIES()) {
            return (ComplicationAppMapping) sz1.a().a(jsonElement, CaloriesComplicationAppMapping.class);
        }
        return null;
    }
}
