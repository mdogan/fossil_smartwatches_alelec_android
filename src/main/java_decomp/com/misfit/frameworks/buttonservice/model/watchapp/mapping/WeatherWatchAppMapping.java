package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import com.fossil.blesdk.device.data.watchapp.WatchApp;
import com.fossil.blesdk.device.data.watchapp.WeatherWatchApp;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherWatchAppMapping extends WatchAppMapping {
    @DexIgnore
    public WeatherWatchAppMapping() {
        super(WatchAppMapping.WatchAppMappingType.INSTANCE.getWEATHER());
    }

    @DexIgnore
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        wd4.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    public WatchApp toSDKSetting() {
        return new WeatherWatchApp();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherWatchAppMapping(Parcel parcel) {
        super(parcel);
        wd4.b(parcel, "parcel");
    }
}
