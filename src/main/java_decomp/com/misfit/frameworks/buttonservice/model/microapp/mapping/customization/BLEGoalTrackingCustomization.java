package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.os.Parcel;
import com.fossil.blesdk.model.microapp.customization.GoalTrackingMicroAppCustomization;
import com.fossil.blesdk.model.microapp.customization.MicroAppCustomization;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BLEGoalTrackingCustomization extends BLECustomization {
    @DexIgnore
    public int goalId;

    @DexIgnore
    public BLEGoalTrackingCustomization(int i) {
        super(1);
        this.goalId = i;
    }

    @DexIgnore
    public MicroAppCustomization getCustomizationFrame() {
        return new GoalTrackingMicroAppCustomization((short) this.goalId);
    }

    @DexIgnore
    public final int getGoalId() {
        return this.goalId;
    }

    @DexIgnore
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getType());
        sb.append(':');
        sb.append(this.goalId);
        return sb.toString();
    }

    @DexIgnore
    public final void setGoalId(int i) {
        this.goalId = i;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.goalId);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BLEGoalTrackingCustomization(Parcel parcel) {
        super(parcel);
        wd4.b(parcel, "in");
        this.goalId = parcel.readInt();
    }
}
