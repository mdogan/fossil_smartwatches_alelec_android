package com.misfit.frameworks.buttonservice.model.watchparams;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Version implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((rd4) null);
    @DexIgnore
    @g02("major")
    public /* final */ int major;
    @DexIgnore
    @g02("minor")
    public /* final */ int minor;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<Version> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(rd4 rd4) {
            this();
        }

        @DexIgnore
        public Version createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new Version(parcel);
        }

        @DexIgnore
        public Version[] newArray(int i) {
            return new Version[i];
        }
    }

    @DexIgnore
    public Version(int i, int i2) {
        this.major = i;
        this.minor = i2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getMajor() {
        return this.major;
    }

    @DexIgnore
    public final int getMinor() {
        return this.minor;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.major);
        sb.append('.');
        sb.append(this.minor);
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeInt(this.major);
        parcel.writeInt(this.minor);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Version(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt());
        wd4.b(parcel, "parcel");
    }
}
