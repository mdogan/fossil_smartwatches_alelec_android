package com.misfit.frameworks.buttonservice.model.background;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.fossil.blesdk.device.data.background.BackgroundImage;
import com.fossil.blesdk.model.background.config.position.BackgroundPositionConfig;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.common.log.MFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BackgroundImgData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((rd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public String imgData;
    @DexIgnore
    public String imgName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<BackgroundImgData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(rd4 rd4) {
            this();
        }

        @DexIgnore
        public BackgroundImgData createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new BackgroundImgData(parcel, (rd4) null);
        }

        @DexIgnore
        public BackgroundImgData[] newArray(int i) {
            return new BackgroundImgData[i];
        }
    }

    /*
    static {
        String simpleName = BackgroundImgData.class.getSimpleName();
        wd4.a((Object) simpleName, "BackgroundImgData::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public /* synthetic */ BackgroundImgData(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getHash() {
        return this.imgName + ':' + this.imgData;
    }

    @DexIgnore
    public final BackgroundImage toSDKBackgroundImage() {
        byte[] bArr;
        try {
            bArr = Base64.decode(this.imgData, 0);
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, ".toSDKBackgroundImage(), ex: " + e);
            bArr = new byte[0];
        }
        byte[] bArr2 = bArr;
        String str2 = this.imgName;
        wd4.a((Object) bArr2, "byteData");
        return new BackgroundImage(str2, bArr2, (BackgroundPositionConfig) null, 4, (rd4) null);
    }

    @DexIgnore
    public String toString() {
        return "{imgName: " + this.imgName + ", imgData: BASE64_DATA_SIZE_" + this.imgData.length() + '}';
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeString(this.imgName);
        parcel.writeString(this.imgData);
    }

    @DexIgnore
    public BackgroundImgData(String str, String str2) {
        wd4.b(str, "imgName");
        wd4.b(str2, "imgData");
        this.imgName = str;
        this.imgData = str2;
    }

    @DexIgnore
    public BackgroundImgData(Parcel parcel) {
        String readString = parcel.readString();
        this.imgName = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        this.imgData = readString2 == null ? "" : readString2;
    }
}
