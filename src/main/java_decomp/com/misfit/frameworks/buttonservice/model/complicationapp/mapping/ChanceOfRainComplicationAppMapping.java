package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import com.fossil.blesdk.device.data.complication.ChanceOfRainComplication;
import com.fossil.blesdk.device.data.complication.Complication;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ChanceOfRainComplicationAppMapping extends ComplicationAppMapping {
    @DexIgnore
    public ChanceOfRainComplicationAppMapping() {
        super(ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getCHANCE_OF_RAIN());
    }

    @DexIgnore
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        wd4.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    public Complication toSDKSetting() {
        return new ChanceOfRainComplication();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ChanceOfRainComplicationAppMapping(Parcel parcel) {
        super(parcel);
        wd4.b(parcel, "parcel");
    }
}
