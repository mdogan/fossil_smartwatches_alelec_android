package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.weather.WeatherInfo;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.device.event.request.WeatherWatchAppRequest;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.model.devicedata.WeatherWatchAppData;
import com.fossil.blesdk.obfuscated.pb4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherInfoWatchAppResponse extends DeviceAppResponse {
    @DexIgnore
    public /* final */ List<WeatherWatchAppInfo> listOfWeatherInfo;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WeatherInfoWatchAppResponse(WeatherWatchAppInfo weatherWatchAppInfo, WeatherWatchAppInfo weatherWatchAppInfo2, WeatherWatchAppInfo weatherWatchAppInfo3, int i, rd4 rd4) {
        this(weatherWatchAppInfo, (i & 2) != 0 ? null : weatherWatchAppInfo2, (i & 4) != 0 ? null : weatherWatchAppInfo3);
    }

    @DexIgnore
    public DeviceData getSDKDeviceData() {
        List<WeatherWatchAppInfo> list = this.listOfWeatherInfo;
        ArrayList arrayList = new ArrayList(pb4.a(list, 10));
        for (WeatherWatchAppInfo sDKWeatherAppInfo : list) {
            arrayList.add(sDKWeatherAppInfo.toSDKWeatherAppInfo());
        }
        Object[] array = arrayList.toArray(new WeatherInfo[0]);
        if (array != null) {
            return new WeatherWatchAppData((WeatherInfo[]) array);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public DeviceData getSDKDeviceResponse(DeviceRequest deviceRequest, Version version) {
        wd4.b(deviceRequest, "deviceRequest");
        if (!(deviceRequest instanceof WeatherWatchAppRequest)) {
            return null;
        }
        List<WeatherWatchAppInfo> list = this.listOfWeatherInfo;
        ArrayList arrayList = new ArrayList(pb4.a(list, 10));
        for (WeatherWatchAppInfo sDKWeatherAppInfo : list) {
            arrayList.add(sDKWeatherAppInfo.toSDKWeatherAppInfo());
        }
        WeatherWatchAppRequest weatherWatchAppRequest = (WeatherWatchAppRequest) deviceRequest;
        Object[] array = arrayList.toArray(new WeatherInfo[0]);
        if (array != null) {
            return new WeatherWatchAppData(weatherWatchAppRequest, (WeatherInfo[]) array);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeTypedList(this.listOfWeatherInfo);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherInfoWatchAppResponse(WeatherWatchAppInfo weatherWatchAppInfo, WeatherWatchAppInfo weatherWatchAppInfo2, WeatherWatchAppInfo weatherWatchAppInfo3) {
        super(DeviceEventId.WEATHER_WATCH_APP);
        wd4.b(weatherWatchAppInfo, "firstWeatherWatchAppInfo");
        this.listOfWeatherInfo = new ArrayList();
        this.listOfWeatherInfo.add(weatherWatchAppInfo);
        if (weatherWatchAppInfo2 != null) {
            this.listOfWeatherInfo.add(weatherWatchAppInfo2);
        }
        if (weatherWatchAppInfo3 != null) {
            this.listOfWeatherInfo.add(weatherWatchAppInfo3);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherInfoWatchAppResponse(Parcel parcel) {
        super(parcel);
        wd4.b(parcel, "parcel");
        this.listOfWeatherInfo = new ArrayList();
        parcel.readTypedList(this.listOfWeatherInfo, WeatherWatchAppInfo.CREATOR);
    }
}
