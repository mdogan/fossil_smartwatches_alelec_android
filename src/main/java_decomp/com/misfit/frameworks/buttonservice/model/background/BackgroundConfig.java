package com.misfit.frameworks.buttonservice.model.background;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.background.BackgroundImageConfig;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BackgroundConfig implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((rd4) null);
    @DexIgnore
    public BackgroundImgData bottomComplicationBackground;
    @DexIgnore
    public BackgroundImgData leftComplicationBackground;
    @DexIgnore
    public BackgroundImgData mainBackground;
    @DexIgnore
    public BackgroundImgData rightComplicationBackground;
    @DexIgnore
    public /* final */ long timestamp;
    @DexIgnore
    public BackgroundImgData topComplicationBackground;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<BackgroundConfig> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(rd4 rd4) {
            this();
        }

        @DexIgnore
        public BackgroundConfig createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new BackgroundConfig(parcel, (rd4) null);
        }

        @DexIgnore
        public BackgroundConfig[] newArray(int i) {
            return new BackgroundConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ BackgroundConfig(Parcel parcel, rd4 rd4) {
        this(parcel);
    }

    @DexIgnore
    private final String getHash() {
        return this.mainBackground.getHash() + ':' + this.topComplicationBackground.getHash() + ':' + this.rightComplicationBackground.getHash() + ':' + this.bottomComplicationBackground.getHash() + ':' + this.leftComplicationBackground.getHash();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BackgroundConfig)) {
            return false;
        }
        return cg4.b(getHash(), ((BackgroundConfig) obj).getHash(), true);
    }

    @DexIgnore
    public final long getTimestamp() {
        return this.timestamp;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public final BackgroundImageConfig toSDKBackgroundImageConfig() {
        return new BackgroundImageConfig(this.mainBackground.toSDKBackgroundImage(), this.topComplicationBackground.toSDKBackgroundImage(), this.rightComplicationBackground.toSDKBackgroundImage(), this.bottomComplicationBackground.toSDKBackgroundImage(), this.leftComplicationBackground.toSDKBackgroundImage());
    }

    @DexIgnore
    public String toString() {
        return "{timestamp: " + this.timestamp + ", " + "mainBackground: " + this.mainBackground + ", " + "topComplicationBackground: " + this.topComplicationBackground + ", " + "rightComplicationBackground: " + this.rightComplicationBackground + ", bottomComplicationBackground: " + this.bottomComplicationBackground + ", " + "leftComplicationBackground: " + this.leftComplicationBackground + '}';
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeLong(this.timestamp);
        this.mainBackground.writeToParcel(parcel, i);
        this.topComplicationBackground.writeToParcel(parcel, i);
        this.rightComplicationBackground.writeToParcel(parcel, i);
        this.bottomComplicationBackground.writeToParcel(parcel, i);
        this.leftComplicationBackground.writeToParcel(parcel, i);
    }

    @DexIgnore
    public BackgroundConfig(long j, BackgroundImgData backgroundImgData, BackgroundImgData backgroundImgData2, BackgroundImgData backgroundImgData3, BackgroundImgData backgroundImgData4, BackgroundImgData backgroundImgData5) {
        wd4.b(backgroundImgData, "mainBackground");
        wd4.b(backgroundImgData2, "topComplicationBackground");
        wd4.b(backgroundImgData3, "rightComplicationBackground");
        wd4.b(backgroundImgData4, "bottomComplicationBackground");
        wd4.b(backgroundImgData5, "leftComplicationBackground");
        this.timestamp = j;
        this.mainBackground = backgroundImgData;
        this.topComplicationBackground = backgroundImgData2;
        this.rightComplicationBackground = backgroundImgData3;
        this.bottomComplicationBackground = backgroundImgData4;
        this.leftComplicationBackground = backgroundImgData5;
    }

    @DexIgnore
    public BackgroundConfig(Parcel parcel) {
        this.timestamp = parcel.readLong();
        this.mainBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.topComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.rightComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.bottomComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.leftComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
    }
}
