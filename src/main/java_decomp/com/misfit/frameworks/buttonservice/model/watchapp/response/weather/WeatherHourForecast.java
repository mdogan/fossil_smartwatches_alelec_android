package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherHourForecast implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((rd4) null);
    @DexIgnore
    public /* final */ int hourIn24Format;
    @DexIgnore
    public /* final */ float temperature;
    @DexIgnore
    public /* final */ WeatherComplicationAppInfo.WeatherCondition weatherCondition;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherHourForecast> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(rd4 rd4) {
            this();
        }

        @DexIgnore
        public WeatherHourForecast createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new WeatherHourForecast(parcel);
        }

        @DexIgnore
        public WeatherHourForecast[] newArray(int i) {
            return new WeatherHourForecast[i];
        }
    }

    @DexIgnore
    public WeatherHourForecast(int i, float f, WeatherComplicationAppInfo.WeatherCondition weatherCondition2) {
        wd4.b(weatherCondition2, "weatherCondition");
        this.hourIn24Format = (i < 0 || 24 <= i) ? 0 : i;
        this.temperature = f;
        this.weatherCondition = weatherCondition2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final com.fossil.blesdk.device.data.weather.WeatherHourForecast toSDKWeatherHourForecast() {
        return new com.fossil.blesdk.device.data.weather.WeatherHourForecast(this.hourIn24Format, this.temperature, this.weatherCondition.toSdkWeatherCondition());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeInt(this.hourIn24Format);
        parcel.writeFloat(this.temperature);
        parcel.writeInt(this.weatherCondition.ordinal());
    }

    @DexIgnore
    public WeatherHourForecast(Parcel parcel) {
        wd4.b(parcel, "parcel");
        this.hourIn24Format = parcel.readInt();
        this.temperature = parcel.readFloat();
        this.weatherCondition = WeatherComplicationAppInfo.WeatherCondition.values()[parcel.readInt()];
    }
}
