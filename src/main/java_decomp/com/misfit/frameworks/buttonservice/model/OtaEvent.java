package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class OtaEvent implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((rd4) null);
    @DexIgnore
    public float process;
    @DexIgnore
    public String serial;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<OtaEvent> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(rd4 rd4) {
            this();
        }

        @DexIgnore
        public OtaEvent createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new OtaEvent(parcel);
        }

        @DexIgnore
        public OtaEvent[] newArray(int i) {
            return new OtaEvent[i];
        }
    }

    @DexIgnore
    public OtaEvent(String str, float f) {
        wd4.b(str, "serial");
        this.serial = str;
        this.process = f;
    }

    @DexIgnore
    public static /* synthetic */ OtaEvent copy$default(OtaEvent otaEvent, String str, float f, int i, Object obj) {
        if ((i & 1) != 0) {
            str = otaEvent.serial;
        }
        if ((i & 2) != 0) {
            f = otaEvent.process;
        }
        return otaEvent.copy(str, f);
    }

    @DexIgnore
    public final String component1() {
        return this.serial;
    }

    @DexIgnore
    public final float component2() {
        return this.process;
    }

    @DexIgnore
    public final OtaEvent copy(String str, float f) {
        wd4.b(str, "serial");
        return new OtaEvent(str, f);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof OtaEvent)) {
            return false;
        }
        OtaEvent otaEvent = (OtaEvent) obj;
        return wd4.a((Object) this.serial, (Object) otaEvent.serial) && Float.compare(this.process, otaEvent.process) == 0;
    }

    @DexIgnore
    public final float getProcess() {
        return this.process;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.serial;
        return ((str != null ? str.hashCode() : 0) * 31) + Float.floatToIntBits(this.process);
    }

    @DexIgnore
    public final void setProcess(float f) {
        this.process = f;
    }

    @DexIgnore
    public final void setSerial(String str) {
        wd4.b(str, "<set-?>");
        this.serial = str;
    }

    @DexIgnore
    public String toString() {
        return "OtaEvent(serial=" + this.serial + ", process=" + this.process + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeString(this.serial);
        parcel.writeFloat(this.process);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public OtaEvent(Parcel parcel) {
        this(r0 == null ? "" : r0, parcel.readFloat());
        wd4.b(parcel, "parcel");
        String readString = parcel.readString();
    }
}
