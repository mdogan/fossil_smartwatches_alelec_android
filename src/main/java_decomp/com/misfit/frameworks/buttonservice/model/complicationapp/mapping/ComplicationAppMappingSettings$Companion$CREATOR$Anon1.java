package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationAppMappingSettings$Companion$CREATOR$Anon1 implements Parcelable.Creator<ComplicationAppMappingSettings> {
    @DexIgnore
    public ComplicationAppMappingSettings createFromParcel(Parcel parcel) {
        wd4.b(parcel, "parcel");
        return new ComplicationAppMappingSettings(parcel, (rd4) null);
    }

    @DexIgnore
    public ComplicationAppMappingSettings[] newArray(int i) {
        return new ComplicationAppMappingSettings[i];
    }
}
