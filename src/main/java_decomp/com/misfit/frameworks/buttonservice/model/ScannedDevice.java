package com.misfit.frameworks.buttonservice.model;

import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.location.DeviceLocation;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ScannedDevice {
    @DexIgnore
    public String deviceMACAddress;
    @DexIgnore
    public String deviceName;
    @DexIgnore
    public String deviceSerial;
    @DexIgnore
    public int rssi;
    @DexIgnore
    public long time; // = System.currentTimeMillis();

    @DexIgnore
    public ScannedDevice(String str, String str2, String str3, int i) {
        wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        wd4.b(str2, "deviceName");
        wd4.b(str3, "deviceMACAddress");
        this.deviceSerial = str;
        this.deviceName = str2;
        this.deviceMACAddress = str3;
        this.rssi = i;
    }

    @DexIgnore
    public final void clone(ScannedDevice scannedDevice) {
        wd4.b(scannedDevice, "from");
        this.deviceSerial = scannedDevice.deviceSerial;
        this.deviceMACAddress = scannedDevice.deviceMACAddress;
        this.time = scannedDevice.time;
        this.rssi = scannedDevice.rssi;
    }

    @DexIgnore
    public final String getDeviceMACAddress() {
        return this.deviceMACAddress;
    }

    @DexIgnore
    public final String getDeviceName() {
        return this.deviceName;
    }

    @DexIgnore
    public final String getDeviceSerial() {
        return this.deviceSerial;
    }

    @DexIgnore
    public final int getRssi() {
        return this.rssi;
    }

    @DexIgnore
    public final long getTime() {
        return this.time;
    }

    @DexIgnore
    public final void setDeviceMACAddress(String str) {
        wd4.b(str, "<set-?>");
        this.deviceMACAddress = str;
    }

    @DexIgnore
    public final void setDeviceName(String str) {
        wd4.b(str, "<set-?>");
        this.deviceName = str;
    }

    @DexIgnore
    public final void setDeviceSerial(String str) {
        wd4.b(str, "<set-?>");
        this.deviceSerial = str;
    }

    @DexIgnore
    public final void setRssi(int i) {
        this.rssi = i;
    }

    @DexIgnore
    public final void setTime(long j) {
        this.time = j;
    }
}
