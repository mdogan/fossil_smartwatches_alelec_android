package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.os.Parcel;
import com.fossil.blesdk.model.microapp.customization.MicroAppCustomization;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BLENonCustomization extends BLECustomization {
    @DexIgnore
    public BLENonCustomization() {
        super(0);
    }

    @DexIgnore
    public MicroAppCustomization getCustomizationFrame() {
        return null;
    }

    @DexIgnore
    public String getHash() {
        return getType() + ":non";
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BLENonCustomization(Parcel parcel) {
        super(parcel);
        wd4.b(parcel, "in");
    }
}
