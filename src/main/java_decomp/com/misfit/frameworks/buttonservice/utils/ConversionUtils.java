package com.misfit.frameworks.buttonservice.utils;

import android.text.TextUtils;
import com.facebook.internal.Utility;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ConversionUtils {
    @DexIgnore
    public static String SHA1(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_SHA1);
            instance.update(str.getBytes("UTF-8"));
            return byteArrayToString(instance.digest());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @DexIgnore
    public static String byteArrayToString(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte valueOf : bArr) {
            sb.append(String.format(Locale.US, "%02x", new Object[]{Byte.valueOf(valueOf)}));
        }
        return sb.toString();
    }

    @DexIgnore
    public static int getTimezoneRawOffsetById(String str) {
        if (TextUtils.isEmpty(str)) {
            return 1024;
        }
        TimeZone timeZone = TimeZone.getTimeZone(str);
        if (timeZone.inDaylightTime(Calendar.getInstance().getTime())) {
            return (timeZone.getRawOffset() + timeZone.getDSTSavings()) / 60000;
        }
        return timeZone.getRawOffset() / 60000;
    }
}
