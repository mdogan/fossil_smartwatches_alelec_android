package com.misfit.frameworks.buttonservice.utils;

import com.fossil.blesdk.obfuscated.id4;
import com.misfit.frameworks.buttonservice.utils.NotificationUtils;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationUtils$Companion$instance$Anon2 extends Lambda implements id4<NotificationUtils> {
    @DexIgnore
    public static /* final */ NotificationUtils$Companion$instance$Anon2 INSTANCE; // = new NotificationUtils$Companion$instance$Anon2();

    @DexIgnore
    public NotificationUtils$Companion$instance$Anon2() {
        super(0);
    }

    @DexIgnore
    public final NotificationUtils invoke() {
        return NotificationUtils.Singleton.INSTANCE.getINSTANCE();
    }
}
