package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import android.text.TextUtils;
import com.facebook.internal.Utility;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.obfuscated.gq4;
import com.fossil.blesdk.obfuscated.hq4;
import com.fossil.blesdk.obfuscated.op4;
import com.fossil.blesdk.obfuscated.sp4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.source.Injection;
import java.security.MessageDigest;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FirmwareUtils {
    @DexIgnore
    public static /* final */ FirmwareUtils INSTANCE; // = new FirmwareUtils();
    @DexIgnore
    public static /* final */ String TAG; // = TAG;

    @DexIgnore
    private final String bytesToString(byte[] bArr) {
        int length = bArr.length;
        String str = "";
        int i = 0;
        while (i < length) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            String num = Integer.toString((bArr[i] & FileType.MASKED_INDEX) + 256, 16);
            wd4.a((Object) num, "Integer.toString((input[\u2026() and 0xff) + 0x100, 16)");
            if (num != null) {
                String substring = num.substring(1);
                wd4.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                sb.append(substring);
                str = sb.toString();
                i++;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
        if (str != null) {
            String lowerCase = str.toLowerCase();
            wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            return lowerCase;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final boolean isLatestFirmware(String str, String str2) {
        wd4.b(str, "oldFw");
        wd4.b(str2, "latestFw");
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return true;
        }
        if (new hq4(str2).compareTo((gq4) new hq4(str)) >= 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:13:? A[RETURN, SYNTHETIC] */
    public final byte[] readFirmware(FirmwareData firmwareData, Context context) {
        byte[] bArr;
        wd4.b(firmwareData, "firmwareData");
        wd4.b(context, "context");
        String firmwareVersion = firmwareData.getFirmwareVersion();
        if (!TextUtils.isEmpty(firmwareVersion)) {
            if (firmwareData.isEmbedded()) {
                try {
                    bArr = op4.a(context.getResources().openRawResource(firmwareData.getRawBundleResource()));
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.e(str, "readFirmware() - e=" + e);
                }
            } else {
                bArr = Injection.INSTANCE.provideFilesRepository(context).readFirmware(firmwareVersion);
            }
            return bArr == null ? bArr : new byte[0];
        }
        bArr = null;
        if (bArr == null) {
        }
    }

    @DexIgnore
    public final boolean verifyFirmware(byte[] bArr, String str) {
        wd4.b(str, "checksum");
        if (!sp4.b(str) && bArr != null) {
            try {
                MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
                instance.update(bArr);
                byte[] digest = instance.digest();
                wd4.a((Object) digest, "md5");
                String bytesToString = bytesToString(digest);
                String lowerCase = str.toLowerCase();
                wd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                return wd4.a((Object) lowerCase, (Object) bytesToString);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local.e(str2, "Error inside " + TAG + ".verifyFirmware - e=" + e);
            }
        }
        return false;
    }
}
