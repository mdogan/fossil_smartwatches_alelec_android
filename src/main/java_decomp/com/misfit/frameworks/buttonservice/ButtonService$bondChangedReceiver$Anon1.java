package com.misfit.frameworks.buttonservice;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ButtonService$bondChangedReceiver$Anon1 extends BroadcastReceiver {
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        wd4.b(context, "context");
        wd4.b(intent, "intent");
        BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
        if (bluetoothDevice != null) {
            switch (intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -1)) {
                case 10:
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String access$getTAG$cp = ButtonService.TAG;
                    local.d(access$getTAG$cp, "bondChangedReceiver - " + bluetoothDevice.getAddress() + ", bond state changed: NONE");
                    return;
                case 11:
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String access$getTAG$cp2 = ButtonService.TAG;
                    local2.d(access$getTAG$cp2, "bondChangedReceiver - " + bluetoothDevice.getAddress() + ", bond state changed: BONDING");
                    return;
                case 12:
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String access$getTAG$cp3 = ButtonService.TAG;
                    local3.d(access$getTAG$cp3, "bondChangedReceiver - " + bluetoothDevice.getAddress() + ", bond state changed: BONDED");
                    return;
                default:
                    return;
            }
        }
    }
}
