package com.misfit.frameworks.buttonservice;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.facebook.places.model.PlaceFields;
import com.fossil.blesdk.adapter.ScanError;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.ya0;
import com.fossil.fitness.FitnessData;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import com.misfit.frameworks.buttonservice.communite.CommunicateManager;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.ICalibrationSession;
import com.misfit.frameworks.buttonservice.communite.ble.ISyncSession;
import com.misfit.frameworks.buttonservice.db.DataFileProvider;
import com.misfit.frameworks.buttonservice.db.HeartRateProvider;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.enums.DeviceErrorState;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.enums.SupportedDevice;
import com.misfit.frameworks.buttonservice.extensions.AlarmExtensionKt;
import com.misfit.frameworks.buttonservice.interfaces.SyncProfileCallback;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import com.misfit.frameworks.buttonservice.log.MFOtaLog;
import com.misfit.frameworks.buttonservice.log.MFSetupLog;
import com.misfit.frameworks.buttonservice.log.MFSyncLog;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.AppInfo;
import com.misfit.frameworks.buttonservice.model.DeviceTask;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.Mapping;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.model.ScannedDevice;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.DeviceUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.buttonservice.utils.NotificationUtils;
import com.misfit.frameworks.buttonservice.utils.SharePreferencesUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Gesture;
import com.zendesk.sdk.network.impl.DeviceInfo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ButtonService extends Service implements SyncProfileCallback, BleCommunicator.CommunicationResultCallback {
    @DexIgnore
    public static String ACTION_ANALYTIC_EVENT; // = ".analytic_event";
    @DexIgnore
    public static String ACTION_CONNECTION_STATE_CHANGE; // = ".action_connection_state_change";
    @DexIgnore
    public static String ACTION_GET_ALARM; // = ".action_get_alarm";
    @DexIgnore
    public static String ACTION_GET_COUNTDOWN; // = ".action_get_countdown";
    @DexIgnore
    public static String ACTION_LOCATION_CHANGED; // = ".action_location_changed";
    @DexIgnore
    public static String ACTION_OTA_PROGRESS; // = ".action_service_ota_progress";
    @DexIgnore
    public static String ACTION_RECEIVE_HEART_RATE_NOTIFICATION; // = ".action_receive_heart_rate_notification";
    @DexIgnore
    public static String ACTION_RETRIEVE_HEART_RATE_FILE; // = ".action_retrieve_heart_rate_file";
    @DexIgnore
    public static String ACTION_SCAN_DEVICE_FOUND; // = ".scan_device_found";
    @DexIgnore
    public static String ACTION_SERVICE_BLE_RESPONSE; // = ".action_service_fw_response";
    @DexIgnore
    public static String ACTION_SERVICE_DEVICE_APP_EVENT; // = ".action_service_device_app_event";
    @DexIgnore
    public static String ACTION_SERVICE_HEARTBEAT_DATA; // = ".action_service_heartbeat_data";
    @DexIgnore
    public static String ACTION_SERVICE_MICRO_APP_CANCEL_EVENT; // = ".action_service_micro_app_cancel_event";
    @DexIgnore
    public static String ACTION_SERVICE_MICRO_APP_EVENT; // = ".action_service_micro_app_event";
    @DexIgnore
    public static String ACTION_SERVICE_MUSIC_EVENT; // = ".action_service_music_event";
    @DexIgnore
    public static String ACTION_SERVICE_STREAMING_EVENT; // = ".action_service_streaming_event";
    @DexIgnore
    public static /* final */ String ACTION_STREAMING_STOP; // = ACTION_STREAMING_STOP;
    @DexIgnore
    public static /* final */ String ACTION_VERIFY_SECRET_KEY; // = ".sevice_action_verify_secret_key";
    @DexIgnore
    public static /* final */ String ALARM_GET_ALARM; // = ALARM_GET_ALARM;
    @DexIgnore
    public static /* final */ String ALARM_HOUR; // = ALARM_HOUR;
    @DexIgnore
    public static /* final */ String ALARM_IS_REPEAT; // = ALARM_IS_REPEAT;
    @DexIgnore
    public static /* final */ String ALARM_MINUTE; // = ALARM_MINUTE;
    @DexIgnore
    public static /* final */ String ALARM_VERIFY_LIST_ALARM; // = ALARM_VERIFY_LIST_ALARM;
    @DexIgnore
    public static /* final */ String APP_INFO; // = APP_INFO;
    @DexIgnore
    public static String BLUETOOTH_NOT_SUPPORTED; // = ".action.countdown.bluetooth.notsupport";
    @DexIgnore
    public static /* final */ long CONNECT_TIMEOUT; // = CONNECT_TIMEOUT;
    @DexIgnore
    public static /* final */ String COUNTDOWN_END; // = COUNTDOWN_END;
    @DexIgnore
    public static /* final */ String COUNTDOWN_PROGRESS; // = COUNTDOWN_PROGRESS;
    @DexIgnore
    public static /* final */ String COUNTDOWN_START; // = COUNTDOWN_START;
    @DexIgnore
    public static /* final */ String COUNTDOWN_TIMEZONE_OFFSET; // = COUNTDOWN_TIMEZONE_OFFSET;
    @DexIgnore
    public static /* final */ String CURRENT_WATCH_PARAMS_VERSION; // = "currentWatchParamVersion";
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String DEVICE_FAMILY; // = "deviceFamily";
    @DexIgnore
    public static /* final */ String DEVICE_MODEL; // = "deviceModel";
    @DexIgnore
    public static /* final */ String DEVICE_RANDOM_KEY; // = "randomKeyFromDevice";
    @DexIgnore
    public static /* final */ String DEVICE_SECRET_KEY; // = "deviceSecretKey";
    @DexIgnore
    public static /* final */ int DISABLE_GOAL_TRACKING_ID; // = 255;
    @DexIgnore
    public static /* final */ String FIRMWARE_VERSION; // = "firmwareVersion";
    @DexIgnore
    public static /* final */ String GET_COUNTDOWN_SETTING; // = GET_COUNTDOWN_SETTING;
    @DexIgnore
    public static /* final */ String GET_VIBRATION_STRENGTH; // = GET_VIBRATION_STRENGTH;
    @DexIgnore
    public static /* final */ int GOAL_TRACKING_ID_RANGE; // = 255;
    @DexIgnore
    public static /* final */ String HEART_RATE_DATA_POINT; // = HEART_RATE_DATA_POINT;
    @DexIgnore
    public static /* final */ String HEART_RATE_SYNC_RESULT; // = HEART_RATE_SYNC_RESULT;
    @DexIgnore
    public static /* final */ String LAST_DEVICE_ERROR_STATE; // = LAST_DEVICE_ERROR_STATE;
    @DexIgnore
    public static /* final */ String LIST_PERMISSION_CODES; // = LIST_PERMISSION_CODES;
    @DexIgnore
    public static /* final */ String LOG_ID; // = LOG_ID;
    @DexIgnore
    public static /* final */ String MUSIC_ACTION_EVENT; // = MUSIC_ACTION_EVENT;
    @DexIgnore
    public static /* final */ String ORIGINAL_SYNC_MODE; // = ORIGINAL_SYNC_MODE;
    @DexIgnore
    public static /* final */ String REALTIME_STEPS; // = REALTIME_STEPS;
    @DexIgnore
    public static String SERIAL_DIANA_PREFIX; // = "D";
    @DexIgnore
    public static String SERVICE_ACTION_RESULT; // = ".service_action_result";
    @DexIgnore
    public static String SERVICE_BLE_PHASE; // = ".service_ble_phase";
    @DexIgnore
    public static /* final */ String SET_COUNTDOWN_IS_ENABLE; // = SET_COUNTDOWN_IS_ENABLE;
    @DexIgnore
    public static /* final */ String START_SYNC_TIME; // = START_SYNC_TIME;
    @DexIgnore
    public static /* final */ String SYNC_MODE; // = SYNC_MODE;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ long TIME_STAMP_FOR_NON_EXECUTABLE_METHOD; // = TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
    @DexIgnore
    public static /* final */ String USER_ID; // = USER_ID;
    @DexIgnore
    public static /* final */ String VIBRATION_STRENGTH_LEVEL; // = VIBRATION_STRENGTH_LEVEL;
    @DexIgnore
    public static /* final */ int WAITING_AFTER_BLUETOOTH_ON; // = 20000;
    @DexIgnore
    public static /* final */ String WATCH_PARAMS_MAJOR; // = "watchParamMajorVersion";
    @DexIgnore
    public static /* final */ String WATCH_PARAMS_MINOR; // = "watchParamMinorVersion";
    @DexIgnore
    public static /* final */ String WATCH_PARAMS_VERSION; // = "watchParamsVersion";
    @DexIgnore
    public static AppInfo appInfo;
    @DexIgnore
    public static FossilDeviceSerialPatternUtil.BRAND fossilBrand;
    @DexIgnore
    public /* final */ ButtonService$bluetoothReceiver$Anon1 bluetoothReceiver; // = new ButtonService$bluetoothReceiver$Anon1(this);
    @DexIgnore
    public /* final */ ButtonService$bondChangedReceiver$Anon1 bondChangedReceiver; // = new ButtonService$bondChangedReceiver$Anon1();
    @DexIgnore
    public IButtonConnectivity.Stub buttonServiceHub; // = new ButtonService$buttonServiceHub$Anon1(this);
    @DexIgnore
    public Set<String> connectQueue;
    @DexIgnore
    public long lastBluetoothOn;
    @DexIgnore
    public ScanServiceInstance scanServiceInstance;
    @DexIgnore
    public ConcurrentHashMap<String, ScannedDevice> scannedDevices;
    @DexIgnore
    public int state; // = 12;
    @DexIgnore
    public HandlerThread thread; // = new HandlerThread(TAG);
    @DexIgnore
    public /* final */ ButtonService$timeZoneChangeReceiver$Anon1 timeZoneChangeReceiver; // = new ButtonService$timeZoneChangeReceiver$Anon1(this);
    @DexIgnore
    public String userId;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[FossilDeviceSerialPatternUtil.BRAND.values().length];

            /*
            static {
                $EnumSwitchMapping$Anon0[FossilDeviceSerialPatternUtil.BRAND.CHAPS.ordinal()] = 1;
                $EnumSwitchMapping$Anon0[FossilDeviceSerialPatternUtil.BRAND.DIESEL.ordinal()] = 2;
                $EnumSwitchMapping$Anon0[FossilDeviceSerialPatternUtil.BRAND.EA.ordinal()] = 3;
                $EnumSwitchMapping$Anon0[FossilDeviceSerialPatternUtil.BRAND.FOSSIL.ordinal()] = 4;
                $EnumSwitchMapping$Anon0[FossilDeviceSerialPatternUtil.BRAND.KATE_SPADE.ordinal()] = 5;
                $EnumSwitchMapping$Anon0[FossilDeviceSerialPatternUtil.BRAND.MICHAEL_KORS.ordinal()] = 6;
                $EnumSwitchMapping$Anon0[FossilDeviceSerialPatternUtil.BRAND.SKAGEN.ordinal()] = 7;
                $EnumSwitchMapping$Anon0[FossilDeviceSerialPatternUtil.BRAND.ARMANI_EXCHANGE.ordinal()] = 8;
                $EnumSwitchMapping$Anon0[FossilDeviceSerialPatternUtil.BRAND.RELIC.ordinal()] = 9;
                $EnumSwitchMapping$Anon0[FossilDeviceSerialPatternUtil.BRAND.MARC_JACOBS.ordinal()] = 10;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        private final void setAppInfo(AppInfo appInfo) {
            ButtonService.appInfo = appInfo;
        }

        @DexIgnore
        public final void broadcastAnalyticEvent(Context context, String str) {
            wd4.b(context, "context");
            wd4.b(str, "message");
            Intent intent = new Intent();
            intent.putExtra("message", str);
            intent.setAction(getACTION_ANALYTIC_EVENT());
            context.sendBroadcast(intent);
        }

        @DexIgnore
        public final String getACTION_ANALYTIC_EVENT() {
            return ButtonService.ACTION_ANALYTIC_EVENT;
        }

        @DexIgnore
        public final String getACTION_CONNECTION_STATE_CHANGE() {
            return ButtonService.ACTION_CONNECTION_STATE_CHANGE;
        }

        @DexIgnore
        public final String getACTION_GET_ALARM() {
            return ButtonService.ACTION_GET_ALARM;
        }

        @DexIgnore
        public final String getACTION_GET_COUNTDOWN() {
            return ButtonService.ACTION_GET_COUNTDOWN;
        }

        @DexIgnore
        public final String getACTION_LOCATION_CHANGED() {
            return ButtonService.ACTION_LOCATION_CHANGED;
        }

        @DexIgnore
        public final String getACTION_OTA_PROGRESS() {
            return ButtonService.ACTION_OTA_PROGRESS;
        }

        @DexIgnore
        public final String getACTION_RECEIVE_HEART_RATE_NOTIFICATION() {
            return ButtonService.ACTION_RECEIVE_HEART_RATE_NOTIFICATION;
        }

        @DexIgnore
        public final String getACTION_RETRIEVE_HEART_RATE_FILE() {
            return ButtonService.ACTION_RETRIEVE_HEART_RATE_FILE;
        }

        @DexIgnore
        public final String getACTION_SCAN_DEVICE_FOUND() {
            return ButtonService.ACTION_SCAN_DEVICE_FOUND;
        }

        @DexIgnore
        public final String getACTION_SERVICE_BLE_RESPONSE() {
            return ButtonService.ACTION_SERVICE_BLE_RESPONSE;
        }

        @DexIgnore
        public final String getACTION_SERVICE_DEVICE_APP_EVENT() {
            return ButtonService.ACTION_SERVICE_DEVICE_APP_EVENT;
        }

        @DexIgnore
        public final String getACTION_SERVICE_HEARTBEAT_DATA() {
            return ButtonService.ACTION_SERVICE_HEARTBEAT_DATA;
        }

        @DexIgnore
        public final String getACTION_SERVICE_MICRO_APP_CANCEL_EVENT() {
            return ButtonService.ACTION_SERVICE_MICRO_APP_CANCEL_EVENT;
        }

        @DexIgnore
        public final String getACTION_SERVICE_MICRO_APP_EVENT() {
            return ButtonService.ACTION_SERVICE_MICRO_APP_EVENT;
        }

        @DexIgnore
        public final String getACTION_SERVICE_MUSIC_EVENT() {
            return ButtonService.ACTION_SERVICE_MUSIC_EVENT;
        }

        @DexIgnore
        public final String getACTION_SERVICE_STREAMING_EVENT() {
            return ButtonService.ACTION_SERVICE_STREAMING_EVENT;
        }

        @DexIgnore
        public final String getACTION_STREAMING_STOP() {
            return ButtonService.ACTION_STREAMING_STOP;
        }

        @DexIgnore
        public final String getALARM_GET_ALARM() {
            return ButtonService.ALARM_GET_ALARM;
        }

        @DexIgnore
        public final String getALARM_HOUR() {
            return ButtonService.ALARM_HOUR;
        }

        @DexIgnore
        public final String getALARM_IS_REPEAT() {
            return ButtonService.ALARM_IS_REPEAT;
        }

        @DexIgnore
        public final String getALARM_MINUTE() {
            return ButtonService.ALARM_MINUTE;
        }

        @DexIgnore
        public final String getALARM_VERIFY_LIST_ALARM() {
            return ButtonService.ALARM_VERIFY_LIST_ALARM;
        }

        @DexIgnore
        public final AppInfo getAppInfo() {
            return ButtonService.appInfo;
        }

        @DexIgnore
        public final String getBLUETOOTH_NOT_SUPPORTED() {
            return ButtonService.BLUETOOTH_NOT_SUPPORTED;
        }

        @DexIgnore
        public final String getCOUNTDOWN_END() {
            return ButtonService.COUNTDOWN_END;
        }

        @DexIgnore
        public final String getCOUNTDOWN_PROGRESS() {
            return ButtonService.COUNTDOWN_PROGRESS;
        }

        @DexIgnore
        public final String getCOUNTDOWN_START() {
            return ButtonService.COUNTDOWN_START;
        }

        @DexIgnore
        public final String getCOUNTDOWN_TIMEZONE_OFFSET() {
            return ButtonService.COUNTDOWN_TIMEZONE_OFFSET;
        }

        @DexIgnore
        public final String getDEVICE_FAMILY() {
            return ButtonService.DEVICE_FAMILY;
        }

        @DexIgnore
        public final int getDISABLE_GOAL_TRACKING_ID() {
            return ButtonService.DISABLE_GOAL_TRACKING_ID;
        }

        @DexIgnore
        public final String getGET_COUNTDOWN_SETTING() {
            return ButtonService.GET_COUNTDOWN_SETTING;
        }

        @DexIgnore
        public final String getGET_VIBRATION_STRENGTH() {
            return ButtonService.GET_VIBRATION_STRENGTH;
        }

        @DexIgnore
        public final int getGOAL_TRACKING_ID_RANGE() {
            return ButtonService.GOAL_TRACKING_ID_RANGE;
        }

        @DexIgnore
        public final String getHEART_RATE_DATA_POINT() {
            return ButtonService.HEART_RATE_DATA_POINT;
        }

        @DexIgnore
        public final String getHEART_RATE_SYNC_RESULT() {
            return ButtonService.HEART_RATE_SYNC_RESULT;
        }

        @DexIgnore
        public final String getLAST_DEVICE_ERROR_STATE() {
            return ButtonService.LAST_DEVICE_ERROR_STATE;
        }

        @DexIgnore
        public final String getLIST_PERMISSION_CODES() {
            return ButtonService.LIST_PERMISSION_CODES;
        }

        @DexIgnore
        public final String getLOG_ID() {
            return ButtonService.LOG_ID;
        }

        @DexIgnore
        public final String getMUSIC_ACTION_EVENT() {
            return ButtonService.MUSIC_ACTION_EVENT;
        }

        @DexIgnore
        public final String getORIGINAL_SYNC_MODE() {
            return ButtonService.ORIGINAL_SYNC_MODE;
        }

        @DexIgnore
        public final String getREALTIME_STEPS() {
            return ButtonService.REALTIME_STEPS;
        }

        @DexIgnore
        public final String getSDKVersion() {
            return "5.8.5-production-release";
        }

        @DexIgnore
        public final String getSERIAL_DIANA_PREFIX() {
            return ButtonService.SERIAL_DIANA_PREFIX;
        }

        @DexIgnore
        public final String getSERVICE_ACTION_RESULT() {
            return ButtonService.SERVICE_ACTION_RESULT;
        }

        @DexIgnore
        public final String getSERVICE_BLE_PHASE() {
            return ButtonService.SERVICE_BLE_PHASE;
        }

        @DexIgnore
        public final String getSET_COUNTDOWN_IS_ENABLE() {
            return ButtonService.SET_COUNTDOWN_IS_ENABLE;
        }

        @DexIgnore
        public final String getSTART_SYNC_TIME() {
            return ButtonService.START_SYNC_TIME;
        }

        @DexIgnore
        public final String getSYNC_MODE() {
            return ButtonService.SYNC_MODE;
        }

        @DexIgnore
        public final String getSdkVersionV2() {
            return "5.8.5-production-release";
        }

        @DexIgnore
        public final List<String> getSupportedDevices(FossilDeviceSerialPatternUtil.BRAND brand) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(SupportedDevice.SAM.getName());
            arrayList.add(SupportedDevice.SAM_FACTORY.getName());
            int i = 0;
            if (brand != null) {
                switch (WhenMappings.$EnumSwitchMapping$Anon0[brand.ordinal()]) {
                    case 1:
                        arrayList.add(SupportedDevice.SAM_CHAP.getName());
                        arrayList.add(SupportedDevice.SAM_CHAP_FULL_OLD.getName());
                        arrayList.add(SupportedDevice.SAM_CHAP_FULL_NEW.getName());
                        break;
                    case 2:
                        arrayList.add(SupportedDevice.SAM_DIESEL.getName());
                        arrayList.add(SupportedDevice.SAM_DIESEL_FULL_OLD.getName());
                        arrayList.add(SupportedDevice.SAM_DIESEL_FULL_NEW.getName());
                        break;
                    case 3:
                        arrayList.add(SupportedDevice.SAM_ARMANI.getName());
                        arrayList.add(SupportedDevice.SAM_ARMANI_FULL_OLD.getName());
                        arrayList.add(SupportedDevice.SAM_ARMANI_FULL_NEW.getName());
                        break;
                    case 4:
                        arrayList.add(SupportedDevice.SAM_FOSSIL.getName());
                        arrayList.add(SupportedDevice.SAM_FOSSIL_FULL_OLD.getName());
                        arrayList.add(SupportedDevice.SAM_FOSSIL_FULL_NEW.getName());
                        arrayList.add(SupportedDevice.QMOTION.getName());
                        break;
                    case 5:
                        arrayList.add(SupportedDevice.SAM_KATE_SPADE.getName());
                        arrayList.add(SupportedDevice.SAM_KATE_SPADE_FULL_OLD.getName());
                        arrayList.add(SupportedDevice.SAM_KATE_SPADE_FULL_NEW.getName());
                        arrayList.add(SupportedDevice.KATE_SPADE.getName());
                        break;
                    case 6:
                        arrayList.add(SupportedDevice.SAM_MICHAEL_KORS.getName());
                        arrayList.add(SupportedDevice.SAM_MK_FULL_OLD.getName());
                        arrayList.add(SupportedDevice.SAM_MK_FULL_NEW.getName());
                        arrayList.add(SupportedDevice.MICHAEL_KORS.getName());
                        break;
                    case 7:
                        arrayList.add(SupportedDevice.SAM_SKAGEN.getName());
                        arrayList.add(SupportedDevice.SAM_SKAGEN_FULL_OLD.getName());
                        arrayList.add(SupportedDevice.SAM_SKAGEN_FULL_NEW.getName());
                        break;
                    case 8:
                        arrayList.add(SupportedDevice.SAM_ARMANI_EXCHANGE.getName());
                        arrayList.add(SupportedDevice.SAM_ARMANI_EXCHANGE_FULL_OLD.getName());
                        arrayList.add(SupportedDevice.SAM_ARMANI_EXCHANGE_FULL_NEW.getName());
                        break;
                    case 9:
                        arrayList.add(SupportedDevice.SAM_RELIC.getName());
                        arrayList.add(SupportedDevice.SAM_RELIC_FULL_NEW.getName());
                        break;
                    case 10:
                        arrayList.add(SupportedDevice.SAM_MARC_JACOBS.getName());
                        arrayList.add(SupportedDevice.SAM_MARC_JACOBS_FULL_NEW.getName());
                        break;
                    default:
                        SupportedDevice[] values = SupportedDevice.values();
                        int length = values.length;
                        while (i < length) {
                            arrayList.add(values[i].getName());
                            i++;
                        }
                        break;
                }
            } else {
                SupportedDevice[] values2 = SupportedDevice.values();
                int length2 = values2.length;
                while (i < length2) {
                    arrayList.add(values2[i].getName());
                    i++;
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = ButtonService.TAG;
            local.d(access$getTAG$cp, "Inside " + ButtonService.TAG + ".getSupportedDevices=" + arrayList);
            return arrayList;
        }

        @DexIgnore
        public final long getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD() {
            return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }

        @DexIgnore
        public final String getVIBRATION_STRENGTH_LEVEL() {
            return ButtonService.VIBRATION_STRENGTH_LEVEL;
        }

        @DexIgnore
        public final void setACTION_ANALYTIC_EVENT(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_ANALYTIC_EVENT = str;
        }

        @DexIgnore
        public final void setACTION_CONNECTION_STATE_CHANGE(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_CONNECTION_STATE_CHANGE = str;
        }

        @DexIgnore
        public final void setACTION_GET_ALARM(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_GET_ALARM = str;
        }

        @DexIgnore
        public final void setACTION_GET_COUNTDOWN(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_GET_COUNTDOWN = str;
        }

        @DexIgnore
        public final void setACTION_LOCATION_CHANGED(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_LOCATION_CHANGED = str;
        }

        @DexIgnore
        public final void setACTION_OTA_PROGRESS(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_OTA_PROGRESS = str;
        }

        @DexIgnore
        public final void setACTION_RECEIVE_HEART_RATE_NOTIFICATION(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_RECEIVE_HEART_RATE_NOTIFICATION = str;
        }

        @DexIgnore
        public final void setACTION_RETRIEVE_HEART_RATE_FILE(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_RETRIEVE_HEART_RATE_FILE = str;
        }

        @DexIgnore
        public final void setACTION_SCAN_DEVICE_FOUND(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_SCAN_DEVICE_FOUND = str;
        }

        @DexIgnore
        public final void setACTION_SERVICE_BLE_RESPONSE(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_SERVICE_BLE_RESPONSE = str;
        }

        @DexIgnore
        public final void setACTION_SERVICE_DEVICE_APP_EVENT(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_SERVICE_DEVICE_APP_EVENT = str;
        }

        @DexIgnore
        public final void setACTION_SERVICE_HEARTBEAT_DATA(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_SERVICE_HEARTBEAT_DATA = str;
        }

        @DexIgnore
        public final void setACTION_SERVICE_MICRO_APP_CANCEL_EVENT(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_SERVICE_MICRO_APP_CANCEL_EVENT = str;
        }

        @DexIgnore
        public final void setACTION_SERVICE_MICRO_APP_EVENT(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_SERVICE_MICRO_APP_EVENT = str;
        }

        @DexIgnore
        public final void setACTION_SERVICE_MUSIC_EVENT(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_SERVICE_MUSIC_EVENT = str;
        }

        @DexIgnore
        public final void setACTION_SERVICE_STREAMING_EVENT(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.ACTION_SERVICE_STREAMING_EVENT = str;
        }

        @DexIgnore
        public final void setBLUETOOTH_NOT_SUPPORTED(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.BLUETOOTH_NOT_SUPPORTED = str;
        }

        @DexIgnore
        public final void setSERIAL_DIANA_PREFIX(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.SERIAL_DIANA_PREFIX = str;
        }

        @DexIgnore
        public final void setSERVICE_ACTION_RESULT(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.SERVICE_ACTION_RESULT = str;
        }

        @DexIgnore
        public final void setSERVICE_BLE_PHASE(String str) {
            wd4.b(str, "<set-?>");
            ButtonService.SERVICE_BLE_PHASE = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ScanServiceInstance implements ScanService.Callback {
        @DexIgnore
        public /* final */ Context context;
        @DexIgnore
        public ScanService scanServiceV2; // = new ScanService(this.context, this, System.currentTimeMillis() / ((long) 1000));
        @DexIgnore
        public /* final */ /* synthetic */ ButtonService this$Anon0;

        @DexIgnore
        public ScanServiceInstance(ButtonService buttonService, Context context2) {
            wd4.b(context2, "context");
            this.this$Anon0 = buttonService;
            this.context = context2;
        }

        @DexIgnore
        public void onDeviceFound(Device device, int i) {
            wd4.b(device, "device");
            DeviceInformation deviceInformation = device.getDeviceInformation();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = ButtonService.TAG;
            local.d(access$getTAG$cp, ".onDeviceFound (v2), serial: " + deviceInformation.getSerialNumber() + ", macAddress: " + deviceInformation.getMacAddress() + ", connectState: " + device.getState() + ", bondState: " + device.getBondState());
            if (device.getState() == Device.State.CONNECTED) {
                i = 0;
            } else if (device.getBondState() == Device.BondState.BONDED) {
                i = ScanService.RETRIEVE_DEVICE_BOND_RSSI_MARK;
            }
            this.this$Anon0.onDeviceFound(new ScannedDevice(deviceInformation.getSerialNumber(), deviceInformation.getName(), deviceInformation.getMacAddress(), i));
        }

        @DexIgnore
        public void onScanFail(ScanError scanError) {
            wd4.b(scanError, "scanError");
        }

        @DexIgnore
        public final void startScan() {
            FLogger.INSTANCE.getLocal().d(ButtonService.TAG, ".startScan()");
            MFLogManager.getInstance(this.context).addLogForActiveLog("", "Start scan device");
            try {
                stopScan();
                this.scanServiceV2 = new ScanService(this.context, this, System.currentTimeMillis() / ((long) 1000));
                ScanService scanService = this.scanServiceV2;
                if (scanService != null) {
                    scanService.setActiveDeviceLog("");
                    MFLogManager.getInstance(this.context).addLogForActiveLog("", "Call 'start scan service'");
                    ScanService scanService2 = this.scanServiceV2;
                    if (scanService2 != null) {
                        scanService2.startScan();
                    } else {
                        wd4.a();
                        throw null;
                    }
                } else {
                    wd4.a();
                    throw null;
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = ButtonService.TAG;
                local.e(access$getTAG$cp, "Error on start scan " + e);
            }
        }

        @DexIgnore
        public final void stopScan() {
            FLogger.INSTANCE.getLocal().d(ButtonService.TAG, ".stopScan()");
            ScanService scanService = this.scanServiceV2;
            if (scanService != null) {
                scanService.stopScan();
            } else {
                wd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[CommunicateMode.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon1; // = new int[CommunicateMode.values().length];

        /*
        static {
            $EnumSwitchMapping$Anon0[CommunicateMode.LINK.ordinal()] = 1;
            $EnumSwitchMapping$Anon0[CommunicateMode.UNLINK.ordinal()] = 2;
            $EnumSwitchMapping$Anon0[CommunicateMode.SWITCH_DEVICE.ordinal()] = 3;
            $EnumSwitchMapping$Anon1[CommunicateMode.SYNC.ordinal()] = 1;
            $EnumSwitchMapping$Anon1[CommunicateMode.MANUAL_SYNC.ordinal()] = 2;
            $EnumSwitchMapping$Anon1[CommunicateMode.OTA.ordinal()] = 3;
            $EnumSwitchMapping$Anon1[CommunicateMode.LINK.ordinal()] = 4;
        }
        */
    }

    /*
    static {
        String simpleName = ButtonService.class.getSimpleName();
        wd4.a((Object) simpleName, "ButtonService::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ Set access$getConnectQueue$p(ButtonService buttonService) {
        Set<String> set = buttonService.connectQueue;
        if (set != null) {
            return set;
        }
        wd4.d("connectQueue");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ScanServiceInstance access$getScanServiceInstance$p(ButtonService buttonService) {
        ScanServiceInstance scanServiceInstance2 = buttonService.scanServiceInstance;
        if (scanServiceInstance2 != null) {
            return scanServiceInstance2;
        }
        wd4.d("scanServiceInstance");
        throw null;
    }

    @DexIgnore
    private final void addLogToActiveLog(String str, String str2) {
        MFLog activeLog = MFLogManager.getInstance(this).getActiveLog(str);
        if (activeLog != null) {
            activeLog.log(str2);
        }
    }

    @DexIgnore
    private final void broadcastHeartRateData(short s, String str) {
        Intent intent = new Intent();
        intent.putExtra(Constants.SERIAL_NUMBER, str);
        intent.putExtra(HEART_RATE_DATA_POINT, s);
        intent.setAction(ACTION_RECEIVE_HEART_RATE_NOTIFICATION);
        sendBroadcast(intent);
    }

    @DexIgnore
    private final void clearDeviceData(String str) {
        if (str != null) {
            ConcurrentHashMap<String, ScannedDevice> concurrentHashMap = this.scannedDevices;
            if (concurrentHashMap != null) {
                concurrentHashMap.remove(str);
                Set<String> set = this.connectQueue;
                if (set != null) {
                    set.remove(str);
                    ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).removeCommunicator(str);
                    return;
                }
                wd4.d("connectQueue");
                throw null;
            }
            wd4.d("scannedDevices");
            throw null;
        }
    }

    @DexIgnore
    private final void confirmStopWorkout(String str, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.i(str2, ".confirmStopWorkout(), stopworkout=" + z);
        ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).confirmStopWorkout(str, z);
    }

    @DexIgnore
    private final void connect(String str) {
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".connect() - serial=" + str + ", gattState=" + communicator.getGattState() + ", hidState=" + communicator.getHidState() + ", communicateMode=" + communicator.getCommunicateMode());
        if (communicator.getCommunicateMode() != CommunicateMode.IDLE) {
            if (communicator.getGattState() == 2) {
                FLogger.INSTANCE.getLocal().i(TAG, ".connect() - Current device has already connected");
                Bundle bundle = new Bundle();
                bundle.putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(communicator.getBleAdapter()));
                broadcastServiceBlePhaseEvent(str, CommunicateMode.RECONNECT, ServiceActionResult.SUCCEEDED, bundle);
                return;
            }
            FLogger.INSTANCE.getLocal().d(TAG, ".connect() - Current syncProfile has pending task. Returning...");
        } else if (DevicePreferenceUtils.getAllActiveButtonSerial(this).contains(str) && communicator.getGattState() == 0) {
            addLogToActiveLog(str, "Connect to device.");
            communicator.startConnectionDeviceSession(false);
        }
    }

    @DexIgnore
    private final ArrayList<Integer> createListErrorState(int i) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        if (i == 1112 || i == 1113) {
            if (!BluetoothUtils.isBluetoothEnable()) {
                arrayList.add(Integer.valueOf(FailureCode.BLUETOOTH_IS_DISABLED));
            }
            if (!LocationUtils.isLocationPermissionGranted(this)) {
                arrayList.add(Integer.valueOf(FailureCode.LOCATION_ACCESS_DENIED));
            }
            if (!LocationUtils.isLocationEnable(this)) {
                arrayList.add(Integer.valueOf(FailureCode.LOCATION_SERVICE_DISABLED));
            }
        } else {
            arrayList.add(Integer.valueOf(i));
        }
        return arrayList;
    }

    @DexIgnore
    private final void deleteDataFiles(String str) {
        int deleteDataFiles = DataFileProvider.getInstance(this).deleteDataFiles(str);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".deleteDataFile(), serial=" + str + " - DONE - count=" + deleteDataFiles);
    }

    @DexIgnore
    private final void deleteHeartRateFiles(List<String> list) {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".deleteHeartRateFile() - count=" + list.size());
            for (String next : list) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "deleteHeartRateFile - id=" + next);
                HeartRateProvider.getInstance(this).deleteDataFile(HeartRateProvider.getInstance(this).getDataFile(next));
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local3.d(str3, ".deleteHeartRateFile() - DONE - count=" + list.size());
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local4.e(str4, ".deleteHeartRateFile() - e=" + e);
        }
    }

    @DexIgnore
    private final synchronized void dequeue() {
        if (BluetoothUtils.isBluetoothEnable()) {
            Set<String> set = this.connectQueue;
            if (set == null) {
                wd4.d("connectQueue");
                throw null;
            } else if (set.size() > 0) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append(".dequeue() - queue=");
                Set<String> set2 = this.connectQueue;
                if (set2 != null) {
                    sb.append(set2);
                    local.d(str, sb.toString());
                    Set<String> set3 = this.connectQueue;
                    if (set3 != null) {
                        String next = set3.iterator().next();
                        if (!TextUtils.isEmpty(next)) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str2 = TAG;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(".dequeue() - serial=");
                            sb2.append(next);
                            sb2.append(", queue=");
                            Set<String> set4 = this.connectQueue;
                            if (set4 != null) {
                                sb2.append(set4);
                                local2.d(str2, sb2.toString());
                                Set<String> set5 = this.connectQueue;
                                if (set5 != null) {
                                    set5.remove(next);
                                    connect(next);
                                } else {
                                    wd4.d("connectQueue");
                                    throw null;
                                }
                            } else {
                                wd4.d("connectQueue");
                                throw null;
                            }
                        }
                        new Handler(Looper.getMainLooper()).post(new ButtonService$dequeue$Anon1(this));
                    } else {
                        wd4.d("connectQueue");
                        throw null;
                    }
                } else {
                    wd4.d("connectQueue");
                    throw null;
                }
            } else {
                FLogger.INSTANCE.getLocal().d(TAG, ".dequeue() - queue is empty. Stop dequeue.");
            }
        } else {
            FLogger.INSTANCE.getLocal().i(TAG, "Bluetooth is OFF, do nothing!!!");
        }
    }

    @DexIgnore
    private final void disconnectAllButton() {
        ScanServiceInstance scanServiceInstance2 = this.scanServiceInstance;
        if (scanServiceInstance2 != null) {
            scanServiceInstance2.stopScan();
            HashSet hashSet = new HashSet(DevicePreferenceUtils.getAllActiveButtonSerial(this));
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".disconnectAll() - remove all serials: " + hashSet);
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                String str2 = (String) it.next();
                try {
                    wd4.a((Object) str2, "serial");
                    deviceDisconnect(str2);
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local2.e(str3, ".disconnectAll() - e=" + e);
                    Thread.currentThread().interrupt();
                }
            }
            Set<String> set = this.connectQueue;
            if (set != null) {
                set.clear();
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str4 = TAG;
                local3.d(str4, ".disconnectAll() - stringSet.size=" + hashSet.size() + ", pairedDevices=" + DevicePreferenceUtils.getAllPairedButtonSerial(this) + ", activeDevices=" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
                return;
            }
            wd4.d("connectQueue");
            throw null;
        }
        wd4.d("scanServiceInstance");
        throw null;
    }

    @DexIgnore
    private final long doUpdateTime(String str) {
        String str2 = TAG;
        Log.d(str2, ".doUpdateTime() for " + str);
        if (!FossilDeviceSerialPatternUtil.isHybridSmartWatchDevice(str)) {
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        long currentTimeMillis = System.currentTimeMillis();
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        if (!communicator.isDeviceReady() || !communicator.startUpdateCurrentTime()) {
            FLogger.INSTANCE.getLocal().e(TAG, ".doUpdateTime() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.UPDATE_CURRENT_TIME, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    private final synchronized void enqueue(String str) {
        if (str != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append(".enqueue() - queue=");
            Set<String> set = this.connectQueue;
            if (set != null) {
                sb.append(set);
                sb.append(", candidate=");
                sb.append(str);
                local.d(str2, sb.toString());
                Set<String> set2 = this.connectQueue;
                if (set2 != null) {
                    if (!set2.contains(str)) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str3 = TAG;
                        local2.d(str3, "Enqueue " + str);
                        Set<String> set3 = this.connectQueue;
                        if (set3 != null) {
                            set3.add(str);
                        } else {
                            wd4.d("connectQueue");
                            throw null;
                        }
                    }
                    new Handler(Looper.getMainLooper()).post(new ButtonService$enqueue$Anon1(this));
                    return;
                }
                wd4.d("connectQueue");
                throw null;
            }
            wd4.d("connectQueue");
            throw null;
        }
    }

    @DexIgnore
    private final int getActiveCommunicatorBySerial(String str) {
        ConcurrentHashMap<String, BleCommunicator> bleCommunicators = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getBleCommunicators();
        if (bleCommunicators.isEmpty()) {
            return CommunicateMode.IDLE.getValue();
        }
        BleCommunicator bleCommunicator = bleCommunicators.get(str);
        if (bleCommunicator == null) {
            return CommunicateMode.IDLE.getValue();
        }
        wd4.a((Object) bleCommunicator, "communicatorConcurrentHa\u2026ommunicateMode.IDLE.value");
        return bleCommunicator.getCommunicateMode().getValue();
    }

    @DexIgnore
    private final int[] getActiveListCommunicator() {
        ConcurrentHashMap<String, BleCommunicator> bleCommunicators = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getBleCommunicators();
        if (bleCommunicators.isEmpty()) {
            return new int[0];
        }
        ArrayList arrayList = new ArrayList();
        int[] iArr = new int[bleCommunicators.size()];
        for (BleCommunicator communicateMode : bleCommunicators.values()) {
            arrayList.add(Integer.valueOf(communicateMode.getCommunicateMode().getValue()));
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            Object obj = arrayList.get(i);
            wd4.a(obj, "communicatorList[i]");
            iArr[i] = ((Number) obj).intValue();
        }
        return iArr;
    }

    @DexIgnore
    private final String getMacAddressFromCache(String str) {
        String macAddress = DeviceUtils.getInstance(this).getMacAddress(this, str);
        wd4.a((Object) macAddress, "DeviceUtils.getInstance(\u2026dress(this, deviceSerial)");
        return macAddress;
    }

    @DexIgnore
    private final List<FitnessData> getSyncData(String str) {
        if (isInActiveDeviceList(str)) {
            return ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).getSyncData();
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".getSyncData() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
        return new ArrayList();
    }

    @DexIgnore
    private final void initReceiverActionWithPackageName() {
        String packageName = getPackageName();
        String str = ACTION_SCAN_DEVICE_FOUND;
        wd4.a((Object) packageName, "packageName");
        if (!StringsKt__StringsKt.a((CharSequence) str, (CharSequence) packageName, false, 2, (Object) null)) {
            ACTION_SCAN_DEVICE_FOUND = packageName + ACTION_SCAN_DEVICE_FOUND;
            ACTION_ANALYTIC_EVENT = packageName + ACTION_ANALYTIC_EVENT;
            ACTION_SERVICE_BLE_RESPONSE = packageName + ACTION_SERVICE_BLE_RESPONSE;
            ACTION_SERVICE_STREAMING_EVENT = packageName + ACTION_SERVICE_STREAMING_EVENT;
            ACTION_SERVICE_MICRO_APP_EVENT = packageName + ACTION_SERVICE_MICRO_APP_EVENT;
            ACTION_SERVICE_MICRO_APP_CANCEL_EVENT = packageName + ACTION_SERVICE_MICRO_APP_CANCEL_EVENT;
            ACTION_SERVICE_DEVICE_APP_EVENT = packageName + ACTION_SERVICE_DEVICE_APP_EVENT;
            ACTION_SERVICE_MUSIC_EVENT = packageName + ACTION_SERVICE_MUSIC_EVENT;
            ACTION_SERVICE_HEARTBEAT_DATA = packageName + ACTION_SERVICE_HEARTBEAT_DATA;
            ACTION_CONNECTION_STATE_CHANGE = packageName + ACTION_CONNECTION_STATE_CHANGE;
            ACTION_OTA_PROGRESS = packageName + ACTION_OTA_PROGRESS;
            ACTION_LOCATION_CHANGED = packageName + ACTION_LOCATION_CHANGED;
            ACTION_GET_ALARM = packageName + ACTION_GET_ALARM;
            BLUETOOTH_NOT_SUPPORTED = packageName + BLUETOOTH_NOT_SUPPORTED;
            ACTION_RECEIVE_HEART_RATE_NOTIFICATION = packageName + ACTION_RECEIVE_HEART_RATE_NOTIFICATION;
            ACTION_RETRIEVE_HEART_RATE_FILE = packageName + ACTION_RETRIEVE_HEART_RATE_FILE;
        }
    }

    @DexIgnore
    private final boolean isInActiveDeviceList(String str) {
        return DevicePreferenceUtils.getAllActiveButtonSerial(this).contains(str);
    }

    @DexIgnore
    private final boolean isInPairedDeviceList(String str) {
        return DevicePreferenceUtils.getAllPairedButtonSerial(this).contains(str);
    }

    @DexIgnore
    private final boolean reconnectRequired(int i, int i2) {
        return i != 2 && i2 == 2;
    }

    @DexIgnore
    private final void registerTimeZoneChangeEvent() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.TIME_TICK");
        intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
        intentFilter.addAction("android.intent.action.TIME_SET");
        registerReceiver(this.timeZoneChangeReceiver, intentFilter);
    }

    @DexIgnore
    private final void saveMacAddressToCache(String str, String str2) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            DeviceUtils.getInstance(this).saveMacAddress(this, str, str2);
        }
    }

    @DexIgnore
    private final void sendDeviceAppResponse(DeviceAppResponse deviceAppResponse, String str, boolean z) {
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".sendDeviceAppResponse() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".sendDeviceAppResponse() - serial=" + str + ", deviceAppResponse: " + deviceAppResponse);
        communicator.startSendDeviceAppResponse(deviceAppResponse, z);
    }

    @DexIgnore
    public static /* synthetic */ void sendDeviceAppResponse$default(ButtonService buttonService, DeviceAppResponse deviceAppResponse, String str, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        buttonService.sendDeviceAppResponse(deviceAppResponse, str, z);
    }

    @DexIgnore
    private final void sendMicroAppRemoteActivity(String str, DeviceAppResponse deviceAppResponse) {
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".sendMicroAppRemoteActivity() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, "Inside " + TAG + ".sendMicroAppRemoteActivity - serial=" + str + ", deviceAppResponse: " + deviceAppResponse);
        BleCommunicator.startSendDeviceAppResponse$default(communicator, deviceAppResponse, false, 2, (Object) null);
    }

    @DexIgnore
    private final void sendMusicAppResponse(MusicResponse musicResponse, String str) {
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".sendMusicAppResponse() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".sendMusicAppResponse() - serial=" + str + ", musicResponse: " + musicResponse);
        communicator.startSendMusicAppResponse(musicResponse);
    }

    @DexIgnore
    public final void addLog(int i, String str, String str2) {
        wd4.b(str, "serial");
        wd4.b(str2, "message");
        MFLogManager.getInstance(this).addLogForActiveLog(str, str2);
    }

    @DexIgnore
    public final void broadcastConnectionStateChange(String str, ConnectionStateChange connectionStateChange) {
        wd4.b(str, "serial");
        wd4.b(connectionStateChange, "status");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "broadcastConnectionStateChange serial " + str + " status " + connectionStateChange);
        Intent intent = new Intent();
        intent.putExtra(Constants.CONNECTION_STATE, connectionStateChange.ordinal());
        intent.putExtra(Constants.SERIAL_NUMBER, str);
        intent.setAction(ACTION_CONNECTION_STATE_CHANGE);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void broadcastDeviceAppEvent(int i, Bundle bundle, String str) {
        wd4.b(bundle, "deviceDataExtra");
        wd4.b(str, "serial");
        Intent intent = new Intent();
        intent.putExtra(Constants.SERIAL_NUMBER, str);
        intent.putExtra(Constants.MICRO_APP_ID, i);
        intent.putExtras(bundle);
        intent.setAction(ACTION_SERVICE_DEVICE_APP_EVENT);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void broadcastDeviceScanFound(ShineDevice shineDevice, int i) {
        wd4.b(shineDevice, "device");
        Intent intent = new Intent();
        intent.putExtra("device", shineDevice);
        intent.putExtra("rssi", i);
        intent.setAction(ACTION_SCAN_DEVICE_FOUND);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void broadcastHeartBeatData(int i, int i2, String str) {
        wd4.b(str, "serial");
        Intent intent = new Intent();
        intent.putExtra(Constants.SERIAL_NUMBER, str);
        intent.putExtra(Constants.DAILY_STEPS, i);
        intent.putExtra(Constants.DAILY_POINTS, i2);
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "Calendar.getInstance()");
        intent.putExtra(Constants.UPDATED_TIME, instance.getTimeInMillis());
        intent.setAction(ACTION_SERVICE_HEARTBEAT_DATA);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void broadcastLocationChanged(Location location) {
        wd4.b(location, PlaceFields.LOCATION);
        Intent intent = new Intent();
        intent.setAction(ACTION_LOCATION_CHANGED);
        intent.putExtra(Constants.LON, location.getLongitude());
        intent.putExtra(Constants.LAT, location.getLatitude());
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void broadcastMicroAppCancelEvent(int i, int i2, Gesture gesture, String str) {
        wd4.b(gesture, "gesture");
        wd4.b(str, "serial");
        Intent intent = new Intent();
        intent.putExtra(Constants.SERIAL_NUMBER, str);
        intent.putExtra("gesture", gesture.getValue());
        intent.putExtra(Constants.MICRO_APP_ID, i);
        intent.putExtra(Constants.VARIANT_ID, i2);
        intent.setAction(ACTION_SERVICE_MICRO_APP_CANCEL_EVENT);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void broadcastMicroAppEvent(int i, int i2, Gesture gesture, String str) {
        wd4.b(gesture, "gesture");
        wd4.b(str, "serial");
        Intent intent = new Intent();
        intent.putExtra(Constants.SERIAL_NUMBER, str);
        intent.putExtra("gesture", gesture.getValue());
        intent.putExtra(Constants.MICRO_APP_ID, i);
        intent.putExtra(Constants.VARIANT_ID, i2);
        intent.setAction(ACTION_SERVICE_MICRO_APP_EVENT);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void broadcastMusicEvent(NotifyMusicEventResponse.MusicMediaAction musicMediaAction, String str) {
        wd4.b(musicMediaAction, "actionEvent");
        wd4.b(str, "serial");
        Intent intent = new Intent();
        intent.putExtra(Constants.SERIAL_NUMBER, str);
        intent.putExtra(MUSIC_ACTION_EVENT, musicMediaAction.ordinal());
        intent.setAction(ACTION_SERVICE_MUSIC_EVENT);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void broadcastOTAEvent(OtaEvent otaEvent) {
        wd4.b(otaEvent, "otaEvent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("process : ");
        sb.append(otaEvent.getSerial());
        be4 be4 = be4.a;
        Locale locale = Locale.US;
        wd4.a((Object) locale, "Locale.US");
        Object[] objArr = {Float.valueOf(otaEvent.getProcess())};
        String format = String.format(locale, " %.1f", Arrays.copyOf(objArr, objArr.length));
        wd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
        sb.append(format);
        local.v(str, sb.toString());
        Intent intent = new Intent();
        intent.putExtra(Constants.OTA_PROCESS, otaEvent);
        intent.setAction(ACTION_OTA_PROGRESS);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void broadcastServiceBlePhaseEvent(String str, CommunicateMode communicateMode, ServiceActionResult serviceActionResult, Bundle bundle) {
        wd4.b(str, "serial");
        wd4.b(communicateMode, "communicateMode");
        wd4.b(serviceActionResult, Constants.RESULT);
        broadcastServiceBlePhaseEvent(str, communicateMode, serviceActionResult, -1, new ArrayList(), bundle);
    }

    @DexIgnore
    public final void broadcastStreamingEvent(String str, Gesture gesture) {
        wd4.b(str, "serial");
        wd4.b(gesture, "gesture");
        Intent intent = new Intent();
        intent.putExtra("gesture", gesture.getValue());
        intent.putExtra(Constants.SERIAL_NUMBER, str);
        intent.setAction(ACTION_SERVICE_STREAMING_EVENT);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void broadcastStreamingStop(String str) {
        wd4.b(str, "serial");
        Intent intent = new Intent();
        intent.putExtra(Constants.SERIAL_NUMBER, str);
        intent.setAction(ACTION_STREAMING_STOP);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final long cancelCalibration(String str) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".cancelCalibration() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        long j = TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        try {
            j = System.currentTimeMillis();
            ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).cancelCalibrationSession();
            return j;
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.e(str3, ".cancelCalibration(), serial=" + str + ", e=" + e);
            return j;
        }
    }

    @DexIgnore
    public final void cancelPairDevice(String str) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".cancelPairDevice() - serial=" + str);
        ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).cancelPairDevice();
    }

    @DexIgnore
    public final void changePendingLogKey(int i, String str, int i2, String str2) {
        wd4.b(str, "curSerial");
        wd4.b(str2, "newSerial");
        MFLogManager.getInstance(this).changePendingLogKey(CommunicateMode.values()[i], str, CommunicateMode.values()[i2], str2);
    }

    @DexIgnore
    public final void clearAutoSetMapping(String str) {
        wd4.b(str, "serial");
        DevicePreferenceUtils.clearAutoSetMapping(this, str);
    }

    @DexIgnore
    public final void connectAllButton() {
        List<String> allActiveButtonSerial = DevicePreferenceUtils.getAllActiveButtonSerial(this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "num button user is " + allActiveButtonSerial.size());
        for (String next : allActiveButtonSerial) {
            wd4.a((Object) next, "activeSerial");
            if (((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(next, this).getGattState() != 2) {
                enqueue(next);
            }
        }
    }

    @DexIgnore
    public final long deviceClearMapping(String str) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceClearMapping() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        List<BLEMapping> autoMapping = DevicePreferenceUtils.getAutoMapping(this, str);
        wd4.a((Object) autoMapping, "DevicePreferenceUtils.getAutoMapping(this, serial)");
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceClearMapping() - serial=" + str);
        if (!communicator.startCleanLinkMappingSession(autoMapping)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceSetAlarm() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.CLEAN_LINK_MAPPINGS, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long deviceCompleteCalibration(String str) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceCompleteCalibration() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceCompleteCalibration(), serial=" + str);
        long j = TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        try {
            BleSession currentSession = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).getCurrentSession();
            if (currentSession instanceof ICalibrationSession) {
                long currentTimeMillis = System.currentTimeMillis();
                ((ICalibrationSession) currentSession).handleApplyHandsPosition();
                return currentTimeMillis;
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local3.d(str4, ".deviceCompleteCalibration() - currentSession=" + currentSession.getClass().getName() + " is not CalibrationSession.");
            return j;
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str5 = TAG;
            local4.e(str5, ".deviceCompleteCalibration(), serial=" + str + ", e=" + e);
            return j;
        }
    }

    @DexIgnore
    public final long deviceDisableHeartRate(String str) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceDisableHeartRate() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        if (!communicator.disableHeartRateNotification()) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceDisableHeartRate() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.DISABLE_HEART_RATE_NOTIFICATION, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final void deviceDisconnect(String str) {
        wd4.b(str, "serial");
        if (!TextUtils.isEmpty(str)) {
            List<String> allActiveButtonSerial = DevicePreferenceUtils.getAllActiveButtonSerial(this);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceDisconnect() with " + str + " current active device is " + allActiveButtonSerial.size());
            BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
            addLogToActiveLog(str, "Device is forced to disconnect, terminate current session.");
            communicator.clearQuickCommandQueue();
            clearDeviceData(str);
            communicator.closeConnection();
        }
    }

    @DexIgnore
    public final long deviceEnableHeartRate(String str) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceEnableHeartRate() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        if (!communicator.enableHeartRateNotification()) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceEnableHeartRate() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.ENABLE_HEART_RATE_NOTIFICATION, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long deviceGetBatteryLevel(String str) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceGetBatteryLevel() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        long currentTimeMillis = System.currentTimeMillis();
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceGetBattery() - serial=" + str);
        if (!communicator.startGetBatteryLevelSession()) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceGetBattery() - Device is BUSY, using cached configuration");
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.BATTERY, ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getBatteryLevel(str));
            broadcastServiceBlePhaseEvent(str, CommunicateMode.GET_BATTERY_LEVEL, ServiceActionResult.SUCCEEDED, bundle);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long deviceGetRssi(String str) {
        wd4.b(str, "serial");
        if (TextUtils.isEmpty(str)) {
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceGetRssi() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceGetRssi() - serial=" + str);
        if (!communicator.startGetRssiSession()) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceGetRssi() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.READ_RSSI, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long deviceGetVibrationStrength(String str) {
        wd4.b(str, "serial");
        if (TextUtils.isEmpty(str)) {
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceGetVibrationStrength() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceGetVibrationStrength() - serial=" + str);
        if (!communicator.startGetVibrationStrengthSession()) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceGetVibrationStrength() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.GET_VIBRATION_STRENGTH, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final void deviceMovingHand(String str, HandCalibrationObj handCalibrationObj) {
        wd4.b(str, "serial");
        wd4.b(handCalibrationObj, "handCalibrationObj");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceMovingHand() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return;
        }
        try {
            BleSession currentSession = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).getCurrentSession();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.d(str3, ".deviceMovingHand() - currentSession=" + currentSession);
            ICalibrationSession iCalibrationSession = (ICalibrationSession) (!(currentSession instanceof ICalibrationSession) ? null : currentSession);
            if (iCalibrationSession != null) {
                iCalibrationSession.handleMoveHandRequest(handCalibrationObj);
                return;
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local3.d(str4, ".deviceMovingHand() - currentSession=" + currentSession.getClass().getName() + " is not CalibrationSession.");
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str5 = TAG;
            local4.e(str5, ".deviceMovingHand(), serial=" + str + ", e=" + e);
        }
    }

    @DexIgnore
    public final long deviceOta(String str, FirmwareData firmwareData, UserProfile userProfile) {
        wd4.b(str, "serial");
        wd4.b(firmwareData, "firmwareData");
        wd4.b(userProfile, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".deviceOta() - serial=" + str + ", isEmbedded=" + firmwareData.isEmbedded() + ", checksum=" + firmwareData.getCheckSum() + ", firmwareVersion=" + firmwareData.getFirmwareVersion());
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        communicator.startOtaSession(firmwareData, userProfile);
        return currentTimeMillis;
    }

    @DexIgnore
    public final long devicePlayAnimation(String str) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".devicePlayAnimation() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        long currentTimeMillis = System.currentTimeMillis();
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".devicePlayAnimation() - serial=" + str);
        if (!communicator.startPlayAnimationSession()) {
            FLogger.INSTANCE.getLocal().e(TAG, ".devicePlayAnimation() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.PLAY_ANIMATION, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long deviceReadRealTimeStep(String str) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceReadRealTimeStep() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        long currentTimeMillis = System.currentTimeMillis();
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceReadRealTimeStep() - serial=" + str);
        if (!communicator.startReadRealTimeStepSession()) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceReadRealTimeStep() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.READ_REAL_TIME_STEP, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long deviceResetHandsToZeroDegree(String str) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceResetHandsToZeroDegree() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceResetHandsToZeroDegree(), serial=" + str);
        long j = TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        try {
            BleSession currentSession = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).getCurrentSession();
            if (currentSession instanceof ICalibrationSession) {
                long currentTimeMillis = System.currentTimeMillis();
                ((ICalibrationSession) currentSession).handleResetHandsPosition();
                return currentTimeMillis;
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local3.d(str4, ".deviceResetHandsToZeroDegree() - currentSession=" + currentSession.getClass().getName() + " is not CalibrationSession.");
            return j;
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str5 = TAG;
            local4.e(str5, ".deviceResetHandsToZeroDegree(), serial=" + str + ", e=" + e);
            return j;
        }
    }

    @DexIgnore
    public final long deviceSendNotification(String str, NotificationBaseObj notificationBaseObj) {
        wd4.b(str, "serial");
        wd4.b(notificationBaseObj, "newNotification");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceSendNotification() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceSendNotification(), serial=" + str + ", newNotification=" + notificationBaseObj);
        communicator.startSendNotification(notificationBaseObj);
        return currentTimeMillis;
    }

    @DexIgnore
    public final void deviceSetAutoListAlarm(List<? extends Alarm> list) {
        if (list == null) {
            FLogger.INSTANCE.getLocal().d(TAG, ".deviceSetAutoListAlarm() - Alarm list is NULL");
            return;
        }
        ArrayList<AlarmSetting> bleAlarmSettings = AlarmExtensionKt.toBleAlarmSettings(list);
        List<String> allActiveButtonSerial = DevicePreferenceUtils.getAllActiveButtonSerial(this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, ".deviceSetAutoListAlarm(), size=" + bleAlarmSettings.size() + ", objects=" + bleAlarmSettings);
        if (!(allActiveButtonSerial == null || allActiveButtonSerial.isEmpty())) {
            String str2 = allActiveButtonSerial.get(0);
            wd4.a((Object) str2, "activeSerial");
            if (!((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str2, this).startSetAutoMultiAlarms(bleAlarmSettings)) {
                broadcastServiceBlePhaseEvent(str2, CommunicateMode.SET_AUTO_MULTI_ALARM, ServiceActionResult.FAILED, (Bundle) null);
                return;
            }
            return;
        }
        FLogger.INSTANCE.getLocal().d(TAG, ".deviceSetAutoListAlarm() cannot set caused by no active device.");
    }

    @DexIgnore
    public final void deviceSetAutoSecondTimezone(String str) {
        wd4.b(str, "secondTimezoneId");
        List<String> allActiveButtonSerial = DevicePreferenceUtils.getAllActiveButtonSerial(this);
        if (!(allActiveButtonSerial == null || allActiveButtonSerial.isEmpty())) {
            String str2 = allActiveButtonSerial.get(0);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local.d(str3, ".deviceSetAutoSecondTimezone(): " + str + ", activeSerial=" + str2);
            wd4.a((Object) str2, "activeSerial");
            if (!((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str2, this).startSetAutoSecondTimezone(str)) {
                broadcastServiceBlePhaseEvent(str2, CommunicateMode.SET_AUTO_SECOND_TIMEZONE, ServiceActionResult.FAILED, (Bundle) null);
                return;
            }
            return;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        local2.d(str4, "Inside " + TAG + ".deviceSetAutoSecondTimezone: " + str + " cannot set caused by no active device.");
    }

    @DexIgnore
    public final long deviceSetInactiveNudgeConfig(String str, InactiveNudgeData inactiveNudgeData) {
        wd4.b(str, "serial");
        wd4.b(inactiveNudgeData, "inactiveNudgeData");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceSetInactiveNudgeConfig() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceSetInactiveNudgeConfig() - serial=" + str);
        if (!communicator.startSetInactiveNudgeConfigSession(inactiveNudgeData)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceSetInactiveNudgeConfig() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_INACTIVE_NUDGE_CONFIG, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long deviceSetListAlarm(String str, List<? extends Alarm> list) {
        wd4.b(str, "serial");
        wd4.b(list, "alarmList");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceSetListAlarm() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceSetListAlarm, serial=" + str);
        if (!communicator.startSetMultipleAlarmsSession(AlarmExtensionKt.toBleAlarmSettings(list))) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceSetListAlarm() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_LIST_ALARM, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long deviceSetMapping(String str, List<? extends BLEMapping> list) {
        wd4.b(str, "serial");
        wd4.b(list, "mappings");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceSetMapping() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceSetMapping() - serial=" + str);
        if (!communicator.startSetLinkMappingSession(list)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceSetMapping() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_LINK_MAPPING, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long deviceSetSecondTimeZone(String str, String str2) {
        wd4.b(str, "serial");
        wd4.b(str2, "timezoneId");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local.d(str3, ".deviceSetSecondTimeZone() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        local2.d(str4, ".deviceSetSecondTimeZone() - timezoneId=" + str2 + ", serial=" + str);
        if (!communicator.startSetSecondTimezoneSession(str2)) {
            FLogger.INSTANCE.getLocal().d(TAG, ".deviceSetSecondTimeZone() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_SECOND_TIMEZONE, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long deviceSetVibrationStrength(String str, VibrationStrengthObj vibrationStrengthObj) {
        wd4.b(str, "serial");
        wd4.b(vibrationStrengthObj, "vibrationStrengthLevelObj");
        if (TextUtils.isEmpty(str)) {
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceSetVibrationStrength() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceSetVibrationStrength() - serial=" + str + ", vibrationStrengthLevel=" + vibrationStrengthObj);
        if (!communicator.startSetVibrationStrengthSession(vibrationStrengthObj)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceSetVibrationStrength() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_VIBRATION_STRENGTH, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long deviceStartCalibration(String str) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".deviceStartCalibration(), serial=" + str);
        long j = TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        try {
            BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
            j = System.currentTimeMillis();
            if (!communicator.startCalibrationSession()) {
                FLogger.INSTANCE.getLocal().e(TAG, ".deviceStartCalibration() - Device is BUSY");
                broadcastServiceBlePhaseEvent(str, CommunicateMode.ENTER_CALIBRATION, ServiceActionResult.FAILED, (Bundle) null);
            }
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.e(str3, ".deviceStartCalibration(), serial=" + str + ", e=" + e);
        }
        return j;
    }

    @DexIgnore
    public final long deviceStartSync(String str, UserProfile userProfile) {
        wd4.b(str, "serial");
        wd4.b(userProfile, "profile");
        if (userProfile.getSyncMode() == 14) {
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SYNC, ServiceActionResult.UNALLOWED_ACTION, new Bundle());
            return System.currentTimeMillis();
        } else if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceStartSync() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SYNC, ServiceActionResult.UNALLOWED_ACTION, new Bundle());
            return System.currentTimeMillis();
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.d(str3, ".deviceStartSync() with " + str);
            BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
            long currentTimeMillis = System.currentTimeMillis();
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local3.d(str4, ".deviceStartSync(), send broadcast start sync for " + str);
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SYNC, ServiceActionResult.PROCESSING, (Bundle) null);
            if (!communicator.startSyncingSession(userProfile)) {
                FLogger.INSTANCE.getLocal().d(TAG, "Sync session can't executed.");
                broadcastServiceBlePhaseEvent(str, CommunicateMode.SYNC, ServiceActionResult.UNALLOWED_ACTION, new Bundle());
            }
            return currentTimeMillis;
        }
    }

    @DexIgnore
    public final long deviceUnlink(String str) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".deviceUnlink() with " + str);
        if (TextUtils.isEmpty(str)) {
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        FLogger.INSTANCE.getLocal().d(TAG, ".deviceUnlink()");
        addLogToActiveLog(str, "Device is unlinked. Terminate current session if exist.");
        communicator.startUnlinkSession();
        return currentTimeMillis;
    }

    @DexIgnore
    public final long deviceUpdateGoalStep(String str, int i) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceUpdateGoalStep() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        long currentTimeMillis = System.currentTimeMillis();
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceUpdateGoalStep() - serial=" + str + ", stepGoal=" + i);
        if (!communicator.startSetStepGoal(i)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceUpdateGoalStep() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_STEP_GOAL, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final int endLog(int i, String str) {
        wd4.b(str, "serial");
        MFLog end = MFLogManager.getInstance(this).end(CommunicateMode.values()[i], str);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".endLog() - communicateMode=" + i + ", serial=" + str + ", mfLog=" + end);
        if (end != null) {
            return end.getStartTimeEpoch();
        }
        return 0;
    }

    @DexIgnore
    public final long forceConnect(String str) {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".forceConnect() with " + str + ", macAddress=" + getMacAddressFromCache(str));
            BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
            if (communicator.getGattState() == 2) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local2.d(str3, ".forceConnect() with " + str + ", device is still connected, no need to reconnect");
                broadcastServiceBlePhaseEvent(str, CommunicateMode.FORCE_CONNECT, ServiceActionResult.SUCCEEDED, (Bundle) null);
            } else {
                communicator.startConnectionDeviceSession(true);
            }
        } else {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local3.d(str4, ".forceConnect() with " + str + ". It is not active device, unallow to reconnect.");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.FORCE_CONNECT, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final void forceSwitchDeviceWithoutErase(String str) {
        wd4.b(str, "newActiveDeviceSerial");
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String str2 = TAG;
        remote.i(component, session, str, str2, "Force switch to new device: " + str + " without erase data");
        disconnectAllButton();
        setActiveSerial(str, getMacAddressFromCache(str));
        if (!cg4.a(str)) {
            ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).resetSettingFlagsToDefault();
        }
    }

    @DexIgnore
    public final List<BLEMapping> getAutoMapping(String str) {
        wd4.b(str, "serial");
        List<BLEMapping> autoMapping = DevicePreferenceUtils.getAutoMapping(this, str);
        wd4.a((Object) autoMapping, "DevicePreferenceUtils.getAutoMapping(this, serial)");
        return autoMapping;
    }

    @DexIgnore
    public final MisfitDeviceProfile getDeviceProfile(String str) {
        wd4.b(str, "serial");
        MisfitDeviceProfile misfitDeviceProfile = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        for (MisfitDeviceProfile next : getPairedDevice()) {
            if (wd4.a((Object) next.getDeviceSerial(), (Object) str)) {
                misfitDeviceProfile = next;
            }
        }
        return misfitDeviceProfile;
    }

    @DexIgnore
    public final int getGattState(String str) {
        wd4.b(str, "serial");
        if (isInActiveDeviceList(str)) {
            return ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).getGattState();
        }
        return 0;
    }

    @DexIgnore
    public final int getHIDState(String str) {
        wd4.b(str, "serial");
        if (isInActiveDeviceList(str)) {
            return ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).getHidState();
        }
        return 0;
    }

    @DexIgnore
    public final List<MisfitDeviceProfile> getPairedDevice() {
        HashSet hashSet = new HashSet(DevicePreferenceUtils.getAllPairedButtonSerial(this));
        hashSet.addAll(DevicePreferenceUtils.getAllActiveButtonSerial(this));
        ArrayList arrayList = new ArrayList();
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            wd4.a((Object) str, "serial");
            arrayList.add(MisfitDeviceProfile.Companion.cloneFrom(((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).getBleAdapter()));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".getPairedDevice() - allDevices=" + hashSet + ", activeDevices=" + DevicePreferenceUtils.getAllActiveButtonSerial(this) + ", profiles=" + arrayList.size());
        return arrayList;
    }

    @DexIgnore
    public final void interrupt(String str) {
        wd4.b(str, "serial");
        if (!TextUtils.isEmpty(str)) {
            BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
            if (communicator.getCommunicateMode() == CommunicateMode.LINK) {
                String str2 = TAG;
                Log.e(str2, "Interrupt " + str);
                MFLog activeLog = MFLogManager.getInstance(this).getActiveLog(str);
                if (activeLog != null) {
                    activeLog.log("Setup session " + str + ". User cancelled");
                }
                onFail(CommunicateMode.LINK, -1, str, DeviceTask.UNKNOWN, DeviceErrorState.UNKNOWN_ERROR);
                DevicePreferenceUtils.removeActiveButtonSerial(this, str);
                clearDeviceData(str);
            }
            addLogToActiveLog(str, "App calls interrupt.");
            communicator.interruptCurrentSession();
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, ".interrupt() - serial=" + str + ", activeDevices=" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
    }

    @DexIgnore
    public final void interruptCurrentSession(String str) {
        wd4.b(str, "serial");
        if (!TextUtils.isEmpty(str)) {
            BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
            addLogToActiveLog(str, "App calls interrupt");
            communicator.interruptCurrentSession();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".interruptCurrentSession() - serial=" + str + ", currentSession=" + communicator.getCurrentSession());
        }
    }

    @DexIgnore
    public final boolean isBleSupported(String str) {
        wd4.b(str, "serial");
        return BluetoothAdapter.getDefaultAdapter() != null && !FossilDeviceSerialPatternUtil.isGen1Device(str);
    }

    @DexIgnore
    public final boolean isLinking(String str) {
        wd4.b(str, "serial");
        if (!TextUtils.isEmpty(str) && ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).getCommunicateMode() == CommunicateMode.LINK) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final boolean isSyncing(String str) {
        wd4.b(str, "checkSerial");
        if (isInActiveDeviceList(str)) {
            return ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).containSyncMode();
        }
        return false;
    }

    @DexIgnore
    public final boolean isUpdatingFirmware(String str) {
        wd4.b(str, "serial");
        return ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).getCommunicateMode() == CommunicateMode.OTA;
    }

    @DexIgnore
    public final void logOut() {
        FLogger.INSTANCE.getLocal().e(TAG, ".logOut() - clear all device data.");
        disconnectAllButton();
        ConcurrentHashMap<String, ScannedDevice> concurrentHashMap = this.scannedDevices;
        if (concurrentHashMap != null) {
            concurrentHashMap.clear();
            DevicePreferenceUtils.logOut(this);
            SharePreferencesUtils.getInstance(this).setString(USER_ID, "");
            return;
        }
        wd4.d("scannedDevices");
        throw null;
    }

    @DexIgnore
    public void onAskForCurrentSecretKey(String str) {
        wd4.b(str, "serial");
        Bundle bundle = new Bundle();
        bundle.putString(Constants.SERIAL_NUMBER, str);
        broadcastServiceBlePhaseEvent(str, CommunicateMode.EXCHANGE_SECRET_KEY, ServiceActionResult.ASK_FOR_CURRENT_SECRET_KEY, bundle);
    }

    @DexIgnore
    public void onAskForLinkServer(String str, CommunicateMode communicateMode, Bundle bundle) {
        wd4.b(str, "serial");
        wd4.b(communicateMode, "communicateMode");
        wd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        broadcastServiceBlePhaseEvent(str, communicateMode, ServiceActionResult.ASK_FOR_LINK_SERVER, bundle);
    }

    @DexIgnore
    public void onAskForRandomKey(String str) {
        wd4.b(str, "serial");
        broadcastServiceBlePhaseEvent(str, CommunicateMode.EXCHANGE_SECRET_KEY, ServiceActionResult.ASK_FOR_RANDOM_KEY, (Bundle) null);
    }

    @DexIgnore
    public void onAskForServerSecretKey(String str, Bundle bundle) {
        wd4.b(str, "serial");
        wd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        broadcastServiceBlePhaseEvent(str, CommunicateMode.EXCHANGE_SECRET_KEY, ServiceActionResult.ASK_FOR_SERVER_SECRET_KEY, bundle);
    }

    @DexIgnore
    public void onAskForStopWorkout(String str) {
        wd4.b(str, "serial");
        broadcastServiceBlePhaseEvent(str, CommunicateMode.SYNC, ServiceActionResult.ASK_FOR_STOP_WORKOUT, (Bundle) null);
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        wd4.b(intent, "intent");
        FLogger.INSTANCE.getLocal().d(TAG, "onBind()");
        return this.buttonServiceHub;
    }

    @DexIgnore
    public void onCommunicatorResult(CommunicateMode communicateMode, String str, int i, List<Integer> list, Bundle bundle) {
        wd4.b(communicateMode, "mode");
        wd4.b(str, "serialNumber");
        wd4.b(list, "requiredPermissionCodes");
        wd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("onCommunicatorResult - serial=");
        sb.append(str);
        sb.append(", mode=");
        sb.append(communicateMode);
        sb.append(", success=");
        sb.append(i == 0);
        local.d(str2, sb.toString());
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        if (i == 0) {
            int i2 = WhenMappings.$EnumSwitchMapping$Anon0[communicateMode.ordinal()];
            if (i2 == 1) {
                for (String next : DevicePreferenceUtils.getAllActiveButtonSerial(this)) {
                    if (!cg4.b(next, str, true)) {
                        wd4.a((Object) next, "oldSerial");
                        deviceDisconnect(next);
                    }
                }
                DevicePreferenceUtils.addActiveButtonSerial(this, str);
                DevicePreferenceUtils.addPairedButtonSerial(this, str);
                communicator.resetSettingFlagsToDefault();
                onGattConnectionStateChanged(str, 2);
            } else if (i2 == 2) {
                DevicePreferenceUtils.removePairedButtonSerial(this, str);
                DevicePreferenceUtils.removeActiveButtonSerial(this, str);
                communicator.cleanUp();
                clearDeviceData(str);
            } else if (i2 == 3) {
                for (String next2 : DevicePreferenceUtils.getAllActiveButtonSerial(this)) {
                    if (!cg4.b(next2, str, true)) {
                        wd4.a((Object) next2, "oldSerial");
                        deviceDisconnect(next2);
                    }
                }
                setActiveSerial(str, getMacAddressFromCache(str));
                communicator.resetSettingFlagsToDefault();
            }
        }
        broadcastServiceBlePhaseEvent(str, communicateMode, i == 0 ? ServiceActionResult.SUCCEEDED : ServiceActionResult.FAILED, i, list, bundle);
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        FLogger fLogger = FLogger.INSTANCE;
        String str = Build.VERSION.RELEASE;
        wd4.a((Object) str, "Build.VERSION.RELEASE");
        String sDKVersion = Companion.getSDKVersion();
        String str2 = Build.MODEL;
        wd4.a((Object) str2, "Build.MODEL");
        fLogger.init("ButtonService", new AppLogInfo("", "", DeviceInfo.PLATFORM_ANDROID, str, "", sDKVersion, str2), new ActiveDeviceInfo("", "", ""), new CloudLogConfig("", "", "", ""), this, false, "BLE");
        FLogger.INSTANCE.getLocal().d(TAG, ".onCreate()");
        this.scannedDevices = new ConcurrentHashMap<>();
        this.connectQueue = new HashSet();
        this.thread.start();
        registerTimeZoneChangeEvent();
        MicroAppEventLogger.initialize(this);
        ya0 ya0 = ya0.c;
        Context applicationContext = getApplicationContext();
        wd4.a((Object) applicationContext, "this@ButtonService.applicationContext");
        ya0.a(applicationContext);
        this.userId = SharePreferencesUtils.getInstance(this).getString(USER_ID);
        if (appInfo == null) {
            appInfo = AppInfo.convertJsonToObject(SharePreferencesUtils.getInstance(this).getString(APP_INFO));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, ".onCreate(),userId=" + this.userId);
        String str4 = this.userId;
        if (str4 == null || cg4.a(str4)) {
            this.userId = cg4.a(ya0.c.c()) ? "example@misfit.com" : ya0.c.c();
        }
        updateUserId(this.userId);
        this.scanServiceInstance = new ScanServiceInstance(this, this);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str5 = TAG;
        local2.e(str5, ".onCreate() current all button is " + DevicePreferenceUtils.getAllActiveButtonSerial(this));
        initReceiverActionWithPackageName();
        registerReceiver(this.bluetoothReceiver, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        registerReceiver(this.bondChangedReceiver, new IntentFilter("android.bluetooth.device.action.BOND_STATE_CHANGED"));
        connectAllButton();
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(TAG, ".onDestroy()");
        unregisterReceiver(this.bluetoothReceiver);
        unregisterReceiver(this.bondChangedReceiver);
        unregisterReceiver(this.timeZoneChangeReceiver);
        this.thread.interrupt();
    }

    @DexIgnore
    public void onDeviceAppsRequest(int i, Bundle bundle, String str) {
        wd4.b(bundle, "deviceDataExtra");
        wd4.b(str, "serial");
        broadcastDeviceAppEvent(i, bundle, str);
    }

    @DexIgnore
    public final void onDeviceFound(ScannedDevice scannedDevice) {
        wd4.b(scannedDevice, "device");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, ".onDeviceFound() device=" + scannedDevice.getDeviceSerial());
        String deviceSerial = scannedDevice.getDeviceSerial();
        if (DeviceIdentityUtils.isMisfitDevice(deviceSerial)) {
            ConcurrentHashMap<String, ScannedDevice> concurrentHashMap = this.scannedDevices;
            if (concurrentHashMap != null) {
                if (concurrentHashMap.get(deviceSerial) == null) {
                    MFLog activeLog = MFLogManager.getInstance(this).getActiveLog("");
                    if (activeLog != null) {
                        activeLog.log("Found: " + deviceSerial + ", MAC " + scannedDevice.getDeviceMACAddress());
                        activeLog.addCandidate(deviceSerial);
                    }
                }
                ConcurrentHashMap<String, ScannedDevice> concurrentHashMap2 = this.scannedDevices;
                if (concurrentHashMap2 != null) {
                    concurrentHashMap2.put(deviceSerial, scannedDevice);
                    if (isInActiveDeviceList(deviceSerial)) {
                        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(deviceSerial, scannedDevice.getDeviceMACAddress(), this);
                        if (communicator.getGattState() != 2 && !communicator.isRunning()) {
                            FLogger.INSTANCE.getLocal().d(TAG, ".onDeviceFound(), Enqueue because serial is in deviceList queue or candidateDevices queue");
                            enqueue(deviceSerial);
                        }
                    }
                    broadcastDeviceScanFound(ShineDevice.Companion.clone(scannedDevice), scannedDevice.getRssi());
                    return;
                }
                wd4.d("scannedDevices");
                throw null;
            }
            wd4.d("scannedDevices");
            throw null;
        }
    }

    @DexIgnore
    public void onExchangeSecretKeySuccess(String str, String str2) {
        wd4.b(str, "serial");
        wd4.b(str2, "secretKey");
        Bundle bundle = new Bundle();
        bundle.putString(DEVICE_SECRET_KEY, str2);
        bundle.putString(Constants.SERIAL_NUMBER, str);
        broadcastServiceBlePhaseEvent(str, CommunicateMode.EXCHANGE_SECRET_KEY, ServiceActionResult.SUCCEEDED, bundle);
    }

    @DexIgnore
    public void onFail(CommunicateMode communicateMode, int i, String str, DeviceTask deviceTask, DeviceErrorState deviceErrorState) {
        wd4.b(communicateMode, "action");
        wd4.b(str, "serial");
        wd4.b(deviceTask, "currentTask");
        wd4.b(deviceErrorState, "errorState");
    }

    @DexIgnore
    public void onFirmwareLatest(String str) {
        wd4.b(str, "serial");
        broadcastServiceBlePhaseEvent(str, CommunicateMode.LINK, ServiceActionResult.LATEST_FW, (Bundle) null);
    }

    @DexIgnore
    public void onGattConnectionStateChanged(String str, int i) {
        wd4.b(str, "serialNumber");
        List<String> allActiveButtonSerial = DevicePreferenceUtils.getAllActiveButtonSerial(this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".onGattConnectionStateChanged serial=" + str + ", state=" + i + ", activeDeviceSerials=" + allActiveButtonSerial);
        if (TextUtils.isEmpty(str) || (allActiveButtonSerial != null && !allActiveButtonSerial.contains(str))) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.i(str3, ".onGattConnectionStateChanged - " + str + " is not active device");
            return;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        boolean isDeviceReady = communicator.isDeviceReady();
        if (i == 2) {
            saveMacAddressToCache(str, communicator.getBleAdapter().getMacAddress());
            if (isDeviceReady) {
                broadcastConnectionStateChange(str, ConnectionStateChange.GATT_ON);
            }
        } else if (i == 0) {
            broadcastConnectionStateChange(str, ConnectionStateChange.GATT_OFF);
        }
    }

    @DexIgnore
    public void onHeartBeatDataReceived(int i, int i2, String str) {
        wd4.b(str, "serial");
        broadcastHeartBeatData(i, i2, str);
    }

    @DexIgnore
    public void onHeartRateNotification(short s, String str) {
        wd4.b(str, "serial");
        broadcastHeartRateData(s, str);
    }

    @DexIgnore
    public void onHidConnectionStateChanged(String str, int i) {
        wd4.b(str, "serialNumber");
        List<String> allActiveButtonSerial = DevicePreferenceUtils.getAllActiveButtonSerial(this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".onHidConnectionStateChanged() serial=" + str + ", state=" + i + ", activeDevices=" + allActiveButtonSerial);
        if (TextUtils.isEmpty(str) || (allActiveButtonSerial != null && !allActiveButtonSerial.contains(str))) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.i(str3, ".onHidConnectionStateChanged() - " + str + " is not active device");
            return;
        }
        if (reconnectRequired(((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).getGattState(), i)) {
            new Handler(getMainLooper()).postDelayed(new ButtonService$onHidConnectionStateChanged$Anon1(this, str), 1000);
        }
        if (i == 2) {
            broadcastConnectionStateChange(str, ConnectionStateChange.HID_ON);
        } else if (i == 0) {
            broadcastConnectionStateChange(str, ConnectionStateChange.HID_OFF);
        }
    }

    @DexIgnore
    public void onOtaProgressUpdated(String str, float f) {
        wd4.b(str, "serialNumber");
        broadcastOTAEvent(new OtaEvent(str, f));
    }

    @DexIgnore
    public final long onPing(String str) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".onPing() - serial=" + str);
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        communicator.onPing();
        return currentTimeMillis;
    }

    @DexIgnore
    public void onPreparationCompleted(boolean z, String str) {
        wd4.b(str, "serial");
        List<String> allActiveButtonSerial = DevicePreferenceUtils.getAllActiveButtonSerial(this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.i(str2, ".onPreparationCompleted - activeSerials " + allActiveButtonSerial + " serial=" + str + ", success=" + z);
        if (z && !TextUtils.isEmpty(str) && allActiveButtonSerial.contains(str)) {
            broadcastConnectionStateChange(str, ConnectionStateChange.GATT_ON);
        }
    }

    @DexIgnore
    public void onReceivedSyncData(String str, Bundle bundle) {
        wd4.b(str, "serialNumber");
        wd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        broadcastServiceBlePhaseEvent(str, CommunicateMode.SYNC, ServiceActionResult.RECEIVED_DATA, bundle);
    }

    @DexIgnore
    public void onRequestLatestFirmware(String str, Bundle bundle) {
        wd4.b(str, "serial");
        wd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        broadcastServiceBlePhaseEvent(str, CommunicateMode.LINK, ServiceActionResult.GET_LATEST_FW, bundle);
    }

    @DexIgnore
    public void onRequestLatestWatchParams(String str, Bundle bundle) {
        wd4.b(str, "serial");
        wd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
        broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_WATCH_PARAMS, ServiceActionResult.ASK_FOR_WATCH_PARAMS, bundle);
    }

    @DexIgnore
    public final void onSetWatchParamResponse(String str, boolean z, WatchParamsFileMapping watchParamsFileMapping) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.i(str2, ".onSetWatchParamResponse(), data=" + watchParamsFileMapping);
        ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).onSetWatchParamResponse(str, z, watchParamsFileMapping);
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i, int i2) {
        FLogger.INSTANCE.getLocal().d(TAG, "onStartCommand()");
        if (cg4.b(intent != null ? intent.getAction() : null, com.misfit.frameworks.buttonservice.utils.Constants.START_FOREGROUND_ACTION, false, 2, (Object) null)) {
            FLogger.INSTANCE.getLocal().d(TAG, "onStartCommand() - Received Start Foreground Intent ");
            NotificationUtils.Companion.getInstance().startForegroundNotification(this, this, "", "", false);
            return 1;
        }
        FLogger.INSTANCE.getLocal().d(TAG, "onStartCommand() - Received Stop Foreground Intent ");
        NotificationUtils.Companion.getInstance().startForegroundNotification(this, this, "", "", true);
        return 1;
    }

    @DexIgnore
    public void onSuccess(CommunicateMode communicateMode, int i, String str, Bundle bundle) {
        wd4.b(communicateMode, "action");
        wd4.b(str, "serial");
        wd4.b(bundle, Mapping.COLUMN_EXTRA_INFO);
    }

    @DexIgnore
    public void onTaskRemoved(Intent intent) {
        wd4.b(intent, "rootIntent");
        super.onTaskRemoved(intent);
        FLogger.INSTANCE.getLocal().d(TAG, ".onTaskRemoved()");
        for (BleCommunicator next : ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getRunningCommunicator()) {
            addLogToActiveLog(next.getSerial(), "App task is removed. Interrupt current session.");
            next.interruptCurrentSession();
        }
        MFLogManager.getInstance(this).stopLogService(FailureCode.USER_KILL_APP_FROM_TASK_MANAGER);
    }

    @DexIgnore
    public boolean onUnbind(Intent intent) {
        wd4.b(intent, "intent");
        return true;
    }

    @DexIgnore
    public void onUpdateFirmwareFailed(String str) {
        wd4.b(str, "serial");
        broadcastServiceBlePhaseEvent(str, CommunicateMode.LINK, ServiceActionResult.UPDATE_FW_FAILED, (Bundle) null);
    }

    @DexIgnore
    public void onUpdateFirmwareSuccess(String str) {
        wd4.b(str, "serial");
        broadcastServiceBlePhaseEvent(str, CommunicateMode.LINK, ServiceActionResult.UPDATE_FW_SUCCESS, (Bundle) null);
    }

    @DexIgnore
    public final long pairDevice(String str, String str2, UserProfile userProfile) {
        wd4.b(str, "serial");
        wd4.b(str2, "macAddress");
        wd4.b(userProfile, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, ".pairDevice() with " + str + ", macAddress=" + str2);
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, str2, this);
        long currentTimeMillis = System.currentTimeMillis();
        saveMacAddressToCache(str, str2);
        communicator.startPairingSession(userProfile);
        return currentTimeMillis;
    }

    @DexIgnore
    public final long pairDeviceResponse(String str, PairingResponse pairingResponse) {
        wd4.b(str, "serial");
        wd4.b(pairingResponse, "response");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".pairDeviceResponse() - serial=" + str + ", firmwareData=" + pairingResponse);
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        communicator.pairDeviceResponse(pairingResponse);
        return currentTimeMillis;
    }

    @DexIgnore
    public final long readCurrentWorkoutSession(String str) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceReadCurrentWorkoutSession() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceReadCurrentWorkoutSession() - serial=" + str);
        if (!communicator.startReadCurrentWorkoutSession()) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceReadCurrentWorkoutSession() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.READ_CURRENT_WORKOUT_SESSION, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long receiveCurrentSecretKey(String str, String str2) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, ".receiveCurrentSecretKey() - serial=" + str + ", serverSecretKey=" + str2);
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        communicator.onReceiveCurrentSecretKey(!TextUtils.isEmpty(str2) ? Base64.decode(str2, 0) : null);
        return currentTimeMillis;
    }

    @DexIgnore
    public final long receiveRandomKey(String str, String str2, int i) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, ".receiveRandomKey() - serial=" + str + ", randomKey=" + str2);
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        communicator.onReceiveServerRandomKey(!TextUtils.isEmpty(str2) ? Base64.decode(str2, 0) : null, i);
        return currentTimeMillis;
    }

    @DexIgnore
    public final long receiveServerSecretKey(String str, String str2, int i) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, ".receiveServerSecretKey() - serial=" + str + ", serverSecretKey=" + str2);
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        communicator.onReceiveServerSecretKey(!TextUtils.isEmpty(str2) ? Base64.decode(str2, 0) : null, i);
        return currentTimeMillis;
    }

    @DexIgnore
    public final void sendCustomCommand(String str, CustomRequest customRequest) {
        wd4.b(str, "serial");
        wd4.b(customRequest, Constants.COMMAND);
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".sendCustomRequest() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".sendCustomRequest() - serial=" + str + ", command: " + customRequest);
        communicator.sendCustomCommand(customRequest);
    }

    @DexIgnore
    public final void setActiveSerial(String str, String str2) {
        wd4.b(str, "serial");
        wd4.b(str2, "macAddress");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.i(str3, ".setActiveSerial() - serial=" + str + ", macAddress=" + str2);
        if (TextUtils.isEmpty(str)) {
            DevicePreferenceUtils.addActiveButtonSerial(this, str);
            return;
        }
        saveMacAddressToCache(str, str2);
        DevicePreferenceUtils.addActiveButtonSerial(this, str);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        local2.d(str4, ".setActiveSerial() - serial=" + str + ", activeDevices=" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
    }

    @DexIgnore
    public final void setAutoBackgroundImageConfig(BackgroundConfig backgroundConfig, String str) {
        wd4.b(backgroundConfig, "backgroundConfig");
        wd4.b(str, "serial");
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".setAutoBackgroundImageConfig() - serial=" + str);
        if (!communicator.startSetAutoBackgroundImageConfig(backgroundConfig)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".setAutoBackgroundImageConfig() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_BACKGROUND_IMAGE_CONFIG, ServiceActionResult.FAILED, (Bundle) null);
        }
    }

    @DexIgnore
    public final void setAutoComplicationApps(ComplicationAppMappingSettings complicationAppMappingSettings, String str) {
        wd4.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        wd4.b(str, "serial");
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "Inside " + TAG + ".setAutoComplicationApps() - serial=" + str);
        if (!communicator.startSetAutoComplicationApps(complicationAppMappingSettings)) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.e(str3, "Inside " + TAG + ".setAutoComplicationApps() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_COMPLICATION_APPS, ServiceActionResult.FAILED, (Bundle) null);
        }
    }

    @DexIgnore
    public final void setAutoMapping(String str, List<? extends BLEMapping> list) {
        wd4.b(str, "serial");
        wd4.b(list, "mappings");
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".setAutoMapping() - serial=" + str);
        if (!communicator.startSetAutoMapping(list)) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.e(str3, "Inside " + TAG + ".setAutoMapping - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_MAPPING, ServiceActionResult.FAILED, (Bundle) null);
        }
    }

    @DexIgnore
    public final void setAutoNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        wd4.b(appNotificationFilterSettings, "notificationFilterSettings");
        wd4.b(str, "serial");
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".setAutoNotificationFilterSettings() - serial=" + str);
        if (!communicator.startSetAutoNotificationFilterSettings(appNotificationFilterSettings)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".setAutoNotificationFilterSettings() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS, ServiceActionResult.FAILED, (Bundle) null);
        }
    }

    @DexIgnore
    public final void setAutoUserBiometricData(UserProfile userProfile) {
        if (userProfile == null) {
            FLogger.INSTANCE.getLocal().d(TAG, ".setAutoUserBiometricData() - user biometric data is NULL");
            return;
        }
        List<String> allActiveButtonSerial = DevicePreferenceUtils.getAllActiveButtonSerial(this);
        UserBiometricData userBiometricData = userProfile.getUserBiometricData();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, ".setAutoUserBiometricData(), biometric data=" + userBiometricData);
        if (!(allActiveButtonSerial == null || allActiveButtonSerial.isEmpty())) {
            String str2 = allActiveButtonSerial.get(0);
            wd4.a((Object) str2, "activeSerial");
            if (!((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str2, this).startSetAutoBiometricData(userBiometricData)) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local2.e(str3, ".setAutoUserBiometricData(), serial=" + str2 + " - Device is BUSY");
                broadcastServiceBlePhaseEvent(str2, CommunicateMode.SET_AUTO_BIOMETRIC_DATA, ServiceActionResult.FAILED, (Bundle) null);
                return;
            }
            return;
        }
        FLogger.INSTANCE.getLocal().d(TAG, ".setAutoUserBiometricData() cannot set caused by no active device.");
    }

    @DexIgnore
    public final void setAutoWatchApps(WatchAppMappingSettings watchAppMappingSettings, String str) {
        wd4.b(watchAppMappingSettings, "watchAppMappingSettings");
        wd4.b(str, "serial");
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".setAutoWatchApps() - serial=" + str);
        if (!communicator.startSetAutoWatchApps(watchAppMappingSettings)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".setAutoWatchApps() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_WATCH_APPS, ServiceActionResult.FAILED, (Bundle) null);
        }
    }

    @DexIgnore
    public final long setBackgroundImageConfig(BackgroundConfig backgroundConfig, String str) {
        wd4.b(backgroundConfig, "backgroundConfig");
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".setBackgroundImageConfig() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".setBackgroundImageConfig() - serial=" + str);
        if (!communicator.startSetBackgroundImageConfig(backgroundConfig)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".setBackgroundImageConfig() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_BACKGROUND_IMAGE_CONFIG, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long setCompilationApps(ComplicationAppMappingSettings complicationAppMappingSettings, String str) {
        wd4.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, "---Inside .setCompilationApps() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".setCompilationApps() - serial=" + str);
        if (!communicator.startSetComplicationApps(complicationAppMappingSettings)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".setCompilationApps() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_COMPLICATION_APPS, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long setFrontLightEnable(String str, boolean z) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".setFrontLightEnable() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".setFrontLightEnable() - serial=" + str + ", isFrontLightEnable=" + z);
        if (!communicator.startSetFrontLightEnable(z)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".setFrontLightEnable() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_FRONT_LIGHT_ENABLE, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final long setHeartRateMode(String str, HeartRateMode heartRateMode) {
        wd4.b(str, "serial");
        wd4.b(heartRateMode, "heartRateMode");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".setHeartRateMode() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".setHeartRateMode() - serial=" + str + ", heartRateMode=" + heartRateMode);
        if (!communicator.startSetHeartRateMode(heartRateMode)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".setHeartRateMode() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_HEART_RATE_MODE, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final void setImplicitDeviceConfig(UserProfile userProfile, String str) {
        wd4.b(userProfile, "userProfile");
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".setImplicitDeviceConfig() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".setImplicitDeviceConfig() - serial=" + str);
        if (!communicator.startSetImplicitDeviceConfig(userProfile)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".setImplicitDeviceConfig() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_IMPLICIT_DEVICE_CONFIG, ServiceActionResult.FAILED, (Bundle) null);
        }
    }

    @DexIgnore
    public final void setImplicitDisplayUnitSettings(UserDisplayUnit userDisplayUnit, String str) {
        wd4.b(userDisplayUnit, "userDisplayUnit");
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".setImplicitDisplayUnitSettings() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".setImplicitDisplayUnitSettings() - serial=" + str);
        if (!communicator.startSetImplicitDisplayUnitSettings(userDisplayUnit)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".setImplicitDisplayUnitSettings() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_IMPLICIT_DISPLAY_UNIT, ServiceActionResult.FAILED, (Bundle) null);
        }
    }

    @DexIgnore
    public final void setLocalizationData(LocalizationData localizationData) {
        wd4.b(localizationData, "localizationData");
        List<String> allActiveButtonSerial = DevicePreferenceUtils.getAllActiveButtonSerial(this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, ".setLocalizationData(), filePath = " + localizationData.getFilePath());
        if (!(allActiveButtonSerial == null || allActiveButtonSerial.isEmpty())) {
            String str2 = allActiveButtonSerial.get(0);
            wd4.a((Object) str2, "activeSerial");
            if (!((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str2, this).startSetLocalizationData(localizationData)) {
                broadcastServiceBlePhaseEvent(str2, CommunicateMode.SET_LOCALIZATION_DATA, ServiceActionResult.FAILED, (Bundle) null);
                return;
            }
            return;
        }
        FLogger.INSTANCE.getLocal().d(TAG, ".setLocalizationData() cannot set caused by no active device.");
    }

    @DexIgnore
    public final long setNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        wd4.b(appNotificationFilterSettings, "notificationFilterSettings");
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".setNotificationFilterSettings() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".setNotificationFilterSettings() - serial=" + str);
        if (!communicator.startSetNotificationFilterSettings(appNotificationFilterSettings)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".setNotificationFilterSettings() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_NOTIFICATION_FILTERS, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final void setPairedDevice(String str, String str2) {
        wd4.b(str, "serial");
        wd4.b(str2, "macAddress");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.i(str3, ".setPairedDevice()=" + str + ", macAddress=" + str2);
        saveMacAddressToCache(str, str2);
        DevicePreferenceUtils.addPairedButtonSerial(this, str);
    }

    @DexIgnore
    public final long setPresetApps(WatchAppMappingSettings watchAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings, BackgroundConfig backgroundConfig, String str) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".setPresetApps() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".setPresetApps() - serial=" + str);
        if (!communicator.startSetPresetApps(watchAppMappingSettings, complicationAppMappingSettings, backgroundConfig)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".setPresetApps() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_PRESET_APPS_DATA, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final void setSecretKey(String str, String str2) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, ".setSecretKey() - serial=" + str + ", secretKey=" + str2);
        ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).setSecretKey(!TextUtils.isEmpty(str2) ? Base64.decode(str2, 0) : null);
    }

    @DexIgnore
    public final long setWatchApps(WatchAppMappingSettings watchAppMappingSettings, String str) {
        wd4.b(watchAppMappingSettings, "watchAppMappingSettings");
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".setWatchApps() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".setWatchApps() - serial=" + str);
        if (!communicator.startSetWatchApps(watchAppMappingSettings)) {
            FLogger.INSTANCE.getLocal().e(TAG, ".setWatchApps() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_WATCH_APPS, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final int startLog(int i, String str) {
        wd4.b(str, "serial");
        int i2 = WhenMappings.$EnumSwitchMapping$Anon1[CommunicateMode.values()[i].ordinal()];
        if (i2 == 1 || i2 == 2) {
            MFSyncLog startSyncLog = MFLogManager.getInstance(this).startSyncLog(str);
            wd4.a((Object) startSyncLog, "MFLogManager.getInstance\u2026his).startSyncLog(serial)");
            return startSyncLog.getStartTimeEpoch();
        } else if (i2 == 3) {
            MFOtaLog startOtaLog = MFLogManager.getInstance(this).startOtaLog(str);
            wd4.a((Object) startOtaLog, "MFLogManager.getInstance(this).startOtaLog(serial)");
            return startOtaLog.getStartTimeEpoch();
        } else if (i2 != 4) {
            return -1;
        } else {
            MFSetupLog startSetupLog = MFLogManager.getInstance(this).startSetupLog(str, fossilBrand);
            wd4.a((Object) startSetupLog, "MFLogManager.getInstance\u2026pLog(serial, fossilBrand)");
            return startSetupLog.getStartTimeEpoch();
        }
    }

    @DexIgnore
    public final long stopCurrentWorkoutSession(String str) {
        wd4.b(str, "serial");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deviceStopCurrentWorkoutSession() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceStopCurrentWorkoutSession() - serial=" + str);
        if (!communicator.startStopCurrentWorkoutSession()) {
            FLogger.INSTANCE.getLocal().e(TAG, ".deviceStopCurrentWorkoutSession() - Device is BUSY");
            broadcastServiceBlePhaseEvent(str, CommunicateMode.STOP_CURRENT_WORKOUT_SESSION, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public final void stopLogService(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, ".stopLogService() - failureCode=" + i);
        MFLogManager.getInstance(this).stopLogService(i);
    }

    @DexIgnore
    public final boolean switchActiveDevice(String str, UserProfile userProfile) {
        wd4.b(str, "newActiveDeviceSerial");
        wd4.b(userProfile, "userProfile");
        if (TextUtils.isEmpty(str)) {
            disconnectAllButton();
            setActiveSerial(str, getMacAddressFromCache(str));
            broadcastServiceBlePhaseEvent(str, CommunicateMode.SWITCH_DEVICE, ServiceActionResult.SUCCEEDED, (Bundle) null);
        } else if (!isInPairedDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".switchActiveDevice() to " + str + ". It is unallowed caused by " + str + " is not in paired device list:" + DevicePreferenceUtils.getAllPairedButtonSerial(this));
            return false;
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.d(str3, ".switchActiveDevice() to " + str + " executed.");
            userProfile.setNewDevice(true);
            if (!((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this).startSwitchDeviceSession(userProfile)) {
                FLogger.INSTANCE.getLocal().d(TAG, "Switch device session can't executed.");
                broadcastServiceBlePhaseEvent(str, CommunicateMode.SWITCH_DEVICE, ServiceActionResult.FAILED, (Bundle) null);
            }
        }
        return true;
    }

    @DexIgnore
    public final long switchDeviceResponse(String str, boolean z, int i) {
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".switchDeviceResponse() - serial=" + str + ", isSuccess=" + z);
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        long currentTimeMillis = System.currentTimeMillis();
        communicator.switchDeviceResponse(z, i);
        return currentTimeMillis;
    }

    @DexIgnore
    public final void updateAppInfo(String str) {
        wd4.b(str, "appInfoJson");
        SharePreferencesUtils.getInstance(this).setString(APP_INFO, str);
    }

    @DexIgnore
    public final void updatePercentageGoalProgress(String str, boolean z, UserProfile userProfile) {
        wd4.b(str, "serial");
        wd4.b(userProfile, "userProfile");
        if (!isInActiveDeviceList(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".updatePercentageGoalProgress() with " + str + " is not allowed caused by this device is not in active list:" + DevicePreferenceUtils.getAllActiveButtonSerial(this));
            return;
        }
        BleCommunicator communicator = ((CommunicateManager) CommunicateManager.Companion.getInstance(this)).getCommunicator(str, this);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, ".deviceUpdateProgressGoal() - profile=" + userProfile);
        BleSession currentSession = communicator.getCurrentSession();
        if (currentSession instanceof ISyncSession) {
            ((ISyncSession) currentSession).updateCurrentStepAndStepGoalFromApp(z, userProfile);
            return;
        }
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        local3.d(str4, ".deviceUpdateProgressGoal() - cannot update to goal, current session is not SyncSession, currentSession: " + currentSession.getClass().getName());
    }

    @DexIgnore
    public final void updateUserId(String str) {
        String c = ya0.c.c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".updateUserId() - Setting userId to SDK - oldUserId=" + this.userId + ", newUserId=" + str + ", sdkUserId=" + c);
        boolean z = false;
        if (!(str == null || cg4.a(str))) {
            if (!cg4.a(c)) {
                String str3 = this.userId;
                if (str3 == null || cg4.a(str3)) {
                    z = true;
                }
                if (!z && !(!wd4.a((Object) str, (Object) this.userId))) {
                    return;
                }
            }
            this.userId = str;
            ya0 ya0 = ya0.c;
            String str4 = this.userId;
            if (str4 != null) {
                ya0.a(str4);
                FLogger.INSTANCE.getLocal().d(TAG, ".updateUserId() - Setting userId to SDK. DONE ");
                SharePreferencesUtils.getInstance(this).setString(USER_ID, this.userId);
                return;
            }
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void broadcastServiceBlePhaseEvent(String str, CommunicateMode communicateMode, ServiceActionResult serviceActionResult, int i, List<Integer> list, Bundle bundle) {
        wd4.b(str, "serial");
        wd4.b(communicateMode, "communicateMode");
        wd4.b(serviceActionResult, Constants.RESULT);
        wd4.b(list, "requiredPermissionCodes");
        Intent intent = new Intent();
        intent.putExtra(Constants.SERIAL_NUMBER, str);
        intent.putExtra(SERVICE_BLE_PHASE, communicateMode.ordinal());
        intent.putExtra(SERVICE_ACTION_RESULT, serviceActionResult.ordinal());
        intent.putExtra(LAST_DEVICE_ERROR_STATE, i);
        intent.putIntegerArrayListExtra(LIST_PERMISSION_CODES, new ArrayList(list));
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.setAction(ACTION_SERVICE_BLE_RESPONSE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "Broadcast " + communicateMode + ", result=" + serviceActionResult + ", intent=" + intent);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final boolean isBleSupported() {
        return BluetoothAdapter.getDefaultAdapter() != null;
    }
}
