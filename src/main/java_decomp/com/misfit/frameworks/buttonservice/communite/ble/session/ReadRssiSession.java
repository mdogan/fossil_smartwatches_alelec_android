package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ReadRssiSession extends BleSessionAbs {
    @DexIgnore
    public int mRemoteRssi; // = 100;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadRssiStep extends BleStateAbs {
        @DexIgnore
        public h90<Integer> task;

        @DexIgnore
        public ReadRssiStep() {
            super(ReadRssiSession.this.getTAG());
            setTimeout(VideoUploader.RETRY_DELAY_UNIT_MS);
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = ReadRssiSession.this.getBleAdapter().readRssi(ReadRssiSession.this.getLogSession(), this);
            if (this.task == null) {
                ReadRssiSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onReadRssiFailed(i90 i90) {
            wd4.b(i90, "error");
            stopTimeout();
            ReadRssiSession.this.stop(FailureCode.FAILED_TO_READ_RSSI);
        }

        @DexIgnore
        public void onReadRssiSuccess(int i) {
            stopTimeout();
            ReadRssiSession.this.mRemoteRssi = i;
            ReadRssiSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            h90<Integer> h90 = this.task;
            if (h90 != null) {
                h90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReadRssiSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.URGENT, CommunicateMode.READ_RSSI, bleAdapterImpl, bleSessionCallback);
        wd4.b(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putInt("rssi", this.mRemoteRssi);
    }

    @DexIgnore
    public BleSession copyObject() {
        ReadRssiSession readRssiSession = new ReadRssiSession(getBleAdapter(), getBleSessionCallback());
        readRssiSession.setDevice(getDevice());
        return readRssiSession;
    }

    @DexIgnore
    public void initStateMap() {
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.READ_RSSI_STATE;
        String name = ReadRssiStep.class.getName();
        wd4.a((Object) name, "ReadRssiStep::class.java.name");
        sessionStateMap.put(sessionState, name);
    }

    @DexIgnore
    public boolean onStart(Object... objArr) {
        wd4.b(objArr, NativeProtocol.WEB_DIALOG_PARAMS);
        super.onStart(Arrays.copyOf(objArr, objArr.length));
        if (!BluetoothUtils.isBluetoothEnable()) {
            enterTaskWithDelayTime(new ReadRssiSession$onStart$Anon1(this), 500);
            return true;
        } else if (getBleAdapter().getDeviceObj() != null) {
            enterStateAsync(createConcreteState(BleSessionAbs.SessionState.READ_RSSI_STATE));
            return true;
        } else {
            enterTaskWithDelayTime(new ReadRssiSession$onStart$Anon3$Anon1(this), 500);
            return true;
        }
    }
}
