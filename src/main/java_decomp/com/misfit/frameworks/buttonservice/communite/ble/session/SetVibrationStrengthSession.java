package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.config.builder.DeviceConfigBuilder;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetVibrationStrengthSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ VibrationStrengthObj mVibrationStrengthLevelObj;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetVibrationStrengthState extends BleStateAbs {
        @DexIgnore
        public h90<DeviceConfigKey[]> task;

        @DexIgnore
        public SetVibrationStrengthState() {
            super(SetVibrationStrengthSession.this.getTAG());
        }

        @DexIgnore
        private final DeviceConfigItem[] prepareData() {
            DeviceConfigBuilder deviceConfigBuilder = new DeviceConfigBuilder();
            deviceConfigBuilder.a(SetVibrationStrengthSession.this.mVibrationStrengthLevelObj.toSDKVibrationStrengthLevel());
            return deviceConfigBuilder.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            SetVibrationStrengthSession setVibrationStrengthSession = SetVibrationStrengthSession.this;
            setVibrationStrengthSession.log("Set Vibration Strength, value=" + SetVibrationStrengthSession.this.mVibrationStrengthLevelObj);
            this.task = SetVibrationStrengthSession.this.getBleAdapter().setDeviceConfig(SetVibrationStrengthSession.this.getLogSession(), prepareData(), this);
            if (this.task == null) {
                SetVibrationStrengthSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(i90 i90) {
            wd4.b(i90, "error");
            stopTimeout();
            SetVibrationStrengthSession.this.stop(FailureCode.FAILED_TO_SET_VIBRATION_STRENGTH);
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetVibrationStrengthSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            h90<DeviceConfigKey[]> h90 = this.task;
            if (h90 != null) {
                h90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetVibrationStrengthSession(VibrationStrengthObj vibrationStrengthObj, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_VIBRATION_STRENGTH, bleAdapterImpl, bleSessionCallback);
        wd4.b(vibrationStrengthObj, "mVibrationStrengthLevelObj");
        wd4.b(bleAdapterImpl, "bleAdapter");
        this.mVibrationStrengthLevelObj = vibrationStrengthObj;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetVibrationStrengthSession setVibrationStrengthSession = new SetVibrationStrengthSession(this.mVibrationStrengthLevelObj, getBleAdapter(), getBleSessionCallback());
        setVibrationStrengthSession.setDevice(getDevice());
        return setVibrationStrengthSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_VIBRATION_STRENGTH_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_VIBRATION_STRENGTH_STATE;
        String name = SetVibrationStrengthState.class.getName();
        wd4.a((Object) name, "SetVibrationStrengthState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
