package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.adapter.ScanError;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.workoutsession.WorkoutSession;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.fitness.FitnessData;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ISessionSdkCallback {
    @DexIgnore
    void onApplyHandPositionFailed(i90 i90);

    @DexIgnore
    void onApplyHandPositionSuccess();

    @DexIgnore
    void onAuthenticateDeviceFail(i90 i90);

    @DexIgnore
    void onAuthenticateDeviceSuccess(byte[] bArr);

    @DexIgnore
    void onConfigureMicroAppFail(i90 i90);

    @DexIgnore
    void onConfigureMicroAppSuccess();

    @DexIgnore
    void onDataTransferCompleted();

    @DexIgnore
    void onDataTransferFailed(i90 i90);

    @DexIgnore
    void onDataTransferProgressChange(float f);

    @DexIgnore
    void onDeviceFound(Device device, int i);

    @DexIgnore
    void onEraseDataFilesFailed(i90 i90);

    @DexIgnore
    void onEraseDataFilesSuccess();

    @DexIgnore
    void onEraseHWLogFailed(i90 i90);

    @DexIgnore
    void onEraseHWLogSuccess();

    @DexIgnore
    void onExchangeSecretKeyFail(i90 i90);

    @DexIgnore
    void onExchangeSecretKeySuccess(byte[] bArr);

    @DexIgnore
    void onFetchDeviceInfoFailed(i90 i90);

    @DexIgnore
    void onFetchDeviceInfoSuccess(DeviceInformation deviceInformation);

    @DexIgnore
    void onGetDeviceConfigFailed(i90 i90);

    @DexIgnore
    void onGetDeviceConfigSuccess(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap);

    @DexIgnore
    void onGetWatchParamsFail();

    @DexIgnore
    void onMoveHandFailed(i90 i90);

    @DexIgnore
    void onMoveHandSuccess();

    @DexIgnore
    void onNextSession();

    @DexIgnore
    void onPlayAnimationFail(i90 i90);

    @DexIgnore
    void onPlayAnimationSuccess();

    @DexIgnore
    void onPlayDeviceAnimation(boolean z, i90 i90);

    @DexIgnore
    void onReadCurrentWorkoutSessionFailed(i90 i90);

    @DexIgnore
    void onReadCurrentWorkoutSessionSuccess(WorkoutSession workoutSession);

    @DexIgnore
    void onReadDataFilesFailed(i90 i90);

    @DexIgnore
    void onReadDataFilesProgressChanged(float f);

    @DexIgnore
    void onReadDataFilesSuccess(FitnessData[] fitnessDataArr);

    @DexIgnore
    void onReadHWLogFailed(i90 i90);

    @DexIgnore
    void onReadHWLogProgressChanged(float f);

    @DexIgnore
    void onReadHWLogSuccess();

    @DexIgnore
    void onReadRssiFailed(i90 i90);

    @DexIgnore
    void onReadRssiSuccess(int i);

    @DexIgnore
    void onReleaseHandControlFailed(i90 i90);

    @DexIgnore
    void onReleaseHandControlSuccess();

    @DexIgnore
    void onRequestHandControlFailed(i90 i90);

    @DexIgnore
    void onRequestHandControlSuccess();

    @DexIgnore
    void onResetHandsFailed(i90 i90);

    @DexIgnore
    void onResetHandsSuccess();

    @DexIgnore
    void onScanFail(ScanError scanError);

    @DexIgnore
    void onSendMicroAppDataFail(i90 i90);

    @DexIgnore
    void onSendMicroAppDataSuccess();

    @DexIgnore
    void onSetAlarmFailed(i90 i90);

    @DexIgnore
    void onSetAlarmSuccess();

    @DexIgnore
    void onSetBackgroundImageFailed(i90 i90);

    @DexIgnore
    void onSetBackgroundImageSuccess();

    @DexIgnore
    void onSetComplicationFailed(i90 i90);

    @DexIgnore
    void onSetComplicationSuccess();

    @DexIgnore
    void onSetDeviceConfigFailed(i90 i90);

    @DexIgnore
    void onSetDeviceConfigSuccess();

    @DexIgnore
    void onSetFrontLightFailed(i90 i90);

    @DexIgnore
    void onSetFrontLightSuccess();

    @DexIgnore
    void onSetLocalizationDataFail(i90 i90);

    @DexIgnore
    void onSetLocalizationDataSuccess();

    @DexIgnore
    void onSetNotificationFilterFailed(i90 i90);

    @DexIgnore
    void onSetNotificationFilterProgressChanged(float f);

    @DexIgnore
    void onSetNotificationFilterSuccess();

    @DexIgnore
    void onSetWatchAppFailed(i90 i90);

    @DexIgnore
    void onSetWatchAppSuccess();

    @DexIgnore
    void onSetWatchParamsFail(i90 i90);

    @DexIgnore
    void onSetWatchParamsSuccess();

    @DexIgnore
    void onStopCurrentWorkoutSessionFailed(i90 i90);

    @DexIgnore
    void onStopCurrentWorkoutSessionSuccess();

    @DexIgnore
    void onVerifySecretKeyFail(i90 i90);

    @DexIgnore
    void onVerifySecretKeySuccess(boolean z);
}
