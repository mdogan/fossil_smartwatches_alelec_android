package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.misfit.frameworks.buttonservice.log.FailureCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class QuickResponseSession$onStart$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ QuickResponseSession this$Anon0;

    @DexIgnore
    public QuickResponseSession$onStart$Anon1(QuickResponseSession quickResponseSession) {
        this.this$Anon0 = quickResponseSession;
    }

    @DexIgnore
    public final void run() {
        this.this$Anon0.stop(FailureCode.FAILED_BY_DEVICE_DISCONNECTED);
    }
}
