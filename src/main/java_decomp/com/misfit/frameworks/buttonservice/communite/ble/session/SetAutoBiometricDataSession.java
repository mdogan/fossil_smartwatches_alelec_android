package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.device.data.config.BiometricProfile;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.config.builder.DeviceConfigBuilder;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.j40;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetAutoBiometricDataSession extends SetAutoSettingsSession {
    @DexIgnore
    public UserBiometricData mNewBiometricData;
    @DexIgnore
    public UserBiometricData mOldBiometricData;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        public DoneState() {
            super(SetAutoBiometricDataSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoBiometricDataSession.this.stop(0);
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetBiometricDataState extends BleStateAbs {
        @DexIgnore
        public h90<DeviceConfigKey[]> task;

        @DexIgnore
        public SetBiometricDataState() {
            super(SetAutoBiometricDataSession.this.getTAG());
        }

        @DexIgnore
        private final DeviceConfigItem[] prepareConfigData() {
            DeviceConfigBuilder deviceConfigBuilder = new DeviceConfigBuilder();
            try {
                BiometricProfile sDKBiometricProfile = SetAutoBiometricDataSession.this.mNewBiometricData.toSDKBiometricProfile();
                deviceConfigBuilder.a(sDKBiometricProfile.getAge(), sDKBiometricProfile.getGender(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getWearingPosition());
            } catch (Exception e) {
                SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
                setAutoBiometricDataSession.log("Set Biometric Data: exception=" + e.getMessage());
            }
            return deviceConfigBuilder.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession.log("Set Biometric Data, " + SetAutoBiometricDataSession.this.mNewBiometricData);
            this.task = SetAutoBiometricDataSession.this.getBleAdapter().setDeviceConfig(SetAutoBiometricDataSession.this.getLogSession(), prepareConfigData(), this);
            if (this.task == null) {
                SetAutoBiometricDataSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetComplicationFailed(i90 i90) {
            wd4.b(i90, "error");
            stopTimeout();
            SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession.storeBiometricData(setAutoBiometricDataSession.mNewBiometricData, true);
            SetAutoBiometricDataSession.this.stop(FailureCode.FAILED_TO_SET_BIOMETRIC_DATA);
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession.storeBiometricData(setAutoBiometricDataSession.mNewBiometricData, false);
            SetAutoBiometricDataSession setAutoBiometricDataSession2 = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession2.enterStateAsync(setAutoBiometricDataSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            h90<DeviceConfigKey[]> h90 = this.task;
            if (h90 != null) {
                h90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoBiometricDataSession(UserBiometricData userBiometricData, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_BIOMETRIC_DATA, bleAdapterImpl, bleSessionCallback);
        wd4.b(userBiometricData, "mNewBiometricData");
        wd4.b(bleAdapterImpl, "bleAdapter");
        this.mNewBiometricData = userBiometricData;
    }

    @DexIgnore
    private final void storeBiometricData(UserBiometricData userBiometricData, boolean z) {
        DevicePreferenceUtils.setAutoBiometricSettings(getBleAdapter().getContext(), new Gson().a((Object) userBiometricData));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.BIOMETRIC);
        }
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetAutoBiometricDataSession setAutoBiometricDataSession = new SetAutoBiometricDataSession(this.mNewBiometricData, getBleAdapter(), getBleSessionCallback());
        setAutoBiometricDataSession.setDevice(getDevice());
        return setAutoBiometricDataSession;
    }

    @DexIgnore
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    public void initSettings() {
        BleState bleState;
        super.initSettings();
        this.mOldBiometricData = DevicePreferenceUtils.getAutoBiometricSettings(getContext());
        if (getBleAdapter().isSupportedFeature(j40.class) == null) {
            log("This device does not support set complication apps.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (UserBiometricData.CREATOR.isSame(this.mOldBiometricData, this.mNewBiometricData)) {
            log("New biometric data and old biometric data are the same. No need to set again.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else {
            storeBiometricData(this.mNewBiometricData, true);
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_BIOMETRIC_DATA_STATE);
        }
        setStartState(bleState);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_BIOMETRIC_DATA_STATE;
        String name = SetBiometricDataState.class.getName();
        wd4.a((Object) name, "SetBiometricDataState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        wd4.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        wd4.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
