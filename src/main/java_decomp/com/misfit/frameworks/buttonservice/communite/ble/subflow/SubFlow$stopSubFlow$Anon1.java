package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.NullBleState;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$stopSubFlow$Anon1", f = "SubFlow.kt", l = {}, m = "invokeSuspend")
public final class SubFlow$stopSubFlow$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $failureCode;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SubFlow this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SubFlow$stopSubFlow$Anon1(SubFlow subFlow, int i, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = subFlow;
        this.$failureCode = i;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        SubFlow$stopSubFlow$Anon1 subFlow$stopSubFlow$Anon1 = new SubFlow$stopSubFlow$Anon1(this.this$Anon0, this.$failureCode, kc4);
        subFlow$stopSubFlow$Anon1.p$ = (lh4) obj;
        return subFlow$stopSubFlow$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SubFlow$stopSubFlow$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            if (!BleState.Companion.isNull(this.this$Anon0.getMCurrentState())) {
                this.this$Anon0.getMCurrentState().stopTimeout();
                SubFlow subFlow = this.this$Anon0;
                boolean unused = subFlow.enterSubState(new NullBleState(subFlow.getTAG()));
            }
            this.this$Anon0.onStop(this.$failureCode);
            this.this$Anon0.setExist(false);
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
