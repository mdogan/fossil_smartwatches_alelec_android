package com.misfit.frameworks.buttonservice.communite.ble;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleSession$enterStateWithDelayTime$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ BleState $state;
    @DexIgnore
    public /* final */ /* synthetic */ BleSession this$Anon0;

    @DexIgnore
    public BleSession$enterStateWithDelayTime$Anon1(BleSession bleSession, BleState bleState) {
        this.this$Anon0 = bleSession;
        this.$state = bleState;
    }

    @DexIgnore
    public final void run() {
        this.this$Anon0.enterState(this.$state);
    }
}
