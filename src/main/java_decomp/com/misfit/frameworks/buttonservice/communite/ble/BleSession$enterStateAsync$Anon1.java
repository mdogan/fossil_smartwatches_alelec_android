package com.misfit.frameworks.buttonservice.communite.ble;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.misfit.frameworks.buttonservice.communite.ble.BleSession$enterStateAsync$Anon1", f = "BleSession.kt", l = {}, m = "invokeSuspend")
public final class BleSession$enterStateAsync$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ BleState $newState;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BleSession this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleSession$enterStateAsync$Anon1(BleSession bleSession, BleState bleState, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = bleSession;
        this.$newState = bleState;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        BleSession$enterStateAsync$Anon1 bleSession$enterStateAsync$Anon1 = new BleSession$enterStateAsync$Anon1(this.this$Anon0, this.$newState, kc4);
        bleSession$enterStateAsync$Anon1.p$ = (lh4) obj;
        return bleSession$enterStateAsync$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BleSession$enterStateAsync$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            this.this$Anon0.enterState(this.$newState);
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
