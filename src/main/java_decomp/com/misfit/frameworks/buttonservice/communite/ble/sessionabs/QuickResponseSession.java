package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.facebook.internal.NativeProtocol;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class QuickResponseSession extends BleSessionAbs {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class EnableMaintainingConnectionState extends BleStateAbs {
        @DexIgnore
        public EnableMaintainingConnectionState() {
            super(QuickResponseSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            Boolean enableMaintainConnection = QuickResponseSession.this.getBleAdapter().enableMaintainConnection(QuickResponseSession.this.getLogSession());
            if (enableMaintainConnection != null) {
                if (enableMaintainConnection.booleanValue()) {
                    QuickResponseSession.this.log("Enable maintaining connection succeeded");
                } else {
                    QuickResponseSession.this.log("Enable maintaining connection failed");
                    QuickResponseSession.this.stop(FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION);
                }
            }
            QuickResponseSession quickResponseSession = QuickResponseSession.this;
            quickResponseSession.enterStateAsync(quickResponseSession.getFirstState());
            return true;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public QuickResponseSession(SessionType sessionType, CommunicateMode communicateMode, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(sessionType, communicateMode, bleAdapterImpl, bleSessionCallback);
        wd4.b(sessionType, "sessionType");
        wd4.b(communicateMode, "communicateMode");
        wd4.b(bleAdapterImpl, "bleAdapter");
        setSerial(bleAdapterImpl.getSerial());
        setContext(bleAdapterImpl.getContext());
    }

    @DexIgnore
    public abstract BleState getFirstState();

    @DexIgnore
    public void initSettings() {
    }

    @DexIgnore
    public void initStateMap() {
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.ENABLE_MAINTAINING_CONNECTION_STATE;
        String name = EnableMaintainingConnectionState.class.getName();
        wd4.a((Object) name, "EnableMaintainingConnectionState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }

    @DexIgnore
    public boolean onStart(Object... objArr) {
        wd4.b(objArr, NativeProtocol.WEB_DIALOG_PARAMS);
        super.onStart(Arrays.copyOf(objArr, objArr.length));
        initSettings();
        if (getBleAdapter().isDeviceReady()) {
            Device deviceObj = getBleAdapter().getDeviceObj();
            if (!(deviceObj != null ? deviceObj.isActive() : false)) {
                enterStateAsync(createConcreteState(BleSessionAbs.SessionState.ENABLE_MAINTAINING_CONNECTION_STATE));
                return true;
            }
            enterStateAsync(getFirstState());
            return true;
        }
        log("Device is disconnected, end now.");
        enterTaskWithDelayTime(new QuickResponseSession$onStart$Anon1(this), 500);
        return true;
    }
}
