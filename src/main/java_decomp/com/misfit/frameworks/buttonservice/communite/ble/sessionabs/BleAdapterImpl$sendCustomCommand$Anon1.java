package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl$sendCustomCommand$Anon1 extends Lambda implements jd4<cb4, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ CustomRequest $command;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$sendCustomCommand$Anon1(BleAdapterImpl bleAdapterImpl, CustomRequest customRequest) {
        super(1);
        this.this$Anon0 = bleAdapterImpl;
        this.$command = customRequest;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((cb4) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(cb4 cb4) {
        wd4.b(cb4, "it");
        BleAdapterImpl bleAdapterImpl = this.this$Anon0;
        FLogger.Session session = FLogger.Session.OTHER;
        bleAdapterImpl.log(session, "Send Custom Command Success: " + this.$command);
    }
}
