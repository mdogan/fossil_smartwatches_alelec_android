package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetFrontLightEnableSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ boolean isFrontLightEnable;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetFrontLightEnableState extends BleStateAbs {
        @DexIgnore
        public h90<cb4> task;

        @DexIgnore
        public SetFrontLightEnableState() {
            super(SetFrontLightEnableSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            SetFrontLightEnableSession setFrontLightEnableSession = SetFrontLightEnableSession.this;
            setFrontLightEnableSession.log("Set Front Light Enable: " + SetFrontLightEnableSession.this.isFrontLightEnable);
            this.task = SetFrontLightEnableSession.this.getBleAdapter().setFrontLightEnable(SetFrontLightEnableSession.this.getLogSession(), SetFrontLightEnableSession.this.isFrontLightEnable, this);
            if (this.task == null) {
                SetFrontLightEnableSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetFrontLightFailed(i90 i90) {
            wd4.b(i90, "error");
            stopTimeout();
            SetFrontLightEnableSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        public void onSetFrontLightSuccess() {
            stopTimeout();
            SetFrontLightEnableSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            h90<cb4> h90 = this.task;
            if (h90 != null) {
                h90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetFrontLightEnableSession(boolean z, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_FRONT_LIGHT_ENABLE, bleAdapterImpl, bleSessionCallback);
        wd4.b(bleAdapterImpl, "bleAdapter");
        this.isFrontLightEnable = z;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetFrontLightEnableSession setFrontLightEnableSession = new SetFrontLightEnableSession(this.isFrontLightEnable, getBleAdapter(), getBleSessionCallback());
        setFrontLightEnableSession.setDevice(getDevice());
        return setFrontLightEnableSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_FRONT_LIGHT_ENABLE_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_FRONT_LIGHT_ENABLE_STATE;
        String name = SetFrontLightEnableState.class.getName();
        wd4.a((Object) name, "SetFrontLightEnableState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
