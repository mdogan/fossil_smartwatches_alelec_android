package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PlayAnimationSession extends EnableMaintainingSession {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PlayDeviceAnimationState extends BleStateAbs {
        @DexIgnore
        public h90<cb4> task;

        @DexIgnore
        public PlayDeviceAnimationState() {
            super(PlayAnimationSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = PlayAnimationSession.this.getBleAdapter().playDeviceAnimation(PlayAnimationSession.this.getLogSession(), this);
            if (this.task == null) {
                PlayAnimationSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onPlayDeviceAnimation(boolean z, i90 i90) {
            stopTimeout();
            if (z) {
                PlayAnimationSession.this.stop(0);
            } else {
                PlayAnimationSession.this.stop(201);
            }
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            h90<cb4> h90 = this.task;
            if (h90 != null) {
                h90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PlayAnimationSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.URGENT, CommunicateMode.PLAY_ANIMATION, bleAdapterImpl, bleSessionCallback);
        wd4.b(bleAdapterImpl, "bleAdapter");
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        PlayAnimationSession playAnimationSession = new PlayAnimationSession(getBleAdapter(), getBleSessionCallback());
        playAnimationSession.setDevice(getDevice());
        return playAnimationSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.PLAY_DEVICE_ANIMATION_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.PLAY_DEVICE_ANIMATION_STATE;
        String name = PlayDeviceAnimationState.class.getName();
        wd4.a((Object) name, "PlayDeviceAnimationState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
