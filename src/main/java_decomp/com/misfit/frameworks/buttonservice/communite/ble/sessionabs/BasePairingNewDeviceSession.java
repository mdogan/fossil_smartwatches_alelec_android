package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.t30;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BasePairingNewDeviceSession extends EnableMaintainingSession {
    @DexIgnore
    public /* final */ BleCommunicator.CommunicationResultCallback communicationResultCallback;
    @DexIgnore
    public byte[] firmwareBytes;
    @DexIgnore
    public FirmwareData firmwareData;
    @DexIgnore
    public boolean isJustUpdateFW;
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class FetchDeviceInfoState extends BleStateAbs {
        @DexIgnore
        public h90<DeviceInformation> task;

        @DexIgnore
        public FetchDeviceInfoState() {
            super(BasePairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = BasePairingNewDeviceSession.this.getBleAdapter().fetchDeviceInfo(BasePairingNewDeviceSession.this.getLogSession(), this);
            if (this.task == null) {
                BasePairingNewDeviceSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onFetchDeviceInfoFailed(i90 i90) {
            wd4.b(i90, "error");
            stopTimeout();
            BasePairingNewDeviceSession.this.stop(FailureCode.FAILED_TO_CONNECT);
        }

        @DexIgnore
        public void onFetchDeviceInfoSuccess(DeviceInformation deviceInformation) {
            wd4.b(deviceInformation, "deviceInformation");
            stopTimeout();
            if (BasePairingNewDeviceSession.this.getBleAdapter().isSupportedFeature(t30.class) != null) {
                BasePairingNewDeviceSession basePairingNewDeviceSession = BasePairingNewDeviceSession.this;
                basePairingNewDeviceSession.enterStateAsync(basePairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW));
                return;
            }
            BasePairingNewDeviceSession basePairingNewDeviceSession2 = BasePairingNewDeviceSession.this;
            basePairingNewDeviceSession2.enterStateAsync(basePairingNewDeviceSession2.createConcreteState(BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            h90<DeviceInformation> h90 = this.task;
            if (h90 != null) {
                h90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BasePairingNewDeviceSession(UserProfile userProfile2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback, BleCommunicator.CommunicationResultCallback communicationResultCallback2) {
        super(SessionType.SPECIAL, CommunicateMode.LINK, bleAdapterImpl, bleSessionCallback);
        wd4.b(userProfile2, "userProfile");
        wd4.b(bleAdapterImpl, "bleAdapter");
        this.userProfile = userProfile2;
        this.communicationResultCallback = communicationResultCallback2;
        setSkipEnableMaintainingConnection(true);
        setLogSession(FLogger.Session.PAIR);
        this.userProfile.setNewDevice(true);
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wd4.b(bleSession, "bleSession");
        return true;
    }

    @DexIgnore
    public final BleCommunicator.CommunicationResultCallback getCommunicationResultCallback() {
        return this.communicationResultCallback;
    }

    @DexIgnore
    public final byte[] getFirmwareBytes() {
        return this.firmwareBytes;
    }

    @DexIgnore
    public final FirmwareData getFirmwareData() {
        return this.firmwareData;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE);
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE;
        String name = FetchDeviceInfoState.class.getName();
        wd4.a((Object) name, "FetchDeviceInfoState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }

    @DexIgnore
    public final boolean isJustUpdateFW() {
        return this.isJustUpdateFW;
    }

    @DexIgnore
    public final void setFirmwareBytes(byte[] bArr) {
        this.firmwareBytes = bArr;
    }

    @DexIgnore
    public final void setFirmwareData(FirmwareData firmwareData2) {
        this.firmwareData = firmwareData2;
    }

    @DexIgnore
    public final void setJustUpdateFW(boolean z) {
        this.isJustUpdateFW = z;
    }
}
