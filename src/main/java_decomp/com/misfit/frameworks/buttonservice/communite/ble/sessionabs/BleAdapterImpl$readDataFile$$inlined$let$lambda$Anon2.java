package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.device.data.config.BiometricProfile;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.fitness.FitnessData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon2 extends Lambda implements jd4<FitnessData[], cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ BiometricProfile $biometricProfile$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon2(BleAdapterImpl bleAdapterImpl, BiometricProfile biometricProfile, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$Anon0 = bleAdapterImpl;
        this.$biometricProfile$inlined = biometricProfile;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((FitnessData[]) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(FitnessData[] fitnessDataArr) {
        wd4.b(fitnessDataArr, "it");
        this.this$Anon0.log(this.$logSession$inlined, "Read Data Files Success");
        this.$callback$inlined.onReadDataFilesSuccess(fitnessDataArr);
    }
}
