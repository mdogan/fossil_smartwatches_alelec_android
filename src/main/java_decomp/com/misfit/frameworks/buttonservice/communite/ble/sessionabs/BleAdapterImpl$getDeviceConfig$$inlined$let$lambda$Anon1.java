package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.HashMap;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon1 extends Lambda implements jd4<HashMap<DeviceConfigKey, DeviceConfigItem>, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$Anon0 = bleAdapterImpl;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((HashMap<DeviceConfigKey, DeviceConfigItem>) (HashMap) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap) {
        wd4.b(hashMap, "it");
        this.this$Anon0.log(this.$logSession$inlined, "Get Device Configuration Success");
        this.this$Anon0.mDeviceConfiguration = hashMap;
        this.$callback$inlined.onGetDeviceConfigSuccess(hashMap);
    }
}
