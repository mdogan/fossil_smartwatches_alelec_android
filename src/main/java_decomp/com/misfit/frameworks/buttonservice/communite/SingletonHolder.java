package com.misfit.frameworks.buttonservice.communite;

import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class SingletonHolder<T, A> {
    @DexIgnore
    public jd4<? super A, ? extends T> creator;
    @DexIgnore
    public volatile T instance;

    @DexIgnore
    public SingletonHolder(jd4<? super A, ? extends T> jd4) {
        wd4.b(jd4, "creator");
        this.creator = jd4;
    }

    @DexIgnore
    public final T getInstance(A a) {
        T t;
        T t2 = this.instance;
        if (t2 != null) {
            return t2;
        }
        synchronized (this) {
            t = this.instance;
            if (t == null) {
                jd4 jd4 = this.creator;
                if (jd4 != null) {
                    t = jd4.invoke(a);
                    this.instance = t;
                    this.creator = null;
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }
        return t;
    }
}
