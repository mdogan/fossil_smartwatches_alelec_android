package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.v40;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BaseSwitchActiveDeviceSession extends EnableMaintainingSession {
    @DexIgnore
    public /* final */ BleCommunicator.CommunicationResultCallback communicationResultCallback;
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class FetchDeviceInfoState extends BleStateAbs {
        @DexIgnore
        public h90<DeviceInformation> task;

        @DexIgnore
        public FetchDeviceInfoState() {
            super(BaseSwitchActiveDeviceSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = BaseSwitchActiveDeviceSession.this.getBleAdapter().fetchDeviceInfo(BaseSwitchActiveDeviceSession.this.getLogSession(), this);
            if (this.task == null) {
                BaseSwitchActiveDeviceSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onFetchDeviceInfoFailed(i90 i90) {
            wd4.b(i90, "error");
            stopTimeout();
            Device deviceObj = BaseSwitchActiveDeviceSession.this.getBleAdapter().getDeviceObj();
            if ((deviceObj != null ? deviceObj.getState() : null) != Device.State.CONNECTED) {
                BaseSwitchActiveDeviceSession.this.stop(FailureCode.FAILED_TO_CONNECT);
            } else if (!retry(BaseSwitchActiveDeviceSession.this.getBleAdapter().getContext(), BaseSwitchActiveDeviceSession.this.getSerial())) {
                BaseSwitchActiveDeviceSession.this.log("Reach the limit retry. Stop.");
                BaseSwitchActiveDeviceSession.this.stop(FailureCode.FAILED_TO_CONNECT);
            }
        }

        @DexIgnore
        public void onFetchDeviceInfoSuccess(DeviceInformation deviceInformation) {
            wd4.b(deviceInformation, "deviceInformation");
            stopTimeout();
            if (BaseSwitchActiveDeviceSession.this.getBleAdapter().isSupportedFeature(v40.class) != null) {
                BaseSwitchActiveDeviceSession baseSwitchActiveDeviceSession = BaseSwitchActiveDeviceSession.this;
                baseSwitchActiveDeviceSession.enterStateAsync(baseSwitchActiveDeviceSession.createConcreteState(BleSessionAbs.SessionState.VERIFY_SECRET_KEY));
                return;
            }
            BaseSwitchActiveDeviceSession baseSwitchActiveDeviceSession2 = BaseSwitchActiveDeviceSession.this;
            baseSwitchActiveDeviceSession2.enterStateAsync(baseSwitchActiveDeviceSession2.createConcreteState(BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            h90<DeviceInformation> h90 = this.task;
            if (h90 != null) {
                h90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseSwitchActiveDeviceSession(UserProfile userProfile2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback, BleCommunicator.CommunicationResultCallback communicationResultCallback2) {
        super(SessionType.UI, CommunicateMode.SWITCH_DEVICE, bleAdapterImpl, bleSessionCallback);
        wd4.b(userProfile2, "userProfile");
        wd4.b(bleAdapterImpl, "bleAdapter");
        this.userProfile = userProfile2;
        this.communicationResultCallback = communicationResultCallback2;
        setSkipEnableMaintainingConnection(true);
        setLogSession(FLogger.Session.OTHER);
        this.userProfile.setNewDevice(true);
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(getBleAdapter()));
    }

    @DexIgnore
    public final BleCommunicator.CommunicationResultCallback getCommunicationResultCallback() {
        return this.communicationResultCallback;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE);
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE;
        String name = FetchDeviceInfoState.class.getName();
        wd4.a((Object) name, "FetchDeviceInfoState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
