package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import com.fossil.blesdk.adapter.ScanError;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.workoutsession.WorkoutSession;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.fitness.FitnessData;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BleStateAbs extends BleState implements ISessionSdkCallback {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ int MAKE_DEVICE_READY_TIMEOUT; // = 30000;
    @DexIgnore
    public static /* final */ int OTA_SUCCESS_TIMEOUT; // = 60000;
    @DexIgnore
    public static /* final */ int PROGRESS_TASK_MAXIMUM_RETRY; // = 3;
    @DexIgnore
    public static /* final */ int PROGRESS_UPDATE_TIMEOUT; // = 30000;
    @DexIgnore
    public static /* final */ int TASK_MAXIMUM_RETRY; // = 3;
    @DexIgnore
    public int maxRetries; // = 3;
    @DexIgnore
    public int retryCounter;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleStateAbs(String str) {
        super(str);
        wd4.b(str, "tagName");
        setTimeout(30000);
    }

    @DexIgnore
    public final int getMaxRetries() {
        return this.maxRetries;
    }

    @DexIgnore
    public final int getRetryCounter() {
        return this.retryCounter;
    }

    @DexIgnore
    public void onApplyHandPositionFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onApplyHandPositionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onAuthenticateDeviceFail(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onAuthenticateDeviceSuccess(byte[] bArr) {
        wd4.b(bArr, "randomKey");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onConfigureMicroAppFail(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onConfigureMicroAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferCompleted() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferProgressChange(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDeviceFound(Device device, int i) {
        wd4.b(device, "device");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseDataFilesFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseDataFilesSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseHWLogFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onExchangeSecretKeyFail(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onExchangeSecretKeySuccess(byte[] bArr) {
        wd4.b(bArr, "secretKey");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onFetchDeviceInfoFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onFetchDeviceInfoSuccess(DeviceInformation deviceInformation) {
        wd4.b(deviceInformation, "deviceInformation");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onGetDeviceConfigFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onGetDeviceConfigSuccess(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap) {
        wd4.b(hashMap, "deviceConfiguration");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onGetWatchParamsFail() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onMoveHandFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onMoveHandSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onNextSession() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onPlayAnimationFail(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onPlayAnimationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onPlayDeviceAnimation(boolean z, i90 i90) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadCurrentWorkoutSessionFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadCurrentWorkoutSessionSuccess(WorkoutSession workoutSession) {
        wd4.b(workoutSession, "workoutSession");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
        wd4.b(fitnessDataArr, "data");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadRssiFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadRssiSuccess(int i) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReleaseHandControlFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReleaseHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onRequestHandControlFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onRequestHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onResetHandsFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onResetHandsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onScanFail(ScanError scanError) {
        wd4.b(scanError, "scanError");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSendMicroAppDataFail(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSendMicroAppDataSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetAlarmFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetAlarmSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetBackgroundImageFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetBackgroundImageSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetComplicationFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetComplicationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetDeviceConfigFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetDeviceConfigSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetFrontLightFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetFrontLightSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetLocalizationDataFail(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetLocalizationDataSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetNotificationFilterFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetNotificationFilterProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetNotificationFilterSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchAppFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchParamsFail(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchParamsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onStopCurrentWorkoutSessionFailed(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onStopCurrentWorkoutSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onVerifySecretKeyFail(i90 i90) {
        wd4.b(i90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onVerifySecretKeySuccess(boolean z) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public final boolean retry(Context context, String str) {
        wd4.b(context, "context");
        wd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "Retry state: " + getTAG() + ", counter=" + this.retryCounter + ", maxRetries=" + this.maxRetries);
        MFLogManager instance = MFLogManager.getInstance(context);
        instance.addLogForActiveLog(str, "Retry state: " + getTAG() + ", counter=" + this.retryCounter + ", maxRetries=" + this.maxRetries);
        int i = this.retryCounter;
        if (i >= this.maxRetries) {
            return false;
        }
        this.retryCounter = i + 1;
        onEnter();
        return true;
    }

    @DexIgnore
    public final void setMaxRetries(int i) {
        this.maxRetries = i;
    }

    @DexIgnore
    public final void setRetryCounter(int i) {
        this.retryCounter = i;
    }
}
