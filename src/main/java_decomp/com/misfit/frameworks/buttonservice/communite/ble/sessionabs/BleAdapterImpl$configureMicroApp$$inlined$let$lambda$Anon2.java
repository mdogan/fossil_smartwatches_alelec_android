package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.error.Error;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon2 extends Lambda implements jd4<Error, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $mappings$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon2(BleAdapterImpl bleAdapterImpl, List list, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$Anon0 = bleAdapterImpl;
        this.$mappings$inlined = list;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Error) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Error error) {
        wd4.b(error, "it");
        this.this$Anon0.logSdkError(this.$logSession$inlined, "Configure MicroApp Failed", ErrorCodeBuilder.Step.SET_MICRO_APP, error);
        this.$callback$inlined.onConfigureMicroAppFail(error.getErrorCode());
    }
}
