package com.misfit.frameworks.buttonservice.communite.ble.device;

import com.fossil.blesdk.error.Error;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicTrackInfoResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Lambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceCommunicator$sendMusicResponseFromQueue$Anon2 extends Lambda implements jd4<Error, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ MusicResponse $response;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceCommunicator this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @sc4(c = "com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendMusicResponseFromQueue$Anon2$Anon2", f = "DeviceCommunicator.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public lh4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceCommunicator$sendMusicResponseFromQueue$Anon2 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(DeviceCommunicator$sendMusicResponseFromQueue$Anon2 deviceCommunicator$sendMusicResponseFromQueue$Anon2, kc4 kc4) {
            super(2, kc4);
            this.this$Anon0 = deviceCommunicator$sendMusicResponseFromQueue$Anon2;
        }

        @DexIgnore
        public final kc4<cb4> create(Object obj, kc4<?> kc4) {
            wd4.b(kc4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, kc4);
            anon2.p$ = (lh4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            oc4.a();
            if (this.label == 0) {
                za4.a(obj);
                DeviceCommunicator$sendMusicResponseFromQueue$Anon2 deviceCommunicator$sendMusicResponseFromQueue$Anon2 = this.this$Anon0;
                deviceCommunicator$sendMusicResponseFromQueue$Anon2.this$Anon0.startSendMusicAppResponse(deviceCommunicator$sendMusicResponseFromQueue$Anon2.$response);
                return cb4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendMusicResponseFromQueue$Anon2(DeviceCommunicator deviceCommunicator, MusicResponse musicResponse) {
        super(1);
        this.this$Anon0 = deviceCommunicator;
        this.$response = musicResponse;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Error) obj);
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(Error error) {
        ErrorCodeBuilder.Step step;
        wd4.b(error, "error");
        MusicResponse musicResponse = this.$response;
        if (musicResponse instanceof NotifyMusicEventResponse) {
            step = ErrorCodeBuilder.Step.NOTIFY_MUSIC_EVENT;
        } else {
            step = musicResponse instanceof MusicTrackInfoResponse ? ErrorCodeBuilder.Step.SEND_TRACK_INFO : null;
        }
        if (step != null) {
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.OTHER;
            String serial = this.this$Anon0.getSerial();
            String access$getTAG$p = this.this$Anon0.getTAG();
            remote.e(component, session, serial, access$getTAG$p, "Send respond: " + this.$response.getType() + " Failed, error=" + ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, error));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$p2 = this.this$Anon0.getTAG();
        local.d(access$getTAG$p2, "device with serial = " + this.this$Anon0.getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue() = " + this.$response.toString() + ", push back by result error=" + error.getErrorCode());
        ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new Anon2(this, (kc4) null), 3, (Object) null);
    }
}
