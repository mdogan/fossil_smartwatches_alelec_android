package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.config.builder.DeviceConfigBuilder;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.SettingsUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetAutoSecondTimezoneSession extends SetAutoSettingsSession {
    @DexIgnore
    public String mOldSecondTimezoneId;
    @DexIgnore
    public /* final */ String mSecondTimezoneId;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneSetAutoSecondTimezoneState extends BleStateAbs {
        @DexIgnore
        public DoneSetAutoSecondTimezoneState() {
            super(SetAutoSecondTimezoneSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoSecondTimezoneSession.this.stop(0);
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetSecondTimezoneState extends BleStateAbs {
        @DexIgnore
        public h90<DeviceConfigKey[]> task;

        @DexIgnore
        public SetSecondTimezoneState() {
            super(SetAutoSecondTimezoneSession.this.getTAG());
        }

        @DexIgnore
        private final DeviceConfigItem[] prepareConfigData() {
            int timezoneRawOffsetById = ConversionUtils.getTimezoneRawOffsetById(SetAutoSecondTimezoneSession.this.mSecondTimezoneId);
            DeviceConfigBuilder deviceConfigBuilder = new DeviceConfigBuilder();
            short s = (short) timezoneRawOffsetById;
            if (SettingsUtils.INSTANCE.isSecondTimezoneInRange(s)) {
                deviceConfigBuilder.a(s);
            } else {
                SetAutoSecondTimezoneSession setAutoSecondTimezoneSession = SetAutoSecondTimezoneSession.this;
                setAutoSecondTimezoneSession.log("Set Device Config: Timezone is out of range: " + timezoneRawOffsetById);
            }
            return deviceConfigBuilder.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetAutoSecondTimezoneSession.this.getBleAdapter().setDeviceConfig(SetAutoSecondTimezoneSession.this.getLogSession(), prepareConfigData(), this);
            if (this.task == null) {
                SetAutoSecondTimezoneSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(i90 i90) {
            wd4.b(i90, "error");
            stopTimeout();
            SetAutoSecondTimezoneSession.this.stop(FailureCode.FAILED_TO_SET_SECOND_TIMEZONE);
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoSecondTimezone(SetAutoSecondTimezoneSession.this.getBleAdapter().getContext(), SetAutoSecondTimezoneSession.this.mSecondTimezoneId);
            DevicePreferenceUtils.removeSettingFlag(SetAutoSecondTimezoneSession.this.getBleAdapter().getContext(), DeviceSettings.SECOND_TIMEZONE);
            SetAutoSecondTimezoneSession setAutoSecondTimezoneSession = SetAutoSecondTimezoneSession.this;
            setAutoSecondTimezoneSession.enterStateAsync(setAutoSecondTimezoneSession.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            h90<DeviceConfigKey[]> h90 = this.task;
            if (h90 != null) {
                h90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoSecondTimezoneSession(String str, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_SECOND_TIMEZONE, bleAdapterImpl, bleSessionCallback);
        wd4.b(str, "mSecondTimezoneId");
        wd4.b(bleAdapterImpl, "bleAdapter");
        this.mSecondTimezoneId = str;
    }

    @DexIgnore
    private final void storeSettings(String str, boolean z) {
        DevicePreferenceUtils.setAutoSecondTimezone(getBleAdapter().getContext(), str);
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.SECOND_TIMEZONE);
        } else {
            DevicePreferenceUtils.removeSettingFlag(getBleAdapter().getContext(), DeviceSettings.SECOND_TIMEZONE);
        }
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetAutoSecondTimezoneSession setAutoSecondTimezoneSession = new SetAutoSecondTimezoneSession(this.mSecondTimezoneId, getBleAdapter(), getBleSessionCallback());
        setAutoSecondTimezoneSession.setDevice(getDevice());
        return setAutoSecondTimezoneSession;
    }

    @DexIgnore
    public BleState getStartState() {
        if (getBleAdapter().isDeviceReady()) {
            String str = this.mSecondTimezoneId;
            String str2 = this.mOldSecondTimezoneId;
            if (str2 == null) {
                wd4.d("mOldSecondTimezoneId");
                throw null;
            } else if (wd4.a((Object) str, (Object) str2)) {
                log("The second timezones are the same, no need to store again.");
                return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
            } else {
                storeSettings(this.mSecondTimezoneId, true);
                return createConcreteState(BleSessionAbs.SessionState.SET_SECOND_TIMEZONE_STATE);
            }
        } else {
            storeSettings(this.mSecondTimezoneId, true);
            log("Device is not ready. Cannot set second timezone.");
            return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
    }

    @DexIgnore
    public void initSettings() {
        String autoSecondTimezoneId = DevicePreferenceUtils.getAutoSecondTimezoneId(getContext());
        wd4.a((Object) autoSecondTimezoneId, "DevicePreferenceUtils.ge\u2026SecondTimezoneId(context)");
        this.mOldSecondTimezoneId = autoSecondTimezoneId;
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_SECOND_TIMEZONE_STATE;
        String name = SetSecondTimezoneState.class.getName();
        wd4.a((Object) name, "SetSecondTimezoneState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneSetAutoSecondTimezoneState.class.getName();
        wd4.a((Object) name2, "DoneSetAutoSecondTimezoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }
}
