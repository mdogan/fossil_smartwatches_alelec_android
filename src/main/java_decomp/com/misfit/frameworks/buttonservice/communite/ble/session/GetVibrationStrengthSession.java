package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.config.VibeStrengthConfig;
import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import java.util.Arrays;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetVibrationStrengthSession extends EnableMaintainingSession {
    @DexIgnore
    public VibeStrengthConfig.VibeStrengthLevel mVibrationStrengthLevel; // = VibeStrengthConfig.VibeStrengthLevel.MEDIUM;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetVibrationStrengthState extends BleStateAbs {
        @DexIgnore
        public h90<HashMap<DeviceConfigKey, DeviceConfigItem>> task;

        @DexIgnore
        public GetVibrationStrengthState() {
            super(GetVibrationStrengthSession.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap) {
            GetVibrationStrengthSession getVibrationStrengthSession = GetVibrationStrengthSession.this;
            be4 be4 = be4.a;
            Object[] objArr = {hashMap.get(DeviceConfigKey.TIME), hashMap.get(DeviceConfigKey.BATTERY), hashMap.get(DeviceConfigKey.BIOMETRIC_PROFILE), hashMap.get(DeviceConfigKey.DAILY_STEP), hashMap.get(DeviceConfigKey.DAILY_STEP_GOAL), hashMap.get(DeviceConfigKey.DAILY_CALORIE), hashMap.get(DeviceConfigKey.DAILY_CALORIE_GOAL), hashMap.get(DeviceConfigKey.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(DeviceConfigKey.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(DeviceConfigKey.DAILY_DISTANCE), hashMap.get(DeviceConfigKey.INACTIVE_NUDGE), hashMap.get(DeviceConfigKey.VIBE_STRENGTH), hashMap.get(DeviceConfigKey.DO_NOT_DISTURB_SCHEDULE)};
            String format = String.format("Get configuration  " + GetVibrationStrengthSession.this.getSerial() + "\n" + "\t[timeConfiguration: \n" + "\ttime = %s,\n" + "\tbattery = %s,\n" + "\tbiometric = %s,\n" + "\tdaily steps = %s,\n" + "\tdaily step goal = %s, \n" + "\tdaily calorie: %s, \n" + "\tdaily calorie goal: %s, \n" + "\tdaily total active minute: %s, \n" + "\tdaily active minute goal: %s, \n" + "\tdaily distance: %s, \n" + "\tinactive nudge: %s, \n" + "\tvibration strength: %s, \n" + "\tdo not disturb schedule: %s, \n" + "\t]", Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            getVibrationStrengthSession.log(format);
        }

        @DexIgnore
        public boolean onEnter() {
            GetVibrationStrengthSession.this.log("Get Vibration Strength Level");
            this.task = GetVibrationStrengthSession.this.getBleAdapter().getDeviceConfig(GetVibrationStrengthSession.this.getLogSession(), this);
            if (this.task == null) {
                GetVibrationStrengthSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onGetDeviceConfigFailed(i90 i90) {
            wd4.b(i90, "error");
            stopTimeout();
            GetVibrationStrengthSession.this.stop(FailureCode.FAILED_TO_GET_VIBRATION_STRENGTH);
        }

        @DexIgnore
        public void onGetDeviceConfigSuccess(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap) {
            wd4.b(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            if (hashMap.containsKey(DeviceConfigKey.VIBE_STRENGTH)) {
                GetVibrationStrengthSession getVibrationStrengthSession = GetVibrationStrengthSession.this;
                DeviceConfigItem deviceConfigItem = hashMap.get(DeviceConfigKey.VIBE_STRENGTH);
                if (deviceConfigItem != null) {
                    getVibrationStrengthSession.mVibrationStrengthLevel = ((VibeStrengthConfig) deviceConfigItem).getVibeStrengthLevel();
                    GetVibrationStrengthSession getVibrationStrengthSession2 = GetVibrationStrengthSession.this;
                    getVibrationStrengthSession2.log("Get Vibration Strength Level Success, value=" + GetVibrationStrengthSession.this.mVibrationStrengthLevel);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
                }
            } else {
                GetVibrationStrengthSession.this.log("Get Vibration Strength Level Success, but no value.");
            }
            GetVibrationStrengthSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            h90<HashMap<DeviceConfigKey, DeviceConfigItem>> h90 = this.task;
            if (h90 != null) {
                h90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetVibrationStrengthSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.READ_REAL_TIME_STEP, bleAdapterImpl, bleSessionCallback);
        wd4.b(bleAdapterImpl, "bleAdapter");
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable(ButtonService.Companion.getVIBRATION_STRENGTH_LEVEL(), VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel$default(VibrationStrengthObj.Companion, this.mVibrationStrengthLevel, false, 2, (Object) null));
    }

    @DexIgnore
    public BleSession copyObject() {
        GetVibrationStrengthSession getVibrationStrengthSession = new GetVibrationStrengthSession(getBleAdapter(), getBleSessionCallback());
        getVibrationStrengthSession.setDevice(getDevice());
        return getVibrationStrengthSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.GET_VIBRATION_STRENGTH_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.GET_VIBRATION_STRENGTH_STATE;
        String name = GetVibrationStrengthState.class.getName();
        wd4.a((Object) name, "GetVibrationStrengthState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
