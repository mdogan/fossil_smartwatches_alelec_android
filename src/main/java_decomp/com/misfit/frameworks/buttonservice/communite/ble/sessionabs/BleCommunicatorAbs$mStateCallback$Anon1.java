package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.event.DeviceEvent;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleCommunicatorAbs$mStateCallback$Anon1 implements Device.a {
    @DexIgnore
    public /* final */ /* synthetic */ BleCommunicatorAbs this$Anon0;

    @DexIgnore
    public BleCommunicatorAbs$mStateCallback$Anon1(BleCommunicatorAbs bleCommunicatorAbs) {
        this.this$Anon0 = bleCommunicatorAbs;
    }

    @DexIgnore
    public void onDeviceStateChanged(Device device, Device.State state, Device.State state2) {
        wd4.b(device, "device");
        wd4.b(state, "previousState");
        wd4.b(state2, "newState");
        this.this$Anon0.handleDeviceStateChanged(device, state, state2);
    }

    @DexIgnore
    public void onEventReceived(Device device, DeviceEvent deviceEvent) {
        wd4.b(device, "device");
        wd4.b(deviceEvent, Constants.EVENT);
        this.this$Anon0.handleEventReceived(device, deviceEvent);
    }
}
