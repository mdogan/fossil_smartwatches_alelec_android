package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl$readRssi$$inlined$let$lambda$Anon1 extends Lambda implements jd4<Integer, cb4> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$readRssi$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$Anon0 = bleAdapterImpl;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke(((Number) obj).intValue());
        return cb4.a;
    }

    @DexIgnore
    public final void invoke(int i) {
        BleAdapterImpl bleAdapterImpl = this.this$Anon0;
        FLogger.Session session = this.$logSession$inlined;
        bleAdapterImpl.log(session, "Read Rssi Success, rssi=" + i);
        this.$callback$inlined.onReadRssiSuccess(i);
    }
}
