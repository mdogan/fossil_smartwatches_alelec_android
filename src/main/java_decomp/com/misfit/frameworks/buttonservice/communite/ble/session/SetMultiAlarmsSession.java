package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetMultiAlarmsSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ List<AlarmSetting> mAlarmSettingList;
    @DexIgnore
    public List<AlarmSetting> mOldMultiAlarmSettings;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetListAlarmsState extends BleStateAbs {
        @DexIgnore
        public h90<cb4> task;

        @DexIgnore
        public SetListAlarmsState() {
            super(SetMultiAlarmsSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetMultiAlarmsSession.this.getBleAdapter().setAlarms(SetMultiAlarmsSession.this.getLogSession(), SetMultiAlarmsSession.this.mAlarmSettingList, this);
            if (this.task == null) {
                SetMultiAlarmsSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetAlarmFailed(i90 i90) {
            wd4.b(i90, "error");
            stopTimeout();
            if (!retry(SetMultiAlarmsSession.this.getContext(), SetMultiAlarmsSession.this.getSerial())) {
                SetMultiAlarmsSession.this.log("Reach the limit retry. Stop.");
                SetMultiAlarmsSession.this.stop(FailureCode.FAILED_TO_SET_ALARM);
            }
        }

        @DexIgnore
        public void onSetAlarmSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoListAlarm(SetMultiAlarmsSession.this.getBleAdapter().getContext(), SetMultiAlarmsSession.this.mAlarmSettingList);
            SetMultiAlarmsSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            h90<cb4> h90 = this.task;
            if (h90 != null) {
                h90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetMultiAlarmsSession(BleAdapterImpl bleAdapterImpl, List<AlarmSetting> list, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_LIST_ALARM, bleAdapterImpl, bleSessionCallback);
        wd4.b(bleAdapterImpl, "bleAdapter");
        wd4.b(list, "mAlarmSettingList");
        this.mAlarmSettingList = list;
        setLogSession(FLogger.Session.SET_ALARM);
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wd4.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_MULTI_ALARM) ? false : true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetMultiAlarmsSession setMultiAlarmsSession = new SetMultiAlarmsSession(getBleAdapter(), this.mAlarmSettingList, getBleSessionCallback());
        setMultiAlarmsSession.setDevice(getDevice());
        return setMultiAlarmsSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_LIST_ALARMS_STATE);
    }

    @DexIgnore
    public void initSettings() {
        super.initSettings();
        this.mOldMultiAlarmSettings = DevicePreferenceUtils.getAutoListAlarm(getBleAdapter().getContext());
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_LIST_ALARMS_STATE;
        String name = SetListAlarmsState.class.getName();
        wd4.a((Object) name, "SetListAlarmsState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
