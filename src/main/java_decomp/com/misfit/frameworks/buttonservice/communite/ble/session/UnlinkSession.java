package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.facebook.internal.NativeProtocol;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UnlinkSession extends BleSessionAbs {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class CloseConnectionState extends BleStateAbs {
        @DexIgnore
        public CloseConnectionState() {
            super(UnlinkSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            UnlinkSession.this.getBleAdapter().closeConnection(UnlinkSession.this.getLogSession(), true);
            UnlinkSession unlinkSession = UnlinkSession.this;
            unlinkSession.log("Unlink Device " + UnlinkSession.this.getSerial() + ": Close connection is called.");
            UnlinkSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UnlinkSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.SPECIAL, CommunicateMode.UNLINK, bleAdapterImpl, bleSessionCallback);
        wd4.b(bleAdapterImpl, "bleAdapter");
        setSerial(bleAdapterImpl.getSerial());
        setContext(bleAdapterImpl.getContext());
        setLogSession(FLogger.Session.REMOVE_DEVICE);
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        UnlinkSession unlinkSession = new UnlinkSession(getBleAdapter(), getBleSessionCallback());
        unlinkSession.setDevice(getDevice());
        return unlinkSession;
    }

    @DexIgnore
    public void initStateMap() {
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE;
        String name = CloseConnectionState.class.getName();
        wd4.a((Object) name, "CloseConnectionState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }

    @DexIgnore
    public boolean onStart(Object... objArr) {
        wd4.b(objArr, NativeProtocol.WEB_DIALOG_PARAMS);
        super.onStart(Arrays.copyOf(objArr, objArr.length));
        enterStateAsync(createConcreteState(BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE));
        return true;
    }
}
