package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.config.builder.DeviceConfigBuilder;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import java.util.HashMap;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UpdateCurrentTimeSession extends EnableMaintainingSession {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetTimeState extends BleStateAbs {
        @DexIgnore
        public h90<DeviceConfigKey[]> task;

        @DexIgnore
        public SetTimeState() {
            super(UpdateCurrentTimeSession.this.getTAG());
        }

        @DexIgnore
        private final DeviceConfigItem[] prepareConfigData() {
            long currentTimeMillis = System.currentTimeMillis();
            long j = (long) 1000;
            long j2 = currentTimeMillis / j;
            DeviceConfigBuilder deviceConfigBuilder = new DeviceConfigBuilder();
            deviceConfigBuilder.a(j2, (short) ((int) (currentTimeMillis - (j * j2))), (short) ((TimeZone.getDefault().getOffset(currentTimeMillis) / 1000) / 60));
            return deviceConfigBuilder.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = UpdateCurrentTimeSession.this.getBleAdapter().setDeviceConfig(UpdateCurrentTimeSession.this.getLogSession(), prepareConfigData(), this);
            if (this.task == null) {
                UpdateCurrentTimeSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(i90 i90) {
            wd4.b(i90, "error");
            stopTimeout();
            UpdateCurrentTimeSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            UpdateCurrentTimeSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            h90<DeviceConfigKey[]> h90 = this.task;
            if (h90 != null) {
                h90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateCurrentTimeSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.UPDATE_CURRENT_TIME, bleAdapterImpl, bleSessionCallback);
        wd4.b(bleAdapterImpl, "bleAdapter");
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wd4.b(bleSession, "bleSession");
        return true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        UpdateCurrentTimeSession updateCurrentTimeSession = new UpdateCurrentTimeSession(getBleAdapter(), getBleSessionCallback());
        updateCurrentTimeSession.setDevice(getDevice());
        return updateCurrentTimeSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.UPDATE_CURRENT_TIME_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.UPDATE_CURRENT_TIME_STATE;
        String name = SetTimeState.class.getName();
        wd4.a((Object) name, "SetTimeState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
