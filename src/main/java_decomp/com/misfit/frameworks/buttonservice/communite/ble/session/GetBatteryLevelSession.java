package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.device.data.config.BatteryConfig;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetBatteryLevelSession extends EnableMaintainingSession {
    @DexIgnore
    public int mBatteryLevel; // = -1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetBatteryLevelState extends BleStateAbs {
        @DexIgnore
        public h90<HashMap<DeviceConfigKey, DeviceConfigItem>> task;

        @DexIgnore
        public GetBatteryLevelState() {
            super(GetBatteryLevelSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            GetBatteryLevelSession.this.log("Get Battery Level");
            this.task = GetBatteryLevelSession.this.getBleAdapter().getDeviceConfig(GetBatteryLevelSession.this.getLogSession(), this);
            if (this.task == null) {
                GetBatteryLevelSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onGetDeviceConfigFailed(i90 i90) {
            wd4.b(i90, "error");
            stopTimeout();
            GetBatteryLevelSession getBatteryLevelSession = GetBatteryLevelSession.this;
            getBatteryLevelSession.mBatteryLevel = getBatteryLevelSession.getBleAdapter().getBatteryLevel();
            GetBatteryLevelSession getBatteryLevelSession2 = GetBatteryLevelSession.this;
            getBatteryLevelSession2.log("Get Battery Level Failed, return the old one:" + GetBatteryLevelSession.this.mBatteryLevel);
            GetBatteryLevelSession.this.stop(0);
        }

        @DexIgnore
        public void onGetDeviceConfigSuccess(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap) {
            wd4.b(hashMap, "deviceConfiguration");
            stopTimeout();
            if (hashMap.containsKey(DeviceConfigKey.BATTERY)) {
                GetBatteryLevelSession getBatteryLevelSession = GetBatteryLevelSession.this;
                DeviceConfigItem deviceConfigItem = hashMap.get(DeviceConfigKey.BATTERY);
                if (deviceConfigItem != null) {
                    getBatteryLevelSession.mBatteryLevel = ((BatteryConfig) deviceConfigItem).getPercentage();
                    GetBatteryLevelSession getBatteryLevelSession2 = GetBatteryLevelSession.this;
                    getBatteryLevelSession2.log("Get Battery Level Success, value=" + GetBatteryLevelSession.this.mBatteryLevel);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
                }
            } else {
                GetBatteryLevelSession.this.log("Get Battery Level Success, but no value.");
            }
            GetBatteryLevelSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            h90<HashMap<DeviceConfigKey, DeviceConfigItem>> h90 = this.task;
            if (h90 != null) {
                h90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetBatteryLevelSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.GET_BATTERY_LEVEL, bleAdapterImpl, bleSessionCallback);
        wd4.b(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putInt(Constants.BATTERY, this.mBatteryLevel);
    }

    @DexIgnore
    public BleSession copyObject() {
        GetBatteryLevelSession getBatteryLevelSession = new GetBatteryLevelSession(getBleAdapter(), getBleSessionCallback());
        getBatteryLevelSession.setDevice(getDevice());
        return getBatteryLevelSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.READ_REAL_TIME_STEPS_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.GET_BATTERY_LEVEL_STATE;
        String name = GetBatteryLevelState.class.getName();
        wd4.a((Object) name, "GetBatteryLevelState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
