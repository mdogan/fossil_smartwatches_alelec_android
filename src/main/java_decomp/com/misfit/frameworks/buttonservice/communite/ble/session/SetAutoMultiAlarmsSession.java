package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.g40;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.extensions.AlarmExtensionKt;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetAutoMultiAlarmsSession extends SetAutoSettingsSession {
    @DexIgnore
    public /* final */ List<AlarmSetting> mAlarmSettingList;
    @DexIgnore
    public List<AlarmSetting> mOldMultiAlarmSettings;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        public DoneState() {
            super(SetAutoMultiAlarmsSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoMultiAlarmsSession.this.stop(0);
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetListAlarmsState extends BleStateAbs {
        @DexIgnore
        public h90<cb4> task;

        @DexIgnore
        public SetListAlarmsState() {
            super(SetAutoMultiAlarmsSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            SetAutoMultiAlarmsSession.this.log("Set auto multi alarms.");
            this.task = SetAutoMultiAlarmsSession.this.getBleAdapter().setAlarms(SetAutoMultiAlarmsSession.this.getLogSession(), SetAutoMultiAlarmsSession.this.mAlarmSettingList, this);
            if (this.task == null) {
                SetAutoMultiAlarmsSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetAlarmFailed(i90 i90) {
            wd4.b(i90, "error");
            stopTimeout();
            if (!retry(SetAutoMultiAlarmsSession.this.getContext(), SetAutoMultiAlarmsSession.this.getSerial())) {
                SetAutoMultiAlarmsSession.this.log("Reach the limit retry. Stop.");
                SetAutoMultiAlarmsSession setAutoMultiAlarmsSession = SetAutoMultiAlarmsSession.this;
                setAutoMultiAlarmsSession.storeSettings(setAutoMultiAlarmsSession.mAlarmSettingList, true);
                SetAutoMultiAlarmsSession.this.stop(FailureCode.FAILED_TO_SET_ALARM);
            }
        }

        @DexIgnore
        public void onSetAlarmSuccess() {
            stopTimeout();
            SetAutoMultiAlarmsSession setAutoMultiAlarmsSession = SetAutoMultiAlarmsSession.this;
            setAutoMultiAlarmsSession.storeSettings(setAutoMultiAlarmsSession.mAlarmSettingList, false);
            SetAutoMultiAlarmsSession setAutoMultiAlarmsSession2 = SetAutoMultiAlarmsSession.this;
            setAutoMultiAlarmsSession2.enterStateAsync(setAutoMultiAlarmsSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            h90<cb4> h90 = this.task;
            if (h90 != null) {
                h90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoMultiAlarmsSession(List<AlarmSetting> list, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_MULTI_ALARM, bleAdapterImpl, bleSessionCallback);
        wd4.b(list, "mAlarmSettingList");
        wd4.b(bleAdapterImpl, "bleAdapter");
        this.mAlarmSettingList = list;
        setLogSession(FLogger.Session.SET_ALARM);
    }

    @DexIgnore
    private final void storeSettings(List<AlarmSetting> list, boolean z) {
        DevicePreferenceUtils.setAutoListAlarm(getBleAdapter().getContext(), list);
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
        }
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetAutoMultiAlarmsSession setAutoMultiAlarmsSession = new SetAutoMultiAlarmsSession(this.mAlarmSettingList, getBleAdapter(), getBleSessionCallback());
        setAutoMultiAlarmsSession.setDevice(getDevice());
        return setAutoMultiAlarmsSession;
    }

    @DexIgnore
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    public void initSettings() {
        BleState bleState;
        super.initSettings();
        List<AlarmSetting> autoListAlarm = DevicePreferenceUtils.getAutoListAlarm(getContext());
        wd4.a((Object) autoListAlarm, "DevicePreferenceUtils.getAutoListAlarm(context)");
        this.mOldMultiAlarmSettings = autoListAlarm;
        if (getBleAdapter().isSupportedFeature(g40.class) != null) {
            List<AlarmSetting> list = this.mOldMultiAlarmSettings;
            if (list != null) {
                if (!list.isEmpty()) {
                    List<AlarmSetting> list2 = this.mOldMultiAlarmSettings;
                    if (list2 == null) {
                        wd4.d("mOldMultiAlarmSettings");
                        throw null;
                    } else if (AlarmExtensionKt.isSame(list2, this.mAlarmSettingList)) {
                        log("The multi alarms are the same, no need to store again.");
                        bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
                    }
                }
                storeSettings(this.mAlarmSettingList, true);
                bleState = createConcreteState(BleSessionAbs.SessionState.SET_LIST_ALARMS_STATE);
            } else {
                wd4.d("mOldMultiAlarmSettings");
                throw null;
            }
        } else {
            log("This device does not support set multi alarm.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
        setStartState(bleState);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_LIST_ALARMS_STATE;
        String name = SetListAlarmsState.class.getName();
        wd4.a((Object) name, "SetListAlarmsState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        wd4.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        wd4.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
