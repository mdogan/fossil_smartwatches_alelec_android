package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetAutoMappingsSession extends SetAutoSettingsSession {
    @DexIgnore
    public List<? extends MicroAppMapping> mappings;
    @DexIgnore
    public List<? extends MicroAppMapping> oldMappings;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneSetAutoMappingState extends BleStateAbs {
        @DexIgnore
        public DoneSetAutoMappingState() {
            super(SetAutoMappingsSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoMappingsSession.this.stop(0);
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class WatchSetMicroAppMappingState extends BleStateAbs {
        @DexIgnore
        public g90<cb4> task;

        @DexIgnore
        public WatchSetMicroAppMappingState() {
            super(SetAutoMappingsSession.this.getTAG());
        }

        @DexIgnore
        public void onConfigureMicroAppFail(i90 i90) {
            wd4.b(i90, "error");
            stopTimeout();
            if (!retry(SetAutoMappingsSession.this.getContext(), SetAutoMappingsSession.this.getSerial())) {
                SetAutoMappingsSession.this.log("Reach the limit retry. Stop.");
                SetAutoMappingsSession setAutoMappingsSession = SetAutoMappingsSession.this;
                setAutoMappingsSession.storeMappings(setAutoMappingsSession.mappings, true);
                SetAutoMappingsSession.this.stop(FailureCode.FAILED_TO_SET_MICRO_APP_MAPPING);
            }
        }

        @DexIgnore
        public void onConfigureMicroAppSuccess() {
            SetAutoMappingsSession setAutoMappingsSession = SetAutoMappingsSession.this;
            setAutoMappingsSession.storeMappings(setAutoMappingsSession.mappings, false);
            SetAutoMappingsSession setAutoMappingsSession2 = SetAutoMappingsSession.this;
            setAutoMappingsSession2.enterState(setAutoMappingsSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            if (!SetAutoMappingsSession.this.mappings.isEmpty()) {
                BleAdapterImpl access$getBleAdapter$p = SetAutoMappingsSession.this.getBleAdapter();
                FLogger.Session access$getLogSession$p = SetAutoMappingsSession.this.getLogSession();
                List<com.fossil.blesdk.model.microapp.MicroAppMapping> convertToSDKMapping = MicroAppMapping.convertToSDKMapping(SetAutoMappingsSession.this.mappings);
                wd4.a((Object) convertToSDKMapping, "MicroAppMapping.convertToSDKMapping(mappings)");
                this.task = access$getBleAdapter$p.configureMicroApp(access$getLogSession$p, convertToSDKMapping, this);
                if (this.task == null) {
                    SetAutoMappingsSession.this.stop(10000);
                } else {
                    startTimeout();
                }
            } else {
                SetAutoMappingsSession setAutoMappingsSession = SetAutoMappingsSession.this;
                setAutoMappingsSession.log("Micro app mapping is empty, size=" + SetAutoMappingsSession.this.mappings.size());
                SetAutoMappingsSession setAutoMappingsSession2 = SetAutoMappingsSession.this;
                setAutoMappingsSession2.enterStateAsync(setAutoMappingsSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
            }
            return true;
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<cb4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoMappingsSession(List<? extends BLEMapping> list, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_MAPPING, bleAdapterImpl, bleSessionCallback);
        wd4.b(list, "microAppMappings");
        wd4.b(bleAdapterImpl, "bleAdapter");
        List<MicroAppMapping> convertToMicroAppMapping = MicroAppMapping.convertToMicroAppMapping(list);
        wd4.a((Object) convertToMicroAppMapping, "MicroAppMapping.convertT\u2026Mapping(microAppMappings)");
        this.mappings = convertToMicroAppMapping;
    }

    @DexIgnore
    private final void storeMappings(List<? extends MicroAppMapping> list, boolean z) {
        DevicePreferenceUtils.setAutoSetMapping(getBleAdapter().getContext(), getBleAdapter().getSerial(), new Gson().a((Object) list));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.MAPPINGS);
        }
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wd4.b(bleSession, "bleSession");
        return true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        List<BLEMapping> convertToBLEMapping = MicroAppMapping.convertToBLEMapping(this.mappings);
        wd4.a((Object) convertToBLEMapping, "MicroAppMapping.convertToBLEMapping(mappings)");
        SetAutoMappingsSession setAutoMappingsSession = new SetAutoMappingsSession(convertToBLEMapping, getBleAdapter(), getBleSessionCallback());
        setAutoMappingsSession.setDevice(getDevice());
        return setAutoMappingsSession;
    }

    @DexIgnore
    public BleState getStartState() {
        if (getBleAdapter().isDeviceReady()) {
            List<? extends MicroAppMapping> list = this.oldMappings;
            if (list == null) {
                wd4.d("oldMappings");
                throw null;
            } else if (BLEMapping.isMappingTheSame(list, this.mappings)) {
                log("New mappings and old mappings are the same. No need to set again.");
                return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
            } else {
                List<? extends MicroAppMapping> list2 = this.mappings;
                List<? extends MicroAppMapping> list3 = this.oldMappings;
                if (list3 == null) {
                    wd4.d("oldMappings");
                    throw null;
                } else if (BLEMapping.compareTimeStamp(list2, list3).longValue() > 0) {
                    storeMappings(this.mappings, true);
                    return createConcreteState(BleSessionAbs.SessionState.SET_MICRO_APP_MAPPING_STATE);
                } else {
                    log("Old mappings timestamp is greater then the new one. No need to set again.");
                    return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
                }
            }
        } else {
            storeMappings(this.mappings, true);
            log("Device is not ready. Cannot set mapping.");
            return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
    }

    @DexIgnore
    public void initSettings() {
        List<MicroAppMapping> convertToMicroAppMapping = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getBleAdapter().getContext(), getBleAdapter().getSerial()));
        wd4.a((Object) convertToMicroAppMapping, "MicroAppMapping.convertT\u2026text, bleAdapter.serial))");
        this.oldMappings = convertToMicroAppMapping;
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_MICRO_APP_MAPPING_STATE;
        String name = WatchSetMicroAppMappingState.class.getName();
        wd4.a((Object) name, "WatchSetMicroAppMappingState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneSetAutoMappingState.class.getName();
        wd4.a((Object) name2, "DoneSetAutoMappingState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }
}
