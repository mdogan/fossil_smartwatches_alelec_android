package com.misfit.frameworks.buttonservice.log.cloud;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.misfit.frameworks.buttonservice.log.LogEvent;
import java.util.List;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter", f = "CloudLogWriter.kt", l = {19}, m = "sendLog")
public final class CloudLogWriter$sendLog$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon10;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public Object L$Anon8;
    @DexIgnore
    public Object L$Anon9;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ CloudLogWriter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudLogWriter$sendLog$Anon1(CloudLogWriter cloudLogWriter, kc4 kc4) {
        super(kc4);
        this.this$Anon0 = cloudLogWriter;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.sendLog((List<LogEvent>) null, this);
    }
}
