package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.db.Log;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$updateCloudFlag$Anon2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$updateCloudFlag$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Log.Flag $logFlag;
    @DexIgnore
    public /* final */ /* synthetic */ List $logIds;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$updateCloudFlag$Anon2(DBLogWriter dBLogWriter, List list, Log.Flag flag, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = dBLogWriter;
        this.$logIds = list;
        this.$logFlag = flag;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DBLogWriter$updateCloudFlag$Anon2 dBLogWriter$updateCloudFlag$Anon2 = new DBLogWriter$updateCloudFlag$Anon2(this.this$Anon0, this.$logIds, this.$logFlag, kc4);
        dBLogWriter$updateCloudFlag$Anon2.p$ = (lh4) obj;
        return dBLogWriter$updateCloudFlag$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DBLogWriter$updateCloudFlag$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            for (List d : wb4.b(this.$logIds, 500)) {
                this.this$Anon0.logDao.updateCloudFlagByIds(wb4.d(d), this.$logFlag);
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
