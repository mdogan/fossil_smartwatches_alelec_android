package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.nc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.uc4;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogEndPointKt {
    @DexIgnore
    public static final <T> Object await(Call<T> call, kc4<? super RepoResponse<T>> kc4) {
        nc4 nc4 = new nc4(IntrinsicsKt__IntrinsicsJvmKt.a(kc4));
        call.a(new LogEndPointKt$await$Anon2$Anon1(nc4));
        Object a = nc4.a();
        if (a == oc4.a()) {
            uc4.c(kc4);
        }
        return a;
    }
}
