package com.misfit.frameworks.buttonservice.log;

import android.util.Log;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.lv1;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.pl4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.sh4;
import com.fossil.blesdk.obfuscated.tb4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.obfuscated.zh4;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$Anon1", f = "BufferLogWriter.kt", l = {220}, m = "invokeSuspend")
public final class BufferLogWriter$renameToFullBufferFile$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $forceFlush;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BufferLogWriter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$renameToFullBufferFile$Anon1(BufferLogWriter bufferLogWriter, boolean z, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = bufferLogWriter;
        this.$forceFlush = z;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        BufferLogWriter$renameToFullBufferFile$Anon1 bufferLogWriter$renameToFullBufferFile$Anon1 = new BufferLogWriter$renameToFullBufferFile$Anon1(this.this$Anon0, this.$forceFlush, kc4);
        bufferLogWriter$renameToFullBufferFile$Anon1.p$ = (lh4) obj;
        return bufferLogWriter$renameToFullBufferFile$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BufferLogWriter$renameToFullBufferFile$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        pl4 pl4;
        lh4 lh4;
        File file;
        Iterable iterable;
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 = this.p$;
            pl4 = this.this$Anon0.mMutex;
            this.L$Anon0 = lh4;
            this.L$Anon1 = pl4;
            this.label = 1;
            if (pl4.a((Object) null, this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 = (lh4) this.L$Anon0;
            za4.a(obj);
            pl4 = (pl4) this.L$Anon1;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        lh4 lh42 = lh4;
        try {
            File[] listFiles = new File(this.this$Anon0.directoryPath).listFiles(BufferLogWriter$renameToFullBufferFile$Anon1$Anon1$founds$Anon1.INSTANCE);
            int i2 = 0;
            if (this.this$Anon0.isMainFLogger) {
                wd4.a((Object) listFiles, "founds");
                if (listFiles.length > 1) {
                    kb4.a((T[]) listFiles, new BufferLogWriter$renameToFullBufferFile$Anon1$$special$$inlined$sortBy$Anon1());
                }
                ArrayList arrayList = new ArrayList();
                int length = listFiles.length;
                for (int i3 = 0; i3 < length; i3++) {
                    file = listFiles[i3];
                    iterable = lv1.b(file, Charset.defaultCharset());
                    tb4.a(arrayList, iterable);
                }
                List d = wb4.d(arrayList);
                int length2 = listFiles.length;
                while (i2 < length2) {
                    File file2 = listFiles[i2];
                    try {
                        file2.delete();
                    } catch (Exception e) {
                        String access$getTAG$cp = BufferLogWriter.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append(".renameToFullBufferFile(), delete buffer file, name=");
                        wd4.a((Object) file2, "it");
                        sb.append(file2.getName());
                        sb.append(", ex=");
                        sb.append(e);
                        Log.e(access$getTAG$cp, sb.toString());
                    }
                    i2++;
                }
                sh4 unused = mg4.a(lh42, zh4.a(), (CoroutineStart) null, new BufferLogWriter$renameToFullBufferFile$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(d, (kc4) null, this, lh42), 2, (Object) null);
            } else {
                wd4.a((Object) listFiles, "founds");
                if (listFiles.length == 0) {
                    i2 = 1;
                }
                if ((i2 ^ 1) != 0) {
                    sh4 unused2 = mg4.a(lh42, zh4.a(), (CoroutineStart) null, new BufferLogWriter$renameToFullBufferFile$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2((kc4) null, this, lh42), 2, (Object) null);
                } else {
                    cb4 cb4 = cb4.a;
                }
            }
            pl4.a((Object) null);
            return cb4.a;
        } catch (Exception e2) {
            String access$getTAG$cp2 = BufferLogWriter.TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(".renameToFullBufferFile(), read buffer file, name=");
            wd4.a((Object) file, "it");
            sb2.append(file.getName());
            sb2.append(", ex=");
            sb2.append(e2);
            Log.e(access$getTAG$cp2, sb2.toString());
            iterable = new ArrayList();
        } catch (Throwable th) {
            pl4.a((Object) null);
            throw th;
        }
    }
}
