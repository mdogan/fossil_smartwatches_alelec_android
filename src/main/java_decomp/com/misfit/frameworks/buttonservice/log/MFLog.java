package com.misfit.frameworks.buttonservice.log;

import android.content.Context;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.model.AppInfo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MFLog implements Serializable {
    @DexIgnore
    public static /* final */ String LOG_PATTERN; // = "[%d] --\t%s%s\n";
    @DexIgnore
    public static /* final */ String TAG_DEBUG; // = "[DEBUG] ";
    @DexIgnore
    public static /* final */ String TAG_EMPTY; // = "";
    @DexIgnore
    public static /* final */ String TAG_ERROR; // = "[ERROR] ";
    @DexIgnore
    public static /* final */ String TAG_INFO; // = "[INFO] ";
    @DexIgnore
    public String appVersion;
    @DexIgnore
    public Set<String> candidates; // = new HashSet();
    @DexIgnore
    public transient Context context;
    @DexIgnore
    public String detailLog; // = "";
    @DexIgnore
    public long endTime;
    @DexIgnore
    public List<Integer> errorCodes;
    @DexIgnore
    public String firmwareVersion;
    @DexIgnore
    public boolean isHidden;
    @DexIgnore
    public String osVersion;
    @DexIgnore
    public String phoneModel;
    @DexIgnore
    public int resultCode;
    @DexIgnore
    public String sdkVersion;
    @DexIgnore
    public String serial;
    @DexIgnore
    public long startTime; // = System.currentTimeMillis();
    @DexIgnore
    public String uuid;

    @DexIgnore
    public MFLog(Context context2) {
        this.context = context2;
        this.isHidden = false;
        this.sdkVersion = "";
        AppInfo appInfo = ButtonService.Companion.getAppInfo();
        if (appInfo != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MFLog", "MFLog - appInfo=" + appInfo.toString());
            this.osVersion = appInfo.getOsVersion();
            this.appVersion = appInfo.getAppVersion();
        }
    }

    @DexIgnore
    private void appendLog(String str, String str2) {
        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        this.detailLog += String.format(Locale.US, LOG_PATTERN, new Object[]{Long.valueOf((timeInMillis - this.startTime) / 1000), str, str2});
    }

    @DexIgnore
    public void addCandidate(String str) {
        if (!this.candidates.contains(str)) {
            this.candidates.add(str);
            appendLog("", "Found device: " + str);
        }
    }

    @DexIgnore
    public void addErrorCode(int i) {
        if (this.errorCodes == null) {
            this.errorCodes = new ArrayList();
        }
        this.errorCodes.add(Integer.valueOf(i));
    }

    @DexIgnore
    public void debug(String str) {
        appendLog(TAG_DEBUG, str);
    }

    @DexIgnore
    public void error(String str) {
        appendLog(TAG_ERROR, str);
    }

    @DexIgnore
    public String getAppVersion() {
        return this.appVersion;
    }

    @DexIgnore
    public int getButtonCount() {
        return this.candidates.size();
    }

    @DexIgnore
    public String getDetailLog() {
        return this.detailLog;
    }

    @DexIgnore
    public long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public int getEndTimeEpoch() {
        return (int) (this.endTime / 1000);
    }

    @DexIgnore
    public List<Integer> getErrorCodes() {
        List<Integer> list = this.errorCodes;
        return list == null ? new ArrayList() : list;
    }

    @DexIgnore
    public String getFirmwareVersion() {
        return this.firmwareVersion;
    }

    @DexIgnore
    public String getLogId() {
        return this.uuid;
    }

    @DexIgnore
    public int getLogType() {
        return 0;
    }

    @DexIgnore
    public String getOsVersion() {
        return this.osVersion;
    }

    @DexIgnore
    public String getPhoneModel() {
        return this.phoneModel;
    }

    @DexIgnore
    public int getResultCode() {
        return this.resultCode;
    }

    @DexIgnore
    public String getSdkVersion() {
        return this.sdkVersion;
    }

    @DexIgnore
    public String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public int getStartTimeEpoch() {
        return (int) (this.startTime / 1000);
    }

    @DexIgnore
    public void info(String str) {
        appendLog(TAG_INFO, str);
    }

    @DexIgnore
    public boolean isHidden() {
        return this.isHidden;
    }

    @DexIgnore
    public void log(String str) {
        appendLog("", str);
    }

    @DexIgnore
    public void setAppVersion(String str) {
        this.appVersion = str;
    }

    @DexIgnore
    public void setDetailLog(String str) {
        this.detailLog = str;
    }

    @DexIgnore
    public void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public void setErrorCodes(List<Integer> list) {
        this.errorCodes = list;
    }

    @DexIgnore
    public void setFirmwareVersion(String str) {
        this.firmwareVersion = str;
    }

    @DexIgnore
    public void setHidden(boolean z) {
        this.isHidden = z;
    }

    @DexIgnore
    public void setLogId(String str) {
        this.uuid = str;
    }

    @DexIgnore
    public void setOsVersion(String str) {
        this.osVersion = str;
    }

    @DexIgnore
    public void setPhoneModel(String str) {
        this.phoneModel = str;
    }

    @DexIgnore
    public void setResultCode(int i) {
        this.resultCode = i;
    }

    @DexIgnore
    public void setSdkVersion(String str) {
        this.sdkVersion = str;
    }

    @DexIgnore
    public void setSerial(String str) {
        this.serial = str;
    }

    @DexIgnore
    public void setStartTime(long j) {
        this.startTime = j;
    }
}
