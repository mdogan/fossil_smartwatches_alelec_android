package com.misfit.frameworks.buttonservice.log.db;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.cg;
import com.fossil.blesdk.obfuscated.fg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.ig;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.qf;
import com.fossil.blesdk.obfuscated.uf;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogDatabase_Impl extends LogDatabase {
    @DexIgnore
    public volatile LogDao _logDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends uf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(hg hgVar) {
            hgVar.b("CREATE TABLE IF NOT EXISTS `log` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timeStamp` INTEGER NOT NULL, `content` TEXT NOT NULL, `cloudFlag` TEXT NOT NULL)");
            hgVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            hgVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '1e76ae5948872f78452bd16b18a13dc4')");
        }

        @DexIgnore
        public void dropAllTables(hg hgVar) {
            hgVar.b("DROP TABLE IF EXISTS `log`");
        }

        @DexIgnore
        public void onCreate(hg hgVar) {
            if (LogDatabase_Impl.this.mCallbacks != null) {
                int size = LogDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) LogDatabase_Impl.this.mCallbacks.get(i)).a(hgVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(hg hgVar) {
            hg unused = LogDatabase_Impl.this.mDatabase = hgVar;
            LogDatabase_Impl.this.internalInitInvalidationTracker(hgVar);
            if (LogDatabase_Impl.this.mCallbacks != null) {
                int size = LogDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) LogDatabase_Impl.this.mCallbacks.get(i)).b(hgVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(hg hgVar) {
        }

        @DexIgnore
        public void onPreMigrate(hg hgVar) {
            cg.a(hgVar);
        }

        @DexIgnore
        public void validateMigration(hg hgVar) {
            HashMap hashMap = new HashMap(4);
            hashMap.put("id", new fg.a("id", "INTEGER", true, 1));
            hashMap.put("timeStamp", new fg.a("timeStamp", "INTEGER", true, 0));
            hashMap.put("content", new fg.a("content", "TEXT", true, 0));
            hashMap.put("cloudFlag", new fg.a("cloudFlag", "TEXT", true, 0));
            fg fgVar = new fg("log", hashMap, new HashSet(0), new HashSet(0));
            fg a = fg.a(hgVar, "log");
            if (!fgVar.equals(a)) {
                throw new IllegalStateException("Migration didn't properly handle log(com.misfit.frameworks.buttonservice.log.db.Log).\n Expected:\n" + fgVar + "\n Found:\n" + a);
            }
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        hg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `log`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public qf createInvalidationTracker() {
        return new qf(this, new HashMap(0), new HashMap(0), "log");
    }

    @DexIgnore
    public ig createOpenHelper(kf kfVar) {
        uf ufVar = new uf(kfVar, new Anon1(1), "1e76ae5948872f78452bd16b18a13dc4", "3ef7199be598e6814da70cf3eed15c90");
        ig.b.a a = ig.b.a(kfVar.b);
        a.a(kfVar.c);
        a.a((ig.a) ufVar);
        return kfVar.a.a(a.a());
    }

    @DexIgnore
    public LogDao getLogDao() {
        LogDao logDao;
        if (this._logDao != null) {
            return this._logDao;
        }
        synchronized (this) {
            if (this._logDao == null) {
                this._logDao = new LogDao_Impl(this);
            }
            logDao = this._logDao;
        }
        return logDao;
    }
}
