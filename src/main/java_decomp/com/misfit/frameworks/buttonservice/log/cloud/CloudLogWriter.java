package com.misfit.frameworks.buttonservice.log.cloud;

import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.yz1;
import com.fossil.blesdk.obfuscated.za4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogUtils;
import com.misfit.frameworks.buttonservice.log.IRemoteLogWriter;
import com.misfit.frameworks.buttonservice.log.LogApiService;
import com.misfit.frameworks.buttonservice.log.LogEndPointKt;
import com.misfit.frameworks.buttonservice.log.LogEvent;
import com.misfit.frameworks.buttonservice.log.RepoResponse;
import com.misfit.frameworks.buttonservice.log.Success;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CloudLogWriter implements IRemoteLogWriter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String ITEMS_PARAM; // = "_items";
    @DexIgnore
    public static /* final */ int REQUEST_SIZE; // = 500;
    @DexIgnore
    public /* final */ LogApiService mApi;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public CloudLogWriter(LogApiService logApiService) {
        wd4.b(logApiService, "mApi");
        this.mApi = logApiService;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00e1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    public Object sendLog(List<LogEvent> list, kc4<? super List<LogEvent>> kc4) {
        CloudLogWriter cloudLogWriter;
        CloudLogWriter$sendLog$Anon1 cloudLogWriter$sendLog$Anon1;
        int i;
        CloudLogWriter cloudLogWriter2;
        Iterator<T> it;
        Gson gson;
        List<List<T>> list2;
        List list3;
        Iterable iterable;
        Object obj;
        List list4;
        List<LogEvent> list5;
        Object obj2;
        Iterator<T> it2;
        CloudLogWriter cloudLogWriter3;
        kc4<? super List<LogEvent>> kc42 = kc4;
        if (kc42 instanceof CloudLogWriter$sendLog$Anon1) {
            cloudLogWriter$sendLog$Anon1 = (CloudLogWriter$sendLog$Anon1) kc42;
            int i2 = cloudLogWriter$sendLog$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                cloudLogWriter$sendLog$Anon1.label = i2 - Integer.MIN_VALUE;
                cloudLogWriter = this;
                Object obj3 = cloudLogWriter$sendLog$Anon1.result;
                Object a = oc4.a();
                i = cloudLogWriter$sendLog$Anon1.label;
                if (i != 0) {
                    za4.a(obj3);
                    Gson gsonForLogEvent = FLogUtils.INSTANCE.getGsonForLogEvent();
                    List<LogEvent> list6 = list;
                    List<List<T>> b = wb4.b(list6, 500);
                    ArrayList arrayList = new ArrayList();
                    Iterator<T> it3 = b.iterator();
                    gson = gsonForLogEvent;
                    cloudLogWriter3 = cloudLogWriter;
                    list2 = b;
                    obj2 = a;
                    list5 = list6;
                    it2 = it3;
                    list3 = arrayList;
                    iterable = list2;
                } else if (i == 1) {
                    yz1 yz1 = (yz1) cloudLogWriter$sendLog$Anon1.L$Anon10;
                    JsonElement jsonElement = (JsonElement) cloudLogWriter$sendLog$Anon1.L$Anon9;
                    list4 = (List) cloudLogWriter$sendLog$Anon1.L$Anon8;
                    Object obj4 = cloudLogWriter$sendLog$Anon1.L$Anon7;
                    iterable = (Iterable) cloudLogWriter$sendLog$Anon1.L$Anon5;
                    list3 = (List) cloudLogWriter$sendLog$Anon1.L$Anon4;
                    list2 = (List) cloudLogWriter$sendLog$Anon1.L$Anon3;
                    gson = (Gson) cloudLogWriter$sendLog$Anon1.L$Anon2;
                    cloudLogWriter2 = (CloudLogWriter) cloudLogWriter$sendLog$Anon1.L$Anon0;
                    za4.a(obj3);
                    Iterator<T> it4 = (Iterator) cloudLogWriter$sendLog$Anon1.L$Anon6;
                    obj = a;
                    list5 = (List) cloudLogWriter$sendLog$Anon1.L$Anon1;
                    it = it4;
                    if (((RepoResponse) obj3) instanceof Success) {
                        list3.addAll(list4);
                    }
                    obj2 = obj;
                    it2 = it;
                    cloudLogWriter3 = cloudLogWriter2;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (it2.hasNext()) {
                    T next = it2.next();
                    List list7 = (List) next;
                    JsonElement b2 = gson.b(list7, new CloudLogWriter$sendLog$Anon2$logEventArrays$Anon1().getType());
                    yz1 yz12 = new yz1();
                    yz12.a(ITEMS_PARAM, b2);
                    Call<yz1> sendLogs = cloudLogWriter3.mApi.sendLogs(yz12);
                    cloudLogWriter$sendLog$Anon1.L$Anon0 = cloudLogWriter3;
                    cloudLogWriter$sendLog$Anon1.L$Anon1 = list5;
                    cloudLogWriter$sendLog$Anon1.L$Anon2 = gson;
                    cloudLogWriter$sendLog$Anon1.L$Anon3 = list2;
                    cloudLogWriter$sendLog$Anon1.L$Anon4 = list3;
                    cloudLogWriter$sendLog$Anon1.L$Anon5 = iterable;
                    cloudLogWriter$sendLog$Anon1.L$Anon6 = it2;
                    cloudLogWriter$sendLog$Anon1.L$Anon7 = next;
                    cloudLogWriter$sendLog$Anon1.L$Anon8 = list7;
                    cloudLogWriter$sendLog$Anon1.L$Anon9 = b2;
                    cloudLogWriter$sendLog$Anon1.L$Anon10 = yz12;
                    cloudLogWriter$sendLog$Anon1.label = 1;
                    Object await = LogEndPointKt.await(sendLogs, cloudLogWriter$sendLog$Anon1);
                    if (await == obj2) {
                        return obj2;
                    }
                    List list8 = list7;
                    cloudLogWriter2 = cloudLogWriter3;
                    obj3 = await;
                    it = it2;
                    obj = obj2;
                    list4 = list8;
                    if (((RepoResponse) obj3) instanceof Success) {
                    }
                    obj2 = obj;
                    it2 = it;
                    cloudLogWriter3 = cloudLogWriter2;
                    if (it2.hasNext()) {
                        return list3;
                    }
                    return obj2;
                }
                return list3;
            }
        }
        cloudLogWriter = this;
        cloudLogWriter$sendLog$Anon1 = new CloudLogWriter$sendLog$Anon1(cloudLogWriter, kc42);
        Object obj32 = cloudLogWriter$sendLog$Anon1.result;
        Object a2 = oc4.a();
        i = cloudLogWriter$sendLog$Anon1.label;
        if (i != 0) {
        }
        if (it2.hasNext()) {
        }
        return list3;
    }
}
