package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.db.Log;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DBLogWriter$writeLog$Anon1 extends Lambda implements jd4<LogEvent, Log> {
    @DexIgnore
    public static /* final */ DBLogWriter$writeLog$Anon1 INSTANCE; // = new DBLogWriter$writeLog$Anon1();

    @DexIgnore
    public DBLogWriter$writeLog$Anon1() {
        super(1);
    }

    @DexIgnore
    public final Log invoke(LogEvent logEvent) {
        wd4.b(logEvent, "it");
        return new Log(logEvent.getTimestamp(), logEvent.toString(), Log.Flag.ADD);
    }
}
