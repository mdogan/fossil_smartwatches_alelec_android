package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.kc4;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface IRemoteLogWriter {
    @DexIgnore
    Object sendLog(List<LogEvent> list, kc4<? super List<LogEvent>> kc4);
}
