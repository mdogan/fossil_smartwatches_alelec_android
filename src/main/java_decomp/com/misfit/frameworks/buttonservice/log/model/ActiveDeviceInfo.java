package com.misfit.frameworks.buttonservice.log.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.wearables.fsl.location.DeviceLocation;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActiveDeviceInfo implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((rd4) null);
    @DexIgnore
    public /* final */ String deviceModel;
    @DexIgnore
    public String deviceSerial;
    @DexIgnore
    public String fwVersion;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ActiveDeviceInfo> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(rd4 rd4) {
            this();
        }

        @DexIgnore
        public ActiveDeviceInfo createFromParcel(Parcel parcel) {
            wd4.b(parcel, "parcel");
            return new ActiveDeviceInfo(parcel);
        }

        @DexIgnore
        public ActiveDeviceInfo[] newArray(int i) {
            return new ActiveDeviceInfo[i];
        }
    }

    @DexIgnore
    public ActiveDeviceInfo(String str, String str2, String str3) {
        wd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        wd4.b(str2, "deviceModel");
        wd4.b(str3, "fwVersion");
        this.deviceSerial = str;
        this.deviceModel = str2;
        this.fwVersion = str3;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getDeviceModel() {
        return this.deviceModel;
    }

    @DexIgnore
    public final String getDeviceSerial() {
        return this.deviceSerial;
    }

    @DexIgnore
    public final String getFwVersion() {
        return this.fwVersion;
    }

    @DexIgnore
    public final void setDeviceSerial(String str) {
        wd4.b(str, "<set-?>");
        this.deviceSerial = str;
    }

    @DexIgnore
    public final void setFwVersion(String str) {
        wd4.b(str, "<set-?>");
        this.fwVersion = str;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wd4.b(parcel, "parcel");
        parcel.writeString(this.deviceSerial);
        parcel.writeString(this.deviceModel);
        parcel.writeString(this.fwVersion);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ActiveDeviceInfo(Parcel parcel) {
        this(r0, r2, r4 == null ? "" : r4);
        wd4.b(parcel, "parcel");
        String readString = parcel.readString();
        readString = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        readString2 = readString2 == null ? "" : readString2;
        String readString3 = parcel.readString();
    }
}
