package com.misfit.frameworks.buttonservice.log;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$flush$Anon1", f = "RemoteFLogger.kt", l = {}, m = "invokeSuspend")
public final class RemoteFLogger$flush$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$flush$Anon1(RemoteFLogger remoteFLogger, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = remoteFLogger;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        RemoteFLogger$flush$Anon1 remoteFLogger$flush$Anon1 = new RemoteFLogger$flush$Anon1(this.this$Anon0, kc4);
        remoteFLogger$flush$Anon1.p$ = (lh4) obj;
        return remoteFLogger$flush$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((RemoteFLogger$flush$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            if (this.this$Anon0.isMainFLogger && !this.this$Anon0.isFlushing) {
                this.this$Anon0.isFlushing = true;
                RemoteFLogger remoteFLogger = this.this$Anon0;
                remoteFLogger.sendInternalMessage(RemoteFLogger.MESSAGE_ACTION_FLUSH, remoteFLogger.floggerName, RemoteFLogger.MessageTarget.TO_ALL_EXCEPT_MAIN_FLOGGER, new Bundle());
                this.this$Anon0.isFlushing = false;
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
