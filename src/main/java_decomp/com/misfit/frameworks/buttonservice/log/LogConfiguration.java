package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogConfiguration {
    @DexIgnore
    public static /* final */ int DEBUG_BUFFER_DEFAULT_THRESHOLD; // = 10;
    @DexIgnore
    public static /* final */ int DEBUG_DB_THRESHOLD_DEFAULT; // = 50;
    @DexIgnore
    public static /* final */ int DEBUG_FLUSH_LOG_TIME_THRESHOLD; // = 1800000;
    @DexIgnore
    public static /* final */ LogConfiguration INSTANCE; // = new LogConfiguration();
    @DexIgnore
    public static /* final */ int RELEASE_BUFFER_DEFAULT_THRESHOLD; // = 100;
    @DexIgnore
    public static /* final */ int RELEASE_DB_THRESHOLD_DEFAULT; // = 500;
    @DexIgnore
    public static /* final */ int RELEASE_FLUSH_LOG_TIME_THRESHOLD; // = 10800000;
}
