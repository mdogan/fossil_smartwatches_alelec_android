package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.wd4;
import java.io.File;
import java.io.FileFilter;
import kotlin.text.Regex;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BufferLogWriter$renameToFullBufferFile$Anon1$Anon1$founds$Anon1 implements FileFilter {
    @DexIgnore
    public static /* final */ BufferLogWriter$renameToFullBufferFile$Anon1$Anon1$founds$Anon1 INSTANCE; // = new BufferLogWriter$renameToFullBufferFile$Anon1$Anon1$founds$Anon1();

    @DexIgnore
    public final boolean accept(File file) {
        wd4.a((Object) file, "subFile");
        if (file.isFile()) {
            String name = file.getName();
            wd4.a((Object) name, "subFile.name");
            if (new Regex(BufferLogWriter.FULL_BUFFER_FILE_NAME_REGEX_PATTERN).matches(name)) {
                return true;
            }
        }
        return false;
    }
}
