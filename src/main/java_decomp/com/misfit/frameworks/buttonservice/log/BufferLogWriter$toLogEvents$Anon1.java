package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BufferLogWriter$toLogEvents$Anon1 extends Lambda implements jd4<String, LogEvent> {
    @DexIgnore
    public /* final */ /* synthetic */ Gson $gson;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$toLogEvents$Anon1(Gson gson) {
        super(1);
        this.$gson = gson;
    }

    @DexIgnore
    public final LogEvent invoke(String str) {
        wd4.b(str, "it");
        try {
            return (LogEvent) this.$gson.a(str, LogEvent.class);
        } catch (Exception unused) {
            return null;
        }
    }
}
