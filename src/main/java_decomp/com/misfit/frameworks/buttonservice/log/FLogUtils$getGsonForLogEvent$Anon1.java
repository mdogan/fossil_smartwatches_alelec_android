package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.a02;
import com.fossil.blesdk.obfuscated.b02;
import com.fossil.blesdk.obfuscated.c02;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FLogUtils$getGsonForLogEvent$Anon1<T> implements c02<Long> {
    @DexIgnore
    public static /* final */ FLogUtils$getGsonForLogEvent$Anon1 INSTANCE; // = new FLogUtils$getGsonForLogEvent$Anon1();

    @DexIgnore
    public final a02 serialize(Long l, Type type, b02 b02) {
        return new a02((Number) new BigDecimal(((double) l.longValue()) / ((double) 1000)).setScale(6, RoundingMode.HALF_UP));
    }
}
