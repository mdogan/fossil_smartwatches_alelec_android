package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.cs4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.wd4;
import kotlin.Result;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogEndPointKt$await$Anon2$Anon1 implements qr4<T> {
    @DexIgnore
    public /* final */ /* synthetic */ kc4 $continuation;

    @DexIgnore
    public LogEndPointKt$await$Anon2$Anon1(kc4 kc4) {
        this.$continuation = kc4;
    }

    @DexIgnore
    public void onFailure(Call<T> call, Throwable th) {
        wd4.b(th, "t");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("await", "onFailure=" + th);
        kc4 kc4 = this.$continuation;
        Failure create = RepoResponse.Companion.create(th);
        Result.a aVar = Result.Companion;
        kc4.resumeWith(Result.m3constructorimpl(create));
    }

    @DexIgnore
    public void onResponse(Call<T> call, cs4<T> cs4) {
        wd4.b(cs4, "response");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("await", "onResponse=" + cs4);
        kc4 kc4 = this.$continuation;
        RepoResponse<T> create = RepoResponse.Companion.create(cs4);
        Result.a aVar = Result.Companion;
        kc4.resumeWith(Result.m3constructorimpl(create));
    }
}
