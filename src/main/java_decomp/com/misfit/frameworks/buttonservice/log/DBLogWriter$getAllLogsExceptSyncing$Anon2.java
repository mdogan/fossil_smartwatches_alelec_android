package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.db.Log;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Lambda;
import kotlin.sequences.SequencesKt___SequencesKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$getAllLogsExceptSyncing$Anon2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$getAllLogsExceptSyncing$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super List<LogEvent>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Lambda implements jd4<Log, LogEvent> {
        @DexIgnore
        public /* final */ /* synthetic */ Gson $gson;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(Gson gson) {
            super(1);
            this.$gson = gson;
        }

        @DexIgnore
        public final LogEvent invoke(Log log) {
            wd4.b(log, "it");
            try {
                LogEvent logEvent = (LogEvent) this.$gson.a(log.getContent(), LogEvent.class);
                logEvent.setTag(Integer.valueOf(log.getId()));
                return logEvent;
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = DBLogWriter.TAG;
                local.e(access$getTAG$cp, ", getAllLogs(), ex: " + e.getMessage());
                return null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$getAllLogsExceptSyncing$Anon2(DBLogWriter dBLogWriter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = dBLogWriter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DBLogWriter$getAllLogsExceptSyncing$Anon2 dBLogWriter$getAllLogsExceptSyncing$Anon2 = new DBLogWriter$getAllLogsExceptSyncing$Anon2(this.this$Anon0, kc4);
        dBLogWriter$getAllLogsExceptSyncing$Anon2.p$ = (lh4) obj;
        return dBLogWriter$getAllLogsExceptSyncing$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DBLogWriter$getAllLogsExceptSyncing$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            return SequencesKt___SequencesKt.g(SequencesKt___SequencesKt.d(wb4.b(this.this$Anon0.logDao.getAllLogEventsExcept(Log.Flag.SYNCING)), new Anon1(new Gson())));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
