package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.bt4;
import com.fossil.blesdk.obfuscated.nt4;
import com.fossil.blesdk.obfuscated.yz1;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface LogApiService {
    @DexIgnore
    @nt4("app_log/event")
    Call<yz1> sendLogs(@bt4 yz1 yz1);
}
