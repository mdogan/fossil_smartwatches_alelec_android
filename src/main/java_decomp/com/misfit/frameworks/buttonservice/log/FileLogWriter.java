package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.nf4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.extensions.SynchronizeQueue;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FileLogWriter extends Thread {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ long FILE_LOG_SIZE_THRESHOLD; // = 3145728;
    @DexIgnore
    public static /* final */ String FILE_NAME_PATTERN; // = "app_log_%s.txt";
    @DexIgnore
    public static /* final */ String LOG_FOLDER; // = "logs";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public FileDebugOption debugOption;
    @DexIgnore
    public String directoryPath; // = "";
    @DexIgnore
    public volatile boolean isRunning;
    @DexIgnore
    public /* final */ SynchronizeQueue<String> logEventQueue; // = new SynchronizeQueue<>();
    @DexIgnore
    public int mCount;
    @DexIgnore
    public /* final */ Object mLocker; // = new Object();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String name = FileLogWriter.class.getName();
        wd4.a((Object) name, "FileLogWriter::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    private final String getFilePath(String str) {
        return this.directoryPath + File.separatorChar + LOG_FOLDER + File.separatorChar + str;
    }

    @DexIgnore
    private final synchronized void rotateFiles() throws Exception {
        for (int i = 2; i >= 0; i--) {
            be4 be4 = be4.a;
            Object[] objArr = {Integer.valueOf(i)};
            String format = String.format(FILE_NAME_PATTERN, Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            File file = new File(getFilePath(format));
            if (file.exists()) {
                FileChannel channel = new RandomAccessFile(file, "rw").getChannel();
                FileLock lock = channel.lock();
                if (i >= 2) {
                    file.delete();
                } else {
                    be4 be42 = be4.a;
                    Object[] objArr2 = {Integer.valueOf(i + 1)};
                    String format2 = String.format(FILE_NAME_PATTERN, Arrays.copyOf(objArr2, objArr2.length));
                    wd4.a((Object) format2, "java.lang.String.format(format, *args)");
                    file.renameTo(new File(getFilePath(format2)));
                }
                lock.release();
                channel.close();
            }
        }
        be4 be43 = be4.a;
        Object[] objArr3 = {0};
        String format3 = String.format(FILE_NAME_PATTERN, Arrays.copyOf(objArr3, objArr3.length));
        wd4.a((Object) format3, "java.lang.String.format(format, *args)");
        new File(getFilePath(format3)).createNewFile();
    }

    @DexIgnore
    public final List<File> exportLogs() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i <= 2; i++) {
            be4 be4 = be4.a;
            Object[] objArr = {Integer.valueOf(i)};
            String format = String.format(FILE_NAME_PATTERN, Arrays.copyOf(objArr, objArr.length));
            wd4.a((Object) format, "java.lang.String.format(format, *args)");
            File file = new File(getFilePath(format));
            if (file.exists()) {
                arrayList.add(file);
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b8, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00b9, code lost:
        java.lang.System.out.print(r0.toString());
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    public void run() {
        if (!cg4.a(this.directoryPath)) {
            while (this.isRunning) {
                be4 be4 = be4.a;
                Object[] objArr = {0};
                String format = String.format(FILE_NAME_PATTERN, Arrays.copyOf(objArr, objArr.length));
                wd4.a((Object) format, "java.lang.String.format(format, *args)");
                File file = new File(getFilePath(format));
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
                if (!file.exists()) {
                    file.createNewFile();
                } else if (file.length() >= FILE_LOG_SIZE_THRESHOLD) {
                    rotateFiles();
                }
                while (this.isRunning) {
                    String poll = this.logEventQueue.poll();
                    if (poll != null) {
                        FileOutputStream fileOutputStream = new FileOutputStream(file, true);
                        byte[] bytes = poll.getBytes(nf4.a);
                        wd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                        fileOutputStream.write(bytes);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        if (this.debugOption != null) {
                            this.mCount++;
                        }
                        if (file.length() >= FILE_LOG_SIZE_THRESHOLD) {
                            rotateFiles();
                        }
                        FileDebugOption fileDebugOption = this.debugOption;
                        if (fileDebugOption != null && this.mCount == fileDebugOption.getMaximumLog()) {
                            fileDebugOption.getCallback().onFinish();
                        }
                    } else {
                        synchronized (this.mLocker) {
                            this.mLocker.wait();
                            cb4 cb4 = cb4.a;
                        }
                    }
                }
                continue;
            }
        }
    }

    @DexIgnore
    public final void startWriter(String str) {
        wd4.b(str, "directoryPath");
        if (!this.isRunning) {
            this.directoryPath = str;
            this.isRunning = true;
            start();
        }
    }

    @DexIgnore
    public final void stopWriter() {
        this.isRunning = false;
    }

    @DexIgnore
    public final synchronized void writeLog(String str) {
        wd4.b(str, "logMessage");
        this.logEventQueue.add(str);
        synchronized (this.mLocker) {
            this.mLocker.notifyAll();
            cb4 cb4 = cb4.a;
        }
    }

    @DexIgnore
    public final void startWriter(String str, FileDebugOption fileDebugOption) {
        wd4.b(str, "directoryPath");
        this.debugOption = fileDebugOption;
        this.mCount = 0;
        startWriter(str);
    }
}
