package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.be4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.lb4;
import com.fossil.blesdk.obfuscated.lv1;
import com.fossil.blesdk.obfuscated.mg4;
import com.fossil.blesdk.obfuscated.mh4;
import com.fossil.blesdk.obfuscated.nf4;
import com.fossil.blesdk.obfuscated.pl4;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.ri4;
import com.fossil.blesdk.obfuscated.rl4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.zh4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.extensions.SynchronizeQueue;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BufferLogWriter extends Thread {
    @DexIgnore
    public static /* final */ String ALL_BUFFER_FILE_NAME_REGEX_PATTERN; // = "\\w*buffer_log\\w*\\.txt";
    @DexIgnore
    public static /* final */ String BUFFER_FILE_NAME; // = "%s_buffer_log.txt";
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ String FULL_BUFFER_FILE_NAME_PATTERN; // = "%s_buffer_log_%s.txt";
    @DexIgnore
    public static /* final */ String FULL_BUFFER_FILE_NAME_REGEX_PATTERN; // = "\\w*buffer_log_\\d+\\.txt";
    @DexIgnore
    public static /* final */ String LOG_FOLDER; // = "buffer_logs";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public IBufferLogCallback callback;
    @DexIgnore
    public BufferDebugOption debugOption;
    @DexIgnore
    public String directoryPath; // = "";
    @DexIgnore
    public /* final */ String floggerName;
    @DexIgnore
    public volatile boolean isMainFLogger;
    @DexIgnore
    public volatile boolean isRunning;
    @DexIgnore
    public /* final */ SynchronizeQueue<LogEvent> logEventQueue; // = new SynchronizeQueue<>();
    @DexIgnore
    public String logFilePath; // = "";
    @DexIgnore
    public /* final */ Object mLocker; // = new Object();
    @DexIgnore
    public /* final */ pl4 mMutex; // = rl4.a(false, 1, (Object) null);
    @DexIgnore
    public /* final */ int threshold;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    @DexIgnore
    public interface IBufferLogCallback {
        @DexIgnore
        void onFullBuffer(List<LogEvent> list, boolean z);

        @DexIgnore
        void onWrittenSummaryLog(LogEvent logEvent);
    }

    /*
    static {
        String name = BufferLogWriter.class.getName();
        wd4.a((Object) name, "BufferLogWriter::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    public BufferLogWriter(String str, int i) {
        wd4.b(str, "floggerName");
        this.floggerName = str;
        this.threshold = i;
    }

    @DexIgnore
    private final String getFullBufferLogFileName(long j) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.directoryPath);
        sb.append(File.separatorChar);
        be4 be4 = be4.a;
        Object[] objArr = {this.floggerName, Long.valueOf(j), Locale.US};
        String format = String.format(FULL_BUFFER_FILE_NAME_PATTERN, Arrays.copyOf(objArr, objArr.length));
        wd4.a((Object) format, "java.lang.String.format(format, *args)");
        sb.append(format);
        return sb.toString();
    }

    @DexIgnore
    private final void renameToFullBufferFile(File file, boolean z) {
        Calendar instance = Calendar.getInstance();
        wd4.a((Object) instance, "Calendar.getInstance()");
        file.renameTo(new File(getFullBufferLogFileName(instance.getTimeInMillis())));
        ri4 unused = mg4.b(mh4.a(zh4.b()), (CoroutineContext) null, (CoroutineStart) null, new BufferLogWriter$renameToFullBufferFile$Anon1(this, z, (kc4) null), 3, (Object) null);
    }

    @DexIgnore
    private final List<LogEvent> toLogEvents(List<String> list) {
        return SequencesKt___SequencesKt.g(SequencesKt___SequencesKt.d(wb4.b(list), new BufferLogWriter$toLogEvents$Anon1(new Gson())));
    }

    @DexIgnore
    public final List<File> exportLogs() {
        File[] listFiles = new File(this.directoryPath).listFiles(BufferLogWriter$exportLogs$Anon1.INSTANCE);
        wd4.a((Object) listFiles, "parentFolder.listFiles {\u2026EGEX_PATTERN.toRegex()) }");
        return lb4.f(listFiles);
    }

    @DexIgnore
    public final synchronized void forceFlushBuffer() {
        writeLog(new FlushLogEvent());
    }

    @DexIgnore
    public final IBufferLogCallback getCallback() {
        return this.callback;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00e8, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00e9, code lost:
        java.lang.System.out.print(r1.toString());
        java.lang.Thread.currentThread().interrupt();
        r13.isRunning = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return;
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0049 A[Catch:{ Exception -> 0x00e8 }] */
    public void run() {
        int i;
        if (!cg4.a(this.logFilePath)) {
            File file = new File(this.logFilePath);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            if (!file.exists()) {
                file.createNewFile();
            } else {
                List<String> b = lv1.b(file, Charset.defaultCharset());
                wd4.a((Object) b, "logLines");
                i = b.size();
                if (i >= this.threshold) {
                    renameToFullBufferFile(file, false);
                }
                while (this.isRunning) {
                    LogEvent poll = this.logEventQueue.poll();
                    boolean z = poll instanceof FlushLogEvent;
                    if (poll != null && !(poll instanceof FinishLogEvent) && !z) {
                        FileOutputStream fileOutputStream = new FileOutputStream(file, true);
                        String str = poll.toString() + "\n";
                        Charset charset = nf4.a;
                        if (str != null) {
                            byte[] bytes = str.getBytes(charset);
                            wd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                            fileOutputStream.write(bytes);
                            fileOutputStream.flush();
                            fileOutputStream.close();
                            i++;
                            if (poll.getLogLevel() == FLogger.LogLevel.SUMMARY) {
                                ri4 unused = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new BufferLogWriter$run$Anon1(this, poll, (kc4) null), 3, (Object) null);
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    if (i >= this.threshold || z) {
                        renameToFullBufferFile(file, z);
                        i = 0;
                    }
                    if (poll instanceof FinishLogEvent) {
                        ri4 unused2 = mg4.b(mh4.a(zh4.a()), (CoroutineContext) null, (CoroutineStart) null, new BufferLogWriter$run$Anon2(this, (kc4) null), 3, (Object) null);
                    }
                    if (poll == null) {
                        synchronized (this.mLocker) {
                            this.mLocker.wait();
                            cb4 cb4 = cb4.a;
                        }
                    }
                }
            }
            i = 0;
            while (this.isRunning) {
            }
        }
    }

    @DexIgnore
    public final void setCallback(IBufferLogCallback iBufferLogCallback) {
        this.callback = iBufferLogCallback;
    }

    @DexIgnore
    public final void startWriter(String str, boolean z, IBufferLogCallback iBufferLogCallback) {
        String str2;
        wd4.b(str, "directoryPath");
        wd4.b(iBufferLogCallback, Constants.CALLBACK);
        this.callback = iBufferLogCallback;
        if (!this.isRunning) {
            this.directoryPath = str + File.separatorChar + LOG_FOLDER;
            if (!cg4.a(this.directoryPath)) {
                StringBuilder sb = new StringBuilder();
                sb.append(this.directoryPath);
                sb.append(File.separatorChar);
                be4 be4 = be4.a;
                Object[] objArr = {this.floggerName, Locale.US};
                String format = String.format(BUFFER_FILE_NAME, Arrays.copyOf(objArr, objArr.length));
                wd4.a((Object) format, "java.lang.String.format(format, *args)");
                sb.append(format);
                str2 = sb.toString();
            } else {
                str2 = "";
            }
            this.logFilePath = str2;
            this.isMainFLogger = z;
            this.isRunning = true;
            start();
        }
    }

    @DexIgnore
    public final void stopWriter() {
        this.isRunning = false;
    }

    @DexIgnore
    public final synchronized void writeLog(LogEvent logEvent) {
        wd4.b(logEvent, "logEvent");
        this.logEventQueue.add(logEvent);
        synchronized (this.mLocker) {
            this.mLocker.notifyAll();
            cb4 cb4 = cb4.a;
        }
    }

    @DexIgnore
    public final void startWriter(String str, boolean z, IBufferLogCallback iBufferLogCallback, BufferDebugOption bufferDebugOption) {
        wd4.b(str, "directoryPath");
        wd4.b(iBufferLogCallback, Constants.CALLBACK);
        this.debugOption = bufferDebugOption;
        startWriter(str, z, iBufferLogCallback);
    }
}
