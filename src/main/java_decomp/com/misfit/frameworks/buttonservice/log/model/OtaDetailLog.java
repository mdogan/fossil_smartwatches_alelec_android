package com.misfit.frameworks.buttonservice.log.model;

import com.fossil.blesdk.obfuscated.g02;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class OtaDetailLog {
    @DexIgnore
    @g02("battery_levels")
    public int batteryLevel;

    @DexIgnore
    public OtaDetailLog() {
        this(0, 1, (rd4) null);
    }

    @DexIgnore
    public OtaDetailLog(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public final int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public final void setBatteryLevel(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        wd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ OtaDetailLog(int i, int i2, rd4 rd4) {
        this((i2 & 1) != 0 ? 0 : i);
    }
}
