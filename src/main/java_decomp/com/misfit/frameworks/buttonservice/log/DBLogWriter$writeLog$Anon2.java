package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.DBLogWriter;
import com.misfit.frameworks.buttonservice.log.db.Log;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$writeLog$Anon2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$writeLog$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$writeLog$Anon2(DBLogWriter dBLogWriter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = dBLogWriter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        DBLogWriter$writeLog$Anon2 dBLogWriter$writeLog$Anon2 = new DBLogWriter$writeLog$Anon2(this.this$Anon0, kc4);
        dBLogWriter$writeLog$Anon2.p$ = (lh4) obj;
        return dBLogWriter$writeLog$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DBLogWriter$writeLog$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            int countExcept = this.this$Anon0.logDao.countExcept(Log.Flag.SYNCING);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = DBLogWriter.TAG;
            local.d(access$getTAG$cp, ".writeLog(), dbCount=" + countExcept);
            if (countExcept >= this.this$Anon0.thresholdValue) {
                DBLogWriter.IDBLogWriterCallback access$getCallback$p = this.this$Anon0.callback;
                if (access$getCallback$p != null) {
                    access$getCallback$p.onReachDBThreshold();
                }
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
