package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Success<T> extends RepoResponse<T> {
    @DexIgnore
    public /* final */ T response;

    @DexIgnore
    public Success(T t) {
        super((rd4) null);
        this.response = t;
    }

    @DexIgnore
    public static /* synthetic */ Success copy$default(Success success, T t, int i, Object obj) {
        if ((i & 1) != 0) {
            t = success.response;
        }
        return success.copy(t);
    }

    @DexIgnore
    public final T component1() {
        return this.response;
    }

    @DexIgnore
    public final Success<T> copy(T t) {
        return new Success<>(t);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof Success) && wd4.a((Object) this.response, (Object) ((Success) obj).response);
        }
        return true;
    }

    @DexIgnore
    public final T getResponse() {
        return this.response;
    }

    @DexIgnore
    public int hashCode() {
        T t = this.response;
        if (t != null) {
            return t.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "Success(response=" + this.response + ")";
    }
}
