package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.sz1;
import com.fossil.blesdk.obfuscated.wd4;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FLogUtils {
    @DexIgnore
    public static /* final */ FLogUtils INSTANCE; // = new FLogUtils();

    @DexIgnore
    public final Gson getGsonForLogEvent() {
        sz1 sz1 = new sz1();
        sz1.a(Long.class, FLogUtils$getGsonForLogEvent$Anon1.INSTANCE);
        Gson a = sz1.a();
        wd4.a((Object) a, "GsonBuilder()\n          \u2026                .create()");
        return a;
    }
}
