package com.misfit.frameworks.buttonservice.log;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$onFullBuffer$Anon1", f = "RemoteFLogger.kt", l = {153}, m = "invokeSuspend")
public final class RemoteFLogger$onFullBuffer$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $forceFlush;
    @DexIgnore
    public /* final */ /* synthetic */ List $logLines;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$onFullBuffer$Anon1(RemoteFLogger remoteFLogger, List list, boolean z, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = remoteFLogger;
        this.$logLines = list;
        this.$forceFlush = z;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        RemoteFLogger$onFullBuffer$Anon1 remoteFLogger$onFullBuffer$Anon1 = new RemoteFLogger$onFullBuffer$Anon1(this.this$Anon0, this.$logLines, this.$forceFlush, kc4);
        remoteFLogger$onFullBuffer$Anon1.p$ = (lh4) obj;
        return remoteFLogger$onFullBuffer$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((RemoteFLogger$onFullBuffer$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            if (this.this$Anon0.isMainFLogger) {
                DBLogWriter access$getDbLogWriter$p = this.this$Anon0.dbLogWriter;
                if (access$getDbLogWriter$p != null) {
                    access$getDbLogWriter$p.writeLog(this.$logLines);
                }
                if (this.$forceFlush) {
                    RemoteFLogger remoteFLogger = this.this$Anon0;
                    this.L$Anon0 = lh4;
                    this.label = 1;
                    if (remoteFLogger.flushDB(this) == a) {
                        return a;
                    }
                }
            } else if (!this.$forceFlush) {
                FLogger.INSTANCE.getLocal().d(RemoteFLogger.TAG, ".onFullBuffer(), broadcast");
                RemoteFLogger remoteFLogger2 = this.this$Anon0;
                String access$getFloggerName$p = remoteFLogger2.floggerName;
                RemoteFLogger.MessageTarget messageTarget = RemoteFLogger.MessageTarget.TO_MAIN_FLOGGER;
                Bundle bundle = Bundle.EMPTY;
                wd4.a((Object) bundle, "Bundle.EMPTY");
                remoteFLogger2.sendInternalMessage(RemoteFLogger.MESSAGE_ACTION_FULL_BUFFER, access$getFloggerName$p, messageTarget, bundle);
            } else {
                RemoteFLogger remoteFLogger3 = this.this$Anon0;
                String access$getFloggerName$p2 = remoteFLogger3.floggerName;
                RemoteFLogger.MessageTarget messageTarget2 = RemoteFLogger.MessageTarget.TO_MAIN_FLOGGER;
                Bundle bundle2 = Bundle.EMPTY;
                wd4.a((Object) bundle2, "Bundle.EMPTY");
                remoteFLogger3.sendInternalMessage(RemoteFLogger.MESSAGE_ACTION_FLUSH, access$getFloggerName$p2, messageTarget2, bundle2);
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
