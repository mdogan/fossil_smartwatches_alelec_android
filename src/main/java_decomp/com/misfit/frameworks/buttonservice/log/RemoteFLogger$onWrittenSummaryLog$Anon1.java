package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$onWrittenSummaryLog$Anon1", f = "RemoteFLogger.kt", l = {172}, m = "invokeSuspend")
public final class RemoteFLogger$onWrittenSummaryLog$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$onWrittenSummaryLog$Anon1(RemoteFLogger remoteFLogger, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = remoteFLogger;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        RemoteFLogger$onWrittenSummaryLog$Anon1 remoteFLogger$onWrittenSummaryLog$Anon1 = new RemoteFLogger$onWrittenSummaryLog$Anon1(this.this$Anon0, kc4);
        remoteFLogger$onWrittenSummaryLog$Anon1.p$ = (lh4) obj;
        return remoteFLogger$onWrittenSummaryLog$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((RemoteFLogger$onWrittenSummaryLog$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = oc4.a();
        int i = this.label;
        if (i == 0) {
            za4.a(obj);
            lh4 lh4 = this.p$;
            RemoteFLogger remoteFLogger = this.this$Anon0;
            this.L$Anon0 = lh4;
            this.label = 1;
            if (remoteFLogger.flushBuffer(this) == a) {
                return a;
            }
        } else if (i == 1) {
            lh4 lh42 = (lh4) this.L$Anon0;
            za4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cb4.a;
    }
}
