package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.ig;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.misfit.frameworks.buttonservice.log.db.LogDatabase$init$Anon1", f = "LogDatabase.kt", l = {}, m = "invokeSuspend")
public final class LogDatabase$init$Anon1 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LogDatabase this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ LogDatabase$init$Anon1 this$Anon0;

        @DexIgnore
        public Anon1(LogDatabase$init$Anon1 logDatabase$init$Anon1) {
            this.this$Anon0 = logDatabase$init$Anon1;
        }

        @DexIgnore
        public final void run() {
            ig openHelper = this.this$Anon0.this$Anon0.getOpenHelper();
            wd4.a((Object) openHelper, "openHelper");
            openHelper.a().b("CREATE TRIGGER IF NOT EXISTS delete_keep_2000 after insert on log WHEN (select count(*) from log) >= 2000 BEGIN DELETE FROM log WHERE id NOT IN  (SELECT id FROM log ORDER BY timeStamp desc limit 2000); END");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LogDatabase$init$Anon1(LogDatabase logDatabase, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = logDatabase;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        LogDatabase$init$Anon1 logDatabase$init$Anon1 = new LogDatabase$init$Anon1(this.this$Anon0, kc4);
        logDatabase$init$Anon1.p$ = (lh4) obj;
        return logDatabase$init$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LogDatabase$init$Anon1) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            this.this$Anon0.runInTransaction((Runnable) new Anon1(this));
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
