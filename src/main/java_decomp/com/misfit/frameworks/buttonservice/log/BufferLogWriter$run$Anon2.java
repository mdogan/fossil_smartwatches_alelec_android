package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lh4;
import com.fossil.blesdk.obfuscated.oc4;
import com.fossil.blesdk.obfuscated.sc4;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@sc4(c = "com.misfit.frameworks.buttonservice.log.BufferLogWriter$run$Anon2", f = "BufferLogWriter.kt", l = {}, m = "invokeSuspend")
public final class BufferLogWriter$run$Anon2 extends SuspendLambda implements kd4<lh4, kc4<? super cb4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public lh4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BufferLogWriter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$run$Anon2(BufferLogWriter bufferLogWriter, kc4 kc4) {
        super(2, kc4);
        this.this$Anon0 = bufferLogWriter;
    }

    @DexIgnore
    public final kc4<cb4> create(Object obj, kc4<?> kc4) {
        wd4.b(kc4, "completion");
        BufferLogWriter$run$Anon2 bufferLogWriter$run$Anon2 = new BufferLogWriter$run$Anon2(this.this$Anon0, kc4);
        bufferLogWriter$run$Anon2.p$ = (lh4) obj;
        return bufferLogWriter$run$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BufferLogWriter$run$Anon2) create(obj, (kc4) obj2)).invokeSuspend(cb4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        oc4.a();
        if (this.label == 0) {
            za4.a(obj);
            BufferDebugOption access$getDebugOption$p = this.this$Anon0.debugOption;
            if (access$getDebugOption$p != null) {
                IFinishCallback callback = access$getDebugOption$p.getCallback();
                if (callback != null) {
                    callback.onFinish();
                }
            }
            return cb4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
