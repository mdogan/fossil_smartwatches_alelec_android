package com.misfit.frameworks.buttonservice.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum ConnectionStateChange {
    GATT_ON,
    GATT_OFF,
    HID_ON,
    HID_OFF
}
