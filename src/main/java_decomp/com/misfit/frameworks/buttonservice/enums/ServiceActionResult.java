package com.misfit.frameworks.buttonservice.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum ServiceActionResult {
    SUCCEEDED,
    FAILED,
    UNALLOWED_ACTION,
    INTERNAL_ERROR,
    PROCESSING,
    RECEIVED_DATA,
    GET_LATEST_FW,
    LATEST_FW,
    UPDATE_FW_FAILED,
    UPDATE_FW_SUCCESS,
    ASK_FOR_LINK_SERVER,
    ASK_FOR_SERVER_SECRET_KEY,
    ASK_FOR_RANDOM_KEY,
    ASK_FOR_CURRENT_SECRET_KEY,
    ASK_FOR_STOP_WORKOUT,
    ASK_FOR_WATCH_PARAMS
}
