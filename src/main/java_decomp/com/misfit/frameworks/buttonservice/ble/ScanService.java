package com.misfit.frameworks.buttonservice.ble;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.fossil.blesdk.adapter.ScanError;
import com.fossil.blesdk.adapter.filter.DeviceScanFilter;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.DeviceType;
import com.fossil.blesdk.obfuscated.rd4;
import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ScanService implements BluetoothLeAdapter.b {
    @DexIgnore
    public static /* final */ long BLE_SCAN_TIMEOUT; // = 120000;
    @DexIgnore
    public static /* final */ int CONNECT_TIMEOUT; // = 10000;
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((rd4) null);
    @DexIgnore
    public static /* final */ int RETRIEVE_DEVICE_BOND_RSSI_MARK; // = -999999;
    @DexIgnore
    public static /* final */ int RETRIEVE_DEVICE_RSSI_MARK; // = 0;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public Handler autoStopHandler;
    @DexIgnore
    public /* final */ Runnable autoStopTask; // = new ScanService$autoStopTask$Anon1(this);
    @DexIgnore
    public Callback callback;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public boolean isScanning; // = false;
    @DexIgnore
    public MFLog mfLog;
    @DexIgnore
    public long startScanTimestamp;
    @DexIgnore
    public /* final */ long tagTime;

    @DexIgnore
    public interface Callback {
        @DexIgnore
        void onDeviceFound(Device device, int i);

        @DexIgnore
        void onScanFail(ScanError scanError);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(rd4 rd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = ScanService.class.getSimpleName();
        wd4.a((Object) simpleName, "ScanService::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ScanService(Context context2, Callback callback2, long j) {
        wd4.b(context2, "context");
        this.callback = callback2;
        this.tagTime = j;
        Context applicationContext = context2.getApplicationContext();
        wd4.a((Object) applicationContext, "context.applicationContext");
        this.context = applicationContext;
    }

    @DexIgnore
    private final String getCollectionTagByActiveLog() {
        MFLog mFLog = this.mfLog;
        if (mFLog == null) {
            String l = Long.toString(this.tagTime);
            wd4.a((Object) l, "java.lang.Long.toString(tagTime)");
            return l;
        } else if (mFLog != null) {
            return String.valueOf(mFLog.getStartTimeEpoch());
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    private final void log(String str) {
        MFLog mFLog = this.mfLog;
        if (mFLog == null) {
            return;
        }
        if (mFLog != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            MFLog mFLog2 = this.mfLog;
            if (mFLog2 != null) {
                sb.append(mFLog2.getSerial());
                sb.append(" - Scanning] ");
                sb.append(str);
                mFLog.log(sb.toString());
                return;
            }
            wd4.a();
            throw null;
        }
        wd4.a();
        throw null;
    }

    @DexIgnore
    private final void startAutoStopTimer() {
        stopAutoStopTimer();
        this.autoStopHandler = new Handler(Looper.getMainLooper());
        Handler handler = this.autoStopHandler;
        if (handler != null) {
            handler.postDelayed(this.autoStopTask, BLE_SCAN_TIMEOUT);
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    private final void stopAutoStopTimer() {
        Handler handler = this.autoStopHandler;
        if (handler != null) {
            handler.removeCallbacks(this.autoStopTask);
        }
    }

    @DexIgnore
    public final Device buildDeviceBySerial(String str, String str2) {
        wd4.b(str, "serial");
        wd4.b(str2, "macAddress");
        try {
            return BluetoothLeAdapter.l.a(str, str2);
        } catch (Exception e) {
            log("BuildDeviceBySerrial, error:" + e);
            return null;
        }
    }

    @DexIgnore
    public void onDeviceFound(Device device, int i) {
        wd4.b(device, "device");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, ".onDeviceFound " + device.getDeviceInformation().getSerialNumber() + ", hashcode=" + hashCode());
        Callback callback2 = this.callback;
        if (callback2 == null) {
            FLogger.INSTANCE.getLocal().e(TAG, ".onDeviceFound, callback is null");
        } else if (callback2 != null) {
            callback2.onDeviceFound(device, i);
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void onStartScanFailed(ScanError scanError) {
        wd4.b(scanError, "scanError");
        stopScan();
        Callback callback2 = this.callback;
        if (callback2 == null) {
            FLogger.INSTANCE.getLocal().e(TAG, ".onStartScanFail, callback is null");
        } else if (callback2 != null) {
            callback2.onScanFail(scanError);
        } else {
            wd4.a();
            throw null;
        }
        this.callback = null;
    }

    @DexIgnore
    public final void setActiveDeviceLog(String str) {
        wd4.b(str, "serial");
        this.mfLog = MFLogManager.getInstance(this.context).getActiveLog(str);
    }

    @DexIgnore
    public final synchronized void startScan() {
        FLogger.INSTANCE.getLocal().d(TAG, ".startScan");
        this.isScanning = true;
        BluetoothLeAdapter.l.a(new DeviceScanFilter().setDeviceTypes(new DeviceType[]{DeviceType.DIANA, DeviceType.MINI, DeviceType.SLIM, DeviceType.SE1}), (BluetoothLeAdapter.b) this);
        log("Called start scan api v2.");
    }

    @DexIgnore
    public final synchronized void startScanWithAutoStopTimer() {
        FLogger.INSTANCE.getLocal().d(TAG, ".startScanWithAutoStopTimer()");
        startAutoStopTimer();
        this.isScanning = true;
        BluetoothLeAdapter.l.a(new DeviceScanFilter().setDeviceTypes(new DeviceType[]{DeviceType.DIANA, DeviceType.MINI, DeviceType.SLIM, DeviceType.SE1}), (BluetoothLeAdapter.b) this);
        this.startScanTimestamp = System.currentTimeMillis();
    }

    @DexIgnore
    public final synchronized void stopScan() {
        FLogger.INSTANCE.getLocal().d(TAG, ".stopScan");
        this.startScanTimestamp = -1;
        this.isScanning = false;
        stopAutoStopTimer();
        BluetoothLeAdapter.l.b((BluetoothLeAdapter.b) this);
        log("Called stop scan api v2.");
    }
}
