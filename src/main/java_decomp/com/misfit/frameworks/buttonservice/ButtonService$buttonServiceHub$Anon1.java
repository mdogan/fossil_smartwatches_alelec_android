package com.misfit.frameworks.buttonservice;

import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cg4;
import com.fossil.blesdk.obfuscated.oa0;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.ya0;
import com.fossil.fitness.FitnessData;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.AppInfo;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ButtonService$buttonServiceHub$Anon1 extends IButtonConnectivity.Stub {
    @DexIgnore
    public /* final */ /* synthetic */ ButtonService this$Anon0;

    @DexIgnore
    public ButtonService$buttonServiceHub$Anon1(ButtonService buttonService) {
        this.this$Anon0 = buttonService;
    }

    @DexIgnore
    public void addLog(int i, String str, String str2) {
        wd4.b(str, "serial");
        wd4.b(str2, "message");
        if (this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.addLog(i, str, str2);
        }
    }

    @DexIgnore
    public void cancelPairDevice(String str) {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.cancelPairDevice(str);
        }
    }

    @DexIgnore
    public void changePendingLogKey(int i, String str, int i2, String str2) {
        wd4.b(str, "curSerial");
        wd4.b(str2, "newSerial");
        if (this.this$Anon0.isBleSupported(str2)) {
            this.this$Anon0.changePendingLogKey(i, str, i2, str2);
        }
    }

    @DexIgnore
    public void confirmStopWorkout(String str, boolean z) {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported()) {
            this.this$Anon0.confirmStopWorkout(str, z);
        }
    }

    @DexIgnore
    public void connectAllButton() throws RemoteException {
        if (this.this$Anon0.isBleSupported()) {
            this.this$Anon0.connectAllButton();
        }
    }

    @DexIgnore
    public void deleteDataFiles(String str) {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.deleteDataFiles(str);
        }
    }

    @DexIgnore
    public void deleteHeartRateFiles(List<String> list, String str) throws RemoteException {
        wd4.b(list, "fileIds");
        wd4.b(str, "serial");
        this.this$Anon0.deleteHeartRateFiles(list);
    }

    @DexIgnore
    public long deviceCancelCalibration(String str) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.cancelCalibration(str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.EXIT_CALIBRATION, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceClearMapping(String str) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceClearMapping(str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.CLEAN_LINK_MAPPINGS, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceCompleteCalibration(String str) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceCompleteCalibration(str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.APPLY_HAND_POSITION, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void deviceDisconnect(String str) throws RemoteException {
        wd4.b(str, "serial");
        if (!this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.DISCONNECT, ServiceActionResult.FAILED, (Bundle) null);
        } else {
            this.this$Anon0.deviceDisconnect(str);
        }
    }

    @DexIgnore
    public long deviceForceReconnect(String str) {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.forceConnect(str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.FORCE_CONNECT, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceGetBatteryLevel(String str) throws RemoteException {
        wd4.b(str, "serial");
        return this.this$Anon0.isBleSupported(str) ? this.this$Anon0.deviceGetBatteryLevel(str) : ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public long deviceGetCountDown(String str) {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (!this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.GET_COUNTDOWN, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceGetRssi(String str) throws RemoteException {
        wd4.b(str, "serial");
        return this.this$Anon0.isBleSupported(str) ? this.this$Anon0.deviceGetRssi(str) : ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public void deviceMovingHand(String str, HandCalibrationObj handCalibrationObj) throws RemoteException {
        wd4.b(str, "serial");
        wd4.b(handCalibrationObj, "handCalibrationObj");
        if (!this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.MOVE_HAND, ServiceActionResult.FAILED, (Bundle) null);
        } else {
            this.this$Anon0.deviceMovingHand(str, handCalibrationObj);
        }
    }

    @DexIgnore
    public long deviceOta(String str, FirmwareData firmwareData, UserProfile userProfile) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str) && !TextUtils.isEmpty(str) && firmwareData != null && userProfile != null) {
            return this.this$Anon0.deviceOta(str, firmwareData, userProfile);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.OTA, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long devicePlayAnimation(String str) throws RemoteException {
        wd4.b(str, "serial");
        return this.this$Anon0.isBleSupported(str) ? this.this$Anon0.devicePlayAnimation(str) : ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public long deviceReadRealTimeStep(String str) {
        wd4.b(str, "serial");
        return this.this$Anon0.isBleSupported(str) ? this.this$Anon0.deviceReadRealTimeStep(str) : ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public long deviceSendNotification(String str, NotificationBaseObj notificationBaseObj) throws RemoteException {
        wd4.b(str, "serial");
        wd4.b(notificationBaseObj, "newNotification");
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceSendNotification(str, notificationBaseObj);
        }
        return ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public void deviceSetAutoCountdownSetting(long j, long j2) {
    }

    @DexIgnore
    public void deviceSetAutoListAlarm(List<? extends Alarm> list) {
        wd4.b(list, "alarmList");
        this.this$Anon0.deviceSetAutoListAlarm(list);
    }

    @DexIgnore
    public void deviceSetAutoSecondTimezone(String str) {
        wd4.b(str, "secondTimezoneId");
        this.this$Anon0.deviceSetAutoSecondTimezone(str);
    }

    @DexIgnore
    public long deviceSetDisableCountDown(String str) {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (!this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_COUNTDOWN, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceSetEnableCountDown(String str, long j, long j2) {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (!this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_COUNTDOWN, ServiceActionResult.FAILED, (Bundle) null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceSetInactiveNudgeConfig(String str, InactiveNudgeData inactiveNudgeData) throws RemoteException {
        wd4.b(str, "serial");
        wd4.b(inactiveNudgeData, "inactiveNudgeData");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceSetInactiveNudgeConfig(str, inactiveNudgeData);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_INACTIVE_NUDGE_CONFIG, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceSetListAlarm(String str, List<? extends Alarm> list) throws RemoteException {
        wd4.b(str, "serial");
        wd4.b(list, "alarms");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceSetListAlarm(str, list);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_LIST_ALARM, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceSetMapping(String str, List<? extends BLEMapping> list) {
        wd4.b(str, "serial");
        wd4.b(list, "mappings");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceSetMapping(str, list);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_LINK_MAPPING, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceSetSecondTimeZone(String str, String str2) throws RemoteException {
        wd4.b(str, "serial");
        wd4.b(str2, "secondTimezoneId");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceSetSecondTimeZone(str, str2);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_SECOND_TIMEZONE, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceSetVibrationStrength(String str, VibrationStrengthObj vibrationStrengthObj) {
        wd4.b(str, "serial");
        wd4.b(vibrationStrengthObj, "vibrationStrengthLevelObj");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceSetVibrationStrength(str, vibrationStrengthObj);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_VIBRATION_STRENGTH, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceStartCalibration(String str) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceStartCalibration(str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.ENTER_CALIBRATION, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void deviceStartScan() throws RemoteException {
        if (this.this$Anon0.isBleSupported()) {
            ButtonService.access$getScanServiceInstance$p(this.this$Anon0).startScan();
        }
    }

    @DexIgnore
    public long deviceStartSync(String str, UserProfile userProfile) throws RemoteException {
        wd4.b(str, "serial");
        wd4.b(userProfile, "profile");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceStartSync(str, userProfile);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SYNC, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void deviceStopScan() throws RemoteException {
        if (this.this$Anon0.isBleSupported()) {
            ButtonService.access$getScanServiceInstance$p(this.this$Anon0).stopScan();
        }
    }

    @DexIgnore
    public long deviceUnlink(String str) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceUnlink(str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.UNLINK, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long deviceUpdateGoalStep(String str, int i) throws RemoteException {
        wd4.b(str, "serial");
        return this.this$Anon0.isBleSupported(str) ? this.this$Anon0.deviceUpdateGoalStep(str, i) : ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public long disableHeartRateNotification(String str) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceDisableHeartRate(str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.DISABLE_HEART_RATE_NOTIFICATION, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long enableHeartRateNotification(String str) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceEnableHeartRate(str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.ENABLE_HEART_RATE_NOTIFICATION, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public int endLog(int i, String str) throws RemoteException {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.endLog(i, str);
        }
        return 0;
    }

    @DexIgnore
    public boolean forceSwitchDeviceWithoutErase(String str) throws RemoteException {
        wd4.b(str, "newActiveDeviceSerial");
        if (!this.this$Anon0.isBleSupported(str)) {
            return false;
        }
        this.this$Anon0.forceSwitchDeviceWithoutErase(str);
        return true;
    }

    @DexIgnore
    public void forceUpdateDeviceData(DeviceAppResponse deviceAppResponse, String str) throws RemoteException {
        wd4.b(deviceAppResponse, "deviceAppResponse");
        wd4.b(str, "serial");
        this.this$Anon0.sendDeviceAppResponse(deviceAppResponse, str, true);
    }

    @DexIgnore
    public List<String> getActiveSerial() throws RemoteException {
        List<String> allActiveButtonSerial = DevicePreferenceUtils.getAllActiveButtonSerial(this.this$Anon0);
        wd4.a((Object) allActiveButtonSerial, "DevicePreferenceUtils.ge\u2026erial(this@ButtonService)");
        return allActiveButtonSerial;
    }

    @DexIgnore
    public List<BLEMapping> getAutoMapping(String str) throws RemoteException {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.getAutoMapping(str);
        }
        return new ArrayList();
    }

    @DexIgnore
    public int getCommunicatorModeBySerial(String str) throws RemoteException {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.getActiveCommunicatorBySerial(str);
        }
        return CommunicateMode.IDLE.getValue();
    }

    @DexIgnore
    public MisfitDeviceProfile getDeviceProfile(String str) {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.getDeviceProfile(str);
        }
        return null;
    }

    @DexIgnore
    public int getGattState(String str) throws RemoteException {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.getGattState(str);
        }
        return 0;
    }

    @DexIgnore
    public int getHIDState(String str) throws RemoteException {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.getHIDState(str);
        }
        return 0;
    }

    @DexIgnore
    public int[] getListActiveCommunicator() throws RemoteException {
        return this.this$Anon0.getActiveListCommunicator();
    }

    @DexIgnore
    public List<MisfitDeviceProfile> getPairedDevice() throws RemoteException {
        if (this.this$Anon0.isBleSupported()) {
            return this.this$Anon0.getPairedDevice();
        }
        return new ArrayList();
    }

    @DexIgnore
    public List<String> getPairedSerial() throws RemoteException {
        List<String> allPairedButtonSerial = DevicePreferenceUtils.getAllPairedButtonSerial(this.this$Anon0);
        wd4.a((Object) allPairedButtonSerial, "DevicePreferenceUtils.ge\u2026erial(this@ButtonService)");
        return allPairedButtonSerial;
    }

    @DexIgnore
    public List<FitnessData> getSyncData(String str) throws RemoteException {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.getSyncData(str);
        }
        FLogger.INSTANCE.getLocal().d(ButtonService.TAG, ".getSyncData(), device is not support");
        return new ArrayList();
    }

    @DexIgnore
    public void init(String str, String str2, String str3, char c, AppLogInfo appLogInfo, ActiveDeviceInfo activeDeviceInfo, CloudLogConfig cloudLogConfig) throws RemoteException {
        wd4.b(str, "sdkLogV2EndPoint");
        wd4.b(str2, "sdkLogV2AccessKey");
        wd4.b(str3, "sdkLogV2SecretKey");
        wd4.b(appLogInfo, "appLogInfo");
        wd4.b(activeDeviceInfo, "activeDeviceInfo");
        wd4.b(cloudLogConfig, "cloudLogConfig");
        ButtonService.fossilBrand = FossilDeviceSerialPatternUtil.BRAND.fromPrefix(c);
        ButtonService.Companion.setAppInfo(new AppInfo(appLogInfo.getAppVersion(), appLogInfo.getPlatformVersion()));
        FLogger.INSTANCE.updateAppLogInfo(appLogInfo);
        FLogger.INSTANCE.updateActiveDeviceInfo(activeDeviceInfo);
        FLogger.INSTANCE.updateCloudLogConfig(cloudLogConfig);
        if ((!cg4.a(str)) && (!cg4.a(str2)) && (!cg4.a(str3))) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = ButtonService.TAG;
            local.d(access$getTAG$cp, "init sdk end point " + str);
            ya0.c.a(new oa0(str, str2, str3));
        }
    }

    @DexIgnore
    public void interrupt(String str) {
        wd4.b(str, "serial");
        if (!this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.INTERRUPT, ServiceActionResult.FAILED, (Bundle) null);
        } else {
            this.this$Anon0.interrupt(str);
        }
    }

    @DexIgnore
    public void interruptCurrentSession(String str) throws RemoteException {
        wd4.b(str, "serial");
        this.this$Anon0.interruptCurrentSession(str);
    }

    @DexIgnore
    public boolean isLinking(String str) throws RemoteException {
        wd4.b(str, "serial");
        return this.this$Anon0.isBleSupported(str) && this.this$Anon0.isLinking(str);
    }

    @DexIgnore
    public boolean isSyncing(String str) throws RemoteException {
        wd4.b(str, "serial");
        return this.this$Anon0.isSyncing(str);
    }

    @DexIgnore
    public boolean isUpdatingFirmware(String str) throws RemoteException {
        wd4.b(str, "serial");
        return this.this$Anon0.isBleSupported(str) && this.this$Anon0.isUpdatingFirmware(str);
    }

    @DexIgnore
    public void logOut() throws RemoteException {
        this.this$Anon0.logOut();
    }

    @DexIgnore
    public long onPing(String str) {
        wd4.b(str, "serial");
        return this.this$Anon0.isBleSupported(str) ? this.this$Anon0.onPing(str) : System.currentTimeMillis();
    }

    @DexIgnore
    public void onSetWatchParamResponse(String str, boolean z, WatchParamsFileMapping watchParamsFileMapping) throws RemoteException {
        wd4.b(str, "serial");
        this.this$Anon0.onSetWatchParamResponse(str, z, watchParamsFileMapping);
    }

    @DexIgnore
    public long pairDevice(String str, String str2, UserProfile userProfile) {
        wd4.b(str, "serial");
        wd4.b(str2, "macAddress");
        wd4.b(userProfile, "userProfile");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.pairDevice(str, str2, userProfile);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.LINK, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long pairDeviceResponse(String str, PairingResponse pairingResponse) {
        wd4.b(str, "serial");
        wd4.b(pairingResponse, "response");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.pairDeviceResponse(str, pairingResponse);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.LINK, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long playVibration(String str, int i, int i2, boolean z) {
        wd4.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(ButtonService.TAG, "playVibration");
        return ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public long readCurrentWorkoutSession(String str) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.readCurrentWorkoutSession(str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.READ_CURRENT_WORKOUT_SESSION, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void removeActiveSerial(String str) throws RemoteException {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported(str)) {
            DevicePreferenceUtils.removeActiveButtonSerial(this.this$Anon0, str);
        }
    }

    @DexIgnore
    public void removePairedSerial(String str) throws RemoteException {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported(str)) {
            DevicePreferenceUtils.removePairedButtonSerial(this.this$Anon0, str);
        }
    }

    @DexIgnore
    public void resetDeviceSettingToDefault(String str) throws RemoteException {
        wd4.b(str, "serial");
    }

    @DexIgnore
    public long resetHandsToZeroDegree(String str) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.deviceResetHandsToZeroDegree(str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.RESET_HAND, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long sendCurrentSecretKey(String str, String str2) {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.receiveCurrentSecretKey(str, str2);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.EXCHANGE_SECRET_KEY, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void sendCustomCommand(String str, CustomRequest customRequest) throws RemoteException {
        wd4.b(str, "serial");
        wd4.b(customRequest, Constants.COMMAND);
        System.currentTimeMillis();
        if (!this.this$Anon0.isBleSupported(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = ButtonService.TAG;
            local.d(access$getTAG$cp, ".sendCustomRequest() with " + str + " is not supported");
            return;
        }
        this.this$Anon0.sendCustomCommand(str, customRequest);
    }

    @DexIgnore
    public void sendDeviceAppResponse(DeviceAppResponse deviceAppResponse, String str) throws RemoteException {
        wd4.b(deviceAppResponse, "deviceAppResponse");
        wd4.b(str, "serial");
        ButtonService.sendDeviceAppResponse$default(this.this$Anon0, deviceAppResponse, str, false, 4, (Object) null);
    }

    @DexIgnore
    public void sendMicroAppRemoteActivity(String str, DeviceAppResponse deviceAppResponse) throws RemoteException {
        wd4.b(str, "serial");
        wd4.b(deviceAppResponse, "deviceAppResponse");
        if (this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.sendMicroAppRemoteActivity(str, deviceAppResponse);
        }
    }

    @DexIgnore
    public void sendMusicAppResponse(MusicResponse musicResponse, String str) throws RemoteException {
        wd4.b(musicResponse, "musicResponse");
        wd4.b(str, "serial");
        this.this$Anon0.sendMusicAppResponse(musicResponse, str);
    }

    @DexIgnore
    public long sendRandomKey(String str, String str2, int i) {
        wd4.b(str, "serial");
        wd4.b(str2, "randomKey");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.receiveRandomKey(str, str2, i);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.EXCHANGE_SECRET_KEY, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long sendServerSecretKey(String str, String str2, int i) {
        wd4.b(str, "serial");
        wd4.b(str2, "serverSecretKey");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.receiveServerSecretKey(str, str2, i);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.EXCHANGE_SECRET_KEY, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void setActiveSerial(String str, String str2) throws RemoteException {
        wd4.b(str, "serial");
        wd4.b(str2, "macAddress");
        if (this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.setActiveSerial(str, str2);
        }
    }

    @DexIgnore
    public void setAutoBackgroundImageConfig(BackgroundConfig backgroundConfig, String str) throws RemoteException {
        wd4.b(backgroundConfig, "backgroundConfig");
        wd4.b(str, "serial");
        if (!this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_BACKGROUND_IMAGE_CONFIG, ServiceActionResult.FAILED, (Bundle) null);
        } else {
            this.this$Anon0.setAutoBackgroundImageConfig(backgroundConfig, str);
        }
    }

    @DexIgnore
    public void setAutoComplicationAppSettings(ComplicationAppMappingSettings complicationAppMappingSettings, String str) throws RemoteException {
        wd4.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        wd4.b(str, "serial");
        if (!this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_COMPLICATION_APPS, ServiceActionResult.FAILED, (Bundle) null);
        } else {
            this.this$Anon0.setAutoComplicationApps(complicationAppMappingSettings, str);
        }
    }

    @DexIgnore
    public void setAutoMapping(String str, List<? extends BLEMapping> list) throws RemoteException {
        wd4.b(str, "serial");
        wd4.b(list, "mappings");
        if (!this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_MAPPING, ServiceActionResult.FAILED, (Bundle) null);
        } else {
            this.this$Anon0.setAutoMapping(str, list);
        }
    }

    @DexIgnore
    public void setAutoNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings, String str) throws RemoteException {
        wd4.b(appNotificationFilterSettings, "notificationFilterSettings");
        wd4.b(str, "serial");
        if (!this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS, ServiceActionResult.FAILED, (Bundle) null);
        } else {
            this.this$Anon0.setAutoNotificationFilterSettings(appNotificationFilterSettings, str);
        }
    }

    @DexIgnore
    public void setAutoUserBiometricData(UserProfile userProfile) throws RemoteException {
        wd4.b(userProfile, "userProfile");
        this.this$Anon0.setAutoUserBiometricData(userProfile);
    }

    @DexIgnore
    public void setAutoWatchAppSettings(WatchAppMappingSettings watchAppMappingSettings, String str) throws RemoteException {
        wd4.b(watchAppMappingSettings, "watchAppMappingSettings");
        wd4.b(str, "serial");
        if (!this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_WATCH_APPS, ServiceActionResult.FAILED, (Bundle) null);
        } else {
            this.this$Anon0.setAutoWatchApps(watchAppMappingSettings, str);
        }
    }

    @DexIgnore
    public long setBackgroundImageConfig(BackgroundConfig backgroundConfig, String str) throws RemoteException {
        wd4.b(backgroundConfig, "backgroundConfig");
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.setBackgroundImageConfig(backgroundConfig, str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_BACKGROUND_IMAGE_CONFIG, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long setComplicationApps(ComplicationAppMappingSettings complicationAppMappingSettings, String str) throws RemoteException {
        wd4.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.setCompilationApps(complicationAppMappingSettings, str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_COMPLICATION_APPS, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long setFrontLightEnable(String str, boolean z) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.setFrontLightEnable(str, z);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_FRONT_LIGHT_ENABLE, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public long setHeartRateMode(String str, int i) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        HeartRateMode fromValue = HeartRateMode.Companion.fromValue(i);
        if (this.this$Anon0.isBleSupported(str) && fromValue != HeartRateMode.NONE) {
            return this.this$Anon0.setHeartRateMode(str, fromValue);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_HEART_RATE_MODE, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void setImplicitDeviceConfig(UserProfile userProfile, String str) throws RemoteException {
        wd4.b(userProfile, "userProfile");
        wd4.b(str, "serial");
        if (!this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_IMPLICIT_DEVICE_CONFIG, ServiceActionResult.FAILED, (Bundle) null);
        } else {
            this.this$Anon0.setImplicitDeviceConfig(userProfile, str);
        }
    }

    @DexIgnore
    public void setImplicitDisplayUnitSettings(UserDisplayUnit userDisplayUnit, String str) throws RemoteException {
        wd4.b(userDisplayUnit, "userDisplayUnit");
        wd4.b(str, "serial");
        if (!this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_IMPLICIT_DISPLAY_UNIT, ServiceActionResult.FAILED, (Bundle) null);
        } else {
            this.this$Anon0.setImplicitDisplayUnitSettings(userDisplayUnit, str);
        }
    }

    @DexIgnore
    public void setLocalizationData(LocalizationData localizationData) throws RemoteException {
        wd4.b(localizationData, "localizationData");
        this.this$Anon0.setLocalizationData(localizationData);
    }

    @DexIgnore
    public long setNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings, String str) throws RemoteException {
        wd4.b(appNotificationFilterSettings, "notificationFilterSettings");
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.setNotificationFilterSettings(appNotificationFilterSettings, str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_NOTIFICATION_FILTERS, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void setPairedSerial(String str, String str2) throws RemoteException {
        wd4.b(str, "serial");
        wd4.b(str2, "macAddress");
        if (this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.setPairedDevice(str, str2);
        }
    }

    @DexIgnore
    public long setPresetApps(WatchAppMappingSettings watchAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings, BackgroundConfig backgroundConfig, String str) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.setPresetApps(watchAppMappingSettings, complicationAppMappingSettings, backgroundConfig, str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_PRESET_APPS_DATA, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void setSecretKey(String str, String str2) throws RemoteException {
        wd4.b(str, "serial");
        this.this$Anon0.setSecretKey(str, str2);
    }

    @DexIgnore
    public long setWatchApps(WatchAppMappingSettings watchAppMappingSettings, String str) throws RemoteException {
        wd4.b(watchAppMappingSettings, "watchAppMappingSettings");
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.setWatchApps(watchAppMappingSettings, str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_WATCH_APPS, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void simulateDisconnection(String str, int i, int i2, int i3, int i4) {
        wd4.b(str, "serial");
    }

    @DexIgnore
    public void simulatePusherEvent(String str, int i, int i2, int i3, int i4, int i5) throws RemoteException {
        wd4.b(str, "serial");
    }

    @DexIgnore
    public int startLog(int i, String str) throws RemoteException {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.startLog(i, str);
        }
        return 0;
    }

    @DexIgnore
    public long stopCurrentWorkoutSession(String str) throws RemoteException {
        wd4.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.stopCurrentWorkoutSession(str);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.STOP_CURRENT_WORKOUT_SESSION, ServiceActionResult.FAILED, (Bundle) null);
        return currentTimeMillis;
    }

    @DexIgnore
    public void stopLogService(int i) {
        this.this$Anon0.stopLogService(i);
    }

    @DexIgnore
    public boolean switchActiveDevice(String str, UserProfile userProfile) throws RemoteException {
        wd4.b(str, "newActiveDeviceSerial");
        wd4.b(userProfile, "userProfile");
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.switchActiveDevice(str, userProfile);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SWITCH_DEVICE, ServiceActionResult.FAILED, (Bundle) null);
        return false;
    }

    @DexIgnore
    public long switchDeviceResponse(String str, boolean z, int i) throws RemoteException {
        wd4.b(str, "serial");
        if (this.this$Anon0.isBleSupported(str)) {
            return this.this$Anon0.switchDeviceResponse(str, z, i);
        }
        this.this$Anon0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SWITCH_DEVICE, ServiceActionResult.FAILED, (Bundle) null);
        return ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    public void updateActiveDeviceInfoLog(ActiveDeviceInfo activeDeviceInfo) throws RemoteException {
        wd4.b(activeDeviceInfo, "activeDeviceInfo");
        FLogger.INSTANCE.updateActiveDeviceInfo(activeDeviceInfo);
    }

    @DexIgnore
    public void updateAppInfo(String str) {
        wd4.b(str, "appInfoJson");
        this.this$Anon0.updateAppInfo(str);
    }

    @DexIgnore
    public void updateAppLogInfo(AppLogInfo appLogInfo) throws RemoteException {
        wd4.b(appLogInfo, "appLogInfo");
        FLogger.INSTANCE.updateAppLogInfo(appLogInfo);
    }

    @DexIgnore
    public void updatePercentageGoalProgress(String str, boolean z, UserProfile userProfile) throws RemoteException {
        wd4.b(str, "serial");
        wd4.b(userProfile, "userProfile");
        if (this.this$Anon0.isBleSupported(str)) {
            this.this$Anon0.updatePercentageGoalProgress(str, z, userProfile);
        }
    }

    @DexIgnore
    public void updateUserId(String str) {
        wd4.b(str, ButtonService.USER_ID);
        this.this$Anon0.updateUserId(str);
    }
}
