package com.misfit.frameworks.buttonservice.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class HeartRateProvider extends BaseDbProvider {
    @DexIgnore
    public static /* final */ int BUFFER_SIZE; // = 1000000;
    @DexIgnore
    public static /* final */ String DB_NAME; // = "heart_rate_file.db";
    @DexIgnore
    public static /* final */ String TAG; // = "HeartRateProvider";
    @DexIgnore
    public static HeartRateProvider sInstance;

    @DexIgnore
    public HeartRateProvider(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<DataFile, String> getDataFileDao() throws SQLException {
        return this.databaseHelper.getDao(DataFile.class);
    }

    @DexIgnore
    public static synchronized HeartRateProvider getInstance(Context context) {
        HeartRateProvider heartRateProvider;
        synchronized (HeartRateProvider.class) {
            if (sInstance == null) {
                sInstance = new HeartRateProvider(context, DB_NAME);
            }
            heartRateProvider = sInstance;
        }
        return heartRateProvider;
    }

    @DexIgnore
    public void clearFiles() {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.i(str, "Inside " + TAG + ".clearHwLog");
            getDataFileDao().deleteBuilder().delete();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.e(str2, "Error inside " + TAG + ".clearHwLog - e=" + e);
        }
    }

    @DexIgnore
    public void deleteDataFile(DataFile dataFile) {
        try {
            getDataFileDao().delete(dataFile);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "Error inside " + TAG + ".deleteDataFile - e=" + e);
        }
    }

    @DexIgnore
    public List<DataFile> getAllDataFiles(long j) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<DataFile, String> queryBuilder = getDataFileDao().queryBuilder();
            queryBuilder.where().lt("syncTime", Long.valueOf(j));
            List<DataFile> query = getDataFileDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "Error inside " + TAG + ".getAllDataFiles - e=" + e);
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x012a, code lost:
        if (r2 != null) goto L_0x012c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x012c, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x015a, code lost:
        if (r2 != null) goto L_0x012c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x015d, code lost:
        return r9;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0160  */
    public DataFile getDataFile(String str, String str2) {
        DataFile dataFile;
        long j;
        String str3;
        int i;
        String str4 = str;
        SQLiteDatabase readableDatabase = this.databaseHelper.getReadableDatabase();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT length(dataFile), serial, syncTime FROM dataFile WHERE `key` = ?");
        sb.append(str2 == null ? "" : " AND serial = ?");
        Cursor rawQuery = readableDatabase.rawQuery(sb.toString(), str2 == null ? new String[]{str4} : new String[]{str4, str2});
        try {
            QueryBuilder<DataFile, String> queryBuilder = getDataFileDao().queryBuilder();
            queryBuilder.where().eq("key", str4);
            if (rawQuery == null || rawQuery.getCount() <= 0) {
                str3 = "";
                i = 0;
                j = 0;
            } else {
                rawQuery.moveToFirst();
                i = rawQuery.getInt(0);
                str3 = rawQuery.getString(1);
                j = rawQuery.getLong(2);
                rawQuery.close();
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str5 = TAG;
            local.d(str5, "Inside .getDataFile - fileData len=" + i);
            if (i > 1000000) {
                StringBuffer stringBuffer = new StringBuffer();
                Cursor cursor = rawQuery;
                long j2 = 0;
                while (j2 < ((long) i)) {
                    long j3 = j2 + 1;
                    try {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("SELECT substr(dataFile, ?, ? ) FROM dataFile WHERE `key` = ?");
                        sb2.append(str2 == null ? "" : " AND serial = ?");
                        cursor = readableDatabase.rawQuery(sb2.toString(), str2 == null ? new String[]{Long.toString(j3), Long.toString(1000000), str4} : new String[]{Long.toString(j3), Long.toString(1000000), str4, str2});
                        if (cursor != null && cursor.getCount() > 0) {
                            cursor.moveToFirst();
                            String string = cursor.getString(0);
                            stringBuffer.append(string);
                            j2 += (long) string.length();
                        }
                    } catch (Exception e) {
                        e = e;
                        rawQuery = cursor;
                        dataFile = null;
                        try {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str6 = TAG;
                            local2.e(str6, "Error inside " + TAG + ".getDataFile - e=" + e);
                        } catch (Throwable th) {
                            th = th;
                            if (rawQuery != null) {
                                rawQuery.close();
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        rawQuery = cursor;
                        if (rawQuery != null) {
                        }
                        throw th;
                    }
                }
                rawQuery = cursor;
                dataFile = new DataFile(str, stringBuffer.toString(), str3, j);
            } else {
                dataFile = getDataFileDao().queryForFirst(queryBuilder.prepare());
            }
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (Exception e2) {
                    e = e2;
                }
            }
        } catch (Exception e3) {
            e = e3;
            dataFile = null;
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            String str62 = TAG;
            local22.e(str62, "Error inside " + TAG + ".getDataFile - e=" + e);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{DataFile.class};
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    public void saveDataFile(DataFile dataFile) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside " + TAG + ".saveDataFile - dataFile=" + dataFile);
        try {
            if (getDataFile(dataFile.key, dataFile.serial) == null) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "Inside " + TAG + ".saveDataFile - Saving dataFile=" + dataFile);
                getDataFileDao().create(dataFile);
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.e(str3, "Error inside " + TAG + ".saveDataFile -e=Data file existed");
            }
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local4.e(str4, "Error inside " + TAG + ".saveDataFile - e=" + e);
        }
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = TAG;
        local5.d(str5, "Inside " + TAG + ".saveDataFile - dataFile=" + dataFile + " - DONE");
    }

    @DexIgnore
    public DataFile getDataFile(String str) {
        return getDataFile(str, (String) null);
    }
}
