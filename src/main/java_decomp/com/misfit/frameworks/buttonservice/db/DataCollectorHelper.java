package com.misfit.frameworks.buttonservice.db;

import android.content.Context;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class DataCollectorHelper {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Context val$context;
        @DexIgnore
        public /* final */ /* synthetic */ HardwareLog val$hwLog;

        @DexIgnore
        public Anon1(HardwareLog hardwareLog, Context context) {
            this.val$hwLog = hardwareLog;
            this.val$context = context;
        }

        @DexIgnore
        public void run() {
            if (this.val$hwLog != null) {
                HwLogProvider.getInstance(this.val$context).saveHwLog(this.val$hwLog);
            }
        }
    }

    @DexIgnore
    public static void saveDataFile(Context context, DataFile dataFile) {
        if (dataFile != null) {
            DataFileProvider.getInstance(context).saveDataFile(dataFile);
        }
    }

    @DexIgnore
    public static void saveHeartRateFile(Context context, DataFile dataFile) {
        if (dataFile != null) {
            HeartRateProvider.getInstance(context).saveDataFile(dataFile);
        }
    }

    @DexIgnore
    public static void saveHwLog(Context context, HardwareLog hardwareLog) {
        Executors.newSingleThreadExecutor().execute(new Anon1(hardwareLog, context));
    }
}
