package com.misfit.frameworks.buttonservice.extensions;

import com.fossil.blesdk.obfuscated.wd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmExtensionKt {
    @DexIgnore
    public static final AlarmSetting.AlarmDay convertCalendarDayToBleDay(int i) {
        switch (i) {
            case 1:
                return AlarmSetting.AlarmDay.SUNDAY;
            case 2:
                return AlarmSetting.AlarmDay.MONDAY;
            case 3:
                return AlarmSetting.AlarmDay.TUESDAY;
            case 4:
                return AlarmSetting.AlarmDay.WEDNESDAY;
            case 5:
                return AlarmSetting.AlarmDay.THURSDAY;
            case 6:
                return AlarmSetting.AlarmDay.FRIDAY;
            case 7:
                return AlarmSetting.AlarmDay.SATURDAY;
            default:
                FLogger.INSTANCE.getLocal().e("AlarmExtension", "Calendar day isn't correct");
                return AlarmSetting.AlarmDay.MONDAY;
        }
    }

    @DexIgnore
    public static final boolean isSame(List<AlarmSetting> list, List<AlarmSetting> list2) {
        boolean z = true;
        boolean z2 = list == null || list.isEmpty();
        boolean z3 = list2 == null || list2.isEmpty();
        if (z2 || z3) {
            if (!z2 || !z3) {
                return false;
            }
            return true;
        } else if (list != null) {
            int size = list.size();
            if (list2 == null) {
                wd4.a();
                throw null;
            } else if (size != list2.size()) {
                return false;
            } else {
                ArrayList arrayList = new ArrayList(list);
                ArrayList arrayList2 = new ArrayList(list2);
                sortMultiAlarmSettingList(arrayList);
                sortMultiAlarmSettingList(arrayList2);
                int size2 = arrayList.size();
                for (int i = 0; i < size2; i++) {
                    Object obj = arrayList.get(i);
                    wd4.a(obj, "tempSetting1[i]");
                    Object obj2 = arrayList2.get(i);
                    wd4.a(obj2, "tempSetting2[i]");
                    z = wd4.a((Object) ((AlarmSetting) obj).toString(), (Object) ((AlarmSetting) obj2).toString());
                    if (!z) {
                        break;
                    }
                }
                return z;
            }
        } else {
            wd4.a();
            throw null;
        }
    }

    @DexIgnore
    public static final void sortMultiAlarmSettingList(List<AlarmSetting> list) {
        Collections.sort(list, AlarmExtensionKt$sortMultiAlarmSettingList$comparator$Anon1.INSTANCE);
    }

    @DexIgnore
    public static final ArrayList<AlarmSetting> toBleAlarmSettings(List<? extends Alarm> list) {
        wd4.b(list, "$this$toBleAlarmSettings");
        ArrayList<AlarmSetting> arrayList = new ArrayList<>();
        for (Alarm alarm : list) {
            int[] days = alarm.getDays();
            if ((days != null ? days.length : 0) == 0) {
                int alarmMinute = alarm.getAlarmMinute() / 60;
                arrayList.add(new AlarmSetting(alarmMinute, alarm.getAlarmMinute() - (alarmMinute * 60)));
            } else {
                HashSet hashSet = new HashSet();
                if (days != null) {
                    for (int convertCalendarDayToBleDay : days) {
                        hashSet.add(convertCalendarDayToBleDay(convertCalendarDayToBleDay));
                    }
                    int alarmMinute2 = alarm.getAlarmMinute() / 60;
                    arrayList.add(new AlarmSetting(alarmMinute2, alarm.getAlarmMinute() - (alarmMinute2 * 60), alarm.isRepeat(), hashSet));
                } else {
                    wd4.a();
                    throw null;
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final com.fossil.blesdk.device.data.alarm.Alarm[] toSDKV2Setting(List<AlarmSetting> list) {
        wd4.b(list, "$this$toSDKV2Setting");
        ArrayList arrayList = new ArrayList();
        for (AlarmSetting sDKV2Setting : list) {
            com.fossil.blesdk.device.data.alarm.Alarm sDKV2Setting2 = sDKV2Setting.toSDKV2Setting();
            if (sDKV2Setting2 != null) {
                arrayList.add(sDKV2Setting2);
            }
        }
        Object[] array = arrayList.toArray(new com.fossil.blesdk.device.data.alarm.Alarm[0]);
        if (array != null) {
            return (com.fossil.blesdk.device.data.alarm.Alarm[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
